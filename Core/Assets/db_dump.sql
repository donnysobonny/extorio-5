--
-- Name: core_classes_models_api; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_api (
    id integer NOT NULL,
    label text,
    class text,
    namespace text,
    description text,
    "extensionName" text,
    "allRead" boolean,
    "readGroups" integer[],
    name text
);


--
-- Name: core_classes_models_api_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_api_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_asset; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_asset (
    id integer NOT NULL,
    "extensionName" text,
    "fileName" text,
    "dirPath" text,
    "contentTypeMain" text,
    "contentTypeSub" text,
    "isFile" boolean,
    "webUrl" text,
    "localUrl" text
);


--
-- Name: core_classes_models_asset_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_asset_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_block; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_block (
    id integer NOT NULL,
    name text,
    description text,
    config text,
    "isHidden" boolean,
    "isPublic" boolean,
    "extensionName" text,
    processor integer,
    "allRead" boolean,
    "readGroups" integer[],
    "modifyGroups" integer[],
    category integer,
    "subCategory" integer
);


--
-- Name: core_classes_models_block_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_block_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_blockcategory; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_blockcategory (
    id integer NOT NULL,
    name text,
    "parentId" integer,
    description text,
    icon text,
    "extensionName" text
);


--
-- Name: core_classes_models_blockcategory_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_blockcategory_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_blockprocessor; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_blockprocessor (
    id integer NOT NULL,
    label text,
    class text,
    namespace text,
    description text,
    "extensionName" text,
    "inlineEnabled" boolean,
    "allInline" boolean,
    "inlineGroups" integer[],
    name text,
    category integer
);


--
-- Name: core_classes_models_blockprocessor_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_blockprocessor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_blockprocessorcategory; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_blockprocessorcategory (
    id integer NOT NULL,
    name text,
    description text,
    icon text,
    "extensionName" text
);


--
-- Name: core_classes_models_blockprocessorcategory_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_blockprocessorcategory_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_emailtemplate; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_emailtemplate (
    id integer NOT NULL,
    name text,
    description text,
    body text,
    "extensionName" text,
    subject text
);


--
-- Name: core_classes_models_emailtemplate_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_emailtemplate_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_emailtransport; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_emailtransport (
    id integer NOT NULL,
    subject text,
    body text,
    "fromName" text,
    "fromEmail" text,
    "toName" text,
    "toEmail" text,
    cc json,
    bcc json,
    "dateCreated" timestamp without time zone,
    "dateSent" timestamp without time zone,
    "numSent" integer,
    "errorMessage" text,
    template integer,
    "mergeVars" json
);


--
-- Name: core_classes_models_emailtransport_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_emailtransport_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_enum; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_enum (
    id integer NOT NULL,
    class text,
    label text,
    namespace text,
    description text,
    "extensionName" text,
    "isHidden" boolean,
    "values" json,
    name text
);


--
-- Name: core_classes_models_enum_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_enum_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_extension; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_extension (
    id integer NOT NULL,
    name text,
    "isInstalled" boolean,
    namespace text,
    build integer,
    "isInternal" boolean
);


--
-- Name: core_classes_models_extension_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_extension_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_language; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_language (
    id integer NOT NULL,
    name text,
    enabled boolean,
    "sLocale" text,
    "mLocale" text,
    image text
);


--
-- Name: core_classes_models_language_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_language_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_layout; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_layout (
    id integer NOT NULL,
    name text,
    description text,
    "extensionName" text,
    layout json
);


--
-- Name: core_classes_models_layout_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_layout_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_model; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_model (
    label text,
    class text,
    id integer NOT NULL,
    namespace text,
    "baseClass" text,
    "baseNamespace" text,
    description text,
    properties integer[],
    "extensionName" text,
    "isHidden" boolean,
    name text
);


--
-- Name: core_classes_models_model_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_model_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_modelwebhook; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_modelwebhook (
    id integer NOT NULL,
    name text,
    action text,
    "modelNamespace" text,
    location text,
    "verificationKey" text,
    "extensionName" text
);


--
-- Name: core_classes_models_modelwebhook_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_modelwebhook_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_page; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_page (
    id integer NOT NULL,
    name text,
    description text,
    address text,
    "default" boolean,
    class text,
    "controllerNamespace" text,
    "viewNamespace" text,
    "extensionName" text,
    "prependToHead" text,
    "appendToHead" text,
    "prependToBody" text,
    "appendToBody" text,
    "isPublic" boolean,
    "isHidden" boolean,
    "publishDate" date,
    "unpublishDate" date,
    template integer,
    theme integer,
    layout json,
    "allRead" boolean,
    "readGroups" integer[],
    "modifyGroups" integer[],
    "readFailPageId" integer
);


--
-- Name: core_classes_models_page_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_page_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_property; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_property (
    id integer NOT NULL,
    label text,
    name text,
    description text,
    "propertyType" text,
    "basicType" text,
    "complexType" text,
    "childModelNamespace" text,
    "enumNamespace" text,
    "isIndex" boolean
);


--
-- Name: core_classes_models_property_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_property_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_task; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_task (
    id integer NOT NULL,
    label text,
    class text,
    namespace text,
    description text,
    "extensionName" text,
    "allRead" boolean,
    "readGroups" integer[],
    name text
);


--
-- Name: core_classes_models_task_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_task_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_template; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_template (
    id integer NOT NULL,
    name text,
    description text,
    "default" boolean,
    "extensionName" text,
    "prependToBody" text,
    "prependToHead" text,
    "appendToBody" text,
    "appendToHead" text,
    "isHidden" boolean,
    layout json,
    "modifyGroups" integer[]
);


--
-- Name: core_classes_models_template_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_template_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_theme; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_theme (
    id integer NOT NULL,
    name text,
    description text,
    "default" boolean,
    "extensionName" text,
    "isHidden" boolean,
    label text
);


--
-- Name: core_classes_models_theme_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_theme_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_translation; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_translation (
    id integer NOT NULL,
    "baseId" integer,
    "languageId" integer,
    msgstr text,
    msgstr_plural text
);


--
-- Name: core_classes_models_translation_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_translation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_translationbase; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_translationbase (
    id integer NOT NULL,
    plural boolean,
    msgid text,
    msgid_plural text,
    comments text,
    "extensionName" text
);


--
-- Name: core_classes_models_translationbase_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_translationbase_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_user; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_user (
    id integer NOT NULL,
    username text,
    password text,
    email text,
    "canLogin" boolean,
    "numLogin" integer,
    "dateLogin" timestamp without time zone,
    "dateCreated" timestamp without time zone,
    "dateUpdated" timestamp without time zone,
    firstname text,
    lastname text,
    avatar text,
    "userGroup" integer,
    bio text,
    shortname text,
    longname text,
    language integer,
    "dateActive" timestamp without time zone,
    address1 text,
    address2 text,
    city text,
    region text,
    "postalCode" text,
    country text,
    "acceptsMarketing" boolean,
    title text,
    gender text,
    telno text,
    middlenames text
);


--
-- Name: core_classes_models_user_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_user_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_useraction; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_useraction (
    id integer NOT NULL,
    "userId" integer,
    type integer,
    date date,
    "time" time without time zone,
    detail1 text,
    detail2 text,
    detail3 text
);


--
-- Name: core_classes_models_useraction_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_useraction_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_useractiontype; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_useractiontype (
    id integer NOT NULL,
    name text,
    description text,
    "detail1Description" text,
    "detail2Description" text,
    "detail3Description" text,
    "extensionName" text
);


--
-- Name: core_classes_models_useractiontype_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_useractiontype_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_usercontact; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_usercontact (
    id integer NOT NULL,
    "userId" integer,
    "byUserId" integer,
    type text,
    topic integer,
    identifier text,
    date date,
    "time" time without time zone
);


--
-- Name: core_classes_models_usercontact_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_usercontact_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_usercontacttopic; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_usercontacttopic (
    id integer NOT NULL,
    name text,
    description text,
    "extensionName" text
);


--
-- Name: core_classes_models_usercontacttopic_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_usercontacttopic_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_usergroup; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_usergroup (
    id integer NOT NULL,
    name text,
    description text,
    privileges integer[],
    "extensionName" text,
    "manageAll" boolean,
    "manageGroups" integer[]
);


--
-- Name: core_classes_models_usergroup_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_usergroup_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_usernote; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_usernote (
    id integer NOT NULL,
    "userId" integer,
    "updatedById" integer,
    "dateUpdated" timestamp without time zone,
    body text
);


--
-- Name: core_classes_models_usernote_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_usernote_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_userprivilege; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_userprivilege (
    id integer NOT NULL,
    name text,
    description text,
    category integer,
    "extensionName" text
);


--
-- Name: core_classes_models_userprivilege_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_userprivilege_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_userprivilegecategory; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_userprivilegecategory (
    id integer NOT NULL,
    name text,
    description text,
    icon text,
    "extensionName" text
);


--
-- Name: core_classes_models_userprivilegecategory_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_userprivilegecategory_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_userreport; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_userreport (
    id integer NOT NULL,
    name text,
    description text,
    query json,
    "extensionName" text
);


--
-- Name: core_classes_models_userreport_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_userreport_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: core_classes_models_usersession; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_classes_models_usersession (
    id integer NOT NULL,
    session text,
    "remoteIP" text,
    "expiryDate" timestamp without time zone,
    "userId" integer
);


--
-- Name: core_classes_models_usersession_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE core_classes_models_usersession_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: orm_modcache; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE orm_modcache (
    id integer NOT NULL,
    "modelNamespace" text,
    "modelInstanceId" integer,
    action text,
    "timestamp" timestamp without time zone,
    "dataBefore" json,
    "dataAfter" json,
    "userId" integer
);


--
-- Name: orm_modcache_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE orm_modcache_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: orm_modcache_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE orm_modcache_id_seq OWNED BY orm_modcache.id;


--
-- Name: tasks_livetask; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tasks_livetask (
    pid integer,
    task text,
    params text,
    "accessedByUserId" integer,
    status text,
    log text,
    "errorMessage" text,
    "isHidden" boolean,
    "dateCreated" timestamp without time zone,
    "dateUpdated" timestamp without time zone,
    id integer NOT NULL,
    action text
);


--
-- Name: tasks_livetask_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tasks_livetask_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tasks_livetask_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tasks_livetask_id_seq OWNED BY tasks_livetask.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY orm_modcache ALTER COLUMN id SET DEFAULT nextval('orm_modcache_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tasks_livetask ALTER COLUMN id SET DEFAULT nextval('tasks_livetask_id_seq'::regclass);


--
-- Data for Name: core_classes_models_api; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (9, 'assets', 'Assets', '\Core\Components\Apis\Assets', 'Api used to create and manage assets', 'Core', false, '{2,1}', 'Assets');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (17, 'blocks', 'Blocks', '\Core\Components\Apis\Blocks', 'This api is used to get, create and modify blocks', 'Core', false, '{2,1}', 'Blocks');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (3, 'users', 'Users', '\Core\Components\Apis\Users', 'The users api, used to login/logout/register users externally', 'Core', true, '{}', 'Users');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (2, 'model-viewer', 'ModelViewer', '\Core\Components\Apis\ModelViewer', 'This api is used to generate the model views.', 'Core', false, '{2,1}', 'Model Viewer');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (19, 'block-categories', 'BlockCategories', '\Core\Components\Apis\BlockCategories', 'This api is used to get, create and modify block categories', 'Core', false, '{2,1}', 'Block Categories');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (21, 'block-processor-categories', 'BlockProcessorCategories', '\Core\Components\Apis\BlockProcessorCategories', 'This api is used to get, create and modify block processor categories', 'Core', false, '{2,1}', 'Block Processor Categories');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (22, 'block-processors', 'BlockProcessors', '\Core\Components\Apis\BlockProcessors', 'This api is used to get, create and modify block processors', 'Core', false, '{2,1}', 'Block Processors');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (23, 'user-groups', 'UserGroups', '\Core\Components\Apis\UserGroups', 'This api is used to get, create and modify user groups', 'Core', false, '{2,1}', 'User Groups');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (24, 'models', 'Models', '\Core\Components\Apis\Models', 'This api is used to get and modify models', 'Core', false, '{2,1}', 'Models');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (25, 'properties', 'Properties', '\Core\Components\Apis\Properties', 'This api is currently just used to get a property', 'Core', false, '{2,1}', 'Properties');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (26, 'user-notes', 'UserNotes', '\Core\Components\Apis\UserNotes', 'This api is used to get, create and modify user notes', 'Core', false, '{2,1}', 'User Notes');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (27, 'enums', 'Enums', '\Core\Components\Apis\Enums', 'This api is used to get and update enum values', 'Core', false, '{2,1}', 'Enums');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (28, 'pages', 'Pages', '\Core\Components\Apis\Pages', 'This api is used to modify a page', 'Core', false, '{2,1}', 'Pages');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (29, 'templates', 'Templates', '\Core\Components\Apis\Templates', 'This api is used to modify a template', 'Core', false, '{2,1}', 'Templates');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (30, 'translations', 'Translations', '\Core\Components\Apis\Translations', 'This api is used to get, create, modify and delete translations', 'Core', false, '{2,1}', 'Translations');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (31, 'localization', 'Localization', '\Core\Components\Apis\Localization', 'Functionality related to localization', 'Core', true, '{}', 'Localization');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (32, 'user-actions', 'UserActions', '\Core\Components\Apis\UserActions', 'This api is used to get user actions', 'Core', true, '{}', 'User Actions');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (33, 'user-reports', 'UserReports', '\Core\Components\Apis\UserReports', 'This api is used to manage user reports', 'Core', true, '{}', 'User Reports');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (34, 'user-action-types', 'UserActionTypes', '\Core\Components\Apis\UserActionTypes', 'This api is used to get user action types', 'Core', true, '{}', 'User Action Types');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (35, 'titles', 'Titles', '\Core\Components\Apis\Titles', 'This api is used to get the user titles enum values', 'Core', true, '{}', 'Titles');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (36, 'genders', 'Genders', '\Core\Components\Apis\Genders', 'This api is used to get the values from the genders enum', 'Core', true, '{}', 'Genders');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (37, 'countries', 'Countries', '\Core\Components\Apis\Countries', 'This api is used to get the values of the Countries enum', 'Core', true, '{}', 'Countries');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (38, 'user-contact-topics', 'UserContactTopics', '\Core\Components\Apis\UserContactTopics', '', 'Core', true, '{}', 'User Contact Topics');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (39, 'user-contact-types', 'UserContactTypes', '\Core\Components\Apis\UserContactTypes', '', 'Core', true, '{}', 'User Contact Types');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (40, 'user-contacts', 'UserContacts', '\Core\Components\Apis\UserContacts', 'This api is used to manage user contacts', 'Core', true, '{}', 'User Contacts');
INSERT INTO core_classes_models_api (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (41, 'font-awesome-icons', 'FontAwesomeIcons', '\Core\Components\Apis\FontAwesomeIcons', 'This api is used to get the values of the font awesome icons enum', 'Core', true, '{}', 'Font Awesome Icons');


--
-- Name: core_classes_models_api_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_api_seq', 41, true);


--
-- Data for Name: core_classes_models_asset; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_asset (id, "extensionName", "fileName", "dirPath", "contentTypeMain", "contentTypeSub", "isFile", "webUrl", "localUrl") VALUES (6, 'Application', 'images', '/', '', '', false, '/Application/Assets/images', 'Application/Assets/images');


--
-- Name: core_classes_models_asset_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_asset_seq', 83, true);


--
-- Data for Name: core_classes_models_block; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_block (id, name, description, config, "isHidden", "isPublic", "extensionName", processor, "allRead", "readGroups", "modifyGroups", category, "subCategory") VALUES (12, 'Extorio admin menu left', 'Displays the extorio admin menu left', '', true, true, 'Core', 1, false, '{2,1}', '{}', 1, 4);
INSERT INTO core_classes_models_block (id, name, description, config, "isHidden", "isPublic", "extensionName", processor, "allRead", "readGroups", "modifyGroups", category, "subCategory") VALUES (14, '401 access denied content', 'The default 401 access denied page content', 'content=%3Cdiv+class%3D%22jumbotron%22%3E%3Cdiv+class%3D%22container%22%3E%3Ch1%3EOops...access+denied!%3C%2Fh1%3E%3Cp%3EIt+looks+like+the+resource+that+you+are+trying+to+access+is+restricted...+%3A(%3C%2Fp%3E%3Cp%3E%3Ca+href%3D%22%2F%22%3EGo+to+homepage%3C%2Fa%3E%3C%2Fp%3E%3Cp%3E%3Cbr%3E%3C%2Fp%3E%3C%2Fdiv%3E%3C%2Fdiv%3E', false, true, 'Application', 17, true, '{}', '{2,1}', 6, 9);
INSERT INTO core_classes_models_block (id, name, description, config, "isHidden", "isPublic", "extensionName", processor, "allRead", "readGroups", "modifyGroups", category, "subCategory") VALUES (15, '404 page not found content', 'The default content for the 404 page not found page', 'content=%3Cdiv+class%3D%22jumbotron%22%3E%3Cdiv+class%3D%22container%22%3E%3Ch1%3EOops...not+found!%3C%2Fh1%3E%3Cp%3EIt+looks+like+the+resource+that+you+are+trying+to+access+could+not+be+found...+%3A(%3C%2Fp%3E%3Cp%3E%3Ca+href%3D%22%2F%22%3EGo+to+homepage%3C%2Fa%3E%3C%2Fp%3E%3Cp%3E%3Cbr%3E%3C%2Fp%3E%3C%2Fdiv%3E%3C%2Fdiv%3E', false, true, 'Application', 17, true, '{}', '{2,1}', 6, 9);
INSERT INTO core_classes_models_block (id, name, description, config, "isHidden", "isPublic", "extensionName", processor, "allRead", "readGroups", "modifyGroups", category, "subCategory") VALUES (45, 'Basic Header', 'This displays a basic header on your website, showing the site name and description', 'container_type=none&container_title=', false, true, 'Application', 13, true, '{}', '{2,1}', 1, 2);
INSERT INTO core_classes_models_block (id, name, description, config, "isHidden", "isPublic", "extensionName", processor, "allRead", "readGroups", "modifyGroups", category, "subCategory") VALUES (44, 'Basic Footer', 'This displays a basic footer on your website, containing your company name', 'align=left', false, true, 'Application', 12, true, '{}', '{2,1}', 1, 3);
INSERT INTO core_classes_models_block (id, name, description, config, "isHidden", "isPublic", "extensionName", processor, "allRead", "readGroups", "modifyGroups", category, "subCategory") VALUES (83, 'Default Nav Menu', 'The default navigation menu, generated by your installed extensions', 'container_type=none&container_title=&displayhome=on&type=tabs&style=', false, true, 'Application', 32, true, '{}', '{2,1}', 6, 7);
INSERT INTO core_classes_models_block (id, name, description, config, "isHidden", "isPublic", "extensionName", processor, "allRead", "readGroups", "modifyGroups", category, "subCategory") VALUES (51, 'Default breadcrumbs', 'Displays breadcrumbs on the page', 'container_type=none&container_title=&trail_to_default=on', false, true, 'Application', 16, true, '{}', '{2,1}', 6, 7);
INSERT INTO core_classes_models_block (id, name, description, config, "isHidden", "isPublic", "extensionName", processor, "allRead", "readGroups", "modifyGroups", category, "subCategory") VALUES (47, 'Default page content', 'Displays the content on the default page', 'container_title=&container_type=none&content=%3Cdiv+class%3D%22jumbotron%22%3E%3Cdiv+class%3D%22container%22%3E%3Ch1%3EWelcome+to+your+Extorio+website!%3C%2Fh1%3E%3Cp%3EYou+should+probably+remove+or+change+this...+%3A)%3C%2Fp%3E%3C%2Fdiv%3E%3C%2Fdiv%3E', false, true, 'Application', 17, true, '{}', '{2,1}', 6, 9);
INSERT INTO core_classes_models_block (id, name, description, config, "isHidden", "isPublic", "extensionName", processor, "allRead", "readGroups", "modifyGroups", category, "subCategory") VALUES (60, 'Horizontal space', 'Displays a horizontal space, useful for separating horizontal content', '', false, true, 'Application', 21, true, '{}', '{}', 6, 10);
INSERT INTO core_classes_models_block (id, name, description, config, "isHidden", "isPublic", "extensionName", processor, "allRead", "readGroups", "modifyGroups", category, "subCategory") VALUES (61, 'Horizontal rule', 'Displays a horizontal rule, useful for creating a defined space between horizontal content', '', false, true, 'Application', 17, true, '{}', '{}', 6, 10);
INSERT INTO core_classes_models_block (id, name, description, config, "isHidden", "isPublic", "extensionName", processor, "allRead", "readGroups", "modifyGroups", category, "subCategory") VALUES (50, 'Extorio admin breadcrumbs', 'Displays the breadcrumbs for extorio''s admin pages', '', true, true, 'Core', 16, false, '{2,1}', '{}', 6, 7);
INSERT INTO core_classes_models_block (id, name, description, config, "isHidden", "isPublic", "extensionName", processor, "allRead", "readGroups", "modifyGroups", category, "subCategory") VALUES (90, 'Extorio admin menu top', 'Displays the extorio admin menu top', '', true, true, 'Core', 41, false, '{2,1}', '{}', 1, 2);
INSERT INTO core_classes_models_block (id, name, description, config, "isHidden", "isPublic", "extensionName", processor, "allRead", "readGroups", "modifyGroups", category, "subCategory") VALUES (46, 'Basic Page Header', 'This displays the page''s name and description as a header', 'container_type=none&container_title=', false, true, 'Application', 14, true, '{}', '{}', 6, 7);
INSERT INTO core_classes_models_block (id, name, description, config, "isHidden", "isPublic", "extensionName", processor, "allRead", "readGroups", "modifyGroups", category, "subCategory") VALUES (62, 'Default user menu', 'Displays the default user menu', 'container_type=none&container_title=&align=right', false, true, 'Application', 24, true, '{}', '{}', 1, 2);
INSERT INTO core_classes_models_block (id, name, description, config, "isHidden", "isPublic", "extensionName", processor, "allRead", "readGroups", "modifyGroups", category, "subCategory") VALUES (102, 'Language Selector', 'Select a language to display the website in', 'container_type=none&container_title=&size=btn-xs&context=btn-default&menualign=left&textalign=left', false, true, 'Application', 42, true, '{}', '{}', 1, 2);


--
-- Name: core_classes_models_block_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_block_seq', 102, true);


--
-- Data for Name: core_classes_models_blockcategory; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_blockcategory (id, name, "parentId", description, icon, "extensionName") VALUES (1, 'Templates', 0, 'Blocks suitable tor template layouts', 'adjust', 'Core');
INSERT INTO core_classes_models_blockcategory (id, name, "parentId", description, icon, "extensionName") VALUES (2, 'Header', 1, 'Blocks suitable for the header of a template layout', 'adjust', 'Core');
INSERT INTO core_classes_models_blockcategory (id, name, "parentId", description, icon, "extensionName") VALUES (3, 'Footer', 1, 'Blocks suitable for the footer of a template layout', 'adjust', 'Core');
INSERT INTO core_classes_models_blockcategory (id, name, "parentId", description, icon, "extensionName") VALUES (4, 'Main Body', 1, 'Blocks suitable for the main body of a template layout', 'adjust', 'Core');
INSERT INTO core_classes_models_blockcategory (id, name, "parentId", description, icon, "extensionName") VALUES (5, 'Snippets', 1, 'Blocks suitable as snippets for a template layout', 'adjust', 'Core');
INSERT INTO core_classes_models_blockcategory (id, name, "parentId", description, icon, "extensionName") VALUES (6, 'Pages', 0, 'Blocks suitable within a page layout', 'adjust', 'Core');
INSERT INTO core_classes_models_blockcategory (id, name, "parentId", description, icon, "extensionName") VALUES (7, 'Header', 6, 'Blocks suitable for the header of a page layout', 'adjust', 'Core');
INSERT INTO core_classes_models_blockcategory (id, name, "parentId", description, icon, "extensionName") VALUES (8, 'Footer', 6, 'Blocks suitable for the footer of a page layout', 'adjust', 'Core');
INSERT INTO core_classes_models_blockcategory (id, name, "parentId", description, icon, "extensionName") VALUES (9, 'Main Body', 6, 'Blocks suitable for the main body of a page layout', 'adjust', 'Core');
INSERT INTO core_classes_models_blockcategory (id, name, "parentId", description, icon, "extensionName") VALUES (10, 'Snippets', 6, 'Blocks suitable as snippets within a page layout', 'adjust', 'Core');


--
-- Name: core_classes_models_blockcategory_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_blockcategory_seq', 10, true);


--
-- Data for Name: core_classes_models_blockprocessor; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (37, 'includer', 'Includer', '\Core\Components\BlockProcessors\Includer', 'Allows for the ability to include includers and/or prepend/append content to the head/body within a block', 'Core', false, false, '{}', 'Includer', 1);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (27, 'asset-manager', 'AssetManager', '\Core\Components\BlockProcessors\AssetManager', 'This block processor is used to display the asset manager', 'Core', true, false, '{2,1}', 'Asset Manager', 2);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (12, 'basic-footer', 'BasicFooter', '\Core\Components\BlockProcessors\BasicFooter', 'Displays a basic footer, based on the application''s company name', 'Core', true, true, '{}', 'Basic Footer', 5);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (13, 'basic-header', 'BasicHeader', '\Core\Components\BlockProcessors\BasicHeader', 'This displays a basic header, including the site name and description', 'Core', false, false, '{}', 'Basic Header', 5);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (14, 'basic-page-header', 'BasicPageHeader', '\Core\Components\BlockProcessors\BasicPageHeader', 'Displays a basic page header, including the page''s name and description', 'Core', true, true, '{}', 'Basic Page Header', 5);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (16, 'breadcrumbs', 'Breadcrumbs', '\Core\Components\BlockProcessors\Breadcrumbs', 'Displays breadcrumbs based on the current environment', 'Core', true, true, '{}', 'Breadcrumbs', 1);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (20, 'markdown', 'Markdown', '\Core\Components\BlockProcessors\Markdown', 'Using markdown syntax, create content using a basic text box', 'Core', false, false, '{}', 'Markdown', 4);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (19, 'plain-text', 'PlainText', '\Core\Components\BlockProcessors\PlainText', 'Create content using a basic text-box. You can use html and markdown syntax with this processor.', 'Core', false, false, '{}', 'Plain Text', 4);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (31, 'carousel', 'Carousel', '\Core\Components\BlockProcessors\Carousel', 'Display a carousel with basic or custom content', 'Core', true, true, '{}', 'Carousel', 6);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (4, 'collapse', 'Collapse', '\Core\Components\BlockProcessors\Collapse', 'Display a bootstrap collapse', 'Core', true, true, '{}', 'Collapse', 6);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (33, 'custom-nav-menu', 'CustomNavMenu', '\Core\Components\BlockProcessors\CustomNavMenu', 'Displays custom nav-menu items, either as links to a page, or to custom links', 'Core', true, true, '{}', 'Custom Nav Menu', 3);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (32, 'default-nav-menu', 'DefaultNavMenu', '\Core\Components\BlockProcessors\DefaultNavMenu', 'Displays nav menu items based on installed exensions', 'Core', true, true, '{}', 'Default Nav Menu', 3);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (17, 'html', 'HTML', '\Core\Components\BlockProcessors\HTML', 'Create html content using the html editor', 'Core', false, false, '{}', 'HTML', 4);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (22, 'horizontal-rule', 'HorizontalRule', '\Core\Components\BlockProcessors\HorizontalRule', 'Creates a horizontal line. Useful for creating defined spaces between rows.', 'Core', true, true, '{}', 'Horizontal Rule', 5);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (21, 'horizontal-space', 'HorizontalSpace', '\Core\Components\BlockProcessors\HorizontalSpace', 'This creates a horizontal space using an empty paragraph. This is useful for separating rows.', 'Core', true, true, '{}', 'Horizontal Space', 5);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (28, 'image', 'Image', '\Core\Components\BlockProcessors\Image', 'A basic processor for displaying an image', 'Core', true, true, '{}', 'Image', 5);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (6, 'tabs', 'Tabs', '\Core\Components\BlockProcessors\Tabs', 'Display a set of bootstrap tabs', 'Core', true, true, '{}', 'Tabs', 6);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (24, 'user-menu', 'UserMenu', '\Core\Components\BlockProcessors\UserMenu', 'Displays a user menu as a login form if no user is logged in, or a link to their account and to log out.', 'Core', true, true, '{}', 'User Menu', 3);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (1, 'extorio-admin-menu-left', 'ExtorioAdminMenuLeft', '\Core\Components\BlockProcessors\ExtorioAdminMenuLeft', 'Display the extorio admin left menu', 'Core', false, false, '{}', 'Extorio Admin Menu Left', 3);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (41, 'extorio-admin-menu-top', 'ExtorioAdminMenuTop', '\Core\Components\BlockProcessors\ExtorioAdminMenuTop', 'Displays the top menu', 'Core', false, false, '{}', 'Extorio Admin Menu Top', 3);
INSERT INTO core_classes_models_blockprocessor (id, label, class, namespace, description, "extensionName", "inlineEnabled", "allInline", "inlineGroups", name, category) VALUES (42, 'language-selector', 'LanguageSelector', '\Core\Components\BlockProcessors\LanguageSelector', 'Displays a list of enabled languages to change how the website is translated', 'Core', true, true, '{}', 'Language Selector', 3);


--
-- Name: core_classes_models_blockprocessor_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_blockprocessor_seq', 42, true);


--
-- Data for Name: core_classes_models_blockprocessorcategory; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_blockprocessorcategory (id, name, description, icon, "extensionName") VALUES (1, 'General Tools', 'Block processors that function as a general utility to the user', 'adjust', 'Core');
INSERT INTO core_classes_models_blockprocessorcategory (id, name, description, icon, "extensionName") VALUES (2, 'Admin Tools', 'Block processors that function as a utility for admins', 'adjust', 'Core');
INSERT INTO core_classes_models_blockprocessorcategory (id, name, description, icon, "extensionName") VALUES (3, 'Menus', 'Block processors that are used to display menus and navigation', 'adjust', 'Core');
INSERT INTO core_classes_models_blockprocessorcategory (id, name, description, icon, "extensionName") VALUES (4, 'Editors', 'Block processors that are used to create editable content', 'adjust', 'Core');
INSERT INTO core_classes_models_blockprocessorcategory (id, name, description, icon, "extensionName") VALUES (5, 'General Content', 'Block processors that are used to serve general content', 'adjust', 'Core');
INSERT INTO core_classes_models_blockprocessorcategory (id, name, description, icon, "extensionName") VALUES (6, 'Interactive Content', 'Block processors that are used to server interactive content', 'adjust', 'Core');


--
-- Name: core_classes_models_blockprocessorcategory_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_blockprocessorcategory_seq', 6, true);


--
-- Data for Name: core_classes_models_emailtemplate; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: core_classes_models_emailtemplate_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_emailtemplate_seq', 6, true);


--
-- Data for Name: core_classes_models_emailtransport; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: core_classes_models_emailtransport_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_emailtransport_seq', 36, true);


--
-- Data for Name: core_classes_models_enum; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_enum (id, class, label, namespace, description, "extensionName", "isHidden", "values", name) VALUES (13, 'FontAwesomeIcons', 'font-awesome-icons', '\Core\Classes\Enums\FontAwesomeIcons', 'Icons for font awesome', 'Core', true, '["adjust","adn","align-center","align-justify","align-left","align-right","ambulance","anchor","android","angle-double-down","angle-double-left","angle-double-right","angle-double-up","angle-down","angle-left","angle-right","angle-up","apple","archive","arrow-circle-down","arrow-circle-left","arrow-circle-o-down","arrow-circle-o-left","arrow-circle-o-right","arrow-circle-o-up","arrow-circle-right","arrow-circle-up","arrow-down","arrow-left","arrow-right","arrows","arrows-alt","arrows-h","arrows-v","arrow-up","asterisk","automobile","backward","ban","bank","bar-chart-o","barcode","bars","bed","beer","behance","behance-square","bell","bell-o","bitbucket","bitbucket-square","bitcoin","bold","bolt","bomb","book","bookmark","bookmark-o","briefcase","btc","bug","building","building-o","bullhorn","bullseye","buysellads","cab","calendar","calendar-o","camera","camera-retro","car","caret-down","caret-left","caret-right","caret-square-o-down","caret-square-o-left","caret-square-o-right","caret-square-o-up","caret-up","cart-arrow-down","cart-plus","certificate","chain","chain-broken","check","check-circle","check-circle-o","check-square","check-square-o","chevron-circle-down","chevron-circle-left","chevron-circle-right","chevron-circle-up","chevron-down","chevron-left","chevron-right","chevron-up","child","circle","circle-o","circle-o-notch","circle-thin","clipboard","clock-o","cloud","cloud-download","cloud-upload","cny","code","code-fork","codepen","coffee","cog","cogs","columns","comment","comment-o","comments","comments-o","compass","compress","connectdevelop","copy","credit-card","crop","crosshairs","css3","cube","cubes","cut","cutlery","dashboard","dashcube","database","dedent","delicious","desktop","deviantart","diamond","digg","dollar","dot-circle-o","download","dribbble","dropbox","drupal","edit","eject","ellipsis-h","ellipsis-v","empire","envelope","envelope-o","envelope-square","eraser","eur","euro","exchange","exclamation","exclamation-circle","exclamation-triangle","expand","external-link","external-link-square","eye","eye-slash","facebook","facebook-official","facebook-square","fast-backward","fast-forward","fax","female","fighter-jet","file","file-archive-o","file-audio-o","file-code-o","file-excel-o","file-image-o","file-movie-o","file-o","file-pdf-o","file-photo-o","file-picture-o","file-powerpoint-o","files-o","file-sound-o","file-text","file-text-o","file-video-o","file-word-o","file-zip-o","film","filter","fire","fire-extinguisher","flag","flag-checkered","flag-o","flash","flask","flickr","floppy-o","folder","folder-o","folder-open","folder-open-o","font","forumbee","forward","foursquare","frown-o","gamepad","gavel","gbp","ge","gear","gears","gift","git","github","github-alt","github-square","git-square","gittip","glass","globe","google","google-plus","google-plus-square","graduation-cap","group","hacker-news","hand-o-down","hand-o-left","hand-o-right","hand-o-up","hdd-o","header","headphones","heart","heartbeat","heart-o","history","home","hospital-o","h-square","html5","image","inbox","indent","info","info-circle","inr","instagram","institution","italic","joomla","jpy","jsfiddle","key","keyboard-o","krw","language","laptop","leaf","leanpub","legal","lemon-o","level-down","level-up","life-bouy","life-ring","life-saver","lightbulb-o","link","linkedin","linkedin-square","linux","list","list-alt","list-ol","list-ul","location-arrow","lock","long-arrow-down","long-arrow-left","long-arrow-right","long-arrow-up","magic","magnet","mail-forward","mail-reply","mail-reply-all","male","map-marker","mars","mars-double","mars-stroke","mars-stroke-h","mars-stroke-v","maxcdn","medium","medkit","meh-o","mercury","microphone","microphone-slash","minus","minus-circle","minus-square","minus-square-o","mobile","mobile-phone","money","moon-o","mortar-board","motorcycle","music","navicon","neuter","openid","outdent","pagelines","paperclip","paper-plane","paper-plane-o","paragraph","paste","pause","paw","pencil","pencil-square","pencil-square-o","phone","phone-square","photo","picture-o","pied-piper","pied-piper-alt","pied-piper-square","pinterest","pinterest-p","pinterest-square","plane","play","play-circle","play-circle-o","plus","plus-circle","plus-square","plus-square-o","power-off","print","puzzle-piece","qq","qrcode","question","question-circle","quote-left","quote-right","ra","random","rebel","recycle","reddit","reddit-square","refresh","renren","reorder","repeat","reply","reply-all","retweet","rmb","road","rocket","rotate-left","rotate-right","rouble","rss","rss-square","rub","ruble","rupee","save","scissors","search","search-minus","search-plus","sellsy","send","send-o","server","share","share-alt","share-alt-square","share-square","share-square-o","shield","ship","shirtsinbulk","shopping-cart","signal","sign-in","sign-out","simplybuilt","sitemap","skyatlas","skype","slack","sliders","smile-o","sort","sort-alpha-asc","sort-alpha-desc","sort-amount-asc","sort-amount-desc","sort-asc","sort-desc","sort-down","sort-numeric-asc","sort-numeric-desc","sort-up","soundcloud","space-shuttle","spinner","spoon","spotify","square","square-o","stack-exchange","stack-overflow","star","star-half","star-half-empty","star-half-full","star-half-o","star-o","steam","steam-square","step-backward","step-forward","stethoscope","stop","street-view","strikethrough","stumbleupon","stumbleupon-circle","subscript","subway","suitcase","sun-o","superscript","support","table","tablet","tachometer","tag","tags","tasks","taxi","tencent-weibo","terminal","text-height","text-width","th","th-large","th-list","thumbs-down","thumbs-o-down","thumbs-o-up","thumbs-up","thumb-tack","ticket","times","times-circle","times-circle-o","tint","toggle-down","toggle-left","toggle-right","toggle-up","train","transgender","transgender-alt","trash-o","tree","trello","trophy","truck","try","tumblr","tumblr-square","turkish-lira","twitter","twitter-square","umbrella","underline","undo","university","unlink","unlock","unlock-alt","unsorted","upload","usd","user","user-md","user-plus","users","user-secret","user-times","venus","venus-double","venus-mars","viacoin","video-camera","vimeo-square","vine","vk","volume-down","volume-off","volume-up","warning","wechat","weibo","weixin","whatsapp","wheelchair","windows","won","wordpress","wrench","xing","xing-square","yahoo","yen","youtube","youtube-play","youtube-square"]', 'Font Awesome Icons');
INSERT INTO core_classes_models_enum (id, class, label, namespace, description, "extensionName", "isHidden", "values", name) VALUES (7, 'InternalExtensions', 'internal-extensions', '\Core\Classes\Enums\InternalExtensions', 'A list of internal extensions', 'Core', true, '["Core","Application"]', 'Internal Extensions');
INSERT INTO core_classes_models_enum (id, class, label, namespace, description, "extensionName", "isHidden", "values", name) VALUES (15, 'InternalUserGroups', 'internal-user-groups', '\Core\Classes\Enums\InternalUserGroups', '', 'Core', true, '["Super Administrator","Administrator","User"]', 'Internal User Groups');
INSERT INTO core_classes_models_enum (id, class, label, namespace, description, "extensionName", "isHidden", "values", name) VALUES (10, 'ModelWebhookActions', 'model-webhook-actions', '\Core\Classes\Enums\ModelWebhookActions', 'The actions for model webhooks', 'Core', true, '["create","update","delete"]', 'Model Webhook Actions');
INSERT INTO core_classes_models_enum (id, class, label, namespace, description, "extensionName", "isHidden", "values", name) VALUES (1, 'PHPTypes', 'php-types', '\Core\Classes\Enums\PHPTypes', 'PHP Types', 'Core', true, '["array","boolean","double","float","integer","object","NULL","resource","string","unknown type"]', 'PHP Types');
INSERT INTO core_classes_models_enum (id, class, label, namespace, description, "extensionName", "isHidden", "values", name) VALUES (3, 'PropertyBasicType', 'property-basic-type', '\Core\Classes\Enums\PropertyBasicType', 'The basic property types', 'Core', true, '["checkbox","email","password","textfield","textarea","number","decimal","date","time","datetime"]', 'Property Basic Type');
INSERT INTO core_classes_models_enum (id, class, label, namespace, description, "extensionName", "isHidden", "values", name) VALUES (4, 'PropertyType', 'property-type', '\Core\Classes\Enums\PropertyType', 'The property types', 'Core', true, '["basic","enum","complex","meta"]', 'Property Type');
INSERT INTO core_classes_models_enum (id, class, label, namespace, description, "extensionName", "isHidden", "values", name) VALUES (5, 'TaskStatus', 'task-status', '\Core\Classes\Enums\TaskStatus', 'The statuses of a task', 'Core', true, '["not started","queued","running","completed","failed","killed"]', 'Task Status');
INSERT INTO core_classes_models_enum (id, class, label, namespace, description, "extensionName", "isHidden", "values", name) VALUES (8, 'InternalPages', 'internal-pages', '\Core\Classes\Enums\InternalPages', 'A list of internal pages', 'Core', true, '["\/404-page-not-found\/","\/401-access-denied\/","\/my-account\/","\/user-authentication\/","\/user-forgot-password\/","\/user-login\/","\/users\/","\/user-register\/","\/user-logout\/","\/extorio-admin\/login\/","\/extorio-admin\/401-access-denied\/","\/extorio-admin\/users\/"]', 'Internal Pages');
INSERT INTO core_classes_models_enum (id, class, label, namespace, description, "extensionName", "isHidden", "values", name) VALUES (11, 'InternalEvents', 'internal-events', '\Core\Classes\Enums\InternalEvents', 'A list of events that are part of the core framework', 'Core', true, '["on_exception","user_login","user_logout","user_update","user_delete","user_create","extorio_admin_menu_left_menus","extorio_admin_menu_left_items","extorio_admin_menu_top_menus","extorio_admin_menu_top_items","extorio_on_start","extorio_on_end","default_nav_menu_items","user_list_columns","user_list_cells","my_account_controller","my_account_view_tabs","my_account_view_content","user_profiles_controller","user_profiles_view_tabs","user_profiles_view_content","theme_compile_less_files","theme_minify_css_files","theme_minify_js_files","user_actions_detail_display","user_actions_detail_filter","user_edit_controller","user_edit_view_tabs","user_edit_view_content","user_login_controller","user_login_view","user_register_controller","user_register_view","user_create_controller","user_create_view"]', 'Internal Events');
INSERT INTO core_classes_models_enum (id, class, label, namespace, description, "extensionName", "isHidden", "values", name) VALUES (18, 'Titles', 'titles', '\Core\Classes\Enums\Titles', 'A list of user titles', 'Core', true, '["Unknown","Mr","Mrs","Miss","Ms","Master","Mx"]', 'Titles');
INSERT INTO core_classes_models_enum (id, class, label, namespace, description, "extensionName", "isHidden", "values", name) VALUES (19, 'Genders', 'genders', '\Core\Classes\Enums\Genders', 'A list of user genders', 'Core', true, '["Unknown","Male","Female"]', 'Genders');
INSERT INTO core_classes_models_enum (id, class, label, namespace, description, "extensionName", "isHidden", "values", name) VALUES (20, 'UserContactType', 'user-contact-type', '\Core\Classes\Enums\UserContactType', '', 'Core', true, '["Unknown","System Email","Email","Phone","Direct"]', 'User Contact Type');
INSERT INTO core_classes_models_enum (id, class, label, namespace, description, "extensionName", "isHidden", "values", name) VALUES (16, 'Countries', 'countries', '\Core\Classes\Enums\Countries', 'A list of countries in the world', 'Core', true, '["Unknown","United Kingdom","United States","Afghanistan","Aland Islands","Albania","Algeria","American Samoa","Andorra","Angola","Anguilla","Antarctica","Antigua and Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia and Herzegovina","Botswana","Bouvet Island","Brazil","British Indian Ocean Territory","Brunei Darussalam","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Caribbean Netherlands","Cayman Islands","Central African Republic","Chad","Chile","China","Christmas Island","Cocos (Keeling) Islands","Colombia","Comoros","Congo","Congo, Democratic Republic of","Cook Islands","Costa Rica","Cote d''Ivoire","Croatia","Cuba","Cura\u00e7ao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Guiana","French Polynesia","French Southern Territories","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guernsey","Guinea","Guinea-Bissau","Guyana","Haiti","Heard and McDonald Islands","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kuwait","Kyrgyzstan","Lao People''s Democratic Republic","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Martinique","Mauritania","Mauritius","Mayotte","Mexico","Micronesia, Federated States of","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Niue","Norfolk Island","North Korea","Northern Mariana Islands","Norway","Oman","Pakistan","Palau","Palestine, State of","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Pitcairn","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russian Federation","Rwanda","Saint Barthelemy","Saint Helena","Saint Kitts and Nevis","Saint Lucia","Saint Vincent and the Grenadines","Saint-Martin","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Sint Maarten","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Georgia and the South Sandwich Islands","South Korea","South Sudan","Spain","Sri Lanka","St. Pierre and Miquelon","Sudan","Suriname","Svalbard and Jan Mayen Islands","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","The Netherlands","Timor-Leste","Togo","Tokelau","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Turks and Caicos Islands","Tuvalu","Uganda","Ukraine","United Arab Emirates","Uruguay","Uzbekistan","Vanuatu","Vatican","Venezuela","Vietnam","Virgin Islands","Wallis and Futuna Islands","Western Sahara","Yemen","Zambia","Zimbabwe"]', 'Countries');


--
-- Name: core_classes_models_enum_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_enum_seq', 20, true);


--
-- Data for Name: core_classes_models_extension; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_extension (id, name, "isInstalled", namespace, build, "isInternal") VALUES (9, 'Core', true, '\Core\Components\Extension\Extension', 0, true);
INSERT INTO core_classes_models_extension (id, name, "isInstalled", namespace, build, "isInternal") VALUES (10, 'Application', true, '\Application\Components\Extension\Extension', 0, true);


--
-- Name: core_classes_models_extension_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_extension_seq', 12, true);


--
-- Data for Name: core_classes_models_language; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (47, 'Ukrainian', true, 'uk', 'uk', '/Core/Assets/images/flags/Ukraine.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (48, 'Urdu', true, 'ur', 'ur', '/Core/Assets/images/flags/Pakistan.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (1, 'English', true, 'en', 'en', '/Core/Assets/images/flags/United Kingdom(Great Britain).png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (2, 'Arabic', true, 'ar', 'ar', '/Core/Assets/images/flags/Arab League.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (3, 'Bosnian', true, 'bs', 'bs-Latn', '/Core/Assets/images/flags/Bosnia & Herzegovina.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (4, 'Bulgarian', true, 'bg', 'bg', '/Core/Assets/images/flags/Bulgaria.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (5, 'Catalan', true, 'ca', 'ca', '/Core/Assets/images/flags/Spain.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (6, 'Chinese Simplified', true, 'zh_Hans', 'zh-CHS', '/Core/Assets/images/flags/China.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (7, 'Chinese Traditional', true, 'zh_Hant', 'zh-CHT', '/Core/Assets/images/flags/China.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (8, 'Croatian', true, 'hr', 'hr', '/Core/Assets/images/flags/Croatia.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (9, 'Czech', true, 'cs', 'cs', '/Core/Assets/images/flags/Czech%20Republic.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (10, 'Danish', true, 'da', 'da', '/Core/Assets/images/flags/Denmark.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (11, 'Dutch', true, 'nl', 'nl', '/Core/Assets/images/flags/Netherlands.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (12, 'Estonian', true, 'et', 'et', '/Core/Assets/images/flags/Estonia.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (13, 'Finnish', true, 'fi', 'fi', '/Core/Assets/images/flags/Finland.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (14, 'French', true, 'fr', 'fr', '/Core/Assets/images/flags/France.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (15, 'German', true, 'de', 'de', '/Core/Assets/images/flags/Germany.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (16, 'Greek', true, 'el', 'el', '/Core/Assets/images/flags/Greece.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (17, 'Haitian Creole', true, 'ht', 'ht', '/Core/Assets/images/flags/Haiti.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (18, 'Hebrew', true, 'he', 'he', '/Core/Assets/images/flags/Israel.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (19, 'Hindi', true, 'hi', 'hi', '/Core/Assets/images/flags/India.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (20, 'Hmong Daw', true, 'mww', 'mww', '/Core/Assets/images/flags/China.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (21, 'Hungarian', true, 'hu', 'hu', '/Core/Assets/images/flags/Hungary.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (49, 'Vietnamese', true, 'vi', 'vi', '/Core/Assets/images/flags/Viet%20Nam.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (22, 'Indonesian', true, 'id', 'id', '/Core/Assets/images/flags/Indonezia.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (23, 'Italian', true, 'it', 'it', '/Core/Assets/images/flags/Italy.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (24, 'Japanese', true, 'ja', 'ja', '/Core/Assets/images/flags/Japan.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (25, 'Kiswahili', true, 'sw', 'sw', '/Core/Assets/images/flags/African%20Union.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (26, 'Klingon', true, 'tlh', 'tlh', '/Core/Assets/images/flags/Klingon.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (27, 'Korean', true, 'ko', 'ko', '/Core/Assets/images/flags/South%20Korea.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (28, 'Latvian', true, 'lv', 'lv', '/Core/Assets/images/flags/Latvia.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (29, 'Lithuanian', true, 'lt', 'lt', '/Core/Assets/images/flags/Lithuania.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (30, 'Malay', true, 'ms', 'ms', '/Core/Assets/images/flags/Malaysia.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (31, 'Maltese', true, 'mt', 'mt', '/Core/Assets/images/flags/Malta.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (32, 'Norwegian', true, 'nn', 'no', '/Core/Assets/images/flags/Norway.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (33, 'Persian', true, 'fa', 'fa', '/Core/Assets/images/flags/Iran.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (34, 'Polish', true, 'pl', 'pl', '/Core/Assets/images/flags/Poland.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (35, 'Portuguese', true, 'pt', 'pt', '/Core/Assets/images/flags/Portugal.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (36, 'Queretaro Otomi', true, 'otq', 'otq', '/Core/Assets/images/flags/Mexico.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (37, 'Romanian', true, 'ro', 'ro', '/Core/Assets/images/flags/Romania.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (38, 'Russian', true, 'ru', 'ru', '/Core/Assets/images/flags/Russian%20Federation.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (39, 'Serbian (Cyrillic)', true, 'sr_Cyrl', 'sr-Cyrl', '/Core/Assets/images/flags/Serbia(Yugoslavia).png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (40, 'Serbian (Latin)', true, 'sr_Latn', 'sr-Latn', '/Core/Assets/images/flags/Serbia(Yugoslavia).png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (41, 'Slovak', true, 'sk', 'sk', '/Core/Assets/images/flags/Slovakia.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (42, 'Slovenian', true, 'sl', 'sl', '/Core/Assets/images/flags/Slovenia.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (43, 'Spanish', true, 'es', 'es', '/Core/Assets/images/flags/Spain.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (44, 'Swedish', true, 'sv', 'sv', '/Core/Assets/images/flags/Sweden.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (45, 'Thai', true, 'th', 'th', '/Core/Assets/images/flags/Thailand.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (46, 'Turkish', true, 'tr', 'tr', '/Core/Assets/images/flags/Turkey.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (50, 'Welsh', true, 'cy', 'cy', '/Core/Assets/images/flags/Wales.png');
INSERT INTO core_classes_models_language (id, name, enabled, "sLocale", "mLocale", image) VALUES (51, 'Yucatec Maya', true, 'yua', 'yua', '/Core/Assets/images/flags/Belize.png');


--
-- Name: core_classes_models_language_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_language_seq', 51, true);


--
-- Data for Name: core_classes_models_layout; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_layout (id, name, description, "extensionName", layout) VALUES (2, 'Basic layout with side bar', 'A basic layout with a side bar', 'Core', '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":3,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":""},{"x":3,"width":9,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":""}]}]}]}]}');
INSERT INTO core_classes_models_layout (id, name, description, "extensionName", layout) VALUES (3, 'Complex layout', 'A complex layout with header, footer, outer side bar and inner side bar', 'Core', '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":3,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":""},{"x":3,"width":9,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true},{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":3,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":""},{"x":3,"width":9,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":""}]}]}]},{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true}]}]}]}');
INSERT INTO core_classes_models_layout (id, name, description, "extensionName", layout) VALUES (1, 'Default layout', 'The default layout', 'Core', '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":""}]}]}]}]}');


--
-- Name: core_classes_models_layout_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_layout_seq', 6, true);


--
-- Data for Name: core_classes_models_model; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('block', 'Block', 72, '\Core\Classes\Models\Block', 'B_Block', '\Core\Classes\Models\B_Block', 'A block represents a modular, reusable piece of content that can be inserted into a layout. A block uses a block processor to generate its content', '{146,147,148,149,150,151,153,358,359,360,361,362}', 'Core', true, 'Block');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('user', 'User', 84, '\Core\Classes\Models\User', 'B_User', '\Core\Classes\Models\B_User', 'A user of the website and/or admin', '{235,236,319,455,320,401,402,237,380,239,240,241,424,321,348,410,294,295,438,439,440,432,433,434,435,436,437,454}', 'Core', true, 'User');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('email-transport', 'EmailTransport', 99, '\Core\Classes\Models\EmailTransport', 'B_EmailTransport', '\Core\Classes\Models\B_EmailTransport', 'This model represents an email transport that can be reviewed and re-sent', '{296,297,316,312,299,300,301,302,303,304,305,306,307,308}', 'Core', true, 'Email Transport');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('property', 'Property', 66, '\Core\Classes\Models\Property', 'B_Property', '\Core\Classes\Models\B_Property', 'A property of a model', '{106,107,108,109,110,111,112,113,114}', 'Core', true, 'Property');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('asset', 'Asset', 102, '\Core\Classes\Models\Asset', 'B_Asset', '\Core\Classes\Models\B_Asset', 'An asset owned by an extension', '{329,324,325,326,327,330,331,322}', 'Core', true, 'Asset');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('template', 'Template', 81, '\Core\Classes\Models\Template', 'B_Template', '\Core\Classes\Models\B_Template', 'A template resembles the outer content of pages. Many pages can use the same template,effectively making a template site-wide.', '{209,210,211,212,213,214,215,216,219,217,379}', 'Core', true, 'Template');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('user-group', 'UserGroup', 107, '\Core\Classes\Models\UserGroup', 'B_UserGroup', '\Core\Classes\Models\B_UserGroup', '', '{341,342,343,397,398,344}', 'Core', true, 'User Group');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('email-template', 'EmailTemplate', 100, '\Core\Classes\Models\EmailTemplate', 'B_EmailTemplate', '\Core\Classes\Models\B_EmailTemplate', 'An email template that can optionally be used as the content of an email transport and can use replacers for dynamic content', '{309,310,318,311,314}', 'Core', true, 'Email Template');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('user-privilege', 'UserPrivilege', 106, '\Core\Classes\Models\UserPrivilege', 'B_UserPrivilege', '\Core\Classes\Models\B_UserPrivilege', '', '{337,338,339,340}', 'Core', true, 'User Privilege');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('user-privilege-category', 'UserPrivilegeCategory', 105, '\Core\Classes\Models\UserPrivilegeCategory', 'B_UserPrivilegeCategory', '\Core\Classes\Models\B_UserPrivilegeCategory', '', '{333,334,335,336}', 'Core', true, 'User Privilege Category');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('user-contact-topic', 'UserContactTopic', 118, '\Core\Classes\Models\UserContactTopic', 'B_UserContactTopic', '\Core\Classes\Models\B_UserContactTopic', 'A topic for a user contact', '{444,445,446}', 'Core', true, 'User Contact Topic');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('user-session', 'UserSession', 86, '\Core\Classes\Models\UserSession', 'B_UserSession', '\Core\Classes\Models\B_UserSession', 'The user''s session', '{345,248,249,250}', 'Core', true, 'User Session');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('model-webhook', 'ModelWebhook', 97, '\Core\Classes\Models\ModelWebhook', 'B_ModelWebhook', '\Core\Classes\Models\B_ModelWebhook', 'A model webhook is used to make external notifications that a model instance has been created, updated or deleted.', '{277,278,279,281,282,283}', 'Core', true, 'Model Webhook');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('api', 'Api', 69, '\Core\Classes\Models\Api', 'B_Api', '\Core\Classes\Models\B_Api', 'An api component', '{364,130,131,132,133,134,349,350}', 'Core', true, 'Api');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('user-note', 'UserNote', 112, '\Core\Classes\Models\UserNote', 'B_UserNote', '\Core\Classes\Models\B_UserNote', '', '{393,394,395,396}', 'Core', true, 'User Note');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('user-action', 'UserAction', 110, '\Core\Classes\Models\UserAction', 'B_UserAction', '\Core\Classes\Models\B_UserAction', '', '{381,387,388,389,390,391,392}', 'Core', true, 'User Action');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('page', 'Page', 78, '\Core\Classes\Models\Page', 'B_Page', '\Core\Classes\Models\B_Page', 'A page in an MVC framework, with layout editing capabilities. A page is accessed via a unique address', '{178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,195,196,199,375,378,376,377}', 'Core', true, 'Page');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('user-report', 'UserReport', 117, '\Core\Classes\Models\UserReport', 'B_UserReport', '\Core\Classes\Models\B_UserReport', 'A user report', '{426,441,442,443}', 'Core', true, 'User Report');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('user-contact', 'UserContact', 119, '\Core\Classes\Models\UserContact', 'B_UserContact', '\Core\Classes\Models\B_UserContact', 'A contact made with a user', '{447,448,449,450,451,452,453}', 'Core', true, 'User Contact');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('layout', 'Layout', 77, '\Core\Classes\Models\Layout', 'B_Layout', '\Core\Classes\Models\B_Layout', 'Building complex layouts can sometimes be quite time consuming. For this reason, this model allows you to create pre-defined layouts that can be loaded in the edit view', '{174,175,176,177}', 'Core', true, 'Layout');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('model', 'Model', 68, '\Core\Classes\Models\Model', 'B_Model', '\Core\Classes\Models\B_Model', 'A model is a class that has the ability to store itself and communicate with the database. In a way, a model is schematic, describing how a stored instance of itself would look.', '{366,117,118,119,120,121,122,123,124,125}', 'Core', true, 'Model');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('block-category', 'BlockCategory', 108, '\Core\Classes\Models\BlockCategory', 'B_BlockCategory', '\Core\Classes\Models\B_BlockCategory', '', '{353,354,355,356,357}', 'Core', true, 'Block Category');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('task', 'Task', 80, '\Core\Classes\Models\Task', 'B_Task', '\Core\Classes\Models\B_Task', 'Resembles a task module', '{363,203,204,205,206,207,346,347}', 'Core', true, 'Task');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('user-action-type', 'UserActionType', 111, '\Core\Classes\Models\UserActionType', 'B_UserActionType', '\Core\Classes\Models\B_UserActionType', '', '{382,383,384,385,386,425}', 'Core', true, 'User Action Type');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('block-processor-category', 'BlockProcessorCategory', 109, '\Core\Classes\Models\BlockProcessorCategory', 'B_BlockProcessorCategory', '\Core\Classes\Models\B_BlockProcessorCategory', '', '{367,368,369,370}', 'Core', true, 'Block Processor Category');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('enum', 'Enum', 75, '\Core\Classes\Models\Enum', 'B_Enum', '\Core\Classes\Models\B_Enum', 'An enum is mainly used as a model property, and is similar to a SET, resembling a set of strict values that a property can be. It also closely resembles an enum in OOP', '{373,166,165,167,168,169,170,171}', 'Core', true, 'Enum');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('block-processor', 'BlockProcessor', 73, '\Core\Classes\Models\BlockProcessor', 'B_BlockProcessor', '\Core\Classes\Models\B_BlockProcessor', 'A block processor is used by a block to output content', '{365,156,157,158,159,160,292,351,352,371}', 'Core', true, 'Block Processor');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('extension', 'Extension', 76, '\Core\Classes\Models\Extension', 'B_Extension', '\Core\Classes\Models\B_Extension', 'This model is used to record whether an extension is enabled/disabled', '{172,456,457,275,459}', 'Core', true, 'Extension');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('theme', 'Theme', 82, '\Core\Classes\Models\Theme', 'B_Theme', '\Core\Classes\Models\B_Theme', 'A theme resembles the styling applied to a page. It is ultimately a styling framework for front end developers', '{220,374,221,222,223,224}', 'Core', true, 'Theme');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('translationbase', 'TranslationBase', 115, '\Core\Classes\Models\TranslationBase', 'B_TranslationBase', '\Core\Classes\Models\B_TranslationBase', 'A base translation', '{411,412,413,414,416}', 'Core', true, 'TranslationBase');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('translation', 'Translation', 116, '\Core\Classes\Models\Translation', 'B_Translation', '\Core\Classes\Models\B_Translation', 'A translation', '{418,419,420,421}', 'Core', true, 'Translation');
INSERT INTO core_classes_models_model (label, class, id, namespace, "baseClass", "baseNamespace", description, properties, "extensionName", "isHidden", name) VALUES ('language', 'Language', 114, '\Core\Classes\Models\Language', 'B_Language', '\Core\Classes\Models\B_Language', 'A language entity', '{404,408,409,406,423}', 'Core', true, 'Language');


--
-- Name: core_classes_models_model_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_model_seq', 119, true);


--
-- Data for Name: core_classes_models_modelwebhook; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: core_classes_models_modelwebhook_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_modelwebhook_seq', 5, true);


--
-- Data for Name: core_classes_models_page; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (28, 'Extorio Apis', 'The entry point for api components', '/extorio/apis/', false, 'ExtorioApis', '\Core\Components\Controllers\ExtorioApis', '\Core\Components\Views\ExtorioApis', 'Core', '', '', '', '', true, true, NULL, NULL, NULL, NULL, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","rows":[]}]}],"cells":[]}', true, '{}', '{}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (50, 'Extorio Blocks', 'The entry point for block components', '/extorio/blocks/', false, 'ExtorioBlocks', '\Core\Components\Controllers\ExtorioBlocks', '\Core\Components\Views\ExtorioBlocks', 'Core', '', '', '', '', true, true, NULL, NULL, NULL, NULL, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","rows":[]}]}],"cells":[]}', true, '{}', '{}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (51, 'Extorio Install', 'The extorio installation page', '/extorio/install/', false, 'ExtorioInstall', '\Core\Components\Controllers\ExtorioInstall', '\Core\Components\Views\ExtorioInstall', 'Core', '', '', '', '', true, true, NULL, NULL, NULL, NULL, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","rows":[]}]}],"cells":[]}', false, '{}', '{}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (52, 'Extorio Layouts', 'The entry point for layouts', '/extorio/layouts/', false, 'ExtorioLayouts', '\Core\Components\Controllers\ExtorioLayouts', '\Core\Components\Views\ExtorioLayouts', 'Core', '', '', '', '', true, true, NULL, NULL, NULL, NULL, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","rows":[]}]}],"cells":[]}', true, '{}', '{}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (29, 'Admin Apis', 'Manage the api components', '/extorio-admin/apis/', false, 'ExtorioAdminApis', '\Core\Components\Controllers\ExtorioAdminApis', '\Core\Components\Views\ExtorioAdminApis', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (31, 'Admin Assets', 'Manage the assets', '/extorio-admin/assets/', false, 'ExtorioAdminAssets', '\Core\Components\Controllers\ExtorioAdminAssets', '\Core\Components\Views\ExtorioAdminAssets', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 31);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (34, 'Admin Blocks', 'Manage the blocks', '/extorio-admin/blocks/', false, 'ExtorioAdminBlocks', '\Core\Components\Controllers\ExtorioAdminBlocks', '\Core\Components\Views\ExtorioAdminBlocks', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (37, 'Admin Enums', 'Manage the enums', '/extorio-admin/enums/', false, 'ExtorioAdminEnums', '\Core\Components\Controllers\ExtorioAdminEnums', '\Core\Components\Views\ExtorioAdminEnums', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (46, 'Admin Templates', 'Manage templates', '/extorio-admin/templates/', false, 'ExtorioAdminTemplates', '\Core\Components\Controllers\ExtorioAdminTemplates', '\Core\Components\Views\ExtorioAdminTemplates', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (47, 'Admin Themes', 'Manage themes', '/extorio-admin/themes/', false, 'ExtorioAdminThemes', '\Core\Components\Controllers\ExtorioAdminThemes', '\Core\Components\Views\ExtorioAdminThemes', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (53, 'Extorio Tasks', 'The entry point for tasks', '/extorio/tasks/', false, 'ExtorioTasks', '\Core\Components\Controllers\ExtorioTasks', '\Core\Components\Views\ExtorioTasks', 'Core', '', '', '', '', true, true, NULL, NULL, NULL, NULL, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","rows":[]}]}],"cells":[]}', true, '{}', '{}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (76, 'Admin Email Settings', 'Manage the settings related to emails', '/extorio-admin/email-settings/', false, 'AdminEmailSettings', '\Core\Components\Controllers\AdminEmailSettings', '\Core\Components\Views\AdminEmailSettings', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (73, 'Admin Email Templates', 'Manage the email templates', '/extorio-admin/email-templates/', false, 'AdminEmailTemplates', '\Core\Components\Controllers\AdminEmailTemplates', '\Core\Components\Views\AdminEmailTemplates', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (75, 'Admin Email Transports', 'Manage email transports', '/extorio-admin/email-transports/', false, 'AdminEmailTransports', '\Core\Components\Controllers\AdminEmailTransports', '\Core\Components\Views\AdminEmailTransports', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (38, 'Admin Extensions', 'Manage your extensions', '/extorio-admin/extensions/', false, 'ExtorioAdminExtensions', '\Core\Components\Controllers\ExtorioAdminExtensions', '\Core\Components\Views\ExtorioAdminExtensions', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (24, 'Admin Login', 'Admin login', '/extorio-admin/login/', false, 'ExtorioAdminLogin', '\Core\Components\Controllers\ExtorioAdminLogin', '\Core\Components\Views\ExtorioAdminLogin', 'Core', '', '', '', '', true, true, NULL, NULL, 3, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (41, 'Admin Logout', 'Admin Logout', '/extorio-admin/logout/', false, 'ExtorioAdminLogout', '\Core\Components\Controllers\ExtorioAdminLogout', '\Core\Components\Views\ExtorioAdminLogout', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (57, 'Admin Model Viewer', 'The model viewer', '/extorio-admin/model-viewer/', false, 'ExtorioAdminModelViewer', '\Core\Components\Controllers\ExtorioAdminModelViewer', '\Core\Components\Views\ExtorioAdminModelViewer', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (42, 'Admin Models', 'Manage the models', '/extorio-admin/models/', false, 'ExtorioAdminModels', '\Core\Components\Controllers\ExtorioAdminModels', '\Core\Components\Views\ExtorioAdminModels', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (26, 'Admin Pages', 'Manage the pages of your website', '/extorio-admin/pages/', false, 'ExtorioAdminPages', '\Core\Components\Controllers\ExtorioAdminPages', '\Core\Components\Views\ExtorioAdminPages', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (44, 'Admin Task Manager', 'Manage running tasks, and setup new tasks', '/extorio-admin/task-manager/', false, 'ExtorioAdminTaskManager', '\Core\Components\Controllers\ExtorioAdminTaskManager', '\Core\Components\Views\ExtorioAdminTaskManager', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (85, 'Extorio Block Processors', 'Entry point for processing block processors', '/extorio/block-processors/', false, 'ExtorioBlockProcessors', '\Core\Components\Controllers\ExtorioBlockProcessors', '\Core\Components\Views\ExtorioBlockProcessors', 'Core', '', '', '', '', true, true, NULL, NULL, NULL, NULL, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","rows":[]}]}],"cells":[]}', true, '{}', '{}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (23, 'Home', 'Welcome home :)', '/', true, '_Default', '\Application\Components\Controllers\_Default', '\Application\Components\Views\_Default', 'Application', '', '', '', '', true, false, NULL, NULL, NULL, NULL, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":false,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":47,"classes":"","styling":"","rows":[]}]}]}]}],"cells":[]}', true, '{}', '{2,1}', NULL);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (21, 'Access Denied', '', '/401-access-denied/', false, '_401AccessDenied', '\Application\Components\Controllers\_401AccessDenied', '\Application\Components\Views\_401AccessDenied', 'Application', '', '', '', '', true, false, NULL, NULL, NULL, NULL, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":false,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":14,"classes":"","styling":"","rows":[]}]}]}]}],"cells":[]}', true, '{}', '{2,1}', NULL);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (45, 'Admin Tasks', 'Manage task components', '/extorio-admin/tasks/', false, 'ExtorioAdminTasks', '\Core\Components\Controllers\ExtorioAdminTasks', '\Core\Components\Views\ExtorioAdminTasks', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (74, 'Admin User Settings', 'Manage the settings related to users', '/extorio-admin/user-settings/', false, 'AdminUserSettings', '\Core\Components\Controllers\AdminUserSettings', '\Core\Components\Views\AdminUserSettings', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (49, 'Admin Users', 'Manage the users and admins for your website', '/extorio-admin/users/', false, 'ExtorioAdminUsers', '\Core\Components\Controllers\ExtorioAdminUsers', '\Core\Components\Views\ExtorioAdminUsers', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (100, 'User Register', '', '/user-register/', false, 'UserRegister', '\Core\Components\Controllers\UserRegister', '\Core\Components\Views\UserRegister', 'Core', '', '', '', '', true, false, NULL, NULL, NULL, NULL, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","rows":[]}]}]}]}],"cells":[]}', true, '{}', '{2,1}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (103, 'User Forgot Password', '', '/user-forgot-password/', false, 'UserForgotPassword', '\Core\Components\Controllers\UserForgotPassword', '\Core\Components\Views\UserForgotPassword', 'Core', '', '', '', '', true, false, NULL, NULL, NULL, NULL, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","rows":[]}]}]}]}],"cells":[]}', true, '{}', '{2,1}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (105, 'My Account', '', '/my-account/', false, 'MyAccount', '\Core\Components\Controllers\MyAccount', '\Core\Components\Views\MyAccount', 'Core', '', '', '', '', true, false, NULL, NULL, NULL, NULL, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","configurable":false,"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}]}]}],"cells":[]}', true, '{}', '{2,1}', 99);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (22, 'Page Not Found', '', '/404-page-not-found/', false, '_404PageNotFound', '\Application\Components\Controllers\_404PageNotFound', '\Application\Components\Views\_404PageNotFound', 'Application', '', '', '', '', true, false, NULL, NULL, NULL, NULL, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":false,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":15,"classes":"","styling":"","rows":[]}]}]}]}],"cells":[]}', true, '{}', '{2,1}', 0);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (36, 'Admin Dashboard', 'Welcome to Extorio :)', '/extorio-admin/', false, 'ExtorioAdminDashboard', '\Core\Components\Controllers\ExtorioAdminDashboard', '\Core\Components\Views\ExtorioAdminDashboard', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (30, 'Admin Account', 'Manage your admin account', '/extorio-admin/account/', false, 'ExtorioAdminAccount', '\Core\Components\Controllers\ExtorioAdminAccount', '\Core\Components\Views\ExtorioAdminAccount', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (33, 'Admin Block Processors', 'Manage the block processor components', '/extorio-admin/block-processors/', false, 'ExtorioAdminBlockProcessors', '\Core\Components\Controllers\ExtorioAdminBlockProcessors', '\Core\Components\Views\ExtorioAdminBlockProcessors', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (39, 'Admin Layouts', 'Manage your layouts', '/extorio-admin/layouts/', false, 'ExtorioAdminLayouts', '\Core\Components\Controllers\ExtorioAdminLayouts', '\Core\Components\Views\ExtorioAdminLayouts', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","configurable":false,"rows":[{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (40, 'Admin Log Manager', 'Manage log files', '/extorio-admin/log-manager/', false, 'ExtorioAdminLogManager', '\Core\Components\Controllers\ExtorioAdminLogManager', '\Core\Components\Views\ExtorioAdminLogManager', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (62, 'Admin Model Webhooks', 'Manage the model webhooks', '/extorio-admin/model-webhooks/', false, 'ExtorioAdminModelWebhooks', '\Core\Components\Controllers\ExtorioAdminModelWebhooks', '\Core\Components\Views\ExtorioAdminModelWebhooks', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (102, 'User Authentication', '', '/user-authentication/', false, 'UserAuthentication', '\Core\Components\Controllers\UserAuthentication', '\Core\Components\Views\UserAuthentication', 'Core', '', '', '', '', true, false, NULL, NULL, NULL, NULL, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","rows":[]}]}]}]}],"cells":[]}', true, '{}', '{2,1}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (104, 'User Profiles', '', '/users/', false, 'UserProfiles', '\Core\Components\Controllers\UserProfiles', '\Core\Components\Views\UserProfiles', 'Core', '', '', '', '', true, false, NULL, NULL, NULL, NULL, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","rows":[]}]}]}]}],"cells":[]}', true, '{}', '{2,1}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (110, 'Admin User Privilege Categories', 'Manage the user privilege categories', '/extorio-admin/user-privilege-categories/', false, 'AdminUserPrivilegeCategories', '\Core\Components\Controllers\AdminUserPrivilegeCategories', '\Core\Components\Views\AdminUserPrivilegeCategories', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","configurable":false,"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (109, 'Admin User Privileges', 'Manage user privileges', '/extorio-admin/user-privileges/', false, 'AdminUserPrivileges', '\Core\Components\Controllers\AdminUserPrivileges', '\Core\Components\Views\AdminUserPrivileges', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","configurable":false,"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (108, 'Admin User Groups', 'Manage user groups', '/extorio-admin/user-groups/', false, 'AdminUserGroups', '\Core\Components\Controllers\AdminUserGroups', '\Core\Components\Views\AdminUserGroups', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","configurable":false,"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (112, 'Admin Extorio Settings', 'Manage the settings of extorio', '/extorio-admin/extorio-settings/', false, 'AdminExtorioSettings', '\Core\Components\Controllers\AdminExtorioSettings', '\Core\Components\Views\AdminExtorioSettings', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","configurable":false,"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}]}]}],"cells":[]}', true, '{}', '{1}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (113, 'Admin Application Settings', 'Manage the application settings', '/extorio-admin/application-settings/', false, 'AdminApplicationSettings', '\Core\Components\Controllers\AdminApplicationSettings', '\Core\Components\Views\AdminApplicationSettings', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","configurable":false,"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false,"rows":[]}]}]}]}],"cells":[]}', true, '{}', '{1}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (56, 'Admin Access Denied', 'Although you are an admin, you do not have access to this page... :(', '/extorio-admin/401-access-denied/', false, 'ExtorioAdmin401AccessDenied', '\Core\Components\Controllers\ExtorioAdmin401AccessDenied', '\Core\Components\Views\ExtorioAdmin401AccessDenied', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":14,"classes":"","styling":"","configurable":true,"rows":[]}]}],"cells":[]}', true, '{}', '{1}', 24);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (118, 'Admin Translations', 'Manage translations and translation bases', '/extorio-admin/translations/', false, 'AdminTranslations', '\Core\Components\Controllers\AdminTranslations', '\Core\Components\Views\AdminTranslations', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"role":"row","classes":"","styling":"","collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","spacing":true,"cells":[{"role":"cell","classes":"","styling":"","x":0,"width":12,"align":"inherit","hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"blockid":0,"rows":[{"role":"row","classes":"","styling":"","collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","spacing":true,"cells":[{"role":"content","classes":"","styling":"","x":0,"width":12,"align":"inherit","hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"blockid":0,"rows":[]}]}]}]}]}', true, '{}', '{1}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (119, 'Admin Block Categories', 'Manage block categories', '/extorio-admin/block-categories/', false, 'AdminBlockCategories', '\Core\Components\Controllers\AdminBlockCategories', '\Core\Components\Views\AdminBlockCategories', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"role":"row","classes":"","styling":"","collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","spacing":true,"cells":[{"role":"cell","classes":"","styling":"","x":0,"width":12,"align":"inherit","hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"blockid":0,"rows":[{"role":"row","classes":"","styling":"","collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","spacing":true,"cells":[{"role":"content","classes":"","styling":"","x":0,"width":12,"align":"inherit","hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"blockid":0,"rows":[]}]}]}]}]}', true, '{}', '{1}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (123, 'Admin User Reports', 'Manage user reports', '/extorio-admin/user-reports/', false, 'AdminUserReports', '\Core\Components\Controllers\AdminUserReports', '\Core\Components\Views\AdminUserReports', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"role":"row","classes":"","styling":"","collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","spacing":true,"cells":[{"role":"cell","classes":"","styling":"","x":0,"width":12,"align":"inherit","hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"blockid":0,"rows":[{"role":"row","classes":"","styling":"","collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","spacing":true,"cells":[{"role":"content","classes":"","styling":"","x":0,"width":12,"align":"inherit","hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"blockid":0,"rows":[]}]}]}]}]}', true, '{}', '{1}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (125, 'Admin User Action Types', 'Manage the user action types', '/extorio-admin/user-action-types/', false, 'AdminUserActionTypes', '\Core\Components\Controllers\AdminUserActionTypes', '\Core\Components\Views\AdminUserActionTypes', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"role":"row","classes":"","styling":"","collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","spacing":true,"cells":[{"role":"cell","classes":"","styling":"","x":0,"width":12,"align":"inherit","hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"blockid":0,"rows":[{"role":"row","classes":"","styling":"","collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","spacing":true,"cells":[{"role":"content","classes":"","styling":"","x":0,"width":12,"align":"inherit","hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"blockid":0,"rows":[]}]}]}]}]}', true, '{}', '{1}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (101, 'User Logout', '', '/user-logout/', false, 'UserLogout', '\Core\Components\Controllers\UserLogout', '\Core\Components\Views\UserLogout', 'Core', '', '', '', '', true, true, NULL, NULL, NULL, NULL, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","rows":[]}]}]}]}],"cells":[]}', true, '{}', '{2,1}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (124, 'Admin User Contact Topics', 'Manage user contact topics', '/extorio-admin/user-contact-topics/', false, 'AdminUserContactTopics', '\Core\Components\Controllers\AdminUserContactTopics', '\Core\Components\Views\AdminUserContactTopics', 'Core', '', '', '', '', true, true, NULL, NULL, 2, 3, '{"rows":[{"role":"row","classes":"","styling":"","collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","spacing":true,"cells":[{"role":"cell","classes":"","styling":"","x":0,"width":12,"align":"inherit","hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"blockid":0,"rows":[{"role":"row","classes":"","styling":"","collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","spacing":true,"cells":[{"role":"content","classes":"","styling":"","x":0,"width":12,"align":"inherit","hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"blockid":0,"rows":[]}]}]}]}]}', true, '{}', '{1}', 21);
INSERT INTO core_classes_models_page (id, name, description, address, "default", class, "controllerNamespace", "viewNamespace", "extensionName", "prependToHead", "appendToHead", "prependToBody", "appendToBody", "isPublic", "isHidden", "publishDate", "unpublishDate", template, theme, layout, "allRead", "readGroups", "modifyGroups", "readFailPageId") VALUES (99, 'User Login', '', '/user-login/', false, 'UserLogin', '\Core\Components\Controllers\UserLogin', '\Core\Components\Views\UserLogin', 'Core', '', '', '', '', true, false, NULL, NULL, NULL, NULL, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","rows":[]}]}]}]}],"cells":[]}', true, '{}', '{2,1}', 21);


--
-- Name: core_classes_models_page_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_page_seq', 125, true);


--
-- Data for Name: core_classes_models_property; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (108, 'description', 'description', 'The description', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (110, 'enumNamespace', 'enumNamespace', 'If the property is an enum, this is the enum''s namespace', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (113, 'basicType', 'basicType', 'If the property type is basic, this is the basic type', 'enum', 'checkbox', 'array', '', '\Core\Classes\Enums\PropertyBasicType', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (114, 'complexType', 'complexType', 'If the property is complex, this is the complex type', 'enum', 'checkbox', 'array', '', '\Core\Classes\Enums\PHPTypes', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (117, 'label', 'label', 'The label. This can be changed', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (120, 'baseClass', 'baseClass', 'The class of the base class.', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (121, 'baseNamespace', 'baseNamespace', 'The namespace of the base class', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (123, 'extensionName', 'extensionName', 'The extension name that this model belongs to', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (130, 'label', 'label', 'The label, used to access the api. This can be changed', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (132, 'namespace', 'namespace', 'The namespace of the api component', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (133, 'description', 'description', 'The description', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (146, 'name', 'name', 'The name of the block', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (147, 'description', 'description', 'A description of the block', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (149, 'isHidden', 'isHidden', 'Whether the block is hidden from block management and the pallet', 'basic', 'checkbox', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (151, 'extensionName', 'extensionName', 'The extension that owns the block', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (156, 'label', 'label', 'The label. This can be changed', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (159, 'description', 'description', 'The description', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (160, 'extensionName', 'extensionName', 'The extension that owns this block processor', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (166, 'label', 'label', 'The label. This can be changed', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (167, 'namespace', 'namespace', 'The namespace', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (169, 'extensionName', 'extensionName', 'The extension that owns this enum', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (170, 'isHidden', 'isHidden', 'Whether this enum is hidden from the enum management page', 'basic', 'checkbox', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (172, 'name', 'name', 'The name of the extension. This should be class-name safe', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (174, 'name', 'name', 'The name of the layout', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (175, 'description', 'description', 'The description', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (179, 'description', 'description', 'A description of the page', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (180, 'address', 'address', 'The page''s address. Used to access the page', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (184, 'viewNamespace', 'viewNamespace', 'The view namespace', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (186, 'prependToHead', 'prependToHead', 'Content to prepend to the head of the document', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (187, 'appendToHead', 'appendToHead', 'Content to append to the head of the document', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (189, 'appendToBody', 'appendToBody', 'Content to append to the body of the document', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (191, 'isHidden', 'isHidden', 'Whether the page is hidden from page management', 'basic', 'checkbox', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (193, 'unpublishDate', 'unpublishDate', 'The unpublish date of the page (optional). This allows you to set a date where the page will be unpublished', 'basic', 'date', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (196, 'theme', 'theme', 'The theme the page will use. If no theme is specified, the page will use the default theme', 'complex', 'checkbox', 'object', '\Core\Classes\Models\Theme', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (177, 'layout', 'layout', 'The actual layout', 'meta', 'checkbox', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (203, 'label', 'label', 'The label of the task, can be changed', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (206, 'description', 'description', 'A description of the task', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (207, 'extensionName', 'extensionName', 'The extension that owns this task', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (209, 'name', 'name', 'The name of the template', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (211, 'default', 'default', 'Whether this template is default', 'basic', 'checkbox', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (212, 'extensionName', 'extensionName', 'The extension that owns this template', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (214, 'prependToHead', 'prependToHead', 'Content to prepend to the head', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (215, 'appendToBody', 'appendToBody', 'Content to append to the body', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (199, 'layout', 'layout', 'The actual layout', 'meta', 'checkbox', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (106, 'name', 'name', 'The name of the property. This cannot be changed', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (107, 'label', 'label', 'The label of a property, this can be changed', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (249, 'remoteIP', 'remoteIP', 'The ip address of the session', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (109, 'childModelNamespace', 'childModelNamespace', 'If the property is complex, this is the child model''s namespace', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (336, 'extensionName', 'extension name', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (157, 'class', 'class', 'The class. This cannot be changed', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (158, 'namespace', 'namespace', 'The namespace', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (118, 'class', 'class', 'The class, normally auto-generated by the label. This cannot be changed.', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (165, 'class', 'class', 'The class, this cannot be changed', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (182, 'class', 'class', 'The class of the MVC components', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (111, 'isIndex', 'isIndex', 'Whether the property is indexed or not. If you will search for models using this propety a lot, you should consider indexing the property to speed up searches. This also allows you to search using this property in the model viewer', 'basic', 'checkbox', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (112, 'propertyType', 'propertyType', 'The property type', 'enum', 'checkbox', 'array', '', '\Core\Classes\Enums\PropertyType', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (119, 'namespace', 'namespace', 'The namespace, generated by the extensionName and class.', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (122, 'description', 'description', 'The description of the model', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (124, 'isHidden', 'isHidden', 'Whether this model is hidden from model management and model viewer', 'basic', 'checkbox', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (125, 'properties', 'properties', 'The model''s properties', 'complex', 'checkbox', 'array', '\Core\Classes\Models\Property', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (134, 'extensionName', 'extensionName', 'The extension name that this api belongs to', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (148, 'config', 'config', 'This is generated by the block configuration modal. It is a http query string of the params that get passed to the block processor to output the content', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (150, 'isPublic', 'isPublic', 'Whether the block is public. Blocks that are not public are visible in the block management page and the pallet, but will now be displayed on the website', 'basic', 'checkbox', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (153, 'processor', 'processor', 'The block processor that this block uses to display it''s content', 'complex', 'checkbox', 'object', '\Core\Classes\Models\BlockProcessor', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (168, 'description', 'description', 'A description', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (171, 'values', 'values', 'The values of the enum', 'meta', 'checkbox', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (176, 'extensionName', 'extensionName', 'The extension that owns the layout', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (181, 'default', 'default', 'Whether the page is the default page', 'basic', 'checkbox', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (183, 'controllerNamespace', 'controllerNamespace', 'The controller namesapce', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (185, 'extensionName', 'extensionName', 'The extension that owns this page', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (188, 'prependToBody', 'prependToBody', 'Content to prepend to the body of the document', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (190, 'isPublic', 'isPublic', 'Whether the page is public. Trying to access a page that is not public will redirect to 404 page not found', 'basic', 'checkbox', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (192, 'publishDate', 'publishDate', 'The publish date of the page (optional). This allows you to set a date where the page will be published', 'basic', 'date', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (195, 'template', 'template', 'The template the page will use. If no template specified, the page will use the default template', 'complex', 'checkbox', 'object', '\Core\Classes\Models\Template', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (205, 'namespace', 'namespace', 'The task namespace', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (210, 'description', 'description', 'The template description', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (213, 'prependToBody', 'prependToBody', 'Content to prepend to the body', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (216, 'appendToHead', 'appendToHead', 'Content to append to the head', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (217, 'isHidden', 'isHidden', 'Whether this template is hidden from the template management page', 'basic', 'checkbox', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (219, 'layout', 'layout', 'The actual layout', 'meta', 'checkbox', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (220, 'name', 'name', 'The name of the theme', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (221, 'description', 'description', 'A description of the theme', 'basic', 'textarea', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (222, 'default', 'default', 'Whether this is the default theme', 'basic', 'checkbox', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (224, 'isHidden', 'isHidden', 'Whether the theme is hidden from the theme management page', 'basic', 'checkbox', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (235, 'username', 'username', 'The username', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (236, 'password', 'password', 'The password', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (237, 'email', 'email', 'The email', 'basic', 'email', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (239, 'canLogin', 'canLogin', 'Whether the user can login', 'basic', 'checkbox', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (240, 'numLogin', 'numLogin', 'The number of times a user has logged in', 'basic', 'number', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (248, 'session', 'session', 'The actual session string', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (250, 'expiryDate', 'expiryDate', 'When the session expires', 'basic', 'datetime', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (223, 'extensionName', 'extensionName', 'The extension that owns the theme', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (241, 'dateLogin', 'dateLogin', 'The date they last logged in', 'basic', 'datetime', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (275, 'isInstalled', 'isInstalled', 'Whether the extension is installed', 'basic', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (277, 'name', 'name', 'A descriptive name for the webhook', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (278, 'action', 'action', 'The action that triggers this webhook', 'enum', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\ModelWebhookActions', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (337, 'name', 'name', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (131, 'class', 'class', 'The class of the api component. Cannot be changed', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (279, 'modelNamespace', 'modelNamespace', 'The namespace of the model', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (281, 'location', 'location', 'The location to send the webhook', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (282, 'verificationKey', 'verificationKey', 'A verification key to send with the webhook to be able to verify that the webhook is authentic at the other end. If this is left blank, we will use the key set in the config', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (283, 'extensionName', 'extensionName', 'The name of the extension that owns this webhook', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (178, 'name', 'name', 'The name of the page. Used in page headers.', 'basic', 'textfield', 'array', '', '', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (292, 'inlineEnabled', 'inline enabled', 'Whether or not this processor can be used inline', 'basic', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (294, 'dateCreated', 'dateCreated', 'The date and time that the user was created', 'basic', 'datetime', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (295, 'dateUpdated', 'dateUpdated', 'The date and time that the user was updated', 'basic', 'datetime', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (296, 'subject', 'subject', 'The email''s subject', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (297, 'body', 'body', 'The body of the email, if no template is being used', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (299, 'fromName', 'from name', 'The name of the sender', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (300, 'fromEmail', 'from email', 'The email of the sender', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (301, 'toName', 'toName', 'The name of the receiver', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (302, 'toEmail', 'toEmail', 'The email of the receiver', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (303, 'cc', 'cc', 'An email => name collection of recipients to be cc into the email', 'meta', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (304, 'bcc', 'bcc', 'An email => name collection of recipients to be bcc into the email', 'meta', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (305, 'dateCreated', 'date created', 'The date the transport was created', 'basic', 'datetime', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (306, 'dateSent', 'date sent', 'The date the transport was last sent (even if unsuccessful)', 'basic', 'datetime', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (307, 'numSent', 'num sent', 'The number of times this transport has been sent to a recipient', 'basic', 'number', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (308, 'errorMessage', 'error message', 'If the transport fails to send internally, the error message will be found here', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (309, 'name', 'name', 'A name that describes the template', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (310, 'description', 'description', 'A description of the template', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (312, 'template', 'template', 'A transport can specify the use of an email template to generate it''s content, rather than set the content manually', 'complex', 'checkbox', 'object', '\Core\Classes\Models\EmailTemplate', '\Core\Classes\Enums\InternalExtensions', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (314, 'extensionName', 'extension name', 'The name of the extension that owns this template', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (316, 'mergeVars', 'merge vars', 'A set of key => value pairs to be used in the replacement of replacers in the body of the transport', 'meta', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (311, 'body', 'body', 'The body of the template, including it''s replacers. If an email transport uses a template, it''s body will be set to this', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (318, 'subject', 'subject', 'The subject. If a transport uses a template, it inherits the templates subject', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (319, 'firstname', 'firstname', 'The first name of the user', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (320, 'lastname', 'lastname', 'The last name of the user', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (321, 'avatar', 'avatar', 'The user''s avatar', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\InternalExtensions', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (322, 'extensionName', 'extensionName', 'The name of the extension that owns this asset', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (324, 'fileName', 'fileName', 'The name of the file', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (326, 'contentTypeMain', 'contentTypeMain', 'The main content type', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (327, 'contentTypeSub', 'contentTypeSub', 'The sub content type', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (329, 'isFile', 'isFile', '', 'basic', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (325, 'dirPath', 'dirPath', 'The path to the file within the extension''s asset directory', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (330, 'webUrl', 'webUrl', 'The web-accessible url', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (331, 'localUrl', 'localUrl', 'The local url', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (333, 'name', 'name', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (334, 'description', 'description', '', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (335, 'icon', 'icon', '', 'enum', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (338, 'description', 'description', '', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (339, 'category', 'category', '', 'complex', 'checkbox', 'object', '\Core\Classes\Models\UserPrivilegeCategory', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (340, 'extensionName', 'extension name', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (341, 'name', 'name', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (342, 'description', 'description', '', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (343, 'privileges', 'privileges', '', 'complex', 'checkbox', 'array', '\Core\Classes\Models\UserPrivilege', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (345, 'userId', 'user id', '', 'basic', 'number', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (346, 'allRead', 'allRead', '', 'basic', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (347, 'readGroups', 'readGroups', '', 'complex', 'checkbox', 'array', '\Core\Classes\Models\UserGroup', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (348, 'userGroup', 'userGroup', '', 'complex', 'checkbox', 'object', '\Core\Classes\Models\UserGroup', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (349, 'allRead', 'allRead', '', 'basic', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (350, 'readGroups', 'readGroups', '', 'complex', 'checkbox', 'array', '\Core\Classes\Models\UserGroup', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (351, 'allInline', 'allInline', '', 'basic', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (352, 'inlineGroups', 'inlineGroups', '', 'complex', 'checkbox', 'array', '\Core\Classes\Models\UserGroup', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (353, 'name', 'name', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (354, 'parentId', 'parentId', '', 'basic', 'number', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (355, 'description', 'description', '', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (356, 'icon', 'icon', '', 'enum', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (357, 'extensionName', 'extensionName', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (358, 'allRead', 'allRead', '', 'basic', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (359, 'readGroups', 'readGroups', '', 'complex', 'checkbox', 'array', '\Core\Classes\Models\UserGroup', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (360, 'modifyGroups', 'modifyGroups', '', 'complex', 'checkbox', 'array', '\Core\Classes\Models\UserGroup', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (361, 'category', 'category', '', 'complex', 'checkbox', 'object', '\Core\Classes\Models\BlockCategory', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (362, 'subCategory', 'subCategory', '', 'complex', 'checkbox', 'object', '\Core\Classes\Models\BlockCategory', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (363, 'name', 'name', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (204, 'class', 'class', 'The class of the task, cannot be changed', 'basic', 'textfield', 'array', '', '', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (364, 'name', 'name', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (365, 'name', 'name', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (366, 'name', 'name', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (367, 'name', 'name', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (368, 'description', 'description', '', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (369, 'icon', 'icon', '', 'enum', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (371, 'category', 'category', '', 'complex', 'checkbox', 'object', '\Core\Classes\Models\BlockProcessorCategory', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (370, 'extensionName', 'extensionName', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (373, 'name', 'name', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (374, 'label', 'label', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (375, 'allRead', 'allRead', '', 'basic', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (376, 'readGroups', 'readGroups', '', 'complex', 'checkbox', 'array', '\Core\Classes\Models\UserGroup', '\Core\Classes\Enums\BlockCategory', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (377, 'modifyGroups', 'modifyGroups', '', 'complex', 'checkbox', 'array', '\Core\Classes\Models\UserGroup', '\Core\Classes\Enums\BlockCategory', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (378, 'readFailPageId', 'readFailPageId', '', 'basic', 'number', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (379, 'modifyGroups', 'modifyGroups', '', 'complex', 'checkbox', 'array', '\Core\Classes\Models\UserGroup', '\Core\Classes\Enums\BlockCategory', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (380, 'bio', 'bio', '', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (381, 'userId', 'userId', '', 'basic', 'number', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (382, 'name', 'name', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (383, 'description', 'description', '', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (384, 'detail1Description', 'detail1Description', '', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (385, 'detail2Description', 'detail2Description', '', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (386, 'detail3Description', 'detail3Description', '', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (387, 'type', 'type', '', 'complex', 'checkbox', 'object', '\Core\Classes\Models\UserActionType', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (388, 'date', 'date', '', 'basic', 'date', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (389, 'time', 'time', '', 'basic', 'time', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (390, 'detail1', 'detail1', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (391, 'detail2', 'detail2', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (392, 'detail3', 'detail3', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (393, 'userId', 'userId', '', 'basic', 'number', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (394, 'updatedById', 'updatedById', '', 'basic', 'number', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (395, 'dateUpdated', 'dateUpdated', '', 'basic', 'datetime', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (396, 'body', 'body', '', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (397, 'manageAll', 'manageAll', '', 'basic', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (398, 'manageGroups', 'manageGroups', '', 'complex', 'checkbox', 'array', '\Core\Classes\Models\UserGroup', '\Core\Classes\Enums\BlockCategory', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (344, 'extensionName', 'extensionName', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (401, 'shortname', 'shortname', 'The generated short name of the user', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (402, 'longname', 'longname', 'The generated long name for this user', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (404, 'name', 'name', 'the name', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (406, 'enabled', 'enabled', 'Whether this language is enabled', 'basic', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (408, 'sLocale', 'sLocale', 'The system locale', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (409, 'mLocale', 'mLocale', 'The microsoft locale', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (410, 'language', 'language', 'The user''s currently selected language', 'complex', 'checkbox', 'object', '\Core\Classes\Models\Language', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (411, 'plural', 'plural', '', 'basic', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (412, 'msgid', 'msgid', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (413, 'msgid_plural', 'msgid_plural', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (414, 'comments', 'comments', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (416, 'extensionName', 'extensionName', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (418, 'baseId', 'baseId', '', 'basic', 'number', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (419, 'languageId', 'languageId', '', 'basic', 'number', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (420, 'msgstr', 'msgstr', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (421, 'msgstr_plural', 'msgstr_plural', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (423, 'image', 'image', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (424, 'dateActive', 'dateActive', '', 'basic', 'datetime', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (425, 'extensionName', 'extensionName', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (426, 'name', 'name', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\FontAwesomeIcons', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (432, 'address1', 'address1', 'Line 1 of the address', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (434, 'city', 'city', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (435, 'region', 'region', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (436, 'postalCode', 'postalCode', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (437, 'country', 'country', '', 'enum', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (438, 'acceptsMarketing', 'acceptsMarketing', '', 'basic', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (439, 'title', 'title', '', 'enum', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Titles', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (440, 'gender', 'gender', '', 'enum', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Genders', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (441, 'description', 'description', '', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (442, 'query', 'query', '', 'meta', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (443, 'extensionName', 'extensionName', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (444, 'name', 'name', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (445, 'description', 'description', '', 'basic', 'textarea', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (446, 'extensionName', 'extensionName', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (447, 'userId', 'userId', '', 'basic', 'number', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (448, 'byUserId', 'byUserId', '', 'basic', 'number', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (449, 'type', 'type', '', 'enum', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\UserContactType', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (450, 'topic', 'topic', '', 'complex', 'checkbox', 'object', '\Core\Classes\Models\UserContactTopic', '\Core\Classes\Enums\Countries', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (451, 'identifier', 'identifier', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (452, 'date', 'date', '', 'basic', 'date', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (453, 'time', 'time', '', 'basic', 'time', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (433, 'address2', 'address2', 'Line 2 of the address', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (454, 'telno', 'telno', 'The user''s phone number', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (455, 'middlenames', 'middlenames', '', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (456, 'namespace', 'namespace', 'The namespace of the Extension component', 'basic', 'textfield', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', true);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (457, 'build', 'build', 'The currently patched build of the extension', 'basic', 'number', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', false);
INSERT INTO core_classes_models_property (id, label, name, description, "propertyType", "basicType", "complexType", "childModelNamespace", "enumNamespace", "isIndex") VALUES (459, 'isInternal', 'isInternal', 'Whether or not this extension is internal', 'basic', 'checkbox', 'array', '\Core\Classes\Models\Api', '\Core\Classes\Enums\Countries', true);


--
-- Name: core_classes_models_property_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_property_seq', 459, true);


--
-- Data for Name: core_classes_models_task; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_task (id, label, class, namespace, description, "extensionName", "allRead", "readGroups", name) VALUES (2, 'data', 'Data', '\Core\Components\Tasks\Data', 'Tasks related to data management', 'Core', false, '{2,1}', 'data');


--
-- Name: core_classes_models_task_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_task_seq', 4, true);


--
-- Data for Name: core_classes_models_template; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_template (id, name, description, "default", "extensionName", "prependToBody", "prependToHead", "appendToBody", "appendToHead", "isHidden", layout, "modifyGroups") VALUES (10, 'Complex Template', 'a complex template with header, footer, outer side bar and inner side bar', false, 'Application', '', '', '', '', false, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":3,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[]},{"x":3,"width":9,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":45,"classes":"","styling":"","rows":[]}]},{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":3,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[]},{"x":3,"width":9,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","rows":[]}]}]}]},{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":44,"classes":"","styling":"","rows":[]}]}]}]}],"cells":[]}', '{2,1}');
INSERT INTO core_classes_models_template (id, name, description, "default", "extensionName", "prependToBody", "prependToHead", "appendToBody", "appendToHead", "isHidden", layout, "modifyGroups") VALUES (3, 'Extorio Admin Login', 'The template used on the login page', false, 'Core', '', '', '', '', true, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":true,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":60,"classes":"","styling":"","rows":[]}]},{"collapsexs":false,"collapsesm":false,"collapsemd":true,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":4,"width":4,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"well well-lg","styling":"","rows":[]}]},{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":60,"classes":"","styling":"","rows":[]}]}]}]}],"cells":[]}', '{1}');
INSERT INTO core_classes_models_template (id, name, description, "default", "extensionName", "prependToBody", "prependToHead", "appendToBody", "appendToHead", "isHidden", layout, "modifyGroups") VALUES (9, 'Basic Template With Side Bar', 'basic template with side bar', false, 'Application', '', '', '', '', false, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":3,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[]},{"x":3,"width":9,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","rows":[{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","rows":[]}]}]}]}],"cells":[]}', '{2,1}');
INSERT INTO core_classes_models_template (id, name, description, "default", "extensionName", "prependToBody", "prependToHead", "appendToBody", "appendToHead", "isHidden", layout, "modifyGroups") VALUES (2, 'Extorio Admin', 'Extorio admin template', false, 'Core', '', '', '', '', true, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":true,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"notranslate","styling":"","configurable":false,"rows":[{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":false,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":46,"classes":"well well-sm","styling":"","configurable":true}]},{"collapsexs":false,"collapsesm":false,"collapsemd":true,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":2,"hidexs":false,"hidesm":false,"hidemd":true,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":12,"classes":"","styling":"","configurable":true},{"x":2,"width":10,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","configurable":false,"rows":[{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":50,"classes":"","styling":"","configurable":true}]},{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"well well-sm","styling":"","configurable":false}]}]}]}]}]}]}', '{}');
INSERT INTO core_classes_models_template (id, name, description, "default", "extensionName", "prependToBody", "prependToHead", "appendToBody", "appendToHead", "isHidden", layout, "modifyGroups") VALUES (1, 'Default Template', 'Your default template', true, 'Application', '', '', '', '', false, '{"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"","styling":"","configurable":false,"rows":[{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":45,"classes":"","styling":"","configurable":true}]},{"collapsexs":false,"collapsesm":false,"collapsemd":true,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"well well-sm","styling":"","configurable":false,"rows":[{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":2,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":102,"classes":"","styling":"","configurable":true},{"x":2,"width":10,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":62,"classes":"","styling":"","configurable":true}]}]}]},{"collapsexs":false,"collapsesm":false,"collapsemd":true,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":1,"width":10,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"cell","blockid":0,"classes":"panel panel-default panel-body","styling":"","configurable":false,"rows":[{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":46,"classes":"","styling":"","configurable":true}]},{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":83,"classes":"","styling":"","configurable":true}]},{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":51,"classes":"","styling":"","configurable":true}]},{"collapsexs":false,"collapsesm":true,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"content","blockid":0,"classes":"","styling":"","configurable":false}]}]}]},{"collapsexs":false,"collapsesm":false,"collapsemd":false,"collapselg":false,"collapseall":false,"align":"top","role":"row","spacing":true,"cells":[{"x":0,"width":12,"hidexs":false,"hidesm":false,"hidemd":false,"hidelg":false,"hideall":false,"align":"inherit","role":"block","blockid":44,"classes":"","styling":"","configurable":true}]}]}]}]}', '{}');


--
-- Name: core_classes_models_template_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_template_seq', 11, true);


--
-- Data for Name: core_classes_models_theme; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (3, 'Extorio Admin', 'extorio admin theme', false, 'Core', true, 'extorio-admin');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (5, 'Cosmo', 'The cosmo bootswatch theme', false, 'Application', false, 'cosmo');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (6, 'Cyborg', 'The cyborg bootswatch theme', false, 'Application', false, 'cyborg');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (7, 'Darkly', 'The darkly bootswatch theme', false, 'Application', false, 'darkly');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (8, 'Flatly', 'The flatly bootswatch theme', false, 'Application', false, 'flatly');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (9, 'Journal', 'The journal bootswatch theme', false, 'Application', false, 'journal');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (10, 'Lumen', 'The lumen bootswatch theme', false, 'Application', false, 'lumen');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (11, 'Paper', 'The paper bootswatch theme', false, 'Application', false, 'paper');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (12, 'Readable', 'The readable bootswatch theme', false, 'Application', false, 'readable');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (13, 'Sandstone', 'The sandstone bootswatch theme', false, 'Application', false, 'sandstone');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (14, 'Simplex', 'The simplex bootswatch theme', false, 'Application', false, 'simplex');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (15, 'Slate', 'The slate bootswatch theme', false, 'Application', false, 'slate');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (16, 'Spacelab', 'The spacelab bootswatch theme', false, 'Application', false, 'spacelab');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (17, 'Superhero', 'The superhero bootswatch theme', false, 'Application', false, 'superhero');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (18, 'United', 'The united bootswatch theme', false, 'Application', false, 'united');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (19, 'Yeti', 'The yeti bootswatch theme', false, 'Application', false, 'yeti');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (4, 'Cerulean', 'The cerulean bootswatch theme', false, 'Application', false, 'cerulean');
INSERT INTO core_classes_models_theme (id, name, description, "default", "extensionName", "isHidden", label) VALUES (2, 'Default Theme', 'Your default theme', true, 'Application', false, 'default-theme');


--
-- Name: core_classes_models_theme_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_theme_seq', 20, true);


--
-- Data for Name: core_classes_models_translation; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: core_classes_models_translation_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_translation_seq', 14, true);


--
-- Data for Name: core_classes_models_translationbase; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: core_classes_models_translationbase_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_translationbase_seq', 9, true);


--
-- Data for Name: core_classes_models_useractiontype; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_useractiontype (id, name, description, "detail1Description", "detail2Description", "detail3Description", "extensionName") VALUES (5, 'user_create', 'Created a user', 'The user that was created', '', '', 'Core');
INSERT INTO core_classes_models_useractiontype (id, name, description, "detail1Description", "detail2Description", "detail3Description", "extensionName") VALUES (6, 'user_modify', 'Modified a user', 'The user that was modified', '', '', 'Core');
INSERT INTO core_classes_models_useractiontype (id, name, description, "detail1Description", "detail2Description", "detail3Description", "extensionName") VALUES (7, 'user_delete', 'Deleted a user', 'The user that was deleted', '', '', 'Core');
INSERT INTO core_classes_models_useractiontype (id, name, description, "detail1Description", "detail2Description", "detail3Description", "extensionName") VALUES (8, 'user_login', 'Logged in', '', '', '', 'Core');
INSERT INTO core_classes_models_useractiontype (id, name, description, "detail1Description", "detail2Description", "detail3Description", "extensionName") VALUES (10, 'user_update_basic', 'Updated basic user details', '', '', '', 'Core');
INSERT INTO core_classes_models_useractiontype (id, name, description, "detail1Description", "detail2Description", "detail3Description", "extensionName") VALUES (11, 'user_update_email', 'Updated email address', '', '', '', 'Core');
INSERT INTO core_classes_models_useractiontype (id, name, description, "detail1Description", "detail2Description", "detail3Description", "extensionName") VALUES (12, 'user_update_password', 'Updated password', '', '', '', 'Core');
INSERT INTO core_classes_models_useractiontype (id, name, description, "detail1Description", "detail2Description", "detail3Description", "extensionName") VALUES (13, 'user_update_username', 'Updated username', '', '', '', 'Core');


--
-- Name: core_classes_models_useractiontype_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_useractiontype_seq', 13, true);


--
-- Data for Name: core_classes_models_usercontact; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: core_classes_models_usercontact_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_usercontact_seq', 3, true);


--
-- Data for Name: core_classes_models_usercontacttopic; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_usercontacttopic (id, name, description, "extensionName") VALUES (1, 'Unknown', 'The topic is unknown', 'Core');


--
-- Name: core_classes_models_usercontacttopic_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_usercontacttopic_seq', 3, true);


--
-- Data for Name: core_classes_models_usergroup; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_usergroup (id, name, description, privileges, "extensionName", "manageAll", "manageGroups") VALUES (3, 'User', 'A general user of your website', '{}', 'Core', false, '{}');
INSERT INTO core_classes_models_usergroup (id, name, description, privileges, "extensionName", "manageAll", "manageGroups") VALUES (2, 'Administrator', 'A general administrator of your website', '{89,35,36,12,98,97,51,54,56,53,55,63,66,71,70,62,67,64,65,84,69,68,91,92,23,26,121,118,119,17,18,45,46,44}', 'Core', false, '{3}');
INSERT INTO core_classes_models_usergroup (id, name, description, privileges, "extensionName", "manageAll", "manageGroups") VALUES (1, 'Super Administrator', 'A super administrator of your website', '{4,89,90,35,37,36,32,34,33,29,31,5,30,12,9,7,6,98,97,99,96,95,94,48,50,49,87,88,51,52,61,91,93,92,106,105,104,103,100,102,101,41,43,42,23,24,25,2,1,26,28,27,17,19,18,20,22,21,45,47,46,44,38,40,39,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,153}', 'Core', true, '{}');


--
-- Name: core_classes_models_usergroup_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_usergroup_seq', 4, true);


--
-- Data for Name: core_classes_models_usernote; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: core_classes_models_usernote_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_usernote_seq', 18, true);


--
-- Data for Name: core_classes_models_userprivilege; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (1, 'tasks_read_all', 'This user is able to access (and run) all tasks', 1, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (2, 'pages_read_all', 'This allows the user to read all pages', 2, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (34, 'block_processor_categories_delete', 'Allows the ability to delete block processor categories', 11, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (33, 'block_processor_categories_modify', 'Allows the ability to modify block processor categories', 11, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (28, 'templates_delete_all', 'Allows the ability to delete all templates', 10, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (27, 'templates_modify_all', 'Allows the ability to modify all templates', 10, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (19, 'users_delete', 'Allows the ability to delete any users within user groups that you manage', 7, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (20, 'user_groups_create', 'Allows the ability to create user groups', 8, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (22, 'user_groups_delete', 'Allows the ability to delete user groups', 8, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (23, 'pages_create', 'allows the ability to create pages', 2, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (24, 'pages_delete_all', 'Allows the ability to delete all pages', 2, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (25, 'pages_modify_all', 'Allows the ability to modify all pages', 2, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (21, 'user_groups_modify', 'Allows the ability to modify user groups', 8, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (38, 'user_privileges_create', 'Allows the ability to create new user privileges', 9, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (40, 'user_privileges_delete', 'Allows the ability to delete user privileges', 9, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (39, 'user_privileges_modify', 'Allows the ability to modify user privileges', 9, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (41, 'models_create', 'Allows the ability to create models', 14, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (42, 'models_modify', 'Allows the ability to modify models', 14, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (43, 'models_delete', 'Allows the ability to delete models', 14, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (4, 'apis_read_all', 'This privilege allows the user to read all apis', 3, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (29, 'block_processors_create', 'Allows the ability to create block processors', 4, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (31, 'block_processors_delete', 'Allows the ability to delete block processors', 4, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (5, 'block_processors_inline_all', 'This privilege allows the user to use all block processors inline', 4, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (30, 'block_processors_modify', 'Allows the ability to modify block processors', 4, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (12, 'blocks_create', 'This privilege allows you to create new blocks', 5, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (9, 'blocks_delete_all', 'This privilege allows you to delete blocks', 5, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (7, 'blocks_modify_all', 'This privilege allows the user to modify all blocks', 5, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (6, 'blocks_read_all', 'This privilege allows the user to read all blocks', 5, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (35, 'block_categories_create', 'Allows the ability to create block categories', 12, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (36, 'block_categories_modify', 'Allows the ability to modify block categories', 12, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (26, 'templates_create', 'Allows the ability to create templates', 10, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (17, 'users_create', 'Allows for the ability to create users within any user groups that you manage', 7, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (18, 'users_modify', 'Allows the ability to modify users within any user groups that you manage', 7, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (37, 'block_categories_delete', 'Allows the ability to delete block categories', 12, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (32, 'block_processor_categories_create', 'Allows the ability to create block processor categories', 11, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (44, 'user_notes_read', 'Allows the ability to read the notes for users that you manage', 15, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (45, 'user_notes_create', 'Allows the ability to create notes for users that you manage', 15, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (46, 'user_notes_modify', 'Allows the ability to modify notes for users that you manage', 15, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (47, 'user_notes_delete', 'Allows the ability to delete notes for users that you manage', 15, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (48, 'enums_create', 'Allows the ability to create enums', 16, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (49, 'enums_modify', 'Allows the ability to modify enums', 16, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (50, 'enums_delete', 'Allows the ability to delete enums', 16, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (51, 'extorio_admin', 'Allows the user to access the extorio-admin pages', 6, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (52, 'extorio_menus_all', 'Displays all menu sections for this user', 17, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (54, 'extorio_menus_content_management', 'Displays the content management menu section for this user', 17, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (57, 'extorio_menus_data_management', 'Displays the data management menu section for this user', 17, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (58, 'extorio_menus_developers', 'Displays the developers menu section for this user', 17, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (56, 'extorio_menus_email_management', 'Displays the email management menu section for this user', 17, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (53, 'extorio_menus_extorio', 'Displays the extorio menu section for this user', 17, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (59, 'extorio_menus_settings', 'Displays the settings menu section for this user', 17, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (55, 'extorio_menus_user_management', 'Displays the user management menu section for this user', 17, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (60, 'extorio_menus_utilities', 'Displays the utilities menu section for this user', 17, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (61, 'extorio_pages_all', 'Allows access to all extorio admin pages', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (62, 'extorio_pages_extensions', 'Allows access to the extensions admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (63, 'extorio_pages_assets', 'Allows access to the assets admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (64, 'extorio_pages_pages', 'Allows access to the pages admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (65, 'extorio_pages_templates', 'Allows access to the templates admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (66, 'extorio_pages_blocks', 'Allows access to the blocks admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (67, 'extorio_pages_layouts', 'Allows access to the layouts admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (68, 'extorio_pages_users', 'Allows access to the users admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (69, 'extorio_pages_user_groups', 'Allows access to the user groups admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (70, 'extorio_pages_email_transports', 'Allows access to the email templates admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (71, 'extorio_pages_email_templates', 'Allows access to the email templates admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (72, 'extorio_pages_models', 'Allows access to the models admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (73, 'extorio_pages_model_webhooks', 'Allows access to the model webhooks admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (74, 'extorio_pages_model_viewer', 'Allows access to the model viewer admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (75, 'extorio_pages_enums', 'Allows access to the enums admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (77, 'extorio_pages_user_settings', 'Allows access to the user settings admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (78, 'extorio_pages_email_settings', 'Allows access to the email settings admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (79, 'extorio_pages_user_privileges', 'Allows access to the user privileges admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (80, 'extorio_pages_user_privilege_categories', 'Allows access to the user privilege categories admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (81, 'extorio_pages_block_processors', 'Allows access to the block processors admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (82, 'extorio_pages_apis', 'Allows access to the apis admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (83, 'extorio_pages_tasks', 'Allows access to the tasks admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (84, 'extorio_pages_themes', 'Allows access to the themes admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (85, 'extorio_pages_log_manager', 'Allows access to the log manager admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (86, 'extorio_pages_task_manager', 'Allows access to the task manager admin page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (87, 'extensions_install', 'Allows this user to install extensions', 19, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (88, 'extensions_uninstall', 'Allows the ability to uninstall extensions', 19, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (89, 'assets_create', 'Allows the ability to create assets', 20, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (90, 'assets_delete', 'Allows the ability to delete assets', 20, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (91, 'layouts_create', 'Allows the ability to create layouts', 21, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (92, 'layouts_modify', 'Allows for the ability to modify layouts', 21, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (93, 'layouts_delete', 'Allows the ability to delete layouts', 21, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (94, 'email_transports_view', 'Allows for the ability to view the contents of all email transports', 22, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (95, 'email_transports_resend', 'Allows for the ability to resend any email transports', 22, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (96, 'email_transports_delete', 'Allows for the ability to delete any email transports', 22, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (97, 'email_templates_create', 'Allows the ability to create email templates', 23, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (99, 'email_templates_delete', 'Allows the ability to delete email templates', 23, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (100, 'model_webhooks_create', 'Allows the ability to create model webhooks', 24, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (101, 'model_webhooks_modify', 'Allows the ability to modify model webhooks', 24, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (102, 'model_webhooks_delete', 'Allows the ability to delete model webhooks', 24, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (103, 'model_viewer_list', 'Allows the ability to view data in list view in the model viewer', 25, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (104, 'model_viewer_edit', 'Allows the ability to create/edit entries in the model viewer edit view', 25, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (105, 'model_viewer_detail', 'Allows the ability to view an entry in the detail view', 25, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (106, 'model_viewer_delete', 'Allows the ability to delete an entry within the model viewer', 25, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (107, 'user_privilege_categories_create', 'Allows the ability to create user privilege categories', 26, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (108, 'user_privilege_categories_modify', 'Allows the ability to modify user privilege categories', 26, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (109, 'user_privilege_categories_delete', 'Allows the ability to delete user privilege categories', 26, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (110, 'apis_create', 'Allows the ability to create apis', 3, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (111, 'api_modify', 'Allows the ability to modify apis', 3, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (112, 'apis_delete', 'Allows the ability to delete apis', 3, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (113, 'tasks_create', 'Allows the ability to create tasks', 1, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (114, 'tasks_modify', 'Allows the ability to modify tasks', 1, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (115, 'tasks_delete', 'Allows the ability to delete tasks', 1, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (116, 'tasks_run', 'Allows the ability to run tasks from the task manager', 1, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (117, 'tasks_kill', 'Allows the ability to kill tasks from the task manager', 1, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (118, 'themes_create', 'Allows the ability to create a theme', 27, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (119, 'themes_modify', 'Allows the ability to modify themes', 27, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (120, 'themes_delete', 'Allows the ability to delete themes', 27, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (121, 'themes_compile', 'Allows the ability to compile themes from the theme management page', 27, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (122, 'logs_view', 'Allows the ability to view the contents of a log file', 28, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (123, 'logs_delete', 'Allows the ability to delete logs', 28, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (124, 'extorio_pages_application_settings', 'Allows access to the application settings page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (125, 'extorio_pages_extorio_settings', 'Allows access to the extorio settings page', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (126, 'translation_base_create', 'Allows the ability to create base translations', 29, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (127, 'translation_base_modify', 'Allows the ability to modify base translations', 29, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (128, 'translation_base_delete', 'Allows the ability to delete base tanslations', 29, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (129, 'translation_create', 'Allows the ability to create translations', 30, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (130, 'translation_modify', 'Allows the ability to modify translations', 30, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (131, 'translation_delete', 'Allows the ability to delete translations', 30, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (132, 'extorio_pages_translations', '', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (133, 'extorio_pages_block_categories', '', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (134, 'user_actions_read', 'Allows the ability to view the user actions for users that you manage', 31, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (135, 'user_reports_create', 'Allows the ability to create new user reports', 32, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (136, 'user_reports_delete', 'Allows the ability to delete user reports', 32, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (137, 'extorio_pages_user_reports', '', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (138, 'user_reports_modify', 'Allows the ability to modify user reports', 32, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (98, 'email_templates_modify', 'Allows the ability to modify email templates', 23, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (139, 'user_contacts_read', 'Allows the ability to read the contacts for users that you manage', 33, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (140, 'user_contacts_create', 'Allows the ability to create contacts for users that you manage', 33, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (141, 'user_contacts_modify', 'Allows the ability to modify contacts for users that you manage', 33, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (142, 'user_contacts_delete', 'Allows the ability to delete contacts for users that you manage', 33, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (143, 'user_contact_topics_create', 'Allows the ability to create contact topics', 34, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (144, 'user_contact_topics_modify', 'Allows the ability to modify user contact topics', 34, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (145, 'user_contact_topics_delete', 'Allows the ability to delete user contact topics', 34, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (146, 'extorio_pages_user_contact_topics', '', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (147, 'user_action_types_create', 'Allows the ability to create user action types', 35, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (148, 'user_action_types_modify', 'Allows the ability to modify user action types', 35, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (149, 'user_action_types_delete', 'Allows the ability to delete user action types', 35, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (150, 'extorio_pages_user_action_types', '', 18, 'Core');
INSERT INTO core_classes_models_userprivilege (id, name, description, category, "extensionName") VALUES (153, 'extensions_update', '', 19, 'Core');


--
-- Name: core_classes_models_userprivilege_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_userprivilege_seq', 153, true);


--
-- Data for Name: core_classes_models_userprivilegecategory; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (1, 'Tasks', 'Privileges related to tasks', 'tasks', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (2, 'Pages', 'Privileges related to pages', 'files-o', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (14, 'Models', 'Privileges related to models', 'database', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (6, 'Extorio', 'Extorio related privileges', 'dashboard', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (5, 'Blocks', 'Privileges related to blocks', 'square', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (4, 'Block Processors', 'Privileges related to block processors', 'cogs', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (3, 'Apis', 'Privileges related to apis', 'globe', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (8, 'User Groups', 'Privileges related to user groups', 'users', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (12, 'Block Categories', 'Privileges related to block categories', 'list', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (9, 'User Privileges', 'Privileges related to user privileges', 'key', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (7, 'User', 'Privileges related to users', 'users', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (10, 'Templates', 'Privileges related to templates', 'file', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (11, 'Block Processor Categories', 'Privileges related to block processor categories', 'list', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (15, 'User Notes', 'Privileges related to user notes', 'comments', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (16, 'Enums', 'Privileges related to enums', 'database', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (17, 'Extorio Menus', 'Privileges related to extorio menus', 'bars', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (18, 'Extorio Pages', 'Privileges related to extorio admin pages', 'file', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (19, 'Extensions', 'Privileges related to extensions', 'bolt', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (20, 'Assets', 'Privileges related to assets', 'files-o', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (21, 'Layouts', 'Privileges related to layouts', 'adjust', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (22, 'Email Transports', 'Privileges related to email transports', 'envelope', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (23, 'Email Templates', 'Privileges related to email templates', 'envelope', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (24, 'Model Webhooks', 'Privileges related to model webhooks', 'database', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (25, 'Model Viewer', 'Privileges related to the model viewer', 'database', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (26, 'User Privilege Categories', 'Privileges related to user privilege categories', 'adjust', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (27, 'Themes', 'Privileges related to themes', 'image', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (28, 'Logs', 'Privileges related to logs', 'files-o', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (29, 'Translation Base', 'Privileges related to translation base', 'globe', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (30, 'Translation', 'Privileges related to translation', 'globe', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (31, 'User Actions', 'Privileges related to user actions', 'user', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (32, 'User Reports', 'Privileges related to user reports', 'user', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (33, 'User Contacts', 'Privileges related to user contacts', 'users', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (34, 'User Contact Topics', 'Privileges related to user contact topics', 'users', 'Core');
INSERT INTO core_classes_models_userprivilegecategory (id, name, description, icon, "extensionName") VALUES (35, 'User Action Types', 'Privileges related to user action types', 'users', 'Core');


--
-- Name: core_classes_models_userprivilegecategory_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_userprivilegecategory_seq', 36, true);


--
-- Data for Name: core_classes_models_userreport; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: core_classes_models_userreport_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_userreport_seq', 11, true);


--
-- Name: core_classes_models_usersession_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('core_classes_models_usersession_seq', 38, true);

--
-- Data for Name: tasks_livetask; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: tasks_livetask_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('tasks_livetask_id_seq', 1, false);


--
-- Name: core_classes_models_api_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_api
    ADD CONSTRAINT core_classes_models_api_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_asset_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_asset
    ADD CONSTRAINT core_classes_models_asset_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_block_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_block
    ADD CONSTRAINT core_classes_models_block_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_blockcategory_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_blockcategory
    ADD CONSTRAINT core_classes_models_blockcategory_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_blockprocessor_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_blockprocessor
    ADD CONSTRAINT core_classes_models_blockprocessor_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_blockprocessorcategory_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_blockprocessorcategory
    ADD CONSTRAINT core_classes_models_blockprocessorcategory_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_emailtemplate_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_emailtemplate
    ADD CONSTRAINT core_classes_models_emailtemplate_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_emailtransport_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_emailtransport
    ADD CONSTRAINT core_classes_models_emailtransport_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_enum_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_enum
    ADD CONSTRAINT core_classes_models_enum_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_extension_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_extension
    ADD CONSTRAINT core_classes_models_extension_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_language_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_language
    ADD CONSTRAINT core_classes_models_language_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_layout_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_layout
    ADD CONSTRAINT core_classes_models_layout_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_model_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_model
    ADD CONSTRAINT core_classes_models_model_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_modelwebhook_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_modelwebhook
    ADD CONSTRAINT core_classes_models_modelwebhook_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_page_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_page
    ADD CONSTRAINT core_classes_models_page_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_property_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_property
    ADD CONSTRAINT core_classes_models_property_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_task_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_task
    ADD CONSTRAINT core_classes_models_task_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_template_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_template
    ADD CONSTRAINT core_classes_models_template_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_theme_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_theme
    ADD CONSTRAINT core_classes_models_theme_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_translation_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_translation
    ADD CONSTRAINT core_classes_models_translation_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_translationbase_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_translationbase
    ADD CONSTRAINT core_classes_models_translationbase_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_user_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_user
    ADD CONSTRAINT core_classes_models_user_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_useraction_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_useraction
    ADD CONSTRAINT core_classes_models_useraction_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_useractiontype_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_useractiontype
    ADD CONSTRAINT core_classes_models_useractiontype_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_usercontact_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_usercontact
    ADD CONSTRAINT core_classes_models_usercontact_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_usercontacttopic_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_usercontacttopic
    ADD CONSTRAINT core_classes_models_usercontacttopic_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_usergroup_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_usergroup
    ADD CONSTRAINT core_classes_models_usergroup_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_usernote_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_usernote
    ADD CONSTRAINT core_classes_models_usernote_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_userprivilege_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_userprivilege
    ADD CONSTRAINT core_classes_models_userprivilege_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_userprivilegecategory_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_userprivilegecategory
    ADD CONSTRAINT core_classes_models_userprivilegecategory_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_userreport_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_userreport
    ADD CONSTRAINT core_classes_models_userreport_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_usersession_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_classes_models_usersession
    ADD CONSTRAINT core_classes_models_usersession_id_pk PRIMARY KEY (id);


--
-- Name: orm_modcache_id_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY orm_modcache
    ADD CONSTRAINT orm_modcache_id_pk PRIMARY KEY (id);


--
-- Name: core_classes_models_api_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_api_extensionname_index ON core_classes_models_api USING btree ("extensionName");


--
-- Name: core_classes_models_api_label_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_api_label_index ON core_classes_models_api USING btree (label);


--
-- Name: core_classes_models_api_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_api_name_index ON core_classes_models_api USING btree (name);


--
-- Name: core_classes_models_asset_contenttypemain_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_asset_contenttypemain_index ON core_classes_models_asset USING btree ("contentTypeMain");


--
-- Name: core_classes_models_asset_contenttypesub_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_asset_contenttypesub_index ON core_classes_models_asset USING btree ("contentTypeSub");


--
-- Name: core_classes_models_asset_dirpath_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_asset_dirpath_index ON core_classes_models_asset USING btree ("dirPath");


--
-- Name: core_classes_models_asset_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_asset_extensionname_index ON core_classes_models_asset USING btree ("extensionName");


--
-- Name: core_classes_models_asset_filename_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_asset_filename_index ON core_classes_models_asset USING btree ("fileName");


--
-- Name: core_classes_models_asset_isfile_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_asset_isfile_index ON core_classes_models_asset USING btree ("isFile");


--
-- Name: core_classes_models_block_category_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_block_category_index ON core_classes_models_block USING btree (category);


--
-- Name: core_classes_models_block_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_block_extensionname_index ON core_classes_models_block USING btree ("extensionName");


--
-- Name: core_classes_models_block_ishidden_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_block_ishidden_index ON core_classes_models_block USING btree ("isHidden");


--
-- Name: core_classes_models_block_ispublic_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_block_ispublic_index ON core_classes_models_block USING btree ("isPublic");


--
-- Name: core_classes_models_block_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_block_name_index ON core_classes_models_block USING btree (name);


--
-- Name: core_classes_models_block_subcategory_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_block_subcategory_index ON core_classes_models_block USING btree ("subCategory");


--
-- Name: core_classes_models_blockcategory_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_blockcategory_extensionname_index ON core_classes_models_blockcategory USING btree ("extensionName");


--
-- Name: core_classes_models_blockcategory_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_blockcategory_name_index ON core_classes_models_blockcategory USING btree (name);


--
-- Name: core_classes_models_blockcategory_parentid_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_blockcategory_parentid_index ON core_classes_models_blockcategory USING btree ("parentId");


--
-- Name: core_classes_models_blockprocessor_category_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_blockprocessor_category_index ON core_classes_models_blockprocessor USING btree (category);


--
-- Name: core_classes_models_blockprocessor_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_blockprocessor_extensionname_index ON core_classes_models_blockprocessor USING btree ("extensionName");


--
-- Name: core_classes_models_blockprocessor_inlineenabled_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_blockprocessor_inlineenabled_index ON core_classes_models_blockprocessor USING btree ("inlineEnabled");


--
-- Name: core_classes_models_blockprocessor_label_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_blockprocessor_label_index ON core_classes_models_blockprocessor USING btree (label);


--
-- Name: core_classes_models_blockprocessor_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_blockprocessor_name_index ON core_classes_models_blockprocessor USING btree (name);


--
-- Name: core_classes_models_blockprocessorcategory_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_blockprocessorcategory_extensionname_index ON core_classes_models_blockprocessorcategory USING btree ("extensionName");


--
-- Name: core_classes_models_blockprocessorcategory_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_blockprocessorcategory_name_index ON core_classes_models_blockprocessorcategory USING btree (name);


--
-- Name: core_classes_models_emailtemplate_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_emailtemplate_extensionname_index ON core_classes_models_emailtemplate USING btree ("extensionName");


--
-- Name: core_classes_models_emailtemplate_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_emailtemplate_name_index ON core_classes_models_emailtemplate USING btree (name);


--
-- Name: core_classes_models_emailtransport_datecreated_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_emailtransport_datecreated_index ON core_classes_models_emailtransport USING btree ("dateCreated");


--
-- Name: core_classes_models_emailtransport_datesent_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_emailtransport_datesent_index ON core_classes_models_emailtransport USING btree ("dateSent");


--
-- Name: core_classes_models_emailtransport_fromemail_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_emailtransport_fromemail_index ON core_classes_models_emailtransport USING btree ("fromEmail");


--
-- Name: core_classes_models_emailtransport_fromname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_emailtransport_fromname_index ON core_classes_models_emailtransport USING btree ("fromName");


--
-- Name: core_classes_models_emailtransport_subject_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_emailtransport_subject_index ON core_classes_models_emailtransport USING btree (subject);


--
-- Name: core_classes_models_emailtransport_toemail_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_emailtransport_toemail_index ON core_classes_models_emailtransport USING btree ("toEmail");


--
-- Name: core_classes_models_emailtransport_toname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_emailtransport_toname_index ON core_classes_models_emailtransport USING btree ("toName");


--
-- Name: core_classes_models_enum_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_enum_extensionname_index ON core_classes_models_enum USING btree ("extensionName");


--
-- Name: core_classes_models_enum_ishidden_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_enum_ishidden_index ON core_classes_models_enum USING btree ("isHidden");


--
-- Name: core_classes_models_enum_label_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_enum_label_index ON core_classes_models_enum USING btree (label);


--
-- Name: core_classes_models_enum_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_enum_name_index ON core_classes_models_enum USING btree (name);


--
-- Name: core_classes_models_extension_isinstalled_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_extension_isinstalled_index ON core_classes_models_extension USING btree ("isInstalled");


--
-- Name: core_classes_models_extension_isinternal_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_extension_isinternal_index ON core_classes_models_extension USING btree ("isInternal");


--
-- Name: core_classes_models_extension_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_extension_name_index ON core_classes_models_extension USING btree (name);


--
-- Name: core_classes_models_extension_namespace_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_extension_namespace_index ON core_classes_models_extension USING btree (namespace);


--
-- Name: core_classes_models_language_enabled_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_language_enabled_index ON core_classes_models_language USING btree (enabled);


--
-- Name: core_classes_models_language_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_language_name_index ON core_classes_models_language USING btree (name);


--
-- Name: core_classes_models_layout_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_layout_extensionname_index ON core_classes_models_layout USING btree ("extensionName");


--
-- Name: core_classes_models_layout_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_layout_name_index ON core_classes_models_layout USING btree (name);


--
-- Name: core_classes_models_model_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_model_extensionname_index ON core_classes_models_model USING btree ("extensionName");


--
-- Name: core_classes_models_model_ishidden_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_model_ishidden_index ON core_classes_models_model USING btree ("isHidden");


--
-- Name: core_classes_models_model_label_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_model_label_index ON core_classes_models_model USING btree (label);


--
-- Name: core_classes_models_model_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_model_name_index ON core_classes_models_model USING btree (name);


--
-- Name: core_classes_models_modelwebhook_action_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_modelwebhook_action_index ON core_classes_models_modelwebhook USING btree (action);


--
-- Name: core_classes_models_modelwebhook_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_modelwebhook_extensionname_index ON core_classes_models_modelwebhook USING btree ("extensionName");


--
-- Name: core_classes_models_modelwebhook_modelnamespace_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_modelwebhook_modelnamespace_index ON core_classes_models_modelwebhook USING btree ("modelNamespace");


--
-- Name: core_classes_models_modelwebhook_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_modelwebhook_name_index ON core_classes_models_modelwebhook USING btree (name);


--
-- Name: core_classes_models_page_address_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_page_address_index ON core_classes_models_page USING btree (address);


--
-- Name: core_classes_models_page_default_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_page_default_index ON core_classes_models_page USING btree ("default");


--
-- Name: core_classes_models_page_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_page_extensionname_index ON core_classes_models_page USING btree ("extensionName");


--
-- Name: core_classes_models_page_ishidden_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_page_ishidden_index ON core_classes_models_page USING btree ("isHidden");


--
-- Name: core_classes_models_page_ispublic_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_page_ispublic_index ON core_classes_models_page USING btree ("isPublic");


--
-- Name: core_classes_models_page_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_page_name_index ON core_classes_models_page USING btree (name);


--
-- Name: core_classes_models_page_publishdate_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_page_publishdate_index ON core_classes_models_page USING btree ("publishDate");


--
-- Name: core_classes_models_page_unpublishdate_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_page_unpublishdate_index ON core_classes_models_page USING btree ("unpublishDate");


--
-- Name: core_classes_models_property_basictype_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_property_basictype_index ON core_classes_models_property USING btree ("basicType");


--
-- Name: core_classes_models_property_complextype_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_property_complextype_index ON core_classes_models_property USING btree ("complexType");


--
-- Name: core_classes_models_property_isindex_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_property_isindex_index ON core_classes_models_property USING btree ("isIndex");


--
-- Name: core_classes_models_property_label_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_property_label_index ON core_classes_models_property USING btree (name);


--
-- Name: core_classes_models_property_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_property_name_index ON core_classes_models_property USING btree (label);


--
-- Name: core_classes_models_property_propertytype_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_property_propertytype_index ON core_classes_models_property USING btree ("propertyType");


--
-- Name: core_classes_models_task_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_task_extensionname_index ON core_classes_models_task USING btree ("extensionName");


--
-- Name: core_classes_models_task_label_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_task_label_index ON core_classes_models_task USING btree (label);


--
-- Name: core_classes_models_task_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_task_name_index ON core_classes_models_task USING btree (name);


--
-- Name: core_classes_models_template_default_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_template_default_index ON core_classes_models_template USING btree ("default");


--
-- Name: core_classes_models_template_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_template_extensionname_index ON core_classes_models_template USING btree ("extensionName");


--
-- Name: core_classes_models_template_ishidden_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_template_ishidden_index ON core_classes_models_template USING btree ("isHidden");


--
-- Name: core_classes_models_template_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_template_name_index ON core_classes_models_template USING btree (name);


--
-- Name: core_classes_models_theme_default_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_theme_default_index ON core_classes_models_theme USING btree ("default");


--
-- Name: core_classes_models_theme_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_theme_extensionname_index ON core_classes_models_theme USING btree ("extensionName");


--
-- Name: core_classes_models_theme_ishidden_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_theme_ishidden_index ON core_classes_models_theme USING btree ("isHidden");


--
-- Name: core_classes_models_theme_label_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_theme_label_index ON core_classes_models_theme USING btree (label);


--
-- Name: core_classes_models_theme_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_theme_name_index ON core_classes_models_theme USING btree (name);


--
-- Name: core_classes_models_translation_baseid_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_translation_baseid_index ON core_classes_models_translation USING btree ("baseId");


--
-- Name: core_classes_models_translation_languageid_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_translation_languageid_index ON core_classes_models_translation USING btree ("languageId");


--
-- Name: core_classes_models_translation_msgstr_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_translation_msgstr_index ON core_classes_models_translation USING btree (msgstr);


--
-- Name: core_classes_models_translationbase_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_translationbase_extensionname_index ON core_classes_models_translationbase USING btree ("extensionName");


--
-- Name: core_classes_models_translationbase_msgid_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_translationbase_msgid_index ON core_classes_models_translationbase USING btree (msgid);


--
-- Name: core_classes_models_user_acceptsmarketing_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_user_acceptsmarketing_index ON core_classes_models_user USING btree ("acceptsMarketing");


--
-- Name: core_classes_models_user_canlogin_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_user_canlogin_index ON core_classes_models_user USING btree ("canLogin");


--
-- Name: core_classes_models_user_city_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_user_city_index ON core_classes_models_user USING btree (city);


--
-- Name: core_classes_models_user_country_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_user_country_index ON core_classes_models_user USING btree (country);


--
-- Name: core_classes_models_user_email_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_user_email_index ON core_classes_models_user USING btree (email);


--
-- Name: core_classes_models_user_firstname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_user_firstname_index ON core_classes_models_user USING btree (firstname);


--
-- Name: core_classes_models_user_gender_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_user_gender_index ON core_classes_models_user USING btree (gender);


--
-- Name: core_classes_models_user_lastname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_user_lastname_index ON core_classes_models_user USING btree (lastname);


--
-- Name: core_classes_models_user_middlenames_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_user_middlenames_index ON core_classes_models_user USING btree (middlenames);


--
-- Name: core_classes_models_user_password_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_user_password_index ON core_classes_models_user USING btree (password);


--
-- Name: core_classes_models_user_postalcode_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_user_postalcode_index ON core_classes_models_user USING btree ("postalCode");


--
-- Name: core_classes_models_user_region_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_user_region_index ON core_classes_models_user USING btree (region);


--
-- Name: core_classes_models_user_title_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_user_title_index ON core_classes_models_user USING btree (title);


--
-- Name: core_classes_models_user_usergroup_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_user_usergroup_index ON core_classes_models_user USING btree ("userGroup");


--
-- Name: core_classes_models_user_username_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_user_username_index ON core_classes_models_user USING btree (username);


--
-- Name: core_classes_models_useraction_date_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_useraction_date_index ON core_classes_models_useraction USING btree (date);


--
-- Name: core_classes_models_useraction_detail1_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_useraction_detail1_index ON core_classes_models_useraction USING btree (detail1);


--
-- Name: core_classes_models_useraction_detail2_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_useraction_detail2_index ON core_classes_models_useraction USING btree (detail2);


--
-- Name: core_classes_models_useraction_detail3_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_useraction_detail3_index ON core_classes_models_useraction USING btree (detail3);


--
-- Name: core_classes_models_useraction_time_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_useraction_time_index ON core_classes_models_useraction USING btree ("time");


--
-- Name: core_classes_models_useraction_type_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_useraction_type_index ON core_classes_models_useraction USING btree (type);


--
-- Name: core_classes_models_useraction_userid_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_useraction_userid_index ON core_classes_models_useraction USING btree ("userId");


--
-- Name: core_classes_models_useractiontype_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_useractiontype_extensionname_index ON core_classes_models_useractiontype USING btree ("extensionName");


--
-- Name: core_classes_models_useractiontype_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_useractiontype_name_index ON core_classes_models_useractiontype USING btree (name);


--
-- Name: core_classes_models_usercontact_byuserid_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usercontact_byuserid_index ON core_classes_models_usercontact USING btree ("byUserId");


--
-- Name: core_classes_models_usercontact_date_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usercontact_date_index ON core_classes_models_usercontact USING btree (date);


--
-- Name: core_classes_models_usercontact_identifier_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usercontact_identifier_index ON core_classes_models_usercontact USING btree (identifier);


--
-- Name: core_classes_models_usercontact_time_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usercontact_time_index ON core_classes_models_usercontact USING btree ("time");


--
-- Name: core_classes_models_usercontact_topic_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usercontact_topic_index ON core_classes_models_usercontact USING btree (topic);


--
-- Name: core_classes_models_usercontact_type_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usercontact_type_index ON core_classes_models_usercontact USING btree (type);


--
-- Name: core_classes_models_usercontact_userid_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usercontact_userid_index ON core_classes_models_usercontact USING btree ("userId");


--
-- Name: core_classes_models_usercontacttopic_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usercontacttopic_extensionname_index ON core_classes_models_usercontacttopic USING btree ("extensionName");


--
-- Name: core_classes_models_usercontacttopic_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usercontacttopic_name_index ON core_classes_models_usercontacttopic USING btree (name);


--
-- Name: core_classes_models_usergroup_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usergroup_extensionname_index ON core_classes_models_usergroup USING btree ("extensionName");


--
-- Name: core_classes_models_usergroup_manageall_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usergroup_manageall_index ON core_classes_models_usergroup USING btree ("manageAll");


--
-- Name: core_classes_models_usergroup_managegroups_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usergroup_managegroups_index ON core_classes_models_usergroup USING btree ("manageGroups");


--
-- Name: core_classes_models_usergroup_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usergroup_name_index ON core_classes_models_usergroup USING btree (name);


--
-- Name: core_classes_models_usergroup_privileges_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usergroup_privileges_index ON core_classes_models_usergroup USING btree (privileges);


--
-- Name: core_classes_models_usernote_dateupdated_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usernote_dateupdated_index ON core_classes_models_usernote USING btree ("dateUpdated");


--
-- Name: core_classes_models_usernote_updatedbyid_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usernote_updatedbyid_index ON core_classes_models_usernote USING btree ("updatedById");


--
-- Name: core_classes_models_usernote_userid_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usernote_userid_index ON core_classes_models_usernote USING btree ("userId");


--
-- Name: core_classes_models_userprivilege_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_userprivilege_extensionname_index ON core_classes_models_userprivilege USING btree ("extensionName");


--
-- Name: core_classes_models_userprivilege_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_userprivilege_name_index ON core_classes_models_userprivilege USING btree (name);


--
-- Name: core_classes_models_userprivilegecategory_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_userprivilegecategory_extensionname_index ON core_classes_models_userprivilegecategory USING btree ("extensionName");


--
-- Name: core_classes_models_userprivilegecategory_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_userprivilegecategory_name_index ON core_classes_models_userprivilegecategory USING btree (name);


--
-- Name: core_classes_models_userreport_extensionname_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_userreport_extensionname_index ON core_classes_models_userreport USING btree ("extensionName");


--
-- Name: core_classes_models_userreport_name_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_userreport_name_index ON core_classes_models_userreport USING btree (name);


--
-- Name: core_classes_models_usersession_expirydate_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usersession_expirydate_index ON core_classes_models_usersession USING btree ("expiryDate");


--
-- Name: core_classes_models_usersession_remoteip_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usersession_remoteip_index ON core_classes_models_usersession USING btree ("remoteIP");


--
-- Name: core_classes_models_usersession_session_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usersession_session_index ON core_classes_models_usersession USING btree (session);


--
-- Name: core_classes_models_usersession_userid_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX core_classes_models_usersession_userid_index ON core_classes_models_usersession USING btree ("userId");


--
-- Name: orm_modcache_action_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX orm_modcache_action_index ON orm_modcache USING btree (action);


--
-- Name: orm_modcache_modelinstanceid_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX orm_modcache_modelinstanceid_index ON orm_modcache USING btree ("modelInstanceId");


--
-- Name: orm_modcache_modelnamespace_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX orm_modcache_modelnamespace_index ON orm_modcache USING btree ("modelNamespace");


--
-- Name: orm_modcache_timestamp_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX orm_modcache_timestamp_index ON orm_modcache USING btree ("timestamp");


--
-- Name: orm_modcache_userid_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX orm_modcache_userid_index ON orm_modcache USING btree ("userId");


--
-- Name: tasks_livetask_accessedbyuserid_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX tasks_livetask_accessedbyuserid_index ON tasks_livetask USING btree ("accessedByUserId");


--
-- Name: tasks_livetask_datecreated_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX tasks_livetask_datecreated_index ON tasks_livetask USING btree ("dateCreated");


--
-- Name: tasks_livetask_dateupdated_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX tasks_livetask_dateupdated_index ON tasks_livetask USING btree ("dateUpdated");


--
-- Name: tasks_livetask_ishidden_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX tasks_livetask_ishidden_index ON tasks_livetask USING btree ("isHidden");


--
-- Name: tasks_livetask_pid_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX tasks_livetask_pid_index ON tasks_livetask USING btree (pid);


--
-- Name: tasks_livetask_status_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX tasks_livetask_status_index ON tasks_livetask USING btree (status);
