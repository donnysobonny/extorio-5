//
// Resize event listener fix
//
(function () {
    var attachEvent = document.attachEvent;
    var isIE = navigator.userAgent.match(/Trident/);
    var requestFrame = (function () {
        var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame ||
            function (fn) {
                return window.setTimeout(fn, 20);
            };
        return function (fn) {
            return raf(fn);
        };
    })();

    var cancelFrame = (function () {
        var cancel = window.cancelAnimationFrame || window.mozCancelAnimationFrame || window.webkitCancelAnimationFrame ||
            window.clearTimeout;
        return function (id) {
            return cancel(id);
        };
    })();

    function resizeListener(e) {
        var win = e.target || e.srcElement;
        if (win.__resizeRAF__) cancelFrame(win.__resizeRAF__);
        win.__resizeRAF__ = requestFrame(function () {
            var trigger = win.__resizeTrigger__;
            trigger.__resizeListeners__.forEach(function (fn) {
                fn.call(trigger, e);
            });
        });
    }

    function objectLoad(e) {
        this.contentDocument.defaultView.__resizeTrigger__ = this.__resizeElement__;
        this.contentDocument.defaultView.addEventListener('resize', resizeListener);
    }

    window.addResizeListener = function (element, fn) {
        if (!element.__resizeListeners__) {
            element.__resizeListeners__ = [];
            if (attachEvent) {
                element.__resizeTrigger__ = element;
                element.attachEvent('onresize', resizeListener);
            }
            else {
                if (getComputedStyle(element).position == 'static') element.style.position = 'relative';
                var obj = element.__resizeTrigger__ = document.createElement('object');
                obj.setAttribute('style', 'display: block; position: absolute; top: 0; left: 0; height: 100%; width: 100%; overflow: hidden; pointer-events: none; z-index: -1;');
                obj.__resizeElement__ = element;
                obj.onload = objectLoad;
                obj.type = 'text/html';
                if (isIE) element.appendChild(obj);
                obj.data = 'about:blank';
                if (!isIE) element.appendChild(obj);
            }
        }
        element.__resizeListeners__.push(fn);
    };

    window.removeResizeListener = function (element, fn) {
        element.__resizeListeners__.splice(element.__resizeListeners__.indexOf(fn), 1);
        if (!element.__resizeListeners__.length) {
            if (attachEvent) element.detachEvent('onresize', resizeListener);
            else {
                element.__resizeTrigger__.contentDocument.defaultView.removeEventListener('resize', resizeListener);
                element.__resizeTrigger__ = !element.removeChild(element.__resizeTrigger__);
            }
        }
    }
})();

(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define('jquery', factory);
    }
    else {
        factory(jQuery);
    }
})(function ($) {
    'use strict';

    var LayoutRowDefaults = {
            collapsexs: false,
            collapsesm: true,
            collapsemd: false,
            collapselg: false,
            collapseall: false,
            align: "top",
            role: "row",
            spacing: true
        },
        LayoutRow = function (elem, layout) {
            this.layout = layout;
            this.container = elem;
            /**
             * @type {LayoutRow}
             */
            this.parentrow = undefined;
            this.menu = false;
            this.editable = elem.attr('data-ls-editable') == "true" ? true : false;
            this.options = $.extend({}, LayoutRowDefaults);
        },
        LayoutCellDefaults = {
            x: 0,
            width: 1,
            hidexs: false,
            hidesm: false,
            hidemd: false,
            hidelg: false,
            hideall: false,
            align: "inherit",
            role: "cell",
            blockid: 0,
            classes: "",
            styling: "",
            configurable: false
        },
        LayoutCell = function (elem, layout) {
            this.layout = layout;
            this.container = elem;
            /**
             * @type {LayoutRow}
             */
            this.parentrow = undefined;
            this.menu = false;
            this.editable = elem.attr('data-ls-editable') == "true" ? true : false;
            this.options = $.extend({}, LayoutCellDefaults);
        },
        LayoutInsertDefaults = {
            role: "cell",
            blockid: 0
        },
        LayoutInsert = function (elem) {
            this.container = elem;
            this.options = $.extend({}, LayoutInsertDefaults);
        };
    LayoutRow.prototype = {
        pull: function () {
            if (this.container) {
                var value;
                for (var k in this.options) {
                    value = this.container.attr("data-ls-" + k);
                    if (value !== undefined) {
                        //if numeric
                        if (!isNaN(parseFloat(value)) && isFinite(value)) {
                            this.options[k] = parseFloat(value);
                        }
                        //if bool
                        else if (value === "true") {
                            this.options[k] = true;
                        }
                        else if (value === "false") {
                            this.options[k] = false;
                        }
                        //must be a string
                        else {
                            this.options[k] = value;
                        }
                    }
                }
            }
        },
        push: function () {
            if (this.container) {
                for (var k in this.options) {
                    this.container.attr("data-ls-" + k, this.options[k]);
                }
            }
        },
        setup: function () {
            this.pull();
            this.push();
        },
        initOnce: function () {
            var self = this;
            this.pull();
            //create the placeholder
            this.container.append($('<div data-ls-editable="false" class="layout_cell_placeholder layout_cell">' +
                '   <div class="layout_cell_placeholder_content layout_cell_content_outer">' +
                '       <div class="layout_cell_content_inner"></div>' +
                '   </div>' +
                '</div>').hide());
            //manually setup the placeholder cell
            this.container.contents('.layout_cell_placeholder').each(function () {
                var data;
                $(this).data('layoutcell', (data = new LayoutCell($(this))));
                data.setup();
            });

            if (this.parentrow === undefined) {
                this.parentrow = this.container.parent('.layout_cell_content_inner').parent('.layout_cell_content_outer').parent('.layout_cell').parent('.layout_row').data('layoutrow');
            }

            if (this.editable) {
                //setup the menu
                var html = '';
                html += ' <div class="layout_edit_menu">';
                html += ' <div class="btn-group" role="group" aria-label="">';
                html += ' <button title="Row" type="button" class="btn btn-warning btn-xs layout_edit_menu_info">Row</button>';
                html += ' <button title="collapse on extra small screens" type="button" class="btn btn-default btn-xs layout_edit_menu_collapsexs"><span class="fa fa-mobile"></span></button>';
                html += ' <button title="collapse on small screens" type="button" class="btn btn-default btn-xs layout_edit_menu_collapsesm"><span class="fa fa-tablet"></span></button>';
                html += ' <button title="collapse on medium screens" type="button" class="btn btn-default btn-xs layout_edit_menu_collapsemd"><span class="fa fa-laptop"></span></button>';
                html += ' <button title="collapse on large screens" type="button" class="btn btn-default btn-xs layout_edit_menu_collapselg"><span class="fa fa-desktop"></span></button>';
                html += ' <button title="collapse on all screens" type="button" class="btn btn-default btn-xs layout_edit_menu_collapseall"><span class="fa fa-star"></span></button>';
                html += ' </div>';
                html += ' <div class="btn-group" role="group" aria-label="">';
                html += ' <button title="align cells top" type="button" class="btn btn-default btn-xs layout_edit_menu_aligntop"><span class="fa fa-long-arrow-up"></span></button>';
                html += ' <button title="align cells middle" type="button" class="btn btn-default btn-xs layout_edit_menu_alignmiddle"><span class="fa fa-arrows-v"></span></button>';
                html += ' <button title="align cells bottom" type="button" class="btn btn-default btn-xs layout_edit_menu_alignbottom"><span class="fa fa-long-arrow-down"></span></button>';
                html += ' </div>';
                html += ' <div class="btn-group" role="group" aria-label="">';
                html += ' <button title="toggle spacing" type="button" class="btn btn-default btn-xs layout_edit_menu_spacing"><span class="fa fa-square-o"></span></button>';
                html += ' </div>';
                html += ' <div class="btn-group" role="group" aria-label="">';
                html += ' <button title="remove row" type="button" class="btn btn-danger btn-xs layout_edit_menu_remove"><span class="fa fa-trash"></span></button>';
                html += ' </div>';
                html += ' </div>';

                this.container.append(html);
                this.menu = this.container.contents('.layout_edit_menu');

                //functionality
                this.menu.find('.layout_edit_menu_collapsexs').off().on("click", function () {
                    if (self.options.collapsexs) {
                        self.options.collapsexs = false;
                    } else {
                        self.options.collapsexs = true;
                    }
                    self.options.collapsesm = false;
                    self.options.collapsemd = false;
                    self.options.collapselg = false;
                    self.options.collapseall = false;
                    self.push();
                    self.update();
                });
                this.menu.find('.layout_edit_menu_collapsesm').off().on("click", function () {
                    if (self.options.collapsesm) {
                        self.options.collapsesm = false;
                    } else {
                        self.options.collapsesm = true;
                    }
                    self.options.collapsexs = false;
                    self.options.collapsemd = false;
                    self.options.collapselg = false;
                    self.options.collapseall = false;
                    self.push();
                    self.update();
                });
                this.menu.find('.layout_edit_menu_collapsemd').off().on("click", function () {
                    if (self.options.collapsemd) {
                        self.options.collapsemd = false;
                    } else {
                        self.options.collapsemd = true;
                    }
                    self.options.collapsexs = false;
                    self.options.collapsesm = false;
                    self.options.collapselg = false;
                    self.options.collapseall = false;
                    self.push();
                    self.update();
                });
                this.menu.find('.layout_edit_menu_collapselg').off().on("click", function () {
                    if (self.options.collapselg) {
                        self.options.collapselg = false;
                    } else {
                        self.options.collapselg = true;
                    }
                    self.options.collapsexs = false;
                    self.options.collapsesm = false;
                    self.options.collapsemd = false;
                    self.options.collapseall = false;
                    self.push();
                    self.update();
                });
                this.menu.find('.layout_edit_menu_collapseall').off().on("click", function () {
                    if (self.options.collapseall) {
                        self.options.collapseall = false;
                    } else {
                        self.options.collapseall = true;
                    }
                    self.options.collapsexs = false;
                    self.options.collapsesm = false;
                    self.options.collapsemd = false;
                    self.options.collapselg = false;
                    self.push();
                    self.update();
                });

                this.menu.find('.layout_edit_menu_aligntop').off().on("click", function () {
                    self.options.align = "top";
                    self.push();
                    self.alignCells();
                    self.update();
                });
                this.menu.find('.layout_edit_menu_alignmiddle').off().on("click", function () {
                    self.options.align = "middle";
                    self.push();
                    self.alignCells();
                    self.update();
                });
                this.menu.find('.layout_edit_menu_alignbottom').off().on("click", function () {
                    self.options.align = "bottom";
                    self.push();
                    self.alignCells();
                    self.update();
                });

                this.menu.find('.layout_edit_menu_spacing').off().on("click", function () {
                    if (self.options.spacing) {
                        self.options.spacing = false;
                    } else {
                        self.options.spacing = true;
                    }
                    self.push();
                    self.update();
                });

                this.menu.find('.layout_edit_menu_remove').off().on("click", function () {
                    //we can only remove the row when it has a parent cell
                    var parent = self.container.parent('.layout_cell_content_inner');
                    if (parent.length > 0) {
                        self.container.remove();
                        $.extorio_messageInfo("Row removed");
                    } else {
                        $.extorio_messageError("Unable to remove row as it is not contained within a cell");
                    }
                });

                //a row can take empty cells, blocks, and the content cell
                this.container.droppable({
                    accept: ".layout_insert",
                    greedy: true,
                    drop: function (event, ui) {
                        var _role = ui.draggable.attr('data-ls-role');
                        if (_role == "row") {
                            $.extorio_messageWarning("It's not possible to insert a row into another row. Rows are placed into cells.");
                            return;
                        }
                        var xAvail = {
                            0: true,
                            1: true,
                            2: true,
                            3: true,
                            4: true,
                            5: true,
                            6: true,
                            7: true,
                            8: true,
                            9: true,
                            10: true,
                            11: true
                        };
                        //work out the x avail
                        $(this).children(':not(.layout_cell_placeholder).layout_cell').each(function () {
                            var x = parseInt($(this).attr('data-ls-x'));
                            var width = parseInt($(this).attr('data-ls-width'));
                            var right = x + width;

                            while (x < right) {
                                xAvail[x] = false;
                                x++;
                            }
                        });
                        //knowing now the x avail, find the left-most x and width to place the dropped insert
                        var avail = false;
                        var availX, availW;
                        for (var i = 0; i < 12; i++) {
                            if (!avail) {
                                if (xAvail[i]) {
                                    avail = true;
                                    availX = i;
                                    availW = 1;
                                }
                            } else {
                                if (xAvail[i]) {
                                    availW++;
                                } else {
                                    break;
                                }
                            }
                        }
                        if (avail) {
                            var role = ui.draggable.attr('data-ls-role');
                            var cell;
                            switch (role) {
                                case "new" :
                                    cell = $('<div class="layout_cell" data-ls-role="block" data-ls-editable="true" data-ls-blockid="">' +
                                        '   <span class="layout_cell_block_label"></span>' +
                                        '   <div class="layout_cell_content_outer">' +
                                        '       <div class="layout_cell_content_inner"></div>' +
                                        '   </div>' +
                                        '</div>');


                                    $.extorio_modal({
                                        "title": "Create new block",
                                        "closetext": "Cancel",
                                        "size": "modal-lg",
                                        width: "96%",
                                        "content": '<div id="create_block_form">' +
                                        '   <div class="row">' +
                                        '       <div class="col-sm-3">' +
                                        '           <h3>Block</h3>' +
                                        '           <div class="form-group">' +
                                        '               <label for="create_block_name">Name</label>' +
                                        '               <input type="text" class="form-control input-sm" id="create_block_name" name="create_block_name" placeholder="Give your block a name...">' +
                                        '           </div>' +
                                        '           <div class="form-group">' +
                                        '               <label for="create_block_description">Description</label>' +
                                        '               <textarea placeholder="(optional) Describe this block..." class="form-control" id="create_block_description" name="create_block_description"></textarea>' +
                                        '           </div>' +
                                        '           <div class="form-group">' +
                                        '               <label for="create_block_category">Category</label>' +
                                        '               <select class="form-control input-sm" id="create_block_category" name="create_block_category"></select>' +
                                        '           </div>' +
                                        '           <div class="panel panel-default">' +
                                        '               <div class="panel-heading">' +
                                        '                   <h3 class="panel-title">Access</h3>' +
                                        '               </div>' +
                                        '               <div class="panel-body">' +
                                        '                   <div class="checkbox">' +
                                        '                       <label>' +
                                        '                           <input checked="checked" id="create_block_all_read" type="checkbox"> All users can view this block' +
                                        '                       </label>' +
                                        '                   </div>' +
                                        '                   <div style="display: none;" id="create_block_read_group_select_outer" class="form-group">' +
                                        '                       <label>Select the user groups that can view this block</label>' +
                                        '                       <div id="create_block_read_group_select"></div>' +
                                        '                   </div>' +
                                        '                   <div class="form-group">' +
                                        '                       <label>Select the user groups that can modify this block</label>' +
                                        '                       <div id="create_block_modify_group_select"></div>' +
                                        '                   </div>' +
                                        '               </div>' +
                                        '           </div>' +
                                        '       </div>' +
                                        '       <div class="col-sm-9">' +
                                        '           <h3>Select a processor</h3>' +
                                        '           <div class="form-group">' +
                                        '               <div class="row">' +
                                        '                   <div class="col-xs-6">' +
                                        '                       <div class="input-group">' +
                                        '                           <input type="text" class="form-control create_block_filter_search" placeholder="Search..." aria-describedby="sizing-addon1">' +
                                        '                           <span class="input-group-addon" id="sizing-addon1"><span class="fa fa-search"></span></span>' +
                                        '                       </div>' +
                                        '                   </div>' +
                                        '                   <div class="col-xs-6">' +
                                        '                       <select class="form-control create_block_filter_category"></select>' +
                                        '                   </div>' +
                                        '               </div><br />' +
                                        '               <div class="create_block_select_info"></div>' +
                                        '               <div class="create_block_select"></div>' +
                                        '               <input type="hidden" id="create_block_processor_id" name="create_block_processor_id" value="0" />' +
                                        '           </div>' +
                                        '       </div>' +
                                        '   </div>' +
                                        '</div>',
                                        "onopen": function () {
                                            var modal = this;
                                            modal.find('#create_block_all_read').on("change", function () {
                                                if ($(this).prop("checked")) {
                                                    $('#create_block_read_group_select_outer').hide();
                                                } else {
                                                    $('#create_block_read_group_select_outer').show();
                                                }
                                            });

                                            var selectedItem = null;
                                            var selectedId = 0;
                                            var loadProcessors = function () {
                                                var select = modal.find('.create_block_select');
                                                select.extorio_showLoader();
                                                var data = {};
                                                var search = modal.find('.create_block_filter_search').val();
                                                var categoryId = modal.find('.create_block_filter_category').val();
                                                if (search.length > 0) {
                                                    data.name = search;
                                                }
                                                if (categoryId != null) {
                                                    if (categoryId.length > 0) {
                                                        data.categoryId = categoryId;
                                                    }
                                                }
                                                $.extorio_api({
                                                    endpoint: "/block-processors/filter",
                                                    type: "GET",
                                                    data: data,
                                                    oncomplete: function () {
                                                        select.extorio_hideLoader();
                                                    },
                                                    onsuccess: function (response) {
                                                        select.html('');
                                                        var columns = 4;
                                                        var nextRow = 0;
                                                        var row = null;
                                                        if (response.data.length > 0) {
                                                            for (var i = 0; i <= response.data.length; i++) {
                                                                if (i < response.data.length) {
                                                                    var proc = response.data[i];
                                                                    if (i == nextRow) {
                                                                        if (row != null) {
                                                                            select.append(row);
                                                                        }
                                                                        row = $('<div class="row"></div>');
                                                                        nextRow += columns;
                                                                    }
                                                                    var style = selectedId == proc.id ? "success" : "info";
                                                                    var desc = proc.description.length > 0 ? proc.description : 'No description available.';
                                                                    var item = $('<div class="col-xs-' + (12 / columns) + '">' +
                                                                        '   <div id="create_block_item_' + proc.id + '" class="panel panel-' + style + '">' +
                                                                        '       <div style="font-size: 12px; padding: 5px 8px;" class="panel-heading">' +
                                                                        '           <h3 class="panel-title">' + proc.name + '</h3>' +
                                                                        '       </div>' +
                                                                        '       <div style="padding: 10px; font-size: 10px;" class="panel-body">' +
                                                                        '           <p><span class="label label-primary">' + proc.extensionName + '</span> : <span class="label label-info">' + proc.category.name + '</span></p>' +
                                                                        '           ' + desc + '<br /><p></p>' +
                                                                        '           <button data-parent="create_block_item_' + proc.id + '" data-id="' + proc.id + '" data-name="' + proc.name + '" class="btn btn-' + style + ' btn-xs btn-block create_block_select_item">select</button>' +
                                                                        '       </div>' +
                                                                        '   </div>' +
                                                                        '</div>');
                                                                    if (selectedId == proc.id) {
                                                                        selectedItem = item.find('.panel');
                                                                        selectedItem.removeClass('panel-info').addClass('panel-success');
                                                                        selectedItem.find('.create_block_select_item').removeClass('btn-info').addClass('btn-success');
                                                                    }

                                                                    item.find('.create_block_select_item').on("click", function () {
                                                                        var id = $(this).attr('data-id');
                                                                        var name = $(this).attr('data-name');
                                                                        var parent = $(this).attr('data-parent');
                                                                        if (selectedItem != null) {
                                                                            selectedItem.removeClass('panel-success').addClass('panel-info');
                                                                            selectedItem.find('.create_block_select_item').removeClass('btn-success').addClass('btn-info');
                                                                        }
                                                                        selectedItem = $('#' + parent);
                                                                        selectedItem.removeClass('panel-info').addClass('panel-success');
                                                                        selectedItem.find('.create_block_select_item').removeClass('btn-info').addClass('btn-success');
                                                                        selectedId = parseInt(id);
                                                                        modal.find('#create_block_processor_id').val(id);
                                                                        modal.find('.create_block_select_info').html('You have selected <span class="label label-success">' + name + '</span><br /><br />');
                                                                    });

                                                                    row.append(item);
                                                                } else {
                                                                    if (row != null) {
                                                                        select.append(row);
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            select.html("<p>No processors found.</p>");
                                                        }
                                                    }
                                                });
                                            };
                                            $.extorio_api({
                                                endpoint: "/block-processor-categories/filter",
                                                type: "GET",
                                                oncomplete: function () {
                                                },
                                                onsuccess: function (response) {
                                                    var select = modal.find('.create_block_filter_category');
                                                    select.html('');
                                                    select.append('<option value="">--select a category--</option>');
                                                    for (var i = 0; i < response.data.length; i++) {
                                                        select.append('<option value="' + response.data[i].id + '">' + response.data[i].name + '</option>');
                                                    }
                                                    loadProcessors();
                                                }
                                            });
                                            $.extorio_api({
                                                endpoint: "/block-categories/tree",
                                                type: "GET",
                                                oncomplete: function () {
                                                },
                                                onsuccess: function (response) {
                                                    var select = modal.find('#create_block_category');
                                                    select.html('');
                                                    for (var i = 0; i < response.data.length; i++) {
                                                        var main = response.data[i];
                                                        var group = $('<optgroup label="' + main.name + '"></optgroup>');
                                                        for (var j = 0; j < main.subCategories.length; j++) {
                                                            var sub = main.subCategories[j];
                                                            group.append('<option value="' + main.id + ':' + sub.id + '">' + main.name + ' / ' + sub.name + '</option>');
                                                        }
                                                        select.append(group);
                                                    }
                                                }
                                            });
                                            $.extorio_api({
                                                endpoint: "/user-groups/filter",
                                                type: "GET",
                                                oncomplete: function () {
                                                },
                                                onsuccess: function (response) {
                                                    var reads = $('#create_block_read_group_select');
                                                    var mods = $('#create_block_modify_group_select');
                                                    for (var i = 0; i < response.data.length; i++) {
                                                        var group = response.data[i];
                                                        reads.append('<div class="checkbox">' +
                                                            '   <label>' +
                                                            '       <input data-groupid="' + group.id + '" type="checkbox"> ' + group.name + ' <small>' + group.description + '</small>' +
                                                            '   </label>' +
                                                            '</div>');
                                                        if (group.id == 1 || group.id == 2) {
                                                            mods.append('<div class="checkbox">' +
                                                                '   <label>' +
                                                                '       <input checked="checked" data-groupid="' + group.id + '" type="checkbox"> ' + group.name + ' <small>' + group.description + '</small>' +
                                                                '   </label>' +
                                                                '</div>');
                                                        } else {
                                                            mods.append('<div class="checkbox">' +
                                                                '   <label>' +
                                                                '       <input data-groupid="' + group.id + '" type="checkbox"> ' + group.name + ' <small>' + group.description + '</small>' +
                                                                '   </label>' +
                                                                '</div>');
                                                        }
                                                    }
                                                }
                                            });

                                            modal.find('.create_block_filter_category').on("change", function () {
                                                loadProcessors();
                                            });

                                            var typeTime = new Date();
                                            modal.find('.create_block_filter_search').extorio_searchinput(function () {
                                                loadProcessors();
                                            });
                                        },
                                        "oncontinuebutton": function () {
                                            var modal = this;
                                            var b = {};
                                            b.isPublic = true;
                                            b.name = modal.find('#create_block_name').val();
                                            b.description = modal.find('#create_block_description').val();
                                            var catraw = modal.find('#create_block_category').val();
                                            var catparts = catraw.split(":");
                                            b.category = catparts[0];
                                            b.subCategory = catparts[1];
                                            b.allRead = modal.find('#create_block_all_read').prop("checked");
                                            b.readGroups = [];
                                            b.modifyGroups = [];
                                            modal.find('#create_block_read_group_select input').each(function () {
                                                if ($(this).prop("checked")) {
                                                    b.readGroups.push($(this).attr('data-groupid'));
                                                }
                                            });
                                            modal.find('#create_block_modify_group_select input').each(function () {
                                                if ($(this).prop("checked")) {
                                                    b.modifyGroups.push($(this).attr('data-groupid'));
                                                }
                                            });
                                            b.processor = modal.find('#create_block_processor_id').val();
                                            $.extorio_showFullPageLoader();
                                            $.extorio_api({
                                                endpoint: "/blocks",
                                                type: "POST",
                                                data: {
                                                    data: b
                                                },
                                                oncomplete: function () {
                                                    $.extorio_hideFullPageLoader();
                                                },
                                                onsuccess: function (response) {
                                                    var id = parseInt(response.data.id);
                                                    $.extorio_messageSuccess("Block successfully created");
                                                    cell.attr('data-ls-blockid', id);
                                                    $.extorio_api({
                                                        endpoint: "/blocks/" + id + "/can_modify",
                                                        type: "GET",
                                                        onsuccess: function (response2) {
                                                            if (response2.data) {
                                                                cell.attr('data-ls-configurable','true');
                                                            }
                                                            var lcell = cell.data('layoutcell');
                                                            lcell.pull();
                                                            lcell.update();
                                                            if (response2.data) {
                                                                cell.find('.layout_edit_menu_edit').trigger("click");
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                        },
                                        "onclosebutton": function () {
                                            cell.remove();
                                        }
                                    });
                                    break;
                                case "cell" :
                                    cell = $('<div class="layout_cell" data-ls-role="cell" data-ls-editable="true">' +
                                        '   <div class="layout_cell_content_outer">' +
                                        '       <div class="layout_cell_content_inner"></div>' +
                                        '   </div>' +
                                        '</div>');
                                    break;
                                case "block" :
                                    var blockid = ui.draggable.attr('data-ls-blockid');
                                    cell = $('<div class="layout_cell" data-ls-role="block" data-ls-editable="true" data-ls-blockid="' + blockid + '">' +
                                        '   <span class="layout_cell_block_label"></span>' +
                                        '   <div class="layout_cell_content_outer">' +
                                        '       <div class="layout_cell_content_inner"></div>' +
                                        '   </div>' +
                                        '</div>');
                                    //fetch the label
                                    cell.find('> .layout_cell_block_label').extorio_fetchBlockLabelContent(blockid);
                                    //fetch the block content
                                    cell.find('> .layout_cell_content_outer > .layout_cell_content_inner').extorio_fetchBlockViewContent(blockid);
                                    break;
                                case "content" :
                                    cell = $('<div class="layout_cell" data-ls-role="content" data-ls-editable="true">' +
                                        '   <div class="layout_cell_content_outer">' +
                                        '       <div class="layout_cell_content_inner">' +
                                        '           <div class="well well-lg">' +
                                        '               <h3>Content cell</h3>' +
                                        '               <p>The page layout/content will be displayed here</p>' +
                                        '           </div>' +
                                        '       </div>' +
                                        '   </div>' +
                                        '</div>');
                                    break;
                            }
                            var data;
                            cell.attr('data-ls-x', availX);
                            cell.attr('data-ls-width', availW);
                            self.container.append(cell);

                            //rebuild the layout
                            self.layout.layoutsystem();

                            self.fixHeight(true);
                            self.sortCells();
                            self.alignCells();
                            switch (_role) {
                                case "cell":
                                    $.extorio_messageInfo("Cell inserted");
                                    break;
                                case "block":
                                    $.extorio_messageInfo("Block inserted");
                                    //when a block is inserted, check whether we can modify
                                    var blockId = cell.attr("data-ls-blockid");
                                    $.extorio_api({
                                        endpoint: "/blocks/" + blockId + "/can_modify",
                                        type: "GET",
                                        onsuccess: function(resp) {
                                            if(resp.data) {
                                                cell.attr('data-ls-configurable','true');
                                                var lcell = cell.data("layoutcell");
                                                lcell.pull();
                                                lcell.update();
                                            }
                                        }
                                    });
                                    break;
                                case "content":
                                    $.extorio_messageInfo("Content cell inserted");
                                    break;
                                case "new":
                                    $.extorio_messageInfo("New block inserted");
                                    break;
                            }
                        } else {
                            $.extorio_messageWarning("There is not enough space in this cell to insert anything. Please make space within the cell first.");
                        }
                    }
                });
            }

            this.sortCells();
            this.alignCells();
            this.update();
            this.push();
        },
        update: function () {
            if (this.editable) {
                if (this.options.collapsexs) {
                    this.menu.find('.layout_edit_menu_collapsexs').addClass('btn-primary').removeClass('btn-default');
                } else {
                    this.menu.find('.layout_edit_menu_collapsexs').addClass('btn-default').removeClass('btn-primary');
                }
                if (this.options.collapsesm) {
                    this.menu.find('.layout_edit_menu_collapsesm').addClass('btn-primary').removeClass('btn-default');
                } else {
                    this.menu.find('.layout_edit_menu_collapsesm').addClass('btn-default').removeClass('btn-primary');
                }
                if (this.options.collapsemd) {
                    this.menu.find('.layout_edit_menu_collapsemd').addClass('btn-primary').removeClass('btn-default');
                } else {
                    this.menu.find('.layout_edit_menu_collapsemd').addClass('btn-default').removeClass('btn-primary');
                }
                if (this.options.collapselg) {
                    this.menu.find('.layout_edit_menu_collapselg').addClass('btn-primary').removeClass('btn-default');
                } else {
                    this.menu.find('.layout_edit_menu_collapselg').addClass('btn-default').removeClass('btn-primary');
                }
                if (this.options.collapseall) {
                    this.menu.find('.layout_edit_menu_collapseall').addClass('btn-primary').removeClass('btn-default');
                } else {
                    this.menu.find('.layout_edit_menu_collapseall').addClass('btn-default').removeClass('btn-primary');
                }

                if (this.options.align == "top") {
                    this.menu.find('.layout_edit_menu_aligntop').addClass('btn-primary').removeClass('btn-default');
                    this.menu.find('.layout_edit_menu_alignmiddle').addClass('btn-default').removeClass('btn-primary');
                    this.menu.find('.layout_edit_menu_alignbottom').addClass('btn-default').removeClass('btn-primary');
                }
                if (this.options.align == "middle") {
                    this.menu.find('.layout_edit_menu_aligntop').addClass('btn-default').removeClass('btn-primary');
                    this.menu.find('.layout_edit_menu_alignmiddle').addClass('btn-primary').removeClass('btn-default');
                    this.menu.find('.layout_edit_menu_alignbottom').addClass('btn-default').removeClass('btn-primary');
                }
                if (this.options.align == "bottom") {
                    this.menu.find('.layout_edit_menu_aligntop').addClass('btn-default').removeClass('btn-primary');
                    this.menu.find('.layout_edit_menu_alignmiddle').addClass('btn-default').removeClass('btn-primary');
                    this.menu.find('.layout_edit_menu_alignbottom').addClass('btn-primary').removeClass('btn-default');
                }

                if (this.options.spacing) {
                    this.menu.find('.layout_edit_menu_spacing').addClass('btn-primary').removeClass('btn-default');
                } else {
                    this.menu.find('.layout_edit_menu_spacing').addClass('btn-default').removeClass('btn-primary');
                }
            }
        },
        fixHeight: function (recursive) {
            var self = this;
            var maxHeight = 0;
            this.container.find(':not(.layout_cell_placeholder).layout_cell').each(function () {
                var height = $(this).outerHeight(true);
                if (height > maxHeight) {
                    maxHeight = height;
                }
            });
            if (maxHeight == 0) {
                this.container.height("auto");
            } else {
                this.container.height(maxHeight);
            }

            //if recursive
            if (recursive === true) {
                if (this.parentrow !== undefined) {
                    this.parentrow.fixHeight();
                }
            }
        },
        alignCells: function () {
            var self = this;
            this.container.children(":not(.layout_cell_placeholder).layout_cell[data-ls-align='inherit']").each(function () {
                var cell = $(this);
                switch (self.options.align) {
                    case "top" :
                        cell.css("top", 0).css("bottom", "auto");
                        break;
                    case "middle" :
                        cell.css("top", (self.container.outerHeight(false) - cell.outerHeight(true)) / 2).css("bottom", "auto");
                        break;
                    case "bottom" :
                        cell.css("top", "auto").css("bottom", 0);
                        break;
                }
            });
        },
        sortCells: function () {
            var self = this;
            var childCells = self.container.children(":not(.layout_cell_placeholder).layout_cell");
            //sort the cells by their x value
            childCells.sort(function (a, b) {
                var an = parseInt(a.getAttribute('data-ls-x')),
                    bn = parseInt(b.getAttribute('data-ls-x'));

                if (an > bn) {
                    return 1;
                }
                if (an < bn) {
                    return -1;
                }
                return 0;
            });
            childCells.detach().appendTo(self.container);
        }
    };
    LayoutCell.prototype = {
        pull: function () {
            if (this.container) {
                var value;
                for (var k in this.options) {
                    value = this.container.attr("data-ls-" + k);
                    if (value !== undefined) {
                        //if numeric
                        if (!isNaN(parseFloat(value)) && isFinite(value)) {
                            this.options[k] = parseFloat(value);
                        }
                        //if bool
                        else if (value === "true") {
                            this.options[k] = true;
                        }
                        else if (value === "false") {
                            this.options[k] = false;
                        }
                        //must be a string
                        else {
                            this.options[k] = value;
                        }
                    }
                }
            }
        },
        push: function () {
            if (this.container) {
                for (var k in this.options) {
                    this.container.attr("data-ls-" + k, this.options[k]);
                }
            }
        },
        setup: function () {
            this.pull();
            this.push();
        },
        initOnce: function () {
            var self = this;
            this.pull();

            if (this.parentrow === undefined) {
                this.parentrow = this.container.parent('.layout_row').data('layoutrow');
            }

            if (this.editable) {
                var html = '';
                html += ' <div class="layout_edit_menu">';
                html += ' <div class="btn-group" role="group" aria-label="">';
                html += ' <button title="Cell" type="button" class="btn btn-warning btn-xs layout_edit_menu_info">Cell</button>';
                html += ' <button title="hide cell on extra small screens" type="button" class="btn btn-default btn-xs layout_edit_menu_hidexs"><span class="fa fa-mobile"></span></button>';
                html += ' <button title="hide cell on small screens" type="button" class="btn btn-default btn-xs layout_edit_menu_hidesm"><span class="fa fa-tablet"></span></button>';
                html += ' <button title="hide cell on medium screens" type="button" class="btn btn-default btn-xs layout_edit_menu_hidemd"><span class="fa fa-laptop"></span></button>';
                html += ' <button title="hide cell on large screens" type="button" class="btn btn-default btn-xs layout_edit_menu_hidelg"><span class="fa fa-desktop"></span></button>';
                html += ' <button title="hide cell on all screens" type="button" class="btn btn-default btn-xs layout_edit_menu_hideall"><span class="fa fa-star"></span></button>';
                html += ' </div>';
                html += ' <div class="btn-group" role="group" aria-label="">';
                html += ' <button title="align cell top" type="button" class="btn btn-default btn-xs layout_edit_menu_aligntop"><span class="fa fa-long-arrow-up"></span></button>';
                html += ' <button title="align cell middle" type="button" class="btn btn-default btn-xs layout_edit_menu_alignmiddle"><span class="fa fa-arrows-v"></span></button>';
                html += ' <button title="align cell bottom" type="button" class="btn btn-default btn-xs layout_edit_menu_alignbottom"><span class="fa fa-long-arrow-down"></span></button>';
                html += ' </div>';
                if (this.options.role == "block") {
                    html += ' <div class="btn-group" role="group" aria-label="">';
                    html += ' <button disabled="disabled" title="block configuration" type="button" class="btn btn-info btn-xs disbled layout_edit_menu_edit"><span class="fa fa-cog"></span> B</button>';
                    html += ' </div>';
                }
                html += ' <div class="btn-group" role="group" aria-label="">';
                html += ' <button title="cell configuration" type="button" class="btn btn-info btn-xs layout_edit_menu_advanced"><span class="fa fa-cog"></span> C</button>';
                html += ' </div>';
                html += ' <div class="btn-group" role="group" aria-label="">';
                html += ' <button title="remove cell" type="button" class="btn btn-danger btn-xs layout_edit_menu_remove"><span class="fa fa-trash"></span></button>';
                html += ' </div>';
                html += ' </div>';
                html += ' <div title="resize" class="layout_edit_menu_resizeleft ui-resizable-handle ui-resizable-w"></div>';
                html += ' <div title="resize" class="layout_edit_menu_resizeright ui-resizable-handle ui-resizable-e"></div>';

                //making the cell sortable allows us to move rows up and down really easily
                this.container.find('> .layout_cell_content_outer > .layout_cell_content_inner').sortable();

                this.container.append(html);
                this.menu = this.container.contents('.layout_edit_menu');

                this.menu.find('.layout_edit_menu_edit').off().on("click", function () {
                    $.extorio_modal({
                        size: "modal-lg",
                        width: "96%",
                        title: "Block configuration",
                        content: '',
                        closetext: "Cancel",
                        continuetext: "Save",
                        oncontinuebutton: function () {
                            this.find("form#extorio_block_edit_form").submit(function (e) {
                                //serialize the form. This will be the new config value of the block
                                var config = $(this).serialize();

                                //save the config
                                $.extorio_api({
                                    endpoint: "/blocks/" + self.options.blockid,
                                    type: "POST",
                                    data: {
                                        data: {
                                            config: config
                                        }
                                    },
                                    onsuccess: function(resp) {
                                        //update visible blocks with this id
                                        $('.layout_cell[data-ls-blockid="' + self.options.blockid + '"] > .layout_cell_content_outer > .layout_cell_content_inner').extorio_fetchBlockViewContent(self.options.blockid);
                                        $.extorio_messageSuccess("Block updated successfully");
                                    }
                                });

                                e.preventDefault();
                            }).submit();
                        }
                    }).find('.modal-body').extorio_fetchBlockEditContent(self.options.blockid);
                });

                this.menu.find('.layout_edit_menu_advanced').off().on("click", function () {
                    $.extorio_modal({
                        title: "Cell configuration",
                        closetext: "Cancel",
                        continuetext: "Save",
                        content: '' +
                        '<div class="form-group">' +
                        '   <input value="' + self.options.classes + '" type="text" class="form-control" id="classes" name="classes" placeholder="Classes">' +
                        '</div>' +
                        '<div class="form-group">' +
                        '   <textarea class="form-control" id="styling" name="styling" placeholder="Styling">' + self.options.styling + '</textarea>' +
                        '</div>',
                        oncontinuebutton: function () {
                            var modal = this;
                            self.options.classes = modal.find('#classes').val();
                            self.options.styling = modal.find('#styling').val();
                            self.push();
                            self.updateClasses();
                            self.updateStyling();
                            if (self.parentrow !== undefined) {
                                self.parentrow.fixHeight(true);
                            }
                        }
                    });
                });

                this.menu.find('.layout_edit_menu_hidexs').off().on("click", function () {
                    if (self.options.hidexs) {
                        self.options.hidexs = false;
                    } else {
                        self.options.hidexs = true;
                    }
                    self.options.hidesm = false;
                    self.options.hidemd = false;
                    self.options.hidelg = false;
                    self.options.hideall = false;
                    self.push();
                    self.update();
                });
                this.menu.find('.layout_edit_menu_hidesm').off().on("click", function () {
                    if (self.options.hidesm) {
                        self.options.hidesm = false;
                    } else {
                        self.options.hidesm = true;
                    }
                    self.options.hidexs = false;
                    self.options.hidemd = false;
                    self.options.hidelg = false;
                    self.options.hideall = false;
                    self.push();
                    self.update();
                });
                this.menu.find('.layout_edit_menu_hidemd').off().on("click", function () {
                    if (self.options.hidemd) {
                        self.options.hidemd = false;
                    } else {
                        self.options.hidemd = true;
                    }
                    self.options.hidexs = false;
                    self.options.hidesm = false;
                    self.options.hidelg = false;
                    self.options.hideall = false;
                    self.push();
                    self.update();
                });
                this.menu.find('.layout_edit_menu_hidelg').off().on("click", function () {
                    if (self.options.hidelg) {
                        self.options.hidelg = false;
                    } else {
                        self.options.hidelg = true;
                    }
                    self.options.hidexs = false;
                    self.options.hidesm = false;
                    self.options.hidemd = false;
                    self.options.hideall = false;
                    self.push();
                    self.update();
                });
                this.menu.find('.layout_edit_menu_hideall').off().on("click", function () {
                    if (self.options.hideall) {
                        self.options.hideall = false;
                    } else {
                        self.options.hideall = true;
                    }
                    self.options.hidexs = false;
                    self.options.hidesm = false;
                    self.options.hidemd = false;
                    self.options.hidelg = false;
                    self.push();
                    self.update();
                });

                this.menu.find('.layout_edit_menu_aligntop').off().on("click", function () {
                    if (self.options.align == "top") {
                        self.options.align = "inherit";
                    } else {
                        self.options.align = "top";
                    }
                    self.push();
                    self.alignCell();
                    self.update();
                });
                this.menu.find('.layout_edit_menu_alignmiddle').off().on("click", function () {
                    if (self.options.align == "middle") {
                        self.options.align = "inherit";
                    } else {
                        self.options.align = "middle";
                    }
                    self.push();
                    self.alignCell();
                    self.update();
                });
                this.menu.find('.layout_edit_menu_alignbottom').off().on("click", function () {
                    if (self.options.align == "bottom") {
                        self.options.align = "inherit";
                    } else {
                        self.options.align = "bottom";
                    }
                    self.push();
                    self.alignCell();
                    self.update();
                });

                this.menu.find('.layout_edit_menu_remove').off().on("click", function () {
                    var parentrow = self.parentrow;
                    self.container.remove();
                    switch (self.options.role) {
                        case "cell" :
                            $.extorio_messageInfo("Cell removed");
                            break;
                        case "block" :
                            $.extorio_messageInfo("Block removed");
                            break;
                        case "content" :
                            $.extorio_messageInfo("Content cell removed");
                            break;
                        case "new" :
                            $.extorio_messageInfo("New cell removed");
                            break;
                    }
                    if (parentrow !== undefined) {
                        parentrow.fixHeight(true);
                    }
                });

                //a cell can only be droppable, blocks cannot be.
                //a cell can only take rows
                if (this.options.role === "cell") {
                    this.container.droppable({
                        accept: ".layout_insert[data-ls-role='row']",
                        greedy: true,
                        drop: function (event, ui) {
                            var role = ui.draggable.attr('data-ls-role');
                            if (role == "row") {
                                var row = $('<div class="layout_row" data-ls-editable="true" data-ls-role="row"></div>');
                                self.container.find('> .layout_cell_content_outer > .layout_cell_content_inner').append(row);
                                //re build the layout
                                self.layout.layoutsystem();
                                //fix height
                                self.layout.layoutsystem("fixHeight");
                                $.extorio_messageInfo("Row added");
                            } else {
                                $.extorio_messageWarning("It is not possible to insert a " + role + " into a cell.");
                            }
                        }
                    });
                }

                //cannot move if not in a row
                var minW;
                if (this.parentrow !== undefined) {
                    var placeholderCell = this.parentrow.container.contents('.layout_cell_placeholder').data('layoutcell');
                    var onStartMoving = function (event, ui) {
                        minW = Math.ceil(self.parentrow.container.width() / 12);
                        placeholderCell.options.x = self.options.x;
                        placeholderCell.options.width = self.options.width;
                        placeholderCell.push();
                        placeholderCell.container.show();
                    };
                    var onStopMoving = function (event, ui) {
                        placeholderCell.container.hide();
                        self.options.x = placeholderCell.options.x;
                        self.options.width = placeholderCell.options.width;
                        self.container.removeAttr("style");
                        self.push();
                        self.alignCell();

                        self.parentrow.sortCells();
                    };

                    //resize
                    this.container.resizable({
                        handles: {
                            'e': this.container.contents('.layout_edit_menu_resizeleft')[0],
                            'w': this.container.contents('.layout_edit_menu_resizeright')[0]
                        },
                        start: onStartMoving,
                        stop: onStopMoving,
                        minWidth: minW,
                        resize: function (event, ui) {
                            //fix the width
                            self.container.width(ui.size.width);

                            var left = Math.round(ui.position.left / minW);
                            //take into consideration the padding for the handles
                            var width = Math.round((ui.size.width + 32) / minW);
                            var right = left + width;
                            var canMove = true;
                            if (left < 0) {
                                canMove = false;
                            }
                            if (right > 12) {
                                canMove = false;
                            }
                            if (canMove) {
                                self.parentrow.container.children(":not(.layout_cell_placeholder):not(.ui-resizable-resizing).layout_cell").each(function () {
                                    if (canMove) {
                                        var tleft = parseInt($(this).attr('data-ls-x'));
                                        var twidth = parseInt($(this).attr('data-ls-width'));
                                        var tright = tleft + twidth;
                                        //if cell is on the left
                                        if (tright > left && tleft <= left) {
                                            canMove = false;
                                        }
                                        //if cell is on the right
                                        if (tleft < right && tright >= right) {
                                            canMove = false;
                                        }
                                    }
                                });
                            }
                            if (canMove) {
                                placeholderCell.options.x = left;
                                placeholderCell.options.width = width;
                                placeholderCell.push();
                            }
                        }
                    });

                    //move
                    this.container.draggable({
                        handle: this.menu,
                        scroll: false,
                        appendTo: 'body',
                        start: onStartMoving,
                        stop: onStopMoving,
                        drag: function (event, ui) {
                            var left = Math.round(ui.position.left / minW);
                            var width = parseInt($(this).attr('data-ls-width'));
                            var right = left + width;
                            var canMove = true;
                            if (left < 0) {
                                canMove = false;
                            }
                            if (right > 12) {
                                canMove = false;
                            }
                            if (canMove) {
                                self.parentrow.container.children(":not(.layout_cell_placeholder):not(.ui-draggable-dragging).layout_cell").each(function () {
                                    if (canMove) {
                                        var tleft = parseInt($(this).attr('data-ls-x'));
                                        var twidth = parseInt($(this).attr('data-ls-width'));
                                        var tright = tleft + twidth;
                                        //if cell is on the left
                                        if (tright > left && tleft <= left) {
                                            canMove = false;
                                        }
                                        //if cell is on the right
                                        if (tleft < right && tright >= right) {
                                            canMove = false;
                                        }
                                    }
                                });
                            }
                            if (canMove) {
                                placeholderCell.options.x = left;
                                placeholderCell.options.width = width;
                                placeholderCell.push();
                            }
                        },
                        containment: null
                    });
                }
            }

            //when the cell is resized, call fixHeight on the parent row (recursively)
            if (this.container[0] !== undefined && this.parentrow !== undefined) {
                addResizeListener(this.container[0], function () {
                    self.parentrow.fixHeight(true);
                });
            }

            this.updateClasses();
            this.updateStyling();
            this.alignCell();
            this.update();
            this.push();
        },
        updateClasses: function (fixHeight) {
            var classes = "layout_cell_content_inner ";
            var inner = this.container.find('> .layout_cell_content_outer > .layout_cell_content_inner');
            if (inner.hasClass("ui-sortable")) {
                classes += "ui-sortable ";
            }
            classes += this.options.classes;

            inner.removeAttr("class").addClass(classes);
        },
        updateStyling: function (fixHeight) {
            this.container.find('> .layout_cell_content_outer > .layout_cell_content_inner').attr("style", this.options.styling);
        },
        update: function () {
            if (this.editable) {
                if(this.options.role == "block") {
                    if(this.options.configurable) {
                        this.menu.find('.layout_edit_menu_edit').removeClass("disabled").removeAttr("disabled");
                    } else {
                        this.menu.find('.layout_edit_menu_edit').addClass("disabled").attr("disabled","disabled");
                    }
                }

                if (this.options.hidexs) {
                    this.menu.find('.layout_edit_menu_hidexs').addClass('btn-info').removeClass('btn-default');
                } else {
                    this.menu.find('.layout_edit_menu_hidexs').addClass('btn-default').removeClass('btn-info');
                }
                if (this.options.hidesm) {
                    this.menu.find('.layout_edit_menu_hidesm').addClass('btn-info').removeClass('btn-default');
                } else {
                    this.menu.find('.layout_edit_menu_hidesm').addClass('btn-default').removeClass('btn-info');
                }
                if (this.options.hidemd) {
                    this.menu.find('.layout_edit_menu_hidemd').addClass('btn-info').removeClass('btn-default');
                } else {
                    this.menu.find('.layout_edit_menu_hidemd').addClass('btn-default').removeClass('btn-info');
                }
                if (this.options.hidelg) {
                    this.menu.find('.layout_edit_menu_hidelg').addClass('btn-info').removeClass('btn-default');
                } else {
                    this.menu.find('.layout_edit_menu_hidelg').addClass('btn-default').removeClass('btn-info');
                }
                if (this.options.hideall) {
                    this.menu.find('.layout_edit_menu_hideall').addClass('btn-info').removeClass('btn-default');
                } else {
                    this.menu.find('.layout_edit_menu_hideall').addClass('btn-default').removeClass('btn-info');
                }

                if (this.options.align == "top") {
                    this.menu.find('.layout_edit_menu_aligntop').addClass('btn-info').removeClass('btn-default');
                    this.menu.find('.layout_edit_menu_alignmiddle').addClass('btn-default').removeClass('btn-info');
                    this.menu.find('.layout_edit_menu_alignbottom').addClass('btn-default').removeClass('btn-info');
                }
                if (this.options.align == "middle") {
                    this.menu.find('.layout_edit_menu_aligntop').addClass('btn-default').removeClass('btn-info');
                    this.menu.find('.layout_edit_menu_alignmiddle').addClass('btn-info').removeClass('btn-default');
                    this.menu.find('.layout_edit_menu_alignbottom').addClass('btn-default').removeClass('btn-info');
                }
                if (this.options.align == "bottom") {
                    this.menu.find('.layout_edit_menu_aligntop').addClass('btn-default').removeClass('btn-info');
                    this.menu.find('.layout_edit_menu_alignmiddle').addClass('btn-default').removeClass('btn-info');
                    this.menu.find('.layout_edit_menu_alignbottom').addClass('btn-info').removeClass('btn-default');
                }
            }
        },
        alignCell: function () {
            var align;
            if (this.parentrow !== undefined) {
                if (this.options.align === "inherit") {
                    align = this.parentrow.options.align;
                } else {
                    align = this.options.align;
                }
                switch (align) {
                    case "top" :
                        this.container.css("top", 0).css("bottom", "auto");
                        break;
                    case "middle" :
                        this.container.css("top", (this.parentrow.container.outerHeight(false) - this.container.outerHeight(true)) / 2).css("bottom", "auto");
                        break;
                    case "bottom" :
                        this.container.css("top", "auto").css("bottom", 0);
                        break;
                }
            }
        }
    };

    LayoutInsert.prototype = {
        pull: function () {
            if (this.container) {
                var value;
                for (var k in this.options) {
                    value = this.container.attr("data-ls-" + k);
                    if (value !== undefined) {
                        //if numeric
                        if (!isNaN(parseFloat(value)) && isFinite(value)) {
                            this.options[k] = parseFloat(value);
                        }
                        //if bool
                        else if (value === "true") {
                            this.options[k] = true;
                        }
                        else if (value === "false") {
                            this.options[k] = false;
                        }
                        //must be a string
                        else {
                            this.options[k] = value;
                        }
                    }
                }
            }
        },
        push: function () {
            if (this.container) {
                for (var k in this.options) {
                    this.container.attr("data-ls-" + k, this.options[k]);
                }
            }
        },
        setup: function () {
            this.pull();
            this.push();
        },
        initOnce: function () {
            var self = this;
            var helper = "";
            switch (this.options.role) {
                case "new" :
                    helper = "Drop this element on a row";
                    break;
                case "row" :
                    helper = "Drop this row on a cell";
                    break;
                case "cell" :
                    helper = "Drop this cell on a row";
                    break;
                case "block" :
                    helper = "Drop this block on a row";
                    break;
                case "content" :
                    helper = "Drop this cell on a row";
                    break;
            }
            this.container.draggable({
                appendTo: "body",
                cursorAt: {top: -5, left: -5},
                cursorType: "move",
                helper: function (event) {
                    return $("<strong>" + helper + "</strong>");
                },
                opacity: 0.7
            });
        }
    };

    $.fn.layoutsystem = function (func) {
        return this.each(function () {
            var layout = this;

            /**
             * @type {LayoutRow[]}
             */
            var rows = [];
            /**
             * @type {LayoutCell[]}
             */
            var cells = [];
            var i = 0;

            if (func === undefined || func === "setup") {
                $(layout).find('.layout_row').each(function () {
                    var data = $(this).data('layoutrow');
                    if (!data) {
                        $(this).data('layoutrow', (data = new LayoutRow($(this), $(layout))));
                        data.setup();
                        data.initOnce();
                        data.update();
                    }
                    rows.push(data);
                });
                $(layout).find('.layout_cell').each(function () {
                    var data = $(this).data('layoutcell');
                    if (!data) {
                        $(this).data('layoutcell', (data = new LayoutCell($(this), $(layout))));
                        data.setup();
                        data.initOnce();
                        data.update();
                    }
                    cells.push(data);
                });

                $(layout).data("rows", rows);
                $(layout).data("cells", cells);
            } else {
                switch (func) {
                    case "update" :
                        rows = $(layout).data("rows") || {};
                        cells = $(layout).data("cells") || {};

                        for (i = 0; i < cells.length; i++) {
                            cells[i].update();
                        }

                        for (i = 0; i < rows.length; i++) {
                            rows[i].update();
                        }
                        break;
                    case "fixHeight" :
                        rows = $(layout).data("rows") || {};

                        for (i = 0; i < rows.length; i++) {
                            rows[i].fixHeight(true);
                        }
                        break;
                }
            }
        });
    };

    $.fn.layoutsystem__toObject = function () {
        var object = {};
        object.rows = [];
        object.cells = [];
        var rows = [];
        var row = {};
        var cells = [];
        var cell = {};
        var i = 0;
        //if we find a direct row, we are working on the first row
        rows = this.contents('.layout_row');
        if (rows.length > 0) {
            i = 0;
            rows.each(function () {
                row = $(this).data('layoutrow');
                object.rows[i] = row.options;
                object.rows[i].cells = [];
                object.rows[i].cells = $(this).layoutsystem__toObject().cells;
                i++;
            });
        } else {
            //if we find direct cells, we are working on a row
            cells = this.contents('.layout_cell:not(.layout_cell_placeholder)');
            if (cells.length > 0) {
                i = 0;
                cells.each(function () {
                    cell = $(this).data('layoutcell');
                    object.cells[i] = cell.options;
                    object.cells[i].rows = [];
                    object.cells[i].rows = $(this).layoutsystem__toObject().rows;
                    i++;
                });
            } else {
                //we're finding rows inside of a cell
                rows = this.find('> .layout_cell_content_outer > .layout_cell_content_inner > .layout_row');
                if (rows.length > 0) {
                    i = 0;
                    rows.each(function () {
                        row = $(this).data('layoutrow');
                        object.rows[i] = row.options;
                        object.rows[i].cells = [];
                        object.rows[i].cells = $(this).layoutsystem__toObject().cells;
                        i++;
                    });
                }
            }
        }
        return object;
    };

    $.fn.layoutinsert = function () {
        return this.each(function () {
            var data = $(this).data('layoutinsert');
            if (!data) {
                $(this).data('layoutinsert', (data = new LayoutInsert($(this))));
                data.setup();
            }
            data.initOnce();
        });
    };
});