(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define('jquery', factory);
    }
    else {
        factory(jQuery);
    }
})(function ($) {
    'use strict';
    var SelectSearchDefalts = {
        endpoint: null,
        data: {},
        btnclass: "btn btn-default",
        value: null,
        display: null,
        values: []
    };
    var SelectSearch = function (select, options) {
        this.select = select;
        this.dropdown = null;
        this.options = $.extend(SelectSearchDefalts, options);

        if(this.options.endpoint == null || this.options.endpoint.length <= 0) {
            throw new DOMException("A SelectSeach object must specify the options.endpoint");
        }
        
        var self = this;

        this.dropdown = $('<div class="extorio_searchselect dropdown">' +
            '   <button class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">' +
            '       <span class="extorio_selectsearch_display pull-left">test</span> <span class="caret"></span>' +
            '   </button>' +
            '   <ul class="dropdown-menu extorio_searchselect_menu">' +
            '       <li class="extorio_searchselect_item">' +
            '           <div class="input-group input-group-sm">' +
            '               <input type="text" class="form-control extorio_searchselect_search" placeholder="Search...">' +
            '               <span class="input-group-addon"><span class="fa fa-search"></span></span>' +
            '           </div>' +
            '       </li>' +
            '       <li><p>Enter a search term above...</p></li>' +
            '   </ul>' +
            '</div>');
        this.dropdown.find('.dropdown-toggle').addClass(this.options.btnclass);

        this.dropdown.find('.extorio_searchselect_search').extorio_searchinput(function (val) {
            $.extorio_api({
                endpoint: self.options.endpoint,
                type: "GET",
                data: $.extend({
                    phrase: val
                },self.options.data),
                onerror: function () {
                    self.options.values = {};
                    self.populate();
                },
                onsuccess: function (resp) {
                    self.options.values = resp.data;
                    self.populate();
                }
            });
        });

        this.dropdown.insertAfter(this.select);

        this.display();
    }
    SelectSearch.prototype = {
        removeItems: function () {
            this.dropdown.find('.extorio_searchselect_menu li:not(.extorio_searchselect_item)').each(function () {
                $(this).remove();
            });
            this.select.html('');
        },
        getSeachValue: function () {
            return this.dropdown.find('.extorio_searchselect_search').val();
        },
        populate: function () {
            this.removeItems();
            var self = this;
            var menu = this.dropdown.find('.extorio_searchselect_menu');
            var found = false;
            for(var k in this.options.values) {
                found = true;
                this.select.append('<option value="'+k+'">'+this.options.values[k]+'</option>');
                if(k == this.options.value) {
                    menu.append('<li class="active"><a class="extorio_searchselect_select" data-value="'+k+'" href="javascript:;">'+this.options.values[k]+'</a></li>');
                } else {
                    menu.append('<li><a class="extorio_searchselect_select" data-value="'+k+'" href="javascript:;">'+this.options.values[k]+'</a></li>');
                }
            }
            if(found) {
                menu.find('.extorio_searchselect_select').on("click", function () {
                    self.options.value = $(this).attr('data-value');
                    self.options.display = self.options.values[self.options.value];
                    self.select.val(self.options.value);
                    self.select.trigger("change");
                    self.display();
                    self.populate();
                });
            } else {
                menu.append('<li><p>No results found</p></li>');
            }
        },
        display: function () {
            if(this.options.display != null && this.options.display.length > 0) {
                this.dropdown.find('.extorio_selectsearch_display').text(this.options.display);
            } else {
                this.dropdown.find('.extorio_selectsearch_display').text("Nothing selected");
            }
        }
    }

    $.fn.extorio_selectsearch = function (options) {
        return this.each(function () {
            $(this).hide();
            var data = $(this).data('extorio_selectsearch');
            if(!data) {
                $(this).data('extorio_selectsearch',new SelectSearch($(this),options));
            }
        });
    }
});