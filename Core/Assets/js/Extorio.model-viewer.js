//load the list view of a model
$.fn.extorio_modelViewer_list = function (model, options) {
    if (typeof options === "undefined") {
        options = {};
    }
    return this.each(function () {
        var that = $(this);
        that.extorio_showLoader();
        $.extorio_api({
            endpoint: "/model-viewer/listview/" + model,
            type: "GET",
            data: options,
            oncomplete: function() {
                that.extorio_hideLoader();
            },
            onsuccess: function(data) {
                that.html(data.html);
            }
        });
    });
};
//load the list view into a dialog
$.extorio_modelViewer_list_dialog = function (model, options) {
    return $.extorio_dialog({
        size: "modal-lg",
        title: "Viewing list: " + model,
        content: ""
    }).find('.modal-body').extorio_modelViewer_list(model, options);
};

//load the detail view of a model
$.fn.extorio_modelViewer_detail = function (model, id) {
    return this.each(function () {
        var that = $(this);
        that.extorio_showLoader();
        $.extorio_api({
            endpoint: "/model-viewer/detailview/" + model + "/" + id,
            type: "GET",
            oncomplete: function() {
                that.extorio_hideLoader();
            },
            onsuccess: function(data) {
                that.html(data.html);
            }
        });
    });
};
//load the detail view into a dialog
$.extorio_modelViewer_detail_dialog = function (model, id) {
    $.extorio_dialog({
        size: "modal-lg",
        title: "Viewing detail: " + model,
        content: ""
    }).find('.modal-body').extorio_modelViewer_detail(model, id);
};

//load the edit view of a model
$.fn.extorio_modelViewer_edit = function (model, id, options) {
    if (typeof options === "undefined") {
        options = {};
    }
    return this.each(function () {
        console.log("test");
        var that = $(this);
        that.extorio_showLoader();
        var url = "/model-viewer/editview/" + model;
        if (typeof id !== "undefined") {
            url += "/" + id;
        }
        $.extorio_api({
            endpoint: url,
            type: "GET",
            data: options,
            oncomplete: function() {
                that.extorio_hideLoader();
            },
            onsuccess: function(data) {
                var html = $(data.html);
                html.find('.model_view_table_checkbox_display').each(function () {
                    $(this).on('change', function () {
                        var checked = $(this).prop("checked");
                        if (checked) {
                            $('#' + $(this).attr('data-target')).val('true');
                        } else {
                            $('#' + $(this).attr('data-target')).val('false');
                        }
                    });
                });
                html.find('.model_view_table_number').on('keypress', function (ev) {
                    var keyCode = window.event ? ev.keyCode : ev.which;
                    //codes for 0-9
                    if (keyCode < 48 || keyCode > 57) {
                        //codes for backspace, delete, enter
                        if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !ev.ctrlKey) {
                            ev.preventDefault();
                        }
                    }
                });
                html.find('.model_view_table_decimal').on('keypress', function (ev) {
                    var keyCode = window.event ? ev.keyCode : ev.which;
                    //codes for 0-9
                    if (keyCode < 48 || keyCode > 57) {
                        //codes for backspace, delete, enter and period
                        if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !ev.ctrlKey && keyCode != 46) {
                            ev.preventDefault();
                        }
                    }
                });
                html.find('.model_view_table_time').each(function () {
                    $(this).datetimepicker({
                        startView: 1,
                        minView: 0,
                        maxView: 1,
                        format: "hh:ii:ss",
                        iconType: "fa"
                    });
                });
                html.find('.model_view_table_date').each(function () {
                    $(this).datetimepicker({
                        minView: 2,
                        maxView: 2,
                        format: "yyyy-mm-dd"
                    });
                });
                html.find('.model_view_table_datetime').each(function () {
                    $(this).datetimepicker({
                        format: "yyyy-mm-dd hh:ii:ss"
                    });
                });
                that.html(html);
                that.extorio_hideLoader();
            }
        });
    });
};

$.extorio_modelViewer_delete_dialog = function(model, id, ondelete) {
    if(typeof ondelete !== "function") {
        ondelete = function(){};
    }
    $.extorio_modal({
        size: "modal-sm",
        title: "Delete object?",
        content: "Are you sure you want to delete this object?",
        oncontinuebutton: function() {
            $.extorio_showFullPageLoader();
            $.extorio_api({
                endpoint: "/model-viewer/delete/" + model + "/" + id,
                type: "POST",
                oncomplete: function() {
                    $.extorio_hideFullPageLoader();
                },
                onsuccess: function(data) {
                    $.extorio_messageSuccess("Object deleted");
                    ondelete();
                }
            });
        }
    });
};