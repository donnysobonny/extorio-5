//override the html icon/command so that we can persistently set the content of the code view for saving
$.FroalaEditor.DefineIcon('html', {NAME: 'code'});
$.FroalaEditor.RegisterCommand('html', {
    title: 'Code View',
    focus: false,
    undo: false,
    refreshAfterCallback: false,
    callback: function () {
        var editor = this;
        editor.codeView.toggle();
        if(editor.$box.hasClass("fr-code-view")) {
            editor.$wp.on("keyup", function() {
                editor.$original_element.val(editor.codeView.get());
            });
        } else {
            editor.$wp.off("keydown");
        }
    }
});
//insert image using asset manager
$.FroalaEditor.DefineIcon('extorio_image', {NAME: 'image'});
$.FroalaEditor.RegisterCommand('extorio_image', {
    title: 'Insert Image',
    focus: false,
    undo: false,
    refreshAfterCallback: false,
    callback: function () {
        var editor = this;
        $.extorio_dialog_assetManager({
            selectable: true,
            displaytypes: "image",
            onfileselected: function(url) {
                editor.image.insert(url,true,{});
            }
        });
    }
});
//replace image using asset manager
$.FroalaEditor.DefineIcon('extorio_replace_image', {NAME: 'exchange'});
$.FroalaEditor.RegisterCommand('extorio_replace_image', {
    title: 'Replace Image',
    focus: false,
    undo: false,
    refreshAfterCallback: false,
    callback: function () {
        var editor = this;
        editor.popups.hideAll();
        var $img = this.image.get();
        $.extorio_dialog_assetManager({
            selectable: true,
            displaytypes: "image",
            onfileselected: function(url) {
                editor.image.insert(url,true,{},$img);
            }
        });
    }
});
//give images bootstrap image styles
$.FroalaEditor.DefineIcon('extorio_image_style', { NAME: 'magic'})
$.FroalaEditor.RegisterCommand('extorio_image_style', {
    title: 'Style',
    type: 'dropdown',
    icon: 'extorio_image_style',
    options: {
        'img-thumbnail': 'Thumbnail',
        'img-circle': 'Circle',
        'img-rounded': 'Rounded'
    },
    undo: true,
    focus: true,
    refreshAfterCallback: true,
    callback: function (cmd, val, params) {
        var image = this.image.get();
        image.removeClass("img-thumbnail").removeClass("img-rounded").removeClass("img-circle").addClass(val);
    },
    refreshOnShow: function ($btn, $dropdown) {
        var image = this.image.get();
        if(image.hasClass('img-thumbnail')) {
            $dropdown.find('a[data-param1="img-thumbnail"]').addClass('fr-active');
            $dropdown.find('a[data-param1="img-rounded"]').removeClass('fr-active');
            $dropdown.find('a[data-param1="img-circle"]').removeClass('fr-active');
        } else if(image.hasClass('img-rounded')) {
            $dropdown.find('a[data-param1="img-thumbnail"]').removeClass('fr-active');
            $dropdown.find('a[data-param1="img-rounded"]').addClass('fr-active');
            $dropdown.find('a[data-param1="img-circle"]').removeClass('fr-active');
        } else if(image.hasClass('img-circle')) {
            $dropdown.find('a[data-param1="img-thumbnail"]').removeClass('fr-active');
            $dropdown.find('a[data-param1="img-rounded"]').removeClass('fr-active');
            $dropdown.find('a[data-param1="img-circle"]').addClass('fr-active');
        }
    }
});
//complex version of editor
$.fn.extorio_editable = function(options) {
    return this.each(function() {
        var finalOptions = $.extend({
            autosave: false,
            autosaveInverval: 10000,
            onAutosave: function() {},
            htmlAllowedEmptyTags: ['textarea', 'a', 'iframe', 'object', 'video', 'style', 'script', 'span'],
            inlineMode: false,
            toolbarSticky: false,
            codeBeautifier: true,
            codeMirror: true,
            codeMirrorOptions: {
                matchBrackets: true,
                autoCloseBrackets: true,
                matchTags: true,
                autoCloseTags: true,
                indentWithTabs: true,
                lineNumbers: true,
                lineWrapping: true,
                mode: 'text/html',
                tabMode: 'indent',
                tabSize: 2
            },
            toolbarButtons: [
                'bold',
                'italic',
                'underline',
                'strikeThrough',
                'subscript',
                'superscript',
                'fontSize',
                'fontFamily',
                'color',
                '|',
                'paragraphFormat',
                'align',
                'formatOL',
                'formatUL',
                'outdent',
                'indent',
                'quote',
                '|',
                'insertHR',
                'insertLink',
                'extorio_image',
                'insertTable',
                '|',
                'html'
            ],
            toolbarButtonsMD: [
                'bold',
                'italic',
                'underline',
                'fontSize',
                'fontFamily',
                'color',
                'paragraphFormat',
                'align',
                'formatOL',
                'formatUL',
                'outdent',
                'indent',
                'quote',
                'insertHR',
                'insertLink',
                'extorio_image',
                'insertTable',
                'html'
            ],
            toolbarButtonsSM: [
                'bold',
                'italic',
                'underline',
                'fontSize',
                'insertLink',
                'extorio_image',
                'insertTable'
            ],
            toolbarButtonsXS: [
                'bold',
                'italic',
                'fontSize'
            ],
            imageEditButtons: ['extorio_replace_image', 'imageAlign', 'imageRemove', '|', 'imageLink', 'linkEdit', 'linkRemove', '-', 'imageDisplay', 'extorio_image_style', 'imageAlt', 'imageSize']
        },options);

        if(finalOptions.autosave) {
            var self = $(this);
            var lastSave = "";
            $(this).data("autosave_interval",setInterval(function() {
                var val = self.val();
                if(val != lastSave) {
                    finalOptions.onAutosave(val);
                    lastSave = val;
                }
            }, finalOptions.autosaveInverval));
            $(this).on("remove", function() {
                clearInterval(self.data("autosave_interval"));
            });
        }

        $(this).froalaEditor(finalOptions);
        //temp fix to remove unliscence link
        $(this).data('froala.editor').$box.find('> div:not(.fr-toolbar,.fr-wrapper)').remove();
    });
};
//basic version of editor
$.fn.extorio_editable_basic = function(options) {
    return this.each(function() {

        var finalOptions = $.extend({
            autosave: false,
            autosaveInverval: 10000,
            onAutosave: function() {},
            htmlAllowedEmptyTags: ['textarea', 'a', 'iframe', 'object', 'video', 'style', 'span'],
            inlineMode: false,
            toolbarSticky: false,
            toolbarButtons: [
                'bold',
                'italic',
                'underline',
                'paragraphFormat',
                'align',
                'formatOL',
                'formatUL',
                'insertHR',
                'insertLink'
            ],
            toolbarButtonsMD: [
                'bold',
                'italic',
                'underline',
                'paragraphFormat',
                'align',
                'formatOL',
                'formatUL',
                'insertHR',
                'insertLink'
            ],
            toolbarButtonsSM: [
                'bold',
                'italic',
                'underline',
                'insertLink'
            ],
            toolbarButtonsXS: [
                'bold',
                'italic',
                'underline'
            ],
            imageEditButtons: ['extorio_replace_image', 'imageAlign', 'imageRemove', '|', 'imageLink', 'linkEdit', 'linkRemove', '-', 'imageDisplay', 'extorio_image_style', 'imageAlt', 'imageSize']
        },options);

        if(finalOptions.autosave) {
            var self = $(this);
            var lastSave = "";
            $(this).data("autosave_interval",setInterval(function() {
                var val = self.val();
                if(val != lastSave) {
                    finalOptions.onAutosave(val);
                    lastSave = val;
                }
            }, finalOptions.autosaveInverval));
            $(this).on("remove", function() {
                clearInterval(self.data("autosave_interval"));
            });
        }

        $(this).froalaEditor(finalOptions);
        //temp fix to remove unliscence link
        $(this).data('froala.editor').$box.find('> div:not(.fr-toolbar,.fr-wrapper)').remove();
    });
};