(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define('jquery', factory);
    }
    else {
        factory(jQuery);
    }
})(function ($) {
    'use strict';

    var user_report_date_filters_0 = ".user_report_date_filters_0";
    var user_report_date_filters_1 = ".user_report_date_filters_1";
    var user_report_date_filters_2 = ".user_report_date_filters_2";
    var user_report_date_filters_3 = ".user_report_date_filters_3";

    var getCond = function(obj) {
        if(obj.hasOwnProperty("$eq")) {
            return "$eq";
        }
        if(obj.hasOwnProperty("$ne")) {
            return "$ne";
        }
        if(obj.hasOwnProperty("$lk")) {
            return "$lk";
        }
        if(obj.hasOwnProperty("$gt")) {
            return "$gt";
        }
        if(obj.hasOwnProperty("$lt")) {
            return "$lt";
        }
    }

    var Field = function(body,type,cond,value) {
        this.body = body;
        this.type = type;
        this.cond = cond;
        this.value = value;
        var self = this;

        switch (this.type) {
            case "$canLogin" :
                this.body.html('<p style="margin-bottom: 5px;" class="text-muted">Find users that...</p>' +
                    '<select class="form-control">' +
                    '   <option value="1">Can log in</option>' +
                    '   <option value="0">Cannot log in</option>' +
                    '</select>');
                console.log(this.value);
                if(this.value == false) {
                    this.body.find('.form-control').val("0");
                } else {
                    this.value = true;
                }
                this.body.find('.form-control').on("change", function() {
                    if($(this).val() == "1") {
                        self.value = true;
                    } else {
                        self.value = false;
                    }
                });
                break;
            case "$dateLogin" :
                this.body.html('<p style="margin-bottom: 5px;" class="text-muted">Find users that have logged in...</p>');
                if(this.value == undefined) {
                    this.cond = "$eq";
                    this.value = "0|days|past";
                }
                this.body.extorio_reports_addDateFilter(this.cond,this.value,function(cond, value) {
                    self.cond = cond;
                    self.value = value;
                });
                break;
            case "$dateActive" :
                this.body.html('<p style="margin-bottom: 5px;" class="text-muted">Find users that have been active...</p>');
                if(this.value == undefined) {
                    this.cond = "$eq";
                    this.value = "0|days|past";
                }
                this.body.extorio_reports_addDateFilter(this.cond,this.value,function(cond, value) {
                    self.cond = cond;
                    self.value = value;
                });
                break;
            case "$userGroup" :
                this.body.html('<p style="margin-bottom: 5px;" class="text-muted">Find users that are...</p>');
                $.extorio_api({
                    endpoint: "/user-groups/filter",
                    data: {
                        limit: 0,
                        skip: 0
                    },
                    onsuccess: function(resp) {
                        var values = {};
                        for(var i = 0; i < resp.data.length; i++) {
                            values[resp.data[i].id] = resp.data[i].name;
                        }
                        if(self.value == undefined) {
                            self.cond = "$eq";
                            console.log(resp.data[0].id);
                            self.value = resp.data[0].id;
                        }
                        self.body.extorio_reports_addSelectFilter(self.cond,self.value,values,function(cond,value) {
                            self.cond = cond;
                            self.value = value;
                        });
                    }
                });
                break;
            case "$dateCreated" :
                this.body.html('<p style="margin-bottom: 5px;" class="text-muted">Find users that were created...</p>');
                if(this.value == undefined) {
                    this.cond = "$eq";
                    this.value = "0|days|past";
                }
                this.body.extorio_reports_addDateFilter(this.cond,this.value,function(cond, value) {
                    self.cond = cond;
                    self.value = value;
                });
                break;
            case "$dateUpdated" :
                this.body.html('<p style="margin-bottom: 5px;" class="text-muted">Find users that were updated...</p>');
                if(this.value == undefined) {
                    this.cond = "$eq";
                    this.value = "0|days|past";
                }
                this.body.extorio_reports_addDateFilter(this.cond,this.value,function(cond, value) {
                    self.cond = cond;
                    self.value = value;
                });
                break;
            case "$acceptsMarketing" :
                this.body.html('<p style="margin-bottom: 5px;" class="text-muted">Find users that...</p>' +
                    '<select class="form-control">' +
                    '   <option value="1">Accept marketing</option>' +
                    '   <option value="0">Do not accept marketing</option>' +
                    '</select>');
                if(this.value == false) {
                    this.body.find('.form-control').val("0");
                } else {
                    this.value = true;
                }
                this.body.find('.form-control').on("change", function() {
                    if($(this).val() == "1") {
                        self.value = true;
                    } else {
                        self.value = false;
                    }
                });
                break;
            case "$title" :
                this.body.html('<p style="margin-bottom: 5px;" class="text-muted">Find users where their title...</p>');
                $.extorio_api({
                    endpoint:"/titles",
                    onsuccess: function(resp) {
                        var values = {};
                        for(var i = 0; i < resp.data.length; i++) {
                            values[resp.data[i]] = resp.data[i];
                        }
                        if(self.value == undefined) {
                            self.cond = "$eq";
                            self.value = resp.data[0];
                        }
                        self.body.extorio_reports_addSelectFilter(self.cond,self.value,values,function(cond,value) {
                            self.cond = cond;
                            self.value = value;
                        });
                    }
                });
                break;
            case "$gender" :
                this.body.html('<p style="margin-bottom: 5px;" class="text-muted">Find users where their gender...</p>');
                $.extorio_api({
                    endpoint:"/genders",
                    onsuccess: function(resp) {
                        var values = {};
                        for(var i = 0; i < resp.data.length; i++) {
                            values[resp.data[i]] = resp.data[i];
                        }
                        if(self.value == undefined) {
                            self.cond = "$eq";
                            self.value = resp.data[0];
                        }
                        self.body.extorio_reports_addSelectFilter(self.cond,self.value,values,function(cond,value) {
                            self.cond = cond;
                            self.value = value;
                        });
                    }
                });
                break;

            case "$city":
                this.body.html('<p style="margin-bottom: 5px;" class="text-muted">Find users where the city is...</p>');
                if(this.value == undefined) {
                    this.cond = "$eq";
                    this.value = "";
                }
                this.body.extorio_reports_addTextFilter(this.cond,this.value,function(cond,value) {
                    self.cond = cond;
                    self.value = value;
                });
                break;
            case "$region" :
                this.body.html('<p style="margin-bottom: 5px;" class="text-muted">Find users where the region is...</p>');
                if(this.value == undefined) {
                    this.cond = "$eq";
                    this.value = "";
                }
                this.body.extorio_reports_addTextFilter(this.cond,this.value,function(cond,value) {
                    self.cond = cond;
                    self.value = value;
                });
                break;
            case "$postalCode" :
                this.body.html('<p style="margin-bottom: 5px;" class="text-muted">Find users where the postal code is...</p>');
                if(this.value == undefined) {
                    this.cond = "$eq";
                    this.value = "";
                }
                this.body.extorio_reports_addTextFilter(this.cond,this.value,function(cond,value) {
                    self.cond = cond;
                    self.value = value;
                });
                break;

            case "$country":
                this.body.html('<p style="margin-bottom: 5px;" class="text-muted">Find users where their country...</p>');
                $.extorio_api({
                    endpoint:"/countries",
                    onsuccess: function(resp) {
                        var values = {};
                        for(var i = 0; i < resp.data.length; i++) {
                            values[resp.data[i]] = resp.data[i];
                        }
                        if(self.value == undefined) {
                            self.cond = "$eq";
                            self.value = resp.data[0];
                        }
                        self.body.extorio_reports_addSelectFilter(self.cond,self.value,values,function(cond,value) {
                            self.cond = cond;
                            self.value = value;
                        });
                    }
                });
                break;
        }

    };
    var Action = function(body,query,display) {
        this.body = body;
        this.display = display;

        this.type = query.type;
        this.date = query.date;
        this.time = query.time;
        this.detail1 = query.detail1;
        this.detail2 = query.detail2;
        this.detail3 = query.detail3;
        this.count = query.count;

        if(this.count == undefined) {
            this.count = {};
            this.count["$eq"] = 1;
        }

        this.actionType;

        var self = this;

        $.extorio_api({
            endpoint: "/user-action-types/" + this.type,
            onsuccess: function(resp) {
                self.actionType = resp.data;
                self.display.html(self.actionType.description);
                self.body.html('<div class="checkbox">' +
                    '   <label>' +
                    '       <input class="user_report_date_enable" type="checkbox"> Enable date filtering' +
                    '   </label>' +
                    '</div>' +
                    '<div style="margin-bottom: 5px;" class="user_report_date_setting">' +
                    '   <p style="margin-bottom: 5px;" class="text-muted">Where this action happened...</p>' +
                    '</div>' +
                    '<div class="checkbox">' +
                    '   <label>' +
                    '       <input class="user_report_time_enable" type="checkbox"> Enable time filtering' +
                    '   </label>' +
                    '</div>' +
                    '<div style="margin-bottom: 5px;" class="user_report_time_setting">' +
                    '   <p style="margin-bottom: 5px;" class="text-muted">Where this action happened...</p>' +
                    '</div>' +
                    '<div class="checkbox user_report_detail1_checkbox">' +
                    '   <label>' +
                    '       <input class="user_report_detail1_enable" type="checkbox"> Enable detail1 filtering' +
                    '   </label>' +
                    '</div>' +
                    '<div style="margin-bottom: 5px;" class="user_report_detail1_setting"></div>' +
                    '<div class="checkbox user_report_detail2_checkbox">' +
                    '   <label>' +
                    '       <input class="user_report_detail2_enable" type="checkbox"> Enable detail2 filtering' +
                    '   </label>' +
                    '</div>' +
                    '<div style="margin-bottom: 5px;" class="user_report_detail2_setting"></div>' +
                    '<div class="checkbox user_report_detail3_checkbox">' +
                    '   <label>' +
                    '       <input class="user_report_detail3_enable" type="checkbox"> Enable detail3 filtering' +
                    '   </label>' +
                    '</div>' +
                    '<div style="margin-bottom: 5px;" class="user_report_detail3_setting"></div>' +
                    '<p style="margin-bottom: 5px;" class="text-muted">Where this action has happened...</p>' +
                    '');
                var select;
                var cond = "";
                var raw = "";
                var parts = [];

                cond = "$eq";
                raw = 1;
                if(self.count != undefined) {
                    for(var k in self.count) {
                        cond = k;
                        raw = self.count[k];
                        break;
                    }
                }
                self.body.extorio_reports_addCountFilter(cond,raw,function(cond,value) {
                    self.count = {};
                    self.count[cond] = value;
                });

                var setupDetailFilter = function(J,L) {
                    var setting = self.body.find('.user_report_detail'+L+'_setting');
                    if(self.actionType.filters.length > J) {
                        setting.html('<p style="margin-bottom: 5px;" class="text-muted">'+self.actionType["detail"+L+"Description"]+'</p>');
                        if(typeof self.actionType.filters[J] == "object") {

                            cond = "$eq";
                            for(var k in self.actionType.filters[J]) {
                                raw = k;
                                break;
                            }
                            if(self["detail"+L] != undefined) {
                                self.body.find('.user_report_detail'+L+'_enable').prop("checked",true);
                                setting.show();
                                for(var k in self["detail"+L]) {
                                    cond = k;
                                    break;
                                }
                                raw = self["detail"+L][cond];
                            } else {
                                setting.hide();
                            }
                            setting.extorio_reports_addSelectFilter(cond,raw,self.actionType.filters[J],function(cond,value) {
                                self["detail"+L] = {};
                                self["detail"+L][cond] = value;
                            });
                            self.body.find('.user_report_detail'+L+'_enable').on("change", function() {
                                if($(this).prop("checked")) {
                                    self["detail"+L] = {};
                                    for(var k in self.actionType.filters[J]) {
                                        self["detail"+L]["$eq"] = k;
                                        break;
                                    }
                                    setting.extorio_reports_setSelectFilter("$eq",self["detail"+L]["$eq"]);
                                    setting.show();
                                } else {
                                    self["detail"+L] = undefined;
                                    setting.hide();
                                }
                            });
                        } else if(self.actionType.filters[J] == true) {
                            cond = "$eq";
                            raw = "";
                            if(self["detail"+L] != undefined) {
                                for(var k in self["detail"+L]) {
                                    cond = k;
                                    break;
                                }
                                raw = self["detail"+L][cond];
                            } else {
                                setting.hide();
                            }
                            setting.extorio_reports_addTextFilter(cond,raw,function(cond,value) {
                                self["detail"+L] = {};
                                self["detail"+L][cond] = value;
                            });
                            self.body.find('.user_report_detail'+L+'_enable').on("change", function() {
                                if($(this).prop("checked")) {
                                    self["detail"+L] = {};
                                    self["detail"+L]["$eq"] = "";
                                    setting.extorio_reports_setTextFilder("$eq","");
                                    setting.show();
                                } else {
                                    self["detail"+L] = undefined;
                                    setting.hide();
                                }
                            });
                        } else {
                            self.body.find('.user_report_detail'+L+'_checkbox').remove();
                            setting.remove();
                        }
                    } else {
                        self.body.find('.user_report_detail'+L+'_checkbox').remove();
                        setting.remove();
                    }
                }

                for(var J = 0; J < 3; J++) {
                    var L = J+1;
                    setupDetailFilter(J,L);
                }

                select = self.body.find('.user_report_date_1');
                for(var i = 0; i <= 100; i++) {
                    select.append('<option value="'+i+'">'+i+'</option>');
                }
                cond = "$eq";
                raw = "0|days|past";
                if(self.date != undefined) {
                    self.body.find('.user_report_date_enable').prop("checked",true);
                    self.body.find('.user_report_date_setting').show();
                    for(var k in self.date) {
                        cond = k;
                        break;
                    }
                    raw = self.date[k];
                } else {
                    self.body.find('.user_report_date_setting').hide();
                }
                self.body.find('.user_report_date_setting').extorio_reports_addDateFilter(cond,raw,function(cond, value) {
                    self.date = {};
                    self.date[cond] = value;
                });
                self.body.find('.user_report_date_enable').on("change", function() {
                    if($(this).prop("checked")) {
                        self.date = {};
                        self.date["$eq"] = "0|days|past";
                        self.body.find('.user_report_date_setting').extorio_reports_setDateFilter("$eq","0|days|past");
                        self.body.find('.user_report_date_setting').show();
                    } else {
                        self.date = undefined;
                        self.body.find('.user_report_date_setting').hide();
                    }
                });

                cond = "$eq";
                raw = "0|seconds|past";
                if(self.time != undefined) {
                    self.body.find('.user_report_time_enable').prop("checked",true);
                    self.body.find('.user_report_time_setting').show();
                    for(var k in self.time) {
                        cond = k;
                        break;
                    }
                    raw = self.time[k];
                } else {
                    self.body.find('.user_report_time_setting').hide();
                }

                self.body.find('.user_report_time_setting').extorio_reports_addTimeFilter(cond,raw,function(cond,value) {
                    self.time = {};
                    self.time[cond] = value;
                });

                self.body.find('.user_report_time_enable').on("change", function() {
                    if($(this).prop("checked")) {
                        self.time = {};
                        self.time["$eq"] = "0|seconds|past";
                        self.body.find('.user_report_time_setting').extorio_reports_setTimeFilter("$eq","0|seconds|past");
                        self.body.find('.user_report_time_setting').show();
                    } else {
                        self.time = undefined;
                        self.body.find('.user_report_time_setting').hide();
                    }
                });
            }
        });
    };
    var Contact = function(body,query) {
        this.body = body;

        this.type = query.type;
        this.topic = query.topic;
        this.identifier = query.identifier;
        this.date = query.date;
        this.time = query.time;
        this.count = query.count;

        if(this.count == undefined) {
            this.count = {};
            this.count["$eq"] = 1;
        }

        var self = this;
        var select;
        var cond = "";
        var raw = "";
        var parts = [];

        self.body.html('<div class="checkbox">' +
            '   <label>' +
            '       <input class="user_report_date_enable" type="checkbox"> Enable date filtering' +
            '   </label>' +
            '</div>' +
            '<div style="margin-bottom: 5px; display: none;" class="user_report_date_setting">' +
            '   <p style="margin-bottom: 5px;" class="text-muted">Where the contact date is...</p>' +
            '</div>' +
            '<div class="checkbox">' +
            '   <label>' +
            '       <input class="user_report_time_enable" type="checkbox"> Enable time filtering' +
            '   </label>' +
            '</div>' +
            '<div style="margin-bottom: 5px; display: none;" class="user_report_time_setting">' +
            '   <p style="margin-bottom: 5px;" class="text-muted">Where the contact time is...</p>' +
            '</div>' +
            '<div class="checkbox">' +
            '   <label>' +
            '       <input class="user_report_type_enable" type="checkbox"> Enable type filtering' +
            '   </label>' +
            '</div>' +
            '<div style="margin-bottom: 5px; display: none;" class="user_report_type_setting">' +
            '   <p style="margin-bottom: 5px;" class="text-muted">Where the contact type...</p>' +
            '</div>' +
            '<div class="checkbox">' +
            '   <label>' +
            '       <input class="user_report_topic_enable" type="checkbox"> Enable topic filtering' +
            '   </label>' +
            '</div>' +
            '<div style="margin-bottom: 5px; display: none;" class="user_report_topic_setting">' +
            '   <p style="margin-bottom: 5px;" class="text-muted">Where the topic...</p>' +
            '</div>' +
            '<div class="checkbox">' +
            '   <label>' +
            '       <input class="user_report_identifier_enable" type="checkbox"> Enable identifier filtering' +
            '   </label>' +
            '</div>' +
            '<div style="margin-bottom: 5px; display: none;" class="user_report_identifier_setting">' +
            '   <p style="margin-bottom: 5px;" class="text-muted">Where the identifier is...</p>' +
            '</div>' +
            '<p style="margin-bottom: 5px;" class="text-muted">Where this contact has happened...</p>' +
            '');

        cond = "$eq";
        raw = 1;
        if(self.count != undefined) {
            for(var k in self.count) {
                cond = k;
                raw = self.count[k];
                break;
            }
        }
        self.body.extorio_reports_addCountFilter(cond,raw,function(cond,value) {
            self.count = {};
            self.count[cond] = value;
        });

        $.extorio_api({
            endpoint: "/user-contact-types",
            onsuccess: function(resp) {
                var values = {};
                for(var i = 0; i < resp.data.length; i++) {
                    values[resp.data[i]] = resp.data[i];
                }

                var cond = "$eq";
                for(var k in resp.data) {
                    raw = resp.data[k];
                    break;
                }
                if(self.type != undefined) {
                    self.body.find('.user_report_type_enable').prop("checked",true);
                    self.body.find('.user_report_type_setting').show();
                    for(var k in self.type) {
                        cond = k;
                        raw = self.type[k];
                        break;
                    }
                } else {
                    self.body.find('.user_report_type_setting').hide();
                }
                self.body.find('.user_report_type_setting').extorio_reports_addSelectFilter(cond,raw,values,function(cond,value) {
                    self.type = {};
                    self.type[cond] = value;
                });
                self.body.find('.user_report_type_enable').on("change", function() {
                    if($(this).prop("checked")) {
                        self.type = {};
                        for(var k in resp.data) {
                            self.type["$eq"] = resp.data[k];
                            break;
                        }
                        self.body.find('.user_report_type_setting').extorio_reports_setSelectFilter("$eq",self.type["$eq"]);
                        self.body.find('.user_report_type_setting').show();
                    } else {
                        self.type = undefined;
                        self.body.find('.user_report_type_setting').hide();
                    }
                });
            }
        });

        $.extorio_api({
            endpoint: "/user-contact-topics",
            onsuccess: function(resp) {
                var values = {};
                for(var i = 0; i < resp.data.length; i++) {
                    values[resp.data[i].id] = resp.data[i].name;
                }

                var cond = "$eq";
                for(var k in resp.data) {
                    raw = resp.data[k].id;
                    break;
                }
                if(self.topic != undefined) {
                    self.body.find('.user_report_topic_enable').prop("checked",true);
                    self.body.find('.user_report_topic_setting').show();
                    for(var k in self.topic) {
                        cond = k;
                        raw = self.topic[k];
                        break;
                    }
                } else {
                    self.body.find('.user_report_topic_setting').hide();
                }
                self.body.find('.user_report_topic_setting').extorio_reports_addSelectFilter(cond,raw,values,function(cond,value) {
                    self.topic = {};
                    self.topic[cond] = value;
                });
                self.body.find('.user_report_topic_enable').on("change", function() {
                    if($(this).prop("checked")) {
                        self.topic = {};
                        for(var k in resp.data) {
                            self.topic["$eq"] = resp.data[k].id;
                            break;
                        }
                        self.body.find('.user_report_topic_setting').extorio_reports_setSelectFilter("$eq",self.topic["$eq"]);
                        self.body.find('.user_report_topic_setting').show();
                    } else {
                        self.topic = undefined;
                        self.body.find('.user_report_topic_setting').hide();
                    }
                });
            }
        });

        cond = "$eq";
        raw = "";
        if(self.identifier != undefined) {
            self.body.find('.user_report_identifier_enable').prop("checked",true);
            self.body.find('.user_report_identifier_setting').show();
            for(var k in self.identifier) {
                cond = k;
                raw = self.identifier[k];
                break;
            }
        } else {
            self.body.find('.user_report_identifier_setting').hide();
        }
        self.body.find('.user_report_identifier_setting').extorio_reports_addTextFilter(cond,raw,function(cond,value) {
            self.identifier = {};
            self.identifier[cond] = value;
        });
        self.body.find('.user_report_identifier_enable').on("change", function() {
            if($(this).prop("checked")) {
                self.identifier = {};
                self.identifier["$eq"] = "";
                self.body.find('.user_report_identifier_setting').extorio_reports_setTextFilder("$eq","");
                self.body.find('.user_report_identifier_setting').show();
            } else {
                self.identifier = undefined;
                self.body.find('.user_report_identifier_setting').hide();
            }
        });

        cond = "$eq";
        raw = "0|days|past";
        if(self.date != undefined) {
            self.body.find('.user_report_date_enable').prop("checked",true);
            self.body.find('.user_report_date_setting').show();
            for(var k in self.date) {
                cond = k;
                raw = self.date[k];
                break;
            }
        } else {
            self.body.find('.user_report_date_setting').hide();
        }
        self.body.find('.user_report_date_setting').extorio_reports_addDateFilter(cond,raw,function(cond,value) {
            self.date = {};
            self.date[cond] = value;
        });
        self.body.find('.user_report_date_enable').on("change", function() {
            if($(this).prop("checked")) {
                self.date = {};
                self.date["$eq"] = "0|days|past";
                self.body.find('.user_report_date_setting').extorio_reports_setDateFilter("$eq","0|days|past");
                self.body.find('.user_report_date_setting').show();
            } else {
                self.date = undefined;
                self.body.find('.user_report_date_setting').hide();
            }
        });

        cond = "$eq";
        raw = "0|seconds|past";
        if(self.time != undefined) {
            self.body.find('.user_report_time_enable').prop("checked",true);
            self.body.find('.user_report_time_setting').show();
            for(var k in self.time) {
                cond = k;
                raw = self.time[k];
                break;
            }
        } else {
            self.body.find('.user_report_time_setting').hide();
        }
        self.body.find('.user_report_time_setting').extorio_reports_addTimeFilter(cond,raw,function(cond,value) {
            self.time = {};
            self.time[cond] = value;
        });
        self.body.find('.user_report_time_enable').on("change", function() {
            if($(this).prop("checked")) {
                self.time = {};
                self.time["$eq"] = "0|seconds|past";
                self.body.find('.user_report_time_setting').extorio_reports_setTimeFilter("$eq","0|seconds|past");
                self.body.find('.user_report_time_setting').show();
            } else {
                self.time = undefined;
                self.body.find('.user_report_time_setting').hide();
            }
        });
    };

    $.fn.extorio_reports_addSelectFilter = function(cond,value,values,onChange) {
        return this.each(function() {
            $(this).append('<div class="row">' +
                '   <div class="col-xs-4">' +
                '       <select class="form-control user_report_select_0">' +
                '           <option value="$eq">is</option>' +
                '           <option value="$ne">is not</option>' +
                '       </select>' +
                '   </div>' +
                '   <div class="col-xs-8">' +
                '       <select class="form-control user_report_select_1">' +
                '       </select>' +
                '   </div>' +
                '</div>');
            var select = $(this).find('.user_report_select_1');
            for(var k in values) {
                select.append('<option value="'+k+'">'+values[k]+'</option>');
            }

            $(this).extorio_reports_setSelectFilter(cond,value);

            if(typeof onChange == "function") {
                var self = $(this);
                $(this).find('.user_report_select_0, .user_report_select_1').on("change", function() {
                    onChange(self.find('.user_report_select_0').val(),self.find('.user_report_select_1').val());
                });
            }
        });
    }
    $.fn.extorio_reports_setSelectFilter = function(cond,value) {
        return this.each(function() {
            if(cond) {
                $(this).find('.user_report_select_0').val(cond);
            }
            if(value) {
                $(this).find('.user_report_select_1').val(value);
            }
        });
    }

    $.fn.extorio_reports_addTextFilter = function(cond,value,onChange) {
        return this.each(function() {
            $(this).append('<div class="row">' +
                '   <div class="col-xs-4">' +
                '       <select class="form-control user_report_text_filters_0">' +
                '           <option value="$eq">exactly</option>' +
                '           <option value="$ne">not</option>' +
                '           <option value="$lk">like</option>' +
                '           <option value="$gt">more than</option>' +
                '           <option value="$lt">less than</option>' +
                '       </select>' +
                '   </div>' +
                '   <div class="col-xs-8">' +
                '       <input value="" type="text" class="form-control user_report_text_filters_1" placeholder="Enter a value" />' +
                '   </div>' +
                '</div>');

            $(this).extorio_reports_setTextFilder(cond,value);

            if(typeof onChange == "function") {
                var self = $(this);
                $(this).find('.user_report_text_filters_0').on("change", function() {
                    onChange(self.find('.user_report_text_filters_0').val(),self.find('.user_report_text_filters_1').val());
                });
                $(this).find('.user_report_text_filters_1').on("focus blur keyup", function() {
                    onChange(self.find('.user_report_text_filters_0').val(),self.find('.user_report_text_filters_1').val());
                });
            }
        });
    }
    $.fn.extorio_reports_setTextFilder = function(cond,value) {
        return this.each(function() {
            if(cond) {
                $(this).find('.user_report_text_filters_0').val(cond);
            }
            $(this).find('.user_report_text_filters_1').val(value);
        });
    }

    $.fn.extorio_reports_addDateFilter = function(cond, value, onChange) {
        return this.each(function() {
            $(this).append('<div class="row">' +
                '   <div class="col-xs-3">' +
                '       <select class="form-control user_report_date_filters_0">' +
                '           <option value="$eq">exactly</option>' +
                '           <option value="$ne">not</option>' +
                '           <option value="$gt">after</option>' +
                '           <option value="$lt">before</option>' +
                '       </select>' +
                '   </div>' +
                '   <div class="col-xs-3">' +
                '       <select class="form-control user_report_date_filters_1">' +
                '       </select>' +
                '   </div>' +
                '   <div class="col-xs-3">' +
                '       <select class="form-control user_report_date_filters_2">' +
                '           <option value="days">days</option>' +
                '           <option value="weeks">weeks</option>' +
                '           <option value="months">months</option>' +
                '           <option value="years">years</option>' +
                '       </select>' +
                '   </div>' +
                '   <div class="col-xs-3">' +
                '       <select class="form-control user_report_date_filters_3">' +
                '           <option value="past">ago</option>' +
                '           <option value="future">from now</option>' +
                '       </select>' +
                '   </div>' +
                '</div>');
            var select = $(this).find('.user_report_date_filters_1');
            for(var i = 0; i <= 100; i++) {
                select.append('<option value="'+i+'">'+i+'</option>');
            }

            $(this).extorio_reports_setDateFilter(cond,value);

            if(typeof onChange == "function") {
                var self = $(this);
                $(this).find(user_report_date_filters_0+", "+user_report_date_filters_1+", "+user_report_date_filters_2+", "+user_report_date_filters_3).on("change", function() {
                    onChange(self.find(user_report_date_filters_0).val(),self.find(user_report_date_filters_1).val()+"|"+self.find(user_report_date_filters_2).val()+"|"+self.find(user_report_date_filters_3).val());
                });
            }
        });
    }
    $.fn.extorio_reports_setDateFilter = function(cond,value) {
        return this.each(function() {
            if(cond) {
                $(this).find(user_report_date_filters_0).val(cond);
            }
            if(value) {
                var parts = value.split("|");
                $(this).find(user_report_date_filters_1).val(parts[0]);
                $(this).find(user_report_date_filters_2).val(parts[1]);
                $(this).find(user_report_date_filters_3).val(parts[2]);
            }
        });
    }

    $.fn.extorio_reports_addTimeFilter = function(cond,value,onChange) {
        return this.each(function() {
            $(this).append('<div class="row">' +
                '   <div class="col-xs-3">' +
                '       <select class="form-control user_report_time_filters_0">' +
                '           <option value="$eq">exactly</option>' +
                '           <option value="$ne">not</option>' +
                '           <option value="$gt">after</option>' +
                '           <option value="$lt">before</option>' +
                '       </select>' +
                '   </div>' +
                '   <div class="col-xs-3">' +
                '       <select class="form-control user_report_time_filters_1">' +
                '       </select>' +
                '   </div>' +
                '   <div class="col-xs-3">' +
                '       <select class="form-control user_report_time_filters_2">' +
                '           <option value="seconds">seconds</option>' +
                '           <option value="minutes">minutes</option>' +
                '           <option value="hours">hours</option>' +
                '       </select>' +
                '   </div>' +
                '   <div class="col-xs-3">' +
                '       <select class="form-control user_report_time_filters_3">' +
                '           <option value="past">ago</option>' +
                '           <option value="future">from now</option>' +
                '       </select>' +
                '   </div>' +
                '</div>');
            var select = $(this).find('.user_report_time_filters_1');
            for(var i = 0; i <= 100; i++) {
                select.append('<option value="'+i+'">'+i+'</option>');
            }

            $(this).extorio_reports_setTimeFilter(cond,value);

            if(typeof onChange == "function") {
                var self = $(this);
                $(this).find('.user_report_time_filters_0, .user_report_time_filters_1, .user_report_time_filters_2, .user_report_time_filters_3').on("change", function() {
                    onChange(self.find('.user_report_time_filters_0').val(),self.find('.user_report_time_filters_1').val() + "|" + self.find('.user_report_time_filters_2').val() + "|" + self.find('.user_report_time_filters_3').val());
                });
            }
        });
    }
    $.fn.extorio_reports_setTimeFilter = function(cond,value) {
        return this.each(function() {
            if(cond) {
                $(this).find('.user_report_time_filters_0').val(cond);
            }
            if(value) {
                var parts = value.split("|");
                $(this).find('.user_report_time_filters_1').val(parts[0]);
                $(this).find('.user_report_time_filters_2').val(parts[1]);
                $(this).find('.user_report_time_filters_3').val(parts[2]);
            }
        });
    }

    $.fn.extorio_reports_addCountFilter = function(cond,value,onChange) {
        return this.each(function() {
            $(this).append('<div class="row">' +
                '   <div class="col-xs-4">' +
                '       <select class="form-control user_report_count_0">' +
                '           <option value="$eq">exactly</option>' +
                '           <option value="$ne">not</option>' +
                '           <option value="$gt">more than</option>' +
                '           <option value="$lt">less than</option>' +
                '       </select>' +
                '   </div>' +
                '   <div class="col-xs-4">' +
                '       <select class="form-control user_report_count_1">' +
                '       </select>' +
                '   </div>' +
                '   <div class="col-xs-4">' +
                '       <table style="height: 21px;" cellspacing="0" cellpadding="0" border="0">' +
                '           <tbody>' +
                '               <tr><td><strong>time(s)</strong></td></tr>' +
                '           </tbody>' +
                '       </table>' +
                '   </div>' +
                '</div>');
            var select = $(this).find('.user_report_count_1');
            for(var i = 0; i <= 100; i++) {
                select.append('<option value="'+i+'">'+i+'</option>');
            }

            $(this).extorio_reports_setCountFilter(cond,value);

            if(typeof onChange == "function") {
                var self = $(this);
                $(this).find('.user_report_count_0, .user_report_count_1').on("change", function() {
                    onChange(self.find('.user_report_count_0').val(),self.find('.user_report_count_1').val());
                });
            }
        });
    }
    $.fn.extorio_reports_setCountFilter = function(cond,value) {
        return this.each(function() {
            if(cond) {
                $(this).find('.user_report_count_0').val(cond);
            }
            if(value) {
                $(this).find('.user_report_count_1').val(value);
            }
        });
    }

    $.fn.extorio_reports_addField = function(type,cond,value) {
        return this.each(function() {
            var self = $(this);
            var display = "";
            switch (type) {
                case "$canLogin":
                    display = "Can Login";
                    break;
                case "$dateLogin":
                    display = "Date Login";
                    break;
                case "$dateActive":
                    display = "Date Active";
                    break;
                case "$userGroup" :
                    display = "User Group";
                    break;
                case "$dateCreated" :
                    display = "Date Created";
                    break;
                case "$dateUpdated" :
                    display = "Date Updated";
                    break;
                case "$acceptsMarketing" :
                    display = "Accepts Marketing";
                    break;
                case "$title" :
                    display = "Title";
                    break;
                case "$gender" :
                    display = "Gender";
                    break;

                case "$city" :
                    display = "City";
                    break;
                case "$region" :
                    display = "Region";
                    break;
                case "$postalCode" :
                    display = "Postal Code";
                    break;

                case "$country" :
                    display = "Country";
                    break;
            }
            var item = $('<div class="panel panel-success user_report_element user_report_field">' +
                '   <div class="panel-heading">' +
                '       <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">' +
                '           <tbody>' +
                '               <tr>' +
                '                   <td style="vertical-align: middle;"><h5 style="margin: 0; white-space: nowrap;">'+display+'</h5></td>' +
                '                   <td style="width: 100%; text-align: right; vertical-align: middle;"><button class="btn btn-xs btn-danger user_report_element_remove"><span class="fa fa-close"></span></button></td>' +
                '               </tr>' +
                '           </tbody>' +
                '       </table>' +
                '   </div>' +
                '   <div class="panel-body">' +
                '   </div>' +
                '</div>');
            var field = new Field(item.find('.panel-body'),type,cond,value);
            item.data('user_report_field',field);
            self.append(item);
            item.find('.user_report_element_remove').on("click", function() {
                item.remove();
            });
        });
    }

    $.fn.extorio_reports_addGroup = function(type,fields) {
        return this.each(function() {
            var self = $(this);
            var display = type == "$and" ? "AND" : "OR";
            var item = $('<div data-group="'+type+'" class="panel panel-primary user_report_element user_report_group">' +
                '   <div class="panel-heading">' +
                '       <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">' +
                '           <tbody>' +
                '               <tr>' +
                '                   <td style="vertical-align: middle;"><h5 style="margin: 0; white-space: nowrap;">'+display+' group</h5></td>' +
                '                   <td style="width: 100%; text-align: right; vertical-align: middle;"><button class="btn btn-xs btn-danger user_report_element_remove"><span class="fa fa-close"></span></button></td>' +
                '               </tr>' +
                '           </tbody>' +
                '       </table>' +
                '   </div>' +
                '   <div class="panel-body">' +
                '       <div class="btn-group" role="group">' +
                '           <button type="button" class="btn btn-xs btn-success user_report_add_field"><span class="fa fa-plus"></span> field</button>' +
                '           <button type="button" class="btn btn-xs btn-warning user_report_add_action"><span class="fa fa-plus"></span> action</button>' +
                '           <button type="button" class="btn btn-xs btn-danger user_report_add_contact"><span class="fa fa-plus"></span> contact</button>' +
                '           <button type="button" class="btn btn-xs btn-primary user_report_add_and"><span class="fa fa-plus"></span> AND group</button>' +
                '           <button type="button" class="btn btn-xs btn-primary user_report_add_or"><span class="fa fa-plus"></span> OR group</button>' +
                '       </div>' +
                '   </div>' +
                '</div>');

            var body = item.find('.panel-body');

            body.find('.user_report_add_contact').on("click", function() {
                body.extorio_reports_addContact({});
            });

            body.find('.user_report_add_action').on("click", function() {
                $.extorio_modal({
                    title: "Select an action type",
                    content: '<select class="form-control"></select>',
                    size: "modal-sm",
                    onopen: function() {
                        var select = this.find('.form-control');
                        $.extorio_api({
                            endpoint: "/user-action-types/",
                            onsuccess: function(resp) {
                                for(var i = 0; i < resp.data.length; i++) {
                                    var at = resp.data[i];
                                    select.append('<option value="'+at.id+'">'+at.description+'</option>');
                                }
                            }
                        });
                    },
                    oncontinuebutton: function() {
                        body.extorio_reports_addAction({
                            type: this.find('.form-control').val()
                        });
                    }
                });
            });

            body.find('.user_report_add_field').on("click", function() {
                $.extorio_modal({
                    title: "Select a field",
                    size: "modal-sm",
                    content: '<select class="form-control">' +
                    '   <option value="$canLogin">Can Login</option>' +
                    '   <option value="$dateLogin">Date Login</option>' +
                    '   <option value="$dateActive">Date Active</option>' +
                    '   <option value="$userGroup">User Group</option>' +
                    '   <option value="$dateCreated">Date Created</option>' +
                    '   <option value="$dateUpdated">Date Updated</option>' +
                    '   <option value="$acceptsMarketing">Accepts Marketing</option>' +
                    '   <option value="$title">Title</option>' +
                    '   <option value="$gender">Gender</option>' +

                    '   <option value="$city">City</option>' +
                    '   <option value="$region">Region</option>' +
                    '   <option value="$postalCode">Postal Code</option>' +

                    '   <option value="$country">Country</option>' +
                    '</select>',
                    oncontinuebutton: function() {
                        body.extorio_reports_addField(this.find('.form-control').val());
                    }
                });
            });

            body.find('.user_report_add_and').on("click", function() {
                body.extorio_reports_addGroup("$and");
            });
            body.find('.user_report_add_or').on("click", function() {
                body.extorio_reports_addGroup("$or");
            });

            self.append(item);

            var parent = self.parent('.user_report_group');
            if(parent.length) {
                item.find('.user_report_element_remove').on("click", function() {
                    item.remove();
                });
            } else {
                item.find('.user_report_element_remove').hide();
            }

            if(fields != undefined) {
                for(var i = 0; i < fields.length; i++) {
                    body.extorio_reports_insert(fields[i]);
                }
            }
        });
    }

    $.fn.extorio_reports_addAction = function(query) {
        return this.each(function() {
            var self = $(this);
            var item = $('<div class="panel panel-warning user_report_element user_report_action">' +
                '   <div class="panel-heading">' +
                '       <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">' +
                '           <tbody>' +
                '               <tr>' +
                '                   <td style="vertical-align: middle;"><h5 class="user_action_display" style="margin: 0; white-space: nowrap;"></h5></td>' +
                '                   <td style="width: 100%; text-align: right; vertical-align: middle;"><button class="btn btn-xs btn-danger user_report_element_remove"><span class="fa fa-close"></span></button></td>' +
                '               </tr>' +
                '           </tbody>' +
                '       </table>' +
                '   </div>' +
                '   <div class="panel-body">' +
                '   </div>' +
                '</div>');
            var action = new Action(item.find('.panel-body'),query,item.find('.user_action_display'));
            item.data('user_report_action',action);
            self.append(item);
            item.find('.user_report_element_remove').on("click", function() {
                item.remove();
            });
        });
    }

    $.fn.extorio_reports_addContact = function(query) {
        return this.each(function() {
            var self = $(this);
            var item = $('<div class="panel panel-danger user_report_element user_report_contact">' +
                '   <div class="panel-heading">' +
                '       <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">' +
                '           <tbody>' +
                '               <tr>' +
                '                   <td style="vertical-align: middle;"><h5 style="margin: 0; white-space: nowrap;">Contact</h5></td>' +
                '                   <td style="width: 100%; text-align: right; vertical-align: middle;"><button class="btn btn-xs btn-danger user_report_element_remove"><span class="fa fa-close"></span></button></td>' +
                '               </tr>' +
                '           </tbody>' +
                '       </table>' +
                '   </div>' +
                '   <div class="panel-body">' +
                '   </div>' +
                '</div>');
            var contact = new Contact(item.find('.panel-body'),query);
            item.data('user_report_contact',contact);
            self.append(item);
            item.find('.user_report_element_remove').on("click", function() {
                item.remove();
            });
        });
    }

    $.fn.extorio_reports_insert = function(query) {
        return this.each(function() {
            for(var k in query) {
                //if numberic
                if(!isNaN(parseFloat(k)) && isFinite(k)) {
                    $(this).extorio_reports_insert(query[k]);
                } else {
                    var cond;
                    switch (k) {
                        case "$and" :
                            $(this).extorio_reports_addGroup("$and",query[k]);
                            break;
                        case "$or" :
                            $(this).extorio_reports_addGroup("$or",query[k]);
                            break;

                        case "$canLogin":
                            var cond = getCond(query[k]);
                            $(this).extorio_reports_addField("$canLogin",cond,query[k]);
                            break;
                        case "$dateLogin":
                            var cond = getCond(query[k]);
                            $(this).extorio_reports_addField("$dateLogin",cond,query[k][cond]);
                            break;
                        case "$dateActive":
                            var cond = getCond(query[k]);
                            $(this).extorio_reports_addField("$dateActive",cond,query[k][cond]);
                            break;
                        case "$userGroup" :
                            var cond = getCond(query[k]);
                            $(this).extorio_reports_addField("$userGroup",cond,query[k][cond]);
                            break;
                        case "$dateCreated" :
                            var cond = getCond(query[k]);
                            $(this).extorio_reports_addField("$dateCreated",cond,query[k][cond]);
                            break;
                        case "$dateUpdated" :
                            var cond = getCond(query[k]);
                            $(this).extorio_reports_addField("$dateUpdated",cond,query[k][cond]);
                            break;
                        case "$acceptsMarketing" :
                            var cond = getCond(query[k]);
                            $(this).extorio_reports_addField("$acceptsMarketing",cond,query[k]);
                            break;
                        case "$title" :
                            var cond = getCond(query[k]);
                            $(this).extorio_reports_addField("$title",cond,query[k][cond]);
                            break;
                        case "$gender" :
                            var cond = getCond(query[k]);
                            $(this).extorio_reports_addField("$gender",cond,query[k][cond]);
                            break;
                        case "$city" :
                            var cond = getCond(query[k]);
                            $(this).extorio_reports_addField("$city",cond,query[k][cond]);
                            break;
                        case "$region" :
                            var cond = getCond(query[k]);
                            $(this).extorio_reports_addField("$region",cond,query[k][cond]);
                            break;
                        case "$postalCode" :
                            var cond = getCond(query[k]);
                            $(this).extorio_reports_addField("$postalCode",cond,query[k][cond]);
                            break;
                        case "$country" :
                            var cond = getCond(query[k]);
                            $(this).extorio_reports_addField("$country",cond,query[k][cond]);
                            break;

                        case "$action":
                            $(this).extorio_reports_addAction(query[k]);
                            break;
                        case "$contact":
                            $(this).extorio_reports_addContact(query[k]);
                            break;

                    }
                }
            }
        });
    }

    $.fn.extorio_reports__toObject = function() {
        var obj = [];
        var self = this;
        //groups
        this.contents('.user_report_group').each(function() {
            var group = $(this).attr('data-group');
            if(group == "$and") {
                obj.push({"$and": $(this).contents('.panel-body').extorio_reports__toObject()});
            } else {
                obj.push({"$or": $(this).contents('.panel-body').extorio_reports__toObject()});
            }
        });
        //fields
        this.contents('.user_report_field').each(function() {
            var inner = {};
            /**
             * @type {Field}
             */
            var field = $(this).data('user_report_field');
            if(field.cond != undefined) {
                inner[field.type] = {};
                inner[field.type][field.cond] = field.value;
            } else {
                inner[field.type] = field.value;
            }
            obj.push(inner);
        });
        //actions
        this.contents('.user_report_action').each(function() {
            var inner = {};
            var action = $(this).data('user_report_action');
            inner.type = action.type;
            inner.count = action.count;
            if(action.date) {
                inner.date = action.date;
            }
            if(action.time) {
                inner.time = action.time;
            }
            if(action.detail1) {
                inner.detail1 = action.detail1;
            }
            if(action.detail2) {
                inner.detail2 = action.detail2;
            }
            if(action.detail3) {
                inner.detail3 = action.detail3;
            }
            obj.push({"$action": inner});
        });
        //contacts
        this.contents('.user_report_contact').each(function() {
            var inner = {};
            var contact = $(this).data('user_report_contact');
            inner.count = contact.count;
            if(contact.type) {
                inner.type = contact.type;
            }
            if(contact.topic) {
                inner.topic = contact.topic;
            }
            if(contact.identifier) {
                inner.identifier = contact.identifier;
            }
            if(contact.date) {
                inner.date = contact.date;
            }
            if(contact.time) {
                inner.time = contact.time;
            }
            obj.push({"$contact": inner});
        });
        return obj;
    }
});