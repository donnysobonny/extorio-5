(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define('jquery', factory);
    }
    else {
        factory(jQuery);
    }
})(function ($) {
    'use strict';

    var extorioAutoTranslate = function () {
        this.access_token = Cookies.get("extorio_auto_translate_access_token");
        this.expires_in = parseInt(Cookies.get("extorio_auto_translate_expires_in"));
        this.from = "en";
        this.to = "en";
        this.threshold = 4;
        this.translating = false;
        this.elems = [];
        this.texts = [];
        this.updateTime = new Date();
    };
    extorioAutoTranslate.prototype = {
        init: function () {
            var self = this;
            $('body').bind('DOMSubtreeModified', function () {
                var d = new Date();
                self.updateTime.setTime(d.getTime() + 240);
                setTimeout(function() {
                    var e = new Date();
                    if(e.getTime() >= self.updateTime.getTime()) {
                        self.translate();
                    }
                },250);
            });
        },
        setTo: function (to) {
            this.to = to;
        },
        getTo: function () {
            return this.to;
        },
        extractElemsAndText: function (elem) {
            var self = this;
            elem.contents().filter(function () {
                return this.nodeType == 3;
            }).each(function () {
                var text = elem.data("extorio_auto_translate_orig");
                var trans = elem.data("extorio_auto_translate_trans");
                if (text == undefined) {
                    text = this.textContent;
                    //strip whitespace
                    text = text.replace(/[\n\f\r\t\v]*/g, '');
                    //remove recurring spaces
                    text = text.replace(/[\xA0 ]+/g, ' ');
                    this.textContent = text;
                    elem.data("extorio_auto_translate_orig", text);
                }
                var lastto = elem.data("extorio_auto_translate_lastto");
                if (lastto != self.to || (text == this.textContent && text != trans)) {
                    elem.data("extorio_auto_translate_lastto", self.to);
                    if (text.length >= self.threshold) {
                        self.elems.push(elem);
                        self.texts.push(text);
                    }
                }
            });
            elem.find(' > :not(.notranslate, [translate="no"], input, textarea, style, script)').each(function () {
                self.extractElemsAndText($(this));
            });
        },
        preTranslate: function (done) {
            if (this.expires_in > new Date().getTime()) {
                if (typeof done == "function") {
                    done();
                }
            } else {
                var self = this;
                $.ajax({
                    url: "/extorio/apis/localization/auto_translate_auth_token",
                    type: "GET",
                    dataType: "json",
                    error: function (a, b, c) {
                        console.error(c);
                    },
                    success: function (resp) {
                        if (resp.error) {
                            console.error(resp.error_message);
                        } else {
                            self.access_token = resp.data.access_token;
                            var date = new Date();
                            date.setSeconds(date.getSeconds() + parseInt(resp.data.expires_in));
                            self.expires_in = date.getTime();

                            Cookies.set("extorio_auto_translate_access_token",self.access_token);
                            Cookies.set("extorio_auto_translate_expires_in",self.expires_in);

                            if (typeof done == "function") {
                                done();
                            }
                        }
                    }
                });
            }
        },
        translate: function () {
            var self = this;
            if (this.translating) {
                return;
            }
            this.translating = true;
            this.elems = [];
            this.texts = [];
            this.extractElemsAndText($('body'));
            this.preTranslate(function() {
                if (self.elems.length > 0) {
                    $.ajax({
                        url: "http://api.microsofttranslator.com/v2/ajax.svc/TranslateArray",
                        type: "GET",
                        dataType: "json",
                        data: {
                            appId: "Bearer " + self.access_token,
                            from: self.from,
                            texts: JSON.stringify(self.texts),
                            to: self.to
                        },
                        error: function (a, b, c) {
                            console.error(c);
                        },
                        success: function (data) {
                            for (var i = 0; i < data.length; i++) {
                                var elem = self.elems[i];
                                var trans = data[i].TranslatedText;
                                elem.data("extorio_auto_translate_trans", trans);
                                elem.contents().filter(function () {
                                    return this.nodeType == 3;
                                }).each(function () {
                                    this.textContent = trans;
                                });
                            }
                            self.translating = false;
                        }
                    });
                } else {
                    self.translating = false;
                }
            });
        }
    };

    var get = function () {
        var data = $('body').data("extorio_auto_translate");
        if (data == undefined) {
            $('body').data('extorio_auto_translate', (data = new extorioAutoTranslate()));
            data.init();
        }
        return data;
    };

    $.extorio_auto_translate_set_to = function (to) {
        var o = get();
        o.setTo(to);
    };
    $.extorio_auto_tranlate_get_to = function () {
        var o = get();
        return o.getTo();
    };
    $.extorio_auto_translate = function (to) {
        var o = get();
        if (to != undefined) {
            o.setTo(to);
        }
        o.translate();
    };
});