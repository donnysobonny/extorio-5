//static methods
//messaging
$.extorio_messageError = function (message) {
    $(document).trigger("extorio_message", {
        "type": "error",
        "message": message
    });
};
$.extorio_messageWarning = function (message) {
    $(document).trigger("extorio_message", {
        "type": "warning",
        "message": message
    });
};
$.extorio_messageInfo = function (message) {
    $(document).trigger("extorio_message", {
        "type": "info",
        "message": message
    });
};
$.extorio_messageSuccess = function (message) {
    $(document).trigger("extorio_message", {
        "type": "success",
        "message": message
    });
};

//loader
$.extorio_showFullPageLoader = function () {
    $('body').extorio_showLoader();
};
$.extorio_hideFullPageLoader = function () {
    $('body').extorio_hideLoader();
};
//progress bar
$.extorio_showProgressBar = function () {
    $('#extorio_progress_bar_top').animate({opacity: 0.5}, 250);
};
$.extorio_hideProgressBar = function() {
    $('#extorio_progress_bar_top').animate({opacity: 0}, 250);
};
$.extorio_setProgressBar = function(progress) {
    $('#extorio_progress_bar_top .progress-bar').attr('aria-valuenow',progress).css("width",progress + "%");
};
//api
var extorio_apiDefaults = {
    endpoint: "",
    dataType: "json",
    disablemessages: false,
    disableprogressbar: false,
    onerror: function (error) {},
    onwarningmessage: function (warning) {},
    oninfomessage: function (info) {},
    onsuccessmessage: function (success) {},
    onsuccess: function (response) {},
    onstart: function() {},
    oncomplete: function() {},
    onuploadprogress: function (progress) {},
    error: function(a,b,c) {
        if(!this.disablemessages) {
            $.extorio_messageError(c);
        }
        this.onerror(c);
        if(!this.disableprogressbar) {
            $.extorio_hideProgressBar();
        }
        this.oncomplete();
    },
    success: function(response) {
        if(response.error) {
            if(!this.disablemessages) {
                $.extorio_messageError(response.error_message);
            }
            this.onerror(response.error_message);
        } else {
            if(response.success_message.length > 0) {
                if(!this.disablemessages) {
                    $.extorio_messageSuccess(response.success_message);
                }
                this.onsuccessmessage(response.success_message);
            }
            if(response.info_message.length > 0) {
                if(!this.disablemessages) {
                    $.extorio_messageInfo(response.info_message);
                }
                this.oninfomessage(response.info_message);
            }
            if(response.warning_message.length > 0) {
                if(!this.disablemessages) {
                    $.extorio_messageWarning(response.warning_message);
                }
                this.onwarningmessage(response.warning_message);
            }
            this.onsuccess(response);
        }
        if(!this.disableprogressbar) {
            $.extorio_hideProgressBar();
        }
        this.oncomplete();
    }
};
$.extorio_api = function (options) {
    var options = $.extend({}, extorio_apiDefaults, options);
    options.url = "/extorio/apis" + options.endpoint;
    options.xhr = function () {
        var xhr = new window.XMLHttpRequest();
        xhr.upload.addEventListener("progress", function (evt) {
            if (evt.lengthComputable) {
                var p = (evt.loaded / evt.total) * 100;
                options.onuploadprogress(p);
                if(!options.disableprogressbar) {
                    $.extorio_setProgressBar(p);
                }
            }
        }, false);

        return xhr;
    };
    if(!options.disableprogressbar) {
        $.extorio_showProgressBar();
    }
    options.onstart();
    return $.ajax(options);
};
//dialog
var extorio_dialogDefaults = {
    container: null,
    size: "",
    width: undefined,
    title: "Dialog",
    content: "This is a dialog!",
    closetext: "Close",
    onopen: function () {
    },
    onclose: function () {
    },
    onclosebutton: function () {
    }
};
$.extorio_dialog = function (options) {
    var options = $.extend({}, extorio_dialogDefaults, options);
    var dialog = $('' +
        '<div class="modal fade">' +
        '   <div class="modal-dialog ' + options.size + '">' +
        '       <div class="modal-content">' +
        '           <div class="modal-header">' +
        '               <button type="button" class="close extorio_modal_close" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        '               <h4 class="modal-title">' + options.title + '</h4>' +
        '           </div>' +
        '           <div class="modal-body">' +
        '               ' + options.content + '' +
        '           </div>' +
        '           <div class="modal-footer">' +
        '               <button type="button" class="btn btn-default extorio_modal_close">' + options.closetext + '</button>' +
        '           </div>' +
        '       </div>' +
        '   </div>' +
        '</div>' +
        '');
    if (options.width != undefined) {
        if ($.isNumeric(options.width)) {
            modal.find('.modal-dialog').attr("style", "width: " + options.width + "px;");
        } else {
            modal.find('.modal-dialog').attr("style", "width: " + options.width);
        }
    }
    dialog.onopen = options.onopen;
    dialog.onclose = options.onclose;
    dialog.onclosebutton = options.onclosebutton;
    dialog.on('hidden.bs.modal', function () {
        dialog.onclose();
        $(this).remove();
    }).on('show.bs.modal', function () {
        dialog.onopen();
    });
    dialog.find('.extorio_modal_close').on("click", function () {
        dialog.on('hidden.bs.modal', function () {
            dialog.onclosebutton();
            $(this).remove();
        }).modal('hide');
    });
    $('body').append(dialog.modal({backdrop: "static"}));
    return dialog;
};
// asset manager
var extorio_dialog_assetManagerDefaults = {
    title: "Assets",
    size: "",
    width: undefined,
    content: "Loading...",
    closetext: "Close",
    onopen: function () {
    },
    onclose: function () {
    },
    onclosebutton: function () {
    },
    onfileselected: function (url) {
    },
    onfilesloaded: function () {
    },
    extension: "Application",
    dirPath: "dirPath",
    allowfiltering: true,
    allowextensions: true,
    displayfolders: true,
    displaytypes: "null",
    selectable: false,
    columns: 4
};
$.extorio_dialog_assetManager = function (options) {
    var options = $.extend({}, extorio_dialog_assetManagerDefaults, options);
    var oldOnOpen = options.onopen;
    options.onopen = function () {
        var dialog = this;
        var body = dialog.find('.modal-body');
        body.extorio_fetchBlockProcessorViewContent("asset-manager", {
            extension: options.extension,
            dirPath: options.dirPath,
            allowfiltering: options.allowfiltering,
            allowextensions: options.allowextensions,
            displayfolders: options.displayfolders,
            displaytypes: options.displaytypes,
            selectable: options.selectable,
            columns: options.columns
        }, function () {
            dialog.find('.modal-body .asset_manager').on("on_files_loaded", function () {
                options.onfilesloaded();
            });
            dialog.find('.modal-body .asset_manager').on("on_file_selected", function (e, url) {
                dialog.modal("hide");
                options.onfileselected(url);
            });
        });
        oldOnOpen();
    };
    $.extorio_dialog(options);
};

//modal
var extorio_modalDefaults = {
    container: null,
    size: "",
    width: undefined,
    title: "Modal",
    content: "This is a modal!",
    closetext: "Close",
    continuetext: "Continue",
    onopen: function () {
    },
    onclose: function () {
    },
    onclosebutton: function () {

    },
    oncontinuebutton: function () {
    }
};
$.extorio_modal = function (options) {
    var options = $.extend({}, extorio_modalDefaults, options);
    var modal = $('' +
        '<div class="modal fade ">' +
        '   <div class="modal-dialog ' + options.size + '">' +
        '       <div class="modal-content">' +
        '           <div class="modal-header">' +
        '               <button type="button" class="close extorio_modal_close" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        '               <h4 class="modal-title">' + options.title + '</h4>' +
        '           </div>' +
        '           <div class="modal-body">' +
        '               ' + options.content + '' +
        '           </div>' +
        '           <div class="modal-footer">' +
        '               <button type="button" class="btn btn-default extorio_modal_close">' + options.closetext + '</button>' +
        '               <button id="extorio_modal_continue" type="button" class="btn btn-primary">' + options.continuetext + '</button>' +
        '           </div>' +
        '       </div>' +
        '   </div>' +
        '</div>' +
        '');
    if (options.width != undefined) {
        if ($.isNumeric(options.width)) {
            modal.find('.modal-dialog').attr("style", "width: " + options.width + "px;");
        } else {
            modal.find('.modal-dialog').attr("style", "width: " + options.width);
        }
    }
    modal.onopen = options.onopen;
    modal.onclose = options.onclose;
    modal.onclosebutton = options.onclosebutton;
    modal.oncontinuebutton = options.oncontinuebutton;
    modal.on('hidden.bs.modal', function () {
        modal.onclose();
        $(this).remove();
    }).on('show.bs.modal', function () {
        modal.onopen();
    }).find('#extorio_modal_continue').on("click", function () {
        modal.on('hidden.bs.modal', function () {
            modal.oncontinuebutton();
            $(this).remove();
        }).modal('hide');
    });
    modal.find('.extorio_modal_close').on("click", function () {
        modal.on('hidden.bs.modal', function () {
            modal.onclosebutton();
            $(this).remove();
        }).modal('hide');
    });
    $('body').append(modal.modal({backdrop: "static"}));
    return modal;
};

//plugin methods
$.fn.extorio_showLoader = function () {
    if (this.contents('.extorio_full_page_loader_outer').length == 0) {
        this.append($('' +
            '<div class="extorio_full_page_loader_outer">' +
            '   <div class="extorio_full_page_loader_inner">' +
            '       <img src="/Core/Assets/images/loader.gif" alt="loading...">' +
            '   </div>' +
            '</div>').hide().fadeIn(200));
    }
};
$.fn.extorio_hideLoader = function () {
    var loader = this.contents('.extorio_full_page_loader_outer');
    if (loader.length > 0) {
        loader.fadeOut({
            duration: 200,
            complete: function () {
                loader.remove();
            }
        });
    }
};

$.fn.extorio_fetchMVCContent = function (address, onFetch) {
    var self = this;
    self.onFetch = onFetch;
    $.ajax({
        url: address,
        data: {
            dispatch_fetch_mode: "mvc",
            dispatch_render_mode: "plain"
        },
        type: "GET",
        dataType: "text",
        error: function (a, b, c) {
            console.log(c);
        },
        success: function (data) {
            self.html(data);
            if (typeof onFetch == "function") {
                self.onFetch();
            }
        }
    });
};
$.fn.extorio_fetchPageLayoutContent = function (address, onFetch) {
    var self = this;
    self.onFetch = onFetch;
    $.ajax({
        url: address,
        data: {
            dispatch_fetch_mode: "page_mvc",
            dispatch_render_mode: "plain"
        },
        type: "GET",
        dataType: "text",
        error: function (a, b, c) {
            console.log(c);
        },
        success: function (data) {
            self.html(data);
            $('#layout_wrapper').layoutsystem();
            if (typeof onFetch == "function") {
                self.onFetch();
            }
        }
    });
};
$.fn.extorio_fetchPageContent = function (address, onFetch) {
    var self = this;
    self.onFetch = onFetch;
    $.ajax({
        url: address,
        data: {
            dispatch_fetch_mode: "template_page_mvc",
            dispatch_template_fetch_mode: "body",
            dispatch_render_mode: "plain"
        },
        type: "GET",
        dataType: "text",
        error: function (a, b, c) {
            console.log(c);
        },
        success: function (data) {
            self.html(data);
            $('#layout_wrapper').layoutsystem();
            if (typeof onFetch == "function") {
                self.onFetch();
            }
        }
    });
};
$.fn.extorio_fetchBlockLabelContent = function (blockid, onFetch) {
    var self = this;
    self.onFetch = onFetch;
    $.ajax({
        url: "/extorio/blocks/" + blockid + "/label",
        type: "GET",
        dataType: "text",
        error: function (a, b, c) {
            console.log(c);
        },
        success: function (data) {
            self.html(data);
            if (typeof onFetch == "function") {
                self.onFetch();
            }
        }
    });
};
$.fn.extorio_fetchBlockViewContent = function (blockid, onFetch) {
    var self = this;
    self.onFetch = onFetch;
    $.ajax({
        url: "/extorio/blocks/" + blockid + "/view",
        type: "GET",
        dataType: "text",
        error: function (a, b, c) {
            console.log(c);
        },
        success: function (data) {
            self.html(data);
            if (typeof onFetch == "function") {
                self.onFetch();
            }
        }
    });
};
$.fn.extorio_fetchBlockEditContent = function (blockid, onFetch) {
    var self = this;
    self.onFetch = onFetch;
    $.ajax({
        url: "/extorio/blocks/" + blockid + "/edit",
        type: "GET",
        dataType: "text",
        error: function (a, b, c) {
            console.log(c);
        },
        success: function (data) {
            if (data.length > 0) {
                self.html(data);
                if (typeof onFetch == "function") {
                    self.onFetch();
                }
            } else {
                self.html('<p>This block does not have any configuration options</p>');
            }
        }
    });
};
$.fn.extorio_fetchBlockProcessorViewContent = function (blockProcessorId, config, onFetch) {
    var self = this;
    self.onFetch = onFetch;
    $.ajax({
        url: "/extorio/block-processors/" + blockProcessorId,
        type: "GET",
        dataType: "text",
        data: config,
        error: function (a, b, c) {
            console.log(c);
        },
        success: function (data) {
            self.html(data);
            if (typeof onFetch == "function") {
                self.onFetch();
            }
        }
    });
};

$.fn.extorio_fetchUserAvatar = function (userId, onFetch) {
    var self = this;
    self.onFetch = onFetch;
    $.ajax({
        url: "/extorio/apis/users/" + userId + "/avatar",
        type: "GET",
        dataType: "json",
        error: function (a, b, c) {
            console.log(c);
        },
        success: function (response) {
            if (response.error) {
                console.log(response.error_message);
            } else {
                self.attr("src", response.data);
                if (typeof onFetch == "function") {
                    self.onFetch();
                }
            }
        }
    });
};
$.fn.extorio_fetchUserShortName = function (userId, onFetch) {
    var self = this;
    self.onFetch = onFetch;
    $.ajax({
        url: "/extorio/apis/users/" + userId + "/shortname",
        type: "GET",
        dataType: "json",
        error: function (a, b, c) {
            console.log(c);
        },
        success: function (response) {
            if (response.error) {
                console.log(response.error_message);
            } else {
                self.html(response.data);
                if (typeof onFetch == "function") {
                    self.onFetch();
                }
            }
        }
    });
};
$.fn.extorio_fetchUserLongName = function (userId, onFetch) {
    var self = this;
    self.onFetch = onFetch;
    $.ajax({
        url: "/extorio/apis/users/" + userId + "/longname",
        type: "GET",
        dataType: "json",
        error: function (a, b, c) {
            console.log(c);
        },
        success: function (response) {
            if (response.error) {
                console.log(response.error_message);
            } else {
                self.html(response.data);
                if (typeof onFetch == "function") {
                    self.onFetch();
                }
            }
        }
    });
};
$.fn.extorio_fetchUserBio = function (userId, onFetch) {
    var self = this;
    self.onFetch = onFetch;
    $.ajax({
        url: "/extorio/apis/users/" + userId + "/bio",
        type: "GET",
        dataType: "json",
        error: function (a, b, c) {
            console.log(c);
        },
        success: function (response) {
            if (response.error) {
                console.log(response.error_message);
            } else {
                self.html(response.data);
                if (typeof onFetch == "function") {
                    self.onFetch();
                }
            }
        }
    });
};
//seach input
$.fn.extorio_searchinput = function (onsearch) {
    return this.each(function () {
        if(typeof onsearch == "function") {
            var d = new Date();
            var typeTime = d.getTime();
            var self = $(this);
            self.onseach = onsearch;
            self.on('keydown', function () {
                var d = new Date();
                typeTime = d.getTime() + 499;
                setTimeout(function() {
                    var e = new Date();
                    if(e.getTime() > typeTime) {
                        self.onseach(self.val());
                    }
                },500);
            });
        }
    });
}
//proper serialization of form inputs
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

var setModalsAndBackdropsOrder = function () {
    var modalZIndex = 1040;
    var top = -15;
    $('.modal.in').each(function (index) {
        var $modal = $(this);
        modalZIndex++;
        top += 15;
        $modal.css('zIndex', modalZIndex);
        $modal.find('.modal-dialog').css("top", top + "px");
        $modal.next('.modal-backdrop.in').addClass('hidden').css('zIndex', modalZIndex - 1);
    });
    $('.modal.in:visible:last').focus().next('.modal-backdrop.in').removeClass('hidden');
    if ($('.modal:visible').length > 0) {
        $('body').addClass('modal-open');
    }
};
//fix modals overlapping
$(document).on('show.bs.modal', '.modal', function (event) {
    $(this).appendTo($('body'));
}).on('shown.bs.modal', '.modal.in', function (event) {
    setModalsAndBackdropsOrder();
}).on('hidden.bs.modal', '.modal', function (event) {
    setModalsAndBackdropsOrder();
});

//
// default message handlers
//
var extorio_message_index = 0;
$(document).on("extorio_message", function (a, data) {
    var messageContainer = $("#extorio_message_container");
    if (messageContainer.length == 0) {
        $('body').append('<div style="position: fixed; left: 35%; right: 35%; opacity: 0.8; top: 7%; z-index: 999999;" id="extorio_message_container"></div>');
        messageContainer = $("#extorio_message_container");
    }
    var html = '';
    switch (data.type) {
        case "error" :
            html = '' +
                '<div id="extorio_message_' + extorio_message_index + '" style="opacity:0;box-shadow: 0px 0px 5px 0px;" class="extorio_message alert alert-danger alert-dismissable" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                '   <span class="fa fa-exclamation-triangle"></span>  ' + data.message +
                '</div>';
            break;
        case "warning" :
            html = '' +
                '<div id="extorio_message_' + extorio_message_index + '" style="opacity:0;box-shadow: 0px 0px 5px 0px;" class="extorio_message alert alert-warning alert-dismissable" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                '   <span class="fa fa-exclamation-triangle"></span>  ' + data.message +
                '</div>';
            break;
        case "info" :
            html = '' +
                '<div id="extorio_message_' + extorio_message_index + '" style="opacity:0;box-shadow: 0px 0px 5px 0px;" class="extorio_message alert alert-info alert-dismissable" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                '   <span class="fa fa-exclamation-circle"></span>  ' + data.message +
                '</div>';
            break;
        case "success" :
            html = '' +
                '<div id="extorio_message_' + extorio_message_index + '" style="opacity:0;box-shadow: 0px 0px 5px 0px;" class="extorio_message alert alert-success alert-dismissable" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                '   <span class="fa fa-check"></span>  ' + data.message +
                '</div>';
            break;
    }
    messageContainer.append(html);
    var message = $('#extorio_message_' + extorio_message_index);
    message.animate({opacity: 1}, 600, "linear", function () {
        message.animate({opacity: 1}, 3000, "linear", function () {
            message.animate({opacity: 0}, 500, "linear", function () {
                message.remove();
            });
        });
    });
    extorio_message_index++;
});