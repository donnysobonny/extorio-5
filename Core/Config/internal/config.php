<?php
return array(
    "extorio" => array(
        "installation_complete" => false,
        "admin_created" => false,
        "database_connected" => false,
        "database_imported" => false,
        "dispatch_settings" => array(
            "all_keys" => array(
                "dispatch_auto_render",
                "dispatch_page_edit_mode",
                "dispatch_template_edit_mode",
                "dispatch_layout_edit_mode",
                "dispatch_render_mode",
                "dispatch_fetch_mode",
                "dispatch_template_fetch_mode",
                "dispatch_auto_parse_inline_blocks"
            ),
            "exposed_keys" => array(
                "dispatch_page_edit_mode",
                "dispatch_template_edit_mode",
                "dispatch_render_mode",
                "dispatch_fetch_mode",
                "dispatch_template_fetch_mode"
            )
        ),
        "authentication" => array(
            "session_type" => "basic",
            "jwt" => array(
                "key" => "extorio_jwt_key",
                "alg" => "HS256"
            )
        )
    ),
    "localization" => array(
        "default_language" => 1,
        "auto_translate" => false,
        "microsoft_credentials" => array(
            "client_id" => "",
            "client_secret" => ""
        )
    ),
    "autoload" => array(
        "Less_Parser" => "Core/Libs/less.php/Less.php",
        "Parsedown" => "Core/Libs/parsedown/Parsedown.php",
        "JWT" => "Core/Libs/jwt/JWT.php",
        "Swift_MailTransport" => "Core/Libs/swiftmailer/swift_required.php",
        "Swift_SendmailTransport" => "Core/Libs/swiftmailer/swift_required.php",
        "Swift_SmtpTransport" => "Core/Libs/swiftmailer/swift_required.php",
        "Swift_Message" => "Core/Libs/swiftmailer/swift_required.php",
        'MatthiasMullie\Minify\CSS' => "Core/Libs/minify/CSS.php",
        'MatthiasMullie\Minify\Minify' => "Core/Libs/minify/Minify.php",
        'MatthiasMullie\Minify\JS' => "Core/Libs/minify/JS.php",
        'MatthiasMullie\Minify\Exception' => "Core/Libs/minify/Exception.php",
        'MatthiasMullie\Minify\Exceptions\BasicException' => "Core/Libs/minify/Exceptions/BasicException.php",
        'MatthiasMullie\Minify\Exceptions\FileImportException' => "Core/Libs/minify/Exceptions/FileImportException.php",
        'MatthiasMullie\Minify\Exceptions\IOException' => "Core/Libs/minify/Exceptions/IOException.php",
        'MatthiasMullie\PathConverter\Converter' => "Core/Libs/path-converter/Converter.php",
    ),
    "includers" => array(
        "js_cookie" => array(
            "description" => "Includes the js cookie library for easily getting/setting cookies",
            "append_to_head" => '<script type="text/javascript" src="/Core/Assets/js-cookie/js/js-cookie.min.js"></script>'
        ),
        "froala_editor_minimal" => array(
            "description" => "Includes the minimal files needed to run the froala editor",
            "append_to_head" => '
                <link rel="stylesheet" href="/Core/Assets/froala/css/froala_editor.min.css" />
                <link rel="stylesheet" href="/Core/Assets/froala/css/froala_style.min.css" />

                <script type="text/javascript" src="/Core/Assets/froala/js/froala_editor.min.js"></script>
            '
        ),
        "froala_editor_advanced" => array(
            "description" => "Includes the files required to run a more advanced version of the froala editor",
            "required_includers" => array(
                "froala_editor_minimal"
            ),
            "append_to_head" => '
                <link rel="stylesheet" href="/Core/Assets/froala/css/plugins/char_counter.min.css">
                <link rel="stylesheet" href="/Core/Assets/froala/css/plugins/code_view.min.css">
                <link rel="stylesheet" href="/Core/Assets/froala/css/plugins/colors.min.css">
                <link rel="stylesheet" href="/Core/Assets/froala/css/plugins/image.min.css">
                <link rel="stylesheet" href="/Core/Assets/froala/css/plugins/image_manager.min.css">
                <link rel="stylesheet" href="/Core/Assets/froala/css/plugins/line_breaker.min.css">
                <link rel="stylesheet" href="/Core/Assets/froala/css/plugins/table.min.css">

                <script type="text/javascript" src="/Core/Assets/froala/js/plugins/align.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/froala/js/plugins/code_beautifier.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/froala/js/plugins/code_view.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/froala/js/plugins/colors.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/froala/js/plugins/entities.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/froala/js/plugins/font_family.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/froala/js/plugins/font_size.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/froala/js/plugins/image.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/froala/js/plugins/line_breaker.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/froala/js/plugins/link.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/froala/js/plugins/lists.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/froala/js/plugins/paragraph_format.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/froala/js/plugins/quote.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/froala/js/plugins/table.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/froala/js/plugins/url.min.js"></script>
            '
        ),
        "codemirror_minimal" => array(
            "description" => "Includes the minimal files needed to run the code mirror library",
            "append_to_head" => '
                <link rel="stylesheet" href="/Core/Assets/codemirror/lib/codemirror.css">
                <script type="text/javascript" src="/Core/Assets/codemirror/lib/codemirror.js"></script>
            '
        ),
        "codemirror_html_minimal" => array(
            "description" => "Includes the minimal files needed to run the code mirror library in html mode",
            "required_includers" => array(
                "codemirror_minimal"
            ),
            "append_to_head" => '<script type="text/javascript" src="/Core/Assets/codemirror/mode/xml/xml.js"></script>'
        ),
        "codemirror_html_advanced" => array(
            "description" => "Includes the files needed to run an advanced version of code mirror in html mode",
            "required_includers" => array(
                "codemirror_html_minimal"
            ),
            "append_to_head" => '
                <script type="text/javascript" src="/Core/Assets/codemirror/addon/fold/foldcode.js"></script>
                <script type="text/javascript" src="/Core/Assets/codemirror/addon/fold/indent-fold.js"></script>
                <script type="text/javascript" src="/Core/Assets/codemirror/addon/fold/xml-fold.js"></script>

                <script type="text/javascript" src="/Core/Assets/codemirror/addon/edit/matchbrackets.js"></script>
                <script type="text/javascript" src="/Core/Assets/codemirror/addon/edit/closebrackets.js"></script>
                <script type="text/javascript" src="/Core/Assets/codemirror/addon/edit/matchtags.js"></script>
                <script type="text/javascript" src="/Core/Assets/codemirror/addon/edit/closetag.js"></script>
            '
        ),
        "extorio_editable" => array(
            "description" => "Includes the files needed to initialize the basic editor",
            "required_includers" => array(
                "froala_editor_minimal"
            ),
            "append_to_head" => '<script type="text/javascript" src="/Core/Assets/js/Extorio.editable.min.js"></script>'
        ),
        "extorio_editable_advanced" => array(
            "description" => "Includes the files needed to initialize the advanced editor",
            "required_includers" => array(
                "froala_editor_advanced",
                "codemirror_html_advanced",
                "extorio_editable"
            )
        ),
        "extorio_model_viewer" => array(
            "append_to_head" => '
                <script type="text/javascript" src="/Core/Assets/js/Extorio.model-viewer.js"></script>
                <link rel="stylesheet" href="/Core/Assets/css/Extorio.model-viewer.css" />
            '
        ),
        "datatables" => array(
            "description" => "Includes the files to use datatables within bootstrap",
            "append_to_head" => '
                <link rel="stylesheet" href="/Core/Assets/datatables/css/datatables.bootstrap.css" />

                <script type="text/javascript" src="/Core/Assets/datatables/js/datatables.js"></script>
                <script type="text/javascript" src="/Core/Assets/datatables/js/datatables.bootstrap.js"></script>
            '
        ),
        "datepicker" => array(
            "description" => "Includes the files for a bootstrap-ready date-picker",
            "append_to_head" => '
                <link href="/Core/Assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
                <script type="text/javascript" src="/Core/Assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
            '
        ),
        "datetimepicker" => array(
            "description" => "Includes the files for a bootstrap-ready datetime-picker",
            "append_to_head" => '
                <link href="/Core/Assets/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
                <script type="text/javascript" src="/Core/Assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
            '
        ),
        "timeago" => array(
            "description" => "Includes the timeago jquery plugin",
            "append_to_head" => '<script type="text/javascript" src="/Core/Assets/timeago/js/timeago.min.js"></script>'
        ),
        "select" => array(
            "description" => "Includes the bootstrap select plugin",
            "append_to_body" => '<script type="text/javascript" src="/Core/Assets/bootstrap-select/dist/js/bootstrap-select.min.js"></script>'
        ),
        "selectsearch" => array(
            "description" => "Includes the selectsearch plugin",
            "append_to_head" => '<script type="text/javascript" src="/Core/Assets/js/Extorio.selectsearch.min.js"></script>'
        ),
        "typeahead" => array(
            "description" => "Includes the bootstrap typeahead plugin",
            "append_to_head" => '<script type="text/javascript" src="/Core/Assets/bootstrap-typeahead/js/bootstrap-typeahead.min.js"></script>'
        ),
        "tabcollapse" => array(
            "description" => "Includes the bootstrap tabcollapse plugin",
            "append_to_head" => '<script type="text/javascript" src="/Core/Assets/bootstrap-tabcollapse/js/bootstrap-tabcollapse.min.js"></script>'
        )
    ),
    "application" => array(
        "name" => "",
        "description" => "",
        "company" => "",
        "address" => "",
        "seo_visible" => true,
        "maintenance_mode" => false,
        "title_from" => "page",
        "favicon" => ""
    ),
    "database" => array(
        "name" => "",
        "username" => "",
        "password" => "",
        "host" => "localhost",
    ),
    "tasks" => array(
        "max_running_tasks" => 3,
        "clean_up_tasks_after_days" => 3
    ),
    "models" => array(
        "max_modcache_entries_per_instance" => 3,
        "webhooks" => array(
            "autofire" => false,
            "default_verification_key" => "extorio_webhook",
            "retry_delay" => 30,
            "max_retries" => 2
        )
    ),
    "extensions" => array(
        "Core" => array(
            "installed_version" => "5.0.0",
            "latest_version" => "5.0.0"
        )
    ),
    "users" => array(
        "user_groups" => array(
            "assign_type" => "single",
            "group" => 3,
            "groups" => array()
        ),
        "passwords" => array(
            "min_length" => 6,
            "contain_special" => false,
            "contain_number" => false
        ),
        "usernames" => array(
            "allow_number" => true,
            "allow_special" => true
        ),
        "emails" => array(
            "validate_emails" => true
        ),
        "defaults" => array(
            "avatar" => "/Core/Assets/images/default_avatar.png"
        ),
        "registration" => array(
            "enabled" => false,
            "authenticate_users" => true,
            "authentication_key" => "extorio_authentication_key",
            "authentication_email" => array(
                "subject" => "Authentication",
                "body" => '<p><strong>Dear **SHORTNAME**</strong></p><p><br></p><p>Thank you for registering with us at <a href="**APPLICATION_ADDRESS**" target="_blank">**APPLICATION_NAME**</a>. In order to complete the registration process, please click on the link below to authenticate your user account. If you are unable to click on the link below, please copy and paste it into your web-browser address bar.</p><p><br></p><p>**AUTHENTICATION_URL**</p><p><br></p><p>Thank you,</p><p>**APPLICATION_COMPANY**</p><p><a href="**APPLICATION_ADDRESS**" target="_blank">**APPLICATION_NAME**</a></p>',
            ),
            "confirm_users" => true
        ),
        "account_email" => array(
            "subject" => "Your User Details",
            "body" => '<p><strong>Dear **SHORTNAME**</strong></p><p><br></p><p>You may now log in at <a href="**APPLICATION_ADDRESS**" target="_blank">**APPLICATION_NAME**</a> by navigating to the <a href="http://**APPLICATION_ADDRESS**/user-login">user login page</a> and entering the details shown below.</p><p><br></p><p><strong>Username</strong>: **USERNAME**</p><p><strong>Password</strong>: **PASSWORD**</p><p><br></p><p>Please keep these details safe and for your own records.</p><p><br></p><p>Thank you,**APPLICATION_COMPANY**</p><p><a href="**APPLICATION_ADDRESS**" target="_blank">**APPLICATION_NAME**</a></p>'
        ),
        "password_reset" => array(
            "enabled" => true,
            "password_reset_key" => "extorio_password_reset_key",
            "password_reset_email" => array(
                "subject" => "Password Reset",
                "body" => '<p><strong>Dear **SHORTNAME**</strong></p><p><br></p><p>We are sorry that you need to reset your password. Not to worry, to continue to reset your password, please click on the link below and follow the instructions on-screen. If you are unable to click on the link below, please copy and paste it into your web-browser address bar.</p><p><br></p><p>**PASSWORD_RESET_URL**</p><p><br></p><p>Thank you,**APPLICATION_COMPANY**</p><p><a href="**APPLICATION_ADDRESS**" target="_blank">**APPLICATION_NAME**</a></p>'
            ),
            "confirm_password_reset" => true
        ),
        "can_change_username" => true,
        "can_change_email" => true,
        "can_change_password" => true
    ),
    "emails" => array(
        "transport" => "mail",
        "smtp" => array(
            "host" => "",
            "port" => 25,
            "username" => "",
            "password" => ""
        ),
        "defaults" => array(
            "from_name" => "",
            "from_email" => ""
        )
    ),
    "assets" => array(
        "max_size" => 10485760,
        "allowed_types" => array(
            "image/jpeg",
            "image/png",
            "image/gif",
            "video/mp4",
            "audio/mp3",
            "application/pdf",
            "text/plain"
        )
    ),
    "themes" => array(
        "smart_compile" => false,
        "smart_minify" => false
    )
);