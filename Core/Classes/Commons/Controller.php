<?php
namespace Core\Classes\Commons;

use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\Url;
use Core\Classes\Models\Page;
use Core\Classes\Utilities\Namespaces;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;

/**
 * The base class extended by all controller components. Pages use controllers to serve the business-end logic
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Controller
 */
class Controller extends Component {
    /**
     * Controllers call the default event when the target method has not been specified or doesn't exist
     */
    public function _onDefault() {
    }

    /**
     * We cached the url that takes us to the default method of the controller here, so that it can be re-used efficiently
     *
     * @var Url
     */
    private $urlToDefault = false;

    /**
     * Set whether extorio should auto render the content. Disable this if you want to define exactly how content is rendered.
     *
     * @param $bool
     */
    final protected function _setDispatchSetting_AutoRender($bool) {
        $this->_Extorio()->setDispatchSetting("dispatch_auto_render", $bool);
    }

    /**
     * Get whether extorio is set to auto render the content
     *
     * @return bool
     */
    final protected function _getDispatchSetting_AutoRender() {
        return $this->_Extorio()->getDispatchSetting("dispatch_auto_render");
    }

    /**
     * Set whether the content output from the MVC will be parsed for inline blocks
     *
     * @param $bool
     */
    final protected function _setDispatchSetting_AutoParseInlineBlocks($bool) {
        $this->_Extorio()->setDispatchSetting("dispatch_auto_parse_inline_blocks", $bool);
    }

    /**
     * Get whether the content output from the MVC will be parsed for inline blocks
     *
     * @return bool
     */
    final protected function _getDispatchSetting_AutoParseInlineBlocks() {
        return $this->_Extorio()->getDispatchSetting("dispatch_auto_parse_inline_blocks");
    }

    /**
     * Redirect to a page
     *
     * @param Page $page
     * @param array $methodParams
     * @param array $queryParams
     * @param int $statusCode
     */
    final protected function _redirectToPage($page, $methodParams = array(), $queryParams = array(), $statusCode = 302) {
        $url = $page->getUrl();
        $url->addFields($methodParams);
        $url->setQueryParams($queryParams);
        $url->redirect($statusCode);
    }

    /**
     * Redirect to the page that is currently set to the default page
     *
     * @param array $methodParams
     * @param array $queryParams
     * @param int $statusCode
     */
    final protected function _redirectToDefaultPage($methodParams = array(), $queryParams = array(), $statusCode = 302) {
        $page = Page::findByDefault();
        if ($page) {
            $this->_redirectToPage($page, $methodParams, $queryParams, $statusCode);
        }
    }

    /**
     * Redirect to the 404 page not found page
     *
     * @param array $methodParams
     * @param array $queryParams
     * @param int $statusCode
     */
    final protected function _redirectTo404PageNotFoundPage($methodParams = array(), $queryParams = array(), $statusCode = 302) {
        $page = Page::find404PageNotFOund();
        if ($page) {
            $this->_redirectToPage($page, $methodParams, $queryParams, $statusCode);
        }
    }

    /**
     * Redirect to the 401 access denied page
     *
     * @param array $methodParams
     * @param array $queryParams
     * @param int $statusCode
     */
    final protected function _redirectTo401AccessDeniedPage($methodParams = array(), $queryParams = array(), $statusCode = 302) {
        $page = Page::find401AccessDenied();
        if ($page) {
            $this->_redirectToPage($page, $methodParams, $queryParams, $statusCode);
        }
    }

    /**
     * Redirect to the controller's default method
     *
     * @param array $methodParams
     * @param array $queryParams
     * @param int $statusCode
     */
    final protected function _redirectToDefault($methodParams = array(), $queryParams = array(), $statusCode = 302) {
        $this->_getUrlToDefault($methodParams, $queryParams)->redirect($statusCode);
    }

    /**
     * Get the url to the default method
     *
     * @param array $methodParams
     * @param array $queryParams
     *
     * @return Url
     */
    final protected function _getUrlToDefault($methodParams = array(), $queryParams = array()) {
        $url = $this->_urlToDefault();
        $newUrl = $url->getUrl(true);
        foreach ($methodParams as $param) {
            $newUrl .= $param . "/";
        }
        $url->setUrl($newUrl);
        $url->setQueryParams($queryParams);
        return $url;
    }

    /**
     * Redirect to a target method on the controller
     *
     * @param $method
     * @param array $methodParams
     * @param array $queryParams
     * @param int $statusCode
     */
    final protected function _redirectToMethod($method, $methodParams = array(), $queryParams = array(), $statusCode = 302) {
        $this->_getUrlToMethod($method, $methodParams, $queryParams)->redirect($statusCode);
    }

    /**
     * Get the url to a target method on the controller
     *
     * @param $method
     * @param array $methodParams
     * @param array $queryParams
     *
     * @return Url
     */
    final protected function _getUrlToMethod($method, $methodParams = array(), $queryParams = array()) {
        $url = $this->_urlToDefault();
        $newUrl = $url->getUrl(true) . $method . "/";
        foreach ($methodParams as $param) {
            $newUrl .= $param . "/";
        }
        $url->setUrl($newUrl);
        $url->setQueryParams($queryParams);
        return $url;
    }

    /**
     * Internally get/cache the url to the default method
     *
     * @return Url
     */
    private function _urlToDefault() {
        if ($this->urlToDefault) {
            return clone($this->urlToDefault);
        } else {
            //find the page of this controller/view
            $page = Page::findOne(Query::n()->where(array('$or' => array("controllerNamespace" => Namespaces::getNamespace($this), "viewNamespace" => Namespaces::getNamespace($this)))));
            if ($page) {
                $this->urlToDefault = $page->getUrl();
            }
            return clone($this->urlToDefault);
        }
    }
}