<?php
namespace Core\Classes\Commons;
/**
 * Events are events that exist in extorio, and are therefore globally accessible to the whole application. Events are
 * designed to contain and run actions.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Event
 */
final class Event {
    /**
     * The unique name of the event
     *
     * @var string
     */
    public $eventName = "";
    /**
     * The actions bound to this event.
     *
     * @var Action[]
     */
    private $actions = array();

    /**
     * The action index, used to create unique indexes for actions
     *
     * @var int
     */
    private $actionIndex = 0;

    /**
     * Add an action to this event
     *
     * @param Action $action
     *
     * @return Action
     */
    public function addAction($action) {
        $this->actionIndex++;
        return $this->actions[$action->position.".".$this->actionIndex] = &$action;
    }

    /**
     * Run an event's actions in order of their position.
     *
     * If one or more actions are set to override other actions, the first (by position) is the only action that is run.
     *
     * If no action is set to overide, up to $maxActions actions will be returned as an array of actions.
     *
     * It will always be an array of actions that is returned, even if only one action is returned.
     *
     * @param mixed $args an array of arguments to send to each action
     * @param int $maxActions the maximum number of actions to return
     *
     * @return array
     */
    public function run($args=array(),$maxActions=INF) {
        //sort the actions by key
        ksort($this->actions);
        $results = array();
        //go through each action to check if there is one that overrides
        foreach($this->actions as $action) {
            if($action->overrideOtherActions) {
                $result = call_user_func_array(array($action,"run"),$args);
                if(!is_null($result)) {
                    $results[] = $result;
                }
                return $results;
            }
        }
        //no overrides were found, run all actions normally
        $n = 0;
        foreach($this->actions as $action) {
            if($n >= $maxActions) {
                break;
            }
            $result = call_user_func_array(array($action,"run"),$args);
            if(!is_null($result)) {
                $results[] = $result;
            }
            $n++;
        }
        return $results;
    }
}