<?php
namespace Core\Classes\Commons;
/**
 * An action object that can be bound to an event.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Action
 */
final class Action {
    /**
     * The Callable that is called by this action
     *
     * @var Callable
     */
    public $callable = null;
    /**
     * The position that this action sits in all actions on the same event. Actions are run in order of their position,
     * provided that no actions override others.
     *
     * @var int
     */
    public $position = 99;
    /**
     * Whether this action should override other actions. The first action that is found to have this set to true
     * overrides all other actions, meaning that the event will only run this action, and no others. If no actions have
     * this set to true, all actions will be run in order.
     *
     * @var bool
     */
    public $overrideOtherActions = false;

    /**
     * Run an action. This makes sure the callable function/method is callable first, and returns the result of the
     * callable, or null.
     *
     * @return mixed
     */
    public function run() {
        return call_user_func_array($this->callable,func_get_args());
    }
}