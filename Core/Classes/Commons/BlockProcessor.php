<?php
namespace Core\Classes\Commons;

/**
 * The base class extended by all block processor components. This is used to create dynamic, modular content.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class BlockProcessor
 */
class BlockProcessor extends Component {

    /**
     * A block processor is used by a Block model instance. The block passes it's config array to this property before
     * the block processor is processed.
     *
     * @var array
     */
    protected $config = array();
    /**
     * If the block processor is being used by a block, this is the id of the block being used
     *
     * @var int
     */
    protected $blockId = null;
    /**
     * Whether or not the block processor is being used inline
     *
     * @var bool
     */
    protected $inline = false;

    /**
     * Set whether this block processor is being used inline or not
     *
     * @param $inline
     */
    final public function _setInline($inline) {
        $this->inline = $inline;
    }

    /**
     * Set the id of the block using this block processor
     *
     * @param $blockId
     */
    final public function _setBlockId($blockId) {
        $this->blockId = $blockId;
    }

    /**
     * Set the config array for this block
     *
     * @param array $config
     */
    final public function _setConfig($config=array()) {
        $this->config = $config;
    }

    /**
     * The view event is called when the block is being displayed in the website
     *
     * @param array $config
     */
    final public function _view() {
        $this->_onView();
    }

    /**
     * The edit event is called when the block is being configured in the layout system
     *
     * @param array $config
     */
    final public function _edit() {
        $this->_onEdit();
    }

    /**
     * Use this method to alter what is displayed in the view event
     */
    protected function _onView() {

    }

    /**
     * Use this method to alter what is displayed in the edit event
     */
    protected function _onEdit() {

    }
}