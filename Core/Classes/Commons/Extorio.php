<?php
namespace Core\Classes\Commons;

use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Enums;
use Core\Classes\Enums\ContainerType;
use Core\Classes\Exceptions\General_Exception;
use Core\Classes\Helpers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\PGDB;
use Core\Classes\Models;
use Core\Classes\Utilities;

/**
 * The bread-and-butter of the framework. This class is an essential component for the running of the framework.
 *
 * This class is globally accessible via Extorio::get(); Components additionally have a helper method: _Extorio();
 *
 * This class is initiated at run time, and stored in the $_ENV["EXTORIO"] variable.
 *
 * Within this class, you can expect to find functionality that is useful to all areas of the framework. To name a few:
 * - getting/setting the logged in user
 * - fetching the content of pages, templates, and blocks
 * - running tasks now and in the background
 * - getting/setting messages
 * - writing to logs
 * - accessing extensions
 * - working with config
 * - etc
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Extorio
 */
final class Extorio {
    /**
     * Whether extoio is already initiated. This allows us to make sure extorio is only ever initiated once.
     *
     * @var bool
     */
    private $initiated = false;

    /**
     * Here we store the cwd, at the point that extorio is initiated, incase it changes later. This is used to generate
     * the local/absolute root of extensions
     *
     * @var string
     */
    private $cwd = "";

    /**
     * Stored PGDB connections
     *
     * @var PGDB[]
     */
    private $dbInstances = array();

    /**
     * Stored events
     *
     * @var Event[]
     */
    private $events = array();

    /**
     * The installed extensions
     *
     * @var Extension[]
     */
    private $extensions = array();

    /**
     * The compiled config
     *
     * @var array
     */
    private $config = null;
    /**
     * The currently logged in user. This is cached when we first try to get the logged in user.
     *
     * @var \Core\Classes\Models\User
     */
    private $loggedInUser = false;

    /**
     * The target page, based on the request uri
     *
     * @var \Core\Classes\Models\Page
     */
    private $targetPage = false;

    /**
     * The target title, originally set by the page title, but can be modified
     *
     * @var string
     */
    private $targetTitle = "";

    /**
     * The target breadcrumbs. If not set while fetching the MVC content, it will be auto-set based on the target page, and params.
     *
     * @var BreadCrumb[]
     */
    private $targetBreadCrumbs = array();

    /**
     * The target method on the target page (if applicable)
     *
     * @var bool
     */
    private $targetPageMethod = false;

    /**
     * Params being sent to the target method (if any)
     *
     * @var array
     */
    private $targetPageParams = array();

    /**
     * The target controller of the target page
     *
     * @var \Core\Classes\Commons\Controller
     */
    private $targetController = false;

    /**
     * The target view of the target page
     *
     * @var \Core\Classes\Commons\Controller
     */
    private $targetView = false;

    /**
     * The target theme of the current page
     *
     * @var \Core\Classes\Models\Theme
     */
    private $targetTheme = false;

    /**
     * The target template of the current page
     *
     * @var \Core\Classes\Models\Template
     */
    private $targetTemplate = false;

    /**
     * The current url object
     *
     * @var \Core\Classes\Helpers\Url
     */
    private $currentUrl = false;

    /**
     * whether to dispatch in page edit mode
     *
     * @var bool
     */
    private $dispatch_page_edit_mode = false;
    /**
     * whether to dispatch in template edit mode
     *
     * @var bool
     */
    private $dispatch_template_edit_mode = false;
    /**
     * whether to dispatch in layout edit mode
     *
     * @var bool
     */
    private $dispatch_layout_edit_mode = false;
    /**
     * The content that dispatch will fetch. Options are:
     * - template_page_mvc
     * - page_mvc
     * - mvc
     *
     * @var string
     */
    private $dispatch_fetch_mode = "template_page_mvc";
    /**
     * Whether dispatch should automatically render the content, based on the other dispatch settings. Setting this to
     * false will mean that the dispatch will not display any content. This is useful if you want to output custom
     * content, such as a json string for an api.
     *
     * @var bool
     */
    private $dispatch_auto_render = true;
    /**
     * The render mode. This is how the content will be "wrapped". For example, "html" will wrap the content in a html
     * document
     *
     * @var string
     */
    private $dispatch_render_mode = "html";
    /**
     * If the fetch mode is template_page_mvc, this tells us what parts of the template we fetch. The options are:
     * - head_body (fetch the head and the body of the template
     * - body (fetch only the contents of the body)
     *
     * @var string
     */
    private $dispatch_template_fetch_mode = "head_body";
    /**
     * Whether or not extorio will automatically parse mvc content for snippets. By default this is true.
     *
     * @var bool
     */
    private $dispatch_auto_parse_inline_blocks = true;
    /**
     * Whether or not extorio will automatically parse mvc content for translations
     *
     * @var bool
     */
    private $dispatch_auto_parse_inline_translations = true;

    /**
     * Since we can only read the input stream once, we cache the contents of the input stream here
     *
     * @var
     */
    private $inputStream;

    /**
     * Globally prepend content to the head
     *
     * @var string
     */
    private $prependToHead = "";
    /**
     * Globally append content to the head
     *
     * @var string
     */
    private $appendToHead = "";
    /**
     * Globally prepend content to the body
     *
     * @var string
     */
    private $prependToBody = "";
    /**
     * Globally append content to the body
     *
     * @var string
     */
    private $appendToBody = "";

    /**
     * A list of includers aleady been included
     *
     * @var array
     */
    private $includedIncluders = array();

    /**
     * An array of files that are required to initiate Extorio, before we can set up the auto loaders
     *
     * @var array
     */
    private $required_files = array(//common files
        "Core/Classes/Commons/Functions.php", "Core/Classes/Commons/Enum.php", "Core/Classes/Commons/Extension.php", "Core/Classes/Commons/Event.php", "Core/Classes/Commons/Action.php", "Core/Classes/Commons/FileSystem/PathInfo.php", "Core/Classes/Commons/FileSystem/Directory.php", "Core/Classes/Commons/FileSystem/File.php",

        //utilities
        "Core/Classes/Utilities/Arrays.php", "Core/Classes/Utilities/Extorio.php", "Core/Classes/Utilities/Server.php", "Core/Classes/Utilities/Strings.php", "Core/Classes/Utilities/Namespaces.php",

        //enums
        "Core/Classes/Enums/internal/PHPTypes.php", "Core/Classes/Enums/internal/InternalEvents.php", "Core/Classes/Enums/internal/InternalExtensions.php",

        //exceptions
        "Core/Classes/Exceptions/Extension_Exception.php", "Core/Classes/Exceptions/Extorio_Exception.php", "Core/Libs/php-gettext/gettext.inc");

    /**
     * These are the error types that are non-recoverable (cause execution to halt). We trigger our on_exception event on these errors, along with normal Exceptions.
     * All other error types are logged in the system_error.log file
     *
     * @var array
     */
    private $nonRecoverableErrors = array(E_ERROR, E_PARSE, E_CORE_ERROR, E_COMPILE_ERROR, E_USER_ERROR);

    /**
     * Initiate and dispatch Extorio. Can only be called once!
     *
     * @return Extorio
     */
    public static function init() {
        $extorio = false;
        session_start();
        try {
            //you can only init once!
            if (isset($_ENV["EXTORIO"])) {
                if (!$_ENV["EXTORIO"]->initiated) {
                    die("You can only initiate extorio once");
                }
            }

            //we set error reporting to ALL here. The handlers will decipher how to handle the error, using this basic logic:
            //  if the error is non-recoverable, use the on_exception event
            //  otherwise, just log the error in the system_error.log file
            ini_set('error_reporting', E_ALL);
            //disable error display
            ini_set('display_errors', 1);

            //ready to init!
            $extorio = new Extorio();
            $extorio->initiated = true;
            //get the current working directory that extorio was started from
            $extorio->cwd();
            //get the input stream straight away
            $extorio->getInputStream();
            $_ENV["EXTORIO"] = &$extorio;

            //require the required files
            foreach ($extorio->required_files as $file) {
                require_once $extorio->cwd() . "/" . $file;
            }

            //set up autoloading
            spl_autoload_register(array($extorio, "on_autoload"), false, false);

            //set up the system error and shutdown handlers to use the on_exception event
            set_error_handler(array($extorio, "on_system_error"));
            register_shutdown_function(array($extorio, "on_system_shutdown"));

            //set up the internal events
            foreach (Enums\InternalEvents::values() as $event) {
                $extorio->createEvent($event);
            }

            //construct the core
            $extorio->constructExtension('\Core\Components\Extension\Extension');;

            //find and construct installed and enabled extensions
            if ($extorio->installationComplete()) {
                foreach (Models\Extension::findAll(Helpers\Query::n()->where(array("isInstalled" => true, "isInternal" => false))) as $e) {
                    $extorio->constructExtension('\\' . $e->name . '\\Components\Extension\Extension');
                }
            }

            //construct the application
            $extorio->constructExtension('\Application\Components\Extension\Extension');

            //bind the on_exception method
            $extorio->bindActionToEvent(Enums\InternalEvents::_on_exception, array($extorio, "on_exception"));

            //if extorio is not yet installed, run the extorio install mvc. Otherwise, run extorio normally
            if (!$extorio->installationComplete()) {
                //start extensions
                foreach ($extorio->extensions as $ext) {
                    $ext->_start();
                }

                //run the extorio_on_start event
                $extorio->getEvent(Enums\InternalEvents::_extorio_on_start)->run();

                //find the controller/view in the Core
                $core = $extorio->getExtension("Core");
                $controller = $core->_constructComponent('\Core\Components\Controllers\ExtorioInstall');
                $view = $core->_constructComponent('\Core\Components\Views\ExtorioInstall');
                //fetch
                $content = $extorio->fetchMVCContent($controller, $view);
                //render
                $extorio->renderContent($content, Enums\DispatchRenderMode::html);
            } else {
                //find the targets
                $extorio->findTargets();

                foreach ($extorio->extensions as $ext) {
                    $ext->_start();
                }

                //update the dateActive field
                $user = $extorio->getLoggedInUser();
                if ($user) {
                    $extorio->getDbInstanceDefault()->query('UPDATE core_classes_models_user SET "dateActive" = $1 WHERE id = $2', array(date('Y-m-d H:i:s'), $user->id));
                }

                //run the extorio_on_start event
                $extorio->getEvent(Enums\InternalEvents::_extorio_on_start)->run();

                //set up localization
                T_setlocale(LC_MESSAGES, Utilities\Localization::getCurrentLanguage()->sLocale);
                T_bindtextdomain("messages", "Locale");
                T_bind_textdomain_codeset("messages", "UTF-8");
                T_textdomain("messages");

                //start constructing the content
                //we always fetch the mvc content
                $content = $extorio->fetchMVCContent($extorio->getTargetController(), $extorio->getTargetView(), $extorio->getTargetPageMethod(), $extorio->getTargetPageParams());
                //at this point, the dispatch settings cannot be modified internally
                //extract the settings from the url
                $extorio->extractDispatchSettingsFromUrl();
                if ($extorio->dispatch_auto_render) {
                    if ($extorio->dispatch_auto_parse_inline_blocks) {
                        $content = $extorio->parseInlineBlocks($content);
                    }
                    if ($extorio->dispatch_auto_parse_inline_translations) {
                        $content = $extorio->parseInlineTranslations($content);
                    }

                    $page = $extorio->getTargetPage();
                    $template = $extorio->getTargetTemplate();

                    //if the breadcrumbs are empty (not been set by the MVC), extract from the url
                    if (empty($extorio->targetBreadCrumbs)) {
                        $extorio->extractBreadCrumbsFromUrl();
                    }

                    //if the title is empty (not set by the MVC), extract from the breadcrumbs
                    if (!strlen($extorio->targetTitle)) {
                        $extorio->extractTitleFromBreadcrumbs();
                    }

                    //if edit mode for page/template, make sure we can modify
                    $loggedInUser = $extorio->getLoggedInUser();
                    if ($extorio->dispatch_page_edit_mode) {
                        $extorio->dispatch_page_edit_mode = Utilities\Pages::canUserModifyPage($loggedInUser->id, $page->id);
                    }
                    if ($extorio->dispatch_template_edit_mode) {
                        $extorio->dispatch_template_edit_mode = Utilities\Templates::canUserModifyTemplate($loggedInUser->id, $template->id);
                    }

                    if (in_array($extorio->dispatch_fetch_mode, array(Enums\DispatchFetchMode::template_page_mvc, Enums\DispatchFetchMode::page_mvc))) {
                        //get the page content
                        $content = $extorio->fetchPageLayoutContent($page, $content, $extorio->dispatch_page_edit_mode);
                    }

                    if ($extorio->dispatch_fetch_mode == Enums\DispatchFetchMode::template_page_mvc) {
                        //get the template content
                        $content = $extorio->fetchTemplateLayoutContent($template, $extorio->getTargetTheme(), $content, $extorio->dispatch_template_edit_mode, $extorio->dispatch_template_fetch_mode);
                    }

                    //finally, render the content
                    $extorio->renderContent($content, $extorio->dispatch_render_mode);
                } else {
                    //render the mvc if nothing else
                    echo $content;
                }
            }

            //run the extorio on end event
            $extorio->getEvent(Enums\InternalEvents::_extorio_on_end)->run();

        } catch (\Exception $ex) {
            //if the on_exception event exists
            $event = false;
            if ($extorio) {
                $event = $extorio->getEvent("on_exception");
                if ($event) {
                    $event->run(array($ex));
                }
            } else {
                if (!$event) {
                    //on exception event could not be triggered
                    echo "<pre>";
                    echo $ex;
                    echo "</pre>";
                }
            }
        }
        return $extorio;
    }

    /**
     * Get the cwd of the website root, regardless of any changes to the system's cwd
     *
     * @return string
     */
    public function cwd() {
        return $this->cwd ? $this->cwd : $this->cwd = str_replace("\\", "/", getcwd());
    }

    /**
     * Get/cache the input stream
     *
     * @return string
     */
    public function getInputStream() {
        if (is_null($this->inputStream)) {
            $this->inputStream = file_get_contents("php://input");
        }
        return $this->inputStream;
    }

    /**
     * Create a new event by name. Returns the newly created event.
     *
     * @param $eventName
     *
     * @return Event
     */
    public function createEvent($eventName) {
        //if this event already exists, do nothing?
        if ($this->getEvent($eventName)) {
            return $this->getEvent($eventName);
        } else {
            $event = new Event();
            $event->eventName = $eventName;
            return $this->events[$eventName] = &$event;
        }
    }

    /**
     * Get an event by name
     *
     * @param $eventName
     *
     * @return Event
     */
    public function getEvent($eventName) {
        if (isset($this->events[$eventName])) {
            return $this->events[$eventName];
        } else {
            return false;
        }
    }

    /**
     * Construct an extension component
     *
     * @param string $extensionClassName
     *
     * @return Extension
     */
    public function constructExtension($extensionClassName) {
        //if the extension is an internal one
        $extensionName = Utilities\Namespaces::getExtensionName($extensionClassName);
        $absoluteRoot = $this->cwd() . "/";
        if (in_array($extensionName, Enums\InternalExtensions::values())) {
            $absoluteRoot .= $extensionName;
            $localRoot = $extensionName;
            $internal = true;
        } else {
            $absoluteRoot .= "Extensions/" . $extensionName;
            $localRoot = "Extensions/" . $extensionName;
            $internal = false;
        }
        //if the file exists
        $file = File::constructFromPath($absoluteRoot . "/" . Utilities\Namespaces::getLoosePath($extensionClassName) . "/Extension.php");
        if ($file->exists()) {
            $file->requireFileOnce();
            //construct
            /** @var Extension $extension */
            $extension = new $extensionClassName();
            $extension->_extensionName = $extensionName;
            $extension->_absoluteRoot = $absoluteRoot;
            $extension->_localRoot = $localRoot;
            $extension->_internal = $internal;
            $this->extensions[$extensionName] = $extension;
            return $extension;
        } else {
            return false;
        }
    }

    /**
     * Discover whether installation is complete in the config
     *
     * @return bool
     */
    public function installationComplete() {
        $this->getConfig();
        if (isset($this->config["extorio"]["installation_complete"])) {
            return $this->config["extorio"]["installation_complete"];
        } else {
            return false;
        }
    }

    /**
     * Get the compiled config
     *
     * @return array
     */
    public function getConfig() {
        if (is_null($this->config)) {
            $this->getConfigCache();
        }
        return $this->config;
    }

    private function getConfigCache() {
        $file = File::constructFromPath($this->cwd() . "/Cache/config_cache.php");
        if ($file->exists()) {
            $this->config = $file->includeFile();
        } else {
            $this->cacheConfig();
        }
    }

    public function cacheConfig() {
        $config = array();
        foreach ($this->getExtensions() as $e) {
            $config = Utilities\Extorio::mergeConfigArrays($config, $e->_getConfig(true, true));
        }
        $file = File::createFromPath($this->cwd() . "/Cache/config_cache.php");
        if ($file) {
            $template = File::constructFromPath("Core/Assets/file-templates/config_cache.txt");
            if ($template) {
                $content = $template->read();
                if (!$content)
                    return false;

                $content = str_replace("*|CONFIG_ARRAY|*", var_export($config, true), $content);

                if (!$file->write($content))
                    return false;
            }
            $file->close();
        }
        $this->config = $config;
        return true;
    }

    /**
     * Get all started extensions
     *
     * @return Extension[]
     */
    public function getExtensions() {
        return $this->extensions;
    }

    /**
     * Bind an action to an event. Returns the newly created action
     *
     * @param string $eventName
     * @param Callable $callable
     * @param int $position where the action sits in the actions bound to this event. Actions are run in order of their positoin
     * @param bool $overrideOtherActions the first action that has this set to true, overrides all other actions on this event
     *
     * @return Action
     */
    public function bindActionToEvent($eventName, $callable, $position = 99, $overrideOtherActions = false) {
        $event = $this->getEvent($eventName);
        if ($event) {
            $action = new Action();
            $action->position = $position;
            $action->callable = $callable;
            $action->overrideOtherActions = $overrideOtherActions;
            return $event->addAction($action);
        } else {
            return false;
        }
    }

    /**
     * Get a started Extension
     *
     * @param $extensionName
     *
     * @return Extension
     */
    public function getExtension($extensionName) {
        if (isset($this->extensions[$extensionName])) {
            return $this->extensions[$extensionName];
        } else {
            return false;
        }
    }

    /**
     * Fetch the content of a controller and view component
     *
     * @param Controller $controller
     * @param Controller $view
     * @param bool $method
     * @param array $params
     *
     * @return string
     */
    public function fetchMVCContent($controller, $view, $method = false, $params = array()) {
        //capture everything within a buffer
        ob_start();
        if ($controller) {
            //work out whether the method is actually a method, or an additional param
            if ($method) {
                if (!is_callable(array($controller, $method))) {
                    array_unshift($params, $method);
                    $method = null;
                }
            }

            //begin
            $controller->_onBegin();

            //call the target method
            if ($method) {
                call_user_func_array(array($controller, $method), $params);
            } else {
                call_user_func_array(array($controller, "_onDefault"), $params);
            }

            //end
            $controller->_onEnd();

            if ($view) {
                //have the view inherit the controller's public properties
                foreach ($controller as $key => $value) {
                    if (!in_array($key, array("moduleType", "extensionName", "extensionRoot"))) {
                        $view->$key = $value;
                    }
                }

                //before we call the view methods, we need to make sure the view has overridden the controller
                //methods
                $viewReflector = new \ReflectionClass($view);
                $viewClassName = get_class($view);

                if ($viewReflector->getMethod("_onBegin")->class == $viewClassName) {
                    //begin
                    $view->_onBegin();
                }

                //call the target method
                if ($method) {
                    if ($viewReflector->getMethod($method)->class == $viewClassName) {
                        call_user_func_array(array($view, $method), $params);
                    }
                } else {
                    if ($viewReflector->getMethod("_onDefault")->class == $viewClassName) {
                        call_user_func_array(array($view, "_onDefault"), $params);
                    }
                }

                if ($viewReflector->getMethod("_onEnd")->class == $viewClassName) {
                    //end
                    $view->_onEnd();
                }
            }
        }

        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    /**
     * Render content, wrapping the content based on the render mode. This also sets the content type accordingly
     *
     * @param $content
     * @param string $renderMode
     */
    public function renderContent($content, $renderMode = "html") {
        switch ($renderMode) {
            case Enums\DispatchRenderMode::html :
                header("Content-Type: text/html;");
                $content = '<!DOCTYPE html>
<html>
    ' . $content . '
</html>
                ';
                echo $content;
                break;
            case Enums\DispatchRenderMode::plain :
                header("Content-Type: text/plain;");
                echo $content;
                break;
        }

    }

    /**
     * This method is used to find the target page, controller, view, theme and template based on the request url
     */
    private function findTargets() {
        //first we need to find the page
        $page = false;
        $this->currentUrl = Helpers\Url::n(Utilities\Server::getRequestURI());
        $url = $this->currentUrl->getUrl();
        //if no url, or url is "/", is there a default page?
        if ($url == "/") {
            $page = Models\Page::findByDefault();
        } else {
            //find the closest matching page
            $db = $this->getDbInstanceDefault();
            $sql = 'SELECT id FROM core_classes_models_page WHERE address in (';
            $params = array();
            $index = 1;
            $parts = $this->currentUrl->getFields();
            $current = "/";
            $in = "";
            foreach ($parts as $part) {
                $current .= $part . "/";
                $in .= "$" . $index . ",";
                $params[] = $current;
                $index++;
            }
            $sql .= substr($in, 0, strlen($in) - 1);
            $sql .= ') ORDER BY LENGTH(address) DESC LIMIT 1';
            $row = $db->query($sql, $params)->fetchRow();
            if ($row) {
                $page = Models\Page::findById($row[0], 3);
            }
        }
        //if no page, the page is not public, or the publish dates are out of bounds, set the page to 404 page not found
        if (!$page) {
            $page = Models\Page::find404PageNotFOund();
        } else {
            if (!$page->isPublic || (!is_null($page->publishDate) && time() < strtotime($page->publishDate)) || (!is_null($page->unpublishDate) && time() > strtotime($page->unpublishDate))) {
                $page = Models\Page::find404PageNotFOund();
            }
        }
        //make sure we can read the target page
        if (!Utilities\Pages::canUserReadPage($this->getLoggedInUserId(), $page->id)) {
            $page = Models\Page::findById($page->readFailPageId, 3);
            if (!$page) {
                $page = Models\Page::find401AccessDenied();
            }
        }
        //if on admin page, make sure we can be here
        if ($this->onExtorioAdminPage()) {
            if (!Utilities\Users::userHasPrivilege($this->getLoggedInUserId(), "extorio_admin", "Core")) {
                $page = Models\Page::findExtorioAdminLogin();
            }
        }
        //if we are not currently on the target page, redirect to it
        if (strpos(Utilities\Strings::startsAndEndsWith($url, "/"), $page->address) === false) {
            $page->redirect(array("r" => Utilities\Server::getRequestURI()));
        }
        //if we have gotten to this point, we have a page
        $this->targetPage = $page;
        //do we need to respond with a 404, or 401?
        if ($this->targetPage->address == Enums\InternalPages::_404PageNotFound) {
            header("HTTP/1.1 404 Not Found");
        }
        if ($this->targetPage->address == Enums\InternalPages::_401AccessDenied || $this->targetPage->address == Enums\InternalPages::_extorioAdmin401AccessDenied) {
            header("HTTP/1.1 401 Access Denied");
        }
        //extract the target controller and view
        if (strlen($this->targetPage->controllerNamespace)) {
            $extension = $this->getExtension($this->targetPage->extensionName);
            if ($extension) {
                $this->targetController = $extension->_constructComponent($this->targetPage->controllerNamespace);
                if (strlen($this->targetPage->viewNamespace)) {
                    $this->targetView = $extension->_constructComponent($this->targetPage->viewNamespace);
                }
            }
        }
        //extract the theme
        if ($this->targetPage->theme) {
            $this->targetTheme = $this->targetPage->theme;
        } else {
            $this->targetTheme = Models\Theme::findByDefault();
        }
        //extract the template
        if ($this->targetPage->template) {
            $this->targetTemplate = $this->targetPage->template;
        } else {
            $this->targetTemplate = Models\Template::findByDefault();
        }

        //if we have a target controller, discover the target method and params
        if ($this->targetController) {
            //get the fields in the url that extend the page's address
            $url = Utilities\Server::getRequestURL();
            $url = Utilities\Strings::startsAndEndsWith($url, "/");
            $additional = str_replace($this->targetPage->address, "", $url);
            $additional = Utilities\Strings::notStartsAndNotEndsWith($additional, "/");
            $this->targetPageParams = array();
            if (strlen($additional)) {
                $fields = explode("/", $additional);
                if (count($fields)) {
                    $method = Utilities\Strings::replaceNonWordCharacters(urldecode($fields[0]), "_");
                    if (is_callable(array($this->targetController, $method))) {
                        $this->targetPageMethod = $method;
                        array_shift($fields);
                    }
                }
                for ($i = 0; $i < count($fields); $i++) {
                    $this->targetPageParams[] = urldecode($fields[$i]);
                }
            }
        }
    }

    /**
     * Get the default db instance
     *
     * @return PGDB
     */
    public function getDbInstanceDefault() {
        $this->getConfig();
        if ($this->getDbInstance("default")) {
            return $this->getDbInstance("default");
        } else {
            return $this->createDbInstance("default", $this->config["database"]["name"], $this->config["database"]["username"], $this->config["database"]["password"], $this->config["database"]["host"]);
        }
    }

    /**
     * Get a db instance by name
     *
     * @param $dbInstanceName
     *
     * @return PGDB
     */
    public function getDbInstance($dbInstanceName) {
        if (isset($this->dbInstances[$dbInstanceName])) {
            return $this->dbInstances[$dbInstanceName];
        } else {
            return false;
        }
    }

    /**
     * Create a db instance
     *
     * @param $dbInstanceName
     * @param $dbName
     * @param $dbUser
     * @param $dbPass
     * @param string $dbHost
     *
     * @return PGDB
     */
    public function createDbInstance($dbInstanceName, $dbName, $dbUser, $dbPass, $dbHost = "localhost") {
        if ($db = $this->getDbInstance($dbInstanceName)) {
            return $db;
        }
        return $this->dbInstances[$dbInstanceName] = PGDB::n($dbName, $dbUser, $dbPass, $dbHost);
    }

    /**
     * Get the id of the logged in user
     *
     * @return int
     */
    public function getLoggedInUserId() {
        return $this->getLoggedInUser()->id;
    }

    /**
     * Get the logged in user
     *
     * @return Models\User
     */
    public function getLoggedInUser() {
        if ($this->loggedInUser) {
            return $this->loggedInUser;
        }
        $session = $this->getLoggedInUserSession();
        if ($session) {
            $ip = Utilities\Server::getIpAddress();
            $db = $this->getDbInstanceDefault();
            $sql = 'SELECT "userId" FROM core_classes_models_usersession WHERE session = $1 AND "expiryDate" >= $2 LIMIT 1';
            $params = array($session, date("Y-m-d H:i:s"));
            $row = $db->query($sql, $params)->fetchRow();
            if ($row) {
                return $this->loggedInUser = Models\User::findById($row[0], 2);
            }
        }
        return false;
    }

    /**
     * Get the logged in user's session or NULL if no session could be found/validated
     *
     * @return string
     */
    public function getLoggedInUserSession() {
        $this->getConfig();
        switch ($this->config["extorio"]["authentication"]["session_type"]) {
            case "basic" :
                if (isset($_SESSION["user_session"])) {
                    return $_SESSION["user_session"];
                } elseif (isset($_COOKIE["user_session"])) {
                    return Models\UserSession::findOne(Helpers\Query::n()->where(array("session" => $_COOKIE["user_session"], "remoteIP" => Utilities\Server::getIpAddress())))->session;
                } elseif (isset($_GET["user_session"])) {
                    return Models\UserSession::findOne(Helpers\Query::n()->where(array("session" => $_GET["user_session"], "remoteIP" => Utilities\Server::getIpAddress())))->session;
                }
                break;
            case "jwt" :
                $token = "";
                if (isset($_SESSION["user_session"])) {
                    $token = $_SESSION["user_session"];
                } elseif (isset($_COOKIE["user_session"])) {
                    $token = Models\UserSession::findOne(Helpers\Query::n()->where(array("session" => $_COOKIE["user_session"], "remoteIP" => Utilities\Server::getIpAddress())))->session;
                } elseif (isset($_GET["user_session"])) {
                    $token = Models\UserSession::findOne(Helpers\Query::n()->where(array("session" => $_GET["user_session"], "remoteIP" => Utilities\Server::getIpAddress())))->session;
                }
                try {
                    $decoded = \JWT::decode($token, $this->config["extorio"]["authentication"]["jwt"]["key"], $this->config["extorio"]["authentication"]["jwt"]["alg"]);
                    //make sure the issuer is correct
                    if ($decoded->iss == $this->config["application"]["address"]) {
                        return $decoded->user_session;
                    }
                } catch (\Exception $ex) {
                    //log this?
                }
                break;
        }
        return null;
    }

    /**
     * Get whether we are on an extorio admin page
     *
     * @return bool
     */
    public function onExtorioAdminPage() {
        return substr(Utilities\Strings::startsAndEndsWith(Utilities\Server::getRequestURL(), "/"), 0, 15) == "/extorio-admin/";
    }

    /**
     * Get the page's target controller
     *
     * @return Controller
     */
    public function getTargetController() {
        return $this->targetController;
    }

    /**
     * Get the page's target view
     *
     * @return Controller
     */
    public function getTargetView() {
        return $this->targetView;
    }

    /**
     * Get the page's target method
     *
     * @return bool
     */
    public function getTargetPageMethod() {
        return $this->targetPageMethod;
    }

    /**
     * Get the target method's params
     *
     * @return array
     */
    public function getTargetPageParams() {
        return $this->targetPageParams;
    }

    /**
     * Extract the dispatch settings from the query string of the request
     *
     */
    public function extractDispatchSettingsFromUrl() {
        $parsed = array();
        parse_str(Utilities\Server::getQueryString(), $parsed);
        $this->getConfig();
        foreach ($this->config["extorio"]["dispatch_settings"]["exposed_keys"] as $key) {
            if (isset($parsed[$key])) {
                switch ($parsed[$key]) {
                    case "true" :
                        $this->$key = true;
                        break;
                    case "false" :
                        $this->$key = false;
                        break;
                    default :
                        $this->$key = $parsed[$key];
                        break;
                }
            }
        }
    }

    /**
     * Parse content for inline blocks
     *
     * @param string $content
     *
     * @return string
     * @throws \Core\Classes\Exceptions\Extension_Exception
     */
    public function parseInlineBlocks($content) {
        $iss = $this->getInlineSnippets("b", $content);
        foreach ($iss as $is) {
            //strip out the type and config
            $type = "";
            $config = array();
            $qpos = strpos($is->match, "?");
            if ($qpos > 0) {
                $type = substr($is->match, 0, $qpos);
                $query = substr($is->match, $qpos + 1, (strlen($is->match) - $qpos) - 1);
                if (strlen($query)) {
                    parse_str($query, $config);
                }
            } else {
                $type = $is->match;
            }
            $b = Models\BlockProcessor::findOne(Helpers\Query::n()->where(array("label" => $type, "inlineEnabled" => true)), 2);
            $parsed = false;
            if ($b) {
                $loggedInUser = $this->getLoggedInUser();
                if (Utilities\BlockProcessors::canUserInlineBlockProcessor($loggedInUser ? $loggedInUser->id : 0, $b->id)) {
                    //fetch the content
                    $e = $this->getExtension($b->extensionName);
                    if ($e) {
                        $blockProcessor = $e->_constructComponent($b->namespace);
                        if ($blockProcessor) {
                            $content = str_replace($is->orig, $this->fetchBlockProcessorViewContent($blockProcessor, $config, null, true), $content);
                            $parsed = true;
                        }
                    }
                }
            }
            if (!$parsed) {
                //clear the syntax
                $content = str_replace($is->orig, "", $content);
            } else {
                //re-parse, in case there is any inline code within the new content
                $content = $this->parseInlineBlocks($content);
            }
        }
        return $content;
    }

    /**
     * Get any inline snippets within some content
     *
     * @param string $tag
     * @param string $content
     *
     * @return Helpers\InlineSnippet[]
     */
    public function getInlineSnippets($tag, $content) {
        $snippets = array();
        $output = array();
        if (preg_match_all("/{" . $tag . "}(.+?){\/" . $tag . "}/", $content, $output)) {
            foreach ($output[0] as $i => $orig) {
                $snippets[] = Helpers\InlineSnippet::n($orig, $output[1][$i]);
            }
        }
        return $snippets;
    }

    /**
     * Fetch the view content of a block processor
     *
     * @param BlockProcessor $blockProcessor
     * @param array $config
     *
     * @return string
     */
    public function fetchBlockProcessorViewContent($blockProcessor, $config = array(), $blockId = null, $inline = false) {
        return $this->fetchBlockProcessorContent("view", $blockProcessor, $config, $blockId, $inline);
    }

    /**
     * Fetch the block processor content by type
     *
     * @param string $type
     * @param BlockProcessor $blockProcessor
     * @param array $config
     *
     * @return string
     */
    private function fetchBlockProcessorContent($type, $blockProcessor, $config = array(), $blockId = null, $inline = false) {
        ob_start();
        $container_type = "none";
        $container_title = "";
        if ($blockProcessor) {
            $blockProcessor->_setBlockId($blockId);
            $blockProcessor->_setInline($inline);
            if (is_string($config)) {
                $a = array();
                parse_str($config, $a);
                $config = $a;
            }

            //smart cast the config
            $config = Utilities\Arrays::smartCast($config);

            //the config may specify a container type, container name
            if (isset($config["container_type"]))
                $container_type = $config["container_type"];
            if (isset($config["container_title"]))
                $container_title = $config["container_title"];

            //pass the config to the block processor as a fall-back
            $blockProcessor->_setConfig($config);
            //if the config is not empty, we are going to override the public properties of the block processor
            if (!empty($config)) {
                foreach ($blockProcessor as $key => $value) {
                    if (isset($config[$key])) {
                        $blockProcessor->$key = $config[$key];
                    } else {
                        //if the property is not in the config, and the public property has a default value of type bollean,
                        //we force it to be FALSE, to help simulate unchecked checkboxes
                        if (gettype($blockProcessor->$key) == Enums\PHPTypes::_boolean) {
                            $blockProcessor->$key = false;
                        }
                    }
                }
            }

            switch ($type) {
                case "edit" :
                    ?>
                    <form id="extorio_block_edit_form">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Container options</h3>
                            </div>
                            <div style="padding-bottom: 0;" class="panel-body">
                                <div class="form-group">
                                    <label for="container_type">Container type</label>
                                    <select class="form-control" id="container_type" name="container_type">
                                        <option value="none">none</option>
                                        <option <?php
                                        if ($container_type == "panel-default")
                                            echo 'selected="selected"';
                                        ?> value="panel-default">Panel (default)
                                        </option>
                                        <option <?php
                                        if ($container_type == "panel-primary")
                                            echo 'selected="selected"';
                                        ?> value="panel-primary">Panel (primary)
                                        </option>
                                        <option <?php
                                        if ($container_type == "panel-success")
                                            echo 'selected="selected"';
                                        ?> value="panel-success">Panel (success)
                                        </option>
                                        <option <?php
                                        if ($container_type == "panel-info")
                                            echo 'selected="selected"';
                                        ?> value="panel-info">Panel (info)
                                        </option>
                                        <option <?php
                                        if ($container_type == "panel-warning")
                                            echo 'selected="selected"';
                                        ?> value="panel-warning">Panel (warning)
                                        </option>
                                        <option <?php
                                        if ($container_type == "panel-danger")
                                            echo 'selected="selected"';
                                        ?> value="panel-danger">Panel (danger)
                                        </option>
                                        <option <?php
                                        if ($container_type == "well-small")
                                            echo 'selected="selected"';
                                        ?> value="well-small">Well (small)
                                        </option>
                                        <option <?php
                                        if ($container_type == "well-medium")
                                            echo 'selected="selected"';
                                        ?> value="well-medium">Well (medium)
                                        </option>
                                        <option <?php
                                        if ($container_type == "well-large")
                                            echo 'selected="selected"';
                                        ?> value="well-large">Well (large)
                                        </option>
                                        <option <?php
                                        if ($container_type == "alert-info")
                                            echo 'selected="selected"';
                                        ?> value="alert-info">Alert (info)
                                        </option>
                                        <option <?php
                                        if ($container_type == "alert-success")
                                            echo 'selected="selected"';
                                        ?> value="alert-success">Alert (success)
                                        </option>
                                        <option <?php
                                        if ($container_type == "alert-warning")
                                            echo 'selected="selected"';
                                        ?> value="alert-warning">Alert (warning)
                                        </option>
                                        <option <?php
                                        if ($container_type == "alert-danger")
                                            echo 'selected="selected"';
                                        ?> value="alert-danger">Alert (danger)
                                        </option>
                                        <option <?php
                                        if ($container_type == "jumbotron-fluid")
                                            echo 'selected="selected"';
                                        ?> value="jumbotron-fluid">Jumbotron (fluid)
                                        </option>
                                        <option <?php
                                        if ($container_type == "jumbotron-fixed")
                                            echo 'selected="selected"';
                                        ?> value="jumbotron-fixed">Jumbotron (fixed)
                                        </option>
                                        <option <?php
                                        if ($container_type == "thumbnail")
                                            echo 'selected="selected"';
                                        ?> value="thumbnail">Thumbnail
                                        </option>
                                    </select>
                                </div>
                                <div style="<?php
                                if ($container_type != "panel-default" && $container_type != "panel-primary" && $container_type != "panel-success" && $container_type != "panel-info" && $container_type != "panel-warning" && $container_type != "panel-danger") {
                                    echo 'display: none;';
                                }
                                ?>" id="container_title_container" class="form-group">
                                    <label for="container_title">Panel Title (optional)</label>
                                    <input value="<?= $container_title ?>" type="text" class="form-control"
                                           id="container_title" name="container_title" placeholder="Title (optional)">
                                </div>
                                <script>
                                    $(function () {
                                        $('#container_type').on("change", function () {
                                            var val = $(this).val();
                                            if (
                                                val == "panel-default" ||
                                                val == "panel-primary" ||
                                                val == "panel-success" ||
                                                val == "panel-info" ||
                                                val == "panel-warning" ||
                                                val == "panel-danger"
                                            ) {
                                                $('#container_title_container').fadeIn();
                                            } else {
                                                $('#container_title_container').fadeOut();
                                            }
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                        <?php
                        $blockProcessor->_onBegin();
                        $blockProcessor->_edit();
                        $blockProcessor->_onEnd();
                        ?>
                    </form>
                    <?php
                    $content = ob_get_contents();
                    ob_end_clean();
                    return $content;
                    break;
                case "view" :
                    $blockProcessor->_onBegin();
                    $blockProcessor->_view();
                    $blockProcessor->_onEnd();
                    $content = ob_get_contents();
                    ob_end_clean();
                    return $this->fetchContainedContent($container_type, $container_title, $content);
                    break;
            }
        }
    }

    public function fetchContainedContent($containerType = "none", $containerTitle = "", $content) {
        ob_start();
        switch ($containerType) {
            case ContainerType::_panelDefault :
                ?>
                <div class="panel panel-default">
                    <?php
                    if (strlen($containerTitle)) {
                        ?>
                        <div class="panel-heading">
                            <h3 class="panel-title"><?= $containerTitle ?></h3>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="panel-body">
                        <?= $content ?>
                    </div>
                </div>
                <?php
                break;
            case ContainerType::_panelPrimary :
                ?>
                <div class="panel panel-primary">
                    <?php
                    if (strlen($containerTitle)) {
                        ?>
                        <div class="panel-heading">
                            <h3 class="panel-title"><?= $containerTitle ?></h3>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="panel-body">
                        <?= $content ?>
                    </div>
                </div>
                <?php
                break;
            case ContainerType::_panelSuccess :
                ?>
                <div class="panel panel-success">
                    <?php
                    if (strlen($containerTitle)) {
                        ?>
                        <div class="panel-heading">
                            <h3 class="panel-title"><?= $containerTitle ?></h3>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="panel-body">
                        <?= $content ?>
                    </div>
                </div>
                <?php
                break;
            case ContainerType::_panelInfo :
                ?>
                <div class="panel panel-info">
                    <?php
                    if (strlen($containerTitle)) {
                        ?>
                        <div class="panel-heading">
                            <h3 class="panel-title"><?= $containerTitle ?></h3>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="panel-body">
                        <?= $content ?>
                    </div>
                </div>
                <?php
                break;
            case ContainerType::_panelWarning :
                ?>
                <div class="panel panel-warning">
                    <?php
                    if (strlen($containerTitle)) {
                        ?>
                        <div class="panel-heading">
                            <h3 class="panel-title"><?= $containerTitle ?></h3>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="panel-body">
                        <?= $content ?>
                    </div>
                </div>
                <?php
                break;
            case ContainerType::_panelDanger :
                ?>
                <div class="panel panel-danger">
                    <?php
                    if (strlen($containerTitle)) {
                        ?>
                        <div class="panel-heading">
                            <h3 class="panel-title"><?= $containerTitle ?></h3>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="panel-body">
                        <?= $content ?>
                    </div>
                </div>
                <?php
                break;
            case ContainerType::_wellSmall :
                ?>
                <div class="well well-sm">
                    <?= $content ?>
                </div>
                <?php
                break;
            case ContainerType::_wellMedium :
                ?>
                <div class="well">
                    <?= $content ?>
                </div>
                <?php
                break;
            case ContainerType::_wellLarge :
                ?>
                <div class="well well-lg">
                    <?= $content ?>
                </div>
                <?php
                break;
            case ContainerType::_alertInfo :
                ?>
                <div class="alert alert-info">
                    <?= $content ?>
                </div>
                <?php
                break;
            case ContainerType::_alertSuccess :
                ?>
                <div class="alert alert-success">
                    <?= $content ?>
                </div>
                <?php
                break;
            case ContainerType::_alertWarning :
                ?>
                <div class="alert alert-warning">
                    <?= $content ?>
                </div>
                <?php
                break;
            case ContainerType::_alertDanger :
                ?>
                <div class="alert alert-danger">
                    <?= $content ?>
                </div>
                <?php
                break;
            case ContainerType::_jumbotronFluid :
                ?>
                <div class="jumbotron">
                    <?= $content ?>
                </div>
                <?php
                break;
            case ContainerType::_jumbotronFixed :
                ?>
                <div class="jumbotron">
                    <div class="container">
                        <?= $content ?>
                    </div>
                </div>
                <?php
                break;
            case ContainerType::_thumbnail :
                ?>
                <div class="thumbnail">
                    <?= $content ?>
                </div>
                <?php
                break;
            default :
                echo $content;
                break;
        }
        $c = ob_get_contents();
        ob_end_clean();
        return $c;
    }

    /**
     * Parse content for inline translations
     *
     * @param string $content
     *
     * @return string
     */
    public function parseInlineTranslations($content) {
        //single translations
        $iss = $this->getInlineSnippets("t", $content);
        foreach ($iss as $is) {
            $content = str_replace($is->orig, t($is->match), $content);
        }
        //plural
        $iss = $this->getInlineSnippets("tn", $content);
        foreach ($iss as $is) {
            $parts = explode("|", $is->orig);
            $content = str_replace($is->orig, tn($parts[0], $parts[1], intval($parts[2])), $content);
        }
        return $content;
    }

    /**
     * Get the target page
     *
     * @return \Core\Classes\Models\Page
     */
    public function getTargetPage() {
        return $this->targetPage;
    }

    /**
     * Get the page's target template
     *
     * @return Models\Template
     */
    public function getTargetTemplate() {
        return $this->targetTemplate;
    }

    /**
     * Extract the breadcrumbs from the current url
     */
    public function extractBreadCrumbsFromUrl() {
        $this->targetBreadCrumbs = array();
        $address = "/";
        $fields = $this->currentUrl->getFields();
        for ($i = 0; $i < count($fields); $i++) {
            $field = $fields[$i];
            $address .= urlencode($field) . "/";
            //add the breadcrumb
            $this->addBreadCrumb(BreadCrumb::n(($i + 1) == count($fields), Utilities\Strings::titleSafe($field), $address));
        }
    }

    /**
     * Append a breadcrumb to the currently set breadcrumbs
     *
     * @param Helpers\BreadCrumb $breadCrumb
     */
    public function addBreadCrumb($breadCrumb) {
        $this->targetBreadCrumbs[] = $breadCrumb;
    }

    /**
     * Extract the title based on the currently set breadcrumbs
     */
    public function extractTitleFromBreadcrumbs() {
        $this->targetTitle = "";
        foreach ($this->targetBreadCrumbs as $crumb) {
            $this->targetTitle .= $crumb->label . " > ";
            if ($crumb->active) {
                break;
            }
        }
        $this->targetTitle = substr($this->targetTitle, 0, strlen($this->targetTitle) - 3);
    }

    /**
     * Fetch the page layout content, inserting the mvc content
     *
     * @param Models\Page $page
     * @param string $mvcContent
     * @param bool $pageEditMode
     *
     * @return string
     */
    public function fetchPageLayoutContent($page, $mvcContent = null, $pageEditMode = false) {
        return Helpers\LayoutSystem::display(Helpers\LayoutSystem::constructFromArray($page->layout), $mvcContent, "layout_mvc_content", $pageEditMode);
    }

    /**
     * Fetch the template layout content, inserting the page layout content.
     *
     * @param Models\Template $template
     * @param Models\Theme $theme
     * @param null $pageLayoutContent
     * @param bool $templateEditMode
     * @param string $templateFetchMode
     *
     * @return string
     */
    public function fetchTemplateLayoutContent($template, $theme, $pageLayoutContent = null, $templateEditMode = false, $templateFetchMode = "head_body") {
        ob_start();
        $this->getConfig();
        if ($this->config["themes"]["smart_compile"]) {
            $theme->compileLessToCss();
        }
        if ($this->config["themes"]["smart_minify"]) {
            $theme->minify();
        }
        $title = $this->config["application"]["name"];
        if (strlen($title)) {
            $title .= " :: ";
        }
        $page = $this->getTargetPage();
        if (strlen($this->targetTitle)) {
            $title .= $this->targetTitle;
        } else {
            $title .= $page->name;
        }
        $layoutContent = "";
        if ($templateFetchMode == Enums\DispatchTemplateFetchMode::head_body) {
            $this->includeIncluder("js_cookie");
            if ($this->dispatch_page_edit_mode || $this->dispatch_template_edit_mode || $this->dispatch_layout_edit_mode) {
                $this->includeIncluder("extorio_editable_advanced");
            }

            $layoutContent = Helpers\LayoutSystem::display(Helpers\LayoutSystem::constructFromArray($template->layout), $pageLayoutContent, "layout_page_content", $templateEditMode);
            if ($this->onExtorioAdminPage()) {
                $favicon = "/Core/Assets/images/favicon.png";
            } else {
                $favicon = $this->config["application"]["favicon"];
            }
            ?>
            <head>
                <?= $this->prependToHead ?>
                <?= $template->prependToHead ?>
                <?= $page->prependToHead ?>

                <title><?= $title ?></title>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <?php
                if (strlen($favicon)) {
                    ?>
                    <link rel="icon" type="image/png" href="<?= $favicon ?>"><?php
                }
                ?>
                <script type="text/javascript" src="/Core/Assets/jquery/js/jquery.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/jquery/js/jquery-ui.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/bootstrap/js/bootstrap.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/js/Extorio.min.js"></script>
                <script type="text/javascript" src="/Core/Assets/js/Extorio.layoutsystem.min.js"></script>
                <link href="/Core/Assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

                <?php
                $extension = $this->getExtension($theme->extensionName);
                if ($extension) {
                    ?>
                    <script type="text/javascript"
                            src="/<?= $extension->_localRoot . "/Themes/" . $theme->label . "/js/internal/script.min.js" ?>"></script>
                    <link rel="stylesheet"
                          href="/<?= $extension->_localRoot . "/Themes/" . $theme->label . "/css/internal/style.min.css" ?>"/>
                    <link rel="stylesheet"
                          href="/<?= $extension->_localRoot . "/Themes/" . $theme->label . "/css/style_override.css" ?>"/>
                    <?php
                }
                ?>
                <?= $page->appendToHead ?>
                <?= $template->appendToHead ?>
                <?= $this->appendToHead ?>
                <script>
                    $(function () {
                        <?php
                        if($message = $this->getMessagesError()) {
                        ?>
                        $(document).trigger("extorio_message", {
                            "type": "error",
                            "message": "<?=htmlentities(str_replace("\n", "", $message))?>"
                        });
                        <?php
                        }
                        if($message = $this->getMessagesWarning()) {
                        ?>
                        $(document).trigger("extorio_message", {
                            "type": "warning",
                            "message": "<?=htmlentities(str_replace("\n", "", $message))?>"
                        });
                        <?php
                        }
                        if($message = $this->getMessagesInfo()) {
                        ?>
                        $(document).trigger("extorio_message", {
                            "type": "info",
                            "message": "<?=htmlentities(str_replace("\n", "", $message))?>"
                        });
                        <?php
                        }
                        if($message = $this->getMessagesSuccess()) {
                        ?>
                        $(document).trigger("extorio_message", {
                            "type": "success",
                            "message": "<?=htmlentities(str_replace("\n", "", $message))?>"
                        });
                        <?php
                        }
                        ?>
                    })
                </script>
            </head>
            <body style="display: none;">
            <div id="extorio_progress_bar_top" style="" class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                     style=""></div>
            </div>
            <?php
        }
        echo $this->prependToBody;
        echo $template->prependToBody;
        echo $page->prependToBody;
        if (Utilities\Blocks::canUserReadBlock($this->getLoggedInUserId(), 90)) {
            $block = Models\Block::findById(90, 2);
            if ($block) {
                echo $this->fetchBlockProcessorViewContent($this->getExtension("Core")->_constructComponent($block->processor->namespace), array(), $block->id, false);
            }
        }

        ?>
        <div id="layout_wrapper" class="layout_wrapper">
            <?= $layoutContent ?>
        </div>
        <?php
        $this->fetchPalletteMenu();
        echo $page->appendToBody;
        echo $template->appendToBody;
        echo $this->appendToBody;
        if ($templateFetchMode == Enums\DispatchTemplateFetchMode::head_body) {
            ?>
            <script>
                $('body').show();
                var wrapper = $('#layout_wrapper');
                wrapper.layoutsystem();
                wrapper.layoutsystem("fixHeight");
                $(window).load(function () {
                    wrapper.layoutsystem("fixHeight");
                });
            </script>
        <?php
        $language = Utilities\Localization::getCurrentLanguage();
        if ($this->config["localization"]["auto_translate"] && $language->id != 1) {
        ?>
            <script src="/Core/Assets/js/Extorio.auto-translate.min.js" type="text/javascript"></script>
            <script>
                $(function () {
                    $.extorio_auto_translate('<?=$language->mLocale?>');
                });
            </script>
        <?php
        }
        ?>
            </body>
            <?php
        }
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    /**
     * Include an includer. Returns TRUE/FALSE based on whether it was included or not.
     *
     * @param $includer
     *
     * @return bool
     * @throws \Exception
     */
    public function includeIncluder($includer) {
        if (!$this->isIncluderIncluded($includer) && $this->includerExists($includer)) {
            $config = $this->getConfig();
            $data = $config["includers"][$includer];
            if (isset($data["required_includers"]) && is_array($data["required_includers"])) {
                foreach ($data["required_includers"] as $required) {
                    if (!$this->includeIncluder($required)) {
                        throw new \Exception("Trying to include " . $required . " but the includer entry could not be found.");
                    }
                }
            }

            if (isset($data["prepend_to_head"])) {
                $this->prependToHead($data["prepend_to_head"]);
            }
            if (isset($data["append_to_head"])) {
                $this->appendToHead($data["append_to_head"]);
            }
            if (isset($data["prepend_to_body"])) {
                $this->prependToBody($data["prepend_to_body"]);
            }
            if (isset($data["append_to_body"])) {
                $this->appendToBody($data["append_to_body"]);
            }

            $this->includedIncluders[] = $includer;
            return true;
        }
        return $this->isIncluderIncluded($includer);
    }

    /**
     * Is an includer included
     *
     * @param $includer
     *
     * @return bool
     */
    public function isIncluderIncluded($includer) {
        return in_array($includer, $this->includedIncluders);
    }

    /**
     * Does an includer entry exist
     *
     * @param $includer
     *
     * @return bool
     */
    public function includerExists($includer) {
        $config = $this->getConfig();
        return isset($config["includers"][$includer]);
    }

    /**
     * Prepend content to the head
     *
     * @param $content
     */
    public function prependToHead($content) {
        $this->prependToHead .= $content;
    }

    /**
     * Append content to the head
     *
     * @param $content
     */
    public function appendToHead($content) {
        $this->appendToHead .= $content;
    }

    /**
     * Prepend content to the body
     *
     * @param $content
     */
    public function prependToBody($content) {
        $this->prependToBody .= $content;
    }

    /**
     * Append content to the body
     *
     * @param $content
     */
    public function appendToBody($content) {
        $this->appendToBody .= $content;
    }

    /**
     * Getthe error message
     *
     * @return bool
     */
    public function getMessagesError() {
        if (isset($_SESSION["extorio_messages_error"])) {
            $message = $_SESSION["extorio_messages_error"];
            unset($_SESSION["extorio_messages_error"]);
            return $message;
        } else {
            return false;
        }
    }

    /**
     * Get the warning message
     *
     * @return bool
     */
    public function getMessagesWarning() {
        if (isset($_SESSION["extorio_messages_warning"])) {
            $message = $_SESSION["extorio_messages_warning"];
            unset($_SESSION["extorio_messages_warning"]);
            return $message;
        } else {
            return false;
        }
    }

    /**
     * Get the info message
     *
     * @return bool
     */
    public function getMessagesInfo() {
        if (isset($_SESSION["extorio_messages_info"])) {
            $message = $_SESSION["extorio_messages_info"];
            unset($_SESSION["extorio_messages_info"]);
            return $message;
        } else {
            return false;
        }
    }

    /**
     * Get the success message
     *
     * @return bool
     */
    public function getMessagesSuccess() {
        if (isset($_SESSION["extorio_messages_success"])) {
            $message = $_SESSION["extorio_messages_success"];
            unset($_SESSION["extorio_messages_success"]);
            return $message;
        } else {
            return false;
        }
    }

    /**
     * Fetch the pallet menu for editing the layout of pages/templates/layouts
     */
    public function fetchPalletteMenu() {
        $gutterWidth = 10;
        if ($this->dispatch_page_edit_mode || $this->dispatch_template_edit_mode || $this->dispatch_layout_edit_mode) {
            ?>
            <style>
                body {
                    padding-right: 17%;
                }

                #admin_pallet_menu_outer {
                    bottom: 0;
                    box-shadow: 0 0 13px 3px #E1E1E1;
                    font-size: 10px;
                    margin: 0;
                    overflow-x: hidden;
                    overflow-y: auto;
                    position: fixed;
                    right: 0;
                    top: 50px;
                    width: 17%;
                }

                #admin_pallet_menu_outer .panel-heading {
                    font-size: 12px;
                    padding: 5px 8px;
                }

                #admin_pallet_menu_outer .panel-body {
                    padding: <?=$gutterWidth?>px;
                }

                #admin_pallet_menu_outer hr {
                    margin: <?=$gutterWidth?>px 0px;
                }

                #admin_pallet_menu_inner {
                    position: relative;
                    padding: <?=$gutterWidth?>px;
                }

                #admin_pallet_menu_nav {
                    left: auto;
                    position: fixed;
                    right: 0;
                    top: -1px;
                    width: 17%;
                }

                #admin_pallet_menu_content .layout_insert {
                    margin-bottom: <?=$gutterWidth?>px;
                    cursor: move;
                }

                #admin_pallet_menu_content .layout_insert:last-child {
                    margin-bottom: 0px;
                }
            </style>
            <div style="display: none;" id="admin_menu_create_block_form">
                <div>
                    <div class="form-group">
                        <label for="create_block_name">Name</label>
                        <input type="text" class="form-control input-sm" id="create_block_name" name="create_block_name"
                               placeholder="Enter a name">
                    </div>
                    <div class="form-group">
                        <label for="create_block_category">Category</label>
                        <select class="form-control input-sm" id="create_block_category" name="create_block_category">
                            <?php
                            foreach ($categories as $c) {
                                ?>
                                <option value="<?= $c ?>"><?= $c ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="create_block_processor">Processor</label>
                        <select class="form-control input-sm" id="create_block_processor" name="create_block_processor">
                            <?php
                            $processors = Models\BlockProcessor::findAll(Helpers\Query::n()->order(array("label" => '$asc')));
                            foreach ($processors as $processor) {
                                ?>
                                <option value="<?= $processor->id ?>"><?= $processor->label ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div id="admin_pallet_menu_outer" class="well" style="padding: 0px;">
                <div id="admin_pallet_menu_inner">
                    <nav id="admin_pallet_menu_nav" class="navbar navbar-default navbar-inverse">
                        <div class="container-fluid">
                            <span class="navbar-brand"><span class="fa fa-th"></span> Pallet</span>
                        </div>
                    </nav>
                    <div id="admin_pallet_menu_content">

                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                           aria-expanded="true" aria-controls="collapseOne">
                                            <span class="fa fa-th"></span> Elements
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                     aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <div data-ls-role="cell" class="layout_insert">
                                            <div class="panel panel-success">
                                                <div class="panel-heading">
                                                    <strong><span class="fa fa-plus"></span> New empty cell</strong>
                                                </div>
                                                <div class="panel-body">
                                                    Drag this element into a row to an empty cell. Empty cells can
                                                    accept any other element,
                                                </div>
                                            </div>
                                        </div>
                                        <div data-ls-role="row" class="layout_insert">
                                            <div class="panel panel-success">
                                                <div class="panel-heading">
                                                    <strong><span class="fa fa-plus"></span> New empty row</strong>
                                                </div>
                                                <div class="panel-body">
                                                    Drag this element onto a cell to add a new empty row. Empty rows can
                                                    accept cells and blocks.
                                                </div>
                                            </div>
                                        </div>
                                        <div data-ls-role="content" class="layout_insert">
                                            <div class="panel panel-success">
                                                <div class="panel-heading">
                                                    <strong>Page layout/content</strong>
                                                </div>
                                                <div class="panel-body">
                                                    Drag this element onto a cell. This cell will be replaced with the
                                                    page layout (when used in a template) and the page content (when
                                                    used in a page)
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a id="admin_pallet_menu_blocks_select" class="collapsed" data-toggle="collapse"
                                           data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                                           aria-controls="collapseTwo">
                                            <span class="fa fa-th"></span> Blocks
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <select id="admin_pallet_menu_block_category"
                                                style="margin-bottom: <?= $gutterWidth ?>px;"
                                                class="form-control input-sm">

                                        </select>
                                        <select id="admin_pallet_menu_block_extension"
                                                style="margin-bottom: <?= $gutterWidth ?>px;"
                                                class="form-control input-sm">
                                            <option value="">--select an extension--</option>
                                            <?php
                                            $extensions = $this->getExtensions();
                                            foreach ($extensions as $ex) {
                                                ?>
                                                <option
                                                value="<?= $ex->_extensionName ?>"><?= $ex->_extensionName ?></option><?php
                                            }
                                            ?>
                                        </select>
                                        <div class="input-group" style="margin-bottom: <?= $gutterWidth ?>px;">
                                            <input id="admin_pallet_menu_block_name" type="text"
                                                   class="form-control input-sm" placeholder="filter by name">
                                            <span class="input-group-addon input-sm"><span class="fa fa-search"></span></span>
                                        </div>
                                        <hr/>
                                        <?php
                                        $loggedInUser = $this->getLoggedInUser();
                                        if (Utilities\Users::userHasPrivilege($loggedInUser->id, "blocks_create", "Core")) {
                                            ?>
                                            <div data-ls-role="new" class="layout_insert">
                                                <div class="panel panel-success">
                                                    <div class="panel-heading">
                                                        <strong><span class="fa fa-plus"></span> New block</strong>
                                                    </div>
                                                    <div class="panel-body">
                                                        Drag this element into a row to create a new block.
                                                    </div>
                                                </div>
                                            </div>
                                            <hr/>
                                            <?php
                                        }
                                        ?>
                                        <div id="admin_pallet_menu_blocks">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a id="admin_pallet_menu_layouts_select" class="collapsed"
                                           data-toggle="collapse" data-parent="#accordion" href="#collapseThree"
                                           aria-expanded="false" aria-controls="collapseThree">
                                            <span class="fa fa-th"></span> Layouts
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <div class="input-group" style="margin-bottom: <?= $gutterWidth ?>px;">
                                            <input id="admin_pallet_menu_layout_name" type="text"
                                                   class="form-control input-sm" placeholder="filter by name">
                                            <span class="input-group-addon input-sm"><span class="fa fa-search"></span></span>
                                        </div>
                                        <hr/>
                                        <div id="admin_pallet_menu_layouts">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <script>
                $(function () {

                    var blocksDelete = <?php
                        if (Utilities\Users::userHasPrivilege($loggedInUser->id, "blocks_delete_all", "Core"))
                            echo "true"; else echo "false";
                        ?>;

                    var loadBlockCategories = function () {
                        $.ajax({
                            url: "/extorio/apis/block-categories/tree",
                            type: "GET",
                            dataType: "json",
                            error: function () {
                                $.extorio_messageError(c);
                            },
                            success: function (response) {
                                if (response.error) {
                                    $.extorio_messageError(response.error_message);
                                } else {
                                    var container = $('#admin_pallet_menu_block_category');
                                    container.html('');
                                    container.append('<option value="">--select a category--</option>');
                                    for (var i = 0; i < response.data.length; i++) {
                                        var main = response.data[i];
                                        container.append('<option value="' + main.id + '">' + main.name + '</option>');
                                        for (var j = 0; j < main.subCategories.length; j++) {
                                            var sub = main.subCategories[j];
                                            container.append('<option value="' + main.id + ':' + sub.id + '">&nbsp;&nbsp;' + main.name + ' / ' + sub.name + '</option>');
                                        }
                                    }
                                    container.on("change", function () {
                                        loadBlocks();
                                    });
                                }
                            }
                        });
                    };

                    var loadBlocks = function () {
                        var container = $('#admin_pallet_menu_blocks');
                        var extension = $('#admin_pallet_menu_block_extension').val();
                        var catRaw = $('#admin_pallet_menu_block_category').val();
                        var name = $('#admin_pallet_menu_block_name').val();
                        var cats = [];
                        if (catRaw != undefined) {
                            cats = catRaw.split(":");
                        }
                        var data = {};
                        data.isHidden = false;
                        data.isPublic = true;
                        if (cats.length >= 1) {
                            data.categoryId = cats[0];
                            if (cats.length >= 2) {
                                data.subCategoryId = cats[1];
                            }
                        }
                        if (extension.length > 0) {
                            data.extension = extension;
                        }
                        if (name.length > 0) {
                            data.name = name;
                        }
                        container.extorio_showLoader();
                        $.extorio_api({
                            endpoint: "/blocks/filter",
                            type: "GET",
                            data: data,
                            oncomplete: function () {
                                container.extorio_hideLoader();
                            },
                            onsuccess: function (response) {
                                container.html('');
                                if (response.data.length > 0) {
                                    for (var i = 0; i < response.data.length; i++) {
                                        var block = response.data[i];
                                        var desc = block.description.length > 0 ? block.description : "No description available.";
                                        var item = $('<div data-ls-role="block" data-ls-blockid="' + block.id + '" class="layout_insert">' +
                                            '   <div class="panel panel-primary">' +
                                            '       <div class="panel-heading">' +
                                            '           ' + block.name + '' +
                                            '       </div>' +
                                            '       <div class="panel-body">' +
                                            '           <p><span class="label label-primary">' + block.extensionName + '</span> : <span class="label label-info">' + block.category.name + '</span> / <span class="label label-info">' + block.subCategory.name + '</span></p>' +
                                            '           ' + desc + '<br />' +
                                            '           <div class="delete_block_container">' +
                                            '               <p></p>' +
                                            '               <button data-id="' + block.id + '" class="btn btn-xs btn-danger delete_block"><span class="fa fa-trash"></span> delete</button>' +
                                            '           </div>' +
                                            '           ' +
                                            '       </div>' +
                                            '   </div>' +
                                            '</div>');
                                        if (!blocksDelete) {
                                            item.find('.delete_block_container').hide();
                                        }
                                        item.find('.delete_block').on("click", function () {
                                            var id = $(this).attr('data-id');
                                            $.extorio_modal({
                                                title: "Delete block",
                                                content: "Are you sure you want to delete this block?",
                                                size: "modal-sm",
                                                closetext: "cancel",
                                                continuetext: "delete",
                                                oncontinuebutton: function () {
                                                    $.extorio_showFullPageLoader();
                                                    $.extorio_api({
                                                        endpoint: "/blocks/" + id + "/delete",
                                                        type: "POST",
                                                        oncomplete: function () {
                                                            $.extorio_hideFullPageLoader();
                                                        },
                                                        onsuccess: function (response) {
                                                            $.extorio_messageInfo("Block deleted");
                                                            //delete the block if it's in the layout
                                                            $('.layout_cell[data-ls-blockid="' + id + '"]').remove();
                                                            loadBlocks();
                                                        }
                                                    });
                                                }
                                            });
                                        });
                                        container.append(item);
                                    }
                                    $('.layout_insert').layoutinsert();
                                } else {
                                    container.html('No blocks found');
                                }
                            }
                        });
                    };

                    loadBlockCategories();
                    loadBlocks();

                    $('#admin_pallet_menu_block_extension').on("change", function () {
                        loadBlocks();
                    });

                    var typeTime = new Date();
                    $("#admin_pallet_menu_block_name").on("keydown", function () {
                        var d = new Date();
                        typeTime.setTime(d.getTime() + 400);
                        setTimeout(function () {
                            var e = new Date();
                            if (e.getTime() >= typeTime.getTime()) {
                                loadBlocks();
                            }
                        }, 500);
                    });
                    $('.layout_insert').layoutinsert();
                });
            </script>
            <?php
        }
    }

    /**
     * Get the page's target theme
     *
     * @return Models\Theme
     */
    public function getTargetTheme() {
        return $this->targetTheme;
    }

    public function on_autoload($className) {
        //does the class exist in the list of autoloaders?
        $config = $this->getConfig();
        if (isset($config["autoload"][$className])) {
            require_once $config["autoload"][$className];
            return;
        }
        //all other autoloading is done by the extension that owns the class file, assuming
        //that all other classes are PSR-4 safe
        $extensionName = Utilities\Namespaces::getExtensionName($className);
        if (isset($this->extensions[$extensionName])) {
            $this->extensions[$extensionName]->_autoLoadClassFile($className);
        }
    }

    /**
     * We register this method to the system shutdown
     *
     */
    public function on_system_shutdown() {
        $error = error_get_last();
        if ($error) {
            if (in_array($error["type"], $this->nonRecoverableErrors)) {
                $event = $this->getEvent("on_exception");
                if ($event) {
                    throw new \Exception($error["message"], $error["type"]);
                } else {
                    $this->logCustom("system_error.log", $error["message"] . " in " . $error["file"] . ":" . $error["line"]);
                }
                //if the system hasn't exit, do it
                exit;
            } else {
                $this->logCustom("system_error.log", $error["message"] . " in " . $error["file"] . ":" . $error["line"]);
            }
        }
    }

    /**
     * Create a custom log
     *
     * @param $fileName
     * @param $message
     */
    public function logCustom($fileName, $message) {
        $fileName = Utilities\Strings::endsWith($fileName, ".log");
        $file = File::createFromPath("Logs/" . $fileName);
        if ($file) {
            $file->append("[" . date("d-m-Y H:i:s") . "] - " . $message . "\n");
            $file->close();
        }
    }

    /**
     * We register this method to the system error
     *
     * @param $number
     * @param $message
     * @param $file
     * @param $line
     */
    public function on_system_error($number, $message, $file, $line) {
        if (in_array($number, $this->nonRecoverableErrors)) {
            $event = $this->getEvent("on_exception");
            if ($event) {
                throw new \Exception($message, $line);
            } else {
                $this->logCustom("system_error.log", $message . " in " . $file . ":" . $line);
            }
            //if the system hasn't exited, do it
            exit;
        } else {
            $this->logCustom("system_error.log", $message . " in " . $file . ":" . $line);
        }
    }

    /**
     * We register this method to the exception event
     *
     * @param \Exception $exception
     */
    public function on_exception($exception) {
        echo "<pre>";
        echo $exception;
        echo "</pre>";
        $this->logException($exception);
    }

    /**
     * Log an exteption
     *
     * @param $exception
     */
    public function logException($exception) {
        $this->logCustom(get_class($exception) . ".log", $exception->__toString());
    }

    /**
     * Get the current version
     *
     * @return null
     */
    public function currentVersion() {
        $this->getConfig();
        if (isset($this->config["extorio"]["current_version"])) {
            return $this->config["extorio"]["current_version"];
        } else {
            return null;
        }
    }

    /**
     * Get the installed version
     *
     * @return null
     */
    public function installedVersion() {
        $this->getConfig();
        if (isset($this->config["extorio"]["installed_version"])) {
            return $this->config["extorio"]["installed_version"];
        } else {
            return null;
        }
    }

    /**
     * Whether an admin is created
     *
     * @return bool
     */
    public function adminCreated() {
        $this->getConfig();
        if (isset($this->config["extorio"]["admin_created"])) {
            return $this->config["extorio"]["admin_created"];
        } else {
            return false;
        }
    }

    /**
     * Whether the database has been tested and connected
     *
     * @return bool
     */
    public function databaseConnected() {
        $this->getConfig();
        if (isset($this->config["extorio"]["database_connected"])) {
            return $this->config["extorio"]["database_connected"];
        } else {
            return false;
        }
    }

    /**
     * Whether the database has been imported
     *
     * @return bool
     */
    public function databaseImported() {
        $this->getConfig();
        if (isset($this->config["extorio"]["database_imported"])) {
            return $this->config["extorio"]["database_imported"];
        } else {
            return false;
        }
    }

    /**
     * Whether the application settings have been configured
     *
     * @return bool
     */
    public function applicationConfigured() {
        $this->getConfig();
        if (isset($this->config["extorio"]["application_configured"])) {
            return $this->config["extorio"]["application_configured"];
        } else {
            return false;
        }
    }

    /**
     * Check whether user registration is enabled or not
     *
     * @return bool
     */
    public function isUserRegistrationEnabled() {
        $this->getConfig();
        if (isset($this->config["users"]["registration"]["enabled"])) {
            return $this->config["users"]["registration"]["enabled"];
        } else {
            return false;
        }
    }

    /**
     * Get all of the currently included includers
     *
     * @return array
     */
    public function getIncludedIncluders() {
        return $this->includedIncluders;
    }

    /**
     * Get the installed includers
     *
     * @return array
     */
    public function getIncluders() {
        $config = $this->getConfig();
        return $config["includers"];
    }

    /**
     * Log to the debug file
     *
     * @param $message
     */
    public function logDebug($message) {
        $this->logCustom("debug_log.log", $message);
    }

    /**
     * Log to the warning file
     *
     * @param $message
     */
    public function logWarning($message) {
        $this->logCustom("warning_log.log", $message);
    }

    /**
     * Log to the error file
     *
     * @param $message
     */
    public function logError($message) {
        $this->logCustom("error_log.log", $message);
    }

    /**
     * Create a success message
     *
     * @param $message
     */
    public function messageSuccess($message) {
        $_SESSION["extorio_messages_success"] = $message;
    }

    /**
     * Create an info message
     *
     * @param $message
     */
    public function messageInfo($message) {
        $_SESSION["extorio_messages_info"] = $message;
    }

    /**
     * Create a warning message
     *
     * @param $message
     */
    public function messageWarning($message) {
        $_SESSION["extorio_messages_warning"] = $message;
    }

    /**
     * Create an error message
     *
     * @param $message
     */
    public function messageError($message) {
        $_SESSION["extorio_messages_error"] = $message;
    }

    /**
     * Get the logged in user's user group
     *
     * @return Models\UserGroup
     */
    public function getLoggedInUserGroup() {
        return $this->getLoggedInUser()->userGroup;
    }

    /**
     * Get the id of the logged in user's user group
     *
     * @return int
     */
    public function getLoggedInUserGroupId() {
        return $this->getLoggedInUser()->userGroup->id;
    }

    /**
     * Get the user groups that the logged in user manages
     *
     * @return Models\UserGroup[]
     */
    public function getLoggedInUserManageGroups() {
        $loggedInUser = $this->getLoggedInUser();
        if ($loggedInUser) {
            if ($loggedInUser->userGroup->manageAll) {
                return Models\UserGroup::findAll(Helpers\Query::n()->order("name"));
            } else {
                return Models\UserGroup::findAll(Helpers\Query::n()->where(array("id" => array(Helpers\Query::_in => $loggedInUser->userGroup->manageGroups)))->order("name"));
            }
        }
        return array();
    }

    /**
     * Temporarily set the logged in user for this session. (A refresh will revert to the real logged in user)
     *
     * @param Models\User $user
     */
    public function setLoggedInUserTemp($user) {
        $this->loggedInUser = $user;
    }

    /**
     * Set a dispatc setting
     *
     * @param $settingKey
     * @param $settingValue
     */
    public function setDispatchSetting($settingKey, $settingValue) {
        $this->getConfig();
        if (isset($this->config["extorio"]["dispatch_settings"]["all_keys"])) {
            $this->$settingKey = $settingValue;
        }
    }

    /**
     * Get a dispatch setting
     *
     * @param $settingKey
     *
     * @return bool
     */
    public function getDispatchSetting($settingKey) {
        $this->getConfig();
        if (isset($this->config["extorio"]["dispatch_settings"]["all_keys"])) {
            return $this->$settingKey;
        }
        return false;
    }

    /**
     * Store a started extension
     *
     * @param $extension
     */
    public function setExtension($extension) {
        $this->extensions[$extension->extensionName] = $extension;
    }

    /**
     * Unset a started extension
     *
     * @param $extension
     */
    public function unsetExtension($extension) {
        unset($this->extensions[$extension->extensionName]);
    }

    /**
     * Get the current url
     *
     * @return Helpers\Url
     */
    public function getCurrentUrl() {
        return $this->currentUrl;
    }

    /**
     * Get the currently set title
     *
     * @return string
     */
    public function getTargetTitle() {
        return $this->targetTitle;
    }

    /**
     * Set the current title
     *
     * @param $title
     */
    public function setTargetTitle($title) {
        $this->targetTitle = $title;
    }

    /**
     * Get the currently set bread crumbs
     *
     * @return Helpers\BreadCrumb[]
     */
    public function getTargetBreadCrumbs() {
        return $this->targetBreadCrumbs;
    }

    /**
     * Override the currently set breadcrumbs
     *
     * @param Helpers\BreadCrumb[] $breadCrumbs
     */
    public function setTargetBreadCrumbs($breadCrumbs) {
        $this->targetBreadCrumbs = $breadCrumbs;
    }

    /**
     * Run a task now (rather than in the background)
     *
     * @param Task|string $taskComponent can either be the task's components full namespace, or the task component itself
     * @param string $action
     * @param array $params
     */
    public function runTask_Now($taskComponent, $isHidden = false, $action = "_onDefault", $params = array()) {
        if ($taskComponent) {
            ignore_user_abort(true);
            set_time_limit(0);

            if (gettype($taskComponent) == Enums\PHPTypes::_string) {
                $extensionName = Utilities\Namespaces::getExtensionName($taskComponent);
                $extension = $this->getExtension($extensionName);
                if ($extension) {
                    $taskComponent = $extension->_constructComponent($taskComponent);
                }
            }

            //work out whether the action is a callable one, or should be part of the params
            if (!is_callable(array($taskComponent, $action))) {
                array_unshift($params, $action);
                $action = "_onDefault";
            }

            //try and find the task that this module uses
            $task = Models\Task::findOne(Helpers\Query::n()->where(array("namespace" => Utilities\Namespaces::getNamespace($taskComponent))));

            $paramsStr = "";
            foreach ($params as $param) {
                $paramsStr .= $param . ", ";
            }
            $paramsStr = substr($paramsStr, 0, strlen($paramsStr) - 2);
            if ($task) {
                Utilities\Tasks::createLiveTask($task->label, $action, $paramsStr, $isHidden);
            } else {
                Utilities\Tasks::createLiveTask(Utilities\Namespaces::getClassName(get_class($taskComponent)), $action, $paramsStr, $isHidden);
            }

            $db = $this->getDbInstanceDefault();

            //queue the task if need be
            $config = $this->getConfig();
            while ($db->query('SELECT id FROM tasks_livetask WHERE status = $1 OR status = $2', array(Enums\TaskStatus::_queued, Enums\TaskStatus::_running))->numRows() >= $config["tasks"]["max_running_tasks"]) {
                //wait
                Utilities\Tasks::queueLiveTask(getmypid());
                sleep(5);
            }

            //run the task!
            Utilities\Tasks::runLiveTask(getmypid());

            $taskComponent->_onBegin();

            call_user_func_array(array($taskComponent, $action), $params);

            $taskComponent->_onEnd();

            //if we get to this point, we mark the task as completed
            Utilities\Tasks::completeLiveTask(getmypid());
        }
    }

    /**
     * Run a task in the background
     *
     * @param string $taskLabel the label of the task
     * @param bool $isHidden
     * @param string $action
     * @param array $params
     *
     * @throws General_Exception
     */
    public function runTask_Background($taskLabel, $isHidden = false, $action = "_onDefault", $params = array()) {
        $request = "/extorio/tasks/" . $taskLabel . "/";
        $request .= Utilities\Strings::removeQuotes($action);
        foreach ($params as $param) {
            $request .= "/" . Utilities\Strings::removeQuotes($param);
        }
        $request .= "?task_is_hidden=";
        $request .= $isHidden ? "true" : "false";
        $session = Extorio::get()->getLoggedInUserSession();
        //run in the background using the custom entry point
        if (PHP_OS == "WINNT") {
            //windows command
            if ($session) {
                $request .= "^&user_session=" . $session;
            }

            $command = "cd " . getcwd() . "";
            $command .= " & php -f cliaccess.php \"" . $request . "\" ";
            $command .= "> nul 2> nul";
            pclose(popen('start /B cmd /C "' . $command . '"', 'w'));
        } elseif (PHP_OS == "Linux") {
            //linux command
            if ($session) {
                $request .= "\&user_session=" . $session;
            }

            $command = "cd " . getcwd() . "";
            $command .= "; php -f cliaccess.php '" . $request . "' ";
            $command .= ">/dev/null 2>/dev/null ";
            $command .= "&";
            exec($command);
        } else {
            throw new General_Exception("The task system is only supported by Windows and Linux currently.");
        }
    }

    /**
     * Get the Extorio instance. This must be done AFTER extorio is initiated.
     *
     * @return Extorio
     */
    public static function get() {
        if (isset($_ENV["EXTORIO"])) {
            if ($_ENV["EXTORIO"]->initiated) {
                return $_ENV["EXTORIO"];
            }
        }
        die("You must initiate Extorio before you can get it");
    }

    /**
     * Fetch the edit content of a block processor
     *
     * @param BlockProcessor $blockProcessor
     * @param array $config
     *
     * @return string
     */
    public function fetchBlockProcessorEditContent($blockProcessor, $config = array(), $blockId = null, $inline = false) {
        return $this->fetchBlockProcessorContent("edit", $blockProcessor, $config, $blockId, $inline);
    }
}