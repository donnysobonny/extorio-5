<?php
namespace Core\Classes\Commons;

use Core\Classes\Commons\FileSystem\Asset;
use Core\Classes\Commons\FileSystem\Directory;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Exceptions\Extension_Exception;
use Core\Classes\Helpers\Command;
use Core\Classes\Helpers\Query;
use Core\Classes\Models;
use Core\Classes\Models\GeneralAccessPoint;
use Core\Classes\Utilities;

/**
 * Extensions make up the independent sections of the framework. Each extension contains all of it's classes and components
 * which are accessed through the extension.
 *
 * An extension cannot successfully exist without this class, which should be located in
 * <extension name>/Components/Extension/Extension.php
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Extension
 */
class Extension {
    /**
     * The local path to the root of the extension
     *
     * @var string
     */
    public $_localRoot = "";
    /**
     * The absolute path to the root of the extension
     *
     * @var string
     */
    public $_absoluteRoot = "";
    /**
     * The name of the extension
     *
     * @var string
     */
    public $_extensionName = "";
    /**
     * Whether the extension is an internal one (Core/Application)
     *
     * @var bool
     */
    public $_internal = false;
    /**
     * The class file cache
     *
     * @var array
     */
    public $classFileCache = array();
    /**
     * The extension's associated model instance
     *
     * @var Models\Extension
     */
    private $model = null;
    /**
     * This is an internal store of base config
     *
     * @var null
     */
    private $baseConfig = null;
    /**
     * This is an internal store of override config
     *
     * @var null
     */
    private $overrideConfig = null;

    public function _getDescription() {
        return "No description available";
    }

    public function _getWebsite() {
        return "No website given";
    }

    public function _getAuthor() {
        return "No author specified";
    }

    /**
     * Start the extension. This sets the extension up for autoloading, compiles it's config files to Extorio, and adds it
     * to the array of started extension in Extorio
     */
    final public function _start() {
        //and finally run the on start event
        $this->_onStart();
    }

    /**
     * Whenever an extension is started, this event is called. This is useful for adding menu items to the admin pages etc
     */
    protected function _onStart() {
    }

    /**
     * This is called when the extension is being uninstalled.
     * entities.
     */
    final public function _uninstall() {
        if ($this->_internal) {
            return;
        }
        $db = $this->_Extorio()->getDbInstanceDefault();
        $db->begin();
        $this->_onUninstall();
        $ext = $this->getExtensionModel();
        $ext->isInstalled = false;
        $ext->pushThis();
        $db->commit();
        $this->_Extorio()->unsetExtension($this);
        $this->_Extorio()->cacheConfig();
    }

    /**
     * Get the Extorio instance
     *
     * @return Extorio
     */
    final public function _Extorio() {
        return Extorio::get();
    }

    /**
     * This event is called before the extension is installed. Use this to uninstall extension-owned entities
     */
    protected function _onUninstall() {

    }

    /**
     * Get the extension model associated with this component
     *
     * @return Models\Extension
     */
    final public function getExtensionModel() {
        if ($this->model) {
            return $this->model;
        }
        return $this->model = Models\Extension::findByName($this->_extensionName);
    }

    /**
     * This is called when the extension is installed.
     */
    final public function _install() {
        if($this->_internal) {
            return;
        }
        $db = $this->_Extorio()->getDbInstanceDefault();
        $db->begin();
        $this->_onInstall();
        $ext = $this->getExtensionModel();
        $ext->isInstalled = true;
        $ext->pushThis();
        $db->commit();
        $this->_Extorio()->setExtension($this);
        $this->_Extorio()->cacheConfig();
    }

    /**
     * This event is called before the extension is installed. Use this to install extension-owned entities, or to run
     * processes essential to the extension
     */
    protected function _onInstall() {

    }

    final public function _update() {
        if ($this->_getCurrentBuild() >= $this->_getPublicBuild()) {
            return;
        }

        if (!strlen($this->_getPublicRepo())) {
            $this->_Extorio()->logCustom("update.log","No public repo has been specified");
            throw new \Exception("No public repo has been specified");
        }
        $env = array("HOME" => $this->_Extorio()->cwd());
        //make sure git installed
        $c = Command::n("git", null, $this->_localRoot, $env);
        if ($c->getStatus() == 127) {
            $this->_Extorio()->logCustom("update.log","This system does not have GIT installed, and therefore cannot update this extension");
            throw new \Exception("This system does not have GIT installed, and therefore cannot update this extension");
        }
        //check there is an initiated repo first
        if (!file_exists($this->_localRoot . "/.git")) {
            //init the repo
            $c = Command::n("git init", null, $this->_localRoot, $env);
            if ($c->hasError()) {
                $this->_Extorio()->logCustom("update.log","Could not init repo: " . $c->getError());
                throw new \Exception("Could not init repo: " . $c->getError());
            }

            //add all to the repo
            $c = Command::n("git add --all", null, $this->_localRoot, $env);
            if ($c->hasError()) {
                $this->_Extorio()->logCustom("update.log","Could not add all to the repo: " . $c->getError());
                throw new \Exception("Could not add all to the repo: " . $c->getError());
            }

            //if there is a .gitignore file, add it
            if (file_exists($this->_localRoot . "/.gitignore")) {
                $c = Command::n("git add -f .gitignore", null, $this->_localRoot, $env);
                if ($c->hasError()) {
                    $this->_Extorio()->logCustom("update.log","Could not add the gitignore file: " . $c->getError());
                    throw new \Exception("Could not add the gitignore file: " . $c->getError());
                }
            }

            //commit
            $c = Command::n('git commit -a -m "committing any local changes"', null, $this->_localRoot, $env);
            if ($c->hasError()) {
                $this->_Extorio()->logCustom("update.log","Could not commit: " . $c->getError());
                throw new \Exception("Could not commit: " . $c->getError());
            }
        }

        //remove and add origin
        $c = Command::n("git remote remove origin", null, $this->_localRoot, $env);
        $c = Command::n("git remote add origin " . $this->_getPublicRepo(), null, $this->_localRoot, $env);
        if ($c->hasError()) {
            $this->_Extorio()->logCustom("update.log","Could not add remote: " . $c->getError());
            throw new \Exception("Could not add remote: " . $c->getError());
        }

        //fetch from remove. This will often spill out warnings that we can ignore
        $c = Command::n("git fetch origin master", null, $this->_localRoot, $env);

        //and finally, merge, forcing overwrite on "theirs"
        $c = Command::n("git merge -s recursive -X theirs origin/master", null, $this->_localRoot, $env);
        if ($c->hasError()) {
            $this->_Extorio()->logCustom("update.log","Could not merge: " . $c->getError());
            throw new \Exception("Could not merge: " . $c->getError());
        }

        //cache config
        $this->_Extorio()->cacheConfig();
    }

    public function _getCurrentBuild() {
        return 0;
    }

    public function _getPublicBuild() {
        return 0;
    }

    public function _getPublicRepo() {
        return null;
    }

    /**
     * Patch the extension to the current build
     *
     * @param int $build
     */
    final public function _patch() {
        $db = $this->_Extorio()->getDbInstanceDefault();
        $ext = $this->getExtensionModel();
        if ($ext->build >= $this->_getCurrentBuild()) {
            return;
        }
        while ($ext->build < $this->_getCurrentBuild()) {
            $db->begin();
            $ext->build++;
            $this->_onPatch($ext->build);
            $ext->pushThis();
            $db->commit();
        }
        $this->_Extorio()->cacheConfig();
    }

    protected function _onPatch($build) {

    }

    final public function getAbsoluteRoot() {
        return $this->_absoluteRoot;
    }

    /**
     * We register this method as the extension's auto. It attempts to include a class file based on the namespace and
     * the PSR-4 specification.
     *
     * @param $class
     */
    final public function _autoLoadClassFile($class) {
        $file = $this->_findClassFile($class);
        if ($file) {
            $file->includeFile();
        }
    }

    /**
     * Find a class file by the full class name (with namespace)
     *
     * @param string $classNameFull
     *
     * @return File
     */
    final public function _findClassFile($classNameFull) {
        $loosePath = Utilities\Namespaces::getLoosePath($classNameFull);
        if (!isset($this->classFileCache[$loosePath])) {
            $this->generateFileCache($loosePath, $loosePath);
        }
        $className = Utilities\Namespaces::getClassName($classNameFull);
        if (isset($this->classFileCache[$loosePath][$className])) {
            return File::constructFromPath($this->classFileCache[$loosePath][$className]);
        }
        return false;
    }

    /**
     * Generate the file cache within a certain loose path of the extension
     *
     * @param $loosePath
     * @param $currentPath
     */
    final public function generateFileCache($loosePath, $currentPath) {
        $files = File::getFilesWithinDirectory($this->_absoluteRoot . "/" . $currentPath);
        foreach ($files as $file) {
            if ($file->extension() == "php") {
                $this->classFileCache[$loosePath][$file->fileName()] = $file->dirName() . "/" . $file->baseName();
            }
        }
        $dirs = Directory::getDirectoriesWithinDirectory($this->_absoluteRoot . "/" . $currentPath);
        foreach ($dirs as $dir) {
            $this->generateFileCache($loosePath, $currentPath . "/" . $dir->fileName());
        }
    }

    /**
     * Construct a component owned by this extension. This is the method that you should use to create instances of
     * a component. It will construct the component correctly, assigning the properties correctly.
     *
     * @param string $componentClassNameFull
     *
     * @return Component
     * @throws Extension_Exception
     *
     */
    final public function _constructComponent($componentClassNameFull) {
        //make sure the component is owned by this extension
        if (Utilities\Namespaces::getExtensionName($componentClassNameFull) != $this->_extensionName) {
            throw new Extension_Exception("The " . $this->_extensionName . " extension cannot construct the component " . $componentClassNameFull . " because this component does not belong to this extension");
        }
        $fields = Utilities\Namespaces::getFields($componentClassNameFull);
        //construct
        /** @var Component $component */
        $component = new $componentClassNameFull();
        //the third field is the component type
        $component->_componentType = $fields[2];
        $component->_extensionName = $this->_extensionName;
        return $component;
    }

    /**
     * Get only the config override
     *
     * @return array
     */
    final public function _getConfigOverride() {
        return $this->_getConfig(false, true);
    }

    /**
     * Get the config of the extension. You can get either the base or override, or both.
     *
     * @param bool $getBaseConfig
     * @param bool $getOverrideConfig
     *
     * @return array
     */
    final public function _getConfig($getBaseConfig = true, $getOverrideConfig = true) {
        //internally store the configs
        if (is_null($this->baseConfig)) {
            $file = File::constructFromPath($this->_absoluteRoot . "/Config/internal/config.php");
            if ($file->exists()) {
                $this->baseConfig = $file->includeFile();
            } else {
                $this->baseConfig = array();
            }
        }
        if (is_null($this->overrideConfig)) {
            $file = File::constructFromPath($this->_absoluteRoot . "/Config/config_override.php");
            if ($file->exists()) {
                $this->overrideConfig = $file->includeFile();
            } else {
                $this->overrideConfig = array();
            }
        }

        //return the requested combination, merged
        if ($getBaseConfig && !$getOverrideConfig) {
            return $this->baseConfig;
        }
        if (!$getBaseConfig && $getOverrideConfig) {
            return $this->overrideConfig;
        }
        return Utilities\Extorio::mergeConfigArrays($this->baseConfig, $this->overrideConfig);
    }

    /**
     * Merge to the config override file, and optionally update the config cache
     *
     * @param $config
     * @param bool $cache
     *
     * @return bool
     */
    final public function _mergeConfigOverride($config, $cache = true) {
        //overwrite with merged
        return $this->_writeConfigOverride(Utilities\Extorio::mergeConfigArrays($this->_getConfig(false, true), $config), $cache);
    }

    /**
     * Completely overwrite the config override file, and optionally update the config cache
     *
     * @param $config
     * @param bool $cache
     *
     * @return bool
     */
    final public function _writeConfigOverride($config, $cache = true) {
        //update the override file
        $file = File::createFromPath($this->_absoluteRoot . "/Config/config_override.php");
        if ($file) {
            $template = File::constructFromPath("Core/Assets/file-templates/config_override.txt");
            if ($template) {
                $content = $template->read();
                if (!$content)
                    return false;

                $content = str_replace("*|CONFIG_ARRAY|*", var_export($config, true), $content);

                if (!$file->write($content))
                    return false;
            }
            $file->close();
        }
        //update internal store
        $this->overrideConfig = $config;
        //optionally update extorio's cache
        if ($cache) {
            $this->_Extorio()->cacheConfig();
        }
        return true;
    }

    /**
     * Install a Task
     *
     * @param string $name
     * @param string $description
     * @param bool $allRead
     * @param int[]|Models\UserGroup[] $readGroups
     *
     * @return Models\Task
     */
    final protected function _installTask(
        $name,
        $description,
        $allRead = false,
        $readGroups = array(1,2)
    ) {
        $o = Models\Task::q($name, $description, $allRead, $readGroups, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Install api
     *
     * @param string $name
     * @param string $description
     * @param bool $allRead
     * @param int[]|Models\UserGroup[] $readGroups
     *
     * @return Models\Api
     */
    final protected function _installApi(
        $name,
        $description, $allRead = false, $readGroups = array(1, 2)
    ) {
        $o = Models\Api::q($name, $description, $allRead, $readGroups, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Install user group
     *
     * @param string $name
     * @param string $description
     * @param int[]|Models\UserPrivilege[] $privileges
     * @param bool $manageAll
     * @param int[]|Models\UserGroup[] $manageGroups
     *
     * @return Models\UserGroup
     */
    final protected function _installUserGroup($name, $description, $privileges = array(), $manageAll = false, $manageGroups = array()) {
        $o = Models\UserGroup::q($name, $description, $privileges, $manageAll, $manageGroups, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Install user privilege
     *
     * @param string $name
     * @param string $description
     * @param int|Models\UserPrivilegeCategory $category
     *
     * @return Models\UserPrivilege
     */
    final protected function _installPrivilege($name, $description, $category) {
        $o = Models\UserPrivilege::q($name, $description, $category, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $description
     * @param string $icon
     *
     * @return Models\UserPrivilegeCategory
     */
    final protected function _installPrivilegeCategory($name, $description, $icon) {
        $o = Models\UserPrivilegeCategory::q($name, $description, $icon, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Install a page
     *
     * @param string $name
     * @param string $desciption
     * @param string $address
     * @param bool $default
     * @param bool $isPublic
     * @param string $publishDate
     * @param string $unpublishDate
     * @param null|int|Models\Template $template
     * @param null|int|Models\Theme $theme
     * @param array $layout
     * @param bool $allRead
     * @param int $readFailPageId
     * @param int[]|Models\UserGroup[] $readGroups
     * @param int[]|Models\UserGroup[] $modifyGroups
     * @param string $prependToHead
     * @param string $appendToHead
     * @param string $prependToBody
     * @param string $appendToBody
     * @param bool $isHidden
     *
     * @return Models\Page
     */
    final protected function _installPage($name, $desciption, $address, $default = false, $isPublic = true, $publishDate = "", $unpublishDate = "", $template = null, $theme = null, $layout = array(), $allRead = true, $readFailPageId = 0, $readGroups = array(), $modifyGroups = array(1, 2), $prependToHead = "", $appendToHead = "", $prependToBody = "", $appendToBody = "", $isHidden = false) {
        $o = Models\Page::q($name, $desciption, $address, $default, $isPublic, $publishDate, $unpublishDate, $template, $theme, $layout, $allRead, $readFailPageId, $readGroups, $modifyGroups, $prependToHead, $appendToHead, $prependToBody, $appendToBody, $isHidden, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Install theme
     *
     * @param string $name
     * @param string $description
     * @param bool $default
     * @param bool $isHidden
     *
     * @return Models\Theme
     */
    final protected function _installTheme(
        $name,
        $description, $default = false, $isHidden = false
    ) {
        $o = Models\Theme::q($name, $description, $default, $isHidden, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Install layout
     *
     * @param string $name
     * @param string $description
     * @param array $layout
     *
     * @return Models\Layout
     */
    final protected function _installLayout($name, $description, $layout = array()) {
        $o = Models\Layout::q($name, $description, $layout, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Install template
     *
     * @param string $name
     * @param string $description
     * @param bool $default
     * @param array $layout
     * @param int[]|Models\UserGroup[] $modifyGroups
     * @param string $prependToHead
     * @param string $appendToHead
     * @param string $prependToBody
     * @param string $appendToBody
     * @param bool $isHidden
     *
     * @return Models\Template
     */
    final protected function _installTemplate($name, $description, $default = false, $layout = array(), $modifyGroups = array(1, 2), $prependToHead = "", $appendToHead = "", $prependToBody = "", $appendToBody = "", $isHidden = false) {
        $o = Models\Template::q($name, $description, $default, $layout, $modifyGroups, $prependToHead, $appendToHead, $prependToBody, $appendToBody, $isHidden, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Install a model
     *
     * @param string $name
     * @param int[]|Models\Property[] $properties
     * @param string $description
     * @param bool $isHidden
     *
     * @return Models\Model
     */
    final protected function _installModel($name, $properties = array(), $description, $isHidden = false) {
        $o = Models\Model::q($name,$properties,$description,$isHidden,$this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Install an enum
     *
     * @param string $name
     * @param string $description
     * @param array $values
     * @param bool $isHidden
     *
     * @return Models\Enum
     */
    final protected function _installEnum($name, $description, $values = array(), $isHidden = false) {
        $o = Models\Enum::q($name, $description, $values, $isHidden, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Install a block
     *
     * @param string $name
     * @param string $description
     * @param bool $isPublic
     * @param int|Models\BlockProcessor $processor
     * @param int|Models\BlockCategory $category
     * @param int|Models\BlockCategory $subCategory
     * @param string $config
     * @param bool $allRead
     * @param int[]|Models\UserGroup[] $readGroups
     * @param int[]|Models\UserGroup[] $modifyGroups
     * @param bool $isHidden
     *
     * @return Models\Block
     */
    final protected function _installBlock($name, $description, $isPublic = false, $processor, $category, $subCategory, $config = "", $allRead = true, $readGroups = array(), $modifyGroups = array(1, 2), $isHidden = false) {
        $o = Models\Block::q($name, $description, $isPublic, $processor, $category, $subCategory, $config, $allRead, $readGroups, $modifyGroups, $isHidden, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Install block processor
     *
     * @param string $name
     * @param string $description
     * @param int|Models\BlockProcessorCategory $category
     * @param bool $inlineEnabled
     * @param bool $allInline
     * @param int[]|Models\UserGroup[] $inlineGroups
     *
     * @return BlockProcessor
     */
    final protected function _installBlockProcessor(
        $name,
        $description, $category, $inlineEnabled = true, $allInline = true, $inlineGroups = array()
    ) {
        $o = Models\BlockProcessor::q($name, $description, $category, $inlineEnabled, $allInline, $inlineGroups, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Install a block processor category
     *
     * @param string $name
     * @param string $description
     * @param string $icon
     *
     * @return Models\BlockProcessorCategory
     */
    final protected function _installBlockProcessorCategory($name, $description, $icon) {
        $o = Models\BlockProcessorCategory::q($name, $description, $icon, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Install a block category
     *
     * @param string $name
     * @param int $parentId
     * @param string $description
     * @param string $icon
     *
     * @return Models\BlockCategory
     */
    final protected function _installBlockCategory(
        $name, $parentId = 0,
        $description, $icon
    ) {
        $o = Models\BlockCategory::q($name, $parentId, $description, $icon, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Install a model webhook
     *
     * @param string $name
     * @param string $action
     * @param string $modelNamespace
     * @param string $location
     * @param string $verificationKey
     *
     * @return Models\ModelWebhook
     */
    final protected function _installModelWebhook($name, $action, $modelNamespace, $location, $verificationKey) {
        $o = Models\ModelWebhook::q($name, $action, $modelNamespace, $location, $verificationKey, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Install an asset
     *
     * @param bool $isFile
     * @param string $fileName
     * @param string $dirPath
     * @param string $contentTypeMain
     * @param string $contentTypeSub
     * @param string $webUrl
     * @param string $localUrl
     *
     * @return Models\Asset
     */
    final protected function _installAsset($isFile, $fileName, $dirPath, $contentTypeMain, $contentTypeSub, $webUrl, $localUrl
    ) {
        $o = Models\Asset::q($isFile, $fileName, $dirPath, $contentTypeMain, $contentTypeSub, $webUrl, $localUrl, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Install email template
     *
     * @param string $name
     * @param string $description
     * @param string $subject
     * @param string $body
     *
     * @return Models\EmailTemplate
     */
    final protected function _installEmailTemplate($name, $description, $subject, $body) {
        $o = Models\EmailTemplate::q($name, $description, $subject, $body, $this->_extensionName);
        $o->pushThis();
        return $o;
    }

    /**
     * Uninstall all
     *
     */
    final protected function _uninstallAll() {
        $this->_uninstallTasks();
        $this->_uninstallApis();
        $this->_uninstallUserGroups();
        $this->_uninstallPrivileges();
        $this->_uninstallPrivilegeCategorys();
        $this->_uninstallPages();
        $this->_uninstallThemes();
        $this->_uninstallLayouts();
        $this->_uninstallTemplates();
        $this->_uninstallBlocks();
        $this->_uninstallBlockProcessors();
        $this->_uninstallBlockProcessorCategories();
        $this->_uninstallBlockCategories();
        $this->_uninstallModelWebhooks();
        $this->_uninstallAssets();
        $this->_uninstallEmailTemplates();
        $this->_uninstallEnums();
        $this->_uninstallModels();
    }

    /**
     * Uninstall tasks
     *
     */
    final protected function _uninstallTasks() {
        foreach (Models\Task::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall apis
     *
     */
    final protected function _uninstallApis() {
        foreach (Models\Api::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall user groupos
     *
     */
    final protected function _uninstallUserGroups() {
        foreach (Models\UserGroup::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall user privileges
     *
     */
    final protected function _uninstallPrivileges() {
        foreach (Models\UserPrivilege::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall privilege categories
     *
     */
    final protected function _uninstallPrivilegeCategorys() {
        foreach (Models\UserPrivilegeCategory::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall pages
     *
     */
    final protected function _uninstallPages() {
        foreach (Models\Page::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall themes
     *
     */
    final protected function _uninstallThemes() {
        foreach (Models\Theme::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall layouts
     *
     */
    final protected function _uninstallLayouts() {
        foreach (Models\Layout::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall templates
     *
     */
    final protected function _uninstallTemplates() {
        foreach (Models\Template::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall blocks
     *
     */
    final protected function _uninstallBlocks() {
        foreach (Models\Block::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall block processors
     *
     */
    final protected function _uninstallBlockProcessors() {
        foreach (Models\BlockProcessor::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall block processor categories
     *
     */
    final protected function _uninstallBlockProcessorCategories() {
        foreach (Models\BlockProcessorCategory::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall block categories
     *
     */
    final protected function _uninstallBlockCategories() {
        foreach (Models\BlockCategory::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall model webhooks
     *
     */
    final protected function _uninstallModelWebhooks() {
        foreach (Models\ModelWebhook::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall assets
     *
     */
    final protected function _uninstallAssets() {
        foreach (Models\Asset::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall email templates
     *
     */
    final protected function _uninstallEmailTemplates() {
        foreach (Models\EmailTemplate::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall enums
     *
     */
    final protected function _uninstallEnums() {
        foreach (Models\Enum::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }

    /**
     * Uninstall models
     *
     */
    final protected function _uninstallModels() {
        foreach (Models\Model::findAll(Query::n()->where(array("extensionName" => $this->_extensionName))) as $o) {
            $o->deleteThis();
        }
    }
}