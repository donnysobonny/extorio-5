<?php

/**
 * Translate (simple form)
 *
 * @param string $msgid
 *
 * @return string
 */
function t($msgid) {
    return "<span class='notranslate'>".T_gettext($msgid)."</span>";
}

/**
 * Translate and echo (simple form)
 *
 * @param $mgid
 */
function tE($mgid) {
    echo t($mgid);
}

/**
 * Translate (plural form)
 *
 * @param string $singular
 * @param string $plural
 * @param int $number
 *
 * @return string
 */
function tn($singular, $plural, $number) {
    return "<span class='notranslate'>".vsprintf(T_ngettext($singular,$plural,$number),$number)."</span>";
}

/**
 * Translate and echo (plural form)
 *
 * @param string $singular
 * @param string $plural
 * @param int $number
 */
function tnE($singular, $plural, $number) {
    echo tn($singular,$plural,$number);
}