<?php
namespace Core\Classes\Commons\FileSystem;
/**
 * This class extends the File class, giving additional functionality for manipulating images (resizing, cropping etc).
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Image
 */
class Image extends File {

    /**
     * The image resource
     *
     * @var null|resource
     */
    private $image = null;
    /**
     * The image type
     *
     * @var null
     */
    private $imageType = null;

    /**
     * Construct a new Image. If the file is not a valid image, an exception will be thrown
     *
     * @param $filePath
     *
     * @throws \Exception
     */
    public function __construct($filePath) {
        $this->filePath = $filePath;
        //create an image reference
        $image_info = getimagesize($filePath);
        $this->imageType = $image_info[2];
        switch($this->imageType) {
            case IMAGETYPE_JPEG :
                $this->image = imagecreatefromjpeg($this->filePath);
                break;
            case IMAGETYPE_PNG :
                $this->image = imagecreatefrompng($this->filePath);
                break;
            case IMAGETYPE_GIF :
                $this->image = imagecreatefromgif($this->filePath);
                break;
            default:
                throw new \Exception("Invalid file type. An Image object must be created from an image file");
                break;
        }
    }

    /**
     * Save the manipulated file. This should be called in order to commit any changes made to the image.
     *
     * @param int $compression
     *
     * @return bool
     */
    final public function save($compression=75) {
        switch($this->imageType) {
            case IMAGETYPE_JPEG :
                return imagejpeg($this->image, $this->filePath, $compression);
            case IMAGETYPE_PNG :
                return imagepng($this->image, $this->filePath);
            case IMAGETYPE_GIF :
                return imagegif($this->image, $this->filePath);
            default:
                return false;
        }
    }

    /**
     * Get the width
     *
     * @return int
     */
    final public function getWidth() {
        return imagesx($this->image);
    }

    /**
     * Get the height
     *
     * @return int
     */
    final public function getHeight() {
        return imagesy($this->image);
    }

    /**
     * Resize the image, ignoring proportions
     *
     * @param $width
     * @param $height
     *
     * @return bool
     */
    final public function resize($width,$height) {
        $new_image = imagecreatetruecolor($width, $height);
        if(imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight())) {
            $this->image = $new_image;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Resize the image based on a height value, keeping the proportions of the image
     *
     * @param $height
     *
     * @return bool
     */
    final public function resizeToHeight($height) {
        $width = $this->getWidth() * $height / $this->getHeight();
        return $this->resize($width,$height);
    }

    /**
     * Resize the image based on a width value, keeping the proportions of the image
     *
     * @param $width
     *
     * @return bool
     */
    final public function resizeToWidth($width) {
        $height = $this->getheight() * $width / $this->getWidth();
        return $this->resize($width,$height);
    }

    /**
     * Resize the smallest side of the image, while keeping it in proportion
     *
     * @param $size
     *
     * @return bool
     */
    final public function resizeSmallestSide($size) {
        $width = $this->getWidth();
        $height = $this->getHeight();
        if($width < $height) {
            return $this->resizeToWidth($size);
        }
        if($height < $width) {
            return $this->resizeToHeight($size);
        }
        return true;
    }

    /**
     * Resize the largest side of the image, while keeping it in proportion
     *
     * @param $size
     *
     * @return bool
     */
    final public function resizeLargestSide($size) {
        $width = $this->getWidth();
        $height = $this->getHeight();
        if($width > $height) {
            return $this->resizeToWidth($size);
        }
        if($height > $width) {
            return $this->resizeToHeight($size);
        }
        return true;
    }

    /**
     * Crop the image at a certain position
     *
     * @param $width
     * @param $height
     * @param $left
     * @param $top
     *
     * @return bool
     */
    final public function cropAtPosition($width,$height,$left,$top) {
        $new_image = imagecreatetruecolor($width, $height);
        if(imagecopyresampled($new_image, $this->image, 0, 0, $left, $top, $width, $height, $width, $height)) {
            $this->image = $new_image;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Crop the image at it's center
     *
     * @param $width
     * @param $height
     *
     * @return bool
     */
    final public function cropAtCenter($width,$height) {
        $new_image = imagecreatetruecolor($width, $height);
        if(imagecopyresampled($new_image, $this->image, 0, 0, ($this->getWidth()/2)-($width/2), ($this->getHeight()/2)-($height/2), $width, $height, $width, $height)) {
            $this->image = $new_image;
            return true;
        } else {
            return false;
        }
    }
}