<?php
namespace Core\Classes\Commons\FileSystem;

/**
 * This class extends pathInfo and is used specifically for directories (not files!)
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Directory
 */
class Directory extends PathInfo {

    /**
     * @param $filePath
     */
    public function __construct($filePath) {
        $this->filePath = $filePath;
    }

    /**
     * Create a new directory from path. Directories are created recursively
     *
     * @param $filePath
     * @param int $mode
     *
     * @return $this|bool
     */
    public static function createFromPath($filePath,$mode=0775) {
        $dir = Directory::constructFromPath($filePath);
        if(!$dir->exists()) {
            $old = umask(0);
            $check = mkdir($filePath, $mode, true);
            umask($old);
            if(!$check) return false;
        }
        return $dir;
    }

    /**
     * Get all directories within a directory, optionally recursively
     *
     * @param $filePath
     * @param bool $recursive
     *
     * @return Directory[]
     */
    final public static function getDirectoriesWithinDirectory($filePath,$recursive=false) {
        $dirs = array();
        $path = Directory::constructFromPath($filePath);
        if ($path->exists()) {
            $potentials = scandir($path->filePath);

            //unset the back folders
            unset($potentials[0]);
            unset($potentials[1]);
            $potentials = array_values($potentials);

            foreach ($potentials as $potential) {
                //we want dirs
                if (is_dir($path->filePath . "/" . $potential)) {
                    $dirs[] = Directory::constructFromPath($path->filePath . "/" . $potential);
                    if($recursive) {
                        $add = Directory::getDirectoriesWithinDirectory($path->filePath."/".$potential,true);
                        foreach($add as $dir) {
                            $dirs[] = $dir;
                        }
                    }
                }
            }
        }
        return $dirs;
    }

    /**
     * Copy a directory
     *
     * @param $destFilePath
     *
     * @return $this|bool
     */
    final public function copy($destFilePath) {
        $path = Directory::constructFromPath($destFilePath);
        if(copy($this->filePath, $path->filePath)) {
            return Directory::constructFromPath($path->filePath);
        }
        return false;
    }

    /**
     * Rename a directory
     *
     * @param $newFilePath
     *
     * @return $this|bool
     */
    final public function rename($newFilePath) {
        $path = Directory::constructFromPath($newFilePath);
        if(rename($this->filePath,$path->filePath)) {
            return Directory::constructFromPath($path->filePath);
        }
        return false;
    }

    /**
     * Delete a directory. At the moment, this doesn't work recursively or on directories which contain files
     *
     * @return bool
     */
    final public function delete() {
        return unlink($this->filePath);
    }

    /**
     * Change the permissions of a file
     *
     * @param int $mode
     *
     * @return bool
     */
    final public function chmod($mode = 0775) {
        return chmod($this->filePath, $mode);
    }

    /**
     * Change the group of a file
     *
     * @param $group
     *
     * @return bool
     */
    final public function chgrp($group) {
        return chgrp($this->filePath, $group);
    }

    /**
     * Change the owner of a file
     *
     * @param $owner
     *
     * @return bool
     */
    final public function chown($owner) {
        return chown($this->filePath, $owner);
    }
}