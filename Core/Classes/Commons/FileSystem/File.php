<?php
namespace Core\Classes\Commons\FileSystem;

/**
 * This class extends pathInfo, and is used specifically to handle files (not directories!)
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class File
 */
class File extends PathInfo {

    /**
     * When a file is opened, this is the open mode, or FALSE if it is not open.
     *
     * @var bool
     */
    private $openMode = false;
    /**
     * The handle for the file reader
     *
     * @var null
     */
    private $openHandle = null;

    /**
     * @param $filePath
     */
    public function __construct($filePath) {
        $this->filePath = $filePath;
    }

    /**
     * Convert the File to an Asset
     *
     * @return Asset
     */
    public function __toAsset() {
        return new Asset($this->filePath);
    }

    /**
     * Convert the File to an Image
     *
     * @return Image
     */
    public function __toImage() {
        return new Image($this->filePath);
    }

    /**
     * Get all files within a directory, optionally recursively.
     *
     * @param $filePath
     * @param bool $recursive
     *
     * @return $this[]
     */
    final public static function getFilesWithinDirectory($filePath, $recursive = false) {
        $files = array();
        $dir = Directory::constructFromPath($filePath);
        if ($dir->exists()) {
            $potentials = scandir($dir->filePath);

            //unset the back folders
            unset($potentials[0]);
            unset($potentials[1]);
            $potentials = array_values($potentials);

            foreach ($potentials as $potential) {
                //we want files
                if (is_file($dir->filePath . "/" . $potential)) {
                    $files[] = self::constructFromPath($dir->filePath . "/" . $potential);
                } elseif ($recursive && is_dir($dir->filePath . "/" . $potential)) {
                    $add = self::getFilesWithinDirectory($dir->filePath . "/" . $potential, true);
                    foreach ($add as $file) {
                        $files[] = $file;
                    }
                }
            }
        }
        return $files;
    }

    /**
     * Create a file from a path. This also creates the directories that the file should go in if the directories don't
     * exist.
     *
     * @param $filePath
     *
     * @return $this
     */
    final public static function createFromPath($filePath) {
        $file = self::constructFromPath($filePath);
        //make sure the dirname exists first, as it will fail if it doesnt
        if (!Directory::exsistsFromPath($file->dirName())) {
            if (!Directory::createFromPath($file->dirName())) {
                return false;
            }
        }
        //if the file doesn't exist
        if (!$file->exists()) {
            //opening the file will create it
            if (!$file->open("w+")) {
                return false;
            }
            $file->close();
        }
        return $file;
    }

    /**
     * Open this file for modification. Returns opened resource.
     *
     * Note that the methods which modify a file will automatically open the file correctly. You should only open the
     * file yourself if you wish to make custom modifications to it.
     *
     * @param string $mode
     *
     * @return Resource
     */
    final public function open($mode = "r") {
        $this->close();
        $this->openHandle = fopen($this->filePath, $mode);
        if ($this->openHandle) {
            $this->openMode = $mode;
        } else {
            $this->openMode = false;
        }
        return $this->openHandle;
    }

    /**
     * Close the file if it is open
     */
    final public function close() {
        if ($this->openMode) {
            fclose($this->openHandle);

            $this->openMode = false;
            $this->openHandle = null;
        }
    }

    #
    # READING
    #

    /**
     * Read the whole content of a file
     *
     * @return string
     */
    final public function read() {
        if ($this->openMode != "r") {
            $this->open("r");
        }
        return fread($this->openHandle, $this->size());
    }

    /**
     * Read a line of a file as a csv. Best wrapped in a while loop to fetch all files
     *
     * @param null $length
     * @param null $delimiter
     * @param null $enclosure
     * @param null $escape
     *
     * @return array
     */
    final public function getCsv($length = null, $delimiter = null, $enclosure = null, $escape = null) {
        if ($this->openMode != "r") {
            $this->open("r");
        }
        return fgetcsv($this->openHandle, $length, $delimiter, $enclosure, $escape);
    }

    /**
     * Get a character at the file pointer
     *
     * @return string
     */
    final public function getC() {
        if ($this->openMode != "r") {
            $this->open("r");
        }
        return fgetc($this->openHandle);
    }

    /**
     * Get a line at the file pointer. Best wrapped in a while loop to keep fetching each line
     *
     * @param null $length
     *
     * @return string
     */
    final public function getS($length = null) {
        if ($this->openMode != "r") {
            $this->open("r");
        }
        return fgets($this->openHandle, $length);
    }

    /**
     * Get a line at the file pointer and strip out html/php tags
     *
     * @param null $length
     * @param null $allowableTags
     *
     * @return string
     */
    final public function getSs($length = null, $allowableTags = null) {
        if ($this->openMode != "r") {
            $this->open("r");
        }
        return fgetss($this->openHandle, $length, $allowableTags);
    }

    #
    # WRITING
    #

    /**
     * Write to the file. This overwrites the content in the file
     *
     * @param $content
     * @param null $length
     *
     * @return int
     */
    final public function write($content) {
        if ($this->openMode != "w") {
            $this->open("w");
        }
        return fwrite($this->openHandle, $content);
    }

    /**
     * Append to a file. This adds additional content at the end of te file
     *
     * @param $content
     * @param null $length
     *
     * @return int
     */
    final public function append($content) {
        if ($this->openMode != "a") {
            $this->open("a");
        }
        return fwrite($this->openHandle, $content);
    }

    /**
     * Add a set of fields to a csv file.
     *
     * @param $fields
     * @param null $delimiter
     * @param null $enclosure
     *
     * @return int
     */
    final public function putCsv($fields, $delimiter = null, $enclosure = null) {
        if ($this->openMode != "w") {
            $this->open("w");
        }
        return fputcsv($this->openHandle, $fields, $delimiter, $enclosure);
    }

    #
    # UTILITY
    #

    /**
     * Change the permissions of a file
     *
     * @param int $mode
     *
     * @return bool
     */
    final public function chmod($mode = 0775) {
        return chmod($this->filePath, $mode);
    }

    /**
     * Change the group of a file
     *
     * @param $group
     *
     * @return bool
     */
    final public function chgrp($group) {
        return chgrp($this->filePath, $group);
    }

    /**
     * Change the owner of a file
     *
     * @param $owner
     *
     * @return bool
     */
    final public function chown($owner) {
        return chown($this->filePath, $owner);
    }

    /**
     * Copy a file. Note that this will overwrite existing files! This returns the copied file.
     *
     * @param $destFilePath
     *
     * @return $this|bool
     */
    final public function copy($destFilePath) {
        $newFile = self::constructFromPath($destFilePath);
        if (copy($this->filePath, $newFile->filePath)) {
            return self::constructFromPath($newFile->filePath);
        } else {
            return false;
        }
    }

    /**
     * Delete a file
     *
     * @return bool
     */
    final public function delete() {
        return unlink($this->filePath);
    }

    /**
     * Lock a file
     *
     * @param $operation
     *
     * @return bool
     */
    final public function lock($operation) {
        if ($this->openMode) {
            return flock($this->openHandle, $operation);
        } else {
            return false;
        }
    }

    /**
     * Rename a file
     *
     * @param $newName
     *
     * @return $this|bool
     */
    final public function rename($newName) {
        $newFile = self::constructFromPath($newName);
        if (rename($this->filePath, $newFile->filePath)) {
            return self::constructFromPath($newFile->filePath);
        } else {
            return false;
        }
    }

    /**
     * Include the file within the compiler
     *
     * @return mixed
     */
    final public function includeFile() {
        return include $this->filePath;
    }

    /**
     * Include the file within the compiler if it was not already included
     *
     * @return mixed
     */
    final public function includeFileOnce() {
        return include_once $this->filePath;
    }

    /**
     * Include the file within the compiler, but throw an error if the file doesn't exist.
     *
     * @return mixed
     */
    final public function requireFile() {
        return require $this->filePath;
    }

    /**
     * Include the file within the compiler, but throw an error if the file doesn't exist, only if the file hasn't already
     * been included.
     *
     * @return mixed
     */
    final public function requireFileOnce() {
        return require_once $this->filePath;
    }

    /**
     *
     */
    public function __destruct() {
        if ($this->openMode) {
            $this->close();
        }
    }
}