<?php
namespace Core\Classes\Commons\FileSystem;
use Core\Classes\Utilities\Strings;

/**
 * This class is used to generate information on a file.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Core_PathInfo
 */
class PathInfo {
    /**
     * The file path used to construct this file
     *
     * @var string
     */
    protected $filePath = "";

    /**
     * @param $filePath
     */
    public function __construct($filePath) {
        $filePath = Strings::notEndsWith($filePath,"/");
        $this->filePath = $filePath;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->baseName();
    }

    /**
     * Construct an instance based on a filepath.
     *
     * The path can be a file or directory, or neither. An error wont be thrown if
     * the path is invalid, but using the object methods will fail.
     *
     * @param $filePath
     *
     * @return $this
     */
    public static function constructFromPath($filePath) {
        $className = get_called_class();
        return new $className($filePath);
    }

    /**
     * Check whether a file/directory exists at a file path
     *
     * @param $filePath
     *
     * @return bool
     */
    public static function exsistsFromPath($filePath) {
        return file_exists($filePath);
    }

    /**
     * Does the file/directory exist?
     *
     * @return bool
     */
    final public function exists() {
        return file_exists($this->filePath);
    }

    /**
     * The dirname of the file. This is the local path of the file without the file's
     * basename and/or a trailing slash
     *
     * @return string
     */
    final public function dirName() {
        return pathinfo($this->filePath, PATHINFO_DIRNAME);
    }

    /**
     * The basename of the file. This is the file name including the Extension.
     *
     * @return string
     */
    final public function baseName() {
        return pathinfo($this->filePath, PATHINFO_BASENAME);
    }

    /**
     * The Extension name of the file.
     *
     * @return string
     */
    final public function extension() {
        return pathinfo($this->filePath, PATHINFO_EXTENSION);
    }

    /**
     * The file name of the file. This is basically the base name without the Extension.
     *
     * @return mixed|null
     */
    final public function fileName() {
        return pathinfo($this->filePath, PATHINFO_FILENAME);
    }

    /**
     * The permissions of the file
     *
     * @return int
     */
    final public function perms() {
        if (!$this->exists()) {
            return 0;
        } else {
            return fileperms($this->filePath);
        }
    }

    /**
     * The inode of the file
     *
     * @return int
     */
    final public function inode() {
        if (!$this->exists()) {
            return 0;
        } else {
            return fileinode($this->filePath);
        }
    }

    /**
     * The size of the file
     *
     * @return int
     */
    final public function size() {
        if (!$this->exists()) {
            return 0;
        } else {
            return filesize($this->filePath);
        }
    }

    /**
     * The owner of the file
     *
     * @return int
     */
    final public function owner() {
        if (!$this->exists()) {
            return 0;
        } else {
            return fileowner($this->filePath);
        }
    }

    /**
     * The group of the file
     *
     * @return int
     */
    final public function group() {
        if (!$this->exists()) {
            return 0;
        } else {
            return filegroup($this->filePath);
        }
    }

    /**
     * The access time of the file
     *
     * @return int
     */
    final public function ATime() {
        if (!$this->exists()) {
            return 0;
        } else {
            return fileatime($this->filePath);
        }
    }

    /**
     * The modification time of the file
     *
     * @return int
     */
    final public function MTime() {
        if (!$this->exists()) {
            return 0;
        } else {
            return filemtime($this->filePath);
        }
    }

    /**
     * The inode change time
     *
     * @return int
     */
    final public function CTime() {
        if (!$this->exists()) {
            return 0;
        } else {
            return filectime($this->filePath);
        }
    }

    /**
     * The file type
     *
     * @return string
     */
    final public function type() {
        if (!$this->exists()) {
            return null;
        } else {
            return filetype($this->filePath);
        }
    }

    /**
     * Whether the file is writable
     *
     * @return bool
     */
    final public function isWritable() {
        if (!$this->exists()) {
            return false;
        } else {
            return is_writable($this->filePath);
        }
    }

    /**
     * Whether the file is readable
     *
     * @return bool
     */
    final public function isReadable() {
        if (!$this->exists()) {
            return false;
        } else {
            return is_readable($this->filePath);
        }
    }

    /**
     * Whether the file is executable
     *
     * @return bool
     */
    final public function isExecutable() {
        if (!$this->exists()) {
            return false;
        } else {
            return is_executable($this->filePath);
        }
    }

    /**
     * Whether the file is a file
     *
     * @return bool
     */
    final public function isFile() {
        if (!$this->exists()) {
            return false;
        } else {
            return is_file($this->filePath);
        }
    }

    /**
     * Whether the file is a directory
     *
     * @return bool
     */
    final public function isDir() {
        if (!$this->exists()) {
            return false;
        } else {
            return is_dir($this->filePath);
        }
    }

    /**
     * Whether the file is a link
     *
     * @return bool
     */
    final public function isLink() {
        if (!$this->exists()) {
            return false;
        } else {
            return is_link($this->filePath);
        }
    }

    /**
     * Get the link of the file
     *
     * @return string
     */
    final public function link() {
        if (!$this->exists()) {
            return null;
        } else {
            return readlink($this->filePath);
        }
    }

    /**
     * Get the realpath of the file
     *
     * @return string
     */
    final public function realPath() {
        if (!$this->exists()) {
            return null;
        } else {
            return realpath($this->filePath);
        }
    }
}