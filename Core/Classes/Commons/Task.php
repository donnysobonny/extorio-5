<?php
namespace Core\Classes\Commons;
use Core\Classes\Utilities\Tasks;

/**
 * The base class extended by task components. Tasks are used to run tasks in the background, that can be monitored in
 * the task management system
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Task
 */
class Task extends Component {
    /**
     * The default event is called if there is no target method, or the target method doesn't exist
     */
    public function _onDefault() {

    }

    /**
     * Log a message to the live task entry
     *
     * @param $message
     */
    final public function _logToTask($message) {
        Tasks::logToLiveTask(getmypid(),$message);
    }

    /**
     * Safely fail a task, and log an error to the live task
     *
     * @param $message
     */
    final public function _failTask($message) {
        Tasks::failLiveTask(getmypid(),$message);
        exit;
    }
}