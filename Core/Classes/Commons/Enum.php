<?php
namespace Core\Classes\Commons;
/**
 * Enums are used to store a strict set of values, and closely resemble the Enumeration type seen in C languages.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Enum
 */
class Enum {

    /**
     * Get the values of this enum
     *
     * @return array
     */
    public static function values() {
        return array();
    }

    /**
     * Get the value at an index point
     *
     * @param $index
     *
     * @return mixed
     */
    final public static function value($index) {
        $class = get_called_class();
        $values = $class::values();
        return isset($values[$index]) ? $values[$index] : null;
    }

    /**
     * Get whether a value exists in the enum's values
     *
     * @param $value
     *
     * @return bool
     */
    final public static function valueExists($value) {
        $class = get_called_class();
        $values = $class::values();
        return in_array($value,$values) ? true : false;
    }

    /**
     * Get the enum's default value (the value at values[0])
     *
     * @return mixed
     */
    final public static function defaultValue() {
        $class = get_called_class();
        $values = $class::values();
        return count($values) ? $values[0] : null;
    }

}