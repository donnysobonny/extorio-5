<?php
namespace Core\Classes\Commons;
/**
 * The base class extended by api components. An api component is used to easily create apis.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Api
 */
class Api extends Component {

    /**
     * The params that exist in the requests GET params (in the query string)
     *
     * Note that these params have been passed through Arrays::smartCast
     *
     * @var array
     */
    public $_getParams = array();
    /**
     * The params that exist in the requests POST params
     *
     * Note that custom requests (such as a PUT request) are unlikely to include the params in the POST params. In stead,
     * they are likely to exist in the input stream and therefore will be found in allParams
     *
     * Note that these params have been passed through Arrays::smartCast
     *
     * @var array
     */
    public $_postParams = array();
    /**
     * All GET, POST and input stream params.
     *
     * Note that these params have been passed through Arrays::smartCast
     *
     * @var array
     */
    public $_allParams = array();
    /**
     * For requests that send files, this includes the FILE params
     *
     * @var array
     */
    public $_fileData = array();
    /**
     * The HTTP METHOD that was requested
     *
     * @var string
     */
    public $_httpMethod;
    /**
     * The target component method
     *
     * @var string
     */
    public $_method;
    /**
     * The params being passed to the target component method (if any)
     *
     * @var array
     */
    public $_methodParams = array();

    /**
     * This is the output that will be encoded and displayed as the result of the api request
     *
     * @var \stdClass
     */
    public $_output;

    /**
     * This controls how the output is encoded when the _display method is called. All requests can modify this.
     *
     * @var string
     */
    public $output_type = "json";

    /**
     * Display the api output, encoded as configured.
     *
     * Note that this causes the application to EXIT, meaning that no further actions can be taken once this method is called
     */
    final public function _display($statusCode = 200) {
        if($message = $this->_Extorio()->getMessagesError()) {
            if(property_exists($this->_output,"error") && !$this->_output->error) {
                $this->_output->error = true;
                $this->_output->error_message = $message;
            } else {
                $this->_output->error = true;
                $this->_output->error_message = $message;
            }
        }
        if($message = $this->_Extorio()->getMessagesWarning()) {
            $this->_output->warning_message = $message;
        }
        if($message = $this->_Extorio()->getMessagesInfo()) {
            $this->_output->info_message = $message;
        }
        if($message = $this->_Extorio()->getMessagesSuccess()) {
            $this->_output->success_message = $message;
        }
        header("HTTP/1.1 ".$statusCode." ".$this->_output->error_message);
        switch($this->output_type) {
            case "json" :
                $this->_displayAsJson();
                break;
            case "xml" :
                $this->_displayAsXml();
                break;
            default :
                $this->_displayAsJson();
                break;
        }
        exit;
    }

    /**
     * Fail the api, stopping any further action and safely displaying the error
     *
     * @param string $message
     * @param int $statusCode
     */
    final public function _failApi($message="",$statusCode=500) {
        $this->_output->error = true;
        $this->_output->error_message = $message;
        $this->_display($statusCode);
    }

    /**
     * Fail the api due to a bad request (responds with status code 400)
     *
     * @param $message
     */
    final public function _badRequest($message="") {
        $this->_failApi($message,400);
    }

    /**
     * Fail the api due to unauthorized access (responds with status code 401)
     *
     * @param string $message
     */
    final public function _accessDenied($message="") {
        $this->_failApi($message,401);
    }

    /**
     * Fail the api due to resource not being found (responds with status code 404)
     *
     * @param string $message
     */
    final public function _notFound($message="") {
        $this->_failApi($message,404);
    }

    /**
     * Display the output as encoded json
     */
    private function _displayAsJson() {
        print(json_encode($this->_output));
    }

    /**
     * Display the output as encoded xml
     */
    private function _displayAsXml() {
        echo "currently not supported";
    }

    /**
     * The onDefault event is called on the api component if there is no target method
     */
    public function _onDefault() {}
}