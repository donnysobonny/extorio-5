<?php
namespace Core\Classes\Commons;
use Core\Classes\Models;

/**
 * The base class for all components. It is an aggregate class that contains functionality used by all components
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Component
 */
class Component {

    /**
     * The type of component
     *
     * @var string
     */
    public $_componentType = "";
    /**
     * The name of the extension that owns the component
     *
     * @var string
     */
    public $_extensionName = "";

    /**
     * Prepend content to the head of the document
     *
     * @param $content
     */
    final public function _prependToHead($content) {
        $this->_Extorio()->prependToHead($content);
    }

    /**
     * Get the Extorio instance
     *
     * @return Extorio
     * @throws \Core\Classes\Exceptions\Extorio_Exception
     */
    final public function _Extorio() {
        return Extorio::get();
    }

    /**
     * Append content to the head of the document
     *
     * @param $content
     */
    final public function _appendToHead($content) {
        $this->_Extorio()->appendToHead($content);
    }

    /**
     * Prepend content to the body of the document
     *
     * @param $content
     */
    final public function _prependToBody($content) {
        $this->_Extorio()->prependToBody($content);
    }

    /**
     * Append content to the body of the document
     *
     * @param $content
     */
    final public function _appendToBody($content) {
        $this->_Extorio()->appendToBody($content);
    }

    /**
     * Include an in includer
     *
     * @param $includer
     *
     * @return bool
     * @throws \Exception
     */
    final public function _includeIncluder($includer) {
        return $this->_Extorio()->includeIncluder($includer);
    }

    /**
     * Create an error message
     *
     * @param $message
     */
    final public function _messageError($message) {
        $this->_Extorio()->messageError($message);
    }

    /**
     * Create a warning message
     *
     * @param $message
     */
    final public function _messageWarning($message) {
        $this->_Extorio()->messageWarning($message);
    }

    /**
     * Create an in info message
     *
     * @param $message
     */
    final public function _messageInfo($message) {
        $this->_Extorio()->messageInfo($message);
    }

    /**
     * Create a success message
     *
     * @param $message
     */
    final public function _messageSuccess($message) {
        $this->_Extorio()->messageSuccess($message);
    }

    /**
     * Log a message to a custom log file
     *
     * @param $fileName
     * @param $message
     */
    final public function _logCustom($fileName, $message) {
        $this->_Extorio()->logCustom($fileName,$message);
    }

    /**
     * Add a message to the debug log
     *
     * @param $message
     */
    final public function _logDebug($message) {
        $this->_Extorio()->logDebug($message);
    }

    /**
     * Add a message to the warning log
     *
     * @param $message
     */
    final public function _logWarning($message) {
        $this->_Extorio()->logWarning($message);
    }

    /**
     * Add a message to the error log
     *
     * @param $message
     */
    final public function _logError($message) {
        $this->_Extorio()->logError($message);
    }

    /**
     * Get the logged in user
     *
     * @return Models\User
     */
    final public function _getLoggedInUser() {
        return $this->_Extorio()->getLoggedInUser();
    }

    /**
     * Get the logged in user's session
     *
     * @return string
     */
    final public function _getLoggedInUserSession() {
        return $this->_Extorio()->getLoggedInUserSession();
    }

    /**
     * Get the id of the logged in user
     *
     * @return int
     */
    final public function _getLoggedInUserId() {
        return $this->_Extorio()->getLoggedInUserId();
    }

    /**
     * Get the user group of the logged in user
     *
     * @return Models\UserGroup
     */
    final public function _getLoggedInUserGroup() {
        return $this->_Extorio()->getLoggedInUserGroup();
    }

    /**
     * Get id of the user group of the logged in user
     *
     * @return int
     */
    final public function _getLoggedInUserGroupId() {
        return $this->_Extorio()->getLoggedInUserGroupId();
    }

    /**
     * Get the user groups that the logged in user manages
     *
     * @return Models\UserGroup[]
     */
    final public function _getLoggedInUserManageGroups() {
        return $this->_Extorio()->getLoggedInUserManageGroups();
    }

    /**
     * Get the config array
     *
     * @return array
     */
    final public function _getConfig() {
        return $this->_Extorio()->getConfig();
    }

    /**
     * Get the default db instance
     *
     * @return \Core\Classes\Helpers\PGDB
     */
    final public function _getDbInstanceDefault() {
        return $this->_Extorio()->getDbInstanceDefault();
    }

    /**
     * Cache the output of this component for a number of seconds.
     *
     * This simply checks for the HTTP_IF_MODIFIED_SINCE server param, exits with 304 Not Modified if the number of seconds has not passed.
     *
     * @param int $seconds
     */
    final public function _cacheOutput($seconds=1800) {
        $modSince = $_SERVER['HTTP_IF_MODIFIED_SINCE'];
        if($modSince) {
            if(time() - strtotime($modSince) <= $seconds) {
                header("HTTP/1.1 304 Not Modified");
                header("Cache-Control: public, max-age=".$seconds);
                exit;
            }
        }
        header("Last-Modified: ".gmdate('D, d M Y H:i:s ', time()) . 'GMT');
        header("Cache-Control: public, max-age=".$seconds);
    }

    /**
     * Get this Components Extension
     *
     * @return Extension
     */
    final public function _Extension() {
        return $this->_Extorio()->getExtension($this->_extensionName);
    }

    /**
     * The begin event is always called before the component is actually used
     */
    public function _onBegin() {}

    /**
     * The end event is always called when the component is finished being used
     */
    public function _onEnd() {}
}