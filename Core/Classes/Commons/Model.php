<?php
namespace Core\Classes\Commons;
use Core\Classes\Enums;
use Core\Classes\Exceptions\DB_Exception;
use Core\Classes\Helpers\Model_ResultSet;
use Core\Classes\Helpers\PGDB_ResultSet;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities;

/**
 * The base class extended by all model classes. This ultimately sets up a class as persistent.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Model
 */
class Model {

    /**
     * Every object must have an id
     *
     * @var int
     */
    public $id = 0;

    /**
     * During update, we store the old version of the stored object in this property, allowing us to check new against
     * old during the update event.
     *
     * @var $_old $this
     */
    protected $_old;

    /**
     * While pushing, this tells us whether the object is being created, updated, or neither
     *
     * @var string
     */
    protected $_pushMode = "none";

    /**
     * While pushing, this tells us whether the push mode can happen (based on access)
     *
     * @var bool
     */
    protected $_canPush = false;

    /**
     * While pushing, this tells us whether we actually want to commit the push. Created are always committed, while
     * updates are only committed if something changes
     *
     * @var bool
     */
    protected $_commitPush = false;
    /**
     * Lazily construct an empty instance of this model
     *
     * @return $this
     */
    final public static function n() {
        $baseType = self::getClass();
        return new $baseType();
    }

    /**
     * Lazily get the class of this model
     *
     * @return string
     */
    final public static function getClass() {
        return Utilities\Strings::startsWith(get_called_class(),"\\");
    }

    /**
     * Find a result set of this type based onb a query.
     *
     * @param Query $query
     *
     * @return Model_ResultSet
     * @throws DB_Exception
     */
    final public static function findResultSet($query = null) {
        $query = Query::constructFromMixed($query);

        /** @var Model $baseType */
        $baseType = self::getClass();
        //set the base namespace
        $query->setBaseModelNamespace($baseType);
        //build the sql
        $sql = $query->getSelectSql();
        $sql .= $query->getJoinsSql();
        $params = array();
        $sql .= $query->getWhereSql($params);
        $sql .= $query->getGroupSql();
        $sql .= $query->getOrderSql();
        $sql .= $query->getLimitSql();

        $db = Extorio::get()->getDbInstanceDefault();
        return new Model_ResultSet($baseType,$db->query($sql,$params));
    }

    /**
     * Find one instance of this type, based on a query and depth
     *
     * @param Query $query
     * @param int $depth
     *
     * @return $this
     */
    final public static function findOne(
        $query = null,
        $depth = 1
    ) {
        $query = Query::constructFromMixed($query);

        //force the limit to 1
        $query->limit(1);
        $query->skip(0);

        /** @var Model $baseType */
        $baseType = self::getClass();
        $results = $baseType::findAll($query,$depth);
        if(count($results)) {
            return $results[0];
        } else {
            return null;
        }
    }

    /**
     * Find all instances of this type based on a query and depth.
     *
     * @param Query $query
     * @param int $depth
     *
     * @return $this[]
     */
    final public static function findAll($query = null,
        $depth = 1
    ) {
        $query = Query::constructFromMixed($query);

        /** @var Model $baseType */
        $baseType = self::getClass();
        //set the base namespace
        $query->setBaseModelNamespace($baseType);
        //build the sql
        $sql = $query->getSelectSql();
        $sql .= $query->getJoinsSql();
        $params = array();
        $sql .= $query->getWhereSql($params);
        $sql .= $query->getGroupSql();
        $sql .= $query->getOrderSql();
        $sql .= $query->getLimitSql();

        $db = Extorio::get()->getDbInstanceDefault();
        return $baseType::findAllByResultSet($db->query($sql, $params), $depth);
    }

    /**
     * Find all instances of this type based on a result set and depth.
     *
     * The first column of each row in the results should be the id of the instance
     *
     * @param PGDB_ResultSet $result
     * @param int $depth
     *
     * @return $this[]
     */
    final public static function findAllByResultSet(PGDB_ResultSet $result, $depth = 1) {
        /** @var Model $baseType */
        $baseType = self::getClass();
        $results = array();
        while ($row = $result->fetchRow()) {
            $obj = new $baseType();
            $obj->id = intval($row[0]);
            if ($obj->pullThis($depth)) {
                $results[] = $obj;
            }
        }
        return $results;
    }

    /**
     * Find out if one instance exists with a query
     *
     * @param Query $query
     *
     * @return bool
     */
    final public static function existsOne($query = null) {
        /** @var Model $baseType */
        $baseType = self::getClass();
        if ($baseType::findCount($query, 1) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Find the count of instance of this type, based on a query.
     *
     * This is far more efficient than finding all and counting the results.
     *
     * @param Query $query
     *
     * @return int
     * @throws DB_Exception
     * @throws \Exception
     */
    final public static function findCount(
        $query = null
    ) {
        $query = Query::constructFromMixed($query);

        /** @var Model $baseType */
        $baseType = self::getClass();
        //set the base table
        $query->setBaseModelNamespace($baseType);
        //build the sql
        $sql = $query->getSelectSql();
        $sql .= $query->getJoinsSql();
        $params = array();
        $sql .= $query->getWhereSql($params);
        $sql .= $query->getGroupSql();
        $sql .= $query->getOrderSql();
        $sql .= $query->getLimitSql();

        $db = Extorio::get()->getDbInstanceDefault();
        $rs = $db->query($sql,$params);

        return $rs->numRows();
    }

    /**
     * Find if one instance exists by id
     *
     * @param int $id
     *
     * @return bool
     */
    final public static function existsById($id) {
        /** @var Model $baseType */
        $baseType = self::getClass();
        if($baseType::findCount(Query::n()->where(array("id" => $id))) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Push this object to the database
     *
     * @throws DB_Exception
     * @throws \Core\Classes\Exceptions\Extension_Exception
     * @throws \Core\Classes\Exceptions\General_Exception
     * @throws \Exception
     */
    final public function pushThis() {
        $args = func_get_args();
        //0 is whether we are working on a child
        if (isset($args[0])) {
            $workingOnChild = $args[0];
        } else {
            $workingOnChild = false;
        }

        $db = Extorio::get()->getDbInstanceDefault();

        //if working on the base object
        if (!$workingOnChild) {
            //prepare ids
            $this->prepareIds();
            //prepare the properties
            $this->prepareProperties();
            //prepare for updates, defining whether we are committing to a push
            $this->prepareUpdates();
            //create a temp version of the object to compare after the push event
            $temp = $this->__toStdClass();
            //run the beforePush event
            $this->beforePush();
            //if the object changed, we need to re-prepare
            if ($temp != $this->__toStdClass()) {
                $this->prepareIds();
                $this->prepareProperties();
                $this->prepareUpdates();
            }
            //clear $temp
            $temp = null;
        }

        //at this point, the object can no longer be modified, so we start preparing it to be stored in the database
        if ($this->_commitPush) {
            $table = Utilities\Models::getTableName($this->getClass());
            switch ($this->_pushMode) {
                case Enums\PushMode::create :
                    $params = array();
                    $fields = "(";
                    $values = "(";
                    $n = 1;
                    $fields .= '"id",';
                    $values .= '$' . $n . ',';
                    $params[] = $this->id;
                    $n++;
                    foreach ($this->internal_basicProperties() as $name => $data) {
                        $fields .= '"' . $name . '",';
                        $values .= "$" . $n . ",";
                        $params[] = $this->$name;
                        $n++;
                    }
                    foreach ($this->internal_enumProperties() as $name => $data) {
                        $fields .= '"' . $name . '",';
                        $values .= "$" . $n . ",";
                        $params[] = $this->$name;
                        $n++;
                    }
                    foreach ($this->internal_metaProperties() as $name) {
                        $fields .= '"' . $name . '",';
                        $values .= "$" . $n . ",";
                        $params[] = json_encode($this->$name);
                        $n++;
                    }
                    foreach ($this->internal_complexProperties() as $name => $data) {
                        $fields .= '"' . $name . '",';
                        $values .= "$" . $n . ",";
                        switch ($data["type"]) {
                            case Enums\PHPTypes::_object :
                                /** @var Model $child */
                                $child = $this->$name;
                                if (gettype($this->$name) == Enums\PHPTypes::_object) {
                                    $params[] = $child->id;
                                } else {
                                    $params[] = null;
                                }
                                break;
                            case Enums\PHPTypes::_array :
                                /** @var Model[] $children */
                                $children = $this->$name;
                                $ids = array();
                                if (gettype($children) == Enums\PHPTypes::_array) {
                                    for ($i = 0; $i < count($children); $i++) {
                                        if (gettype($children[$i]) == Enums\PHPTypes::_object) {
                                            $ids[] = $children[$i]->id;
                                        }
                                    }
                                }
                                $params[] = Utilities\PGDB::encodeArray($ids);
                                break;
                        }
                        $n++;
                    }
                    $fields = substr($fields, 0, strlen($fields) - 1) . ")";
                    $values = substr($values, 0, strlen($values) - 1) . ")";
                    $sql = '
                    INSERT INTO "' . $table . '" ' . $fields . ' VALUES ' . $values . '
                    ';
                    $db->query($sql, $params);
                    //create the modcache
                    Utilities\Models::createModCache($this->getClass(), $this->id, "create", null, json_encode($this));
                    break;
                case Enums\PushMode::update :
                    $params = array();
                    $set = "";
                    $n = 1;
                    $set .= '"id" = $' . $n . ', ';
                    $params[] = $this->id;
                    $n++;
                    foreach ($this->internal_basicProperties() as $name => $data) {
                        $set .= '"' . $name . '" = $' . $n . ', ';
                        $params[] = $this->$name;
                        $n++;
                    }
                    foreach ($this->internal_enumProperties() as $name => $data) {
                        $set .= '"' . $name . '" = $' . $n . ', ';
                        $params[] = $this->$name;
                        $n++;
                    }
                    foreach ($this->internal_metaProperties() as $name) {
                        $set .= '"' . $name . '" = $' . $n . ', ';
                        $params[] = json_encode($this->$name);
                        $n++;
                    }
                    foreach ($this->internal_complexProperties() as $name => $data) {
                        $set .= '"' . $name . '" = $' . $n . ', ';
                        switch ($data["type"]) {
                            case Enums\PHPTypes::_object :
                                /** @var Model $child */
                                $child = $this->$name;
                                if (gettype($this->$name) == Enums\PHPTypes::_object) {
                                    $params[] = $child->id;
                                } else {
                                    $params[] = null;
                                }
                                break;
                            case Enums\PHPTypes::_array :
                                /** @var Model[] $children */
                                $children = $this->$name;
                                $ids = array();
                                if (gettype($children) == Enums\PHPTypes::_array) {
                                    for ($i = 0; $i < count($children); $i++) {
                                        if (gettype($children[$i]) == Enums\PHPTypes::_object) {
                                            $ids[] = $children[$i]->id;
                                        }
                                    }
                                }
                                $params[] = Utilities\PGDB::encodeArray($ids);
                                break;
                        }
                        $n++;
                    }
                    $set = substr($set, 0, strlen($set) - 2);
                    $sql = '
                    UPDATE "' . $table . '" SET ' . $set . ' WHERE id = ' . $this->id . '
                    ';
                    $db->query($sql, $params);
                    //create the modcache
                    Utilities\Models::createModCache($this->getClass(), $this->id, "update", json_encode($this->_old), json_encode($this));
                    break;
            }

            //if autofire on webhooks is enabled, and they are enabled here
            $config = Extorio::get()->getConfig();
            $core = Extorio::get()->getExtension("Core");
            if ($config["models"]["webhooks"]["autofire"] && $core) {
                Extorio::get()->runTask_Background($core->_constructComponent('\Core\Components\Tasks\Data'), false, "webhook", array(Utilities\Namespaces::getClassName($this->getClass()), $this->id, $this->_pushMode == Enums\PushMode::create ? "create" : "update"));
            }

            //recurse
            foreach ($this->internal_complexProperties() as $name => $data) {
                switch ($data["type"]) {
                    case Enums\PHPTypes::_object :
                        $child = $this->$name;
                        if (gettype($child) == Enums\PHPTypes::_object) {
                            $child->pushThis(true);
                        }
                        break;
                    case Enums\PHPTypes::_array :
                        $children = $this->$name;
                        if (gettype($children) == Enums\PHPTypes::_array) {
                            for ($i = 0; $i < count($children); $i++) {
                                if (gettype($children[$i]) == Enums\PHPTypes::_object) {
                                    $children[$i]->pushThis(true);
                                }
                            }
                        }
                        break;
                }
            }
        }

        //if working on the base object
        if (!$workingOnChild) {
            //run the afterPush event
            $this->afterPush();
        }
    }

    /**
     * This process makes sure that all the "id" properties of the contained objects are using a valid id. This MUST be done before pushing.
     */
    final public function prepareIds() {
        //based on whether the id is set, get/set the sequence
        $this->id = intval($this->id);
        if($this->id) {
            //set it
            Utilities\Models::setSequenceCurrentValue($this->getClass(),$this->id);
        } else {
            //get it
            $this->id = intval(Utilities\Models::getSequenceNextVal($this->getClass()));
        }
        //recurse
        foreach($this->internal_complexProperties() as $name => $data) {
            $childModelNamespace = $data["childModelNamespace"];
            switch($data["type"]) {
                case Enums\PHPTypes::_object :
                    $child = $this->$name;
                    if(gettype($child) == Enums\PHPTypes::_object) {
                        if(Utilities\Namespaces::getNamespace($child) == $childModelNamespace) {
                            $child->prepareIds();
                        }
                    }
                    $this->$name = $child;
                    break;
                case Enums\PHPTypes::_array :
                    $children = $this->$name;
                    if(gettype($children) == Enums\PHPTypes::_array) {
                        for($i = 0; $i < count($children); $i++) {
                            if(gettype($children[$i]) == Enums\PHPTypes::_object) {
                                if(Utilities\Namespaces::getNamespace($children[$i]) == $childModelNamespace) {
                                    $children[$i]->prepareIds();
                                }
                            }
                        }
                    }
                    $this->$name = $children;
                    break;
            }
        }
    }

    /**
     *
     * @return array
     */
    protected static function internal_complexProperties() {
        return array();
    }

    /**
     * This process is designed to prepare the properties of the objects. Nulls are reverted to old, updates that cannot
     * update are reverted to old, creates that cannot create are removed.
     *
     * @param $ignoreAccess
     *
     * @throws \Exception
     */
    final private function prepareProperties() {
        $args = func_get_args();
        //0 is workingOnChild
        if(isset($args[0])) {
            $workingOnChild = $args[0];
        } else {
            $workingOnChild = false;
        }

        //if we are on the base object, prepare internals directly
        if(!$workingOnChild) {
            //resolve references
            $this->resolveThis();
            $this->prepareInternals();
        }

        //if we are on the base object, creating, and unable to create, we have no choice but to stop
        if(!$workingOnChild && $this->_pushMode == Enums\PushMode::create && !$this->_canPush) {
            throw new \Exception("Trying to create ".$this->getClass()." but unable to create");
        }

        //if we are updating, we need to modify the values based on whether we can push
        if($this->_pushMode == Enums\PushMode::update) {
            foreach($this->internal_basicProperties() as $name => $data) {
                if(!$this->_canPush) {
                    //revert to old
                    $this->$name = $this->_old->$name;
                } else {
                    //if null, revert to old
                    if(gettype($this->$name) == Enums\PHPTypes::_nULL) {
                        $this->$name = $this->_old->$name;
                    }
                }
            }
            foreach($this->internal_enumProperties() as $name => $data) {
                if(!$this->_canPush) {
                    //revert to old
                    $this->$name = $this->_old->$name;
                } else {
                    //if null, revert to old
                    if(gettype($this->$name) == Enums\PHPTypes::_nULL) {
                        $this->$name = $this->_old->$name;
                    }
                }
            }
            foreach($this->internal_metaProperties() as $name) {
                if(!$this->_canPush) {
                    //revert to old
                    $this->$name = $this->_old->$name;
                } else {
                    //if null, revert to old
                    if(gettype($this->$name) == Enums\PHPTypes::_nULL) {
                        $this->$name = $this->_old->$name;
                    }
                }
            }
            foreach($this->internal_complexProperties() as $name => $data) {
                $childModelNamespace = $data["childModelNamespace"];
                switch($data["type"]) {
                    case Enums\PHPTypes::_object :
                        if(!$this->_canPush) {
                            if(gettype($this->_old->$name) == Enums\PHPTypes::_object) {
                                $this->$name = clone($this->_old->$name);
                            }
                        } else {
                            if(gettype($this->$name) == Enums\PHPTypes::_nULL) {
                                if(gettype($this->_old->$name) == Enums\PHPTypes::_object) {
                                    $this->$name = clone($this->_old->$name);
                                }
                            }

                            /** @var Model $child */
                            $child = $this->$name;
                            if(gettype($child) == Enums\PHPTypes::_object) {
                                if(Utilities\Namespaces::getNamespace($child) == $childModelNamespace) {
                                    $child->prepareInternals();
                                    //if we are creating the child, but cannot create it, remove it
                                    if($child->_pushMode == Enums\PushMode::create && !$child->_canPush) {
                                        $child = null;
                                    } else {
                                        $child->prepareProperties(true);
                                    }
                                } else {
                                    $child = null;
                                }
                            }
                            $this->$name = $child;
                        }
                        break;
                    case Enums\PHPTypes::_array :
                        if(!$this->_canPush) {
                            $this->$name = null;
                            if(gettype($this->_old->$name) == Enums\PHPTypes::_array) {
                                $children = $this->_old->$name;
                                for($i = 0; $i < count($children); $i++) {
                                    $children[$i] = clone($children[$i]);
                                }
                                $this->$name = $children;
                            }
                        } else {
                            if(gettype($this->$name) == Enums\PHPTypes::_nULL) {
                                if(gettype($this->_old->$name) == Enums\PHPTypes::_array) {
                                    $children = $this->_old->$name;
                                    for($i = 0; $i < count($children); $i++) {
                                        $children[$i] = clone($children[$i]);
                                    }
                                    $this->$name = $children;
                                }
                            }
                        }
                        /** @var Model[] $children */
                        $children = $this->$name;
                        $newChildren = array();
                        if(gettype($children) == Enums\PHPTypes::_array) {
                            for($i = 0; $i < count($children); $i++) {
                                if(gettype($children[$i]) == Enums\PHPTypes::_object) {
                                    if(Utilities\Namespaces::getNamespace($children[$i]) == $childModelNamespace) {
                                        $children[$i]->prepareInternals();
                                        //if we are updating, we will be reverting to old if we cannot push
                                        //so add children that are being updated and/or created that can be created
                                        if($children[$i]->_pushMode == Enums\PushMode::update ||
                                            ($children[$i]->_pushMode == Enums\PushMode::create && $children[$i]->_canPush)
                                        ) {
                                            $children[$i]->prepareProperties(true);
                                            //append
                                            $newChildren[] = $children[$i];
                                        }
                                    }
                                }
                            }
                        }
                        $this->$name = $newChildren;
                        break;
                }
            }
        } else {
            foreach($this->internal_complexProperties() as $name => $data) {
                $childModelNamespace = $data["childModelNamespace"];
                switch($data["type"]) {
                    case Enums\PHPTypes::_object :
                        /** @var Model $child */
                        $child = $this->$name;
                        if(gettype($child) == Enums\PHPTypes::_object) {
                            if(Utilities\Namespaces::getNamespace($child) == $childModelNamespace) {
                                $child->prepareInternals();
                                //if we are creating the child, but cannot create it, remove it
                                if($child->_pushMode == Enums\PushMode::create && !$child->_canPush) {
                                    $child = null;
                                } else {
                                    $child->prepareProperties(true);
                                }
                            } else {
                                $child = null;
                            }
                        }
                        $this->$name = $child;
                        break;
                    case Enums\PHPTypes::_array :
                        /** @var Model[] $children */
                        $children = $this->$name;
                        $newChildren = array();
                        if(gettype($children) == Enums\PHPTypes::_array) {
                            for($i = 0; $i < count($children); $i++) {
                                if(gettype($children[$i]) == Enums\PHPTypes::_object) {
                                    if(Utilities\Namespaces::getNamespace($children[$i]) == $childModelNamespace) {
                                        $children[$i]->prepareInternals();
                                        //if we are updating, we will be reverting to old if we cannot push
                                        //so add children that are being updated and/or created that can be created
                                        if($children[$i]->_pushMode == Enums\PushMode::update ||
                                            ($children[$i]->_pushMode == Enums\PushMode::create && $children[$i]->_canPush)
                                        ) {
                                            $children[$i]->prepareProperties(true);
                                            //append
                                            $newChildren[] = $children[$i];
                                        }
                                    }
                                }
                            }
                        }
                        $this->$name = $newChildren;
                        break;
                }
            }
        }

        //cast the properties
        $this->id = intval($this->id);
        foreach($this->internal_basicProperties() as $name => $data) {
            $this->$name = $this->castBasicProperty($data["basicType"],$this->$name);
        }
        foreach($this->internal_enumProperties() as $name => $data) {
            $this->$name = $this->castEnumProperty($data["enumNamespace"],$this->$name);
        }
        foreach($this->internal_metaProperties() as $name) {
            $this->$name = $this->castMetaProperty($this->$name);
        }
    }

    /**
     * Resolve all references, recursively
     */
    final public function resolveThis() {
        foreach($this->internal_complexProperties() as $name => $data) {
            $childModelNamespace = $data["childModelNamespace"];
            switch($data["type"]) {
                case Enums\PHPTypes::_object :
                    $child = $this->$name;
                    if (gettype($child) == Enums\PHPTypes::_integer) {
                        $child = new $childModelNamespace();
                        $child->id = $this->$name;
                        $child->pullThis(INF);
                    } elseif (gettype($child) == Enums\PHPTypes::_object) {
                        if (Utilities\Namespaces::getNamespace($child) == $childModelNamespace) {
                            $child->resolveThis();
                        }
                    }
                    $this->$name = $child;
                    break;
                case Enums\PHPTypes::_array :
                    $children = $this->$name;
                    if(gettype($children) == Enums\PHPTypes::_array) {
                        for($i = 0; $i < count($children); $i++) {
                            if (gettype($children[$i]) == Enums\PHPTypes::_integer) {
                                $id = $children[$i];
                                $children[$i] = new $childModelNamespace();
                                $children[$i]->id = $id;
                                $children[$i]->pullThis(INF);
                            } elseif (gettype($children[$i]) == Enums\PHPTypes::_object) {
                                if (Utilities\Namespaces::getNamespace($children[$i]) == $childModelNamespace) {
                                    $children[$i]->resolveThis();
                                }
                            }
                        }
                    }
                    $this->$name = $children;
                    break;
            }
        }
    }

    /**
     * This process prepares the object internally, assigning the internal_old, internal_pushMode and internal_canPush.
     * The purpose of this process is to discover how we INTEND to deal with each object. Whether we commit this intention
     * is discovered later.
     *
     * @param $ignoreAccess
     */
    final private function prepareInternals() {
        //try and find the old using the modcache
        $mc = Utilities\Models::getModCache_MostRecent($this->getClass(), $this->id, "update");
        if (!$mc) {
            $mc = Utilities\Models::getModCache_MostRecent($this->getClass(), $this->id, "create");
        }
        if ($mc) {
            $this->_old = $this->constructFromArray(json_decode($mc["dataAfter"], true));
        } else {
            //just find it instead
            $this->_old = $this->findById($this->id, INF);
        }

        //discover whether we are pushing/updating
        if ($this->_old) {
            $this->_pushMode = Enums\PushMode::update;
        } else {
            $this->_pushMode = Enums\PushMode::create;
        }

        //discover whether we can actually push
        $this->_canPush = true;

        //if we are creating, and we can push, commit to push
        if ($this->_pushMode == Enums\PushMode::create && $this->_canPush) {
            $this->_commitPush = true;
        }
    }

    /**
     * Construct an instance of this type from an array.
     *
     * @param $array
     *
     * @return $this
     */
    final public static function constructFromArray($array) {
        $baseType = self::getClass();
        /** @var Model $baseObject */
        $baseObject = new $baseType();
        if (isset($array["id"])) {
            $baseObject->id = $array["id"];
        }
        foreach ($baseObject::internal_basicProperties() as $name => $data) {
            if (isset($array[$name])) {
                $baseObject->$name = $array[$name];
            }
        }
        foreach ($baseObject::internal_enumProperties() as $name => $data) {
            if (isset($array[$name])) {
                $baseObject->$name = $array[$name];
            }
        }
        foreach ($baseObject::internal_metaProperties() as $name) {
            if (isset($array[$name])) {
                $baseObject->$name = $array[$name];
            }
        }
        foreach ($baseObject::internal_complexProperties() as $name => $data) {
            $childModelNamespace = $data["childModelNamespace"];
            switch($data["type"]) {
                case Enums\PHPTypes::_object :
                    $baseObject->$name = null;
                    if (isset($array[$name])) {
                        if (gettype($array[$name]) == Enums\PHPTypes::_array) {
                            $baseObject->$name = clone($childModelNamespace::constructFromArray($array[$name]));
                        } elseif (gettype($array[$name]) == Enums\PHPTypes::_integer) {
                            $baseObject->$name = $array[$name];
                        } elseif (gettype($array[$name]) != Enums\PHPTypes::_nULL) {
                            //if invalid type, and not null, we can assume that we want to clear the property
                            $baseObject->$name = new \stdClass();
                        }
                    }
                    break;
                case Enums\PHPTypes::_array :
                    $baseObject->$name = array();
                    if (isset($array[$name])) {
                        if (gettype($array[$name]) == Enums\PHPTypes::_array) {
                            $children = array();
                            //during construction from array, a for loop doesn't work, as the key could not be a valid index
                            foreach ($array[$name] as $child) {
                                if (gettype($child) == Enums\PHPTypes::_array) {
                                    $children[] = clone($childModelNamespace::constructFromArray($child));
                                } elseif (gettype($child) == Enums\PHPTypes::_integer) {
                                    $children[] = $child;
                                }
                            }
                            $baseObject->$name = $children;
                        }
                    }
                    break;
            }
        }
        return $baseObject;
    }

    /**
     *
     * @return array
     */
    protected static function internal_basicProperties() {
        return array();
    }

    /**
     *
     * @return array
     */
    protected static function internal_enumProperties() {
        return array();
    }

    /**
     *
     * @return array
     */
    protected static function internal_metaProperties() {
        return array();
    }

    /**
     * Find an instance of this type by id and depth
     *
     * @param $id
     * @param int $depth
     *
     * @return $this
     */
    final public static function findById($id, $depth = 1) {
        $baseType = self::getClass();
        /** @var Model $baseObject */
        $baseObject = new $baseType();
        $baseObject->id = $id;
        if ($baseObject->pullThis($depth)) {
            return $baseObject;
        }
        return null;
    }

    /**
     * Pull this object from the database, based on it's id and depth
     *
     * @param int $depth
     *
     * @return bool
     * @throws DB_Exception
     */
    final public function pullThis(
        $depth=1
    ) {
        $args = func_get_args();
        //1 is current depth
        if(isset($args[1])) {
            $currentDepth = $args[1]+1;
        } else {
            $currentDepth = 1;
        }

        $this->id = intval($this->id);

        //reset internals
        $this->_canPush = false;
        $this->_commitPush = false;
        $this->_old = null;
        $this->_pushMode = Enums\PushMode::none;

        $db = Extorio::get()->getDbInstanceDefault();
        //build the query to fetch the data
        $table = Utilities\Models::getTableName($this->getClass());
        $sql = 'SELECT * FROM "'.$table.'" WHERE id = $1 LIMIT 1';
        $row = $db->query($sql,array($this->id))->fetchAssoc();
        if(!$row) {
            return false;
        }

        foreach($this->internal_basicProperties() as $name => $data) {
            if(isset($row[$name])) {
                switch($data["basicType"]) {
                    case Enums\PropertyBasicType::_checkbox :
                        $this->$name = $row[$name] == "t" ? true : false;
                        break;
                    case Enums\PropertyBasicType::_number :
                        $this->$name = intval($row[$name]);
                        break;
                    case Enums\PropertyBasicType::_decimal :
                        $this->$name = doubleval($row[$name]);
                        break;
                    default :
                        $this->$name = $row[$name];
                        break;
                }
            }
        }

        foreach($this->internal_enumProperties() as $name => $data) {
            if(isset($row[$name])) {
                $this->$name = $row[$name];
            }
        }

        foreach($this->internal_metaProperties() as $name) {
            if(isset($row[$name])) {
                $this->$name = json_decode($row[$name],true);
            }
        }

        foreach($this->internal_complexProperties() as $name => $data) {
            $childModelNamespace = $data["childModelNamespace"];
            switch($data["type"]) {
                case Enums\PHPTypes::_object :
                    $this->$name = null;
                    if(strlen($row[$name])) {
                        //based on depth, we are either leaving the reference, or resolving it
                        if($currentDepth < $depth) {
                            //resolve
                            $child = new $childModelNamespace();
                            $child->id = $row[$name];
                            if($child->pullThis($depth,$currentDepth,true)) {
                                $this->$name = $child;
                            }
                        } else {
                            //leave
                            $this->$name = intval($row[$name]);
                        }
                    }
                    break;
                case Enums\PHPTypes::_array :
                    $this->$name = array();
                    $childRefs = Utilities\PGDB::decodeArray($row[$name]);
                    if(gettype($childRefs) == Enums\PHPTypes::_array) {
                        $children = array();
                        //based on the depth, we are either leaving the reference, or resolving it
                        if($currentDepth < $depth) {
                            //resolve
                            for($i = 0; $i < count($childRefs); $i++) {
                                $child = new $childModelNamespace();
                                $child->id = $childRefs[$i];
                                if($child->pullThis($depth,$currentDepth,true)) {
                                    $children[] = $child;
                                }
                            }
                        } else {
                            //leave
                            for($i = 0; $i < count($childRefs); $i++) {
                                $children[] = intval($childRefs[$i]);
                            }
                        }
                        $this->$name = $children;
                    }
                    break;
            }
        }
        $this->beforeRetrieve();
        return true;
    }

    /**
     *
     */
    protected function beforeRetrieve() {

    }

    /**
     * Cast a basic property, based on it's type
     *
     * @param $type
     * @param $value
     *
     * @return mixed
     */
    final private function castBasicProperty($type,$value) {
        switch($type) {
            case Enums\PropertyBasicType::_checkbox :
                if(gettype($value) == Enums\PHPTypes::_boolean) {
                    return $value;
                } elseif(gettype($value) == Enums\PHPTypes::_string) {
                    switch($value) {
                        case "true" :
                            return true;
                        case "false" :
                            return false;
                        case "0" :
                            return false;
                        case "1" :
                            return true;
                        case true :
                        case false :
                            return $value;
                        default :
                            return false;
                    }
                }
                return false;
                break;
            case Enums\PropertyBasicType::_email :
                return strval($value);
                break;
            case Enums\PropertyBasicType::_password :
                return strval($value);
                break;
            case Enums\PropertyBasicType::_textfield :
                return strval($value);
                break;
            case Enums\PropertyBasicType::_textarea :
                return strval($value);
                break;
            case Enums\PropertyBasicType::_number :
                return intval($value);
                break;
            case Enums\PropertyBasicType::_decimal :
                return doubleval($value);
                break;
            case Enums\PropertyBasicType::_date :
                if(strlen($value)) {
                    return date("Y-m-d",strtotime($value));
                } else {
                    return null;
                }
                break;
            case Enums\PropertyBasicType::_time :
                if(strlen($value)) {
                    return date("H:i:s",strtotime($value));
                } else {
                    return null;
                }
                break;
            case Enums\PropertyBasicType::_datetime :
                if(strlen($value)) {
                    return date("Y-m-d H:i:s",strtotime($value));
                } else {
                    return null;
                }
                break;
        }
        return $value;
    }

    /**
     * Cast an enum value, making sure that it exists in the enum values otherwise it uses the default value
     *
     * @param $value
     * @param Enum $enumNamespace
     *
     * @return string
     */
    final private function castEnumProperty($enumNamespace,$value) {
        //if the value doesn't exist, use the default
        if(!$enumNamespace::valueExists($value)) {
            return $enumNamespace::defaultValue();
        }
        return $value;
    }

    /**
     * Cast a meta property, making sure that it is an associative array
     *
     * @param $value
     *
     * @return array|mixed
     */
    final private function castMetaProperty($value) {
        if(gettype($value) == Enums\PHPTypes::_array || gettype($value) == Enums\PHPTypes::_object) {
            return json_decode(json_encode($value),true);
        }
        if(gettype($value) == Enums\PHPTypes::_string) {
            $value = json_decode($value,true);
            if($value) {
                return $value;
            }
        }
        return array();
    }

    /**
     * This process looks for objects that are to be updated, and sets the commitPush internal value based on whether
     * the object has changed.
     */
    final private function prepareUpdates() {
        if ($this->_pushMode == Enums\PushMode::update && $this->_canPush) {
            //converting to stdClass removes internals, making a more accurate comparison
            if ($this->__toStdClass() != $this->_old->__toStdClass()) {
                $this->_commitPush = true;
            }
        }
        //recurse
        foreach ($this->internal_complexProperties() as $name => $data) {
            switch ($data["type"]) {
                case Enums\PHPTypes::_object :
                    $child = $this->$name;
                    if (gettype($child) == Enums\PHPTypes::_object) {
                        $child->prepareUpdates();
                    }
                    $this->$name = $child;
                    break;
                case Enums\PHPTypes::_array :
                    $children = $this->$name;
                    if (gettype($children) == Enums\PHPTypes::_array) {
                        for ($i = 0; $i < count($children); $i++) {
                            if (gettype($children[$i]) == Enums\PHPTypes::_object) {
                                $children[$i]->prepareUpdates();
                            }
                        }
                    }
                    $this->$name = $children;
                    break;
            }
        }
    }

    /**
     * Gets the object cast to a stdClass
     *
     * @return mixed
     */
    final public function __toStdClass() {
        return json_decode(json_encode($this, JSON_NUMERIC_CHECK));
    }

    /**
     * The before push event is used to trigger the beforeCreate/beforeUpdate events on objects
     *
     */
    final private function beforePush() {
        if ($this->_canPush) {
            switch ($this->_pushMode) {
                case Enums\PushMode::create :
                    $this->beforeCreate();
                    break;
                case Enums\PushMode::update :
                    $this->beforeUpdate();
                    break;
            }
        }
        //recurse
        foreach ($this->internal_complexProperties() as $name => $data) {
            switch ($data["type"]) {
                case Enums\PHPTypes::_object :
                    $child = $this->$name;
                    if (gettype($child) == Enums\PHPTypes::_object) {
                        $child->beforePush();
                    }
                    $this->$name = $child;
                    break;
                case Enums\PHPTypes::_array :
                    $children = $this->$name;
                    if (gettype($children) == Enums\PHPTypes::_array) {
                        for ($i = 0; $i < count($children); $i++) {
                            if (gettype($children[$i]) == Enums\PHPTypes::_object) {
                                $children[$i]->beforePush();
                            }
                        }
                    }
                    $this->$name = $children;
                    break;
            }
        }
    }

    /**
     *
     */
    protected function beforeCreate() {

    }

    /**
     *
     */
    protected function beforeUpdate() {

    }

    /**
     * The after push event is used to trigger the afterCreate/afterUpdate events on objects
     *
     */
    final private function afterPush() {
        if ($this->_commitPush) {
            switch ($this->_pushMode) {
                case Enums\PushMode::create :
                    $this->afterCreate();
                    break;
                case Enums\PushMode::update :
                    $this->afterUpdate();
                    break;
            }
        }
        //recurse
        foreach ($this->internal_complexProperties() as $name => $data) {
            switch ($data["type"]) {
                case Enums\PHPTypes::_object :
                    $child = $this->$name;
                    if (gettype($child) == Enums\PHPTypes::_object) {
                        $child->afterPush();
                    }
                    $this->$name = $child;
                    break;
                case Enums\PHPTypes::_array :
                    $children = $this->$name;
                    if (gettype($children) == Enums\PHPTypes::_array) {
                        for ($i = 0; $i < count($children); $i++) {
                            if (gettype($children[$i]) == Enums\PHPTypes::_object) {
                                $children[$i]->afterPush();
                            }
                        }
                    }
                    $this->$name = $children;
                    break;
            }
        }
    }

    /**
     *
     */
    protected function afterCreate() {

    }

    /**
     *
     */
    protected function afterUpdate() {

    }

    /**
     * Delete this object. Note that it is not possible to do this recursively. Only the target object will be deleted,
     * unless the event processing deleted child objects for you.
     *
     * @throws DB_Exception
     * @throws \Core\Classes\Exceptions\Extension_Exception
     * @throws \Core\Classes\Exceptions\General_Exception
     */
    final public function deleteThis() {
        $this->id = intval($this->id);
        if ($this->id) {
            $db = Extorio::get()->getDbInstanceDefault();

            //pull first
            $this->pullThis(INF);

            $this->beforeDelete();

            //delete the actual row
            $tableName = Utilities\Models::getTableName($this->getClass());
            $sql = 'DELETE FROM "' . $tableName . '" WHERE id = $1';
            $db->query($sql, array($this->id));

            //find all models that have this model as a direct child model
            $sql = '
                SELECT M.namespace, P.name, P."complexType" FROM core_classes_models_model M

                -- join on properties
                LEFT JOIN core_classes_models_property P ON
                P.id = ANY(M.properties)

                WHERE P."propertyType" = $1 AND P."childModelNamespace" = $2
                ';
            $rs = $db->query($sql, array(Enums\PropertyType::_complex, $this->getClass()));
            while ($row = $rs->fetchRow()) {
                //remove any references
                $otherTable = Utilities\Models::getTableName($row[0]);
                switch ($row[2]) {
                    case Enums\PHPTypes::_object :
                        $sql = 'UPDATE "' . $otherTable . '" SET "' . $row[1] . '" = NULL WHERE "' . $row[1] . '" = $1';
                        $db->query($sql, array($this->id));
                        break;
                    case Enums\PHPTypes::_array :
                        $sql = 'UPDATE "' . $otherTable . '" SET "' . $row[1] . '" = array_remove("' . $row[1] . '", $1)';
                        $db->query($sql, array($this->id));
                        break;
                }

            }

            //create the modcache
            Utilities\Models::createModCache($this->getClass(), $this->id, "delete", json_encode($this));

            //if autofire on webhooks is enabled, and they are enabled here
            $config = Extorio::get()->getConfig();
            $core = Extorio::get()->getExtension("Core");
            if ($config["models"]["webhooks"]["autofire"]) {
                Extorio::get()->runTask_Background($core->_constructComponent('\Core\Components\Tasks\Data'), false, "webhook", array(Utilities\Namespaces::getClassName($this->getClass()), $this->id, "delete"));
            }

            $this->afterDelete();
        }
    }

    /**
     *
     */
    protected function beforeDelete() {

    }

    /**
     *
     */
    protected function afterDelete() {

    }

    /**
     * This gets a string value of the models' first basic property. Otherwise it get's the id.
     *
     * @return string
     */
    final public function __toString() {
        foreach ($this->internal_basicProperties() as $name => $data) {
            return strval($this->$name);
        }
        return strval($this->id);
    }

    /**
     * Gets the object cast to an array
     *
     * @return mixed
     */
    final public function __toArray() {
        return json_decode(json_encode($this, JSON_NUMERIC_CHECK), true);
    }

    /**
     * Gets the object as a json string
     *
     * @return string
     */
    final public function __toJson() {
        return json_encode($this, JSON_NUMERIC_CHECK);
    }
}