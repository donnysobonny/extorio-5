<?php
namespace Core\Classes\Helpers;
/**
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class UserTab
 */
class UserTab {
    public $name;
    public $icon;
    public $label;

    /**
     * UserTab constructor.
     *
     * @param $label
     * @param $icon
     */
    public function __construct($name,$label, $icon) {
        $this->name = $name;
        $this->label = $label;
        $this->icon =$icon;
    }

    /**
     * Lazy construct
     *
     * @param $label
     * @param $icon
     *
     * @return UserTab
     */
    public static function n($name,$label, $icon) {
        return new UserTab($name,$label,$icon);
    }
}