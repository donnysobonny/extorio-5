<?php
namespace Core\Classes\Helpers;
class BreadCrumb {
    public $active = false;
    public $label;
    public $url;
    public $icon;
    public $disabled = false;
    public $title = "";

    /**
     * Lazily create a breadcrumb
     *
     * @param bool $active
     * @param $label
     * @param $url
     *
     * @return BreadCrumb
     */
    public static function n($active=false,$label,$url="",$icon="",$disabled=false,$title="") {
        $bc = new BreadCrumb();
        $bc->active = $active;
        $bc->label = $label;
        $bc->icon = $icon;
        $bc->disabled = $disabled;
        $bc->title = $title;
        if(strlen($url)) {
            $url = Url::n($url);
            $bc->url = $url->getUri();
        }

        return $bc;
    }
}