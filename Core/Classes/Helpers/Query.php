<?php
namespace Core\Classes\Helpers;
use Core\Classes\Commons\Extorio;
use Core\Classes\Enums\PHPTypes;
use Core\Classes\Enums\PropertyType;
use Core\Classes\Utilities\Models;
use Core\Classes\Utilities\Strings;

/**
 * This is used to help generate the array that makes up the query part of model find requests
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Query
 */
class Query {

    //groups
    const _and = '$and';
    const _or = '$or';

    //operators
    const _eq = '$eq';
    const _ne = '$ne';
    const _gt = '$gt';
    const _gte = '$gte';
    const _lt = '$lt';
    const _lte = '$lte';
    const _in = '$in';
    const _nin = '$nin';
    const _lk = '$lk';

    //ordering
    const _asc = '$asc';
    const _desc = '$desc';

    private $where = array();

    private $order = array();

    private $group = array();

    private $limit = 250;
    private $skip = 0;

    private $targets = array();
    private $targetsToAliasMap = array();

    private $baseModelNamespace;

    private $baseTable;

    /**
     * Set the base model namespace. Must be done in order to generate the final sql
     *
     * @param $baseModelNamespace
     */
    public function setBaseModelNamespace($baseModelNamespace) {
        $this->baseModelNamespace = Strings::startsWith($baseModelNamespace,"\\");;
        $this->baseTable = Models::getTableName($baseModelNamespace);
    }

    /**
     * lazily construct an instance
     *
     * @return Query
     */
    public static function n() {
        $q = new Query();
        return $q;
    }

    /**
     * Construct an instance from an array
     *
     * @param $array
     *
     * @return Query
     */
    public static function constructFromArray($array) {
        $q = self::n();
        $params = array(
            "where",
            "order",
            "limit",
            "skip"
        );

        foreach($params as $param) {
            if(isset($array[$param])) {
                $q->$param = $array[$param];
            } elseif(isset($array['$'.$param])) {
                $q->$param = $array['$'.$param];
            }
        }

        $q->extractTargets($q->where);
        $q->extractTargets($q->order);

        return $q;
    }

    /**
     * Smartly construct an instance, based on the mixed type
     *
     * @param $mixed
     *
     * @return Query
     */
    public static function constructFromMixed($mixed) {
        if(is_object($mixed)) {
            if(get_class($mixed) == 'Core\Classes\Helpers\Query') {
                return $mixed;
            } else {
                return self::constructFromArray(json_decode(json_encode($mixed),true));
            }
        }
        if(is_array($mixed)) {
            return self::constructFromArray($mixed);
        }
        if(is_string($mixed)) {
            $array = json_decode($mixed,true);
            if($array) {
                return self::constructFromArray($array);
            }
        }
        return self::n();
    }

    /**
     * Create the where array, either from an array or a json string
     *
     * @param $mixed
     *
     * @return $this
     */
    public function where($mixed) {
        if(gettype($mixed) == PHPTypes::_array) {
            $this->where = $mixed;
        } elseif(gettype($mixed) == PHPTypes::_string) {
            $this->where = json_decode($mixed,true);
        }
        $this->extractTargets($this->where);
        return $this;
    }

    /**
     * Limit the results by a number
     *
     * @param $num
     *
     * @return $this
     */
    public function limit($num) {
        $this->limit = $num;
        return $this;
    }

    /**
     * Skip the results by a number
     *
     * @param $num
     *
     * @return $this
     */
    public function skip($num) {
        $this->skip = $num;
        return $this;
    }

    /**
     * Order the results. You can pass an array here, to specify the order direction or add a number of order fields. If
     * you pass a string, you will order by one field, asc
     *
     * @param $array
     *
     * @return $this
     */
    public function order($mixed) {
        if(gettype($mixed) == PHPTypes::_array) {
            $this->order = $mixed;
        } elseif(gettype($mixed) == PHPTypes::_string) {
            $this->order = array($mixed);
        }
        $this->extractTargets($this->order);
        return $this;
    }

    /**
     * Get the SELECT part of the sql
     *
     * @return string
     */
    public function getSelectSql() {
        return "SELECT BASE.id FROM ".$this->baseTable." BASE

";
    }

    /**
     * Get the GROUP BY part of the sql
     *
     * @return string
     */
    public function getGroupSql() {
        if(count($this->order)) {
            foreach($this->order as $key => $value) {
                if(is_numeric($key)) {
                    //$value in this case could be a string (to default to ASC) or an array (to specify the direction)
                    switch(gettype($value)) {
                        case PHPTypes::_array:
                            foreach($value as $field => $direction) {
                                $objectPath = Query::stripTargetObjectPath($field);
                                $propertyLeaf = Query::stripTargetPropertyLeaf($field);
                                if(strlen($objectPath)) {
                                    $path = 'PATH'.$this->targetsToAliasMap[$objectPath].'."'.$propertyLeaf.'"';
                                    $this->group .= $path;
                                }
                            }
                            break;
                        case PHPTypes::_string:
                            $objectPath = Query::stripTargetObjectPath($value);
                            $propertyLeaf = Query::stripTargetPropertyLeaf($value);
                            if(strlen($objectPath)) {
                                $path = 'PATH'.$this->targetsToAliasMap[$objectPath].'."'.$propertyLeaf.'"';
                                $this->group[] = $path;
                            }
                            break;
                    }
                } else {
                    $objectPath = Query::stripTargetObjectPath($key);
                    $propertyLeaf = Query::stripTargetPropertyLeaf($key);
                    if(strlen($objectPath)) {
                        $path = 'PATH'.$this->targetsToAliasMap[$objectPath].'."'.$propertyLeaf.'"';
                        $this->group[] = $path;
                    }
                }
            }
        }
        $group = "GROUP BY BASE.id";
        foreach($this->group as $g) {
            $group .= ", ".$g;
        }
        return $group."

";
    }

    /**
     * Get the ORDER BY part of the sql
     *
     * @return string
     */
    public function getOrderSql() {
        $order = "";
        if(count($this->order)) {
            $order = "ORDER BY ";
            foreach($this->order as $key => $value) {
                if(is_numeric($key)) {
                    //$value in this case could be a string (to default to ASC) or an array (to specify the direction)
                    switch(gettype($value)) {
                        case PHPTypes::_array:
                            foreach($value as $field => $direction) {
                                $objectPath = Query::stripTargetObjectPath($field);
                                $propertyLeaf = Query::stripTargetPropertyLeaf($field);
                                if(strlen($objectPath)) {
                                    $path = 'PATH'.$this->targetsToAliasMap[$objectPath].'."'.$propertyLeaf.'"';
                                    $order .= $path." ";
                                } else {
                                    $order .= 'BASE."'.$propertyLeaf.'" ';
                                }
                                if(in_array($direction, array(
                                    Query::_asc,
                                    Query::_desc
                                ))) {
                                    $order .= $direction == Query::_asc ? "ASC, " : "DESC, ";
                                } elseif(in_array(strtoupper($direction), array(
                                    "ASC",
                                    "DESC"
                                ))) {
                                    $order .= strtoupper($direction).", ";
                                } else {
                                    $order .= "ASC, ";
                                }
                            }
                            break;
                        case PHPTypes::_string:
                            $objectPath = Query::stripTargetObjectPath($value);
                            $propertyLeaf = Query::stripTargetPropertyLeaf($value);
                            if(strlen($objectPath)) {
                                $path = 'PATH'.$this->targetsToAliasMap[$objectPath].'."'.$propertyLeaf.'"';
                                $order .= $path." ASC, ";
                            } else {
                                $order .= 'BASE."'.$propertyLeaf.'" ASC, ';
                            }
                            break;
                    }
                } else {
                    $objectPath = Query::stripTargetObjectPath($key);
                    $propertyLeaf = Query::stripTargetPropertyLeaf($key);
                    if(strlen($objectPath)) {
                        $path = 'PATH'.$this->targetsToAliasMap[$objectPath].'."'.$propertyLeaf.'"';
                        $order .= $path." ";
                    } else {
                        $order .= 'BASE."'.$propertyLeaf.'" ';
                    }
                    if(in_array($value, array(
                        Query::_asc,
                        Query::_desc
                    ))) {
                        $order .= $value == Query::_asc ? "ASC, " : "DESC, ";
                    } elseif(in_array(strtoupper($value), array(
                        "ASC",
                        "DESC"
                    ))) {
                        $order .= strtoupper($value).", ";
                    } else {
                        $order .= "ASC, ";
                    }
                }
            }
        }
        return substr($order,0,strlen($order)-2)."

";
    }

    /**
     * Get the LIMIT part of the sql
     *
     * @return string
     */
    public function getLimitSql() {
        $limit = "";
        if($this->limit > 0) {
            $limit .= "LIMIT ".$this->limit." ";
        }
        if($this->skip > 0) {
            $limit .= "OFFSET ".$this->skip." ";
        }
        return $limit;
    }

    /**
     * Get the joins that join the targets together
     *
     * @return string
     */
    public function getJoinsSql() {
        $linkIndex = 1;
        $aliasIndex = 1;
        $joins = "";
        foreach($this->targets as $targetPath) {
            $joins .= $this->getJoinSql($this->baseModelNamespace,explode(".",$targetPath),$targetPath,null,$linkIndex,$aliasIndex);
        }
        return $joins;
    }

    /**
     * Get the sql for an individual join
     *
     * @param $currentModelNamespace
     * @param $targetParts
     * @param $targetPath
     * @param $lastLink
     * @param $linkIndex
     * @param $aliasIndex
     *
     * @return string
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    private function getJoinSql($currentModelNamespace,$targetParts,$targetPath,$lastLink,&$linkIndex,&$aliasIndex) {
        $currentModelNamespace = Strings::startsWith($currentModelNamespace,"\\");
        $joins = "";
        $onBase = $lastLink == null;
        if(count($targetParts)) {
            $part = $targetParts[0];
            //find a valid model>property link
            $sql = '
            SELECT P.*

            FROM core_classes_models_model M

            -- join on properties
            RIGHT JOIN core_classes_models_property P ON
            P.id = ANY(M.properties)

            WHERE M.namespace = $1 AND P.name = $2 AND P."propertyType" = $3
        ';
            $db = Extorio::get()->getDbInstanceDefault();
            $row = $db->query($sql,array($currentModelNamespace,$part,PropertyType::_complex))->fetchAssoc();
            if($row) {
                $childNamespace = $row["childModelNamespace"];
                $childTable = Models::getTableName($childNamespace);
                if(count($targetParts) == 1) {
                    //we're making the path link
                    $alias = "PATH".$aliasIndex;
                    $this->targetsToAliasMap[$targetPath] = $aliasIndex;
                    $aliasIndex ++;
                } else {
                    //we're making a link
                    $alias = "L".$linkIndex;
                    $lastLink = $linkIndex;
                    $linkIndex++;
                }
                //if on base, join to base. Otherwise join to the last link
                if($onBase) {
                    $joins .= 'LEFT JOIN "'.$childTable.'" '.$alias.' ON
'.$alias.'.id = ';
                    switch($row["complexType"]) {
                        case PHPTypes::_object :
                            $joins .= 'BASE."'.$row["name"].'"

';
                            break;
                        case PHPTypes::_array :
                            $joins .= 'ANY(BASE."'.$row["name"].'")

';
                            break;
                    }
                } else {
                    $joins .= 'LEFT JOIN "'.$childTable.'" '.$alias.' ON
'.$alias.'.id = ';
                    switch($row["complexType"]) {
                        case PHPTypes::_object :
                            $joins .= 'L'.$lastLink.'."'.$row["name"].'"

';
                            break;
                        case PHPTypes::_array :
                            $joins .= 'ANY(L'.$lastLink.'."'.$row["name"].'")

';
                            break;
                    }
                }
                //remove the part from targets before recursion
                array_shift($targetParts);
                $joins .= $this->getJoinSql($childNamespace,$targetParts,$targetPath,$lastLink,$linkIndex,$aliasIndex);
            }
        }
        return $joins;
    }

    /**
     * Get the WHERE sql
     *
     * @param $params
     *
     * @return string
     */
    public function getWhereSql(&$params) {
        if(count($this->where)) {
            $where = "";
            $paramIndex = 1;
            return "WHERE ".$this->constructWhere($this->where,'$and',$where,$paramIndex,$params);
        } else {
            return "";
        }
    }

    /**
     * Get the WHERE parts
     *
     * @param $array
     * @param string $groupType
     * @param string $where
     * @param int $paramIndex
     * @param array $params
     *
     * @return string
     */
    private function constructWhere($array,$groupType='$and',&$where="",&$paramIndex=1,&$params=array()) {
        foreach($array as $key => $value) {
            if(gettype($key) == PHPTypes::_integer) {
                //value must be an array
                if(gettype($value) == PHPTypes::_array) {
                    foreach($value as $vkey => $vvalue) {
                        if($vkey == Query::_and || $vkey == Query::_or) {
                            $where .= "(";
                            $this->constructWhere($vvalue,$vkey,$where,$paramIndex,$params);
                            $where .= ") ";
                            $where .= $groupType == Query::_or ? "OR " : "AND ";
                        } else {
                            $type = $groupType == Query::_or ? "OR" : "AND";
                            $objectPath = Query::stripTargetObjectPath($vkey);
                            $propertyLeaf = Query::stripTargetPropertyLeaf($vkey);
                            if(strlen($objectPath)) {
                                $field = 'PATH'.$this->targetsToAliasMap[$objectPath].'."'.$propertyLeaf.'"';
                            } else {
                                $field = 'BASE."'.$propertyLeaf.'"';
                            }
                            if(gettype($vvalue) == PHPTypes::_array) {
                                if(array_key_exists(Query::_eq,$vvalue)) {
                                    if(gettype($vvalue[Query::_eq]) == PHPTypes::_nULL) {
                                        $where .= $field." IS NULL ";
                                    } else {
                                        $where .= $field." = $".$paramIndex." ";
                                        $params[] = $vvalue[Query::_eq];
                                        $paramIndex++;
                                    }
                                }
                                elseif(array_key_exists(Query::_ne,$vvalue)) {
                                    if(gettype($vvalue[Query::_ne]) == PHPTypes::_nULL) {
                                        $where .= $field." IS NOT NULL ";
                                    } else {
                                        $where .= $field." != $".$paramIndex." ";
                                        $params[] = $vvalue[Query::_ne];
                                        $paramIndex++;
                                    }
                                }
                                elseif(array_key_exists(Query::_lt,$vvalue)) {
                                    $where .= $field." < $".$paramIndex." ";
                                    $params[] = $vvalue[Query::_lt];
                                    $paramIndex++;
                                }
                                elseif(array_key_exists(Query::_lte,$vvalue)) {
                                    $where .= $field." <= $".$paramIndex." ";
                                    $params[] = $vvalue[Query::_lte];
                                    $paramIndex++;
                                }
                                elseif(array_key_exists(Query::_gt,$vvalue)) {
                                    $where .= $field." > $".$paramIndex." ";
                                    $params[] = $vvalue[Query::_gt];
                                    $paramIndex++;
                                }
                                elseif(array_key_exists(Query::_gte,$vvalue)) {
                                    $where .= $field." >= $".$paramIndex." ";
                                    $params[] = $vvalue[Query::_gte];
                                    $paramIndex++;
                                }
                                elseif(array_key_exists(Query::_lk,$vvalue)) {
                                    $where .= "LOWER(CAST(".$field." AS text)) LIKE LOWER($".$paramIndex.") ";
                                    $params[] = $vvalue[Query::_lk];
                                    $paramIndex++;
                                }
                                elseif(array_key_exists(Query::_in,$vvalue)) {
                                    $where .= $field." IN (";
                                    if(is_array($vvalue[Query::_in])) {
                                        $ninl = "";
                                        foreach($vvalue[Query::_in] as $nin) {
                                            $ninl .= "$".$paramIndex.",";
                                            $params[] = $nin;
                                            $paramIndex++;
                                        }
                                        $where .= substr($ninl,0,strlen($ninl)-1);
                                    } else {
                                        $where .= "$".$paramIndex;
                                        $params[] = $vvalue[Query::_in];
                                        $paramIndex++;
                                    }
                                    $where .= ") ";
                                }
                                elseif(array_key_exists(Query::_nin,$vvalue)) {
                                    $where .= $field." NOT IN (";
                                    if(is_array($vvalue[Query::_nin])) {
                                        $ninl = "";
                                        foreach($vvalue[Query::_nin] as $nin) {
                                            $ninl .= "$".$paramIndex.",";
                                            $params[] = $nin;
                                            $paramIndex++;
                                        }
                                        $where .= substr($ninl,0,strlen($ninl)-1);
                                    } else {
                                        $where .= "$".$paramIndex;
                                        $params[] = $vvalue[Query::_nin];
                                        $paramIndex++;
                                    }
                                    $where .= ") ";
                                }
                            } else {
                                if(gettype($vvalue) == PHPTypes::_nULL) {
                                    $where .= $field." IS NULL ";
                                } else {
                                    $where .= $field." = $".$paramIndex." ";
                                    $params[] = $vvalue;
                                    $paramIndex++;
                                }
                            }
                            $where .= $type." ";
                        }
                    }
                }
            } else {
                if($key == Query::_and || $key == Query::_or) {
                    $where .= "(";
                    $this->constructWhere($array[$key],$key,$where,$paramIndex,$params);
                    $where .= ") ";
                    $where .= $groupType == Query::_or ? "OR " : "AND ";
                } else {
                    $type = $groupType == Query::_or ? "OR" : "AND";
                    $objectPath = Query::stripTargetObjectPath($key);
                    $propertyLeaf = Query::stripTargetPropertyLeaf($key);
                    if(strlen($objectPath)) {
                        $field = 'PATH'.$this->targetsToAliasMap[$objectPath].'."'.$propertyLeaf.'"';
                    } else {
                        $field = 'BASE."'.$propertyLeaf.'"';
                    }
                    if(gettype($value) == PHPTypes::_array) {
                        if(array_key_exists(Query::_eq,$value)) {
                            if(gettype($value[Query::_eq]) == PHPTypes::_nULL) {
                                $where .= $field." IS NULL ";
                            } else {
                                $where .= $field." = $".$paramIndex." ";
                                $params[] = $value[Query::_eq];
                                $paramIndex++;
                            }
                        }
                        elseif(array_key_exists(Query::_ne,$value)) {
                            if(gettype($value[Query::_ne]) == PHPTypes::_nULL) {
                                $where .= $field." IS NOT NULL ";
                            } else {
                                $where .= $field." != $".$paramIndex." ";
                                $params[] = $value[Query::_ne];
                                $paramIndex++;
                            }
                        }
                        elseif(array_key_exists(Query::_lt,$value)) {
                            $where .= $field." < $".$paramIndex." ";
                            $params[] = $value[Query::_lt];
                            $paramIndex++;
                        }
                        elseif(array_key_exists(Query::_lte,$value)) {
                            $where .= $field." <= $".$paramIndex." ";
                            $params[] = $value[Query::_lte];
                            $paramIndex++;
                        }
                        elseif(array_key_exists(Query::_gt,$value)) {
                            $where .= $field." > $".$paramIndex." ";
                            $params[] = $value[Query::_gt];
                            $paramIndex++;
                        }
                        elseif(array_key_exists(Query::_gte,$value)) {
                            $where .= $field." >= $".$paramIndex." ";
                            $params[] = $value[Query::_gte];
                            $paramIndex++;
                        }
                        elseif(array_key_exists(Query::_lk,$value)) {
                            $where .= "LOWER(CAST(".$field." AS text)) LIKE LOWER($".$paramIndex.") ";
                            $params[] = $value[Query::_lk];
                            $paramIndex++;
                        }
                        elseif(array_key_exists(Query::_in,$value)) {
                            $where .= $field." IN (";
                            if(is_array($value[Query::_in])) {
                                $ninl = "";
                                foreach($value[Query::_in] as $nin) {
                                    $ninl .= "$".$paramIndex.",";
                                    $params[] = $nin;
                                    $paramIndex++;
                                }
                                $where .= substr($ninl,0,strlen($ninl)-1);
                            } else {
                                $where .= "$".$paramIndex;
                                $params[] = $value[Query::_in];
                                $paramIndex++;
                            }
                            $where .= ") ";
                        }
                        elseif(array_key_exists(Query::_nin,$value)) {
                            $where .= $field." NOT IN (";
                            if(is_array($value[Query::_nin])) {
                                $ninl = "";
                                foreach($value[Query::_nin] as $nin) {
                                    $ninl .= "$".$paramIndex.",";
                                    $params[] = $nin;
                                    $paramIndex++;
                                }
                                $where .= substr($ninl,0,strlen($ninl)-1);
                            } else {
                                $where .= "$".$paramIndex;
                                $params[] = $value[Query::_nin];
                                $paramIndex++;
                            }
                            $where .= ") ";
                        }
                    } else {
                        if(gettype($value) == PHPTypes::_nULL) {
                            $where .= $field." IS NULL ";
                        } else {
                            $where .= $field." = $".$paramIndex." ";
                            $params[] = $value;
                            $paramIndex++;
                        }
                    }
                    $where .= $type." ";
                }
            }
        }
        //strip off the last OR/AND
        if(substr($where,strlen($where)-4,4) == " OR ") {
            $where = substr($where,0,strlen($where)-4);
        } elseif(substr($where,strlen($where)-5,5) == " AND ") {
            $where = substr($where,0,strlen($where)-5);
        }
        return $where."

";
    }

    /**
     * Strip out a target's object path (stripping off the leaf)
     *
     * @param $target
     *
     * @return string
     */
    private static function stripTargetObjectPath($target) {
        $parts = explode(".",$target);
        $target = "";
        for($i = 0; $i < count($parts)-1; $i++) {
            $target .= $parts[$i].".";
        }
        return substr($target,0,strlen($target)-1);
    }

    /**
     * Strip out the leaf (leaving off the object path)
     *
     * @param $target
     *
     * @return mixed
     */
    private static function stripTargetPropertyLeaf($target) {
        $parts = explode(".",$target);
        return $parts[count($parts)-1];
    }

    /**
     * Extract targets from the where/order array. This is used to generate the aliases
     *
     * @param $array
     */
    private function extractTargets($array) {
        foreach($array as $key => $value) {
            if(gettype($key) == PHPTypes::_integer) {
                //value must be an array
                if(gettype($value) == PHPTypes::_array) {
                    foreach($value as $vkey => $vvalue) {
                        if($vkey == Query::_and || $vkey == Query::_or) {
                            $this->extractTargets($vvalue);
                        } else {
                            $target = Query::stripTargetObjectPath($vkey);
                            if(strlen($target) && !in_array($target,$this->targets)) {
                                $this->targets[] = $target;
                            }
                        }
                    }
                }
            } else {
                if($key == Query::_and || $key == Query::_or) {
                    $this->extractTargets($value);
                } else {
                    $target = Query::stripTargetObjectPath($key);
                    if(strlen($target) && !in_array($target,$this->targets)) {
                        $this->targets[] = $target;
                    }
                }
            }
        }
    }
}