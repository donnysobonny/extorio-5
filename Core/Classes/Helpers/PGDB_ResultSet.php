<?php
namespace Core\Classes\Helpers;
/**
 * This is an ienumerator, used to enumerate through results of a query
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class PGDB_ResultSet
 */
class PGDB_ResultSet {

    /**
     * The result, returned by a query
     *
     * @var
     */
    private $result;

    /**
     * @param $result
     */
    function __construct($result) {
        $this->result = $result;
    }

    /**
     * Lazily construct a result set
     *
     * @param $result
     *
     * @return PGDB_ResultSet
     */
    public static function n($result) {
        return new PGDB_ResultSet($result);
    }

    /**
     * Fetch all results as an array
     *
     * @return array
     */
    public function fetchAll() {
        return pg_fetch_all($this->result) ? : array();
    }

    /**
     * Fetch the next row in the set as an indexed array
     *
     * @return array
     */
    public function fetchRow() {
        return pg_fetch_row($this->result);
    }

    /**
     * Fetch the next row in the set as an associative array
     *
     * @return array
     */
    public function fetchAssoc() {
        return pg_fetch_assoc($this->result);
    }

    /**
     * Fetch the next row in the set as an object
     *
     * @return object
     */
    public function fetchObject() {
        return pg_fetch_object($this->result);
    }

    /**
     * Get the number of rows in a set
     *
     * @return int
     */
    public function numRows() {
        return pg_num_rows($this->result);
    }

    /**
     * Get the number of affected rows in a set
     *
     * @return int
     */
    public function affectedRows() {
        return pg_affected_rows($this->result);
    }

    /**
     * Get an error related to the result set
     *
     * @return string
     */
    public function error() {
        return pg_result_error($this->result);
    }

    /**
     * Get the status of the result set
     *
     * @return mixed
     */
    public function status() {
        return pg_result_status($this->result);
    }
}