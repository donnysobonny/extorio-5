<?php
namespace Core\Classes\Helpers;
use Core\Classes\Commons\Model;

class Model_ResultSet {
    private $modelNamespace = "";
    /**
     * @var PGDB_ResultSet
     */
    private $pgdbResultSet;

    /**
     * Model_ResultSet constructor.
     *
     * @param string $modelNamespace
     * @param PGDB_ResultSet $pgdbResultSet
     */
    public function __construct($modelNamespace,$pgdbResultSet) {
        $this->modelNamespace = $modelNamespace;
        $this->pgdbResultSet = $pgdbResultSet;
    }

    /**
     * Fetch the next result. Returns NULL if there are no more results.
     *
     * @param int $depth the depth to fetch the result
     *
     * @return Model
     */
    public function fetchNext($depth=1) {
        $row = $this->pgdbResultSet->fetchRow();
        if($row) {
            /** @var Model $type */
            $type = $this->modelNamespace;
            return $type::findById($row[0],$depth);
        } else {
            return null;
        }
    }

    /**
     * Get the number of rows in the set
     *
     * @return int
     */
    public function numResults() {
        return $this->pgdbResultSet->numRows();
    }

    /**
     * Get an error related to the result set
     *
     * @return string
     */
    public function error() {
        return $this->pgdbResultSet->error();
    }

    /**
     * Get the status of the result set
     *
     * @return mixed
     */
    public function status() {
        return $this->pgdbResultSet->status();
    }
}