<?php
namespace Core\Classes\Helpers;
use Core\Classes\Commons\Extorio;
use Core\Classes\Enums\LayoutCellContainerType;
use Core\Classes\Models\Block;
use Core\Classes\Utilities\Blocks;

/**
 * A cell resembles a block that exists in a row. Cells have muliple purposes, as they can be used to contain rows, and
 * blocks.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class LayoutSystem_Cell
 */
class LayoutSystem_Cell {
    /**
     * The role of the cell
     *
     * @var string
     */
    public $role;
    /**
     * The classes that will be applied to the cell
     *
     * @var string
     */
    public $classes = "";
    /**
     * The styling to apply to the row
     *
     * @var string
     */
    public $styling = "";

    /**
     * The x position
     *
     * @var int
     */
    public $x = 0;
    /**
     * The cell width
     *
     * @var int
     */
    public $width = 1;
    /**
     * How the cell should be aligned. "inherit" causes the cell to inherit the row's align
     *
     * @var string
     */
    public $align = "inherit";
    /**
     * Hide the cell on xs screens
     *
     * @var bool
     */
    public $hidexs = false;
    /**
     * Hide the cell on sm screens
     *
     * @var bool
     */
    public $hidesm = false;
    /**
     * Hide the cell on md screens
     *
     * @var bool
     */
    public $hidemd = false;
    /**
     * Hide the cell on large screens
     *
     * @var bool
     */
    public $hidelg = false;
    /**
     * Hide the cell on all screens
     *
     * @var bool
     */
    public $hideall = false;
    /**
     * The id of the block, if the cell's role is "block"
     *
     * @var int
     */
    public $blockid = 0;
    /**
     * The cell's rows, if the cell's role is "cell"
     *
     * @var LayoutSystem_Row[]
     */
    public $rows = array();

    /**
     * Quickly construct a new cell
     *
     * @param string $role
     * @param array $rows
     * @param int $x
     * @param int $width
     * @param string $align
     * @param bool $hidexs
     * @param bool $hidesm
     * @param bool $hidemd
     * @param bool $hidelg
     * @param bool $hideall
     * @param int $blockid
     * @param string $classes
     * @param string $styling
     *
     * @return LayoutSystem_Cell
     */
    public static function quickConstruct(
        $role = "cell",
        $rows = array(),
        $x = 0,
        $width = 1,
        $align = "inherit",
        $hidexs = false,
        $hidesm = false,
        $hidemd = false,
        $hidelg = false,
        $hideall = false,
        $blockid = 0,
        $classes = "",
        $styling = ""
    ) {
        $c = LayoutSystem_Cell::n();
        $c->role = $role;
        $c->rows = $rows;
        $c->x = $x;
        $c->width = $width;
        $c->align = $align;
        $c->hidexs = $hidexs;
        $c->hidesm = $hidesm;
        $c->hidemd = $hidemd;
        $c->hidelg = $hidelg;
        $c->hideall = $hideall;
        $c->blockid = $blockid;
        $c->classes = $classes;
        $c->styling = $styling;

        return $c;
    }

    /**
     * Lazily construct a new instance
     *
     * @return LayoutSystem_Cell
     */
    public static function n() {
        return new LayoutSystem_Cell();
    }

    /**
     * Construct a cell from an array
     *
     * @param $array
     *
     * @return LayoutSystem_Cell
     */
    public static function constructFromArray($array) {
        $c = LayoutSystem_Cell::n();
        foreach($c as $key => $value) {
            if(isset($array[$key])) {
                $c->$key = $array[$key];
            }
        }

        //construct rows
        for($i = 0; $i < count($c->rows); $i++) {
            $c->rows[$i] = LayoutSystem_Row::constructFromArray($c->rows[$i]);
        }

        return $c;
    }

    /**
     * Get the cell to an associative array
     *
     * @return mixed
     */
    public function __toArray() {
        return json_decode(json_encode($this,JSON_NUMERIC_CHECK),true);
    }

    /**
     * Get the cell to html
     *
     * @param $content
     * @param $contentid
     * @param bool $editMode
     *
     * @return string
     * @throws \Core\Classes\Exceptions\Extension_Exception
     */
    public function __toHtml($content, $contentid, $editMode = false) {
        $editable = $editMode ? "true" : "false";
        $loggedInUserId = Extorio::get()->getLoggedInUserId();
        ob_start();
        switch($this->role) {
            case "cell" :
                ?>
<div
    data-ls-x="<?= $this->x ?>"
    data-ls-width="<?= $this->width ?>"
    data-ls-align="<?= $this->align ?>"
    data-ls-hidexs="<?php if ($this->hidexs)
        echo "true"; else echo "false"; ?>"
    data-ls-hidesm="<?php if ($this->hidesm)
        echo "true"; else echo "false"; ?>"
    data-ls-hidemd="<?php if ($this->hidemd)
        echo "true"; else echo "false"; ?>"
    data-ls-hidelg="<?php if ($this->hidelg)
        echo "true"; else echo "false"; ?>"
    data-ls-hideall="<?php if ($this->hideall)
        echo "true"; else echo "false"; ?>"
    data-ls-editable="<?= $editable ?>"
    data-ls-role="cell"
    data-ls-classes="<?=$this->classes?>"
    data-ls-styling="<?=$this->styling?>"
    class="layout_cell">
    <div class="layout_cell_content_outer">
        <div class="layout_cell_content_inner">
            <?php
            foreach ($this->rows as $row) {
                echo $row->__toHtml($content, $contentid, $editMode);
            }
            ?>
        </div>
    </div>
</div>
                <?php
                break;
            case "block" :
                $b = Block::findById($this->blockid,2);
                if($b) {
                    if(Blocks::canUserReadBlock($loggedInUserId,$b->id)) {
                        $canModify = Blocks::canUserModifyBlock($loggedInUserId,$b->id);
                        ?>
<div
    data-ls-x="<?= $this->x ?>"
    data-ls-width="<?= $this->width ?>"
    data-ls-align="<?= $this->align ?>"
    data-ls-hidexs="<?php if ($this->hidexs)
        echo "true"; else echo "false"; ?>"
    data-ls-hidesm="<?php if ($this->hidesm)
        echo "true"; else echo "false"; ?>"
    data-ls-hidemd="<?php if ($this->hidemd)
        echo "true"; else echo "false"; ?>"
    data-ls-hidelg="<?php if ($this->hidelg)
        echo "true"; else echo "false"; ?>"
    data-ls-hideall="<?php if ($this->hideall)
        echo "true"; else echo "false"; ?>"
    data-ls-editable="<?= $editable ?>"
    data-ls-blockid="<?= $b->id ?>"
    data-ls-role="block"
    data-ls-classes="<?=$this->classes?>"
    data-ls-styling="<?=$this->styling?>"
    <?php
    if($editMode && $canModify) {
        ?>data-ls-configurable="true"<?php
    } else {
        ?>data-ls-configurable="false"<?php
    }
    ?>
    class="layout_cell">
    <?php
    if($editMode) {
        ?>
        <span class="layout_cell_block_label"><?=$b->name?> (<?=$b->processor->label?>)</span>
        <?php
    }
    ?>
    <div class="layout_cell_content_outer">
        <div class="layout_cell_content_inner">
            <?php
            //find the block processor module
            $extension = Extorio::get()->getExtension($b->processor->extensionName);
            if($extension) {
                $extorio = Extorio::get();
                //fetch the block's content, and parse for inline content
                $content = $extorio->fetchBlockProcessorViewContent($extension->_constructComponent($b->processor->namespace),$b->config,$b->id,false);
                $content = $extorio->parseInlineBlocks($content);
                $content = $extorio->parseInlineTranslations($content);
                echo $content;
            }
            ?>
        </div>
    </div>
</div>
                    <?php
                    }
                }
                break;
            case "content" :
                if ($editMode) {
                    $content = '
                    <div class="well well-lg">
                        <h3>Content cell</h3>
                        <p>
                            The page layout/content will be displayed here
                        </p>
                    </div>
                    ';
                }
                ?>
<div
    data-ls-x="<?= $this->x ?>"
    data-ls-width="<?= $this->width ?>"
    data-ls-align="<?= $this->align ?>"
    data-ls-hidexs="<?php if ($this->hidexs)
        echo "true"; else echo "false"; ?>"
    data-ls-hidesm="<?php if ($this->hidesm)
        echo "true"; else echo "false"; ?>"
    data-ls-hidemd="<?php if ($this->hidemd)
        echo "true"; else echo "false"; ?>"
    data-ls-hidelg="<?php if ($this->hidelg)
        echo "true"; else echo "false"; ?>"
    data-ls-hideall="<?php if ($this->hideall)
        echo "true"; else echo "false"; ?>"
    data-ls-editable="<?= $editable ?>"
    data-ls-role="content"
    data-ls-classes="<?=$this->classes?>"
    data-ls-styling="<?=$this->styling?>"
    id="<?=$contentid?>"
    class="layout_cell">
    <div class="layout_cell_content_outer">
        <div class="layout_cell_content_inner">
            <?= $content ?>
        </div>
    </div>
</div>
                <?php
                break;
        }
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}