<?php
namespace Core\Classes\Helpers;
/**
 * A simple helper for easily handling command line processes
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Command
 */
class Command {
    private $out;
    private $err;
    private $status;

    /**
     * Command constructor.
     *
     * @param string $cmd The command
     * @param string|null $input Any input to send along with the process
     * @param string|null $cwd Set the working directory of the process
     * @param array|null $env Override the environment variables
     */
    public function __construct($cmd, $input = null, $cwd = null, $env = null) {
        $descriptorspec = array(
            0 => array("pipe", "r"),
            1 => array("pipe", "w"),
            2 => array("pipe", "w")
        );
        $pipes = array();
        $process = proc_open($cmd,$descriptorspec,$pipes,$cwd,$env);
        if(is_resource($process)) {
            fwrite($pipes[0],$input);
            fclose($pipes[0]);

            $this->out = stream_get_contents($pipes[1]);
            fclose($pipes[1]);

            $this->err = stream_get_contents($pipes[2]);
            fclose($pipes[2]);

            $this->status = proc_close($process);
        }
    }

    /**
     * Run a command
     *
     * @param string $cmd The command
     * @param string|null $input Any input to send along with the process
     * @param string|null $cwd Set the working directory of the process
     * @param array|null $env Override the environment variables
     *
     * @return Command
     */
    public static function n($cmd, $input = null, $cwd = null, $env = null) {
        return new Command($cmd,$input,$cwd,$env);
    }

    /**
     * Whether or not the command generated an error
     *
     * @return bool
     */
    public function hasError() {
        return strlen($this->err) > 0;
    }

    /**
     * Get the output from the command
     *
     * @return string
     */
    public function getOutput() {
        return $this->out;
    }

    /**
     * Get the error from the command
     *
     * @return string
     */
    public function getError() {
        return $this->err;
    }

    /**
     * Get the terminating status of the command
     *
     * @return int
     */
    public function getStatus() {
        return $this->status;
    }
}