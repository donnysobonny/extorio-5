<?php
namespace Core\Classes\Helpers;
use Core\Classes\Enums\FontAwesomeIcons;

/**
 * This helper class is used to create nav menu items for use in bootstrap and in the components of extorio that use them
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class NavMenuItem
 */
class NavMenuItem {
    /**
     * Whether of not the item is a header
     *
     * @var bool
     */
    public $isHeader = false;
    /**
     * Whether or not the item is a seperator
     *
     * @var bool
     */
    public $isSeperator = false;
    /**
     * The visual label of the item
     *
     * @var string
     */
    public $label = "";
    /**
     * The font awesome icon
     *
     * @var string
     */
    public $icon = "";
    /**
     * The url of the item
     *
     * @var string
     */
    public $url = "";
    /**
     * The url that makes the item active
     *
     * @var string
     */
    public $activeUrl = "";
    /**
     * Whether clicking on the item takes you to a new window or not
     *
     * @var bool
     */
    public $newWindow = false;
    /**
     * If the item has sub items, it is a dropdown. These are the dropdown's sub items.
     *
     * @var NavMenuItem[]
     */
    public $subItems = array();

    /**
     * Lazily create a new instance
     *
     * @param string $label
     * @param string $url
     * @param string $activeUrl
     * @param string $icon
     * @param bool $newWindow
     * @param NavMenuItem[] $subItems
     *
     * @return NavMenuItem
     */
    public static function n(
        $label = "",
        $url = "",
        $activeUrl = "",
        $icon = FontAwesomeIcons::_square,
        $newWindow = false,
        $subItems = array()
    ) {
        $i = new NavMenuItem();
        $i->label = $label;
        $i->activeUrl = $activeUrl;
        $i->url = $url;
        $i->icon = $icon;
        $i->newWindow = $newWindow;
        $i->subItems = $subItems;
        return $i;
    }

    /**
     * Create an item that is specifically an item (not a dropdown)
     *
     * @param string $label
     * @param string $url
     * @param string $activeUrl
     * @param string $icon
     * @param bool $newWindow
     *
     * @return NavMenuItem
     */
    public static function item(
        $label = "",
        $url = "",
        $activeUrl = "",
        $icon = FontAwesomeIcons::_square,
        $newWindow = false
    ) {
        return NavMenuItem::n($label,$url,$activeUrl,$icon,$newWindow);
    }

    /**
     * Create an item that is specifically a dropdown
     *
     * @param string $label
     * @param string $icon
     * @param NavMenuItem[] $subItems
     *
     * @return NavMenuItem
     */
    public static function dropdown(
        $label = "",
        $icon = FontAwesomeIcons::_square,
        $subItems = array()
    ) {
        return NavMenuItem::n($label,"","",$icon,false,$subItems);
    }

    /**
     * Create an item that is a seperator. Can only be used in dropdowns.
     *
     * @return NavMenuItem
     */
    public static function seperator() {
        $i = new NavMenuItem();
        $i->isSeperator = true;
        return $i;
    }

    /**
     * Create an item that is a header. Can only be used in dropdowns.
     *
     * @param string $label
     * @param string $icon
     *
     * @return NavMenuItem
     */
    public static function header(
        $label = "",
        $icon = FontAwesomeIcons::_square
    ) {
        $i = new NavMenuItem();
        $i->isHeader = true;
        $i->label = $label;
        $i->icon = $icon;
        return $i;
    }

    /**
     * Get the html of an item to be used in a bootstrap nav menu.
     *
     * @param string $currentUrl
     *
     * @return string
     */
    public function __toHTML($currentUrl = "") {
        $html = "";
        if($this->isSeperator) {
            $html = '<li role="separator" class="divider"></li>';
        } elseif($this->isHeader) {
            $html = '<li class="dropdown-header">';
            if(strlen($this->icon)) {
                $html .= '<span class="fa fa-'.$this->icon.'"></span> ';
            }
            $html .= $this->label.'</li>';
        } elseif(empty($this->subItems)) {
            //normal item
            $html = '<li class="';
            if(substr($currentUrl,0,strlen($this->activeUrl)) == $this->activeUrl) {
                $html .= "active";
            }
            $html .= '" target="';
            if($this->newWindow) {
                $html .= "_blank";
            } else {
                $html .= "_self";
            }
            $html .= '" role="presentation"><a href="'.$this->url.'">';
            if(strlen($this->icon)) {
                $html .= '<span class="fa fa-'.$this->icon.'"></span> ';
            }
            $html .= $this->label.'</a></li>';
        } else {
            //dropdown
            $html = '<li role="presentation" class="dropdown">';
            $html .= '    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">';
            if(strlen($this->icon)) {
                $html .= '      <span class="fa fa-'.$this->icon.'"></span> ';
            }
            $html .= '      '.$this->label.' <span class="caret"></span>';
            $html .= '      </a>';
            $html .= '      <ul class="dropdown-menu">';
            foreach($this->subItems as $subItem) {
                $html .= $subItem->__toHTML($currentUrl);
            }
            $html .= '      </ul>';
            $html .= '</li>';
        }
        return $html;
    }
}