<?php
namespace Core\Classes\Helpers;
use Core\Classes\Enums\PHPTypes;
use MatthiasMullie\Minify\Exception;

/**
 * A helper class user to generate the query array for user report queries
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class UserReportQuery
 */
class UserReportQuery {

    //groups
    const _and = '$and';
    const _or = '$or';

    //operators
    const _eq = '$eq';
    const _ne = '$ne';
    const _gt = '$gt';
    const _lt = '$lt';
    const _lk = '$lk';

    //user properties
    const _canLogin = '$canLogin';
    const _dateLogin = '$dateLogin';
    const _dateActive = '$dateActive';
    const _userGroup = '$userGroup';
    const _language = '$language';
    const _dateCreated = '$dateCreated';
    const _dateUpdated = '$dateUpdated';
    const _acceptsMarketing = '$acceptsMarketing';
    const _title = '$title';
    const _gender = '$gender';
    const _city = '$city';
    const _region = '$region';
    const _postalCode = '$postalCode';
    const _country = '$country';

    //misc properties
    const _action = '$action';
    const _contact = '$contact';

    private $query = array();
    private $sql = '';
    private $params = array();
    private $index = 1;

    private $limit = 0;
    private $skip = 0;

    /**
     * Lazy construct
     *
     * @return UserReportQuery
     */
    public static function n() {
        $o = new UserReportQuery();
        return $o;
    }

    /**
     * Get the user properties exposed via reports
     *
     * @return array
     */
    public static function getExposedProperties() {
        return array(
            "id",
            "shortname",
            "longname",
            "email",
            "canLogin",
            "dateLogin",
            "dateActive",
            "dateCreated",
            "dateUpdated",
            "acceptsMarketing",
            "title",
            "gender",
            "address1",
            "address2",
            "city",
            "region",
            "postalCode",
            "country",
            "telno",
        );
    }

    /**
     * Add in the where array
     *
     * @param array $query
     *
     * @return $this
     */
    public function where(array $query) {
        $this->query = $query;
        return $this;
    }

    /**
     * Set the limit
     *
     * @param int $num
     *
     * @return $this
     */
    public function limit($num) {
        $this->limit = intval($num);
        return $this;
    }

    /**
     * Set the offset
     *
     * @param int $num
     *
     * @return $this
     */
    public function skip($num) {
        $this->skip = intval($num);
        return $this;
    }

    /**
     * Get the sql, including the formatted WHERE and LIMIT etc
     *
     * @return string
     */
    public function getSql() {
        $this->generateSql();
        $sql = 'SELECT U.id FROM core_classes_models_user U WHERE '.$this->sql." ";
        $sql .= "ORDER BY username ASC ";
        if($this->limit > 0) {
            $sql .= "LIMIT ".$this->limit." ";
        }
        if($this->skip > 0) {
            $sql .= "OFFSET ".$this->skip." ";
        }
        return $sql;
    }

    /**
     * Get the sql for counting results
     *
     * @return string
     */
    public function getCountSql() {
        $this->generateSql();
        $sql = 'SELECT COUNT(U.id) FROM core_classes_models_user U WHERE '.$this->sql." ";
        if($this->limit > 0) {
            $sql .= "LIMIT ".$this->limit." ";
        }
        if($this->skip > 0) {
            $sql .= "OFFSET ".$this->skip." ";
        }
        return $sql;
    }

    /**
     * Get the where params
     *
     * @return array
     */
    public function getParams() {
        return $this->params;
    }

    private function generateSql() {
        if(!is_array($this->query) || empty($this->query) || strlen($this->sql)) {
            return;
        }
        //check that the outer-most array key is $and/$or
        if(count($this->query) > 1) {
            $this->query = array('$and' => $this->query);
        } else {
            foreach($this->query as $key => $value) {
                if(!in_array($key,array(
                    self::_and,
                    self::_or
                ))) {
                    $this->query = array('$and' => $this->query);
                }
            }
        }
        $this->processOuterElement($this->query);
    }

    private function processOuterElement($elem,$type=false) {
        foreach($elem as $key => $value) {
            if(gettype($key) == PHPTypes::_integer) {
                $this->processInnerElement($value,$type);
            } else {
                $this->processInnerElement(array($key => $value),$type);
            }
        }
        switch($type) {
            case self::_and:
                $this->sql = substr($this->sql,0,strlen($this->sql)-5);
                break;
            case self::_or:
                $this->sql = substr($this->sql,0,strlen($this->sql)-4);
                break;
        }
    }

    private function processInnerElement($elem,$type=false) {
        foreach($elem as $key => $value) {
            //if key is numeric here, then it's probably an outer element
            if(gettype($key) == PHPTypes::_integer) {
                $this->processOuterElement($value);
            } else {
                //key must be a valid operator
                switch($key) {
                    case self::_and:
                        $this->sql .= "(";
                        $this->processOuterElement($value,self::_and);
                        $this->sql .= ") ";
                        break;
                    case self::_or:
                        $this->sql .= "(";
                        $this->processOuterElement($value,self::_or);
                        $this->sql .= ") ";
                        break;
                    case self::_action:
                        $this->processAction($value);
                        break;
                    case self::_contact:
                        $this->processContact($value);
                        break;
                    case self::_canLogin:
                        $this->processConditionalOuter('U."canLogin"',$value);
                        break;
                    case self::_dateLogin:
                        $this->processConditionalOuter('U."dateLogin"',$value,true);
                        break;
                    case self::_dateActive:
                        $this->processConditionalOuter('U."dateActive"',$value,true);
                        break;
                    case self::_userGroup:
                        $this->processConditionalOuter('U."userGroup"',$value);
                        break;
                    case self::_language:
                        $this->processConditionalOuter('U."language"',$value);
                        break;
                    case self::_dateCreated:
                        $this->processConditionalOuter('U."dateCreated"',$value,true);
                        break;
                    case self::_dateUpdated:
                        $this->processConditionalOuter('U."dateUpdated"',$value,true);
                        break;
                    case self::_acceptsMarketing:
                        $this->processConditionalOuter('U."acceptsMarketing"',$value);
                        break;
                    case self::_title:
                        $this->processConditionalOuter('U."title"',$value);
                        break;
                    case self::_gender:
                        $this->processConditionalOuter('U."gender"',$value);
                        break;
                    case self::_city:
                        $this->processConditionalOuter('U."city"',$value);
                        break;
                    case self::_region:
                        $this->processConditionalOuter('U."region"',$value);
                        break;
                    case self::_postalCode:
                        $this->processConditionalOuter('U."postalCode"',$value);
                        break;
                    case self::_country:
                        $this->processConditionalOuter('U."county"',$value);
                        break;
                    default:
                        throw new \Exception($key." is not a valid operator");
                        break;
                }
                switch($type) {
                    case self::_and:
                        $this->sql .= "AND ";
                        break;
                    case self::_or:
                        $this->sql .= "OR ";
                        break;
                }
            }
        }
    }

    private function processAction($action) {
        $u = UserActionQuery::constructFromArray($action);
        if(!strlen($u->type) || $u->type < 0) {
            throw new \Exception("A user action query must have a type");
        }
        if(empty($u->count)) {
            throw new \Exception("A user action query must specify a count");
        }
        $this->sql .= '(SELECT count(id) FROM core_classes_models_useraction WHERE "userId" = U.id AND type = $'.$this->index." AND ";
        $this->index++;
        $this->params[] = $u->type;
        if(!empty($u->date)) {
            $this->processConditionalOuter("date",$u->date,true);
            $this->sql .= "AND ";
        }
        if(!empty($u->time)) {
            $this->processConditionalOuter("time",$u->time,false,true);
            $this->sql .= "AND ";
        }
        if(!empty($u->detail1)) {
            $this->processConditionalOuter("detail1",$u->detail1);
            $this->sql .= "AND ";
        }
        if(!empty($u->detail2)) {
            $this->processConditionalOuter("detail2",$u->detail2);
            $this->sql .= "AND ";
        }
        if(!empty($u->detail3)) {
            $this->processConditionalOuter("detail3",$u->detail3);
            $this->sql .= "AND ";
        }
        $this->sql = substr($this->sql,0,strlen($this->sql)-5);
        $this->sql .= ")";
        $this->processConditionalOuter("",$u->count);
    }

    private function processContact($contact) {
        $u = UserContactQuery::constructFromArray($contact);
        if(empty($u->count)) {
            throw new \Exception("A user contact query must specify a count");
        }
        $this->sql .= '(SELECT count(id) FROM core_classes_models_usercontact WHERE "userId" = U.id AND ';
        if(!empty($u->type)) {
            $this->processConditionalOuter("type",$u->type);
            $this->sql .= "AND ";
        }
        if(!empty($u->topic)) {
            $this->processConditionalOuter("topic",$u->topic);
            $this->sql .= "AND ";
        }
        if(!empty($u->identifier)) {
            $this->processConditionalOuter("identifier",$u->identifier);
            $this->sql .= "AND ";
        }
        if(!empty($u->date)) {
            $this->processConditionalOuter("date",$u->date,true);
            $this->sql .= "AND ";
        }
        if(!empty($u->time)) {
            $this->processConditionalOuter("time",$u->time,false,true);
            $this->sql .= "AND ";
        }
        $this->sql = substr($this->sql,0,strlen($this->sql)-5);
        $this->sql .= ")";
        $this->processConditionalOuter("",$u->count);
    }

    private function processConditionalOuter($field,$value,$date=false,$time=false) {
        if(gettype($value) == PHPTypes::_array) {
            foreach($value as $key => $v) {
                switch($key) {
                    case self::_eq:
                        $this->sql .= ''.$field.' = $'.$this->index." ";
                        $this->index++;
                        $this->processConditionalValue($v,$date,$time);
                        break;
                    case self::_ne:
                        $this->sql .= ''.$field.' != $'.$this->index." ";
                        $this->index++;
                        $this->processConditionalValue($v,$date,$time);
                        break;
                    case self::_gt:
                        $this->sql .= ''.$field.' > $'.$this->index." ";
                        $this->index++;
                        $this->processConditionalValue($v,$date,$time);
                        break;
                    case self::_lt:
                        $this->sql .= ''.$field.' < $'.$this->index." ";
                        $this->index++;
                        $this->processConditionalValue($v,$date,$time);
                        break;
                    case self::_lk:
                        $this->sql .= 'LOWER(CAST('.$field.' AS text)) LIKE LOWER($'.$this->index.') ';
                        $this->index++;
                        $this->processConditionalValue($v,$date,$time,true);
                        break;
                    default:
                        throw new \Exception($key." is not a valid conditional operator");
                        break;
                }
            }
        } else {
            $this->sql .= ''.$field.' = $'.$this->index." ";
            $this->index++;
            $this->processConditionalValue($value,$date,$time);
        }
    }

    private function processConditionalValue($value,$date=false,$time=false,$like=false) {
        if($date) {
            $parts = explode("|",$value);
            $dt = new \DateTime();
            switch($parts[2]) {
                case "past":
                    $dt->modify("- ".$parts[0]." ".$parts[1]);
                    break;
                case "future":
                    $dt->modify("+ ".$parts[0]." ".$parts[1]);
                    break;
            }
            $value = $dt->format("Y-m-d");
        } elseif($time) {
            $parts = explode("|",$value);
            $dt = new \DateTime();
            switch($parts[2]) {
                case "past":
                    $dt->modify("- ".$parts[0]." ".$parts[1]);
                    break;
                case "future":
                    $dt->modify("+ ".$parts[0]." ".$parts[1]);
                    break;
            }
            $value = $dt->format("H:i:s");
        }
        if($like) {
            $this->params[] = "%".$value."%";
        } else {
            $this->params[] = $value;
        }
    }
}