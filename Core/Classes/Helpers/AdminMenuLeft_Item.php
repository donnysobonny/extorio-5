<?php
namespace Core\Classes\Helpers;
/**
 * Used to create a menu item to go into the left admin menu
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class AdminMenuLeft_Item
 */
class AdminMenuLeft_Item {
    /**
     * The label for the item
     *
     * @var string
     */
    public $label = "";
    /**
     * A description for the item
     *
     * @var string
     */
    public $description = "";
    /**
     * The icon class
     *
     * @var string
     */
    public $icon = "circle";
    /**
     * The url that the item goes to
     *
     * @var string
     */
    public $url = "";
    /**
     * The url that makes the item active
     *
     * @var string
     */
    public $active_url = "";
    /**
     * Open in a new window
     *
     * @var bool
     */
    public $new_window = false;

    /**
     * Lazily construct a new instance
     *
     * @param $label
     * @param $description
     * @param $url
     * @param $active_url
     * @param string $icon
     * @param bool $new_window
     *
     * @return AdminMenuLeft_Item
     */
    public static function n($label, $description, $url, $active_url, $icon = "circle", $new_window = false) {
        $i = new AdminMenuLeft_Item();
        $i->label = $label;
        $i->description = $description;
        $i->url = $url;
        $i->active_url = $active_url;
        $i->icon = $icon;
        $i->new_window = $new_window;
        return $i;
    }

    /**
     * Construct an item from an array
     *
     * @param $array
     *
     * @return AdminMenuLeft_Item
     */
    public static function constructFromArray($array) {
        $i = new AdminMenuLeft_Item();
        foreach ($i as $key => $value) {
            if (isset($array[$key])) {
                $i->$key = $array[$key];
            }
        }
        return $i;
    }
}