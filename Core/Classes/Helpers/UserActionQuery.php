<?php
namespace Core\Classes\Helpers;
class UserActionQuery {
    public $type;
    public $date;
    public $time;
    public $detail1;
    public $detail2;
    public $detail3;
    public $count;

    /**
     * Lazy construct
     *
     * @return UserActionQuery
     */
    public static function n() {
        return new UserActionQuery();
    }

    /**
     * Construct from array
     *
     * @param array $array
     *
     * @return UserActionQuery
     */
    public static function constructFromArray($array) {
        $o = UserActionQuery::n();
        foreach($o as $k => $v) {
            if(isset($array[$k])) {
                $o->$k = $array[$k];
            }
        }
        return $o;
    }
}