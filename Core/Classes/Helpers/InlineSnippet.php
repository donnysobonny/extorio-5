<?php
namespace Core\Classes\Helpers;
class InlineSnippet {
    public $orig;
    public $match;

    /**
     * InlineSnippet constructor.
     *
     * @param $orig
     * @param $match
     */
    public function __construct($orig, $match) {
        $this->orig = $orig;
        $this->match = $match;
    }

    /**
     * Lazy constructor
     *
     * @param $orig
     * @param $match
     *
     * @return InlineSnippet
     */
    public static function n($orig,$match) {
        return new InlineSnippet($orig,$match);
    }
}