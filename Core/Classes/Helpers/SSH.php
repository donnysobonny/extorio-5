<?php
namespace Core\Classes\Helpers;
/**
 * A simple helper object for SSH connections and streaming
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class SSH
 */
class SSH {
    private $res;

    /**
     * SSH constructor.
     *
     * @param $res
     */
    public function __construct($res) {
        $this->res = $res;
    }

    /**
     * Lazy construct
     *
     * @param string $host
     * @param int $port
     * @param array|null $methods
     * @param array|null $callbacks
     *
     * @return SSH
     */
    public static function n($host="localhost",$port=22,$methods=null,$callbacks=null) {
        $conn = ssh2_connect($host,$port,$methods,$callbacks);
        if(!$conn) {
            throw new \Exception("Could not reach the ssh server to create the SSH object");
        }
        $ssh = new SSH($conn);
        return $ssh;
    }

    /**
     * Authenticate using a username and password
     *
     * @param $username
     * @param $password
     *
     * @return bool
     * @throws \Exception
     */
    public function authPassword($username,$password) {
        if(!ssh2_auth_password($this->res,$username,$password)) {
            throw new \Exception("Failed to authenticate via password");
        }
        return true;
    }

    /**
     * Execute a command. Throws an exception on error, otherwise returns the output.
     *
     * @param string $cmd
     * @param null $pty
     * @param null $env
     * @param null $width
     * @param null $height
     * @param int $width_height_type
     *
     * @return string
     * @throws \Exception
     */
    public function exec($cmd,$pty=null,$env=null,$width=null,$height=null,$width_height_type=SSH2_TERM_UNIT_CHARS) {
        $stream = ssh2_exec($this->res,$cmd,$pty,$env,$width,$height,$width_height_type);
        $errStream = ssh2_fetch_stream($stream,SSH2_STREAM_STDERR);

        stream_set_blocking($errStream,true);
        stream_set_blocking($stream,true);

        $out = stream_get_contents($stream);
        $err = stream_get_contents($errStream);

        fclose($stream);
        fclose($errStream);

        if(strlen($err)) {
            throw new \Exception($err);
        }

        return $out;
    }
}