<?php
namespace Core\Classes\Helpers;
use Core\Classes\Commons\Extorio;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;

/**
 * This helper class is useful for creating efficient filtering, searching and pagination in preperation for displaying data
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class SimpleFiltering
 */
final class SimpleFiltering {

    private $_srch = "";
    private $_qry = "";
    private $_pg = 0;
    private $_cnt = 0;
    private $_lmt = 10;

    private $filtering = array();
    private $values = array();
    private $searchTypes = array();

    private $uid = "";
    private $url = "";

    /**
     * SimpleFiltering constructor.
     *
     * @param $uid
     */
    public function __construct($uid,$url=null) {
        $this->uid = $uid;
        if(!strlen($url)) {
            $this->url = Server::getRequestURL();
        } else {
            $this->url = $url;
        }
        Extorio::get()->includeIncluder("select");
    }

    /**
     * Lazily construct an instance
     *
     * @param $uid
     *
     * @return SimpleFiltering
     */
    public static function n($uid,$url=null) {
        return new SimpleFiltering($uid,$url);
    }

    /**
     * Add a filter
     *
     * @param $name
     * @param $filters
     */
    public function addFilter($name, $filters) {
        $this->filtering[Strings::labelSafe($name)] = $filters;
    }

    private $emptyIndex = 1;
    /**
     * Add an empty filter
     */
    public function addEmpty() {
        $this->addFilter($this->emptyIndex,null);
        $this->emptyIndex++;
    }

    /**
     * Set the search type options
     *
     * @param array $searchTypes
     */
    public function setSearchTypes($searchTypes) {
        $this->searchTypes = $searchTypes;
    }

    /**
     * Set the number of results
     *
     * @param int $count
     */
    public function setCount($count) {
        $this->_cnt = intval($count);
    }

    /**
     * Set the page limit
     *
     * @param $limit
     */
    public function setLimit($limit) {
        $this->_lmt = intval($limit);
    }

    /**
     * Set the page number
     *
     * @param int $page
     */
    public function setPage($page) {
        $this->_pg = intval($page);
    }

    /**
     * Get the page limit
     *
     * @return int
     */
    public function getLimit() {
        return intval($this->_lmt);
    }

    /**
     * Get a filter balue
     *
     * @param $name
     *
     * @return mixed
     */
    public function getFilter($name) {
        return $this->values[$name];
    }

    /**
     * Get the currently selected search type option
     *
     * @return mixed
     */
    public function getSearchType() {
        return $this->_srch;
    }

    /**
     * Get the current search query
     *
     * @return mixed
     */
    public function getSearchQuery() {
        return $this->_qry;
    }

    /**
     * Get the current page
     *
     * @return int
     */
    public function getPage() {
        return intval($this->_pg);
    }

    /**
     * Get the offset, based on the current page limit and page
     *
     * @return int
     */
    public function getOffset() {
        return $this->getLimit() * $this->getPage();
    }

    /**
     * Display the limit, left, right and order filtering options
     */
    public function displayFiltering() {
        ?>
        <div class="well well-sm">
            <div class="row">
                <?php
                $count = count($this->filtering);
                foreach($this->filtering as $name => $filters) {
                    ?>
                    <div class="col-sm-<?=round(12/$count)?>">
                        <?php $this->displayFilterByName($name) ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <script>
            $(function () {
                $('.simplefiltering_filter').on("change", function() {
                    var url = "<?=$this->url?>";
                    var query = "";
                    $('.simplefiltering_filter').each(function() {
                        var name = $(this).attr('data-filtername');
                        if(name != undefined) {
                            query += name + "=" + $(this).val() + "&";
                        }
                    });
                    var srch = $('#_srch').val();
                    var qry = $('#_qry').val();
                    if(srch != undefined) {
                        query += "_srch="+srch+"&";
                    }
                    if(qry != undefined) {
                        query += "_qry="+qry+"&";
                    }
                    query += "_pg=0&";
                    if(query.length > 0) {
                        url += "?" + query.substr(0,query.length-1);
                    }
                    window.location.href = url;
                });
            });
        </script>
        <?php
    }

    /**
     * Display the search options
     */
    public function displaySearching() {
        $uid = uniqid();
        ?>
        <form id="<?= $uid ?>" method="GET" action="" class="form-inline">
            <?php
            if (count($this->searchTypes) > 0) {
                ?>
                <div class="form-group">
                    <select data-width="100%" class="selectpicker" id="_srch">
                        <?php
                        foreach($this->searchTypes as $k => $v) {
                            if(is_array($v)) {
                                ?>
                                <optgroup label="<?=$k?>">
                                    <?php
                                    foreach($v as $ki => $vi) {
                                        ?>
                                        <option <?php
                                        if($this->_srch == $ki) echo 'selected="selected"';
                                        ?> value="<?=$ki?>"><?=$vi?></option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                                <?php
                                foreach($v as $ki => $vi) {
                                    $vals[] = $ki;
                                }
                            } else {
                                ?>
                                <option <?php
                                if($this->_srch == $k) echo 'selected="selected"';
                                ?> value="<?=$k?>"><?=$v?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <?php
            }
            ?>
            <div class="form-group">
                <div class="input-group">
                    <input value="<?= $this->_qry ?>" type="text" class="form-control" id="_qry"
                           placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit"><span class="fa fa-search"></span></button>
                        </span>
                </div>
            </div>
        </form><br/>
        <script>
            $(function() {
                $('#<?=$uid?>').submit(function (e) {
                    e.preventDefault();
                    var url = "<?=$this->url?>";
                    var query = "";
                    $('.simplefiltering_filter').each(function() {
                        var name = $(this).attr('data-filtername');
                        if(name != undefined) {
                            query += name + "=" + $(this).val() + "&";
                        }
                    });
                    var srch = $('#_srch').val();
                    var qry = $('#_qry').val();
                    if(srch != undefined) {
                        query += "_srch="+srch+"&";
                    }
                    if(qry != undefined) {
                        query += "_qry="+qry+"&";
                    }
                    query += "_pg=0&";
                    if(query.length > 0) {
                        url += "?" + query.substr(0,query.length-1);
                    }
                    window.location.href = url;
                });
            });
        </script>
        <?php
    }

    /**
     * Display pagination
     */
    public function displayPagination() {
        $urlbase = $this->url;
        $query = "";
        foreach($this->values as $name => $value) {
            $query .= $name . "=" . $value . "&";
        }
        $query .= "_srch=" . $this->_srch . "&";
        $query .= "_qry=" . $this->_qry . "&";
        if(strlen($query)) {
            $urlbase .= "?" . $query;
        }

        $page = $this->_pg + 1;
        $totalPages = ceil($this->_cnt / $this->_lmt);
        if ($totalPages <= 0)
            $totalPages = 1;
        $mid = $page;
        if ($mid > $totalPages - 2) {
            $mid = $totalPages - 2;
        }
        if ($mid < 3) {
            $mid = 3;
        }
        $pagesRaw = array($mid - 2, $mid - 1, $mid, $mid + 1, $mid + 2);
        $pages = array();
        foreach ($pagesRaw as $r) {
            if ($r >= 1 && $r <= $totalPages) {
                $pages[] = $r;
            }
        }
        ?>
        <div style="margin-bottom: 0;" class="well well-sm">
            <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                <tr>
                    <td style="vertical-align: middle;">
                        Page <?= $page ?> of <?= $totalPages ?> <small>(from <?= number_format($this->_cnt) ?> total results)</small>
                    </td>
                    <td style="vertical-align: middle; text-align: right;">
                        <nav>
                            <ul style="margin: 0px; display: inline-block; float: right;" class="pagination">
                                <?php
                                if ($this->_pg <= 0) {
                                    ?>
                                    <li class="disabled"><a href="javascript:;"><span class="fa fa-angle-double-left"></span></a></li><?php
                                } else {
                                    ?>
                                    <li><a href="<?= $urlbase . "_pg=" . ($this->_pg - 1)?>" aria-label="Previous"><span class="fa fa-angle-double-left"></span></a></li><?php
                                }
                                if ($page > 3 && !in_array(1,$pages)) {
                                    ?>
                                    <li><a href="<?= $urlbase . "_pg=0"?>">1</a></li>
                                    <li class="disabled"><a href="javascript:;">...</a></li><?php
                                }
                                foreach ($pages as $p) {
                                    ?>
                                <li class="<?php
                                if (($p - 1) == $this->_pg)
                                    echo 'active';
                                ?>"><a
                                        href="<?= $urlbase . "_pg=" . ($p - 1)?>"><?= $p ?></a>
                                    </li><?php
                                }
                                if (($page + 2) < $totalPages && !in_array($totalPages,$pages)) {
                                    ?>
                                    <li class="disabled"><a href="javascript:;">...</a></li>
                                    <li><a href="<?= $urlbase . "_pg=" . ($totalPages-1)?>"><?=$totalPages?></a></li><?php
                                }
                                if ($page >= $totalPages) {
                                    ?>
                                    <li class="disabled"><a href="javascript:;"><span class="fa fa-angle-double-right"></span></a></li><?php
                                } else {
                                    ?>
                                    <li><a href="<?= $urlbase . "_pg=" . ($this->_pg + 1)?>" aria-label="Next"><span class="fa fa-angle-double-right"></span></a></li><?php
                                }
                                ?>
                            </ul>
                        </nav>
                    </td>
                </tr>
            </table>
        </div>
        <?php
    }

    /**
     * Extract the filtering from $_GET and $_SESSION. This should be done after setting the filters, and before displaying
     */
    public function extractFiltering() {
        foreach($this->filtering as $name => $filters) {
            $values = $this->getFilterValues($name);
            if(isset($_GET[$name]) && in_array($_GET[$name],$values)) {
                $this->values[$name] = $_GET[$name];
            } elseif(isset($_SESSION[$this->uid . "_" . $name]) && in_array($_SESSION[$this->uid . "_" . $name],$values)) {
                $this->values[$name] = $_SESSION[$this->uid . "_" . $name];
            } else {
                if(count($values) > 0) {
                    $this->values[$name] = $values[0];
                } else {
                    $this->values[$name] = null;
                }
            }
        }

        $values = $this->getSearchTypes();
        if(isset($_GET["_srch"]) && in_array($_GET["_srch"],$values)) {
            $this->_srch = $_GET["_srch"];
        } elseif(isset($_SESSION[$this->uid . "_srch"]) && in_array($_SESSION[$this->uid . "_srch"],$values)) {
            $this->_srch = $_SESSION[$this->uid . "_srch"];
        } else {
            $this->_srch = $values[0];
        }
        if(isset($_GET["_qry"])) {
            $this->_qry = $_GET["_qry"];
        } elseif(isset($_SESSION[$this->uid . "_qry"])) {
            $this->_qry = $_SESSION[$this->uid . "_qry"];
        }
        if(isset($_GET["_pg"])) {
            $this->_pg = intval($_GET["_pg"]);
        } elseif(isset($_SESSION[$this->uid . "_pg"])) {
            $this->_pg = intval($_SESSION[$this->uid . "_pg"]);
        }

        foreach($this->values as $name => $value) {
            $_SESSION[$this->uid . "_" . $name] = $value;
        }
        $_SESSION[$this->uid . "_qry"] = $this->_qry;
        $_SESSION[$this->uid . "_srch"] = $this->_srch;
        $_SESSION[$this->uid . "_pg"] = $this->_pg;
    }

    private function displayFilterByName($name) {
        $this->displayFilter($name,$this->filtering[$name],$this->getFilter($name));
    }

    private function displayFilter($name,$filter,$val) {
        if(is_array($filter)) {
            ?>
            <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                <tbody>
                    <tr>
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; padding-right: 10px; white-space: nowrap;"><?=Strings::titleSafe($name)?></td>
                        <td style="width: 100%;">
                            <div class="form-search">
                                <select data-filtername="<?=$name?>" data-width="100%" class="selectpicker simplefiltering_filter">
                                    <?php
                                    foreach($filter as $k => $v) {
                                        if(is_array($v)) {
                                            ?>
                                            <optgroup label="<?=$k?>">
                                                <?php
                                                foreach($v as $ki => $vi) {
                                                    ?>
                                                    <option <?php
                                                    if($val == $ki) echo 'selected="selected"';
                                                    ?> value="<?=$ki?>"><?=$vi?></option>
                                                    <?php
                                                }
                                                ?>
                                            </optgroup>
                                            <?php
                                            foreach($v as $ki => $vi) {
                                                $vals[] = $ki;
                                            }
                                        } else {
                                            ?>
                                            <option <?php
                                            if($val == $k) echo 'selected="selected"';
                                            ?> value="<?=$k?>"><?=$v?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php
        }
    }

    private function getSearchTypes() {
        $vals = array();
        foreach($this->searchTypes as $k => $v) {
            if(is_array($v)) {
                foreach($v as $ki => $vi) {
                    $vals[] = $ki;
                }
            } else {
                $vals[] = $k;
            }
        }
        return $vals;
    }

    private function getFilterValues($name) {
        if(isset($this->filtering[$name]) && is_array($this->filtering[$name])) {
            $vals = array();
            foreach($this->filtering[$name] as $k => $v) {
                if(is_array($v)) {
                    foreach($v as $ki => $vi) {
                        $vals[] = $ki;
                    }
                } else {
                    $vals[] = $k;
                }
            }
            return $vals;
        }
        return array();
    }
}