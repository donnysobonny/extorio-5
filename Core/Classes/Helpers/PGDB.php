<?php
namespace Core\Classes\Helpers;
use Core\Classes\Enums\PHPTypes;
use Core\Classes\Exceptions\DB_Exception;

/**
 * This helper is used by extorio as an OOP alternative to the default pg functions
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class PGDB
 */
class PGDB {

    /**
     * The database name
     *
     * @var string
     */
    private $dbname = "";
    /**
     * The user
     *
     * @var string
     */
    private $user = "";
    /**
     * The password
     *
     * @var string
     */
    private $pass = "";
    /**
     * The host
     *
     * @var string
     */
    private $host = "localhost";
    /**
     * The port
     *
     * @var int
     */
    private $port = 5432;

    /**
     * The actual connection. Note that the connection may not actually be connected
     *
     * @var
     */
    private $conn;

    /**
     * Whether the conn is connected
     *
     * @var bool
     */
    private $connected = false;

    /**
     * Construct a new instance, and try to connect. Note that failure to connect does not throw any errors. In stead,
     * you should check whether we are connected using connected()
     *
     * @param $dbname
     * @param $user
     * @param $pass
     * @param string $host
     * @param int $port
     */
    function __construct($dbname,$user,$pass,$host="localhost",$port=5432) {
        $this->dbname = $dbname;
        $this->user = $user;
        $this->pass = $pass;
        $this->host = $host;
        $this->port = $port;
        //auto connect
        $this->connect();
    }

    /**
     * We disconnect automatically
     */
    function __destruct() {
        //auto disconnect
        $this->disconnect();
    }

    /**
     * Lazily construct a PGDB instance
     *
     * @param $dbname
     * @param $user
     * @param $pass
     * @param string $host
     * @param int $port
     *
     * @return PGDB
     */
    public static function n($dbname,$user,$pass,$host="localhost",$port=5432) {
        return new PGDB($dbname,$user,$pass,$host,$port);
    }

    /**
     * Whether this instance is connected
     *
     * @return bool
     */
    public function connected() {
        return $this->connected;
    }

    /**
     * Connect this instance. Returns TRUE/FALSE based on whether the connection could be made
     *
     * @return bool
     */
    public function connect() {
        if(!$this->connected()) {
            //create the connection
            $str = "";
            if(strlen($this->dbname)) {
                $str .= "dbname=".$this->dbname." ";
            }
            if(strlen($this->user)) {
                $str .= "user=".$this->user." ";
            }
            if(strlen($this->pass)) {
                $str .= "password=".$this->pass." ";
            }
            if(strlen($this->host)) {
                $str .= "host=".$this->host." ";
            }
            if(strlen($this->port)) {
                $str .= "port=".$this->port." ";
            }
            //try and create the connection
            $this->conn = pg_connect($str);
            if(!$this->conn) {
                $this->connected = false;
                return false;
            } else {
                $this->connected = true;
                return true;
            }
        }
        //already connected
        return true;
    }

    /**
     * Get the connection of this instance
     *
     * @return Resource
     */
    public function getConnection() {
        return $this->conn;
    }

    /**
     * Disconnect this instance
     */
    public function disconnect() {
        if($this->connected()) {
            pg_close($this->conn);
            $this->connected = false;
        }
    }

    /**
     * Begin a new transaction
     *
     * @return PGDB_ResultSet
     * @throws DB_Exception
     */
    public function begin() {
        return $this->query("BEGIN");
    }

    /**
     * Commit the current transaction
     *
     * @return PGDB_ResultSet
     * @throws DB_Exception
     */
    public function commit() {
        return $this->query("COMMIT");
    }

    /**
     * Createa savepoint within a transaction
     *
     * @param $savepoint
     *
     * @return PGDB_ResultSet
     * @throws DB_Exception
     */
    public function savepoint($savepoint) {
        return $this->query("SAVEPOINT $1",array($savepoint));
    }

    /**
     * Rollback to a savepoint
     *
     * @param $savepoint
     *
     * @return PGDB_ResultSet
     * @throws DB_Exception
     */
    public function rollbackTo($savepoint) {
        return $this->query("ROLLBACK TO $1",array($savepoint));
    }

    /**
     * Prepare a statement for use in execute
     *
     * @param $stmtname
     * @param $query
     *
     * @return bool|resource
     * @throws DB_Exception
     */
    public function prepare($stmtname,$query) {
        if($this->connected()) {
            $result = pg_prepare($this->conn,$stmtname,$query);
            if($error = $this->lastError()) {
                throw new DB_Exception($error);
            } else {
                return $result;
            }
        }
        return false;
    }

    /**
     * Execute a prepared statement
     *
     * @param $stmtname
     * @param array $params
     *
     * @return PGDB_ResultSet
     * @throws DB_Exception
     */
    public function execute($stmtname,$params=array()) {
        $params = $this->fixBoolParams($params);
        if($this->connected()) {
            $result = PGDB_ResultSet::n(pg_execute($this->conn,$stmtname,$params));
            if($error = $this->lastError()) {
                throw new DB_Exception($error);
            } else {
                return $result;
            }
        }
        return false;
    }

    /**
     * Run a query, optionally passing params.
     *
     * If you pass params, pg_query_params is run, otherwise pg_query is called.
     *
     * @param $query
     * @param array $params
     *
     * @return PGDB_ResultSet
     * @throws DB_Exception
     */
    public function query($query,$params=array()) {
        if($this->connected()) {
            if(empty($params)) {
                $result = PGDB_ResultSet::n(pg_query($this->conn,$query));
                if($error = $this->lastError()) {
                    throw new DB_Exception($error);
                } else {
                    return $result;
                }
            } else {
                $params = $this->fixBoolParams($params);
                $result = PGDB_ResultSet::n(pg_query_params($this->conn,$query,$params));
                if($error = $this->lastError()) {
                    throw new DB_Exception($error);
                } else {
                    return $result;
                }
            }
        }
        return false;
    }

    /**
     * Get the last error on this connection. FALSE if no error
     *
     * @return bool|string
     */
    public function lastError() {
        if($this->connected()) {
            return pg_last_error($this->conn);
        }
        return false;
    }

    /**
     * Get the last notice on this connection. FALSE if no notice
     *
     * @return bool|string
     */
    public function lastNotice() {
        if($this->connected()) {
            return pg_last_notice($this->conn);
        }
        return false;
    }

    /**
     *
     * @param $params
     *
     * @return mixed
     */
    private function fixBoolParams($params) {
        for($i = 0; $i < count($params); $i++) {
            if(gettype($params[$i]) == PHPTypes::_boolean) {
                $params[$i] = $params[$i] ? 1 : 0;
            }
        }
        return $params;
    }
}