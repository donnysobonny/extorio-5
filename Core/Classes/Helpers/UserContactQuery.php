<?php
namespace Core\Classes\Helpers;
class UserContactQuery {
    public $type;
    public $topic;
    public $identifier;
    public $date;
    public $time;
    public $count;

    /**
     * Lazy construct
     *
     * @return UserContactQuery
     */
    public static function n() {
        return new UserContactQuery();
    }

    /**
     * Construct from array
     *
     * @param array $array
     *
     * @return UserContactQuery
     */
    public static function constructFromArray($array) {
        $o = UserContactQuery::n();
        foreach($o as $k => $v) {
            if(isset($array[$k])) {
                $o->$k = $array[$k];
            }
        }
        return $o;
    }
}