<?php
namespace Core\Classes\Helpers;

/**
 * This is used to convert an array to a usable set of layout objects, used in the layout system
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class LayoutSystem
 */
class LayoutSystem {
    /**
     * The rows that make up the layout. The layout itself is unlikely to ever have more than one row
     *
     * @var LayoutSystem_Row[]
     */
    public $rows = array();

    /**
     * Lazily construct a new layout
     *
     * @return LayoutSystem
     */
    public static function n() {
        return new LayoutSystem();
    }

    /**
     * Quickly construct, passing in your rows
     *
     * @param $rows
     *
     * @return LayoutSystem
     */
    public static function quickConstruct($rows) {
        $ls = LayoutSystem::n();
        $ls->rows = $rows;

        return $ls;
    }

    /**
     * Construct a layout from an array
     *
     * @param $array
     *
     * @return LayoutSystem
     */
    public static function constructFromArray($array) {
        $ls = LayoutSystem::n();
        if(isset($array["rows"])) {
            $ls->rows = $array["rows"];
        }

        //construct the rows
        for($i = 0; $i < count($ls->rows); $i++) {
            $ls->rows[$i] = LayoutSystem_Row::constructFromArray($ls->rows[$i]);
        }

        return $ls;
    }

    /**
     * Display layout elements as html
     *
     * @param LayoutSystem $layout
     * @param string $content
     * @param string $contentId
     * @param bool $editMode
     *
     * @return string
     */
    public static function display($layout, $content, $contentId, $editMode = false) {
        return $layout->__toHtml($content,$contentId,$editMode);
    }

    /**
     * Get the default layout
     *
     * @return LayoutSystem
     */
    public static function getDefault() {
        return LayoutSystem::quickConstruct(array(
            LayoutSystem_Row::quickConstruct(array(
                LayoutSystem_Cell::quickConstruct("cell",array(
                    LayoutSystem_Row::quickConstruct(array(
                        LayoutSystem_Cell::quickConstruct("content",array(

                        ),0,12)
                    ))
                ),0,12)
            ))
        ));
    }

    /**
     * Get the layout as html
     *
     * @param $content
     * @param $contentId
     * @param bool $editMode
     *
     * @return string
     */
    public function __toHtml($content, $contentId, $editMode = false) {
        $html = "";
        foreach($this->rows as $row) {
            $html .= $row->__toHtml($content,$contentId,$editMode);
        }
        return $html;
    }

    /**
     * Get the layout as an associative array
     *
     * @return mixed
     */
    public function __toArray() {
        return json_decode(json_encode($this,JSON_NUMERIC_CHECK),true);
    }
}