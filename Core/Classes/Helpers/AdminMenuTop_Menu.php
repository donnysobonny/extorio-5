<?php
namespace Core\Classes\Helpers;
/**
 * Used to create a menu in the top admin menu
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class AdminMenuTop_Menu
 */
class AdminMenuTop_Menu {
    /**
     * The name of the menu
     *
     * @var string
     */
    public $name = "";
    /**
     * The menu label
     *
     * @var string
     */
    public $label = "";
    /**
     * The menu description
     *
     * @var string
     */
    public $description = "";
    /**
     * The icon class
     *
     * @var string
     */
    public $icon_class = "";
    /**
     * The url
     *
     * @var string
     */
    public $url = "";
    /**
     * The url that makes this menu active
     *
     * @var string
     */
    public $active_url = "";
    /**
     * How to align the menu
     *
     * @var string
     */
    public $align = "left";
    /**
     * Whether or not to open this in a new window
     *
     * @var bool
     */
    public $new_window = false;
    /**
     * The minimum required access level to display the menu
     *
     * @var int
     */
    public $access_level = 0;

    /**
     * Lazily construct a new instance
     *
     * @param $name
     * @param $label
     * @param $description
     * @param string $url
     * @param string $active_url
     * @param string $icon_class
     * @param string $align
     * @param bool $new_window
     *
     * @return AdminMenuTop_Menu
     */
    public static function n($name,$label,$description,$url="",$active_url="",$icon_class="",$align="left",$new_window=false,$access_level=0) {
        $m = new AdminMenuTop_Menu();
        $m->name = $name;
        $m->label = $label;
        $m->description = $description;
        $m->url = $url;
        $m->active_url = $active_url;
        $m->align = $align;
        $m->new_window = $new_window;
        $m->icon_class = $icon_class;
        $m->access_level = $access_level;
        return $m;
    }

    /**
     * Construct a new instance from an array
     *
     * @param $array
     *
     * @return AdminMenuTop_Menu
     */
    public static function constructFromArray($array) {
        $m = new AdminMenuTop_Menu();
        foreach($m as $key => $value) {
            if(isset($array[$key])) {
                $m->$key = $array[$key];
            }
        }
        return $m;
    }
}