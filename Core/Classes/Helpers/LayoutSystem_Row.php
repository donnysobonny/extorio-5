<?php
namespace Core\Classes\Helpers;
/**
 * A row is used to contain cells
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class LayoutSystem_Row
 */
class LayoutSystem_Row {
    /**
     * The role of the row, is always "row"
     *
     * @var
     */
    public $role;
    /**
     * Classes to be assigned to the row
     *
     * @var string
     */
    public $classes = "";
    /**
     * Styling to be applied to the row
     *
     * @var string
     */
    public $styling = "";

    /**
     * Whether the row should collapse cells on xs screens
     *
     * @var bool
     */
    public $collapsexs = false;
    /**
     * Whether the row should collapse cells on sm screens
     *
     * @var bool
     */
    public $collapsesm = false;
    /**
     * Whether the row should collapse cells on md screens
     *
     * @var bool
     */
    public $collapsemd = false;
    /**
     * Whether the row should collapse cells on lg screens
     *
     * @var bool
     */
    public $collapselg = false;
    /**
     * Whether the row should collapse cells on all screens
     *
     * @var bool
     */
    public $collapseall = false;
    /**
     * How to align the cells
     *
     * @var string
     */
    public $align = "top";
    /**
     * Whether or not to use gutter width on cells in the row
     *
     * @var bool
     */
    public $spacing = true;
    /**
     * The cells contained in the row
     *
     * @var LayoutSystem_Cell[]
     */
    public $cells = array();

    /**
     * Lazily construct a new row
     *
     * @return LayoutSystem_Row
     */
    public static function n() {
        return new LayoutSystem_Row();
    }

    /**
     * Quickly construct a row
     *
     * @param array $cells
     * @param bool $collapsexs
     * @param bool $collapsesm
     * @param bool $collapsemd
     * @param bool $collapselg
     * @param bool $collapseall
     * @param string $align
     * @param bool $spacing
     * @param string $classes
     * @param string $styling
     *
     * @return LayoutSystem_Row
     */
    public static function quickConstruct(
        $cells = array(),
        $collapsexs = false,
        $collapsesm = false,
        $collapsemd = false,
        $collapselg = false,
        $collapseall = false,
        $align = "top",
        $spacing = true,
        $classes = "",
        $styling = ""
    ) {
        $r = LayoutSystem_Row::n();
        $r->role = "row";
        $r->cells = $cells;
        $r->collapsexs = $collapsexs;
        $r->collapsesm = $collapsesm;
        $r->collapsemd = $collapsemd;
        $r->collapselg = $collapselg;
        $r->collapseall = $collapseall;
        $r->align = $align;
        $r->spacing = $spacing;
        $r->classes = $classes;
        $r->styling = $styling;

        return $r;
    }

    /**
     * Construct a row from an array
     *
     * @param $array
     *
     * @return LayoutSystem_Row
     */
    public static function constructFromArray($array) {
        $r = LayoutSystem_Row::n();
        foreach($r as $key => $value) {
            if(isset($array[$key])) {
                $r->$key = $array[$key];
            }
        }

        //construct cells
        for($i = 0; $i < count($r->cells); $i++) {
            $r->cells[$i] = LayoutSystem_Cell::constructFromArray($r->cells[$i]);
        }

        return $r;
    }

    /**
     * Get the row to an associative array
     *
     * @return mixed
     */
    public function __toArray() {
        return json_decode(json_encode($this,JSON_NUMERIC_CHECK),true);
    }

    /**
     * Get the row as html
     *
     * @param $content
     * @param $contentid
     * @param bool $editMode
     *
     * @return string
     */
    public function __toHtml($content, $contentid, $editMode = false) {
        $editable = $editMode ? "true" : "false";
        ob_start();
        ?>
<div
    data-ls-align="<?= $this->align ?>"
    data-ls-collapsexs="<?php if ($this->collapsexs)
        echo "true"; else echo "false"; ?>"
    data-ls-collapsesm="<?php if ($this->collapsesm)
        echo "true"; else echo "false"; ?>"
    data-ls-collapsemd="<?php if ($this->collapsemd)
        echo "true"; else echo "false"; ?>"
    data-ls-collapselg="<?php if ($this->collapselg)
        echo "true"; else echo "false"; ?>"
    data-ls-collapseall="<?php if ($this->collapseall)
        echo "true"; else echo "false"; ?>"
    data-ls-spacing="<?php if ($this->spacing)
        echo "true"; else echo "false" ?>"
    data-ls-editable="<?= $editable ?>"
    data-ls-role="row"
    data-ls-classes="<?=$this->classes?>"
    data-ls-styling="<?=$this->styling?>"
    class="layout_row">
    <?php
    foreach ($this->cells as $cell) {
        echo $cell->__toHtml($content, $contentid, $editMode);
    }
    ?>
</div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}