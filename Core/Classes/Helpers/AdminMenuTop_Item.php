<?php
namespace Core\Classes\Helpers;
/**
 * Used to create a menu item that goes in the top admin menu
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class AdminMenuTop_Item
 */
class AdminMenuTop_Item {
    /**
     * The label
     *
     * @var string
     */
    public $label = "";
    /**
     * The description
     *
     * @var string
     */
    public $description = "";
    /**
     * The icon class
     *
     * @var string
     */
    public $icon_class = "";
    /**
     * The url
     *
     * @var string
     */
    public $url = "";
    /**
     * The url that makes the item active
     *
     * @var string
     */
    public $active_url = "";
    /**
     * Whether to open in a new window
     *
     * @var bool
     */
    public $new_window = false;

    /**
     * The minimum user access level required to display the item
     *
     * @var int
     */
    public $access_level = 0;

    /**
     * Lazily construct a new instance
     *
     * @param $label
     * @param $description
     * @param $url
     * @param $active_url
     * @param string $icon_class
     * @param bool $new_window
     *
     * @return AdminMenuTop_Item
     */
    public static function n($label,$description,$url,$active_url,$icon_class="",$new_window=false,$access_level=0) {
        $i = new AdminMenuTop_Item();
        $i->label = $label;
        $i->description = $description;
        $i->url = $url;
        $i->active_url = $active_url;
        $i->icon_class = $icon_class;
        $i->new_window = $new_window;
        $i->access_level = $access_level;
        return $i;
    }

    /**
     * Construct an instance from an array
     *
     * @param $array
     *
     * @return AdminMenuTop_Item
     */
    public static function constructFromArray($array) {
        $i = new AdminMenuTop_Item();
        foreach($i as $key => $value) {
            if(isset($array[$key])) {
                $i->$key = $array[$key];
            }
        }
        return $i;
    }
}