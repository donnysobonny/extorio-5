<?php
namespace Core\Classes\Helpers;
use Core\Classes\Commons\Extorio;
use Core\Classes\Exceptions\General_Exception;
use Core\Classes\Utilities\Strings;

/**
 * This helper class is extremely useful in creating safe urls, and extracting parts from a url
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Url
 */
class Url {
    /**
     * The url
     *
     * @var string
     */
    private $url;
    /**
     * The uri
     *
     * @var string
     */
    private $uri;
    /**
     * The query string
     *
     * @var string
     */
    private $queryString;
    /**
     * The query params
     *
     * @var array
     */
    private $queryParams = array();

    /**
     * @param $uri
     *
     * @throws General_Exception
     */
    function __construct($uri) {
        if(!strlen($uri)) {
            throw new General_Exception("Cannot create a Url instance with an empty uri");
        }
        $this->extractComponents($uri);
    }

    /**
     * Get the url to string (returns the uri)
     *
     * @return mixed
     */
    function __toString() {
        return $this->uri;
    }

    /**
     * Lazily construct an instance of this class
     *
     * @param $uri
     *
     * @return Url
     */
    public static function n($uri) {
        return new Url($uri);
    }

    /**
     * Redirect to the url
     *
     * @param int $statusCode
     */
    public function redirect($statusCode=302) {
        //if the url does not start "http", make sure it starts "/"
        if(substr($this->uri,0,4) != "http") {
            header("Location: ".$this->uri,null,intval($statusCode));
            exit;
        } else {
            header("Location: /".$this->uri,null,intval($statusCode));
            exit;
        }
    }

    /**
     * Get the url fields decoded into an array
     *
     * @return array
     */
    public function getFields() {
        $fieldsRaw = explode("/",$this->url);
        $fields = array();
        foreach($fieldsRaw as $field) {
            if(strlen($field)) {
                $fields[] = urldecode($field);
            }
        }
        return $fields;
    }

    /**
     * Get a field by it's index position
     *
     * @param $index
     *
     * @return mixed
     */
    public function getField($index) {
        $fields = $this->getFields();
        if(isset($fields[$index])) {
            return $fields[$index];
        } else {
            return null;
        }
    }

    /**
     * Add a field to the url
     *
     * @param string $field
     */
    public function addField($field) {
        $this->setUrl($this->url .= urlencode($field)."/");
    }

    /**
     * Add a set of fields to the url
     *
     * @param array $fields
     */
    public function addFields($fields) {
        $url = $this->getUrl(true);
        foreach($fields as $field) {
            $url .= urlencode($field)."/";
        }
        $this->setUrl($url);
    }

    /**
     * Get the url part
     *
     * @return string
     */
    public function getUrl($decoded = false) {
        if($decoded) {
            return urldecode($this->url);
        } else {
            return $this->url;
        }
    }

    /**
     * Set the url part
     *
     * @param string $url
     */
    public function setUrl($url) {
        //just incase the url was already encoded
        $this->url = urldecode($url);
        $this->url = Strings::startsAndEndsWith(Strings::urlSafe($this->url),"/");
        $this->updateUri();
    }

    /**
     * Get the uri part
     *
     * @return string
     */
    public function getUri() {
        return $this->uri;
    }

    /**
     * Set the uri part
     *
     * @param string $uri
     */
    public function setUri($uri) {
        $this->extractComponents($uri);
    }

    /**
     * Get the query string part
     *
     * @return string
     */
    public function getQueryString() {
        return $this->queryString;
    }

    /**
     * Set the query string part
     *
     * @param string $queryString
     */
    public function setQueryString($queryString) {
        if(strlen($queryString)) {
            parse_str($queryString,$this->queryParams);
            $this->queryString = http_build_query($this->queryParams);
        } else {
            $this->queryString = "";
            $this->queryParams = array();
        }
        $this->updateUri();
    }

    /**
     * Get the query params
     *
     * @return array
     */
    public function getQueryParams() {
        return $this->queryParams;
    }

    /**
     * Set the query params
     *
     * @param array $queryParams
     */
    public function setQueryParams($queryParams) {
        if(is_array($queryParams)) {
            $this->queryParams = $queryParams;
            $this->queryString = http_build_query($this->queryParams);
        } else {
            $this->queryParams = array();
            $this->queryString = "";
        }
        $this->updateUri();
    }

    /**
     * Extract the parts of the url
     *
     * @param string $uri
     */
    private function extractComponents($uri) {
        //if there is a "?", we have a query string
        $qmarkPos = strpos($uri,"?");
        $queryStringRaw = "";
        if($qmarkPos !== false) {
            //strip out the urlRaw and queryStringRaw
            $urlRaw = substr($uri,0,$qmarkPos);
            $queryStringRaw = substr($uri,$qmarkPos+1,strlen($uri) - ($qmarkPos - 1));
        } else {
            $urlRaw = $uri;
        }

        $this->setUrl($urlRaw);
        $this->setQueryString($queryStringRaw);
        $this->updateUri();
    }

    /**
     * Used to update the uri
     */
    private function updateUri() {
        $this->uri = $this->url;
        if(strlen($this->queryString)) {
            $this->uri .= "?". $this->queryString;
        }
    }
}