<?php
namespace Core\Classes\Helpers;
/**
 * Used to construct a menu that goes into the admin left menu
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class AdminMenuLeft_Menu
 */
class AdminMenuLeft_Menu {
    /**
     * The name of the manu
     *
     * @var string
     */
    public $name = "";
    /**
     * A label for the menu
     *
     * @var string
     */
    public $label = "";
    /**
     * The menu description
     *
     * @var string
     */
    public $description = "";
    /**
     * The icon class for the menu
     *
     * @var string
     */
    public $icon = "circle";

    /**
     * Lazily construct a new instance
     *
     * @param $name
     * @param $label
     * @param $description
     * @param string $icon
     *
     * @return AdminMenuLeft_Menu
     */
    public static function n($name, $label, $description, $icon = "fa fa-circle") {
        $m = new AdminMenuLeft_Menu();
        $m->name = $name;
        $m->label = $label;
        $m->description = $description;
        $m->icon = $icon;
        return $m;
    }

    /**
     * Construct an instance from an array
     *
     * @param $array
     *
     * @return AdminMenuLeft_Menu
     */
    public static function constructFromArray($array) {
        $m = new AdminMenuLeft_Menu();
        foreach ($m as $key => $value) {
            if (isset($array[$key])) {
                $m->$key = $array[$key];
            }
        }
        return $m;
    }
}