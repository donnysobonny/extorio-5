<?php
namespace Core\Classes\Models;
use Core\Classes\Commons\Extorio;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\Strings;

class Task extends B_Task {

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $description
     * @param bool $allRead
     * @param int[]|UserGroup[] $readGroups
     * @param string $extensionName
     *
     * @return Task
     */
    public static function q(
        $name,
        $description,
        $allRead = false,
        $readGroups = array(1,2),
        $extensionName = "Application"
    ) {
        $o = Task::n();
        $o->name = $name;
        $o->description = $description;
        $o->allRead = $allRead;
        $o->readGroups = $readGroups;
        $o->extensionName = $extensionName;

        return $o;
    }

    /**
     * Find by name
     *
     * @param string $name
     *
     * @return Task
     */
    public static function findByName($name) {
        return Task::findOne(Query::n()->where(array("name"=>$name)));
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    private function generalChecks() {
        if(!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }
        if(!strlen($this->name)) {
            throw new \Exception("A task cannot exist without a name");
        }
        $this->label = Strings::labelSafe($this->name);
        if(Task::findOne(
            Query::n()
                ->where(array(
                    "label" => $this->label,
                    "id" => array('$ne'=>$this->id)
                )),1
        )) {
            throw new \Exception("A task already exists with the label ".$this->label);
        }
        if(!strlen($this->class)) {
            $this->class = $this->name;
        }
        $this->class = Strings::classNameSafe($this->class);

        $this->namespace = '\\'.$this->extensionName.'\Components\Tasks\\'.$this->class;
    }

    protected function afterCreate() {
        $this->createComponentFile();
    }

    private function createComponentFile() {
        $extension = Extorio::get()->getExtension($this->extensionName);
        if($extension) {
            if(!$extension->_findClassFile($this->namespace)) {
                //create the file
                $file = File::createFromPath($extension->_absoluteRoot.'/Components/Tasks/'.$this->class.".php");
                $template = File::constructFromPath("Core/Assets/file-templates/Task_template.txt");
                $content = $template->read();
                $content = str_replace("*|DESCRIPTION|*",$this->description,$content);
                $content = str_replace("*|CLASS_NAME|*",$this->class,$content);
                $content = str_replace("*|EXTENSION_NAME|*",$this->extensionName,$content);
                $file->write($content);
            }
        }
    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
        $this->class = $this->_old->class;
        $this->namespace = $this->_old->namespace;
    }

    protected function afterUpdate() {
        $this->createComponentFile();
    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }
}