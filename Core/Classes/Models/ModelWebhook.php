<?php
namespace Core\Classes\Models;

use Core\Classes\Commons\Extorio;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Enums\ModelWebhookActions;

class ModelWebhook extends B_ModelWebhook {

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $action
     * @param string $modelNamespace
     * @param string $location
     * @param string $verificationKey
     * @param string $extensionName
     *
     * @return $this
     */
    public static function q($name, $action, $modelNamespace, $location, $verificationKey, $extensionName = "Application") {
        $o = ModelWebhook::n();
        $o->name = $name;
        $o->action = $action;
        $o->modelNamespace = $modelNamespace;
        $o->location = $location;
        $o->verificationKey = $verificationKey;
        $o->extensionName = $extensionName;

        return $o;
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
    }

    private function generalChecks() {
        //location must be valid
        if (!strlen($this->location)) {
            throw new \Exception("Location cannot be empty");
        }

        //make sure starts with http:// or https://
        if (substr($this->location, 0, 7) != "http://" && substr($this->location, 0, 8) != "https://") {
            throw new \Exception("Location must start http:// or https://");
        }

        if (!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }

        if (!strlen($this->action)) {
            $this->action = ModelWebhookActions::defaultValue();
        }

        if (!strlen($this->verificationKey)) {
            //use default
            $config = Extorio::get()->getConfig();
            $this->verificationKey = $config["models"]["webhooks"]["default_verification_key"];
        }
    }
}