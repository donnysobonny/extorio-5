<?php
namespace Core\Classes\Models;

use Core\Classes\Commons\Extorio;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Helpers\Query;

class Extension extends B_Extension {

    /**
     * Quick construct
     *
     * @param string $name
     * @param bool $isInstalled
     *
     * @return Extension
     */
    public static function q(
        $name,
        $isInstalled = false
    ) {
        $o = Extension::n();
        $o->name = $name;
        $o->isInstalled = $isInstalled;

        return $o;
    }

    public static function findByName($name) {
        return Extension::findOne(
            Query::n()
                ->where(array("name"=>$name))
        );
    }

    /**
     * Get the extension component associated with this extension
     *
     * @return \Core\Classes\Commons\Extension
     */
    public function getExtensionComponent() {
        return Extorio::get()->constructExtension($this->namespace);
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {
        if($this->isInternal) {
            throw new \Exception("An internal extension cannot be deleted");
        }
    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->name = $this->_old->name;
        $this->namespace = $this->_old->namespace;
        $this->isInternal = $this->_old->isInternal;
    }

    private function generalChecks() {
        if(!strlen($this->name)) {
            throw new \Exception("An extension cannot exist without a name");
        }

        if($this->isInternal) {
            $this->isInstalled = true;
        }
    }
}