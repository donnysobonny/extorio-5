<?php
namespace Core\Classes\Models;
use Core\Classes\Commons\Extorio;
use Core\Classes\Enums\UserContactType;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\Emails;

/**
 * This model represents an email transport that can be reviewed and re-sent
 *
 * Class EmailTransport
 */
class EmailTransport extends B_EmailTransport {

    /**
     * Quck construct
     *
     * @param string $subject
     * @param string $body
     * @param array $mergeVars
     * @param null|int|EmailTemplate $template
     * @param string $toName
     * @param string $toEmail
     * @param array $cc
     * @param array $bcc
     * @param null|string $fromName
     * @param null|string $fromEmail
     *
     * @return EmailTransport
     */
    public static function q(
        $subject,
        $body,
        $mergeVars = array(),
        $template = null,
        $toName,
        $toEmail,
        $cc = array(),
        $bcc = array(),
        $fromName = null,
        $fromEmail = null
    ) {
        $o = EmailTransport::n();
        $o->subject = $subject;
        $o->body = $body;
        $o->mergeVars = $mergeVars;
        $o->template = $template;
        $o->toName = $toName;
        $o->toEmail = $toEmail;
        $o->cc = $cc;
        $o->bcc = $bcc;
        $o->fromName = $fromName;
        $o->fromEmail = $fromEmail;

        return $o;
    }

    /**
     * Quick create
     *
     * @param string $subject
     * @param string $body
     * @param array $mergeVars
     * @param null|int|EmailTemplate $template
     * @param string $toName
     * @param string $toEmail
     * @param array $cc
     * @param array $bcc
     * @param null|string $fromName
     * @param null|string $fromEmail
     *
     * @return EmailTransport
     */
    public static function qc(
        $subject,
        $body,
        $mergeVars = array(),
        $template = null,
        $toName,
        $toEmail,
        $cc = array(),
        $bcc = array(),
        $fromName = null,
        $fromEmail = null
    ) {
        $o = EmailTransport::q($subject,$body,$mergeVars,$template,$toName,$toEmail,$cc,$bcc,$fromName,$fromEmail);
        $o->pushThis();

        return $o;
    }

    /**
     * Quick send
     *
     * @param string $subject
     * @param string $body
     * @param array $mergeVars
     * @param null|int|EmailTemplate $template
     * @param string $toName
     * @param string $toEmail
     * @param array $cc
     * @param array $bcc
     * @param null|string $fromName
     * @param null|string $fromEmail
     *
     * @return EmailTransport
     */
    public static function qs(
        $subject,
        $body,
        $mergeVars = array(),
        $template = null,
        $toName,
        $toEmail,
        $cc = array(),
        $bcc = array(),
        $fromName = null,
        $fromEmail = null
    ) {
        $o = EmailTransport::qc($subject,$body,$mergeVars,$template,$toName,$toEmail,$cc,$bcc,$fromName,$fromEmail);
        $o->send();

        return $o;
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->dateCreated = date('Y-m-d H:i:s');
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {

    }

    private function generalChecks() {
        //subject cannot be empty
        if(!strlen($this->subject) && is_null($this->template)) {
            throw new \Exception("Email transport must have a subject if not using a template");
        }
        //must have a toname and toemail
        if(!strlen($this->toName)) {
            throw new \Exception("Email transport must specify the 'toName'");
        }
        if(!strlen($this->toEmail)) {
            throw new \Exception("Email transport must speficy the 'toEmail'");
        }
        if($this->template) {
            if(!strlen($this->subject)) {
                $this->subject = $this->template->subject;
            }
            if(!strlen($this->body)) {
                $this->body = $this->template->body;
            }
        }
    }

    /**
     * Send the email transport. Updates the object internally, storing any error messages and updating the numSent
     *
     */
    public function send() {
        $config = Extorio::get()->getConfig();
        //catch any errors
        $this->errorMessage = "";
        try {
            //create the transport
            switch($config["emails"]["transport"]) {
                case "mail" :
                    $transport = \Swift_MailTransport::newInstance();
                    break;
                case "sendmail" :
                    $transport = \Swift_SendmailTransport::newInstance();
                    break;
                case "smtp" :
                    $transport = \Swift_SmtpTransport::newInstance($config["emails"]["smtp"]["host"],$config["emails"]["smtp"]["port"])
                        ->setUsername($config["emails"]["smtp"]["username"])
                        ->setPassword($config["emails"]["smtp"]["password"])
                        ;
                    break;
                default :
                    $transport = \Swift_MailTransport::newInstance();
                    break;
            }

            //create the mailer from the transport
            $mailer = \Swift_Mailer::newInstance($transport);

            //and finally the message
            $message = \Swift_Message::newInstance($this->subject);
            $message->setBody($this->fetchBodyMerged(),"text/html");

            $fromName = "";
            if(strlen($this->fromEmail)) {
                $fromEmail = $this->fromEmail;
                if(!strlen($this->fromName)) {
                    $fromName = $this->fromEmail;
                }
            } else {
                $fromEmail = $config["emails"]["defaults"]["from_email"];
                $fromName = $config["emails"]["defaults"]["from_name"];
            }

            $this->fromEmail = $fromEmail;
            $this->fromName = $fromName;

            $message->setFrom(array($fromEmail => $fromName));

            $message->setTo(array($this->toEmail => $this->toName));

            $message->setCc($this->cc);
            $message->setBcc($this->bcc);

            $int = $mailer->send($message);
            if($int > 0) {
                $user = User::findOne(Query::n()->where(array("email" => $this->toEmail)));
                if($user) {
                    $uc = UserContact::n();
                    $uc->userId = $user->id;
                    $uc->type = UserContactType::_systemEmail;
                    $uc->identifier = $this->subject;
                    $uc->pushThis();
                }
            }
            $this->numSent += $int;

            $this->dateSent = date('Y-m-d H:i:s');

        } catch(\Exception $ex) {
            $this->errorMessage = $ex->getMessage();
        }
        //update the object
        $this->pushThis();
    }

    /**
     * Merge the mergeVars with the body of the transport
     *
     * @return string
     */
    public function fetchBodyMerged() {
        return Emails::mergeBodyWithVars($this->body,$this->mergeVars);
    }
}