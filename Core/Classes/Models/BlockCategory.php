<?php
namespace Core\Classes\Models;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\Blocks;

/**
 * 
 *
 * Class BlockCategory
 */
class BlockCategory extends B_BlockCategory {

    /**
     * Quick construct
     *
     * @param string $name
     * @param int $parentId
     * @param string $description
     * @param string $icon
     * @param string $extensionName
     *
     * @return BlockCategory
     */
    public static function q(
        $name,
        $parentId = 0,
        $description,
        $icon,
        $extensionName = "Application"
    ) {
        $o = BlockCategory::n();
        $o->name = $name;
        $o->parentId = $parentId;
        $o->description = $description;
        $o->icon = $icon;
        $o->extensionName = $extensionName;

        return $o;
    }

    /**
     * Find by name
     *
     * @param string $name
     *
     * @return BlockCategory
     */
    public static function findByName($name) {
        return BlockCategory::findOne(Query::n()->where(array("name"=>$name)));
    }

    /**
     * @var BlockCategory[]
     */
    public $subCategories = array();

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {
        //cannot remove a block category that is in use
        if(Blocks::getBlockCountByCategory($this->id) > 0) {
            throw new \Exception("Cannot remove a block category that is being used by a block");
        }
        $this->fetchSubCategories();
        if(count($this->subCategories) > 0) {
            throw new \Exception("Cannot remove a block category that contains sub categories");
        }
    }

    protected function afterDelete() {

    }

    public function fetchSubCategories() {
        $this->subCategories = BlockCategory::findAll(
            Query::n()
                ->where(array(
                    "parentId" => $this->id
                ))
                ->order("name")
        );
    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
    }

    private function generalChecks() {
        if(!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }
        if(!strlen($this->name)) {
            throw new \Exception("A block category must have a name");
        }
        if($this->parentId > 0) {
            $p = BlockCategory::findById($this->parentId);
            if($p) {
                if($p->parentId > 0) {
                    throw new \Exception("A sub category must be the child of a main category, not another sub category.");
                }
            }
        }
    }
}