<?php
namespace Core\Classes\Models;
/**
 * 
 *
 * Class UserAction
 */
class UserAction extends B_UserAction {

    /**
     * Quick construct
     *
     * @param int $userId
     * @param int|UserActionType $type
     * @param string $date
     * @param string $time
     * @param string $detail1
     * @param string $detail2
     * @param string $detail3
     *
     * @return $this
     */
    public static function q(
        $userId,
        $type,
        $date = null,
        $time = null,
        $detail1 = "",
        $detail2 = "",
        $detail3 = ""
    ) {
        $ua = UserAction::n();
        $ua->userId = $userId;
        $ua->type = $type;
        $ua->date = $date;
        $ua->time = $time;
        $ua->detail1 = $detail1;
        $ua->detail2 = $detail2;
        $ua->detail3 = $detail3;

        return $ua;
    }

    /**
     * Quick create
     *
     * @param int $userId
     * @param int|UserActionType $type
     * @param string $date
     * @param string $time
     * @param string $detail1
     * @param string $detail2
     * @param string $detail3
     *
     * @return UserAction
     */
    public static function qc(
        $userId,
        $type,
        $date = null,
        $time = null,
        $detail1 = "",
        $detail2 = "",
        $detail3 = ""
    ) {
        $q = UserAction::q($userId,$type,$date,$time,$detail1,$detail2,$detail3);
        $q->pushThis();
        return $q;
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->userId = $this->_old->userId;
        $this->type = $this->_old->type;
    }

    private function generalChecks() {
        if(!strlen($this->userId) || $this->userId <= 0) {
            throw new \Exception("An action must have a userId");
        }
        if(!$this->type) {
            throw new \Exception("An action must have an action type");
        }
        if(!strlen($this->date)) {
            $this->date = date("Y-m-d");
        }
        if(!strlen($this->time)) {
            $this->time = date("H:i:s");
        }
    }
}