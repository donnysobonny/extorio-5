<?php
namespace Core\Classes\Models;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Helpers\Query;

/**
 * 
 *
 * Class UserActionType
 */
class UserActionType extends B_UserActionType {

    public $filters = array();

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $description
     * @param string $detail1Description
     * @param string $detail2Description
     * @param string $detail3Description
     * @param string $extensionName
     *
     * @return UserActionType
     */
    public static function q(
        $name,
        $description,
        $detail1Description,
        $detail2Description,
        $detail3Description,
        $extensionName = "Application"
    ) {
        $o = UserActionType::n();
        $o->name = $name;
        $o->description = $description;
        $o->detail1Description = $detail1Description;
        $o->detail2Description = $detail2Description;
        $o->detail3Description = $detail3Description;
        $o->extensionName = $extensionName;

        return $o;
    }

    /**
     * Find a user action type by name
     *
     * @param string $name
     *
     * @return UserActionType
     */
    public static function findByName($name) {
        return UserActionType::findOne(Query::n()->where(array("name" => $name)));
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
    }

    private function generalChecks() {
        if(!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }
        if(!strlen($this->name)) {
            throw new \Exception("An action type must have a name");
        }
        if(UserActionType::existsOne(
            Query::n()
                ->where(array(
                    "name" => $this->name,
                    "id" => array(Query::_ne => $this->id)
                ))
        )) {
            throw new \Exception("An action type with the name '".$this->name."' already exists");
        }
    }
}