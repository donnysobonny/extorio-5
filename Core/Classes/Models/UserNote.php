<?php
namespace Core\Classes\Models;
use Core\Classes\Commons\Extorio;

/**
 * 
 *
 * Class UserNote
 */
class UserNote extends B_UserNote {

    /**
     * Quick construct
     *
     * @param int $userId
     * @param string $body
     *
     * @return UserNote
     */
    public static function q(
        $userId,
        $body
    ) {
        $o = UserNote::n();
        $o->userId = $userId;
        $o->body = $body;

        return $o;
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();

        $loggedInUser = Extorio::get()->getLoggedInUser();
        if($loggedInUser) {
            $this->updatedById = $loggedInUser->id;
        }
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();

        if($this->body != $this->_old->body) {
            $loggedInUser = Extorio::get()->getLoggedInUser();
            if($loggedInUser) {
                $this->updatedById = $loggedInUser->id;
            }
        }
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->userId = $this->_old->userId;
    }

    private function generalChecks() {
        if(!strlen($this->userId) || $this->userId <= 0) {
            throw new \Exception("A user note must have a userId");
        }

        $this->dateUpdated = date('Y-m-d H:i:s');
    }
}