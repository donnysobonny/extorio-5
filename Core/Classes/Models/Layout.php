<?php
namespace Core\Classes\Models;
use Core\Classes\Commons\Extorio;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Helpers\LayoutSystem;
use Core\Classes\Helpers\Query;

class Layout extends B_Layout {

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $description
     * @param array $layout
     * @param string $extensionName
     *
     * @return Layout
     */
    public static function q(
        $name,
        $description,
        $layout = array(),
        $extensionName = "Application"
    ) {
        $o = Layout::n();
        $o->name = $name;
        $o->description = $description;
        $o->layout = $layout;
        $o->extensionName = $extensionName;

        return $o;
    }

    /**
     * Find by name
     *
     * @param string $name
     *
     * @return Layout
     */
    public static function findByName($name) {
        return Layout::findOne(Query::n()->where(array("name"=>$name)));
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
    }

    private function generalChecks() {
        if(!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }
        if(!strlen($this->name)) {
            throw new \Exception("A layout cannot exist without a name");
        }
        if(empty($this->layout)) {
            $this->layout = LayoutSystem::getDefault()->__toArray();
        }
        if(Layout::findOne(
            Query::n()
                ->where(array(
                    "name" => $this->name,
                    "id" => array('$ne' => $this->id)
                )),1
        )) {
            throw new \Exception("A layout already exists with the name ".$this->name);
        }
    }
}