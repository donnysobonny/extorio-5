<?php
namespace Core\Classes\Models;
/**
 * A contact made with a user
 *
 * Class UserContact
 */
class UserContact extends B_UserContact {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {

    }

    private function generalChecks() {
        if(!strlen($this->userId)) {
            throw new \Exception("A user contact must specify the user id");
        }
        if(!strlen($this->date)) {
            $this->date = date("Y-m-d");
        }
        if(!strlen($this->time)) {
            $this->time = date("H:i:s");
        }
        if(!$this->topic) {
            $this->topic = UserContactTopic::findByName("Unknown");
        }
    }
}