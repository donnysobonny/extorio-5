<?php
namespace Core\Classes\Models;
use Core\Classes\Commons\Extorio;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Helpers\Query;
use MatthiasMullie\Minify\Exception;

/**
 * A user report
 *
 * Class UserReport
 */
class UserReport extends B_UserReport {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
    }

    private function generalChecks() {
        if(!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }
        if(!strlen($this->name)) {
            throw new \Exception("A user report must have a name");
        }
        if(UserReport::existsOne(Query::n()->where(array(
            "name" => $this->name,
            "id" => array(
                Query::_ne => $this->id
            )
        )))) {
            throw new \Exception("A user report's name must be unique");
        }
    }
}