<?php
namespace Core\Classes\Models;
use Core\Classes\Commons\Extorio;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Helpers\Query;

class Block extends B_Block {

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $description
     * @param bool $isPublic
     * @param int|BlockProcessor $processor
     * @param int|BlockCategory $category
     * @param int|BlockCategory $subCategory
     * @param string $config
     * @param bool $allRead
     * @param int[]|UserGroup[] $readGroups
     * @param int[]|UserGroup[] $modifyGroups
     * @param bool $isHidden
     * @param string $extensionName
     *
     * @return Block
     */
    public static function q(
        $name,
        $description,
        $isPublic = false,
        $processor,
        $category,
        $subCategory,
        $config = "",
        $allRead = true,
        $readGroups = array(),
        $modifyGroups = array(1,2),
        $isHidden = false,
        $extensionName = "Application"
    ) {
        $o = Block::n();
        $o->name = $name;
        $o->description = $description;
        $o->isPublic = $isPublic;
        $o->processor = $processor;
        $o->category = $category;
        $o->subCategory = $subCategory;
        $o->config = $config;
        $o->allRead = $allRead;
        $o->readGroups = $readGroups;
        $o->modifyGroups = $modifyGroups;
        $o->isHidden = $isHidden;
        $o->extensionName = $extensionName;

        return $o;
    }

    /**
     * Find by name
     *
     * @param string $name
     *
     * @return Block
     */
    public static function findByName($name) {
        return Block::findOne(Query::n()->where(array("name"=>$name)));
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
    }

    public function generalChecks() {
        if(!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }
        if(!strlen($this->name)) {
            throw new \Exception("A block cannot exist without a name");
        }
        if(!$this->category || !$this->subCategory) {
            throw new \Exception("A block must have a category and a sub category");
        }
        if(Block::findOne(
            Query::n()
                ->where(array(
                    "name" => $this->name,
                    "id" => array('$ne'=>$this->id)
                )),1
        )) {
            throw new \Exception("A block already exists with the name ".$this->name);
        }

        //if the config is an array, convert to http query string
        if(is_array($this->config)) {
            if(!empty($this->config)) {
                $this->config = http_build_query($this->config);
            } else {
                $this->config = "";
            }
        }
    }
}