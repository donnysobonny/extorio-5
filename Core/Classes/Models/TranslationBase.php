<?php
namespace Core\Classes\Models;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Helpers\Query;

/**
 * A base translation
 *
 * Class TranslationBase
 */
class TranslationBase extends B_TranslationBase {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {
        $ts = Translation::findResultSet(Query::n()->where(array(
            "baseId" => $this->id
        )));
        /** @var Translation $obj */
        while($obj = $ts->fetchNext()) {
            $obj->generatePoFile();
        }
    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {
        $ts = Translation::findResultSet(Query::n()->where(array(
            "baseId" => $this->id
        )));
        /** @var Translation $obj */
        while($obj = $ts->fetchNext()) {
            $obj->deleteThis();
        }
    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
        $this->plural = $this->_old->plural;
    }

    private function generalChecks() {
        if(!$this->extensionName) {
            $this->extensionName = InternalExtensions::_application;
        }
        if(!strlen($this->msgid)) {
            throw new \Exception("A translation base must have a msgid");
        }
        if($this->plural && !strlen($this->msgid_plural)) {
            throw new \Exception("A plural translation base must habe a msgid_plural");
        }
    }
}