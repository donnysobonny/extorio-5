<?php
namespace Core\Classes\Models;

use Core\Classes\Enums\PropertyType;
use Core\Classes\Exceptions\General_Exception;
use Core\Classes\Utilities\Strings;

class Property extends B_Property {

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $description
     * @param bool $isIndex
     * @param string $propertyType
     * @param string $basicType
     * @param string $complexType
     * @param string $childModelNamespace
     * @param string $enumNamespace
     *
     * @return Property
     */
    public static function q($name, $description, $isIndex = false, $propertyType, $basicType, $complexType="", $childModelNamespace="", $enumNamespace="") {
        $o = Property::n();
        $o->name = $name;
        $o->description = $description;
        $o->isIndex = $isIndex;
        $o->propertyType = $propertyType;
        $o->basicType = $basicType;
        $o->complexType = $complexType;
        $o->childModelNamespace = $childModelNamespace;
        $o->enumNamespace = $enumNamespace;

        return $o;
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        //fix the name
        if (!strlen($this->name)) {
            throw new \Exception("A property must have a name");
        }
        $this->label = Strings::propertyNameSafe($this->name);

        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        //the name cannot change!
        $this->name = $this->_old->name;

        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {
        //remove paths that this property existed on
    }

    protected function generalChecks() {
        if (strlen($this->childModelNamespace)) {
            $this->childModelNamespace = Strings::startsWith($this->childModelNamespace, "\\");
        }
        if (strlen($this->enumNamespace)) {
            $this->enumNamespace = Strings::startsWith($this->enumNamespace, "\\");
        }

        //meta types cannot be indexed!
        if (in_array($this->propertyType, array(PropertyType::_meta))) {
            $this->isIndex = false;
        }
    }
}