<?php
namespace Core\Classes\Models;

use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Helpers\Query;

/**
 *
 *
 * Class BlockProcessorCategory
 */
class BlockProcessorCategory extends B_BlockProcessorCategory {

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $description
     * @param string $icon
     * @param string $extensionName
     *
     * @return BlockProcessorCategory
     */
    public static function q($name, $description, $icon, $extensionName = "Application") {
        $o = BlockProcessorCategory::n();
        $o->name = $name;
        $o->description = $description;
        $o->icon = $icon;
        $o->extensionName = $extensionName;

        return $o;
    }

    /**
     * Find a block processor category by name
     *
     * @param string $name
     *
     * @return BlockProcessorCategory
     */
    public static function findByName($name) {
        return BlockProcessorCategory::findOne(Query::n()->where(array("name" => $name)));
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
    }

    private function generalChecks() {
        if (!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }
        if (!strlen($this->name)) {
            throw new \Exception("A block processor category must have a name");
        }
    }
}