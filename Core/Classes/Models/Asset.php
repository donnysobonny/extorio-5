<?php
namespace Core\Classes\Models;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\Strings;

/**
 * An asset owned by an extension
 *
 * Class Asset
 */
class Asset extends B_Asset {

    /**
     * Quick construct
     *
     * @param bool $isFile
     * @param string $fileName
     * @param string $dirPath
     * @param string $contentTypeMain
     * @param string $contentTypeSub
     * @param string $webUrl
     * @param string $localUrl
     * @param string $extensionName
     *
     * @return Asset
     */
    public static function q(
        $isFile,
        $fileName,
        $dirPath,
        $contentTypeMain,
        $contentTypeSub,
        $webUrl,
        $localUrl,
        $extensionName = "Application"
    ) {
        $o = Asset::n();
        $o->isFile = $isFile;
        $o->fileName = $fileName;
        $o->dirPath = $dirPath;
        $o->contentTypeMain = $contentTypeMain;
        $o->contentTypeSub = $contentTypeSub;
        $o->webUrl = $webUrl;
        $o->localUrl = $localUrl;
        $o->extensionName = $extensionName;

        return $o;
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {
        unlink($this->localUrl);
    }

    private function updateLocks() {
        $this->isFile = $this->_old->isFile;
        $this->fileName = $this->_old->fileName;
        $this->dirPath = $this->_old->dirPath;
        $this->contentTypeSub = $this->_old->contentTypeSub;
        $this->contentTypeMain = $this->_old->contentTypeMain;
        $this->extensionName = $this->_old->extensionName;
    }

    private function generalChecks() {
        if(!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }
        if($this->isFile) {
            if(!strlen($this->contentTypeMain) || !strlen($this->contentTypeSub)) {
                throw new \Exception("A file asset must have the main and sub content type");
            }
        }
        $this->fileName = Strings::notStartsAndNotEndsWith($this->fileName,"/");
        $this->fileName = Strings::fileNameSafe($this->fileName);
        if(!strlen($this->fileName)) {
            throw new \Exception("An asset cannot exist without a file name");
        }
        $this->dirPath = Strings::startsAndEndsWith($this->dirPath,"/");
        $this->dirPath = Strings::fileNameSafe($this->dirPath);
        if(!strlen($this->dirPath)) {
            throw new \Exception("An asset cannot exist without a dir path");
        }

        if(Asset::findOne(
            Query::n()
                ->where(array(
                    "isFile" => $this->isFile,
                    "fileName" => $this->fileName,
                    "dirPath" => $this->dirPath,
                    "extensionName" => $this->extensionName,
                    "id" => array(
                        Query::_ne => $this->id
                    )
                )),1
        )) {
            throw new \Exception("Cannot create duplicate files or folders");
        }

        $this->localUrl = $this->extensionName."/Assets".$this->dirPath.$this->fileName;
        $this->webUrl = "/".$this->localUrl;
    }
}