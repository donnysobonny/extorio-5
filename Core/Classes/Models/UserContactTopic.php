<?php
namespace Core\Classes\Models;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Helpers\Query;

/**
 * A topic for a user contact
 *
 * Class UserContactTopic
 */
class UserContactTopic extends B_UserContactTopic {

    /**
     * Find a topic by name
     *
     * @param string $name
     *
     * @return UserContactTopic
     */
    public static function findByName($name) {
        return UserContactTopic::findOne(Query::n()->where(array("name" => $name)));
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
    }

    private function generalChecks() {
        if(!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }
        if(!strlen($this->name)) {
            throw new \Exception("A user contact topic must have a name");
        }
        if(UserContactTopic::existsOne(Query::n()->where(array(
            "name" => $this->name,
            "id" => array(
                Query::_ne => $this->id
            )
        )))) {
            throw new \Exception("The name must be unique");
        }
    }
}