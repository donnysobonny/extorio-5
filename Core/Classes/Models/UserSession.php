<?php
namespace Core\Classes\Models;
final class UserSession extends B_UserSession {

    /**
     * Quick construct
     *
     * @param int $userId
     * @param string $session
     * @param string $remoteIP
     * @param string $expiryDate
     *
     * @return UserSession
     */
    public static function q(
        $userId,
        $session,
        $remoteIP,
        $expiryDate
    ) {
        $o = UserSession::n();
        $o->userId = $userId;
        $o->session = $session;
        $o->remoteIP = $remoteIP;
        $o->expiryDate = $expiryDate;

        return $o;
    }
}