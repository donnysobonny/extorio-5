<?php
namespace Core\Classes\Models;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\Users;

/**
 * 
 *
 * Class UserPrivilegeCategory
 */
class UserPrivilegeCategory extends B_UserPrivilegeCategory {

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $description
     * @param string $icon
     * @param string $extensionName
     *
     * @return UserPrivilegeCategory
     */
    public static function q(
        $name,
        $description,
        $icon,
        $extensionName = "Application"
    ) {
        $o = UserPrivilegeCategory::n();
        $o->name = $name;
        $o->description = $description;
        $o->icon = $icon;
        $o->extensionName = $extensionName;

        return $o;
    }

    /**
     * Find by name
     *
     * @param $name
     *
     * @return UserPrivilegeCategory
     */
    public static function findByName($name) {
        return UserPrivilegeCategory::findOne(Query::n()->where(array("name" => $name)));
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {
        //cannot delete a category that is in use
        if(Users::getUserPrivilegeCountByCategory($this->id) > 0) {
            throw new \Exception("It's not possible to delete a user privilege category that is in use by a user privilege");
        }
    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
    }

    private function generalChecks() {
        if(!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }
        if(!strlen($this->name)) {
            throw new \Exception("A user privilege category cannot exist without a name");
        }
    }
}