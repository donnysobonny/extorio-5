<?php
namespace Core\Classes\Models;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\Localization;
use MatthiasMullie\Minify\Exception;

/**
 * A translation
 *
 * Class Translation
 */
class Translation extends B_Translation {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {
        $this->generatePoFile();
    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {
        $this->generatePoFile();
    }

    public function generatePoFile() {
        Localization::generatePoFile($this->languageId);
    }

    private function updateLocks() {

    }

    private function generalChecks() {
        if(!strlen($this->baseId) || $this->baseId <= 0) {
            throw new \Exception("A translation must have a baseId");
        }
        if(!strlen($this->languageId) || $this->languageId <= 0) {
            throw new \Exception("A translation must have a language id");
        }
        if(Translation::existsOne(Query::n()->where(array(
            "baseId" => $this->baseId,
            "languageId" => $this->languageId,
            "id" => array(
                Query::_ne => $this->id
            )
        )))) {
            throw new \Exception("A translation already exists for this language and for this translation base");
        }
    }
}