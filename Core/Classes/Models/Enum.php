<?php
namespace Core\Classes\Models;
use Core\Classes\Commons\Extorio;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Enums\PHPTypes;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\Strings;

class Enum extends B_Enum {

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $description
     * @param array $values
     * @param bool $isHidden
     * @param string $extensionName
     *
     * @return Enum
     */
    public static function q(
        $name,
        $description,
        $values = array(),
        $isHidden = false,
        $extensionName = "Application"
    ) {
        $o = Enum::n();
        $o->name = $name;
        $o->description = $description;
        $o->values = $values;
        $o->isHidden = $isHidden;
        $o->extensionName = $extensionName;

        return $o;
    }

    /**
     * Find by name
     *
     * @param string $name
     *
     * @return Enum
     */
    public static function findByName($name) {
        return Enum::findOne(Query::n()->where(array("name"=>$name)));
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    private function generalChecks() {
        if(!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }
        if(!strlen($this->name)) {
            throw new \Exception("An enum cannot exist without a name");
        }
        $this->label = Strings::labelSafe($this->name);
        if(Enum::findOne(
            Query::n()
                ->where(array(
                    "label" => $this->label,
                    "id" => array('$ne' => $this->id)
                ))
        )) {
            throw new \Exception("An enum already exists with the label ".$this->label);
        }

        if(is_array($this->values)) {
            $values = array();
            for($i = 0; $i < count($this->values); $i++) {
                switch(gettype($this->values[$i])) {
                    case PHPTypes::_string :
                        if(strlen($this->values[$i]) && !in_array($this->values[$i],$values)) {
                            $values[] = $this->values[$i];
                        }

                        break;
                    case PHPTypes::_integer :
                    case PHPTypes::_float :
                    case PHPTypes::_double :
                        if(strlen($this->values[$i]) && !in_array(strval($this->values[$i]),$values)) {
                            $values[] = strval($this->values[$i]);
                        }
                        break;
                    case PHPTypes::_nULL :
                        if(!in_array("NULL",$values)) {
                            $values[] = "NULL";
                        }
                        break;
                    case PHPTypes::_boolean :
                        if($this->values[$i]) {
                            if(!in_array("true",$values)) {
                                $values[] = "true";
                            }
                        } else {
                            if(!in_array("false",$values)) {
                                $values[] = "false";
                            }
                        }
                        break;
                }
            }
            $this->values = $values;
        } else {
            $this->values = array();
        }

        if(!strlen($this->class)) {
            $this->class = $this->label;
        }
        $this->class = Strings::classNameSafe($this->class);

        $this->namespace = '\\'.$this->extensionName.'\Classes\Enums\\'.$this->class;
    }

    protected function afterCreate() {
        $this->createEnumFile();
    }

    public function createEnumFile() {
        $extension = Extorio::get()->getExtension($this->extensionName);
        if($extension) {
            $efile = $extension->_findClassFile($this->namespace);
            if(!$efile) {
                $efile = File::createFromPath($extension->_absoluteRoot."/Classes/Enums/internal/".$this->class.".php");
            }
            if($efile) {
                $content = File::constructFromPath("Core/Assets/file-templates/Enum_template.txt")->read();
                $content = str_replace("*|DESCRIPTION|*",$this->description,$content);
                $content = str_replace("*|EXTENSION_NAME|*",$this->extensionName,$content);
                $content = str_replace("*|CLASS_NAME|*",$this->class,$content);
                $content = str_replace("*|VALUES|*",var_export($this->values,true),$content);
                $constants = "";
                if($this->values) {
                    $usedConstants = array();
                    foreach($this->values as $value) {
                        $k = Strings::propertyNameSafe($value);
                        //we only actually create a constant if the enum value to propertyNameSafe does not exist already
                        if(!in_array($k, $usedConstants)) {
                            $usedConstants[] = $k;
                            $v = "'".str_replace("'","\\'",$value)."'";
                            //to be safe, we want to always start constant names with "_", to be sure we never use an internal key word
                            if(substr($k,0,1) != "_") $k = "_".$k;
                            $constants .= "\tconst ".$k." = ".$v.";\n";
                        }
                    }
                }
                $content = str_replace("*|CONSTANTS|*",$constants,$content);
                $efile->write($content);
            }
        }
    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
        $this->class = $this->_old->class;
        $this->namespace = $this->_old->namespace;
    }

    protected function afterUpdate() {
        $this->createEnumFile();
    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }
}