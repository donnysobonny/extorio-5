<?php
namespace Core\Classes\Models;
use Core\Classes\Commons\Extorio;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Enums\InternalEvents;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\Strings;
use MatthiasMullie\Minify\CSS;
use MatthiasMullie\Minify\JS;

class Theme extends B_Theme {

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $description
     * @param bool $default
     * @param bool $isHidden
     * @param string $extensionName
     *
     * @return Theme
     */
    public static function q(
        $name,
        $description,
        $default = false,
        $isHidden = false,
        $extensionName = "Application"
    ) {
        $o = Theme::n();
        $o->name = $name;
        $o->description = $description;
        $o->default = $default;
        $o->isHidden = $isHidden;
        $o->extensionName = $extensionName;

        return $o;
    }

    /**
     * Find by name
     *
     * @param string $name
     *
     * @return Theme
     */
    public static function findByName($name) {
        return Theme::findOne(Query::n()->where(array("name"=>$name)));
    }

    public static function findByDefault() {
        return Theme::findOne(
            Query::n()
                ->where(array("default"=>true)),2
        );
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    private function generalChecks() {
        if(!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }
        if(!strlen($this->name)) {
            throw new \Exception("A theme must have a name");
        }
        $this->label = Strings::labelSafe($this->name);
        if(Theme::existsOne(Query::n()->where(array(
            "label" => $this->label,
            "extensionName" => $this->extensionName,
            "id" => array(Query::_ne => $this->id)
        )))) {
            throw new \Exception("A theme already exists with the label '".$this->label."' within the extension '".$this->extensionName."'");
        }

        $this->fixDefault();
    }

    private function fixDefault() {
        if ($this->default) {
            $ts = Theme::findAll(Query::n()->where(array("default" => true, "id" => array('$ne' => $this->id))), 1);
            foreach ($ts as $t) {
                $t->default = false;
                $t->pushThis();
            }
        }
    }

    protected function afterCreate() {
        $this->createThemeFiles();
        $this->compileAndMinify();
    }

    public function createThemeFiles() {
        $files = array(
            "css/images/index.html" => "Core/Assets/index.html",
            "css/internal/index.html" => "Core/Assets/index.html",
            "css/internal/style.css" => null,
            "css/internal/style.min.css" => null,
            "css/index.html" => "Core/Assets/index.html",
            "css/style_override.css" => null,
            "fonts/index.html" => "Core/Assets/index.html",
            "images/index.html" => "Core/Assets/index.html",
            "js/internal/index.html" => "Core/Assets/index.html",
            "js/internal/script.min.js" => null,
            "js/script.js" => null,
            "js/index.html" => "Core/Assets/index.html",
            "less/internal/index.html" => "Core/Assets/index.html",
            "less/internal/style.less" => "Core/Assets/file-templates/style_template.less.txt",
            "less/index.html" => "Core/Assets/index.html",
            "less/style_override.less" => null,
            "index.html" => "Core/Assets/index.html",
        );

        $extension = Extorio::get()->getExtension($this->extensionName);
        if($extension) {
            foreach($files as $to => $from) {
                $toFile = File::createFromPath($extension->_absoluteRoot."/Themes/".$this->label."/".$to);
                if(!is_null($from)) {
                    if(!strlen($toFile->read())) {
                        $fromFile = File::constructFromPath($from);
                        if($fromFile->exists()) {
                            $toFile->write($fromFile->read());
                        }
                    }
                }
            }
        }
    }

    /**
     * Compile and minify the theme
     */
    public function compileAndMinify() {
        $this->compileLessToCss();
        $this->minify();
    }

    /**
     * Compile the less files of the theme to css. This also calls the theme_compile_less_files event to allow additional files to be compiled
     *
     * @throws \Exception
     */
    public function compileLessToCss() {
        $extension = Extorio::get()->getExtension($this->extensionName);
        if($extension) {
            $targetFile = File::createFromPath($extension->_absoluteRoot."/Themes/".$this->label."/css/internal/style.css");

            $filesToCheck = array(
                "Core/Assets/less/Extorio.less",
                "Core/Assets/bootstrap/less/bootstrap.less",
                $extension->_absoluteRoot."/Themes/".$this->label."/less/internal/style.less",
                $extension->_absoluteRoot."/Themes/".$this->label."/less/style_override.less"
            );

            $files = array(
                $extension->_absoluteRoot."/Themes/".$this->label."/less/internal/style.less",
                $extension->_absoluteRoot."/Themes/".$this->label."/less/style_override.less"
            );

            $actions = Extorio::get()->getEvent(InternalEvents::_theme_compile_less_files)->run();
            foreach($actions as $action) {
                if(is_array($action)) {
                    foreach($action as $file) {
                        if(file_exists($file)) {
                            $files[] = $file;
                        }
                    }
                } else {
                    if(file_exists($action)) {
                        $files[] = $action;
                    }
                }
            }

            $foundModified = false;
            $targetFileModified = $targetFile->MTime();
            foreach($filesToCheck as $f) {
                $file = File::constructFromPath($f);
                if($file->MTime() > $targetFileModified) {
                    $foundModified = true;
                }
            }

            if($foundModified) {
                $p = new \Less_Parser();
                foreach($files as $file) {
                    $p->parseFile($file);
                }
                $targetFile->write($p->getCss());
            }
        }
    }

    /**
     * Minify both the css and js files
     */
    public function minify() {
        $this->minifyCss();
        $this->minifyJs();
    }

    /**
     * Minify the css files. This also runs the theme_minify_css_files event to add additional themes to be minified
     *
     */
    public function minifyCss() {
        $extension = Extorio::get()->getExtension($this->extensionName);
        if($extension) {

            $targetFile = File::createFromPath($extension->_absoluteRoot . "/Themes/" . $this->label . "/css/internal/style.min.css");
            $targetFileModTime = $targetFile->MTime();

            $files = array($extension->_absoluteRoot . "/Themes/" . $this->label . "/css/internal/style.css"
            );

            $actions = Extorio::get()->getEvent(InternalEvents::_theme_minify_css_files)->run();
            foreach($actions as $action) {
                if(is_array($action)) {
                    foreach($action as $file) {
                        if(file_exists($file)) {
                            $files[] = $file;
                        }
                    }
                } else {
                    if(file_exists($action)) {
                        $files[] = $action;
                    }
                }
            }

            $foundModified = false;
            foreach($files as $f) {
                $file = File::constructFromPath($f);
                if($file->MTime() > $targetFileModTime) {
                    $foundModified = true;
                }
            }

            if($foundModified) {
                $min = new CSS();
                foreach($files as $file) {
                    $min->add($file);
                }
                $min->minify($extension->_absoluteRoot . "/Themes/" . $this->label . "/css/internal/style.min.css");
            }
        }
    }

    /**
     * Minify the js files. This also runs the theme_minify_js_files event to add additional themes to be minified
     *
     */
    public function minifyJs() {
        $extension = Extorio::get()->getExtension($this->extensionName);
        if($extension) {

            $targetFile = File::createFromPath($extension->_absoluteRoot . "/Themes/" . $this->label . "/js/internal/script.min.js");
            $targetFileModTime = $targetFile->MTime();

            $files = array($extension->_absoluteRoot . "/Themes/" . $this->label . "/js/script.js"
            );

            $actions = Extorio::get()->getEvent(InternalEvents::_theme_minify_js_files)->run();
            foreach($actions as $action) {
                if(is_array($action)) {
                    foreach($action as $file) {
                        if(file_exists($file)) {
                            $files[] = $file;
                        }
                    }
                } else {
                    if(file_exists($action)) {
                        $files[] = $action;
                    }
                }
            }

            $foundModified = false;
            foreach($files as $f) {
                $file = File::constructFromPath($f);
                if($file->MTime() > $targetFileModTime) {
                    $foundModified = true;
                }
            }

            if($foundModified) {
                $min = new JS();
                foreach($files as $file) {
                    $min->add($file);
                }
                $min->minify($extension->_absoluteRoot . "/Themes/" . $this->label . "/js/internal/script.min.js");
            }
        }
    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
    }

    protected function afterUpdate() {
        if ($this->default && !$this->_old->default) {
            $this->compileAndMinify();
        }
    }
}