<?php
namespace Core\Classes\Models;
use Core\Classes\Helpers\Query;

/**
 * A language entity
 *
 * Class Language
 */
class Language extends B_Language {

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    public function generalChecks() {
        if(!strlen($this->name)) {
            throw new \Exception("A language must have a name");
        }
        if(!strlen($this->mLocale) || !strlen($this->sLocale)) {
            throw new \Exception("A language must specify the locales");
        }
        if(Language::existsOne(Query::n()->where(array(
            "name" => $this->name,
            "id" => array(
                Query::_ne => $this->id
            )
        )))) {
            throw new \Exception("Name must be unique");
        }
    }

    public function updateLocks() {
        $this->name = $this->_old->name;
    }
}