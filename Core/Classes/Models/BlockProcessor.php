<?php
namespace Core\Classes\Models;
use Core\Classes\Commons\Extorio;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\Strings;

class BlockProcessor extends B_BlockProcessor {

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $description
     * @param int|BlockProcessorCategory $category
     * @param bool $inlineEnabled
     * @param bool $allInline
     * @param int[]|UserGroup[] $inlineGroups
     * @param string $extensionName
     *
     * @return BlockProcessor
     */
    public static function q(
        $name,
        $description,
        $category,
        $inlineEnabled = true,
        $allInline = true,
        $inlineGroups = array(),
        $extensionName = "Application"
    ) {
        $o = BlockProcessor::n();
        $o->name = $name;
        $o->description = $description;
        $o->category = $category;
        $o->inlineEnabled = $inlineEnabled;
        $o->allInline = $allInline;
        $o->inlineGroups = $inlineGroups;
        $o->extensionName = $extensionName;

        return $o;
    }

    /**
     * Find by name
     *
     * @param string $name
     *
     * @return BlockProcessor
     */
    public static function findByName($name) {
        return BlockProcessor::findOne(Query::n()->where(array("name"=>$name)));
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    private function generalChecks() {
        if(!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }
        if(!strlen($this->name)) {
            throw new \Exception("A block processor cannot exist without a name");
        }
        $this->label = Strings::labelSafe($this->name);
        if(!$this->category) {
            throw new \Exception("A block processor must have a category");
        }
        if(BlockProcessor::findOne(
            Query::n()
                ->where(array(
                    "label" => $this->label,
                    "id" => array('$ne'=>$this->id)
                )),1
        )) {
            throw new \Exception("A block already exists with the label ".$this->label);
        }

        if(!strlen($this->class)) {
            $this->class = $this->name;
        }
        $this->class = Strings::classNameSafe($this->class);

        $this->namespace = '\\'.$this->extensionName.'\Components\BlockProcessors\\'.$this->class;
    }

    protected function afterCreate() {
        $this->createComponentFile();
    }

    private function createComponentFile() {
        $extension = Extorio::get()->getExtension($this->extensionName);
        if($extension) {
            if(!$extension->_findClassFile($this->namespace)) {
                //create the file
                $file = File::createFromPath($extension->_absoluteRoot."/Components/BlockProcessors/".$this->class.".php");
                $template = File::constructFromPath("Core/Assets/file-templates/BlockProcessor_template.txt");
                $content = $template->read();
                $content = str_replace("*|DESCRIPTION|*",$this->description,$content);
                $content = str_replace("*|CLASS_NAME|*",$this->class,$content);
                $content = str_replace("*|EXTENSION_NAME|*",$this->extensionName,$content);
                $file->write($content);
            }
        }
    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
        $this->class = $this->_old->class;
        $this->namespace = $this->_old->namespace;
    }

    protected function afterUpdate() {
        $this->createComponentFile();
    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }
}