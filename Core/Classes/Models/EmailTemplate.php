<?php
namespace Core\Classes\Models;
use Core\Classes\Commons\Extorio;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Helpers\Query;

/**
 * An email template that can optionally be used as the content of an email transport and can use replacers for dynamic content
 *
 * Class EmailTemplate
 */
class EmailTemplate extends B_EmailTemplate {

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $description
     * @param string $subject
     * @param string $body
     * @param string $extensionName
     *
     * @return EmailTemplate
     */
    public static function q(
        $name,
        $description,
        $subject,
        $body,
        $extensionName = "Application"
    ) {
        $o = EmailTemplate::n();
        $o->name = $name;
        $o->description = $description;
        $o->subject = $subject;
        $o->body = $body;
        $o->extensionName = $extensionName;

        return $o;
    }

    /**
     * Find by name
     *
     * @param string $name
     *
     * @return EmailTemplate
     */
    public static function findByName($name) {
        return EmailTemplate::findOne(Query::n()->where(array("name"=>$name)));
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
    }

    private function generalChecks() {
        if(!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }

        if(!strlen($this->subject)) {
            throw new \Exception("An email template must have a subject");
        }

        if(!strlen($this->name)) {
            throw new \Exception("An email template cannot exist without a name");
        }
        //make sure the name is unique
        if(EmailTemplate::findOne(
            Query::n()
                ->where(array(
                    "name" => $this->name,
                    "id" => array('$ne' => $this->id)
                )),1
        )) {
            throw new \Exception("The name '".$this->name."' is already in use by another email template");
        }
    }
}