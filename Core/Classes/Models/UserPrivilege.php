<?php
namespace Core\Classes\Models;

use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Enums\InternalUserGroups;
use Core\Classes\Helpers\Query;

/**
 *
 *
 * Class UserPrivilege
 */
class UserPrivilege extends B_UserPrivilege {

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $description
     * @param int|UserPrivilegeCategory $category
     * @param string $extensionName
     *
     * @return UserPrivilege
     */
    public static function q($name, $description, $category, $extensionName = "Application") {
        $o = UserPrivilege::n();
        $o->name = $name;
        $o->description = $description;
        $o->category = $category;
        $o->extensionName = $extensionName;

        return $o;
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {
        //add to super user
        $ug = UserGroup::findOne(Query::n()->where(array("name" => InternalUserGroups::_superAdministrator)), 1);
        $ug->privileges[] = $this->id;
        $ug->pushThis();
    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
    }

    private function generalChecks() {
        if (!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }

        if (!strlen($this->name)) {
            throw new \Exception("A user privilege must have a name");
        }

        if (!$this->category) {
            $this->category = UserPrivilegeCategory::findOne();
        }
    }
}