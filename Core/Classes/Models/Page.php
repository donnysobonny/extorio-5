<?php
namespace Core\Classes\Models;

use Core\Classes\Commons\Extorio;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Enums\InternalPages;
use Core\Classes\Exceptions\Page_Exception;
use Core\Classes\Helpers\LayoutSystem;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\Url;
use Core\Classes\Utilities\Strings;

class Page extends B_Page {

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $desciption
     * @param string $address
     * @param bool $default
     * @param bool $isPublic
     * @param string $publishDate
     * @param string $unpublishDate
     * @param null|int|Template $template
     * @param null|int|Theme $theme
     * @param array $layout
     * @param bool $allRead
     * @param int $readFailPageId
     * @param int[]|UserGroup[] $readGroups
     * @param int[]|UserGroup[] $modifyGroups
     * @param string $prependToHead
     * @param string $appendToHead
     * @param string $prependToBody
     * @param string $appendToBody
     * @param bool $isHidden
     * @param string $extensionName
     *
     * @return Page
     */
    public static function q($name, $desciption, $address, $default = false, $isPublic = true, $publishDate = "", $unpublishDate = "", $template = null, $theme = null, $layout = array(), $allRead = true, $readFailPageId = 0, $readGroups = array(), $modifyGroups = array(1, 2), $prependToHead = "", $appendToHead = "", $prependToBody = "", $appendToBody = "", $isHidden = false, $extensionName = "Application") {
        $o = Page::n();
        $o->name = $name;
        $o->description = $desciption;
        $o->address = $address;
        $o->default = $default;
        $o->isPublic = $isPublic;
        $o->publishDate = $publishDate;
        $o->unpublishDate = $unpublishDate;
        $o->template = $template;
        $o->theme = $theme;
        $o->layout = $layout;
        $o->allRead = $allRead;
        $o->readFailPageId = $readFailPageId;
        $o->readGroups = $readGroups;
        $o->modifyGroups = $modifyGroups;
        $o->prependToHead = $prependToHead;
        $o->appendToHead = $appendToHead;
        $o->prependToBody = $prependToBody;
        $o->appendToBody = $appendToBody;
        $o->isHidden = $isHidden;
        $o->extensionName = $extensionName;

        return $o;
    }

    /**
     * @return Page
     */
    public static function findUserLogin() {
        return Page::findByAddress(InternalPages::_userLogin);
    }

    /**
     * @return Page
     */
    public static function findByAddress($address) {
        $address = Strings::startsAndEndsWith($address, "/");
        return Page::findOne(Query::n()->where(array("address" => $address)), 3);
    }

    /**
     * @return Page
     */
    public static function findExtorioAdminLogin() {
        return Page::findByAddress(InternalPages::_extorioAdminLogin);
    }

    /**
     * @return Page
     */
    public static function find401AccessDenied() {
        return Page::findByAddress(InternalPages::_401AccessDenied);
    }

    /**
     * @return Page
     */
    public static function find404PageNotFOund() {
        return Page::findByAddress(InternalPages::_404PageNotFound);
    }

    /**
     * @return Page
     */
    public static function findByName($name) {
        return Page::findOne(Query::n()->where(array("name" => $name)), 3);
    }

    /**
     * @return Page
     */
    public static function findByControllerType($controllerType) {
        return Page::findOne(Query::n()->where(array('$or' => array("controllerType" => $controllerType, "viewType" => $controllerType))), 3);
    }

    /**
     * @return Page
     */
    public static function findByDefault() {
        return Page::findOne(Query::n()->where(array("default" => true)), 3);
    }

    public function redirect($queryParams = array(), $statusCode = 302) {
        $this->getUrl($queryParams)->redirect($statusCode);
    }

    public function getUrl($queryParams = array()) {
        $url = Url::n($this->address);
        $url->setQueryParams($queryParams);
        return $url;
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    private function generalChecks() {
        //extension name defaults to Application
        if (!strlen($this->extensionName)) {
            $this->extensionName = "Application";
        }

        //page cannot exist without a name
        if (!strlen($this->name)) {
            throw new Page_Exception("A page cannot exist without a name");
        }
        //make sure the name is unique
        if (Page::findOne(Query::n()->where(array("name" => $this->name, "id" => array('$ne' => $this->id))))) {
            throw new Page_Exception("The page name " . $this->name . " already exists");
        }

        //setup the class
        if (!strlen($this->class)) {
            $this->class = $this->name;
        }
        $this->class = Strings::classNameSafe($this->class);

        //setup the controller/view namespaces
        $this->controllerNamespace = '\\' . $this->extensionName . '\Components\Controllers\\' . $this->class;
        $this->viewNamespace = '\\' . $this->extensionName . '\Components\Views\\' . $this->class;

        //default layout
        if (empty($this->layout)) {
            $this->layout = LayoutSystem::getDefault()->__toArray();
        }

        //fix the address
        if (!strlen($this->address)) {
            $this->address = $this->name;
        }
        $url = Url::n($this->address);
        $this->address = Strings::startsAndEndsWith($url->getUrl(), "/");
    }

    protected function afterCreate() {
        //create the controller and view file
        $this->createControllerFile();
        $this->createViewFile();

        $this->fixDefault();

        if ($this->theme) {
            $this->theme->compileAndMinify();
        }
    }

    private function createControllerFile() {
        $extension = Extorio::get()->getExtension($this->extensionName);
        if ($extension) {
            if (!$extension->_findClassFile($this->controllerNamespace)) {
                //create the file
                $file = File::createFromPath($extension->_absoluteRoot . '/Components/Controllers/' . $this->class . ".php");
                $template = File::constructFromPath("Core/Assets/file-templates/Controller_template.txt");
                $content = $template->read();
                $content = str_replace("*|DESCRIPTION|*", $this->description, $content);
                $content = str_replace("*|CLASS_NAME|*", $this->class, $content);
                $content = str_replace("*|EXTENSION_NAME|*", $this->extensionName, $content);
                $file->write($content);
            }
        }
    }

    private function createViewFile() {
        $extension = Extorio::get()->getExtension($this->extensionName);
        if ($extension) {
            if (!$extension->_findClassFile($this->viewNamespace)) {
                //create the file
                $file = File::createFromPath($extension->_absoluteRoot . '/Components/Views/' . $this->class . ".php");
                $template = File::constructFromPath("Core/Assets/file-templates/View_template.txt");
                $content = $template->read();
                $content = str_replace("*|DESCRIPTION|*", $this->description, $content);
                $content = str_replace("*|CLASS_NAME|*", $this->class, $content);
                $content = str_replace("*|EXTENDS_CLASS_NAME|*", $this->controllerNamespace, $content);
                $content = str_replace("*|EXTENSION_NAME|*", $this->extensionName, $content);
                $file->write($content);
            }
        }
    }

    private function fixDefault() {
        if ($this->default) {
            $pages = Page::findAll(Query::n()->where(array("default" => true, "id" => array('$ne' => $this->id))), 1);
            foreach ($pages as $page) {
                $page->default = false;
                $page->pushThis();
            }
        }
    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    private function updateLocks() {
        //if internal page, lock the name
        if (in_array($this->address, InternalPages::values())) {
            $this->address = $this->_old->address;
        }

        //extension
        $this->extensionName = $this->_old->extensionName;
        //class
        $this->class = $this->_old->class;
        //view/controller namespaces
        $this->controllerNamespace = $this->_old->controllerNamespace;
        $this->viewNamespace = $this->_old->viewNamespace;
    }

    protected function afterUpdate() {
        //create the controller and view file
        $this->createControllerFile();
        $this->createViewFile();

        $this->fixDefault();

        if ($this->theme) {
            $this->theme->compileAndMinify();
        }
    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }
}