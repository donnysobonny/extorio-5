<?php
namespace Core\Classes\Models;

use Core\Classes\Commons\Extorio;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Exceptions\General_Exception;
use Core\Classes\Helpers\LayoutSystem;
use Core\Classes\Helpers\Query;

class Template extends B_Template {

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $description
     * @param bool $default
     * @param array $layout
     * @param int[]|UserGroup[] $modifyGroups
     * @param string $prependToHead
     * @param string $appendToHead
     * @param string $prependToBody
     * @param string $appendToBody
     * @param bool $isHidden
     * @param string $extensionName
     *
     * @return Template
     */
    public static function q($name, $description, $default = false, $layout = array(), $modifyGroups = array(1, 2), $prependToHead = "", $appendToHead = "", $prependToBody = "", $appendToBody = "", $isHidden = false, $extensionName = "Application") {
        $o = Template::n();
        $o->name = $name;
        $o->description = $description;
        $o->default = $default;
        $o->layout = $layout;
        $o->modifyGroups = $modifyGroups;
        $o->prependToHead = $prependToHead;
        $o->appendToHead = $appendToHead;
        $o->prependToBody = $prependToBody;
        $o->appendToBody = $appendToBody;
        $o->isHidden = $isHidden;
        $o->extensionName = $extensionName;

        return $o;
    }

    public static function findByDefault() {
        return Template::findOne(Query::n()->where(array("default" => true)), 2);
    }

    public static function findByName($name) {
        return Template::findOne(Query::n()->where(array("name" => $name)), 2);
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {
        $this->fixDefault();
    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {
        $this->fixDefault();
    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
    }

    private function generalChecks() {
        if (!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }
        if (!strlen($this->name)) {
            throw new \Exception("A template cannot exist without a name");
        }
        //make sure name is unique
        if (Template::findOne(Query::n()->where(array("name" => $this->name, "id" => array('$ne' => $this->id))), 1)) {
            throw new \Exception("A template already exists with the name " . $this->name);
        }

        if (empty($this->layout)) {
            $this->layout = LayoutSystem::getDefault()->__toArray();
        }
    }

    private function fixDefault() {
        if ($this->default) {
            $ts = Template::findAll(Query::n()->where(array("default" => true, "id" => array('$ne' => $this->id))), 1);
            foreach ($ts as $t) {
                $t->default = false;
                $t->pushThis();
            }
        }
    }
}