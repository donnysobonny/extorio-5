<?php
namespace Core\Classes\Models;
/**
 * A property of a model
 *
 * Class B_Property
 */
class B_Property extends \Core\Classes\Commons\Model {

    
    /**
     * The name of the property. This cannot be changed
     *
     * @var string
     */
    public $name;
    
    /**
     * The label of a property, this can be changed
     *
     * @var string
     */
    public $label;
    
    /**
     * The description
     *
     * @var string
     */
    public $description;
    
    /**
     * If the property is complex, this is the child model's namespace
     *
     * @var string
     */
    public $childModelNamespace;
    
    /**
     * If the property is an enum, this is the enum's namespace
     *
     * @var string
     */
    public $enumNamespace;
    
    /**
     * Whether the property is indexed or not. If you will search for models using this propety a lot, you should consider indexing the property to speed up searches. This also allows you to search using this property in the model viewer
     *
     * @var bool
     */
    public $isIndex;
    
    /**
     * The property type
     *
     * @var string
     */
    public $propertyType;
    
    /**
     * If the property type is basic, this is the basic type
     *
     * @var string
     */
    public $basicType;
    
    /**
     * If the property is complex, this is the complex type
     *
     * @var string
     */
    public $complexType;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'label' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'childModelNamespace' => 
  array (
    'basicType' => 'textfield',
  ),
  'enumNamespace' => 
  array (
    'basicType' => 'textfield',
  ),
  'isIndex' => 
  array (
    'basicType' => 'checkbox',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
  'propertyType' => 
  array (
    'enumNamespace' => '\\Core\\Classes\\Enums\\PropertyType',
  ),
  'basicType' => 
  array (
    'enumNamespace' => '\\Core\\Classes\\Enums\\PropertyBasicType',
  ),
  'complexType' => 
  array (
    'enumNamespace' => '\\Core\\Classes\\Enums\\PHPTypes',
  ),
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}