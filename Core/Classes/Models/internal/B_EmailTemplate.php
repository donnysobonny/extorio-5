<?php
namespace Core\Classes\Models;
/**
 * An email template that can optionally be used as the content of an email transport and can use replacers for dynamic content
 *
 * Class B_EmailTemplate
 */
class B_EmailTemplate extends \Core\Classes\Commons\Model {

    
    /**
     * A name that describes the template
     *
     * @var string
     */
    public $name;
    
    /**
     * A description of the template
     *
     * @var string
     */
    public $description;
    
    /**
     * The subject. If a transport uses a template, it inherits the templates subject
     *
     * @var string
     */
    public $subject;
    
    /**
     * The body of the template, including it's replacers. If an email transport uses a template, it's body will be set to this
     *
     * @var string
     */
    public $body;
    
    /**
     * The name of the extension that owns this template
     *
     * @var string
     */
    public $extensionName;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'subject' => 
  array (
    'basicType' => 'textfield',
  ),
  'body' => 
  array (
    'basicType' => 'textarea',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}