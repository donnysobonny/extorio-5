<?php
namespace Core\Classes\Models;
/**
 * A template resembles the outer content of pages. Many pages can use the same template,effectively making a template site-wide.
 *
 * Class B_Template
 */
class B_Template extends \Core\Classes\Commons\Model {

    
    /**
     * The name of the template
     *
     * @var string
     */
    public $name;
    
    /**
     * The template description
     *
     * @var string
     */
    public $description;
    
    /**
     * Whether this template is default
     *
     * @var bool
     */
    public $default;
    
    /**
     * The extension that owns this template
     *
     * @var string
     */
    public $extensionName;
    
    /**
     * Content to prepend to the body
     *
     * @var string
     */
    public $prependToBody;
    
    /**
     * Content to prepend to the head
     *
     * @var string
     */
    public $prependToHead;
    
    /**
     * Content to append to the body
     *
     * @var string
     */
    public $appendToBody;
    
    /**
     * Content to append to the head
     *
     * @var string
     */
    public $appendToHead;
    
    /**
     * The actual layout
     *
     * @var array
     */
    public $layout;
    
    /**
     * Whether this template is hidden from the template management page
     *
     * @var bool
     */
    public $isHidden;
    
    /**
     * 
     *
     * @var \Core\Classes\Models\UserGroup[]
     */
    public $modifyGroups;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'default' => 
  array (
    'basicType' => 'checkbox',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
  'prependToBody' => 
  array (
    'basicType' => 'textarea',
  ),
  'prependToHead' => 
  array (
    'basicType' => 'textarea',
  ),
  'appendToBody' => 
  array (
    'basicType' => 'textarea',
  ),
  'appendToHead' => 
  array (
    'basicType' => 'textarea',
  ),
  'isHidden' => 
  array (
    'basicType' => 'checkbox',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
  'modifyGroups' => 
  array (
    'type' => 'array',
    'childModelNamespace' => '\\Core\\Classes\\Models\\UserGroup',
  ),
);
    }

    protected static function internal_metaProperties() {
        return array (
  0 => 'layout',
);
    }
}