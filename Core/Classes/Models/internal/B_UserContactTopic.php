<?php
namespace Core\Classes\Models;
/**
 * A topic for a user contact
 *
 * Class B_UserContactTopic
 */
class B_UserContactTopic extends \Core\Classes\Commons\Model {

    
    /**
     * 
     *
     * @var string
     */
    public $name;
    
    /**
     * 
     *
     * @var string
     */
    public $description;
    
    /**
     * 
     *
     * @var string
     */
    public $extensionName;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}