<?php
namespace Core\Classes\Models;
/**
 * An asset owned by an extension
 *
 * Class B_Asset
 */
class B_Asset extends \Core\Classes\Commons\Model {

    
    /**
     * 
     *
     * @var bool
     */
    public $isFile;
    
    /**
     * The name of the file
     *
     * @var string
     */
    public $fileName;
    
    /**
     * The path to the file within the extension's asset directory
     *
     * @var string
     */
    public $dirPath;
    
    /**
     * The main content type
     *
     * @var string
     */
    public $contentTypeMain;
    
    /**
     * The sub content type
     *
     * @var string
     */
    public $contentTypeSub;
    
    /**
     * The web-accessible url
     *
     * @var string
     */
    public $webUrl;
    
    /**
     * The local url
     *
     * @var string
     */
    public $localUrl;
    
    /**
     * The name of the extension that owns this asset
     *
     * @var string
     */
    public $extensionName;
    

    protected static function internal_basicProperties() {
        return array (
  'isFile' => 
  array (
    'basicType' => 'checkbox',
  ),
  'fileName' => 
  array (
    'basicType' => 'textfield',
  ),
  'dirPath' => 
  array (
    'basicType' => 'textfield',
  ),
  'contentTypeMain' => 
  array (
    'basicType' => 'textfield',
  ),
  'contentTypeSub' => 
  array (
    'basicType' => 'textfield',
  ),
  'webUrl' => 
  array (
    'basicType' => 'textfield',
  ),
  'localUrl' => 
  array (
    'basicType' => 'textfield',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}