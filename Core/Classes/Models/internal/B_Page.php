<?php
namespace Core\Classes\Models;
/**
 * A page in an MVC framework, with layout editing capabilities. A page is accessed via a unique address
 *
 * Class B_Page
 */
class B_Page extends \Core\Classes\Commons\Model {

    
    /**
     * The name of the page. Used in page headers.
     *
     * @var string
     */
    public $name;
    
    /**
     * A description of the page
     *
     * @var string
     */
    public $description;
    
    /**
     * The page's address. Used to access the page
     *
     * @var string
     */
    public $address;
    
    /**
     * Whether the page is the default page
     *
     * @var bool
     */
    public $default;
    
    /**
     * The class of the MVC components
     *
     * @var string
     */
    public $class;
    
    /**
     * The controller namesapce
     *
     * @var string
     */
    public $controllerNamespace;
    
    /**
     * The view namespace
     *
     * @var string
     */
    public $viewNamespace;
    
    /**
     * The extension that owns this page
     *
     * @var string
     */
    public $extensionName;
    
    /**
     * Content to prepend to the head of the document
     *
     * @var string
     */
    public $prependToHead;
    
    /**
     * Content to append to the head of the document
     *
     * @var string
     */
    public $appendToHead;
    
    /**
     * Content to prepend to the body of the document
     *
     * @var string
     */
    public $prependToBody;
    
    /**
     * Content to append to the body of the document
     *
     * @var string
     */
    public $appendToBody;
    
    /**
     * Whether the page is public. Trying to access a page that is not public will redirect to 404 page not found
     *
     * @var bool
     */
    public $isPublic;
    
    /**
     * Whether the page is hidden from page management
     *
     * @var bool
     */
    public $isHidden;
    
    /**
     * The publish date of the page (optional). This allows you to set a date where the page will be published
     *
     * @var string
     */
    public $publishDate;
    
    /**
     * The unpublish date of the page (optional). This allows you to set a date where the page will be unpublished
     *
     * @var string
     */
    public $unpublishDate;
    
    /**
     * The template the page will use. If no template specified, the page will use the default template
     *
     * @var \Core\Classes\Models\Template
     */
    public $template;
    
    /**
     * The theme the page will use. If no theme is specified, the page will use the default theme
     *
     * @var \Core\Classes\Models\Theme
     */
    public $theme;
    
    /**
     * The actual layout
     *
     * @var array
     */
    public $layout;
    
    /**
     * 
     *
     * @var bool
     */
    public $allRead;
    
    /**
     * 
     *
     * @var integer
     */
    public $readFailPageId;
    
    /**
     * 
     *
     * @var \Core\Classes\Models\UserGroup[]
     */
    public $readGroups;
    
    /**
     * 
     *
     * @var \Core\Classes\Models\UserGroup[]
     */
    public $modifyGroups;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'address' => 
  array (
    'basicType' => 'textfield',
  ),
  'default' => 
  array (
    'basicType' => 'checkbox',
  ),
  'class' => 
  array (
    'basicType' => 'textfield',
  ),
  'controllerNamespace' => 
  array (
    'basicType' => 'textfield',
  ),
  'viewNamespace' => 
  array (
    'basicType' => 'textfield',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
  'prependToHead' => 
  array (
    'basicType' => 'textarea',
  ),
  'appendToHead' => 
  array (
    'basicType' => 'textarea',
  ),
  'prependToBody' => 
  array (
    'basicType' => 'textarea',
  ),
  'appendToBody' => 
  array (
    'basicType' => 'textarea',
  ),
  'isPublic' => 
  array (
    'basicType' => 'checkbox',
  ),
  'isHidden' => 
  array (
    'basicType' => 'checkbox',
  ),
  'publishDate' => 
  array (
    'basicType' => 'date',
  ),
  'unpublishDate' => 
  array (
    'basicType' => 'date',
  ),
  'allRead' => 
  array (
    'basicType' => 'checkbox',
  ),
  'readFailPageId' => 
  array (
    'basicType' => 'number',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
  'template' => 
  array (
    'type' => 'object',
    'childModelNamespace' => '\\Core\\Classes\\Models\\Template',
  ),
  'theme' => 
  array (
    'type' => 'object',
    'childModelNamespace' => '\\Core\\Classes\\Models\\Theme',
  ),
  'readGroups' => 
  array (
    'type' => 'array',
    'childModelNamespace' => '\\Core\\Classes\\Models\\UserGroup',
  ),
  'modifyGroups' => 
  array (
    'type' => 'array',
    'childModelNamespace' => '\\Core\\Classes\\Models\\UserGroup',
  ),
);
    }

    protected static function internal_metaProperties() {
        return array (
  0 => 'layout',
);
    }
}