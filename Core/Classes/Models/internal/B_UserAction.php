<?php
namespace Core\Classes\Models;
/**
 * 
 *
 * Class B_UserAction
 */
class B_UserAction extends \Core\Classes\Commons\Model {

    
    /**
     * 
     *
     * @var integer
     */
    public $userId;
    
    /**
     * 
     *
     * @var \Core\Classes\Models\UserActionType
     */
    public $type;
    
    /**
     * 
     *
     * @var string
     */
    public $date;
    
    /**
     * 
     *
     * @var string
     */
    public $time;
    
    /**
     * 
     *
     * @var string
     */
    public $detail1;
    
    /**
     * 
     *
     * @var string
     */
    public $detail2;
    
    /**
     * 
     *
     * @var string
     */
    public $detail3;
    

    protected static function internal_basicProperties() {
        return array (
  'userId' => 
  array (
    'basicType' => 'number',
  ),
  'date' => 
  array (
    'basicType' => 'date',
  ),
  'time' => 
  array (
    'basicType' => 'time',
  ),
  'detail1' => 
  array (
    'basicType' => 'textfield',
  ),
  'detail2' => 
  array (
    'basicType' => 'textfield',
  ),
  'detail3' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
  'type' => 
  array (
    'type' => 'object',
    'childModelNamespace' => '\\Core\\Classes\\Models\\UserActionType',
  ),
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}