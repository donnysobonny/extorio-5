<?php
namespace Core\Classes\Models;
/**
 * A user of the website and/or admin
 *
 * Class B_User
 */
class B_User extends \Core\Classes\Commons\Model {

    
    /**
     * The username
     *
     * @var string
     */
    public $username;
    
    /**
     * The password
     *
     * @var string
     */
    public $password;
    
    /**
     * The first name of the user
     *
     * @var string
     */
    public $firstname;
    
    /**
     * 
     *
     * @var string
     */
    public $middlenames;
    
    /**
     * The last name of the user
     *
     * @var string
     */
    public $lastname;
    
    /**
     * The generated short name of the user
     *
     * @var string
     */
    public $shortname;
    
    /**
     * The generated long name for this user
     *
     * @var string
     */
    public $longname;
    
    /**
     * The email
     *
     * @var string
     */
    public $email;
    
    /**
     * 
     *
     * @var string
     */
    public $bio;
    
    /**
     * Whether the user can login
     *
     * @var bool
     */
    public $canLogin;
    
    /**
     * The number of times a user has logged in
     *
     * @var integer
     */
    public $numLogin;
    
    /**
     * The date they last logged in
     *
     * @var string
     */
    public $dateLogin;
    
    /**
     * 
     *
     * @var string
     */
    public $dateActive;
    
    /**
     * The user's avatar
     *
     * @var string
     */
    public $avatar;
    
    /**
     * 
     *
     * @var \Core\Classes\Models\UserGroup
     */
    public $userGroup;
    
    /**
     * The user's currently selected language
     *
     * @var \Core\Classes\Models\Language
     */
    public $language;
    
    /**
     * The date and time that the user was created
     *
     * @var string
     */
    public $dateCreated;
    
    /**
     * The date and time that the user was updated
     *
     * @var string
     */
    public $dateUpdated;
    
    /**
     * 
     *
     * @var bool
     */
    public $acceptsMarketing;
    
    /**
     * 
     *
     * @var string
     */
    public $title;
    
    /**
     * 
     *
     * @var string
     */
    public $gender;
    
    /**
     * Line 1 of the address
     *
     * @var string
     */
    public $address1;
    
    /**
     * Line 2 of the address
     *
     * @var string
     */
    public $address2;
    
    /**
     * 
     *
     * @var string
     */
    public $city;
    
    /**
     * 
     *
     * @var string
     */
    public $region;
    
    /**
     * 
     *
     * @var string
     */
    public $postalCode;
    
    /**
     * 
     *
     * @var string
     */
    public $country;
    
    /**
     * The user's phone number
     *
     * @var string
     */
    public $telno;
    

    protected static function internal_basicProperties() {
        return array (
  'username' => 
  array (
    'basicType' => 'textfield',
  ),
  'password' => 
  array (
    'basicType' => 'textfield',
  ),
  'firstname' => 
  array (
    'basicType' => 'textfield',
  ),
  'middlenames' => 
  array (
    'basicType' => 'textfield',
  ),
  'lastname' => 
  array (
    'basicType' => 'textfield',
  ),
  'shortname' => 
  array (
    'basicType' => 'textfield',
  ),
  'longname' => 
  array (
    'basicType' => 'textfield',
  ),
  'email' => 
  array (
    'basicType' => 'email',
  ),
  'bio' => 
  array (
    'basicType' => 'textarea',
  ),
  'canLogin' => 
  array (
    'basicType' => 'checkbox',
  ),
  'numLogin' => 
  array (
    'basicType' => 'number',
  ),
  'dateLogin' => 
  array (
    'basicType' => 'datetime',
  ),
  'dateActive' => 
  array (
    'basicType' => 'datetime',
  ),
  'avatar' => 
  array (
    'basicType' => 'textfield',
  ),
  'dateCreated' => 
  array (
    'basicType' => 'datetime',
  ),
  'dateUpdated' => 
  array (
    'basicType' => 'datetime',
  ),
  'acceptsMarketing' => 
  array (
    'basicType' => 'checkbox',
  ),
  'address1' => 
  array (
    'basicType' => 'textfield',
  ),
  'address2' => 
  array (
    'basicType' => 'textfield',
  ),
  'city' => 
  array (
    'basicType' => 'textfield',
  ),
  'region' => 
  array (
    'basicType' => 'textfield',
  ),
  'postalCode' => 
  array (
    'basicType' => 'textfield',
  ),
  'telno' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
  'title' => 
  array (
    'enumNamespace' => '\\Core\\Classes\\Enums\\Titles',
  ),
  'gender' => 
  array (
    'enumNamespace' => '\\Core\\Classes\\Enums\\Genders',
  ),
  'country' => 
  array (
    'enumNamespace' => '\\Core\\Classes\\Enums\\Countries',
  ),
);
    }

    protected static function internal_complexProperties() {
        return array (
  'userGroup' => 
  array (
    'type' => 'object',
    'childModelNamespace' => '\\Core\\Classes\\Models\\UserGroup',
  ),
  'language' => 
  array (
    'type' => 'object',
    'childModelNamespace' => '\\Core\\Classes\\Models\\Language',
  ),
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}