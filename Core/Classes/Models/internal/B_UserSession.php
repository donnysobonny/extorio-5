<?php
namespace Core\Classes\Models;
/**
 * The user's session
 *
 * Class B_UserSession
 */
class B_UserSession extends \Core\Classes\Commons\Model {

    
    /**
     * 
     *
     * @var integer
     */
    public $userId;
    
    /**
     * The actual session string
     *
     * @var string
     */
    public $session;
    
    /**
     * The ip address of the session
     *
     * @var string
     */
    public $remoteIP;
    
    /**
     * When the session expires
     *
     * @var string
     */
    public $expiryDate;
    

    protected static function internal_basicProperties() {
        return array (
  'userId' => 
  array (
    'basicType' => 'number',
  ),
  'session' => 
  array (
    'basicType' => 'textfield',
  ),
  'remoteIP' => 
  array (
    'basicType' => 'textfield',
  ),
  'expiryDate' => 
  array (
    'basicType' => 'datetime',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}