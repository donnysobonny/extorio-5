<?php
namespace Core\Classes\Models;
/**
 * A language entity
 *
 * Class B_Language
 */
class B_Language extends \Core\Classes\Commons\Model {

    
    /**
     * the name
     *
     * @var string
     */
    public $name;
    
    /**
     * The system locale
     *
     * @var string
     */
    public $sLocale;
    
    /**
     * The microsoft locale
     *
     * @var string
     */
    public $mLocale;
    
    /**
     * Whether this language is enabled
     *
     * @var bool
     */
    public $enabled;
    
    /**
     * 
     *
     * @var string
     */
    public $image;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'sLocale' => 
  array (
    'basicType' => 'textfield',
  ),
  'mLocale' => 
  array (
    'basicType' => 'textfield',
  ),
  'enabled' => 
  array (
    'basicType' => 'checkbox',
  ),
  'image' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}