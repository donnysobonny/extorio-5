<?php
namespace Core\Classes\Models;
/**
 * A model webhook is used to make external notifications that a model instance has been created, updated or deleted.
 *
 * Class B_ModelWebhook
 */
class B_ModelWebhook extends \Core\Classes\Commons\Model {

    
    /**
     * A descriptive name for the webhook
     *
     * @var string
     */
    public $name;
    
    /**
     * The action that triggers this webhook
     *
     * @var string
     */
    public $action;
    
    /**
     * The namespace of the model
     *
     * @var string
     */
    public $modelNamespace;
    
    /**
     * The location to send the webhook
     *
     * @var string
     */
    public $location;
    
    /**
     * A verification key to send with the webhook to be able to verify that the webhook is authentic at the other end. If this is left blank, we will use the key set in the config
     *
     * @var string
     */
    public $verificationKey;
    
    /**
     * The name of the extension that owns this webhook
     *
     * @var string
     */
    public $extensionName;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'modelNamespace' => 
  array (
    'basicType' => 'textfield',
  ),
  'location' => 
  array (
    'basicType' => 'textfield',
  ),
  'verificationKey' => 
  array (
    'basicType' => 'textfield',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
  'action' => 
  array (
    'enumNamespace' => '\\Core\\Classes\\Enums\\ModelWebhookActions',
  ),
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}