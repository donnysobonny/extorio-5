<?php
namespace Core\Classes\Models;
/**
 * 
 *
 * Class B_UserGroup
 */
class B_UserGroup extends \Core\Classes\Commons\Model {

    
    /**
     * 
     *
     * @var string
     */
    public $name;
    
    /**
     * 
     *
     * @var string
     */
    public $description;
    
    /**
     * 
     *
     * @var \Core\Classes\Models\UserPrivilege[]
     */
    public $privileges;
    
    /**
     * 
     *
     * @var bool
     */
    public $manageAll;
    
    /**
     * 
     *
     * @var \Core\Classes\Models\UserGroup[]
     */
    public $manageGroups;
    
    /**
     * 
     *
     * @var string
     */
    public $extensionName;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'manageAll' => 
  array (
    'basicType' => 'checkbox',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
  'privileges' => 
  array (
    'type' => 'array',
    'childModelNamespace' => '\\Core\\Classes\\Models\\UserPrivilege',
  ),
  'manageGroups' => 
  array (
    'type' => 'array',
    'childModelNamespace' => '\\Core\\Classes\\Models\\UserGroup',
  ),
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}