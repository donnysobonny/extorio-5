<?php
namespace Core\Classes\Models;
/**
 * An enum is mainly used as a model property, and is similar to a SET, resembling a set of strict values that a property can be. It also closely resembles an enum in OOP
 *
 * Class B_Enum
 */
class B_Enum extends \Core\Classes\Commons\Model {

    
    /**
     * 
     *
     * @var string
     */
    public $name;
    
    /**
     * The label. This can be changed
     *
     * @var string
     */
    public $label;
    
    /**
     * The class, this cannot be changed
     *
     * @var string
     */
    public $class;
    
    /**
     * The namespace
     *
     * @var string
     */
    public $namespace;
    
    /**
     * A description
     *
     * @var string
     */
    public $description;
    
    /**
     * The extension that owns this enum
     *
     * @var string
     */
    public $extensionName;
    
    /**
     * Whether this enum is hidden from the enum management page
     *
     * @var bool
     */
    public $isHidden;
    
    /**
     * The values of the enum
     *
     * @var array
     */
    public $values;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'label' => 
  array (
    'basicType' => 'textfield',
  ),
  'class' => 
  array (
    'basicType' => 'textfield',
  ),
  'namespace' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
  'isHidden' => 
  array (
    'basicType' => 'checkbox',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
  0 => 'values',
);
    }
}