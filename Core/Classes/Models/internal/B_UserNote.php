<?php
namespace Core\Classes\Models;
/**
 * 
 *
 * Class B_UserNote
 */
class B_UserNote extends \Core\Classes\Commons\Model {

    
    /**
     * 
     *
     * @var integer
     */
    public $userId;
    
    /**
     * 
     *
     * @var integer
     */
    public $updatedById;
    
    /**
     * 
     *
     * @var string
     */
    public $dateUpdated;
    
    /**
     * 
     *
     * @var string
     */
    public $body;
    

    protected static function internal_basicProperties() {
        return array (
  'userId' => 
  array (
    'basicType' => 'number',
  ),
  'updatedById' => 
  array (
    'basicType' => 'number',
  ),
  'dateUpdated' => 
  array (
    'basicType' => 'datetime',
  ),
  'body' => 
  array (
    'basicType' => 'textarea',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}