<?php
namespace Core\Classes\Models;
/**
 * This model represents an email transport that can be reviewed and re-sent
 *
 * Class B_EmailTransport
 */
class B_EmailTransport extends \Core\Classes\Commons\Model {

    
    /**
     * The email's subject
     *
     * @var string
     */
    public $subject;
    
    /**
     * The body of the email, if no template is being used
     *
     * @var string
     */
    public $body;
    
    /**
     * A set of key => value pairs to be used in the replacement of replacers in the body of the transport
     *
     * @var array
     */
    public $mergeVars;
    
    /**
     * A transport can specify the use of an email template to generate it's content, rather than set the content manually
     *
     * @var \Core\Classes\Models\EmailTemplate
     */
    public $template;
    
    /**
     * The name of the sender
     *
     * @var string
     */
    public $fromName;
    
    /**
     * The email of the sender
     *
     * @var string
     */
    public $fromEmail;
    
    /**
     * The name of the receiver
     *
     * @var string
     */
    public $toName;
    
    /**
     * The email of the receiver
     *
     * @var string
     */
    public $toEmail;
    
    /**
     * An email => name collection of recipients to be cc into the email
     *
     * @var array
     */
    public $cc;
    
    /**
     * An email => name collection of recipients to be bcc into the email
     *
     * @var array
     */
    public $bcc;
    
    /**
     * The date the transport was created
     *
     * @var string
     */
    public $dateCreated;
    
    /**
     * The date the transport was last sent (even if unsuccessful)
     *
     * @var string
     */
    public $dateSent;
    
    /**
     * The number of times this transport has been sent to a recipient
     *
     * @var integer
     */
    public $numSent;
    
    /**
     * If the transport fails to send internally, the error message will be found here
     *
     * @var string
     */
    public $errorMessage;
    

    protected static function internal_basicProperties() {
        return array (
  'subject' => 
  array (
    'basicType' => 'textfield',
  ),
  'body' => 
  array (
    'basicType' => 'textarea',
  ),
  'fromName' => 
  array (
    'basicType' => 'textfield',
  ),
  'fromEmail' => 
  array (
    'basicType' => 'textfield',
  ),
  'toName' => 
  array (
    'basicType' => 'textfield',
  ),
  'toEmail' => 
  array (
    'basicType' => 'textfield',
  ),
  'dateCreated' => 
  array (
    'basicType' => 'datetime',
  ),
  'dateSent' => 
  array (
    'basicType' => 'datetime',
  ),
  'numSent' => 
  array (
    'basicType' => 'number',
  ),
  'errorMessage' => 
  array (
    'basicType' => 'textarea',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
  'template' => 
  array (
    'type' => 'object',
    'childModelNamespace' => '\\Core\\Classes\\Models\\EmailTemplate',
  ),
);
    }

    protected static function internal_metaProperties() {
        return array (
  0 => 'mergeVars',
  1 => 'cc',
  2 => 'bcc',
);
    }
}