<?php
namespace Core\Classes\Models;
/**
 * A base translation
 *
 * Class B_TranslationBase
 */
class B_TranslationBase extends \Core\Classes\Commons\Model {

    
    /**
     * 
     *
     * @var bool
     */
    public $plural;
    
    /**
     * 
     *
     * @var string
     */
    public $msgid;
    
    /**
     * 
     *
     * @var string
     */
    public $msgid_plural;
    
    /**
     * 
     *
     * @var string
     */
    public $comments;
    
    /**
     * 
     *
     * @var string
     */
    public $extensionName;
    

    protected static function internal_basicProperties() {
        return array (
  'plural' => 
  array (
    'basicType' => 'checkbox',
  ),
  'msgid' => 
  array (
    'basicType' => 'textfield',
  ),
  'msgid_plural' => 
  array (
    'basicType' => 'textfield',
  ),
  'comments' => 
  array (
    'basicType' => 'textfield',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}