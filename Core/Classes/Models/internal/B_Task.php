<?php
namespace Core\Classes\Models;
/**
 * Resembles a task module
 *
 * Class B_Task
 */
class B_Task extends \Core\Classes\Commons\Model {

    
    /**
     * 
     *
     * @var string
     */
    public $name;
    
    /**
     * The label of the task, can be changed
     *
     * @var string
     */
    public $label;
    
    /**
     * The class of the task, cannot be changed
     *
     * @var string
     */
    public $class;
    
    /**
     * The task namespace
     *
     * @var string
     */
    public $namespace;
    
    /**
     * A description of the task
     *
     * @var string
     */
    public $description;
    
    /**
     * The extension that owns this task
     *
     * @var string
     */
    public $extensionName;
    
    /**
     * 
     *
     * @var bool
     */
    public $allRead;
    
    /**
     * 
     *
     * @var \Core\Classes\Models\UserGroup[]
     */
    public $readGroups;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'label' => 
  array (
    'basicType' => 'textfield',
  ),
  'class' => 
  array (
    'basicType' => 'textfield',
  ),
  'namespace' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
  'allRead' => 
  array (
    'basicType' => 'checkbox',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
  'readGroups' => 
  array (
    'type' => 'array',
    'childModelNamespace' => '\\Core\\Classes\\Models\\UserGroup',
  ),
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}