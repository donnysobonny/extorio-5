<?php
namespace Core\Classes\Models;
/**
 * This model is used to record whether an extension is enabled/disabled
 *
 * Class B_Extension
 */
class B_Extension extends \Core\Classes\Commons\Model {

    
    /**
     * The name of the extension. This should be class-name safe
     *
     * @var string
     */
    public $name;
    
    /**
     * The namespace of the Extension component
     *
     * @var string
     */
    public $namespace;
    
    /**
     * The currently patched build of the extension
     *
     * @var integer
     */
    public $build;
    
    /**
     * Whether the extension is installed
     *
     * @var bool
     */
    public $isInstalled;
    
    /**
     * Whether or not this extension is internal
     *
     * @var bool
     */
    public $isInternal;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'namespace' => 
  array (
    'basicType' => 'textfield',
  ),
  'build' => 
  array (
    'basicType' => 'number',
  ),
  'isInstalled' => 
  array (
    'basicType' => 'checkbox',
  ),
  'isInternal' => 
  array (
    'basicType' => 'checkbox',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}