<?php
namespace Core\Classes\Models;
/**
 * A theme resembles the styling applied to a page. It is ultimately a styling framework for front end developers
 *
 * Class B_Theme
 */
class B_Theme extends \Core\Classes\Commons\Model {

    
    /**
     * The name of the theme
     *
     * @var string
     */
    public $name;
    
    /**
     * 
     *
     * @var string
     */
    public $label;
    
    /**
     * A description of the theme
     *
     * @var string
     */
    public $description;
    
    /**
     * Whether this is the default theme
     *
     * @var bool
     */
    public $default;
    
    /**
     * The extension that owns the theme
     *
     * @var string
     */
    public $extensionName;
    
    /**
     * Whether the theme is hidden from the theme management page
     *
     * @var bool
     */
    public $isHidden;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'label' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'default' => 
  array (
    'basicType' => 'checkbox',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
  'isHidden' => 
  array (
    'basicType' => 'checkbox',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}