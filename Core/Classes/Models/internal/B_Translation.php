<?php
namespace Core\Classes\Models;
/**
 * A translation
 *
 * Class B_Translation
 */
class B_Translation extends \Core\Classes\Commons\Model {

    
    /**
     * 
     *
     * @var integer
     */
    public $baseId;
    
    /**
     * 
     *
     * @var integer
     */
    public $languageId;
    
    /**
     * 
     *
     * @var string
     */
    public $msgstr;
    
    /**
     * 
     *
     * @var string
     */
    public $msgstr_plural;
    

    protected static function internal_basicProperties() {
        return array (
  'baseId' => 
  array (
    'basicType' => 'number',
  ),
  'languageId' => 
  array (
    'basicType' => 'number',
  ),
  'msgstr' => 
  array (
    'basicType' => 'textfield',
  ),
  'msgstr_plural' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}