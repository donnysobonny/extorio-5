<?php
namespace Core\Classes\Models;
/**
 * 
 *
 * Class B_UserActionType
 */
class B_UserActionType extends \Core\Classes\Commons\Model {

    
    /**
     * 
     *
     * @var string
     */
    public $name;
    
    /**
     * 
     *
     * @var string
     */
    public $description;
    
    /**
     * 
     *
     * @var string
     */
    public $detail1Description;
    
    /**
     * 
     *
     * @var string
     */
    public $detail2Description;
    
    /**
     * 
     *
     * @var string
     */
    public $detail3Description;
    
    /**
     * 
     *
     * @var string
     */
    public $extensionName;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'detail1Description' => 
  array (
    'basicType' => 'textarea',
  ),
  'detail2Description' => 
  array (
    'basicType' => 'textarea',
  ),
  'detail3Description' => 
  array (
    'basicType' => 'textarea',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}