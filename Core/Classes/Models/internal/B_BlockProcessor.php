<?php
namespace Core\Classes\Models;
/**
 * A block processor is used by a block to output content
 *
 * Class B_BlockProcessor
 */
class B_BlockProcessor extends \Core\Classes\Commons\Model {

    
    /**
     * 
     *
     * @var string
     */
    public $name;
    
    /**
     * The label. This can be changed
     *
     * @var string
     */
    public $label;
    
    /**
     * The class. This cannot be changed
     *
     * @var string
     */
    public $class;
    
    /**
     * The namespace
     *
     * @var string
     */
    public $namespace;
    
    /**
     * The description
     *
     * @var string
     */
    public $description;
    
    /**
     * The extension that owns this block processor
     *
     * @var string
     */
    public $extensionName;
    
    /**
     * Whether or not this processor can be used inline
     *
     * @var bool
     */
    public $inlineEnabled;
    
    /**
     * 
     *
     * @var bool
     */
    public $allInline;
    
    /**
     * 
     *
     * @var \Core\Classes\Models\UserGroup[]
     */
    public $inlineGroups;
    
    /**
     * 
     *
     * @var \Core\Classes\Models\BlockProcessorCategory
     */
    public $category;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'label' => 
  array (
    'basicType' => 'textfield',
  ),
  'class' => 
  array (
    'basicType' => 'textfield',
  ),
  'namespace' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
  'inlineEnabled' => 
  array (
    'basicType' => 'checkbox',
  ),
  'allInline' => 
  array (
    'basicType' => 'checkbox',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
  'inlineGroups' => 
  array (
    'type' => 'array',
    'childModelNamespace' => '\\Core\\Classes\\Models\\UserGroup',
  ),
  'category' => 
  array (
    'type' => 'object',
    'childModelNamespace' => '\\Core\\Classes\\Models\\BlockProcessorCategory',
  ),
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}