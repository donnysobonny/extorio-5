<?php
namespace Core\Classes\Models;
/**
 * 
 *
 * Class B_UserPrivilege
 */
class B_UserPrivilege extends \Core\Classes\Commons\Model {

    
    /**
     * 
     *
     * @var string
     */
    public $name;
    
    /**
     * 
     *
     * @var string
     */
    public $description;
    
    /**
     * 
     *
     * @var \Core\Classes\Models\UserPrivilegeCategory
     */
    public $category;
    
    /**
     * 
     *
     * @var string
     */
    public $extensionName;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
  'category' => 
  array (
    'type' => 'object',
    'childModelNamespace' => '\\Core\\Classes\\Models\\UserPrivilegeCategory',
  ),
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}