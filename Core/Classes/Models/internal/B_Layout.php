<?php
namespace Core\Classes\Models;
/**
 * Building complex layouts can sometimes be quite time consuming. For this reason, this model allows you to create pre-defined layouts that can be loaded in the edit view
 *
 * Class B_Layout
 */
class B_Layout extends \Core\Classes\Commons\Model {

    
    /**
     * The name of the layout
     *
     * @var string
     */
    public $name;
    
    /**
     * The description
     *
     * @var string
     */
    public $description;
    
    /**
     * The extension that owns the layout
     *
     * @var string
     */
    public $extensionName;
    
    /**
     * The actual layout
     *
     * @var array
     */
    public $layout;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
  0 => 'layout',
);
    }
}