<?php
namespace Core\Classes\Models;
/**
 * 
 *
 * Class B_BlockCategory
 */
class B_BlockCategory extends \Core\Classes\Commons\Model {

    
    /**
     * 
     *
     * @var string
     */
    public $name;
    
    /**
     * 
     *
     * @var integer
     */
    public $parentId;
    
    /**
     * 
     *
     * @var string
     */
    public $description;
    
    /**
     * 
     *
     * @var string
     */
    public $icon;
    
    /**
     * 
     *
     * @var string
     */
    public $extensionName;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'parentId' => 
  array (
    'basicType' => 'number',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
  'icon' => 
  array (
    'enumNamespace' => '\\Core\\Classes\\Enums\\FontAwesomeIcons',
  ),
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}