<?php
namespace Core\Classes\Models;
/**
 * A contact made with a user
 *
 * Class B_UserContact
 */
class B_UserContact extends \Core\Classes\Commons\Model {

    
    /**
     * 
     *
     * @var integer
     */
    public $userId;
    
    /**
     * 
     *
     * @var integer
     */
    public $byUserId;
    
    /**
     * 
     *
     * @var string
     */
    public $type;
    
    /**
     * 
     *
     * @var \Core\Classes\Models\UserContactTopic
     */
    public $topic;
    
    /**
     * 
     *
     * @var string
     */
    public $identifier;
    
    /**
     * 
     *
     * @var string
     */
    public $date;
    
    /**
     * 
     *
     * @var string
     */
    public $time;
    

    protected static function internal_basicProperties() {
        return array (
  'userId' => 
  array (
    'basicType' => 'number',
  ),
  'byUserId' => 
  array (
    'basicType' => 'number',
  ),
  'identifier' => 
  array (
    'basicType' => 'textfield',
  ),
  'date' => 
  array (
    'basicType' => 'date',
  ),
  'time' => 
  array (
    'basicType' => 'time',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
  'type' => 
  array (
    'enumNamespace' => '\\Core\\Classes\\Enums\\UserContactType',
  ),
);
    }

    protected static function internal_complexProperties() {
        return array (
  'topic' => 
  array (
    'type' => 'object',
    'childModelNamespace' => '\\Core\\Classes\\Models\\UserContactTopic',
  ),
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}