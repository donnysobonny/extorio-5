<?php
namespace Core\Classes\Models;
/**
 * An api component
 *
 * Class B_Api
 */
class B_Api extends \Core\Classes\Commons\Model {

    
    /**
     * 
     *
     * @var string
     */
    public $name;
    
    /**
     * The label, used to access the api. This can be changed
     *
     * @var string
     */
    public $label;
    
    /**
     * The class of the api component. Cannot be changed
     *
     * @var string
     */
    public $class;
    
    /**
     * The namespace of the api component
     *
     * @var string
     */
    public $namespace;
    
    /**
     * The description
     *
     * @var string
     */
    public $description;
    
    /**
     * The extension name that this api belongs to
     *
     * @var string
     */
    public $extensionName;
    
    /**
     * 
     *
     * @var bool
     */
    public $allRead;
    
    /**
     * 
     *
     * @var \Core\Classes\Models\UserGroup[]
     */
    public $readGroups;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'label' => 
  array (
    'basicType' => 'textfield',
  ),
  'class' => 
  array (
    'basicType' => 'textfield',
  ),
  'namespace' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'extensionName' => 
  array (
    'basicType' => 'textfield',
  ),
  'allRead' => 
  array (
    'basicType' => 'checkbox',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
  'readGroups' => 
  array (
    'type' => 'array',
    'childModelNamespace' => '\\Core\\Classes\\Models\\UserGroup',
  ),
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}