<?php
namespace Core\Classes\Models;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Enums\InternalUserGroups;
use Core\Classes\Utilities\Users;

/**
 * 
 *
 * Class UserGroup
 */
class UserGroup extends B_UserGroup {

    /**
     * Quick construct
     *
     * @param string $name
     * @param string $description
     * @param int[]|UserPrivilege[] $privileges
     * @param bool $manageAll
     * @param int[]|UserGroup[] $manageGroups
     * @param string $extensionName
     *
     * @return UserGroup
     */
    public static function q(
        $name,
        $description,
        $privileges = array(),
        $manageAll = false,
        $manageGroups = array(),
        $extensionName = "Application"
    ) {
        $o = UserGroup::n();
        $o->name = $name;
        $o->description = $description;
        $o->privileges = $privileges;
        $o->manageAll = $manageAll;
        $o->manageGroups = $manageGroups;
        $o->extensionName = $extensionName;

        return $o;
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {
        if(in_array($this->name,InternalUserGroups::values())) {
            throw new \Exception("Internal user groups cannot be deleted");
        }
        if(Users::getUserCountByUserGroup($this->id) > 0) {
            throw new \Exception("It's not possible to delete a user group that is currently being used by one or more users");
        }
    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->extensionName = $this->_old->extensionName;
        if(in_array($this->name,InternalUserGroups::values())) {
            $this->name = $this->_old->name;
        }
    }

    private function generalChecks() {
        if(!strlen($this->extensionName)) {
            $this->extensionName = InternalExtensions::_application;
        }

        if(!strlen($this->name)) {
            throw new \Exception("A user group must have a name");
        }
    }
}