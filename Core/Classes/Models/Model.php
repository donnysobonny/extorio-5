<?php
namespace Core\Classes\Models;

use Core\Classes\Commons\Extorio;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Enums\InternalExtensions;
use Core\Classes\Enums\PHPTypes;
use Core\Classes\Enums\PropertyBasicType;
use Core\Classes\Enums\PropertyType;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\Models;
use Core\Classes\Utilities\Strings;

final class Model extends B_Model {

    /**
     * Quick construct
     *
     * @param string $name
     * @param int[]|Property[] $properties
     * @param string $description
     * @param bool $isHidden
     * @param string $extensionName
     *
     * @return Model
     */
    public static function q($name, $properties = array(), $description, $isHidden = false, $extensionName = "Application") {
        $o = Model::n();
        $o->name = $name;
        $o->properties = $properties;
        $o->description = $description;
        $o->isHidden = $isHidden;
        $o->extensionName = $extensionName;

        return $o;
    }

    /**
     * Find by name
     *
     * @param string $name
     *
     * @return Model
     */
    public static function findByName($name) {
        return Model::findOne(Query::n()->where(array("name"=>$name)));
    }

    public static function findByNamespace($namespace) {
        return Model::findOne(Query::n()->where(array("namespace" => $namespace)));
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        if (!strlen($this->extensionName)) {
            $this->extensionName = "Application";
        }

        $this->fixLabel();

        if (!strlen($this->class)) {
            $this->class = Strings::classNameSafe($this->name);
        }
        $this->baseClass = "B_" . $this->class;
        $this->namespace = '\\' . $this->extensionName . '\Classes\Models\\' . $this->class;
        $this->baseNamespace = '\\' . $this->extensionName . '\Classes\Models\\' . $this->baseClass;
    }

    public function fixLabel() {
        if (!strlen($this->name)) {
            throw new \Exception("A model cannot exist without a name");
        }
        $this->label = Strings::labelSafe($this->name);
        $m = Model::findOne(Query::n()->where(array("label" => $this->label, "id" => array('$ne' => $this->id))), 1);
        if ($m) {
            throw new \Exception("A model already exists with the label " . $this->label);
        }
    }

    protected function afterCreate() {
        $this->updateClassFiles();
        $this->createTable();
        $this->createSequence();
    }

    public function updateClassFiles() {
        $extension = Extorio::get()->getExtension($this->extensionName);
        if ($extension) {
            $baseFile = $extension->_findClassFile($this->baseNamespace);
            if (!$baseFile) {
                $baseFile = File::createFromPath($extension->_absoluteRoot . "/Classes/Models/internal/" . $this->baseClass . ".php");
            }
            $modelFile = $extension->_findClassFile($this->namespace);
            if (!$modelFile) {
                $modelFile = File::createFromPath($extension->_absoluteRoot . "/Classes/Models/" . $this->class . ".php");
            } else {
                $modelFile = false;
            }
            #
            # working on the base file
            #
            $content = File::constructFromPath("Core/Assets/file-templates/B_Model_template.txt")->read();
            $content = str_replace("*|EXTENSION_NAME|*", $this->extensionName, $content);
            $content = str_replace("*|MODEL_TYPE|*", $this->baseClass, $content);
            $propertyDefinitions = "";
            $basicProperties = array();
            $enumProperties = array();
            $complexProperties = array();
            $metaProperties = array();
            if ($this->properties) {
                foreach ($this->properties as $property) {
                    switch ($property->propertyType) {
                        case PropertyType::_basic :
                            $basicProperties[$property->label] = array("basicType" => $property->basicType);
                            switch ($property->basicType) {
                                case PropertyBasicType::_checkbox :
                                    $propertyDefinitions .= "
    /**
     * " . $property->description . "
     *
     * @var bool
     */
    public $" . $property->label . ";
    ";
                                    break;
                                case PropertyBasicType::_email :
                                    $propertyDefinitions .= "
    /**
     * " . $property->description . "
     *
     * @var string
     */
    public $" . $property->label . ";
    ";
                                    break;
                                case PropertyBasicType::_password :
                                    $propertyDefinitions .= "
    /**
     * " . $property->description . "
     *
     * @var string
     */
    public $" . $property->label . ";
    ";
                                    break;
                                case PropertyBasicType::_textfield :
                                    $propertyDefinitions .= "
    /**
     * " . $property->description . "
     *
     * @var string
     */
    public $" . $property->label . ";
    ";
                                    break;
                                case PropertyBasicType::_textarea :
                                    $propertyDefinitions .= "
    /**
     * " . $property->description . "
     *
     * @var string
     */
    public $" . $property->label . ";
    ";
                                    break;
                                case PropertyBasicType::_number :
                                    $propertyDefinitions .= "
    /**
     * " . $property->description . "
     *
     * @var integer
     */
    public $" . $property->label . ";
    ";
                                    break;
                                case PropertyBasicType::_decimal :
                                    $propertyDefinitions .= "
    /**
     * " . $property->description . "
     *
     * @var double
     */
    public $" . $property->label . ";
    ";
                                    break;
                                case PropertyBasicType::_date :
                                    $propertyDefinitions .= "
    /**
     * " . $property->description . "
     *
     * @var string
     */
    public $" . $property->label . ";
    ";
                                    break;
                                case PropertyBasicType::_time :
                                    $propertyDefinitions .= "
    /**
     * " . $property->description . "
     *
     * @var string
     */
    public $" . $property->label . ";
    ";
                                    break;
                                case PropertyBasicType::_datetime :
                                    $propertyDefinitions .= "
    /**
     * " . $property->description . "
     *
     * @var string
     */
    public $" . $property->label . ";
    ";
                                    break;
                            }
                            break;
                        case PropertyType::_enum :
                            $enumProperties[$property->label] = array("enumNamespace" => $property->enumNamespace);
                            $propertyDefinitions .= "
    /**
     * " . $property->description . "
     *
     * @var string
     */
    public $" . $property->label . ";
    ";
                            break;
                        case PropertyType::_complex :
                            $complexProperties[$property->label] = array("type" => $property->complexType, "childModelNamespace" => $property->childModelNamespace);
                            $type = $property->childModelNamespace;
                            if ($property->complexType == PHPTypes::_array) {
                                $type .= "[]";
                            }
                            $propertyDefinitions .= "
    /**
     * " . $property->description . "
     *
     * @var " . Strings::startsWith($type, "\\") . "
     */
    public $" . $property->label . ";
    ";
                            break;
                        case PropertyType::_meta :
                            $metaProperties[] = $property->label;
                            $propertyDefinitions .= "
    /**
     * " . $property->description . "
     *
     * @var array
     */
    public $" . $property->label . ";
    ";
                            break;
                    }
                }
            }

            $content = str_replace("*|DESCRIPTION|*", $this->description, $content);

            $content = str_replace("*|PROPERTY_DEFINITIONS|*", $propertyDefinitions, $content);
            $content = str_replace("*|BASIC_PROPERTIES|*", var_export($basicProperties, true), $content);
            $content = str_replace("*|ENUM_PROPERTIES|*", var_export($enumProperties, true), $content);
            $content = str_replace("*|COMPLEX_PROPERTIES|*", var_export($complexProperties, true), $content);
            $content = str_replace("*|META_PROPERTIES|*", var_export($metaProperties, true), $content);

            $baseFile->write($content);
            #
            # working on the model file
            #
            if ($modelFile) {
                $content = File::constructFromPath("Core/Assets/file-templates/Model_template.txt")->read();
                $content = str_replace("*|DESCRIPTION|*", $this->description, $content);
                $content = str_replace("*|EXTENSION_NAME|*", $this->extensionName, $content);
                $content = str_replace("*|MODEL_TYPE|*", $this->class, $content);
                $modelFile->write($content);
            }
        }
    }

    public function createTable() {
        $db = Extorio::get()->getDbInstanceDefault();
        $tableName = Models::getTableName($this->namespace);
        //generate the sql
        $columns = "";
        if ($this->properties) {
            foreach ($this->properties as $property) {
                if ($property->name != "id") {
                    switch ($property->propertyType) {
                        case PropertyType::_basic :
                            $columns .= '"' . $property->label . '" ';
                            switch ($property->basicType) {
                                case PropertyBasicType::_checkbox :
                                    $columns .= "BOOLEAN,";
                                    break;
                                case PropertyBasicType::_email :
                                    $columns .= "TEXT,";
                                    break;
                                case PropertyBasicType::_password :
                                    $columns .= "TEXT,";
                                    break;
                                case PropertyBasicType::_textfield :
                                    $columns .= "TEXT,";
                                    break;
                                case PropertyBasicType::_textarea :
                                    $columns .= "TEXT,";
                                    break;
                                case PropertyBasicType::_number :
                                    $columns .= "INTEGER,";
                                    break;
                                case PropertyBasicType::_decimal :
                                    $columns .= "DOUBLE PRECISION,";
                                    break;
                                case PropertyBasicType::_date :
                                    $columns .= "DATE,";
                                    break;
                                case PropertyBasicType::_time :
                                    $columns .= "TIME,";
                                    break;
                                case PropertyBasicType::_datetime :
                                    $columns .= "TIMESTAMP,";
                                    break;
                            }
                            break;
                        case PropertyType::_complex :
                            $columns .= '"' . $property->label . '" ';
                            switch ($property->complexType) {
                                case PHPTypes::_object :
                                    $columns .= "INTEGER,";
                                    break;
                                case PHPTypes::_array :
                                    $columns .= "INTEGER[],";
                                    break;
                            }
                            break;
                        case PropertyType::_enum :
                            $columns .= '"' . $property->label . '" ';
                            $columns .= "TEXT,";
                            break;
                        case PropertyType::_meta :
                            $columns .= '"' . $property->label . '" ';
                            $columns .= "JSON,";
                            break;
                    }
                }
            }
        }
        if (strlen($columns)) {
            $columns = "," . substr($columns, 0, strlen($columns) - 1);
        }
        $sql = '
        CREATE TABLE IF NOT EXISTS "' . $tableName . '" (
            id integer CONSTRAINT ' . $tableName . '_id_pk PRIMARY KEY
            ' . $columns . '
        )
        ';
        //create the table
        $db->query($sql);
        //create the indexes
        if ($this->properties) {
            foreach ($this->properties as $property) {
                if ($property->isIndex) {
                    $name = strtolower($property->label);
                    $sql = 'CREATE INDEX "' . $tableName . "_" . $name . '_index" ON "' . $tableName . '" (
                        "' . $property->label . '" ASC NULLS LAST
                    )';
                    $db->query($sql);
                }
            }
        }
    }

    public function createSequence() {
        $db = Extorio::get()->getDbInstanceDefault();
        $sequenceName = Models::getSequenceName($this->namespace);
        $sql = 'CREATE SEQUENCE "' . $sequenceName . '"';
        $db->query($sql);
    }

    protected function beforeUpdate() {
        $this->extensionName = $this->_old->extensionName;
        $this->fixLabel();

        if (!strlen($this->class)) {
            $this->class = Strings::classNameSafe($this->name);
        }
        $this->baseClass = "B_" . $this->class;
        $this->namespace = '\\' . $this->extensionName . '\Classes\Models\\' . $this->class;
        $this->baseNamespace = '\\' . $this->extensionName . '\Classes\Models\\' . $this->baseClass;
    }

    protected function afterUpdate() {
        $this->updateClassFiles();
        $this->updateTable();
        //remove any loose properties
        $newIds = array();
        foreach ($this->properties as $p) {
            $newIds[] = $p->id;
        }
        foreach ($this->_old->properties as $p) {
            //if no longer in list of properties, delete
            if (!in_array($p->id, $newIds)) {
                $p->deleteThis();
            }
        }
    }

    public function updateTable() {
        $db = Extorio::get()->getDbInstanceDefault();
        $tableName = Models::getTableName($this->namespace);
        //get the current fields of the table
        $currentFields = array();
        $sql = "SELECT column_name FROM information_schema.columns WHERE table_name = $1";
        $rs = $db->query($sql, array($tableName));
        while ($row = $rs->fetchRow()) {
            $currentFields[] = $row[0];
        }
        $newProperties = array("id");
        foreach ($this->properties as $property) {
            if ($property->name != "id") {
                $newProperties[] = $property->label;
                //if property didn't used to exist, we need to add it
                if (!in_array($property->label, $currentFields)) {
                    $sql = 'ALTER TABLE "' . $tableName . '" ADD COLUMN "' . $property->label . '" ';
                    switch ($property->propertyType) {
                        case PropertyType::_basic :
                            switch ($property->basicType) {
                                case PropertyBasicType::_checkbox :
                                    $sql .= "BOOLEAN";
                                    break;
                                case PropertyBasicType::_email :
                                    $sql .= "TEXT";
                                    break;
                                case PropertyBasicType::_password :
                                    $sql .= "TEXT";
                                    break;
                                case PropertyBasicType::_textfield :
                                    $sql .= "TEXT";
                                    break;
                                case PropertyBasicType::_textarea :
                                    $sql .= "TEXT";
                                    break;
                                case PropertyBasicType::_number :
                                    $sql .= "INTEGER";
                                    break;
                                case PropertyBasicType::_decimal :
                                    $sql .= "DOUBLE PRECISION";
                                    break;
                                case PropertyBasicType::_date :
                                    $sql .= "DATE";
                                    break;
                                case PropertyBasicType::_time :
                                    $sql .= "TIME";
                                    break;
                                case PropertyBasicType::_datetime :
                                    $sql .= "TIMESTAMP";
                                    break;
                            }
                            break;
                        case PropertyType::_complex :
                            switch ($property->complexType) {
                                case PHPTypes::_object :
                                    $sql .= "INTEGER";
                                    break;
                                case PHPTypes::_array :
                                    $sql .= "INTEGER[]";
                                    break;
                            }
                            break;
                        case PropertyType::_enum :
                            $sql .= "TEXT";
                            break;
                        case PropertyType::_meta :
                            $sql .= "JSON";
                            break;
                    }
                    $db->query($sql);
                    //if the property is an index
                    if ($property->isIndex) {
                        $name = strtolower($property->label);
                        $sql = 'CREATE INDEX "' . $tableName . "_" . $name . '_index" ON "' . $tableName . '" (
                        "' . $property->label . '" ASC NULLS LAST
                        )';
                        $db->query($sql);
                    }
                } else {
                    //if the property's propertyType, basicType or complexType changes, we might need to change the type
                    if ($property->_old->propertyType != $property->propertyType || $property->_old->basicType != $property->basicType || $property->_old->complexType != $property->complexType) {
                        //TODO: look at whether we can make conversions between types. For now, we are simply clearing the field before we attempt the change the type
                        $sql = 'UPDATE "' . $tableName . '" SET "' . $property->label . '" = NULL';
                        $db->query($sql);
                        //TODO: we know that the type "text" can be converted to any type, where as some other types cannot be converted to some others. So we convert to text first
                        $sql = 'ALTER TABLE "' . $tableName . '" ALTER COLUMN "' . $property->label . '" TYPE TEXT USING ("' . $property->label . '"::text)';
                        $db->query($sql);
                        $sql = 'ALTER TABLE "' . $tableName . '" ALTER COLUMN "' . $property->label . '" TYPE ';
                        switch ($property->propertyType) {
                            case PropertyType::_basic :
                                switch ($property->basicType) {
                                    case PropertyBasicType::_checkbox :
                                        $sql .= 'BOOLEAN USING ("' . $property->label . '"::boolean)';
                                        break;
                                    case PropertyBasicType::_email :
                                        $sql .= 'TEXT USING ("' . $property->label . '"::text)';
                                        break;
                                    case PropertyBasicType::_password :
                                        $sql .= 'TEXT USING ("' . $property->label . '"::text)';
                                        break;
                                    case PropertyBasicType::_textfield :
                                        $sql .= 'TEXT USING ("' . $property->label . '"::text)';
                                        break;
                                    case PropertyBasicType::_textarea :
                                        $sql .= 'TEXT USING ("' . $property->label . '"::text)';
                                        break;
                                    case PropertyBasicType::_number :
                                        $sql .= 'INTEGER USING ("' . $property->label . '"::integer)';
                                        break;
                                    case PropertyBasicType::_decimal :
                                        $sql .= 'DOUBLE PRECISION USING ("' . $property->label . '"::double precision)';
                                        break;
                                    case PropertyBasicType::_date :
                                        $sql .= 'DATE USING ("' . $property->label . '"::date)';
                                        break;
                                    case PropertyBasicType::_time :
                                        $sql .= 'TIME USING ("' . $property->label . '"::time)';
                                        break;
                                    case PropertyBasicType::_datetime :
                                        $sql .= 'TIMESTAMP USING ("' . $property->label . '"::timestamp)';
                                        break;
                                }
                                break;
                            case PropertyType::_complex :
                                switch ($property->complexType) {
                                    case PHPTypes::_object :
                                        $sql .= 'INTEGER USING ("' . $property->label . '"::integer)';
                                        break;
                                    case PHPTypes::_array :
                                        $sql .= 'INTEGER[] USING ("' . $property->label . '"::integer[])';
                                        break;
                                }
                                break;
                            case PropertyType::_enum :
                                $sql .= 'TEXT USING ("' . $property->label . '"::text)';
                                break;
                            case PropertyType::_meta :
                                $sql .= 'JSON USING ("' . $property->label . '"::json)';
                                break;
                        }
                        $db->query($sql);
                    }
                    //if the index has gone from true to false, remove the index
                    if ($property->_old->isIndex == true && $property->isIndex == false) {
                        $sql = 'DROP INDEX "' . $tableName . "_" . strtolower($property->label) . '_index"';
                        $db->query($sql);
                    }
                    //if the index has gone from false to true, create the index
                    if ($property->_old->isIndex == false && $property->isIndex == true) {
                        $sql = 'CREATE INDEX "' . $tableName . "_" . strtolower($property->label) . '_index" ON "' . $tableName . '" (
                        "' . $property->label . '" ASC NULLS LAST
                        )';
                        $db->query($sql);
                    }
                }
            }
        }
        //for any of the current properties that were not in the new properties, delete them
        foreach ($currentFields as $field) {
            if (!in_array($field, $newProperties)) {
                $sql = 'ALTER TABLE "' . $tableName . '" DROP COLUMN "' . $field . '"';
                $db->query($sql);
            }
        }
    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {
        //remove properties
        if ($this->properties) {
            foreach ($this->properties as $property) {
                $property->deleteThis();
            }
        }
        //remove the table and sequence
        $this->deleteSequence();
        $this->deleteTable();
        //clear the modcache
        Models::clearModCacheByModel($this->namespace);
    }

    public function deleteSequence() {
        $db = Extorio::get()->getDbInstanceDefault();
        $table = Models::getSequenceName($this->namespace);
        $sql = 'DROP SEQUENCE "' . $table . '"';
        $db->query($sql);
    }

    public function deleteTable() {
        $db = Extorio::get()->getDbInstanceDefault();
        $table = Models::getTableName($this->namespace);
        $sql = 'DROP TABLE "' . $table . '"';
        $db->query($sql);
    }
}