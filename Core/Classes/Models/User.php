<?php
namespace Core\Classes\Models;
use Core\Classes\Commons;
use Core\Classes\Enums\Genders;
use Core\Classes\Enums\InternalEvents;
use Core\Classes\Enums\Titles;
use Core\Classes\Enums\UserAccessLevels;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\UserReportQuery;
use Core\Classes\Utilities\Users;

class User extends B_User {

    /**
     * Quick construct a User
     *
     * @param sting $username
     * @param string $password
     * @param string $email
     * @param int|UserGroup $userGroup
     * @param string $firstname
     * @param string $lastname
     * @param bool $canLogin
     *
     * @return User
     */
    public static function q(
        $username,
        $password,
        $email,
        $userGroup,
        $firstname = "",
        $lastname = "",
        $canLogin = true
    ) {
        $u = User::n();
        $u->username = $username;
        $u->password = $password;
        $u->email = $email;
        $u->userGroup = $userGroup;
        $u->firstname = $firstname;
        $u->lastname = $lastname;
        $u->canLogin = $canLogin;

        return $u;
    }

    /**
     * Find a set of users using a user report
     *
     * @param int $userReportId
     * @param int $limit
     * @param int $skip
     * @param int $depth
     *
     * @return User[]
     */
    public static function findByUserReport($userReportId,$limit=0,$skip=0,$depth=1) {
        $ur = UserReport::findById($userReportId);
        if(!$ur) {
            return array();
        }
        $urq = UserReportQuery::n();
        $urq->where($ur->query)->limit($limit)->skip($skip);
        return User::findByUserReportQuery($urq,$depth);
    }

    /**
     * Find the count using a user report
     *
     * @param int $userReportId
     * @param int $limit
     * @param int $skip
     *
     * @return int
     */
    public static function findCountByUserReport($userReportId,$limit=0,$skip=0) {
        $ur = UserReport::findById($userReportId);
        if(!$ur) {
            return 0;
        }
        $urq = UserReportQuery::n();
        $urq->where($ur->query)->limit($limit)->skip($skip);
        return User::findCountByUserReportQuery($urq);
    }

    /**
     * Find a set of users based on a user report query
     *
     * @param UserReportQuery $userReportQuery
     * @param int $depth
     *
     * @return User[]
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function findByUserReportQuery(UserReportQuery $userReportQuery,$depth=1) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        return User::findAllByResultSet($db->query($userReportQuery->getSql(),$userReportQuery->getParams()),$depth);
    }

    /**
     * Find the count using a user report query
     *
     * @param UserReportQuery $userReportQuery
     *
     * @return int
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function findCountByUserReportQuery(UserReportQuery $userReportQuery) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $row = $db->query($userReportQuery->getCountSql(),$userReportQuery->getParams())->fetchRow();
        if($row) {
            return intval($row[0]);
        }
        return 0;
    }

    /**
     * Get this user's profile url
     *
     * @return string
     */
    public function profileUrl() {
        return Users::getProfileUrl($this->id);
    }

    /**
     * Get the url to edit this user
     *
     * @return string
     */
    public function editUrl() {
        return Users::getEditUrl($this->id);
    }

    protected function beforeCreate() {
        $this->generalChecks();

        $this->dateCreated = date("Y-m-d H:i:s");
    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();

        $this->dateUpdated = date("Y-m-d H:i:s");
    }

    protected function afterDelete() {
        //run the delete event
        Commons\Extorio::get()->getEvent(InternalEvents::_user_delete)->run(array($this));

        //delete the session
        $sessions = UserSession::findAll(Query::n()->where(array("userId"=>$this->id)));
        foreach($sessions as $session) {
            $session->deleteThis();
        }
    }

    protected function afterCreate() {
        //run the create event
        Commons\Extorio::get()->getEvent(InternalEvents::_user_create)->run(array($this));
    }

    protected function afterUpdate() {
        //run the update event
        Commons\Extorio::get()->getEvent(InternalEvents::_user_update)->run(array($this));
    }


    private function updateLocks() {
        $config = Commons\Extorio::get()->getConfig();
        //check if username/email/password changes can be made
        if(!$config["users"]["can_change_username"]) {
            $this->username = $this->_old->username;
        }
        if(!$config["users"]["can_change_email"]) {
            $this->email = $this->_old->email;
        }
        if(!$config["users"]["can_change_password"]) {
            $this->password = $this->_old->password;
        }
    }

    private function generalChecks() {
        //if username and email empty
        if(!strlen($this->username) && !strlen($this->email)) {
            throw new \Exception("Username and email cannot be empty");
        } else {
            if(!strlen($this->username)) {
                $this->username = $this->email;
            } elseif(!strlen($this->email)) {
                $this->email = $this->username;
            }
        }

        //username and email must be unique
        if(User::findOne(
            Query::n()->where(array(
                '$and' => array(
                    '$or' => array(
                        "username" => $this->username,
                        "email" => $this->email
                    ),
                    "id" => array('$ne' => $this->id)
                )
            )),1
        )) {
            throw new \Exception("Username and/or email is already in use");
        }

        //password cannot be empty
        if(!strlen($this->password)) {
            throw new \Exception("Password cannot be empty");
        }

        if($this->gender == Genders::_unknown) {
            switch($this->title) {
                case Titles::_mr:
                    $this->gender = Genders::_male;
                    break;
                case Titles::_mrs:
                    $this->gender = Genders::_female;
                    break;
                case Titles::_miss:
                    $this->gender = Genders::_female;
                    break;
                case Titles::_ms:
                    $this->gender = Genders::_female;
                    break;
                case Titles::_master:
                    $this->gender = Genders::_male;
                    break;
            }
        }

        $config = Commons\Extorio::get()->getConfig();

        if(!$this->userGroup) {
            switch($config["users"]["user_groups"]["assign_type"]) {
                case "single" :
                    $this->userGroup = UserGroup::findById($config["users"]["user_groups"]["group"],INF);
                    break;
                case "random" :
                    $this->userGroup = UserGroup::findAll($config["users"]["user_groups"]["groups"][rand(0,count($config["users"]["user_groups"]["groups"])-1)],INF);
                    break;
                case "least" :
                    $db = Commons\Extorio::get()->getDbInstanceDefault();
                    $in = "";
                    foreach($config["users"]["user_groups"]["groups"] as $id) {
                        $in .= intval($id).",";
                    }
                    $in = substr($in,0,strlen($in)-1);
                    $sql = '
                    SELECT

                    "userGroup" FROM core_classes_models_user

                    WHERE "userGroup" IN ('.$in.')

                    GROUP BY "userGroup"

                    ORDER BY count(id) ASC

                    LIMIT 1
                    ';
                    $row = $db->query($sql)->fetchRow();
                    $this->userGroup = UserGroup::findById(intval($row[0]),INF);
                    break;
            }
        }

        //if the avatar not set, use the default
        if(!strlen($this->avatar)) {
            $this->avatar = $config["users"]["defaults"]["avatar"];
        }

        //set the short and long name
        $this->shortname = ucwords(strlen($this->firstname) ? $this->firstname : $this->username);
        if(strlen($this->firstname)) {
            $longName = $this->firstname;
            if(strlen($this->middlenames)) {
                $longName .= " ".$this->middlenames;
            }
            if(strlen($this->lastname)) {
                $longName .= " ".$this->lastname;
            }
        } else {
            $longName = $this->username;
        }
        $this->longname = ucwords($longName);
    }
}