<?php
namespace Core\Classes\Enums;
/**
 * The container type for a cell
 *
 * Class ContainerType
 */
class ContainerType {

	const _none = 'none';
	const _panelDefault = 'panel-default';
	const _panelPrimary = 'panel-primary';
	const _panelSuccess = 'panel-success';
	const _panelInfo = 'panel-info';
	const _panelWarning = 'panel-warning';
	const _panelDanger = 'panel-danger';
	const _wellSmall = 'well-small';
	const _wellMedium = 'well-medium';
	const _wellLarge = 'well-large';
	const _alertInfo = 'alert-info';
	const _alertSuccess = 'alert-success';
	const _alertWarning = 'alert-warning';
	const _alertDanger = 'alert-danger';
	const _jumbotronFluid = 'jumbotron-fluid';
	const _jumbotronFixed = 'jumbotron-fixed';
	const _thumbnail = 'thumbnail';

}