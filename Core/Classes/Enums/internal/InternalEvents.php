<?php
namespace Core\Classes\Enums;
/**
 * A list of events that are part of the core framework
 *
 * Class InternalEvents
 */
class InternalEvents extends \Core\Classes\Commons\Enum {

	const _on_exception = 'on_exception';
	const _user_login = 'user_login';
	const _user_logout = 'user_logout';
	const _user_update = 'user_update';
	const _user_delete = 'user_delete';
	const _user_create = 'user_create';
	const _extorio_admin_menu_left_menus = 'extorio_admin_menu_left_menus';
	const _extorio_admin_menu_left_items = 'extorio_admin_menu_left_items';
	const _extorio_admin_menu_top_menus = 'extorio_admin_menu_top_menus';
	const _extorio_admin_menu_top_items = 'extorio_admin_menu_top_items';
	const _extorio_on_start = 'extorio_on_start';
	const _extorio_on_end = 'extorio_on_end';
	const _default_nav_menu_items = 'default_nav_menu_items';
	const _user_list_columns = 'user_list_columns';
	const _user_list_cells = 'user_list_cells';
	const _my_account_controller = 'my_account_controller';
	const _my_account_view_tabs = 'my_account_view_tabs';
	const _my_account_view_content = 'my_account_view_content';
	const _user_profiles_controller = 'user_profiles_controller';
	const _user_profiles_view_tabs = 'user_profiles_view_tabs';
	const _user_profiles_view_content = 'user_profiles_view_content';
	const _theme_compile_less_files = 'theme_compile_less_files';
	const _theme_minify_css_files = 'theme_minify_css_files';
	const _theme_minify_js_files = 'theme_minify_js_files';
	const _user_actions_detail_display = 'user_actions_detail_display';
	const _user_actions_detail_filter = 'user_actions_detail_filter';
	const _user_edit_controller = 'user_edit_controller';
	const _user_edit_view_tabs = 'user_edit_view_tabs';
	const _user_edit_view_content = 'user_edit_view_content';
	const _user_login_controller = 'user_login_controller';
	const _user_login_view = 'user_login_view';
	const _user_register_controller = 'user_register_controller';
	const _user_register_view = 'user_register_view';
	const _user_create_controller = 'user_create_controller';
	const _user_create_view = 'user_create_view';

    public static function values() {
        return array (
  0 => 'on_exception',
  1 => 'user_login',
  2 => 'user_logout',
  3 => 'user_update',
  4 => 'user_delete',
  5 => 'user_create',
  6 => 'extorio_admin_menu_left_menus',
  7 => 'extorio_admin_menu_left_items',
  8 => 'extorio_admin_menu_top_menus',
  9 => 'extorio_admin_menu_top_items',
  10 => 'extorio_on_start',
  11 => 'extorio_on_end',
  12 => 'default_nav_menu_items',
  13 => 'user_list_columns',
  14 => 'user_list_cells',
  15 => 'my_account_controller',
  16 => 'my_account_view_tabs',
  17 => 'my_account_view_content',
  18 => 'user_profiles_controller',
  19 => 'user_profiles_view_tabs',
  20 => 'user_profiles_view_content',
  21 => 'theme_compile_less_files',
  22 => 'theme_minify_css_files',
  23 => 'theme_minify_js_files',
  24 => 'user_actions_detail_display',
  25 => 'user_actions_detail_filter',
  26 => 'user_edit_controller',
  27 => 'user_edit_view_tabs',
  28 => 'user_edit_view_content',
  29 => 'user_login_controller',
  30 => 'user_login_view',
  31 => 'user_register_controller',
  32 => 'user_register_view',
  33 => 'user_create_controller',
  34 => 'user_create_view',
);
    }
}