<?php
namespace Core\Classes\Enums;
class WebhookAction extends \Core\Classes\Commons\Enum {

	const _model_create = 'model_create';
	const _model_update = 'model_update';
	const _model_delete = 'model_delete';

    public static function values() {
        return array (
  0 => 'model_create',
  1 => 'model_update',
  2 => 'model_delete',
);
    }
}