<?php
namespace Core\Classes\Enums;
/**
 * 
 *
 * Class UserContactType
 */
class UserContactType extends \Core\Classes\Commons\Enum {

	const _unknown = 'Unknown';
	const _systemEmail = 'System Email';
	const _email = 'Email';
	const _phone = 'Phone';
	const _direct = 'Direct';

    public static function values() {
        return array (
  0 => 'Unknown',
  1 => 'System Email',
  2 => 'Email',
  3 => 'Phone',
  4 => 'Direct',
);
    }
}