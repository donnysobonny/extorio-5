<?php
namespace Core\Classes\Enums;
/**
 * A list of user titles
 *
 * Class Titles
 */
class Titles extends \Core\Classes\Commons\Enum {

	const _unknown = 'Unknown';
	const _mr = 'Mr';
	const _mrs = 'Mrs';
	const _miss = 'Miss';
	const _ms = 'Ms';
	const _master = 'Master';
	const _mx = 'Mx';

    public static function values() {
        return array (
  0 => 'Unknown',
  1 => 'Mr',
  2 => 'Mrs',
  3 => 'Miss',
  4 => 'Ms',
  5 => 'Master',
  6 => 'Mx',
);
    }
}