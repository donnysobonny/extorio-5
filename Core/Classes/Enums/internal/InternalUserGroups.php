<?php
namespace Core\Classes\Enums;
/**
 * 
 *
 * Class InternalUserGroups
 */
class InternalUserGroups extends \Core\Classes\Commons\Enum {

	const _superAdministrator = 'Super Administrator';
	const _administrator = 'Administrator';
	const _user = 'User';

    public static function values() {
        return array (
  0 => 'Super Administrator',
  1 => 'Administrator',
  2 => 'User',
);
    }
}