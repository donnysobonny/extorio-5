<?php
namespace Core\Classes\Enums;
class InternalExtensions extends \Core\Classes\Commons\Enum {

	const _core = 'Core';
	const _application = 'Application';

    public static function values() {
        return array (
  0 => 'Core',
  1 => 'Application',
);
    }
}