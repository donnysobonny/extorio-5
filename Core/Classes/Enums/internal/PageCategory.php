<?php
namespace Core\Classes\Enums;
/**
 * Page categories, used to help filter pages.
 *
 * Class PageCategory
 */
class PageCategory extends \Core\Classes\Commons\Enum {

	const _uncategorized = 'uncategorized';
	const _extorioPages = 'extorio pages';
	const _userPages = 'user pages';

    public static function values() {
        return array (
  0 => 'uncategorized',
  1 => 'extorio pages',
  2 => 'user pages',
);
    }
}