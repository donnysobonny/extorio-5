<?php
namespace Core\Classes\Enums;
/**
 * A list of internal pages
 *
 * Class InternalPages
 */
class InternalPages extends \Core\Classes\Commons\Enum {

	const _404PageNotFound = '/404-page-not-found/';
	const _401AccessDenied = '/401-access-denied/';
	const _myAccount = '/my-account/';
	const _userAuthentication = '/user-authentication/';
	const _userForgotPassword = '/user-forgot-password/';
	const _userLogin = '/user-login/';
	const _users = '/users/';
	const _userRegister = '/user-register/';
	const _userLogout = '/user-logout/';
	const _extorioAdminLogin = '/extorio-admin/login/';
	const _extorioAdmin401AccessDenied = '/extorio-admin/401-access-denied/';
	const _extorioAdminUsers = '/extorio-admin/users/';

    public static function values() {
        return array (
  0 => '/404-page-not-found/',
  1 => '/401-access-denied/',
  2 => '/my-account/',
  3 => '/user-authentication/',
  4 => '/user-forgot-password/',
  5 => '/user-login/',
  6 => '/users/',
  7 => '/user-register/',
  8 => '/user-logout/',
  9 => '/extorio-admin/login/',
  10 => '/extorio-admin/401-access-denied/',
  11 => '/extorio-admin/users/',
);
    }
}