<?php
namespace Core\Classes\Enums;
class PHPTypes extends \Core\Classes\Commons\Enum {

	const _array = 'array';
	const _boolean = 'boolean';
	const _double = 'double';
	const _float = 'float';
	const _integer = 'integer';
	const _object = 'object';
	const _nULL = 'NULL';
	const _resource = 'resource';
	const _string = 'string';
	const _unknownType = 'unknown type';

    public static function values() {
        return array (
  0 => 'array',
  1 => 'boolean',
  2 => 'double',
  3 => 'float',
  4 => 'integer',
  5 => 'object',
  6 => 'NULL',
  7 => 'resource',
  8 => 'string',
  9 => 'unknown type',
);
    }
}