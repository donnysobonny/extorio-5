<?php
namespace Core\Classes\Enums;
class PropertyType extends \Core\Classes\Commons\Enum {

	const _basic = 'basic';
	const _enum = 'enum';
	const _complex = 'complex';
	const _meta = 'meta';

    public static function values() {
        return array (
  0 => 'basic',
  1 => 'enum',
  2 => 'complex',
  3 => 'meta',
);
    }
}