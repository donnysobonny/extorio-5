<?php
namespace Core\Classes\Enums;
class PropertyBasicType extends \Core\Classes\Commons\Enum {

	const _checkbox = 'checkbox';
	const _email = 'email';
	const _password = 'password';
	const _textfield = 'textfield';
	const _textarea = 'textarea';
	const _number = 'number';
	const _decimal = 'decimal';
	const _date = 'date';
	const _time = 'time';
	const _datetime = 'datetime';

    public static function values() {
        return array (
  0 => 'checkbox',
  1 => 'email',
  2 => 'password',
  3 => 'textfield',
  4 => 'textarea',
  5 => 'number',
  6 => 'decimal',
  7 => 'date',
  8 => 'time',
  9 => 'datetime',
);
    }
}