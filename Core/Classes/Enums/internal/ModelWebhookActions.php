<?php
namespace Core\Classes\Enums;
class ModelWebhookActions extends \Core\Classes\Commons\Enum {

	const _create = 'create';
	const _update = 'update';
	const _delete = 'delete';

    public static function values() {
        return array (
  0 => 'create',
  1 => 'update',
  2 => 'delete',
);
    }
}