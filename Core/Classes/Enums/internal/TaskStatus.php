<?php
namespace Core\Classes\Enums;
class TaskStatus extends \Core\Classes\Commons\Enum {

	const _notStarted = 'not started';
	const _queued = 'queued';
	const _running = 'running';
	const _completed = 'completed';
	const _failed = 'failed';
	const _killed = 'killed';

    public static function values() {
        return array (
  0 => 'not started',
  1 => 'queued',
  2 => 'running',
  3 => 'completed',
  4 => 'failed',
  5 => 'killed',
);
    }
}