<?php
namespace Core\Classes\Enums;
/**
 * A list of user genders
 *
 * Class Genders
 */
class Genders extends \Core\Classes\Commons\Enum {

	const _unknown = 'Unknown';
	const _male = 'Male';
	const _female = 'Female';

    public static function values() {
        return array (
  0 => 'Unknown',
  1 => 'Male',
  2 => 'Female',
);
    }
}