<?php
namespace Core\Classes\Enums;
final class UserAccessLevels {
    const guest = 0;
    const user_reader = 1;
    const user_writer = 2;
    const user_modifier = 3;
    const user_remove = 4;
    const user_super = 5;
    const admin_reader = 6;
    const admin_writer = 7;
    const admin_modifier = 8;
    const admin_remover = 9;
    const admin_super = 10;
}