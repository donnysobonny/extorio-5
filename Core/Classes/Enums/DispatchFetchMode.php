<?php
namespace Core\Classes\Enums;
class DispatchFetchMode {
    const template_page_mvc = "template_page_mvc";
    const page_mvc = "page_mvc";
    const mvc = "mvc";
}