<?php
namespace Core\Classes\Enums;
class DispatchRenderMode {
    const html = "html";
    const plain = "plain";
}