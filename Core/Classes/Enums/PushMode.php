<?php
namespace Core\Classes\Enums;
class PushMode {
    const none = "none";
    const update = "update";
    const create = "create";
}