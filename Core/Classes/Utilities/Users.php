<?php
namespace Core\Classes\Utilities;
use Core\Classes\Commons;
use Core\Classes\Enums\InternalEvents;
use Core\Classes\Enums\PHPTypes;
use Core\Classes\Exceptions\User_Exception;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\UserReportQuery;
use Core\Classes\Models\EmailTransport;
use Core\Classes\Models\User;
use Core\Classes\Models\UserAction;
use Core\Classes\Models\UserActionType;
use Core\Classes\Models\UserReport;
use Core\Classes\Models\UserSession;

/**
 * A set of utility methods, mostly used internally, for extorio users. For external functionality, refer to the Users api
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Users
 */
class Users {

    /**
     * Generate a csv file from a user report. This returns the local file path of the csv
     *
     * @param int $userReportId
     * @param int $limit
     * @param int $skip
     *
     * @return string
     */
    public static function generateCsvFromUserReport($userReportId,$limit=0,$skip=0) {
        $ur = UserReport::findById($userReportId);
        if(!$ur) {
            return null;
        }
        $urq = UserReportQuery::n();
        $urq->limit($limit)->skip($skip);
        return Users::generateCsvFromUserReportQuery($urq);
    }

    /**
     * Generate a csv file from a user report query. This returns the local file path of the csv
     *
     * @param UserReportQuery $userReportQuery
     *
     * @return string
     */
    public static function generateCsvFromUserReportQuery(UserReportQuery $userReportQuery) {
        $users = User::findByUserReportQuery($userReportQuery,1);
        Commons\FileSystem\Directory::createFromPath("Application/Assets/user-reports");
        $fn = "Application/Assets/user-reports/".uniqid().".csv";
        $fh = fopen($fn,"w+");
        fputcsv($fh,UserReportQuery::getExposedProperties());
        foreach($users as $user) {
            $fields = array();
            foreach(UserReportQuery::getExposedProperties() as $p) {
                $fields[$p] = $user->$p;
            }
            fputcsv($fh,$fields);
        }
        fclose($fh);
        return $fn;
    }

    /**
     * Get the url to the user's profile
     *
     * @param int $userId
     *
     * @return string
     */
    public static function getProfileUrl($userId) {
        return "/users/".$userId;
    }

    /**
     * Get the url to the edit page of this user
     *
     * @param int $userId
     *
     * @return string
     */
    public static function getEditUrl($userId) {
        return "/extorio-admin/users/edit/".$userId;
    }

    /**
     * Get the name of a user group by id
     *
     * @param int $userGroupId
     *
     * @return string
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getUserGroupName($userGroupId) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT name FROM core_classes_models_usergroup WHERE id = $1 LIMIT 1';
        $row = $db->query($sql,array($userGroupId))->fetchRow();
        if($row) {
            return $row[0];
        } else {
            return "";
        }
    }

    /**
     * Get the user group id of a user
     *
     * @param int $userId
     *
     * @return int
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getUserGroupIdByUser($userId) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT "userGroup" FROM core_classes_models_user WHERE id = $1 LIMIT 1';
        $row = $db->query($sql,array($userId))->fetchRow();
        if($row) {
            return intval($row[0]);
        } else {
            return null;
        }
    }

    /**
     * Check whether a user manager another user, and has a certain privilege
     *
     * @param int $managerId
     * @param int $userId
     * @param string $privilegeName
     * @param null|string $extensionName
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function userManagesUserAndHasPrivilege($managerId, $userId, $privilegeName, $extensionName=null) {
        if(!strlen($managerId) || $managerId <= 0) {
            return false;
        }
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT

        M.id

        FROM core_classes_models_user M

        LEFT JOIN core_classes_models_usergroup MG ON
        MG.id = M."userGroup"

        RIGHT JOIN core_classes_models_userprivilege MP ON
        MP.id = ANY(MG.privileges)

        WHERE
        M.id = $1 AND
        MP.name = $2 AND
        (
            MG."manageAll" = true OR
            (
                SELECT count(U.id) FROM core_classes_models_user U

                WHERE U.id = $3 AND
                U."userGroup" = ANY(MG."manageGroups")

                LIMIT 1
            ) >= 1
        )
        ';
        $params = array($managerId,$privilegeName,$userId);
        if(strlen($extensionName)) {
            $sql .= 'AND MP."extensionName" = $4 ';
            $params[] = $extensionName;
        }
        $sql .= 'LIMIT 1';
        $row = $db->query($sql,$params)->fetchRow();
        if($row) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check whether a user manager another user, and whether they have atleast one out of a set of privileges
     *
     * @param int $managerId
     * @param int $userId
     * @param string[] $privilegeNames
     * @param null|string $extensionName
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function userManagesUserAndHasPrivileges_OR($managerId, $userId, $privilegeNames, $extensionName=null) {
        if(!strlen($managerId) || $managerId <= 0) {
            return false;
        }
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT

        count(M.id)

        FROM core_classes_models_user M

        LEFT JOIN core_classes_models_usergroup MG ON
        MG.id = M."userGroup"

        RIGHT JOIN core_classes_models_userprivilege MP ON
        MP.id = ANY(MG.privileges)

        WHERE
        M.id = $1 AND
        MP.name IN (';
        $in = "";
        foreach($privilegeNames as $priv) {
            $in .= "'".$priv."',";
        }
        $sql .= substr($in,0,strlen($in)-1);
        $sql .= ') AND
        (
            MG."manageAll" = true OR
            (
                SELECT count(U.id) FROM core_classes_models_user U

                WHERE U.id = $2 AND
                U."userGroup" = ANY(MG."manageGroups")

                LIMIT 1
            ) >= 1
        )
        ';
        $params = array($managerId,$userId);
        if(strlen($extensionName)) {
            $sql .= 'AND MP."extensionName" = $3 ';
            $params[] = $extensionName;
        }
        $sql .= 'LIMIT 1';
        $row = $db->query($sql,$params)->fetchRow();
        if($row) {
            return intval($row[0]) >= 1;
        } else {
            return false;
        }
    }

    /**
     * Check whether a user manager another user, and whether they have a set of privileges
     *
     * @param int $managerId
     * @param int $userId
     * @param string[] $privilegeNames
     * @param null|string $extensionName
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function userManagesUserAndHasPrivileges_AND($managerId, $userId, $privilegeNames, $extensionName=null) {
        if(!strlen($managerId) || $managerId <= 0) {
            return false;
        }
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT

        count(M.id)

        FROM core_classes_models_user M

        LEFT JOIN core_classes_models_usergroup MG ON
        MG.id = M."userGroup"

        RIGHT JOIN core_classes_models_userprivilege MP ON
        MP.id = ANY(MG.privileges)

        WHERE
        M.id = $1 AND
        MP.name IN (';
        $in = "";
        foreach($privilegeNames as $priv) {
            $in .= "'".$priv."',";
        }
        $sql .= substr($in,0,strlen($in)-1);
        $sql .= ') AND
        (
            MG."manageAll" = true OR
            (
                SELECT count(U.id) FROM core_classes_models_user U

                WHERE U.id = $2 AND
                U."userGroup" = ANY(MG."manageGroups")

                LIMIT 1
            ) >= 1
        )
        ';
        $params = array($managerId,$userId);
        if(strlen($extensionName)) {
            $sql .= 'AND MP."extensionName" = $3 ';
            $params[] = $extensionName;
        }
        $sql .= 'LIMIT 1';
        $row = $db->query($sql,$params)->fetchRow();
        if($row) {
            return intval($row[0]) == count($privilegeNames);
        } else {
            return false;
        }
    }

    /**
     * Check whether a user is able to manage a user group
     *
     * @param int $managerId
     * @param int $groupId
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function userManagesGroup($managerId,$groupId) {
        if(!strlen($managerId) || $managerId <= 0) {
            return false;
        }
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT

        M.id

        FROM core_classes_models_user M

        LEFT JOIN core_classes_models_usergroup MG ON
        MG.id = M."userGroup"

        WHERE
        M.id = $1 AND
        (
            MG."manageAll" = true OR
            $2 = ANY(MG."manageGroups")
        )

        LIMIT 1
        ';
        $row = $db->query($sql,array($managerId,$groupId))->fetchRow();
        if($row) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get whether a user is able to manage another user
     *
     * @param int $managerId
     * @param int $userId
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function userManagesUser($managerId,$userId) {
        if(!strlen($managerId) || $managerId <= 0) {
            return false;
        }
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT

        M.id

        FROM core_classes_models_user M

        LEFT JOIN core_classes_models_usergroup MG ON
        MG.id = M."userGroup"

        WHERE
        M.id = $1 AND
        (
            MG."manageAll" = true OR
            (
                SELECT count(U.id) FROM core_classes_models_user U

                WHERE U.id = $2 AND
                U."userGroup" = ANY(MG."manageGroups")

                LIMIT 1
            ) >= 1
        )

        LIMIT 1
        ';
        $row = $db->query($sql,array($managerId,$userId))->fetchRow();
        if($row) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check whether a user has a certain privilege
     *
     * @param int $userId
     * @param string $privilegeName
     * @param string|null $extensionName
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function userHasPrivilege($userId,$privilegeName,$extensionName=null) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT
        U.id

        FROM core_classes_models_user U

        RIGHT JOIN core_classes_models_usergroup UG ON
        UG.id = U."userGroup"

        RIGHT JOIN core_classes_models_userprivilege UP ON
        UP.id = ANY(UG.privileges)

        WHERE
        U.id = $1 AND
        UP.name = $2
        ';
        $params = array($userId,$privilegeName);
        if(strlen($extensionName)) {
            $sql .= ' AND UP."extensionName" = $3 ';
            $params[] = $extensionName;
        }
        $sql .= 'LIMIT 1';
        $row = $db->query($sql,$params)->fetchRow();
        if($row) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check whether a user has atleast one out of a list of privileges
     *
     * @param int $userId
     * @param string[] $privilegeNames
     * @param string|null $extensionName
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function userHasPrivileges_OR($userId,$privilegeNames,$extensionName=null) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT
        count(U.id)

        FROM core_classes_models_user U

        RIGHT JOIN core_classes_models_usergroup UG ON
        UG.id = U."userGroup"

        RIGHT JOIN core_classes_models_userprivilege UP ON
        UP.id = ANY(UG.privileges)

        WHERE
        U.id = $1 AND
        UP.name IN (
        ';
        $params = array($userId);
        $in = "";
        foreach($privilegeNames as $priv) {
            $in .= "'".$priv."',";
        }
        $sql .= substr($in,0,strlen($in)-1);
        $sql .= ")";
        if(strlen($extensionName)) {
            $sql .= ' AND UP."extensionName" = $2 ';
            $params[] = $extensionName;
        }
        $sql .= 'LIMIT 1';
        $row = $db->query($sql,$params)->fetchRow();
        if($row) {
            return intval($row[0]) >= 1;
        } else {
            return false;
        }
    }

    /**
     * Check whether a user has all privileges from a list of privileges
     *
     * @param int $userId
     * @param string[] $privilegeNames
     * @param string|null $extensionName
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function userHasPrivileges_AND($userId,$privilegeNames,$extensionName=null) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT
        count(U.id)

        FROM core_classes_models_user U

        RIGHT JOIN core_classes_models_usergroup UG ON
        UG.id = U."userGroup"

        RIGHT JOIN core_classes_models_userprivilege UP ON
        UP.id = ANY(UG.privileges)

        WHERE
        U.id = $1 AND
        UP.name IN (
        ';
        $params = array($userId);
        $in = "";
        foreach($privilegeNames as $priv) {
            $in .= "'".$priv."',";
        }
        $sql .= substr($in,0,strlen($in)-1);
        $sql .= ")";
        if(strlen($extensionName)) {
            $sql .= ' AND UP."extensionName" = $2 ';
            $params[] = $extensionName;
        }
        $sql .= 'LIMIT 1';
        $row = $db->query($sql,$params)->fetchRow();
        if($row) {
            return intval($row[0]) == count($privilegeNames);
        } else {
            return false;
        }
    }

    /**
     * Get the number of users within a user group
     *
     * @param $userGroupId
     *
     * @return int
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getUserCountByUserGroup($userGroupId) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT count(id) FROM core_classes_models_user WHERE "userGroup" = $1';
        $row = $db->query($sql,array($userGroupId))->fetchRow();
        if($row) {
            return intval($row[0]);
        } else {
            return 0;
        }
    }

    /**
     * Get the number of user priveleges within a user privilege category
     *
     * @param $userPrivilegeCategoryId
     *
     * @return int
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getUserPrivilegeCountByCategory($userPrivilegeCategoryId) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT count(id) FROM core_classes_models_userprivilege WHERE category = $1';
        $row = $db->query($sql,array($userPrivilegeCategoryId))->fetchRow();
        if($row) {
            return intval($row[0]);
        } else {
            return 0;
        }
    }

    /**
     * Get the avatar of a user
     *
     * @param $userId
     *
     * @return string
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getAvatar($userId) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $sql = "SELECT avatar FROM core_classes_models_user WHERE id = $1 LIMIT 1";
        $row = $db->query($sql, array($userId))->fetchRow();
        if($row) {
            return $row[0];
        } else {
            return null;
        }
    }

    /**
     * Get the shortname of a user
     *
     * @param $userId
     *
     * @return string
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getShortname($userId) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $sql = "SELECT shortname FROM core_classes_models_user WHERE id = $1 LIMIT 1";
        $row = $db->query($sql, array($userId))->fetchRow();
        if($row) {
            return $row[0];
        } else {
            return null;
        }
    }

    /**
     * Get the longname of a user
     *
     * @param $userId
     *
     * @return string
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getLongname($userId) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $sql = "SELECT longname FROM core_classes_models_user WHERE id = $1 LIMIT 1";
        $row = $db->query($sql, array($userId))->fetchRow();
        if($row) {
            return $row[0];
        } else {
            return null;
        }
    }

    /**
     * Attempt to log in based on username/password. Thrown errors are user-safe.
     *
     * This should be used to log in a user for use within the Extorio admin/site. For external logins, you should use
     * the Users api.
     *
     * The user's session is returned.
     *
     * @param $username
     * @param $password
     * @param int $sessionDays
     * @param bool $cookie
     * @param int $cookieDays
     *
     * @return string
     * @throws User_Exception
     */
    public static function login($username,$password,$sessionDays=30,$cookie=false,$cookieDays=30) {
        //logout the current user
        Users::logout();
        //clean up espired
        Users::cleanUpExpiredSessions();

        if(!strlen($username) || !strlen($password)) {
            throw new User_Exception("You must enter a username and password in order to log in");
        }

        $user = User::findOne(
            Query::n()
                ->where(array(
                    '$and' => array(
                        '$or' => array(
                            "username" => $username,
                            "email" => $username,
                        ),
                        "password" => $password
                    )
                )),2
        );
        if(!$user) {
            throw new User_Exception("The username and/or password that you entered was incorrect");
        } else {
            //check that the user can login
            if(!$user->canLogin) {
                throw new User_Exception("Unable to log in as this user");
            }

            $session = UserSession::n();
            $session->userId = $user->id;
            $session->session = md5(time().$user->id.$user->username.$user->password);
            $session->expiryDate = date("Y-m-d H:i:s",time() + (60*60*24*$sessionDays));
            $session->remoteIP = Server::getIpAddress();
            $session->pushThis();

            if(gettype($user->numLogin) == PHPTypes::_nULL) {
                $user->numLogin = 1;
            } else {
                $user->numLogin++;
            }
            $user->dateLogin = date("Y-m-d H:i:s");
            $user->pushThis();

            //create the session either as a raw session, or jwt based on the session type
            $config = Commons\Extorio::get()->getConfig();
            $user_session = "";
            switch($config["extorio"]["authentication"]["session_type"]) {
                case "basic" :
                    $user_session = $session->session;
                    break;
                case "jwt" :
                    $user_session = \JWT::encode(array(
                        "iss" => $config["application"]["address"],
                        "user_session" => $session->session
                    ),$config["extorio"]["authentication"]["jwt"]["key"],$config["extorio"]["authentication"]["jwt"]["alg"]);
                    break;
            }

            $_SESSION["user_session"] = $user_session;
            if($cookie) {
                setcookie("user_session",$user_session,time() + (60*60*24*$cookieDays),"/");
            } else {
                setcookie("user_session",null,time() - (60*60*24*7),"/");
            }

            //run the user_login event
            Commons\Extorio::get()->getEvent(InternalEvents::_user_login)->run(array($user));

            //user_login action
            UserAction::qc(
                $user->id,
                UserActionType::findByName("user_login")
            );

            return $user_session;
        }
    }

    /**
     * Log the currently logged in user out. To be used to log out the current user using the admin/site.
     *
     * To log out an external user, you should use the Users api
     */
    public static function logout() {
        //if there is a logged in user, we need to delete the session object
        $extorio = Commons\Extorio::get();
        $user = $extorio->getLoggedInUser();
        if($user) {
            $session = $extorio->getLoggedInUserSession();
            $extorio->setLoggedInUserTemp(null);
            if($session) {
                $us = UserSession::findOne(Query::n()->where(array(
                    "session" => $session
                )));
                if($us) {
                    $us->deleteThis();
                }
            }
            //clear the globals
            if(isset($_SESSION["user_session"])) {
                unset($_SESSION["user_session"]);
            }
            if(isset($_GET["user_session"])) {
                unset($_GET["user_session"]);
            }
            if(isset($_COOKIE["user_session"])) {
                unset($_COOKIE["user_session"]);
                setcookie("user_session",null,time() - (60*60*24*7),"/");
            }

            Commons\Extorio::get()->getEvent(InternalEvents::_user_logout)->run(array($user));
        }
    }

    /**
     * Register a new user, using the registration config settings. Returns the new user
     *
     * @param $username
     * @param $password
     * @param $email
     * @param $firstname
     * @param $lastname
     * @param $avatar
     *
     * @return User
     * @throws \Exception
     *
     */
    public static function register(
        $username,
        $password,
        $email,
        $firstname = "",
        $lastname = "",
        $avatar = null
    ) {
        $config = Commons\Extorio::get()->getConfig();
        //is registration enabled?
        if($config["users"]["registration"]["enabled"]) {
            $user = User::n();
            $user->username = $username;
            $user->password = $password;
            $user->email = $email;
            $user->firstname = $firstname;
            $user->lastname = $lastname;
            $user->avatar = $avatar;

            //email validation
            Users::validateEmail($user->email);

            //password validation
            Users::validatePassword($user->password);

            //username validation
            Users::validateUsername($user->username);

            //if registration is enabled and there are no issues, then we forcefully create the user
            $user->pushThis();

            //if we are required to authenticate users, they can not yet login
            if($config["users"]["registration"]["authenticate_users"]) {
                $user->canLogin = false;
                //send the authentication email
                Users::sendAuthenticationEmail($user);
            } else {
                //user is able to log in
                $user->canLogin = true;
                //do we confirm the user?
                if($config["users"]["registration"]["confirm_users"]) {
                    //send account email
                    Users::sendAccountEmail($user);
                }
            }

            return $user;

        } else {
            throw new \Exception("Registration is disabled on this website");
        }
    }

    /**
     * Send an authentication email to a user, containing the authnetication url for authenticating their account
     *
     * @param User $user
     *
     * @return EmailTransport
     */
    public static function sendAuthenticationEmail($user) {
        $config = Commons\Extorio::get()->getConfig();
        $url = $config["application"]["address"]."/user-authentication/?e=".$user->email."&t=".md5($user->email.$config["users"]["registration"]["authentication_key"]);
        return EmailTransport::qs(
            $config["users"]["registration"]["authentication_email"]["subject"],
            $config["users"]["registration"]["authentication_email"]["body"],
            array(
                "USERNAME" => $user->username,
                "SHORTNAME" => $user->shortname,
                "LONGNAME" => $user->longname,
                "APPLICATION_ADDRESS" => $config["application"]["address"],
                "APPLICATION_NAME" => $config["application"]["name"],
                "APPLICATION_COMPANY" => $config["application"]["company"],
                "AUTHENTICATION_URL" => '<a href="'.$url.'" target="_blank">'.$url.'</a>'
            ),
            null,
            $user->shortname,
            $user->email
        );
    }

    /**
     * Send the account email to the user, containing the user's login details
     *
     * @param User $user
     *
     * @return EmailTransport
     */
    public static function sendAccountEmail($user) {
        $config = Commons\Extorio::get()->getConfig();

        return EmailTransport::qs(
            $config["users"]["account_email"]["subject"],
            $config["users"]["account_email"]["body"],
            array(
                "USERNAME" => $user->username,
                "SHORTNAME" => $user->shortname,
                "LONGNAME" => $user->longname,
                "PASSWORD" => $user->password,
                "APPLICATION_ADDRESS" => $config["application"]["address"],
                "APPLICATION_NAME" => $config["application"]["name"],
                "APPLICATION_COMPANY" => $config["application"]["company"]
            ),
            null,
            $user->shortname,
            $user->email
        );
    }

    /**
     * Send the password reset email to the user
     *
     * @param User $user
     *
     * @return EmailTransport
     */
    public static function sendPasswordResetEmail($user) {
        $config = Commons\Extorio::get()->getConfig();
        $url = $config["application"]["address"]."/user-forgot-password/?e=".$user->email."&t=".md5($user->email.$config["users"]["password_reset"]["password_reset_key"]);

        return EmailTransport::qs(
            $config["users"]["password_reset"]["password_reset_email"]["subject"],
            $config["users"]["password_reset"]["password_reset_email"]["body"],
            array(
                "USERNAME" => $user->username,
                "SHORTNAME" => $user->shortname,
                "LONGNAME" => $user->longname,
                "APPLICATION_ADDRESS" => $config["application"]["address"],
                "APPLICATION_NAME" => $config["application"]["name"],
                "APPLICATION_COMPANY" => $config["application"]["company"],
                "PASSWORD_RESET_URL" => '<a href="'.$url.'" target="_blank">'.$url.'</a>'
            ),
            null,
            $user->shortname,
            $user->email
        );
    }

    /**
     * Change the password of a user, making use of the config options for passwords
     *
     * @param User $user
     * @param $newPassword
     *
     * @return User
     * @throws \Exception
     */
    public static function changePassword($user,$newPassword) {
        $config = Commons\Extorio::get()->getConfig();
        if(!$config["users"]["can_change_password"]) {
            throw new \Exception("Users are currently not able to change their password");
        }

        Users::validatePassword($newPassword);
        $user->password = $newPassword;
        $user->pushThis();



        return $user;
    }

    /**
     * Change the username of a user, making use of the config options for users
     *
     * @param User $user
     * @param $newUsername
     *
     * @return User
     * @throws \Exception
     */
    public static function changeUsername($user,$newUsername) {
        $config = Commons\Extorio::get()->getConfig();
        if(!$config["users"]["can_change_username"]) {
            throw new \Exception("Users are currently not able to change their username");
        }

        Users::validateUsername($newUsername);
        $user->username = $newUsername;
        $user->pushThis();

        return $user;
    }

    /**
     * Change the email of a user, making use of the config options for users
     *
     * @param User $user
     * @param $newEmail
     *
     * @return User
     * @throws \Exception
     */
    public static function changeEmail($user,$newEmail) {
        $config = Commons\Extorio::get()->getConfig();
        if(!$config["users"]["can_change_email"]) {
            throw new \Exception("Users are currently not able to change their email address");
        }

        Users::validateEmail($newEmail);
        $user->email = $newEmail;
        $user->pushThis();

        return $user;
    }

    /**
     * Validate a password
     *
     * @param $password
     *
     * @throws \Exception
     */
    public static function validatePassword($password) {
        $config = Commons\Extorio::get()->getConfig();

        //min length
        if(strlen($password) < $config["users"]["registration"]["password_min_length"]) {
            throw new \Exception("Password too short. Must be at least ".$config["users"]["registration"]["password_min_length"]." characters");
        }
        //contain special
        if($config["users"]["registration"]["password_contain_special"]) {
            if($password == Strings::removeSpecialCharacters($password)) {
                throw new \Exception("Password must contain at least one special character");
            }
        }
        //contain number
        if($config["users"]["registration"]["password_contain_number"]) {
            if($password == Strings::removeNumbers($password)) {
                throw new \Exception("Password must contain at least one number");
            }
        }
    }

    /**
     * Validate a username
     *
     * @param $username
     *
     * @throws \Exception
     */
    public static function validateUsername($username) {
        $config = Commons\Extorio::get()->getConfig();

        if(!$config["users"]["registration"]["username_allow_number"]) {
            if($username != Strings::removeNumbers($username)) {
                throw new \Exception("Username cannot contain numbers");
            }
        }
        if(!$config["users"]["registration"]["username_allow_special"]) {
            if($username != Strings::removeSpecialCharacters($username)) {
                throw new \Exception("Username cannot contain special characters");
            }
        }
    }

    /**
     * Validate an email address
     *
     * @param $email
     *
     * @throws \Exception
     */
    public static function validateEmail($email) {
        $config = Commons\Extorio::get()->getConfig();

        if($config["users"]["registration"]["validate_emails"]) {
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new \Exception("Email address invalid");
            }
        }
    }

    /**
     * Delete all expired user sessions
     */
    public static function cleanUpExpiredSessions() {
        $uss = UserSession::findAll(Query::n()->where(array(
            "expiryDate" => array(
                Query::_lte => date("Y-m-d H:i:s")
            )
        )));
        foreach($uss as $us) {
            $us->deleteThis();
        }
    }
}