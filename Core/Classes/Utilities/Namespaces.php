<?php
namespace Core\Classes\Utilities;
use Core\Classes\Enums\PHPTypes;

/**
 * Utility methods that make it a little easier to handle PHP namespaces
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Namespaces
 */
class Namespaces {
    /**
     * Get the namespace of an object. Should be used instead of get_class as it formats the namespace correctly
     *
     * @param $object
     *
     * @return string
     */
    public static function getNamespace($object) {
        if(gettype($object) == PHPTypes::_object) {
            return Strings::startsWith(get_class($object),"\\");
        } elseif(gettype($object) == PHPTypes::_string) {
            return Strings::startsWith($object,"\\");
        }
        return "";
    }

    /**
     * Get the PSR-4 file path of the namespace
     *
     * @param $namespace
     *
     * @return mixed
     */
    public static function toPath($namespace) {
        $namespace = Strings::notStartsWith($namespace,"\\");
        return str_replace("\\","/",$namespace);
    }

    /**
     * Get the fields of the namespace
     *
     * @param $namespace
     *
     * @return array
     */
    public static function getFields($namespace) {
        $namespace = Strings::notStartsWith($namespace,"\\");
        return explode("\\",$namespace);
    }

    /**
     * Get the PSR-4 path, as a loose path (the last field in the namespace could exist in any sub directory)
     *
     * @param $namespace
     *
     * @return string
     */
    public static function getLoosePath($namespace) {
        $fields = Namespaces::getFields($namespace);
        $loosePath = "";
        for($i = 1; $i < count($fields)-1; $i++) {
            $loosePath .= $fields[$i]."/";
        }
        return Strings::notEndsWith($loosePath,"/");
    }

    /**
     * Get the extension of the namespace (the first field)
     *
     * @param $namespace
     *
     * @return mixed
     */
    public static function getExtensionName($namespace) {
        $fields = Namespaces::getFields($namespace);
        return $fields[0];
    }

    /**
     * Get the class name of the namesapce (the last field)
     *
     * @param $namespace
     *
     * @return mixed
     */
    public static function getClassName($namespace) {
        $fields = Namespaces::getFields($namespace);
        return $fields[count($fields)-1];
    }
}