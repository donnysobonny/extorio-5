<?php
namespace Core\Classes\Utilities;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\BlockCategory;

class Blocks {

    /**
     * Get the block category tree
     *
     * @return BlockCategory[]
     */
    public static function getBlockCategoryTree() {
        $tree = BlockCategory::findAll(
            Query::n()
                ->where(array(
                    "parentId" => 0
                ))
                ->order("name")
        );
        foreach($tree as $main) {
            $main->fetchSubCategories();
        }
        return $tree;
    }

    /**
     * Check whether a user is able to modify a block
     *
     * @param int $userId
     * @param int $blockId
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function canUserModifyBlock($userId,$blockId) {
        if(!$userId) $userId = 0;
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT
        id
        FROM core_classes_models_block

        WHERE id = $1 AND
        $2 = ANY("modifyGroups")

        LIMIT 1';
        $row = $db->query($sql,array($blockId,Users::getUserGroupIdByUser($userId)))->fetchRow();
        if($row) {
            return true;
        }
        return Users::userHasPrivilege($userId,"blocks_modify_all","Core");
    }

    /**
     * Check whether a user is able to read a block
     *
     * @param int $userId
     * @param int $blockId
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function canUserReadBlock($userId,$blockId) {
        if(!$userId) $userId = 0;
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT
        id
        FROM core_classes_models_block

        WHERE id = $1 AND
        (
          "allRead" = $2 OR
          $3 = ANY("readGroups")
        )
        LIMIT 1';
        $row = $db->query($sql,array($blockId,true,Users::getUserGroupIdByUser($userId)))->fetchRow();
        if($row) {
            return true;
        }
        return Users::userHasPrivilege($userId,"blocks_read_all","Core");
    }

    /**
     * Get the number of blocks within a block category
     *
     * @param int $blockCategoryId
     *
     * @return int
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getBlockCountByCategory($blockCategoryId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT count(id) FROM core_classes_models_block WHERE (category = $1 OR "subCategory" = $1)';
        $row = $db->query($sql,array($blockCategoryId))->fetchRow();
        if($row) {
            return intval($row[0]);
        } else {
            return 0;
        }
    }
}