<?php
namespace Core\Classes\Utilities;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Enums\PHPTypes;

/**
 * Utility methods, useful for extorio processes
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Extorio
 */
final class Extorio {
    /**
     * Merge two config arrays, where the override overrides the base
     *
     * @param $baseConfig
     * @param $overrideConfig
     *
     * @return mixed
     */
    public static function mergeConfigArrays($baseConfig,$overrideConfig) {
        foreach($overrideConfig as $k => $v) {
            //if $v is an array
            if(gettype($v) == PHPTypes::_array) {
                //if override is empty, or a numeric array, it overrides
                if(empty($overrideConfig[$k]) || isset($overrideConfig[$k][0])) {
                    $baseConfig[$k] = $overrideConfig[$k];
                } else {
                    //recurse
                    $baseConfig[$k] = self::mergeConfigArrays($baseConfig[$k],$overrideConfig[$k]);
                }
            } else {
                //override always overrides here
                $baseConfig[$k] = $overrideConfig[$k];
            }
        }
        return $baseConfig;
    }

    /**
     * Discover whether postgresql is installed as an extension for php
     *
     * @return bool
     */
    public static function pgInstalled() {
        return extension_loaded("pgsql");
    }

    /**
     * Discover whether we can run php from the command line. Can also be used to discover whether we can use the
     * command line from PHP
     *
     * @return bool
     */
    public static function canCliAccess() {
        if(PHP_OS == "WINNT") {
            //windows command
            $command = "cd ".getcwd()." & php -f testcliaccess.php";
            return exec($command) == "success";
        } elseif(PHP_OS == "Linux") {
            //linux command
            $command = "cd ".getcwd()."; php -f testcliaccess.php";
            return exec($command) == "success";
        } else {
            return false;
        }
    }

    /**
     * Create the robots.txt file based on the application config
     *
     */
    public static function createRobotsTxt() {
        $config = \Core\Classes\Commons\Extorio::get()->getConfig();
        $file = File::createFromPath("robots.txt");
        $file->write("");
        $file->append("User-agent: *\n");
        //if Application is set to disable seo, hide everything
        if(!$config["application"]["seo_visible"]) {
            $file->append("Disallow: /\n");
        } else {
            //hide only the extorio pages
            $file->append("Disallow: /extorio/\n");
        }
    }
}