<?php
namespace Core\Classes\Utilities;
use Core\Classes\Enums\PHPTypes;

/**
 * Utilities for PHP arrays
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Arrays
 */
final class Arrays {
    /**
     * Discover whether the array is an associative one, or numeric
     *
     * @param $array
     *
     * @return bool
     */
    final public static function isAssoc($array) {
        return array_keys($array) !== range(0, count($array) - 1);
    }
    /**
     * Check whether an array has any values, other than arrays
     *
     * @param $array
     *
     * @return bool
     */
    final public static function valuesEmpty($array) {
        foreach($array as $values) {
            switch(gettype($values)) {
                case PHPTypes::_array :
                    if(!self::valuesEmpty($values)) {
                        return false;
                    }
                    break;
                case PHPTypes::_string :
                    if(strlen($values)) {
                        return false;
                    }
                    break;
                case PHPTypes::_integer :
                    return false;
                    break;
                case PHPTypes::_boolean :
                    return false;
                    break;
                case PHPTypes::_double :
                    return false;
                    break;
                case PHPTypes::_float :
                    return false;
                    break;
                default :
                    break;
            }
        }
        return true;
    }

    /**
     * Smartly cast an array by detecting the type of each element, and casting accordingly. Note that if $array is not an array, NULL is returned
     *
     * @param $array
     *
     * @return array
     */
    final public static function smartCast($array) {
        if(gettype($array) == PHPTypes::_array) {
            foreach($array as $key => $value) {
                if(gettype($value) == PHPTypes::_array) {
                    $array[$key] = Arrays::smartCast($value);
                } else {
                    $array[$key] = Strings::smartCast($value);
                }
            }
        } else {
            return null;
        }
        return $array;
    }

    /**
     * Get the value within an array, by specifying the path to the value. Returns FALSE if the value doesn't exist.
     *
     * @param array $array
     * @param array $path
     *
     * @return mixed
     */
    final public static function getPathValue($array,$path) {
        $element = $array;
        while(count($path)) {
            if(!isset($element[$path[0]])) {
                return false;
            }
            $element = $element[$path[0]];
            array_shift($path);
        }
        return $element;
    }

    /**
     * Set the value within an array, by specifying the path to the value.
     *
     * @param array $array
     * @param array $path
     * @param mixed $value
     */
    final public static function setPathValue(&$array,$path,$value) {
        $element = &$array;
        while(count($path)) {
            $element = &$element[$path[0]];
            array_shift($path);
        }
        $element = $value;
    }

    /**
     * Unset the value within an array, by specifying the path to the value.
     *
     * @param array $array
     * @param array $path
     */
    final public static function unsetPathValue(&$array,$path) {
        $element = &$array;
        while(count($path)) {
            if(count($path) == 1) {
                unset($element[$path[0]]);
            } else {
                $element = &$element[$path[0]];
            }
            array_shift($path);
        }
    }

    /**
     * Append a value within an array, by specifying the path constructWhere the value should be appended.
     *
     * Note that arrays/objects are appended as element of an array. Basic types are concatenated.
     *
     * @param array $array
     * @param array $path
     * @param mixed $value
     */
    final public static function appendPathValue(&$array,$path,$value) {
        $element = &$array;
        while(count($path)) {
            $element = &$element[$path[0]];
            array_shift($path);
        }
        if(gettype($value) == PHPTypes::_array || gettype($value) == PHPTypes::_object) {
            $element[] = $value;
        } else {
            $element .= $value;
        }
    }

    /**
     * Prepend a value within an array, by specifying the path constructWhere the value should be prepended.
     *
     * Note that arrays/objects are prepended as element of an array. Basic types are concatenated.
     *
     * @param array $array
     * @param array $path
     * @param mixed $value
     */
    final public static function prependPathValue(&$array,$path,$value) {
        $element = &$array;
        while(count($path)) {
            $element = &$element[$path[0]];
            array_shift($path);
        }
        if(gettype($value) == PHPTypes::_array || gettype($value) == PHPTypes::_object) {
            array_unshift($element,$value);
        } else {
            $element = $element . $value;
        }
    }
}