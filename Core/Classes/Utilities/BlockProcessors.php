<?php
namespace Core\Classes\Utilities;

class BlockProcessors {
    /**
     * Check whether a user is able to access a block processor inline
     *
     * @param int $userId
     * @param int $blockProcessorId
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function canUserInlineBlockProcessor($userId,$blockProcessorId) {
        if(!$userId) $userId = 0;
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT
        id
        FROM core_classes_models_blockprocessor

        WHERE id = $1 AND
        (
          "allInline" = $2 OR
          $3 = ANY("inlineGroups")
        )
        LIMIT 1';
        $row = $db->query($sql,array($blockProcessorId,true,Users::getUserGroupIdByUser($userId)))->fetchRow();
        if($row) {
            return true;
        }
        return Users::userHasPrivilege($userId,"block_processors_inline_all","Core");
    }
}