<?php
namespace Core\Classes\Utilities;
/**
 * Utilities for emails
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Emails
 */
class Emails {
    /**
     * Replace replacers with variables in the body of an email.
     *
     * @param $body
     * @param array $vars
     * @param string $lhs
     * @param string $rhs
     *
     * @return mixed
     */
    public static function mergeBodyWithVars($body,$vars=array(),$lhs="**",$rhs="**") {
        foreach($vars as $k => $v) {
            $body = str_replace($lhs.$k.$rhs,$v,$body);
        }
        return $body;
    }
}