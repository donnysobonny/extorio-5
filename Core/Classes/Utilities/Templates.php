<?php
namespace Core\Classes\Utilities;
class Templates {
    /**
     * Check whether a user is able to modify a template
     *
     * @param int $userId
     * @param int $templateId
     *
     * @return bool
     */
    public static function canUserModifyTemplate($userId, $templateId) {
        if(!$userId) $userId = 0;
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT
        id
        FROM core_classes_models_template

        WHERE id = $1 AND
        $2 = ANY("modifyGroups")
        LIMIT 1';
        $row = $db->query($sql,array($templateId,Users::getUserGroupIdByUser($userId)))->fetchRow();
        if($row) {
            return true;
        }
        return Users::userHasPrivilege($userId,"templates_modify_all","Core");
    }
}