<?php
namespace Core\Classes\Utilities;
use Core\Classes\Commons\FileSystem\Directory;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Models\Language;

class Localization {

    public static function generatePoFile($languageId) {
        $lang = Language::findById($languageId);
        if($lang) {
            $content = 'msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: '.date("Y-m-d H:i").'+0000\n"
"PO-Revision-Date: '.date("Y-m-d H:i").'+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: '.$lang->sLocale.'\n"
';

            $sql = '
            SELECT

            T.msgstr,
            T.msgstr_plural,
            TT.plural,
            TT.msgid,
            TT.msgid_plural

            FROM core_classes_models_translation T
            LEFT JOIN core_classes_models_translationbase TT ON
            TT.id = T."baseId"

            WHERE T."languageId" = $1
            ';
            $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
            $query = $db->query($sql,array($lang->id));
            while($row = $query->fetchAssoc()) {
                $plural = PGDB::decodeBool($row["plural"]);
                if(!$plural) {
                    $content .= '
# ...
msgid "'.Strings::removeDoubleQuotes($row["msgid"]).'"
msgstr "'.Strings::removeDoubleQuotes($row["msgstr"]).'"
';
                } else {
                        $content .= '
# ...
msgid "'.Strings::removeDoubleQuotes($row["msgid"]).'"
msgid_plural "'.Strings::removeDoubleQuotes($row["msgid_plural"]).'"
msgstr[0] "'.Strings::removeDoubleQuotes($row["msgstr"]).'"
msgstr[1] "'.Strings::removeDoubleQuotes($row["msgstr_plural"]).'"
';
                }
            }

            //make sure the directory exists first
            Directory::createFromPath("Locale/".$lang->sLocale."/LC_MESSAGES");
            //modify the file
            $file = File::createFromPath("Locale/".$lang->sLocale."/LC_MESSAGES/messages.po");
            $file->write($content);
            Localization::generateMoFile("Locale/".$lang->sLocale."/LC_MESSAGES/messages.po","Locale/".$lang->sLocale."/LC_MESSAGES/messages.mo");
        }
    }

    private static function generateMoFile($input, $output = false) {
        if ( !$output )
            $output = str_replace( '.po', '.mo', $input );
        $hash = Localization::parsePoFile( $input );
        if ( $hash === false ) {
            return false;
        } else {
            Localization::writeMoFile( $hash, $output );
            return true;
        }
    }
    private static function cleanHelper($x) {
        if (is_array($x)) {
            foreach ($x as $k => $v) {
                $x[$k] = Localization::cleanHelper($v);
            }
        } else {
            if ($x[0] == '"')
                $x = substr($x, 1, -1);
            $x = str_replace("\"\n\"", '', $x);
            $x = str_replace('$', '\\$', $x);
        }
        return $x;
    }
    private static function parsePoFile($in) {
        // read .po file
        $fh = fopen($in, 'r');
        if ($fh === false) {
            // Could not open file resource
            return false;
        }
        // results array
        $hash = array ();
        // temporary array
        $temp = array ();
        // state
        $state = null;
        $fuzzy = false;
        // iterate over lines
        while(($line = fgets($fh, 65536)) !== false) {
            $line = trim($line);
            if ($line === '')
                continue;
            list ($key, $data) = preg_split('/\s/', $line, 2);

            switch ($key) {
                case '#,' : // flag...
                    $fuzzy = in_array('fuzzy', preg_split('/,\s*/', $data));
                case '#' : // translator-comments
                case '#.' : // extracted-comments
                case '#:' : // reference...
                case '#|' : // msgid previous-untranslated-string
                    // start a new entry
                    if (sizeof($temp) && array_key_exists('msgid', $temp) && array_key_exists('msgstr', $temp)) {
                        if (!$fuzzy)
                            $hash[] = $temp;
                        $temp = array ();
                        $state = null;
                        $fuzzy = false;
                    }
                    break;
                case 'msgctxt' :
                    // context
                case 'msgid' :
                    // untranslated-string
                case 'msgid_plural' :
                    // untranslated-string-plural
                    $state = $key;
                    $temp[$state] = $data;
                    break;
                case 'msgstr' :
                    // translated-string
                    $state = 'msgstr';
                    $temp[$state][] = $data;
                    break;
                default :
                    if (strpos($key, 'msgstr[') !== FALSE) {
                        // translated-string-case-n
                        $state = 'msgstr';
                        $temp[$state][] = $data;
                    } else {
                        // continued lines
                        switch ($state) {
                            case 'msgctxt' :
                            case 'msgid' :
                            case 'msgid_plural' :
                                $temp[$state] .= "\n" . $line;
                                break;
                            case 'msgstr' :
                                $temp[$state][sizeof($temp[$state]) - 1] .= "\n" . $line;
                                break;
                            default :
                                // parse error
                                fclose($fh);
                                return FALSE;
                        }
                    }
                    break;
            }
        }
        fclose($fh);

        // add final entry
        if ($state == 'msgstr')
            $hash[] = $temp;
        // Cleanup data, merge multiline entries, reindex hash for ksort
        $temp = $hash;
        $hash = array ();
        foreach ($temp as $entry) {
            foreach ($entry as & $v) {
                $v = Localization::cleanHelper($v);
                if ($v === FALSE) {
                    // parse error
                    return FALSE;
                }
            }
            $hash[$entry['msgid']] = $entry;
        }
        return $hash;
    }
    private static function writeMoFile($hash, $out) {
        // sort by msgid
        ksort($hash, SORT_STRING);
        // our mo file data
        $mo = '';
        // header data
        $offsets = array ();
        $ids = '';
        $strings = '';
        foreach ($hash as $entry) {
            $id = $entry['msgid'];
            if (isset ($entry['msgid_plural']))
                $id .= "\x00" . $entry['msgid_plural'];
            // context is merged into id, separated by EOT (\x04)
            if (array_key_exists('msgctxt', $entry))
                $id = $entry['msgctxt'] . "\x04" . $id;
            // plural msgstrs are NUL-separated
            $str = implode("\x00", $entry['msgstr']);
            // keep track of offsets
            $offsets[] = array (
                strlen($ids
                ), strlen($id), strlen($strings), strlen($str));
            // plural msgids are not stored (?)
            $ids .= $id . "\x00";
            $strings .= $str . "\x00";
        }
        // keys start after the header (7 words) + index tables ($#hash * 4 words)
        $key_start = 7 * 4 + sizeof($hash) * 4 * 4;
        // values start right after the keys
        $value_start = $key_start +strlen($ids);
        // first all key offsets, then all value offsets
        $key_offsets = array ();
        $value_offsets = array ();
        // calculate
        foreach ($offsets as $v) {
            list ($o1, $l1, $o2, $l2) = $v;
            $key_offsets[] = $l1;
            $key_offsets[] = $o1 + $key_start;
            $value_offsets[] = $l2;
            $value_offsets[] = $o2 + $value_start;
        }
        $offsets = array_merge($key_offsets, $value_offsets);
        // write header
        $mo .= pack('Iiiiiii', 0x950412de, // magic number
            0, // version
            sizeof($hash), // number of entries in the catalog
            7 * 4, // key index offset
            7 * 4 + sizeof($hash) * 8, // value index offset,
            0, // hashtable size (unused, thus 0)
            $key_start // hashtable offset
        );
        // offsets
        foreach ($offsets as $offset)
            $mo .= pack('i', $offset);
        // ids
        $mo .= $ids;
        // strings
        $mo .= $strings;
        file_put_contents($out, $mo);
    }

    /**
     * Get the website's default language
     *
     * @return Language
     */
    public static function getDefaultLanguage() {
        $config = \Core\Classes\Commons\Extorio::get()->getConfig();
        return Language::findById($config["localization"]["default_language"]);
    }

    /**
     * Get the currently set language based on the environment and/or the logged in user
     *
     * @return Language
     */
    public static function getCurrentLanguage() {
        $loggedInUser = \Core\Classes\Commons\Extorio::get()->getLoggedInUser();
        if($loggedInUser) {
            if($loggedInUser->language) {
                return $loggedInUser->language;
            }
        }
        if(isset($_COOKIE["user_language"])) {
            return Language::findById($_COOKIE["user_language"]);
        }
        if(isset($_SESSION["user_language"])) {
            return Language::findById($_SESSION["user_language"]);
        }
        return Localization::getDefaultLanguage();
    }

    /**
     * Set the current language for this session, and optionally as a cookie
     *
     * @param int $languageId
     * @param bool $cookie
     * @param int $cookieDays
     */
    public static function setCurrentLanguage($languageId,$cookie=false,$cookieDays=30) {
        $_SESSION["user_language"] = $languageId;
        if($cookie) {
            setcookie("user_language",$languageId,time() + (60*60*24*$cookieDays),"/");
        } else {
            setcookie("user_language",null,time() - (60*60*24*7),"/");
        }
    }
}