<?php
namespace Core\Classes\Utilities;
use Core\Classes\Commons;
use Core\Classes\Enums\TaskStatus;
use Core\Classes\Exceptions\General_Exception;

/**
 * A set of utility methods related to tasks
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Tasks
 */
class Tasks {
    /**
     * Check whether a user is able to read a task
     *
     * @param int $userId
     * @param int $taskId
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function canUserReadTask($userId,$taskId) {
        if(!$userId) $userId = 0;
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT
        id
        FROM core_classes_models_task

        WHERE id = $1 AND
        (
          "allRead" = $2 OR
          $3 = ANY("readGroups")
        )
        LIMIT 1';
        $row = $db->query($sql,array($taskId,true,Users::getUserGroupIdByUser($userId)))->fetchRow();
        if($row) {
            return true;
        }
        return Users::userHasPrivilege($userId,"tasks_read_all","Core");
    }

    /**
     * Create a live task entry
     *
     * @param $taskName
     * @param $taskAction
     * @param $taskParams
     * @param bool $isHidden
     *
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function createLiveTask($taskName,$taskAction,$taskParams,$isHidden=false) {
        $extorio = Commons\Extorio::get();
        $loggedInUser = $extorio->getLoggedInUser();
        $accessedByUserId = 0;
        if($loggedInUser) $accessedByUserId = $loggedInUser->id;
        $db = $extorio->getDbInstanceDefault();
        $sql = '
        INSERT INTO tasks_livetask (
          "pid",
          "task",
          "action",
          "params",
          "accessedByUserId",
          "status",
          "isHidden",
          "dateCreated"
        ) VALUES ($1,$2,$3,$4,$5,$6,$7,$8)
        ';
        $params = array(
            getmypid(),
            $taskName,
            $taskAction,
            $taskParams,
            $accessedByUserId,
            TaskStatus::_notStarted,
            $isHidden,
            date("Y-m-d H:i:s")
        );
        $db->query($sql,$params);
    }

    /**
     * Clean up tasks, based on config settings
     *
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function cleanUpTasks() {
        $extorio = Commons\Extorio::get();
        $config = $extorio->getConfig();
        $db = $extorio->getDbInstanceDefault();
        $days = $config["tasks"]["clean_up_tasks_after_days"];
        $date = date("Y-m-d H:i:s",time() - 60*60*24*$days);
        $db->query('DELETE FROM tasks_livetask WHERE
        (
          "status" = $1 AND
          "dateCreated" <= $2
        ) OR
        (
          (
            "status" = $3 OR
            "status" = $4 OR
            "status" = $5
          ) AND
          "dateUpdated" <= $6
        )
        ',array(
            TaskStatus::_notStarted,
            $date,
            TaskStatus::_completed,
            TaskStatus::_failed,
            TaskStatus::_killed,
            $date
        ));
    }

    /**
     * Get a live task entry by pid
     *
     * @param $pid
     *
     * @return array
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getLiveTask($pid) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        return $db->query('SELECT * FROM tasks_livetask WHERE pid = $1 LIMIT 1',array($pid))->fetchAssoc();
    }

    /**
     * Get all live tasks
     *
     * @param bool $getHidden
     *
     * @return array
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getLiveTasks($getHidden=false) {
        //clean up before we get the tasks
        Tasks::cleanUpTasks();

        $db = Commons\Extorio::get()->getDbInstanceDefault();
        if($getHidden) {
            return $db->query('SELECT * FROM tasks_livetask')->fetchAll();
        } else {
            return $db->query('SELECT * FROM tasks_livetask WHERE "isHidden" = false')->fetchAll();
        }
    }

    /**
     * Log to a live task
     *
     * @param $pid
     * @param $message
     *
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function logToLiveTask($pid,$message) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $t = Tasks::getLiveTask($pid);
        if($t) {
            $t["log"] .= "[" . date("d-m-Y H:i:s") . "] - " . $message . "\n";
            $t["dateUpdated"] = date("Y-m-d H:i:s");
            $db->query('UPDATE tasks_livetask SET "log" = $1, "dateUpdated" = $2 WHERE pid = $3',array($t["log"],$t["dateUpdated"],$pid));
        }
    }

    /**
     * Queue a live task
     *
     * @param $pid
     *
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function queueLiveTask($pid) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $t = Tasks::getLiveTask($pid);
        if($t) {
            $t["status"] = TaskStatus::_queued;
            $t["dateUpdated"] = date("Y-m-d H:i:s");
            $db->query('UPDATE tasks_livetask SET "status" = $1, "dateUpdated" = $2 WHERE pid = $3',array($t["status"],$t["dateUpdated"],$pid));
        }
    }

    /**
     * Run a live task
     *
     * @param $pid
     *
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function runLiveTask($pid) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $t = Tasks::getLiveTask($pid);
        if($t) {
            $t["status"] = TaskStatus::_running;
            $t["dateUpdated"] = date("Y-m-d H:i:s");
            $db->query('UPDATE tasks_livetask SET "status" = $1, "dateUpdated" = $2 WHERE pid = $3',array($t["status"],$t["dateUpdated"],$pid));
        }
    }

    /**
     * Complete a live task
     *
     * @param $pid
     *
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function completeLiveTask($pid) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $t = Tasks::getLiveTask($pid);
        if($t) {
            $t["status"] = TaskStatus::_completed;
            $t["dateUpdated"] = date("Y-m-d H:i:s");
            $db->query('UPDATE tasks_livetask SET "status" = $1, "dateUpdated" = $2 WHERE pid = $3',array($t["status"],$t["dateUpdated"],$pid));
        }
    }

    /**
     * Fail a live task
     *
     * @param $pid
     * @param $errorMessage
     *
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function failLiveTask($pid,$errorMessage) {
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $t = Tasks::getLiveTask($pid);
        if($t) {
            $t["status"] = TaskStatus::_failed;
            $t["errorMessage"] = $errorMessage;
            $t["dateUpdated"] = date("Y-m-d H:i:s");
            $db->query('UPDATE tasks_livetask SET "status" = $1, "errorMessage" = $2, "dateUpdated" = $3 WHERE pid = $4',array($t["status"],$t["errorMessage"],$t["dateUpdated"],$pid));
        }
    }

    /**
     * Kill a live task
     *
     * @param $pid
     *
     * @throws General_Exception
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function killLiveTask($pid) {
        //kill the process
        if(PHP_OS == "WINNT") {
            $command = "cd ".getcwd()."";
            $command .= " & taskkill /F /PID ".$pid." ";
            exec($command);
        } elseif (PHP_OS == "Linux") {
            $command = "cd ".getcwd()."";
            $command .= "; kill -KILL ".$pid." ";
            exec($command);
        } else {
            throw new General_Exception("The task system is only supported by Windows and Linux currently.");
        }
        $db = Commons\Extorio::get()->getDbInstanceDefault();
        $t = Tasks::getLiveTask($pid);
        if($t) {
            $t["status"] = TaskStatus::_killed;
            $t["dateUpdated"] = date("Y-m-d H:i:s");
            $db->query('UPDATE tasks_livetask SET "status" = $1, "dateUpdated" = $2 WHERE pid = $3',array($t["status"],$t["dateUpdated"],$pid));
        }
    }
}