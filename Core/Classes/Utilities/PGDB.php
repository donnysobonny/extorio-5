<?php
namespace Core\Classes\Utilities;
/**
 * Utility methods for the PGDB class
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class PGDB
 */
class PGDB {
    /**
     * Generate a string that can be used in a tsquery, converted from a phrase
     *
     * @param string $phrase
     *
     * @return string
     */
    public static function generateTsQueryString($phrase) {
        //to lower, remove recurring spaces, and remove special characters
        $phrase = strtolower(Strings::removeRecurring(Strings::removeQuotes(Strings::removeSpecialCharacters($phrase))," "));
        //strip out the words
        $words = explode(" ",$phrase);
        $clean = "";
        //seperate words with &
        foreach($words as $word) {
            if(strlen($word)) {
                $clean .= $word." & ";
            }
        }
        return substr($clean,0,strlen($clean)-3);
    }
    /**
     * Decode PG arrays to a PHP array.
     *
     * Note: this only works on single-dimension arrays!
     *
     * @param $literal
     *
     * @return array|void
     */
    public static function decodeArray($literal) {
        if ($literal == '')
            return;
        preg_match_all('/(?<=^\{|,)(([^,"{]*)|\s*"((?:[^"\\\\]|\\\\(?:.|[0-9]+|x[0-9a-f]+))*)"\s*)(,|(?<!^\{)(?=\}$))/i', $literal, $matches, PREG_SET_ORDER);
        $values = array();
        foreach ($matches as $match) {
            $values[] = $match[3] != '' ? stripcslashes($match[3]) : (strtolower($match[2]) == 'null' ? null : $match[2]);
        }
        return $values;
    }

    /**
     * Encode a PHP array to a PG array
     *
     * Note, this only works for single-dimension arrays!
     *
     * @param $array
     *
     * @return string
     */
    public static function encodeArray($array) {
        $parts = "";
        foreach($array as $part) {
            $parts .= $part.",";
        }
        return "{".substr($parts,0,strlen($parts)-1)."}";
    }

    /**
     * Decode a PG boolean into a PHP boolean
     *
     * @param string $literal
     *
     * @return bool
     */
    public static function decodeBool($literal) {
        switch(strtolower(substr($literal,0,1))) {
            case "t" :
                return true;
            case "f" :
                return false;
        }
    }

    /**
     * Encode a PHP boolean into a PG boolean
     *
     * @param bool $bool
     *
     * @return string
     */
    public static function encodeBool($bool) {
        if($bool) {
            return "t";
        } else {
            return "f";
        }
    }
}