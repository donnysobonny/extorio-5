<?php
namespace Core\Classes\Utilities;
/**
 * A set of useful utility methods for handling PHP strings
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Strings
 */
final class Strings {

    /**
     * Convert a valid date to a readable time ago
     *
     * @param string $date
     *
     * @return string
     */
    public static function dateToTimeAgo($date) {
        if (empty($date)) {
            return "No date provided";
        }

        $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
        $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

        $now = time();
        $unix_date = strtotime($date);

        // check validity of date
        if (empty($unix_date)) {
            return "Bad date";
        }

        // is it future date or past date
        if ($now > $unix_date) {
            $difference = $now - $unix_date;
            $tense = "ago";

        } else {
            $difference = $unix_date - $now;
            $tense = "from now";
        }

        for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
            $difference /= $lengths[$j];
        }

        $difference = round($difference);
        if($difference == 0) {
            return "just now";
        }

        if ($difference != 1) {
            $periods[$j] .= "s";
        }

        return "$difference $periods[$j] {$tense}";
    }

    /**
     * Convert html to usable plain text. Breaking html elements are either converted to new lines or spaces based on
     * $nesLines
     *
     * @param $html
     * @param bool $newLines whether to convert breaking elements to new lines
     *
     * @return mixed|string
     */
    public static function htmlToPlain($html,$newLines=true) {
        $breakingElementsA = array(
            "<br />",
            "<br/>",
            "<br>",
            "<hr />",
            "<hr/>",
            "<hr>",
            "</p>",
            "</div>",
            "</ul>",
            "</li>",
            "</code>",
            "</pre>",
            "</blockquote>"
        );
        $breakingContainerElemetns = "<br /><br/><br><hr /><hr/><hr><p></p><div></div><ul></ul><li></li><code></code><pre></pre><blockquote></blockquote>";
        //strip unwanted tags
        $html = strip_tags($html,$breakingContainerElemetns);
        //convert all breaking elements to new lines or spaces
        if($newLines) {
            $html = str_ireplace($breakingElementsA,"\r\n",$html);
        } else {
            $html = str_ireplace($breakingElementsA," ",$html);
        }
        //remove any remaining html
        $html = strip_tags($html);
        //remove html entities
        $html = html_entity_decode($html,ENT_QUOTES|ENT_HTML5);
        //remove preceding whitespace
        $html = ltrim($html);
        //remove recurring spaces
        $html = Strings::removeRecurring($html," ");

        return $html;
    }

    /**
     * Shorten a string. Useful for shortening the length of a blog post.
     *
     * @param $string
     * @param $length
     * @param string $append
     *
     * @return string
     */
    public static function shorten($string,$length,$append="...") {
        if(strlen($string) > ($length - strlen($append))) {
            $string = substr($string,0,($length - strlen($append))).$append;
        }
        return $string;
    }

    /**
     * Smartly cast a string to a php type, such as double/int/bool. This is used by Arrays::smartCast on simple types
     *
     * @param $string
     *
     * @return mixed
     */
    public static function smartCast($string) {
        //if string is numeric
        if(is_numeric($string)) {
            //cast to double/int based on whether there is a "."
            if(strpos($string,".") !== false) {
                return doubleval($string);
            } else {
                return intval($string);
            }
        }
        //convert string bools, and nulls
        switch(strtolower($string)) {
            case "true" :
                return true;
            case "false" ;
                return false;
            case "null" :
                return null;
        }
        return $string;
    }

    /**
     * Convert the encoding of a string
     *
     * @param $string
     * @param $encType
     *
     * @return mixed|string
     */
    public static function convertToEncoding($string, $encType) {
        // detect the character encoding of the incoming file
        $encoding = mb_detect_encoding($string, "auto");

        // escape all of the question marks so we can remove artifacts from
        // the unicode conversion process
        $target = str_replace("?", "[question_mark]", $string);

        // convert the string to the target encoding
        $target = mb_convert_encoding($target, $encType, $encoding);

        // remove any question marks that have been introduced because of illegal characters
        $target = str_replace("?", "", $target);

        // replace the token string "[question_mark]" with the symbol "?"
        $target = str_replace("[question_mark]", "?", $target);

        return $target;
    }

    /**
     * Make sure a string ends with $endWith
     *
     * @param $subject
     * @param $endWith
     *
     * @return string
     */
    public static function endsWith($subject, $endWith) {
        if (substr($subject, strlen($subject) - strlen($endWith), strlen($endWith)) != $endWith) {
            $subject .= $endWith;
        }
        return $subject;
    }

    /**
     * Make sure a string starts with $startWith
     *
     * @param $subject
     * @param $startWith
     *
     * @return string
     */
    public static function startsWith($subject, $startWith) {
        if (substr($subject, 0, strlen($startWith)) != $startWith) {
            $subject = $startWith . $subject;
        }
        return $subject;
    }

    /**
     * Make sure a string starts and ends with $startAndEndWith
     *
     * @param $subject
     * @param $startAndEndWith
     *
     * @return string
     */
    public static function startsAndEndsWith($subject, $startAndEndWith) {
        $subject = Strings::startsWith($subject, $startAndEndWith);
        $subject = Strings::endsWith($subject, $startAndEndWith);
        return $subject;
    }

    /**
     * Make sure a string doesn't end with $notEndWith
     *
     * @param $subject
     * @param $notEndWith
     *
     * @return string
     */
    public static function notEndsWith($subject, $notEndWith) {
        if (substr($subject, strlen($subject) - strlen($notEndWith), strlen($notEndWith)) == $notEndWith) {
            $subject = substr($subject, 0, strlen($subject) - strlen($notEndWith));
        }
        return $subject;
    }

    /**
     * Make sure a string doesn't start with $notStartWith
     *
     * @param $subject
     * @param $notStartWith
     *
     * @return string
     */
    public static function notStartsWith($subject, $notStartWith) {
        if (substr($subject, 0, strlen($notStartWith)) == $notStartWith) {
            $subject = substr($subject, strlen($notStartWith), strlen($subject) - strlen($notStartWith));
        }
        return $subject;
    }

    /**
     * Make sure a string doesn't start or end with $notStartAndEndWith
     *
     * @param $subject
     * @param $notStartAndEndWith
     *
     * @return string
     */
    public static function notStartsAndNotEndsWith($subject, $notStartAndEndWith) {
        $subject = Strings::notStartsWith($subject,$notStartAndEndWith);
        $subject = Strings::notEndsWith($subject,$notStartAndEndWith);
        return $subject;
    }

    /**
     * Replace non-word characters
     *
     * @param $string
     * @param string $replace
     *
     * @return mixed
     */
    public static function replaceNonWordCharacters($string, $replace = "") {
        return preg_replace("/[\W]/", $replace, $string);
    }

    /**
     * Replace word characters
     *
     * @param $string
     * @param string $replace
     *
     * @return mixed
     */
    public static function replaceWordCharacters($string, $replace = "") {
        return preg_replace("/[\w]/", $replace, $string);
    }

    /**
     * Remove word characters
     *
     * @param $string
     *
     * @return mixed
     */
    public static function removeWordCharacters($string) {
        return Strings::replaceWordCharacters($string);
    }

    /**
     * Replace non-number characters
     *
     * @param $string
     * @param string $replace
     *
     * @return mixed
     */
    public static function replaceNonNumbers($string, $replace = "") {
        return preg_replace("/[\D]/", $replace, $string);
    }

    /**
     * Replace numbers
     *
     * @param $string
     * @param string $replace
     *
     * @return mixed
     */
    public static function replaceNumbers($string, $replace = "") {
        return preg_replace("/[\d]/", $replace, $string);
    }

    /**
     * Remove numbers
     *
     * @param $string
     *
     * @return mixed
     */
    public static function removeNumbers($string) {
        return Strings::replaceNumbers($string);
    }

    /**
     * Replace special characters
     *
     * @param $string
     * @param string $replace
     *
     * @return mixed
     */
    public static function replaceSpecialCharacters($string, $replace = "") {
        return preg_replace("/[^a-zA-Z0-9]/", $replace, $string);
    }

    /**
     * Remove special characters
     *
     * @param $string
     *
     * @return mixed
     */
    public static function removeSpecialCharacters($string) {
        return Strings::replaceSpecialCharacters($string);
    }

    /**
     * Replace single quotes
     *
     * @param $string
     * @param string $replace
     *
     * @return mixed
     */
    public static function replaceSingleQuotes($string, $replace = "") {
        return str_replace("'",$replace,$string);
    }

    /**
     * Remove single quotes
     *
     * @param $string
     *
     * @return mixed
     */
    public static function removeSingleQuotes($string) {
        return Strings::replaceSingleQuotes($string);
    }

    /**
     * Replace double quotes
     *
     * @param $string
     * @param string $replace
     *
     * @return mixed
     */
    public static function replaceDoubleQuotes($string, $replace = "") {
        return str_replace('"',$replace,$string);
    }

    /**
     * Remove single quotes
     *
     * @param $string
     *
     * @return mixed
     */
    public static function removeDoubleQuotes($string) {
        return Strings::replaceDoubleQuotes($string);
    }

    /**
     * Replace all quotes
     *
     * @param $string
     * @param string $replace
     *
     * @return mixed
     */
    public static function replaceQuotes($string, $replace = "") {
        return str_replace(array("'",'"'),$replace,$string);
    }

    /**
     * Remove all quotes
     *
     * @param $string
     *
     * @return mixed
     */
    public static function removeQuotes($string) {
        return Strings::replaceQuotes($string);
    }

    /**
     * Remove chars that are repeating directly beside each other. For example "---" would be converted to "-"
     *
     * @param $string
     * @param $recurringChar
     *
     * @return mixed
     */
    public static function removeRecurring($string,$recurringChar) {
        return preg_replace(
            "/[".$recurringChar."]+/",
            $recurringChar,
            $string
        );
    }

    /**
     * Replace spaces
     *
     * @param $string
     * @param string $replace
     *
     * @return mixed
     */
    public static function replaceSpaces($string, $replace = "") {
        return str_replace(" ", $replace, $string);
    }

    /**
     * Remove spaces
     *
     * @param $string
     *
     * @return mixed
     */
    public static function removeSpaces($string) {
        return Strings::replaceSpaces($string);
    }

    /**
     * Replace whitespace characters (including spaces)
     *
     * @param $string
     * @param string $replace
     *
     * @return mixed
     */
    public static function replaceWitespace($string, $replace = "") {
        return preg_replace('/\s+/', $replace, $string);
    }

    /**
     * Remove whitespace characters (including spaces)
     *
     * @param $string
     *
     * @return mixed
     */
    public static function removeWhitespace($string) {
        return Strings::replaceWitespace($string);
    }

    /**
     * Converts a string a title-safe name. This basically removes special characters and capitalises it
     *
     * @param $string
     *
     * @return string
     */
    public static function titleSafe($string) {
        //replace special chars with spaces
        $string = Strings::replaceSpecialCharacters($string," ");
        //remove recursive special spaces
        $string = Strings::removeRecurring($string," ");
        //get rid of start/end spaces
        $string = Strings::notStartsAndNotEndsWith($string," ");
        return ucwords($string);
    }

    /**
     * Converts a string to a label-safe name. This basically removes special characters and converts to lower case
     *
     * @param string $string
     *
     * @return string string
     */
    public static function labelSafe($string) {
        //replace special chars with -
        $string = Strings::replaceSpecialCharacters($string,"-");
        //remove recursive -
        $string = Strings::removeRecurring($string,"-");
        return strtolower($string);
    }

    /**
     * Converts a string to a url-safe string:
     *
     * @param $string
     *
     * @return string
     */
    public static function urlSafe($string) {
        if(!strlen($string)) {
            return "/";
        }
        if(strlen($string) == 1) {
            return "/";
        }
        $fields = explode("/",$string);
        $n = 0;
        $clean = "";
        foreach($fields as $field) {
            if(strlen($field)) {
                if($n > 0) {
                    $clean .= "/".urlencode($field);
                } else {
                    $clean .= urlencode($field);
                }
                $n++;
            } else {
                $clean .= "/";
            }
        }
        return $clean;
    }

    /**
     * Converts a string to a filename-safe string
     *
     * @param $string
     *
     * @return string
     */
    public static function fileNameSafe($string) {
        $string = preg_replace("/[^a-zA-Z0-9.]/", "-", $string);
        $string = Strings::removeRecurring($string,"-");
        $string = Strings::notStartsAndNotEndsWith($string,"-");

        return $string;
    }

    /**
     * Converts a string to a foldername-safe string
     *
     * @param $string
     *
     * @return string
     */
    public static function folderNameSafe($string) {
        $string = preg_replace("/[^a-zA-Z0-9\\/.]/", "-", $string);
        $string = Strings::removeRecurring($string,"-");
        $string = Strings::notStartsAndNotEndsWith($string,"-");

        return $string;
    }

    /**
     * Converts a string to classname-safe string
     *
     * @param $string
     *
     * @return mixed
     */
    public static function classNameSafe($string) {
        $string = preg_replace(
            "/[^a-zA-Z0-9_]/",
            " ",
            $string
        );
        $string = Strings::wordsToCamelCase($string, true);
        $string = Strings::removeSpaces($string);
        if(is_numeric(substr($string,0,1))) {
            $string = "_".$string;
        }
        return $string;
    }

    /**
     * Converts a string to a property name safe string
     *
     * @param $string
     *
     * @return string
     */
    public static function propertyNameSafe($string) {
        $string = preg_replace(
            "/[^a-zA-Z0-9_]/",
            " ",
            $string
        );
        $string = Strings::wordsToCamelCase($string, false);
        $string = Strings::removeSpaces($string);
        if(is_numeric(substr($string,0,1))) {
            $string = "_".$string;
        }
        return $string;
    }

    /**
     * Convert words to camel-case, and remove spaces. Optionally set the first word capitalised
     *
     * @param $string
     * @param bool $firstWordCapitalised
     *
     * @return string
     */
    public static function wordsToCamelCase($string, $firstWordCapitalised = true) {
        $string = ucwords($string);
        $string = str_replace(" ", "", $string);
        if (!$firstWordCapitalised) {
            $string = lcfirst($string);
        }

        return $string;
    }
}