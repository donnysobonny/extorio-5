<?php
namespace Core\Classes\Utilities;
use Core\Classes;

/**
 * Utility methods for models
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Models
 */
final class Models {
    /**
     * Get the table-safe name from a model's namespace
     *
     * @param $modelNamespace
     *
     * @return string
     */
    public static function getTableName($modelNamespace) {
        $modelNamespace = Strings::notStartsAndNotEndsWith($modelNamespace,"\\");
        return strtolower(str_replace("\\","_",$modelNamespace));
    }

    /**
     * Get the sequance-safe name from a model's namespace
     *
     * @param $modelNamespace
     *
     * @return string
     */
    public static function getSequenceName($modelNamespace) {
        return Models::getTableName($modelNamespace)."_seq";
    }

    /**
     * Get the next sequence value for a model
     *
     * @param $modelNamespace
     *
     * @return mixed
     * @throws Classes\Exceptions\DB_Exception
     */
    public static function getSequenceNextVal($modelNamespace) {
        $db = Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = "SELECT nextval($1)";
        $row = $db->query($sql,array(Models::getSequenceName($modelNamespace)))->fetchRow();
        return $row[0];
    }

    /**
     * Set the sequence current value for a model
     *
     * @param $modelNamespace
     * @param $currentValue
     *
     * @throws Classes\Exceptions\DB_Exception
     */
    public static function setSequenceCurrentValue($modelNamespace,$currentValue) {
        $db = Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT last_value FROM "'.Models::getSequenceName($modelNamespace).'" LIMIT 1';
        $row = $db->query($sql)->fetchRow();
        if($row[0] <= $currentValue) {
            //set the value
            $sql = "SELECT setval($1,$2)";
            $db->query($sql,array(Models::getSequenceName($modelNamespace),$currentValue));
        }
    }

    /**
     * Create a modcache entry
     *
     * @param $modelNamespace
     * @param $modelInstanceId
     * @param $action
     * @param null $dataBefore
     * @param null $dataAfter
     *
     * @throws Classes\Exceptions\DB_Exception
     */
    public static function createModCache($modelNamespace,$modelInstanceId,$action,$dataBefore=null,$dataAfter=null) {
        $extorio = Classes\Commons\Extorio::get();
        $config = $extorio->getConfig();
        $loggedInUser = $extorio->getLoggedInUser();
        $userId = $loggedInUser ? $loggedInUser->id : 0;
        $db = $extorio->getDbInstanceDefault();

        //if create/update action, remove any delete cache
        if($action == "create" || $action == "update") {
            $db->query('DELETE FROM orm_modcache WHERE "modelNamespace" = $1 AND "modelInstanceId" = $2 AND "action" = $3',array($modelNamespace,$modelInstanceId,"delete"));
        } elseif($action == "delete") {
            //delete any create/update cache
            $db->query('DELETE FROM orm_modcache WHERE "modelNamespace" = $1 AND "modelInstanceId" = $2 AND ("action" = $3 OR "action" = $4)',array($modelNamespace,$modelInstanceId,"create","update"));
        }

        //clean up entries
        $db->query('DELETE FROM orm_modcache WHERE id IN (
	        SELECT id FROM orm_modcache WHERE "modelNamespace" = $1 AND "modelInstanceId" = $2 ORDER BY "timestamp" DESC OFFSET $3
        )',array($modelNamespace,$modelInstanceId,$config["models"]["max_modcache_entries_per_instance"]));

        $sql = '
INSERT INTO orm_modcache
("modelNamespace", "modelInstanceId", "action", "timestamp", "dataBefore", "dataAfter", "userId")
VALUES ($1, $2, $3, $4, $5, $6, $7);
';
        $db->query($sql,array(
            $modelNamespace,
            $modelInstanceId,
            $action,
            date("Y-m-d H:i:s"),
            $dataBefore,
            $dataAfter,
            $userId
        ));
    }

    /**
     * Get the most recent modcache entry by id/action/userid for a model type
     *
     * @param $modelNamespace
     * @param $modelInstanceId
     * @param $action
     * @param null $userId
     *
     * @return array
     * @throws Classes\Exceptions\DB_Exception
     */
    public static function getModCache_MostRecent($modelNamespace,$modelInstanceId,$action,$userId=null) {
        $db = Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT * FROM orm_modcache WHERE "modelNamespace" = $1 AND "modelInstanceId" = $2 AND "action" = $3 ';
        $params = array($modelNamespace,$modelInstanceId,$action);
        if($userId) {
            $sql .= 'WHERE "userId" = $4 ';
            $params[] = $userId;
        }
        $sql .= 'ORDER BY "timestamp" DESC LIMIT 1';
        return $db->query($sql,$params)->fetchAssoc();
    }

    /**
     * Get all modcache entries by id/action/userid and time for a model type
     *
     * @param $modelNamespace
     * @param $modelInstanceId
     * @param $action
     * @param null $userId
     * @param null $startTimestamp
     * @param null $endTimestamp
     *
     * @return array
     * @throws Classes\Exceptions\DB_Exception
     */
    public static function getModCache_All($modelNamespace,$modelInstanceId,$action,$userId=null,$startTimestamp=null,$endTimestamp=null) {
        $db = Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT * FROM orm_modcache WHERE "modelNamespace" = $1 AND "modelInstanceId" = $2 AND "action" = $3 ';
        $params = array($modelNamespace,$modelInstanceId,$action);
        if($userId || $startTimestamp || $endTimestamp) {
            $sql .= 'WHERE ';
            $n = 4;
            if($userId) {
                $sql .= '"userId" = $'.$n.' AND ';
                $n++;
                $params[] = $userId;
            }
            if($startTimestamp) {
                $sql .= '"timestamp" >= $'.$n.' AND ';
                $n++;
                $params[] = $startTimestamp;
            }
            if($endTimestamp) {
                $sql .= '"timestamp" <= $'.$n.' AND ';
                $n++;
                $params[] = $endTimestamp;
            }
            $sql = substr($sql,0,strlen($sql)-4);
        }
        $sql .= ' ORDER BY "timestamp" DESC ';
        $rows = array();
        $query = $db->query($sql, $params);
        while($row = $query->fetchAssoc()) {
            $rows[] = $row;
        }
        return $row;
    }

    /**
     * Clear the entries from the modcache for a specific model. Useful when deleting a model
     *
     * @param $modelNamespace
     *
     * @throws Classes\Exceptions\DB_Exception
     */
    public static function clearModCacheByModel($modelNamespace) {
        $db = Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'DELETE FROM orm_modcache WHERE "modelNamespace" = $1';
        $db->query($sql,array($modelNamespace));
    }
}