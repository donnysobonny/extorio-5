<?php
namespace Core\Classes\Utilities;
/**
 * Utility methods for getting server information
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Server
 */
final class Server {

    /**
     * Get the request's "if modified since" value for caching
     *
     * @return null
     */
    public static function getModifiedSince() {
        $allHeaders = getallheaders();
        if(isset($allHeaders["If-Modified-Since"])) {
            return $allHeaders["If-Modified-Since"];
        }
        return null;
    }

    /**
     * Get the content type
     *
     * @return bool
     */
    public static function getContentType() {
        if(isset($_SERVER["CONTENT_TYPE"])) {
            return $_SERVER["CONTENT_TYPE"];
        }
        if(isset($_SERVER["HTTP_CONTENT_TYPE"])) {
            return $_SERVER["HTTP_CONTENT_TYPE"];
        }
        return false;
    }

    /**
     * Get the full domain, including http:// or https://
     *
     * @return string
     */
    public static function getDomain() {
        $domain = "http://";
        if(isset($_SERVER["HTTPS"])) {
            if($_SERVER["HTTPS"] == "on") {
                $domain = "https://";
            }
        }

        $domain .= $_SERVER["SERVER_NAME"];

        return $domain;
    }

    /**
     * Get the request method
     *
     * @return string
     */
    public static function getMethod() {
        $method = $_SERVER["REQUEST_METHOD"];

        if($method == "POST" && array_key_exists("HTTP_X_HTTP_METHOD", $_SERVER)) {
            if ($_SERVER["HTTP_X_HTTP_METHOD"] == "DELETE") {
                $method = "DELETE";
            } else if ($_SERVER["HTTP_X_HTTP_METHOD"] == "PUT") {
                $method = "PUT";
            } else {
                return false;
            }
        }

        return strtoupper($method);
    }

    /**
     * Get the requested uri (includes the query string)
     *
     * @return string
     */
    public static function getRequestURI() {
        return $_SERVER["REQUEST_URI"];
    }

    /**
     * Get the request url
     *
     * @return bool
     */
    public static function getRequestURL() {
        if(isset($_SERVER["REDIRECT_URL"])) {
            return $_SERVER["REDIRECT_URL"];
        } else {
            $uri = self::getRequestURI();
            if(strlen($uri)) {
                //we don't want the query string
                return Strings::notEndsWith(str_replace(self::getQueryString(),"",$uri),"?");
            }
        }
        return false;
    }

    /**
     * Get just the query string
     *
     * @return string
     */
    public static function getQueryString() {
        return $_SERVER["QUERY_STRING"];
    }

    /**
     * Get the current requests ip address.
     *
     * Retunrs null if no address could be found.
     *
     * @return string|null The ip address or NULL if couldn't be found
     */
    public static function getIpAddress() {
        if(isset($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        }
        if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        if(isset($_SERVER['REMOTE_ADDR'])) {
            return $_SERVER['REMOTE_ADDR'];
        }
        return false;
    }

    /**
     * Get the request time.
     *
     * @return string the request time
     */
    public static function getRequestTime() {

        return $_SERVER["REQUEST_TIME"];

    }

    /**
     * Get the remote port, used in api Logs
     *
     * @return string the remote port. Used in api Logs
     */
    public static function getRemotePort() {

        return $_SERVER["REMOTE_PORT"];

    }

    /**
     * Get the remote user for verifying that this app is registered
     *
     * @return string the remote user
     */
    public static function getRemoteUser() {

        return $_SERVER["REMOTE_USER"];

    }
}