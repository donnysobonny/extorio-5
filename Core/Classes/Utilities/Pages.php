<?php
namespace Core\Classes\Utilities;

class Pages {

    /**
     * Get the name of a page
     *
     * @param int $pageId
     *
     * @return string
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getPageName($pageId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT name FROM core_classes_models_page WHERE id = $1 LIMIT 1';
        $row = $db->query($sql,array($pageId))->fetchRow();
        if($row) {
            return $row[0];
        }
        return null;
    }

    /**
     * Check whether a user is able to read a page
     *
     * @param int $userId
     * @param int $pageId
     *
     * @return bool
     */
    public static function canUserReadPage($userId,$pageId) {
        if(!$userId) $userId = 0;
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT
        id
        FROM core_classes_models_page

        WHERE id = $1 AND
        (
          "allRead" = $2 OR
          $3 = ANY("readGroups")
        )
        LIMIT 1';
        $row = $db->query($sql,array($pageId,true,Users::getUserGroupIdByUser($userId)))->fetchRow();
        if($row) {
            return true;
        }
        return Users::userHasPrivilege($userId,"pages_read_all","Core");
    }

    /**
     * Check whether a user is able to modify a page
     *
     * @param int $userId
     * @param int $pageId
     *
     * @return bool
     */
    public static function canUserModifyPage($userId,$pageId) {
        if(!$userId) $userId = 0;
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT
        id
        FROM core_classes_models_page

        WHERE id = $1 AND
        $2 = ANY("modifyGroups")
        LIMIT 1';
        $row = $db->query($sql,array($pageId,Users::getUserGroupIdByUser($userId)))->fetchRow();
        if($row) {
            return true;
        }
        return Users::userHasPrivilege($userId,"pages_modify_all","Core");
    }
}