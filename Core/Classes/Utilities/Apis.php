<?php
namespace Core\Classes\Utilities;

class Apis {
    /**
     * Check whether a user is able to read an api
     *
     * @param int $userId
     * @param int $apiId
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function canUserReadApi($userId,$apiId) {
        if(!$userId) $userId = 0;
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT
        id
        FROM core_classes_models_api

        WHERE id = $1 AND
        (
          "allRead" = $2 OR
          $3 = ANY("readGroups")
        )
        LIMIT 1';
        $row = $db->query($sql,array($apiId,true,Users::getUserGroupIdByUser($userId)))->fetchRow();
        if($row) {
            return true;
        }
        return Users::userHasPrivilege($userId,"apis_read_all","Core");
    }
}