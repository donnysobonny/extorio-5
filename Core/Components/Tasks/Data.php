<?php
namespace Core\Components\Tasks;
use Core\Classes\Enums\ModelWebhookActions;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Model;
use Core\Classes\Models\ModelWebhook;
use Core\Classes\Models\User;

class Data extends \Core\Classes\Commons\Task {
    public function webhook($modelType=false,$modelInstanceId=false,$action=false) {
        //webhooks require curl
        if(!extension_loaded('curl')) {
            $this->_failTask("Webhooks require the curl extension");
        }

        if($modelType && $modelInstanceId && $action) {
            $this->_logToTask("Looking for model");

            //check for valid action
            if(!ModelWebhookActions::valueExists($action)) {
                $this->_failTask($action." is not a valid ModelWebhookAction");
            }

            $model = Model::findOne(
                Query::n()
                    ->where(array(
                        "modelNamespace" => $modelType
                    )),1
            );
            if(!$model) {
                $this->_failTask($modelType." could not be used to find a model");
            }

            /** @var Model $type */
            $type = $model->namespace;

            //check to see if there are any webhooks for this model and action
            $webhooks = ModelWebhook::findAll(
                Query::n()
                    ->where(array(
                        "modelNamespace" => $type,
                        "action" => $action
                    )),1
            );
            foreach($webhooks as $webhook) {
                //we pull the target model instance based on the logged in user's access rights
                $mi = $type::findById($modelInstanceId,INF);
                if($mi) {
                    $config = $this->_Extorio()->getConfig();
                    $data = json_decode(json_encode($mi),true);
                    $done = false;
                    $try = 1;
                    while(!$done) {
                        //we send the webhook
                        $this->_logToTask("Making attempt number ".$try);
                        $post = array(
                            "action" => $action,
                            "verification_key" => strlen($webhook->verificationKey) ? $webhook->verificationKey : $config["models"]["webhooks"]["default_verification_key"],
                            "attempt" => $try,
                            "data" => $data
                        );
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_exec($ch);
                        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);

                        //if the code is 200, we're done
                        if($httpCode == 200) {
                            $this->_logToTask("Location returned OK status. Done");
                            $done = true;
                        }

                        //if not done, we will retry
                        if(!$done) {
                            $this->_logToTask("Attempt failed as location returned ".$httpCode.". Waiting to retry");
                            sleep($config["models"]["webhooks"]["retry_delay"]);
                            $try++;
                            if($try > $config["models"]["webhooks"]["max_retries"]) {
                                $done = true;
                                $this->_logToTask("Reached maximum number of tries");
                                $this->_failTask("Reached maximum number of tries");
                            }
                        }
                    }
                }
            }
        } else {
            $this->_failTask("modelType, modelInstanceId or action not found in request");
        }
    }
}