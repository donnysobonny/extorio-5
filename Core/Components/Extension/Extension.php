<?php
namespace Core\Components\Extension;
use Core\Classes\Commons\Extorio;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Commons\FileSystem\Image;
use Core\Classes\Enums\FontAwesomeIcons;
use Core\Classes\Enums\InternalEvents;
use Core\Classes\Enums\InternalPages;
use Core\Classes\Enums\UserContactType;
use Core\Classes\Helpers\AdminMenuLeft_Item;
use Core\Classes\Helpers\AdminMenuLeft_Menu;
use Core\Classes\Helpers\AdminMenuTop_Item;
use Core\Classes\Helpers\AdminMenuTop_Menu;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\UserTab;
use Core\Classes\Models\Language;
use Core\Classes\Models\Page;
use Core\Classes\Models\User;
use Core\Classes\Models\UserAction;
use Core\Classes\Models\UserActionType;
use Core\Classes\Models\UserContactTopic;
use Core\Classes\Models\UserGroup;
use Core\Classes\Utilities\Localization;
use Core\Classes\Utilities\Pages;
use Core\Classes\Utilities\Users;

final class Extension extends \Core\Classes\Commons\Extension {

    public function _getPublicRepo() {
        return "https://donnysobonny@bitbucket.org/donnysobonny/extorio-5-core.git";
    }

    public function _getPublicBuild() {
        return intval(file_get_contents("http://build.extorio.com/index.php"));
    }

    public function _getCurrentBuild() {
        return 0;
    }

    protected function _onPatch($build) {
        switch ($build) {
            default :
                $this->_Extorio()->cacheConfig();
                break;
        }
    }

    protected function _onStart() {

        //if on the user user edit page
        $method = $this->_Extorio()->getTargetPageMethod();
        $page = $this->_Extorio()->getTargetPage();
        //if on login page
        if($page->address == InternalPages::_userLogin) {
            $this->_Extorio()->bindActionToEvent(InternalEvents::_user_login_controller, function() {
                if(isset($_POST["user_login_submitted"])) {
                    $error = false;
                    try {
                        Users::login($_POST["username"],$_POST["password"],30,isset($_POST["keep_logged_in"]));
                    } catch(\Exception $ex) {
                        $error = $ex->getMessage();
                    }
                    if($error) {
                        Extorio::get()->messageError($error);
                    } else {
                        $r = "";
                        if(isset($_GET["r"])) {
                            $r = $_GET["r"];
                        } else {
                            //find the default page
                            $p = Page::findOne(Query::n()->where(array("default"=>true)),1);
                            if($p) {
                                $r = $p->address;
                            } else {
                                $r = "/";
                            }
                        }
                        Extorio::get()->messageSuccess("Login successful");
                        header("Location: ".$r);
                        exit;
                    }
                }
            });
            $this->_Extorio()->bindActionToEvent(InternalEvents::_user_login_view, function() {
                $config = Extorio::get()->getConfig();
                $registerEnabled = $config["users"]["registration"]["enabled"];
                $passwordResetEnabled = $config["users"]["password_reset"]["enabled"];
                ?>
                <form method="post" action="">
                    <div class="form-group">
                        <label for="username">Username/Email address</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Enter your username or email address">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Enter your password">
                    </div>
                    <div class="checkbox">
                        <label>
                            <input id="keep_logged_in" name="keep_logged_in" type="checkbox"> Keep me logged in for 30 days
                        </label>
                    </div>
                    <button type="submit" name="user_login_submitted" class="btn btn-primary">Log in</button>
                    <?php
                    if($registerEnabled) {
                        ?>
                        <p class="help-block">Need an account? <a class="" href="<?=InternalPages::_userRegister?>">Click here to register</a></p>
                        <?php
                    }
                    ?>
                    <?php
                    if($passwordResetEnabled) {
                        ?>
                        <p class="help-block">Forgot your password? <a class="" href="<?=InternalPages::_userForgotPassword?>">Click here</a></a></p>
                        <?php
                    }
                    ?>
                </form>
                <?php
            });
        }
        //if on register page
        if($page->address == InternalPages::_userRegister) {
            $this->_Extorio()->bindActionToEvent(/**
             *
             * @param User $user
             */
                InternalEvents::_user_register_controller, function() {
                $config = Extorio::get()->getConfig();
                $registrationEnabled = $config["users"]["registration"]["enabled"];

                if(isset($_POST["user_register_submitted"]) && $registrationEnabled) {
                    $username = $_POST["username"];
                    $email = $_POST["email"];
                    $cemail = $_POST["cemail"];
                    $password = $_POST["password"];
                    $cpassword = $_POST["cpassword"];

                    $error = false;
                    try{
                        if($email != $cemail) {
                            throw new \Exception("The email addresses that you entered did not match. Please try again.");
                        }
                        if($password != $cpassword) {
                            throw new \Exception("The passwords that you entered did not match. Please try again.");
                        }
                        Users::register($username,$password,$email);
                    } catch(\Exception $ex) {
                        $error = $ex->getMessage();
                    }
                    if($error) {
                        $this->_Extorio()->messageError($error);
                    } else {
                        //registration is successful, display message based on whether authentication will happen or not
                        if($config["users"]["registration"]["authenticate_users"]) {
                            Extorio::get()->messageInfo("An email has been sent to the email address provided, containing a link to authenticate your account. Please follow the instructions in this email to continue.");
                        } else {
                            Extorio::get()->messageSuccess("Registration successful! You may not log in.");
                        }
                    }
                }
            });
            $this->_Extorio()->bindActionToEvent(InternalEvents::_user_register_view, function() {
                $config = Extorio::get()->getConfig();
                $registrationEnabled = $config["users"]["registration"]["enabled"];
                if($registrationEnabled) {
                    ?>
                    <form method="post" action="">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Enter a username">
                        </div>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter a valid email address">
                        </div>
                        <div class="form-group">
                            <label for="cemail">Confirm Email address</label>
                            <input type="email" class="form-control" id="cemail" name="cemail" placeholder="Confirm your email address">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Enter your password">
                        </div>
                        <div class="form-group">
                            <label for="cpassword">Confirm password</label>
                            <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Confirm your password">
                        </div>
                        <button type="submit" name="user_register_submitted" class="btn btn-primary">Register</button>
                        <p class="help-block">Already have an account? <a class="" href="<?=InternalPages::_userLogin?>">Log in here</a></p>
                    </form>
                    <?php
                } else {
                    ?>
                    <div class="alert alert-warning">
                        Registration is currently disabled.
                    </div>
                    <?php
                }
            });
        }
        //if on my account page
        if($page->address == InternalPages::_myAccount) {
            $this->_Extorio()->bindActionToEvent(InternalEvents::_my_account_controller, function() {
                $loggedInUser = Extorio::get()->getLoggedInUser();
                $config = Extorio::get()->getConfig();
                $canChangeUsername = $config["users"]["can_change_username"];
                $canChangeEmail = $config["users"]["can_change_email"];
                $canChangePassword = $config["users"]["can_change_password"];

                if(isset($_POST["user_basic_details_submit"])) {
                    if(strlen($_FILES["avatar"]["name"])) {
                        $file = $_FILES["avatar"];
                        //check the image type
                        $check = File::constructFromPath($file["name"]);
                        if(!in_array($check->extension(), array(
                            "jpg",
                            "jpeg",
                            "png",
                            "gif"
                        ))) {
                            $this->_Extorio()->messageError("The avatar image is not a valid file type");
                        } else {
                            //try to upload
                            if(move_uploaded_file($file["tmp_name"],"Application/Assets/images/avatars/".$loggedInUser->id."_avatar.".$check->extension())) {
                                $image = Image::constructFromPath("Application/Assets/images/avatars/".$loggedInUser->id."_avatar.".$check->extension());
                                //set the smallest
                                $image->resizeSmallestSide(150);
                                //crop
                                $image->cropAtCenter(150,150);
                                $image->save();

                                $loggedInUser->avatar = "/Application/Assets/images/avatars/".$loggedInUser->id."_avatar.".$check->extension();
                            }
                        }
                    }

                    $loggedInUser->firstname = $_POST["firstname"];
                    $loggedInUser->lastname = $_POST["lastname"];
                    $loggedInUser->language = intval($_POST["language"]);

                    $error = false;
                    try {
                        $loggedInUser->pushThis();
                    } catch(\Exception $ex) {
                        $error = $ex->getMessage();
                    }

                    if($error) {
                        Extorio::get()->messageError($error);
                    } else {
                        Extorio::get()->messageSuccess("Details successfully updated");
                    }

                    //user_update_basic action
                    UserAction::qc(
                        $loggedInUser->id,
                        UserActionType::findByName("user_update_basic")
                    );
                }

                if(isset($_POST["user_change_username_submit"]) && $canChangeUsername) {
                    $error = false;
                    try{
                        Users::changeUsername($this->_Extorio()->getLoggedInUser(),$_POST["username"]);

                        //user_update_username action
                        UserAction::qc(
                            $loggedInUser->id,
                            UserActionType::findByName("user_update_username")
                        );
                    } catch(\Exception $ex) {
                        $error = $ex->getMessage();
                    }
                    if($error) {
                        $this->_Extorio()->messageError($error);
                    } else {
                        $this->_Extorio()->messageSuccess("Username successfully updated");
                    }
                }

                if(isset($_POST["user_change_email_submit"]) && $canChangeEmail) {
                    $error = false;
                    try{
                        Users::changeEmail($this->_Extorio()->getLoggedInUser(),$_POST["email"]);

                        //user_update_email action
                        UserAction::qc(
                            $loggedInUser->id,
                            UserActionType::findByName("user_update_email")
                        );
                    } catch(\Exception $ex) {
                        $error = $ex->getMessage();
                    }
                    if($error) {
                        Extorio::get()->messageError($error);
                    } else {
                        Extorio::get()->messageSuccess("Email successfully updated");
                    }
                }

                if(isset($_POST["user_change_password_submit"]) && $canChangePassword) {

                    $password = $_POST["password"];
                    $npassword = $_POST["npassword"];
                    $cpassword = $_POST["cpassword"];

                    $error = false;
                    try{
                        $user = $this->_Extorio()->getLoggedInUser();

                        if($user->password != $password) {
                            throw new \Exception("There was a problem when trying to update your password. Please try again.");
                        }

                        if($npassword != $cpassword) {
                            throw new \Exception("There was a problem when trying to update your password. Please try again.");
                        }

                        Users::changePassword($user,$npassword);

                        //user_update_password action
                        UserAction::qc(
                            $loggedInUser->id,
                            UserActionType::findByName("user_update_password")
                        );
                    } catch(\Exception $ex) {
                        $error = $ex->getMessage();
                    }
                    if($error) {
                        Extorio::get()->messageError($error);
                    } else {
                        Extorio::get()->messageSuccess("Password successfully updated");
                    }
                }
            });
            $this->_Extorio()->bindActionToEvent(InternalEvents::_my_account_view_tabs, function() {
                return array(
                    UserTab::n("mydetails","My Details",FontAwesomeIcons::_user)
                );
            });
            $this->_Extorio()->bindActionToEvent(InternalEvents::_my_account_view_content, function($name) {
                switch($name) {
                    case "mydetails" :
                        $loggedInUser = Extorio::get()->getLoggedInUser();
                        $config = Extorio::get()->getConfig();
                        $canChangeUsername = $config["users"]["can_change_username"];
                        $canChangeEmail = $config["users"]["can_change_email"];
                        $canChangePassword = $config["users"]["can_change_password"];
                        $langs = Language::findAll(Query::n()->where(array("enabled" => true))->order("name"));
                        $dlang = Localization::getCurrentLanguage();
                        ?>
                        <div class="row">
                            <div class="col-md-4">
                                <div style="margin-bottom: 0;" class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Basic details</h3>
                                    </div>
                                    <div class="panel-body">
                                        <form method="post" action="" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="firstname">First name</label>
                                                        <input value="<?=$loggedInUser->firstname?>" type="text" class="form-control" id="firstname" name="firstname" placeholder="First name">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="lastname">Last name</label>
                                                        <input value="<?=$loggedInUser->lastname?>" type="text" class="form-control" id="lastname" name="lastname" placeholder="Last name">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tbody>
                                                    <tr>
                                                        <td style="vertical-align: top">
                                                            <img style="margin-right: 15px; margin-bottom: 0;" class="thumbnail" src="<?=$loggedInUser->avatar?>" width="150" height="150" />
                                                        </td>
                                                        <td style="vertical-align: top">
                                                            <label for="role_select">Avatar</label>
                                                            <p class="help-block">Accepted file types: jpeg, jpg, png, gif. Recommended dimensions: 150x150 (will be cropped to this size)</p>
                                                            <input type="file" name="avatar" id="avatar" />
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="form-group">
                                                <label for="language">Preferred language</label>
                                                <select class="form-control" name="language" id="language">
                                                    <?php
                                                    foreach($langs as $lang) {
                                                        ?><option <?php
                                                        if($lang->id == $dlang->id) echo 'selected="selected"';
                                                        ?> value="<?=$lang->id?>"><?=$lang->name?></option><?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <button type="submit" name="user_basic_details_submit" class="btn btn-primary">Update</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <?php
                                if($canChangeUsername) {
                                    ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Change username</h3>
                                        </div>
                                        <div class="panel-body">
                                            <form class="form-inline" method="post" action="">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="username" name="username" placeholder="Enter a new username">
                                                </div>
                                                <button type="submit" name="user_change_username_submit" class="btn btn-primary">Change username</button>
                                            </form>
                                        </div>
                                    </div>
                                    <?php
                                }
                                if($canChangeEmail) {
                                    ?>
                                    <div style="margin-bottom: 0;"  class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Change email</h3>
                                        </div>
                                        <div class="panel-body">
                                            <form class="form-inline" method="post" action="">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="email" name="email" placeholder="Enter a new email address">
                                                </div>
                                                <button type="submit" name="user_change_email_submit" class="btn btn-primary">Change email</button>
                                            </form>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="col-md-4">
                                <?php
                                if($canChangePassword) {
                                    ?>
                                    <div style="margin-bottom: 0;"  class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Change password</h3>
                                        </div>
                                        <div class="panel-body">
                                            <form method="post" action="">
                                                <div class="form-group">
                                                    <label for="password">Current password</label>
                                                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter current password">
                                                </div>
                                                <hr />
                                                <div class="form-group">
                                                    <label for="npassword">New password</label>
                                                    <input type="password" class="form-control" id="npassword" name="npassword" placeholder="Enter new password">
                                                </div>
                                                <div class="form-group">
                                                    <label for="cpassword">Confirm password</label>
                                                    <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Confirm new password">
                                                </div>
                                                <button type="submit" name="user_change_password_submit" class="btn btn-primary">Change password</button>
                                            </form>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                        break;
                }
            });
        }
        //if on profiles page
        if($page->address == InternalPages::_users) {
            //TODO: work out what should display on this page by default
        }
        //if on user create page
        if($page->address == InternalPages::_extorioAdminUsers && $method == "create") {
            $this->_Extorio()->bindActionToEvent(InternalEvents::_user_create_controller, function() {
                if(isset($_POST["submit"]) || isset($_POST["submit_exit"])) {
                    $u = User::n();
                    $u->username = $_POST["username"];
                    $u->password = $_POST["password"];
                    $u->email = $_POST["email"];
                    $u->canLogin = isset($_POST["canLogin"]);
                    $u->userGroup = intval($_POST["group"]);

                    $error = false;
                    try {
                        $u->pushThis();
                    } catch(\Exception $ex) {
                        $error = $ex->getMessage();
                    }
                    if($error) {
                        Extorio::get()->messageError($error);
                    } else {
                        Extorio::get()->messageSuccess("User successfully saved");

                        //user_create action
                        UserAction::qc(
                            Extorio::get()->getLoggedInUserId(),
                            UserActionType::findByName("user_create"),
                            null,
                            null,
                            $u->id
                        );

                        if(isset($_POST["submit_exit"])) {
                            header("Location: /extorio-admin/users");
                            exit;
                        } else {
                            header("Location: /extorio-admin/users/edit/".$u->id);
                            exit;
                        }
                    }
                }
            });
            $this->_Extorio()->bindActionToEvent(InternalEvents::_user_create_view, function() {
                $loggedInUser = Extorio::get()->getLoggedInUser();
                $groups = $loggedInUser->userGroup->manageGroups;
                if($loggedInUser->userGroup->manageAll) {
                    $groups = UserGroup::findAll(Query::n()->order("name"));
                }
                ?>
                <form method="post" action="">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input value="" type="text" class="form-control" id="username" name="username" placeholder="Enter username">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input value="" type="text" class="form-control" id="password" name="password" placeholder="Enter password">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input value="" type="text" class="form-control" id="email" name="email" placeholder="Enter email">
                    </div>
                    <div class="checkbox">
                        <label>
                            <input checked="checked" id="canLogin" name="canLogin" type="checkbox"> Can login
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="group">User group</label>
                        <select class="form-control" name="group" id="group">
                            <?php
                            foreach($groups as $group) {
                                ?><option value="<?=$group->id?>"><?=$group->name?></option><?php
                            }
                            ?>
                        </select>
                    </div>
                    <button type="submit" name="submit" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
                    <button type="submit" name="submit_exit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
                </form>
                <?php
            });
        }
        //if on user edit page
        if($page->address == InternalPages::_extorioAdminUsers && $method == "edit") {
            $this->_Extorio()->bindActionToEvent(InternalEvents::_user_edit_controller, function($user) {
                /** @var User $user */
                $user = $user;
                if(isset($_POST["basic_submit"]) || isset($_POST["basic_submit_exit"])) {
                    $user->username = $_POST["username"];
                    $user->email = $_POST["email"];
                    $user->password = $_POST["password"];
                    $user->userGroup = intval($_POST["group"]);
                    $user->bio = $_POST["bio"];
                    $user->canLogin = isset($_POST["can_login"]);

                    $error = false;
                    try{
                        $user->pushThis();
                    } catch(\Exception $ex) {
                        $error = $ex->getMessage();
                    }
                    if($error) {
                        Extorio::get()->messageError($error);
                    } else {
                        Extorio::get()->messageSuccess("User saved");

                        //user_modify action
                        UserAction::qc(
                            Extorio::get()->getLoggedInUserId(),
                            UserActionType::findByName("user_modify"),
                            null,
                            null,
                            $user->id
                        );

                        if(isset($_POST["basic_submit_exit"])) {
                            header("Location: /extorio-admin/users");
                            exit;
                        } else {
                            header("Location: /extorio-admin/users/edit/".$user->id);
                            exit;
                        }
                    }
                }
            });
            $this->_Extorio()->bindActionToEvent(InternalEvents::_user_edit_view_tabs, function() {
                return array(
                    UserTab::n("basicdetails","Basic Details",FontAwesomeIcons::_user),
                    UserTab::n("notes","Notes",FontAwesomeIcons::_comment),
                    UserTab::n("actions","Actions",FontAwesomeIcons::_bell),
                    UserTab::n("contacts","Contacts",FontAwesomeIcons::_share)
                );
            });
            $this->_Extorio()->bindActionToEvent(InternalEvents::_user_edit_view_content, function($name,$user) {
                /** @var User $user */
                $user = $user;
                $config = Extorio::get()->getConfig();
                $loggedInUser = Extorio::get()->getLoggedInUser();
                Extorio::get()->includeIncluder("extorio_editable");
                Extorio::get()->includeIncluder("timeago");
                switch($name) {
                    case "basicdetails" :
                        $manageGroups = $loggedInUser->userGroup->manageGroups;
                        if($loggedInUser->userGroup->manageAll) {
                            $manageGroups = UserGroup::findAll(Query::n()->order("name"));
                        }
                        $canChangeUsername = $config["users"]["can_change_username"];
                        $canChangeEmail = $config["users"]["can_change_email"];
                        $canChangePassword = $config["users"]["can_change_password"];
                        ?>
                        <form method="post" action="" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Avatar</label>
                                <div class="col-sm-10">
                                    <img style="max-width: 100px;" class="img-thumbnail" src="<?=$user->avatar?>" alt="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="username" class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-10">
                                    <input <?php
                                    if(!$canChangeUsername) echo 'disabled="disabled"';
                                    ?> value="<?=$user->username?>" type="text" class="form-control" id="username" name="username" placeholder="Enter the username">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input <?php
                                    if(!$canChangeEmail) echo 'disabled="disabled"';
                                    ?> value="<?=$user->email?>" type="text" class="form-control" id="email" name="email" placeholder="Enter the email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-10">
                                    <input <?php
                                    if(!$canChangePassword) echo 'disabled="disabled"';
                                    ?> value="<?=$user->password?>" type="text" class="form-control" id="password" name="password" placeholder="Enter the password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="group" class="col-sm-2 control-label">Group</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="group" id="group">
                                        <?php
                                        foreach($manageGroups as $group) {
                                            ?><option <?php
                                            if($user->userGroup->id == $group->id) echo 'selected="selected"';
                                            ?> value="<?=$group->id?>"><?=$group->name?></option><?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bio" class="col-sm-2 control-label">Bio</label>
                                <div class="col-sm-10">
                                    <textarea id="bio" name="bio"><?=$user->bio?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($user->canLogin) echo 'checked="checked"';
                                            ?> name="can_login" id="can_login" type="checkbox"> User can log in
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div style="margin-bottom: 0;" class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" name="basic_submit" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
                                    <button type="submit" name="basic_submit_exit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
                                </div>
                            </div>
                        </form>
                        <script>
                            $(function() {
                                $('#bio').extorio_editable_basic();
                            });
                        </script>
                        <?php
                        break;
                    case "notes" :
                        ?>
                        <div id="user_notes" class="row">
                            <div class="col-sm-4">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Add new note</h3>
                                    </div>
                                    <div class="panel-body">
                                        <textarea id="user_note_content"></textarea>
                                        <br />
                                        <button class="btn btn-block btn-sm btn-primary user_note_add"><span class="fa fa-plus"></span> Add note</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="list-group" id="user_note_container">

                                </div>
                                <div style="margin-bottom: 0;" class="well well-sm">
                                    <table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align: middle;">
                                                    Page <span class="user_note_cur">0</span> of <span class="user_note_tot">0</span> (from <span class="user_note_res">0</span> total results)
                                                </td>
                                                <td style="vertical-align: middle; text-align: right;">
                                                    <div class="btn-group btn-group-xs" role="group">
                                                        <button type="button" class="btn btn-default user_note_prev"><span class="fa fa-chevron-left"></span></button>
                                                        <button type="button" class="btn btn-default user_note_next"><span class="fa fa-chevron-right"></span></button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <script>
                            $(function() {
                                $('#user_note_content').extorio_editable_basic();
                                var userNotes = $('#user_notes');
                                var userNoteContainer = $('#user_note_container');
                                var userId = <?=$user->id?>;
                                var loggedInId = <?=$loggedInUser->id?>;
                                var notePage = 0;
                                var canRead = <?php
                                if(Users::userManagesUserAndHasPrivilege($loggedInUser->id,$user->id,"user_notes_read")) echo "true"; else echo "false";
                                ?>;
                                var loadNotes = function() {
                                    if(canRead) {
                                        userNotes.extorio_showLoader();
                                        $.extorio_api({
                                            endpoint: "/user-notes/filter",
                                            data: {
                                                userId: userId,
                                                orderBy: "dateUpdated",
                                                orderDirection: "desc",
                                                limit: 5,
                                                skip: notePage * 5
                                            },
                                            oncomplete: function() {
                                                userNotes.extorio_hideLoader();
                                            },
                                            onsuccess: function(resp) {
                                                userNoteContainer.html("");
                                                if(resp.data.length > 0) {
                                                    for(var i = 0; i < resp.data.length; i++) {
                                                        var note = resp.data[i];
                                                        var item = $('<div class="list-group-item">' +
                                                            '   <div class="media">' +
                                                            '       <div class="media-left">' +
                                                            '           <img style="max-width: 100px;" class="media-object user_note_avatar img-thumbnail" src="/Core/Assets/images/default_avatar.png">' +
                                                            '       </div>' +
                                                            '       <div class="media-body">' +
                                                            '           <button style="float: right; opacity: 0.5;" data-id="'+note.id+'" class="btn btn-xs btn-danger user_note_delete"><span class="fa fa-trash"></span></button>' +
                                                            '           '+note.body+'' +
                                                            '           <hr style="margin: 5px 0px;" />' +
                                                            '           <small class="text-muted">by <span class="user_note_by_name">unknown</span> <span class="user_note_timeago"></span></small>' +
                                                            '       </div>' +
                                                            '   </div>' +
                                                            '</div>');
                                                        item.find('.user_note_avatar').extorio_fetchUserAvatar(note.updatedById);
                                                        item.find('.user_note_by_name').extorio_fetchUserShortName(note.updatedById);
                                                        item.find('.user_note_timeago').html($.timeago(note.dateUpdated));
                                                        if(note.updatedById == loggedInId) {
                                                            item.find('.user_note_delete').on("click", function() {
                                                                var id = $(this).attr('data-id');
                                                                $.extorio_modal({
                                                                    title: "Delete note",
                                                                    size: "modal-sm",
                                                                    content: "Are you sure you want to delete this note?",
                                                                    oncontinuebutton: function() {
                                                                        userNotes.extorio_showLoader();
                                                                        $.extorio_api({
                                                                            endpoint: "/user-notes/" + id + "/delete",
                                                                            type: "POST",
                                                                            oncomplete: function() {
                                                                                userNotes.extorio_hideLoader();
                                                                            },
                                                                            onsuccess: function(r) {
                                                                                $.extorio_messageInfo("Note deleted");
                                                                                loadNotes();
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            });
                                                        } else {
                                                            item.find('.user_note_delete').hide();
                                                        }

                                                        userNoteContainer.append(item);
                                                    }

                                                    var totalPages = Math.ceil(resp.countUnlimited / 5);
                                                    if(totalPages <= 0) {
                                                        totalPages = 1;
                                                    }
                                                    if(notePage > 0) {
                                                        $('.user_note_prev').removeClass("disabled").removeAttr("disabled");
                                                    } else {
                                                        $('.user_note_prev').addClass("disabled").attr("disabled","disabled");
                                                    }
                                                    if((notePage + 1) < totalPages) {
                                                        $('.user_note_next').removeClass("disabled").removeAttr("disabled");
                                                    } else {
                                                        $('.user_note_next').addClass("disabled").attr("disabled","disabled");
                                                    }

                                                    $('.user_note_cur').html(notePage+1);
                                                    $('.user_note_tot').html(totalPages);
                                                    $('.user_note_res').html(resp.countUnlimited);

                                                } else {
                                                    userNoteContainer.html('<div class="list-group-item">This user doesn\'t have any notes</div>');
                                                    $('.user_note_prev').addClass("disabled").attr("disabled","disabled");
                                                    $('.user_note_next').addClass("disabled").attr("disabled","disabled");
                                                    $('.user_note_cur').html(0);
                                                    $('.user_note_tot').html(0);
                                                    $('.user_note_res').html(0);
                                                }
                                            }
                                        });
                                    } else {
                                        userNoteContainer.html('<div class="list-group-item">You are not able to read this user\'s notes</div>');
                                    }
                                }

                                $('.user_note_prev').on("click", function() {
                                    notePage--;
                                    loadNotes();
                                });

                                $('.user_note_next').on("click", function() {
                                    notePage++;
                                    loadNotes();
                                });

                                $('.user_note_add').on("click", function() {
                                    userNotes.extorio_showLoader();
                                    var content = $('#user_note_content').val();
                                    $.extorio_api({
                                        endpoint: "/user-notes",
                                        type: "POST",
                                        data: {
                                            data : {
                                                userId: userId,
                                                body: content,
                                            }
                                        },
                                        oncomplete: function() {
                                            userNotes.extorio_hideLoader();
                                        },
                                        onsuccess: function(r) {
                                            $.extorio_messageSuccess("Note added");
                                            notePage = 0;
                                            loadNotes();
                                        }
                                    });
                                });

                                loadNotes();
                            });
                        </script>
                        <?php
                        break;
                    case "actions" :
                        ?>
                        <div id="user_actions">
                            <button class="btn btn-xs btn-default user_action_refresh"><span class="fa fa-refresh"></span> refresh</button>
                            <br /><br />
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="list-group" id="user_actions_container">

                                    </div>
                                    <div style="margin-bottom: 0;" class="well well-sm">
                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
                                            <tbody>
                                            <tr>
                                                <td style="vertical-align: middle;">
                                                    Page <span class="user_action_cur">0</span> of <span class="user_action_tot">0</span> (from <span class="user_action_res">0</span> total results)
                                                </td>
                                                <td style="vertical-align: middle; text-align: right;">
                                                    <div class="btn-group btn-group-xs" role="group">
                                                        <button type="button" class="btn btn-default user_action_prev"><span class="fa fa-chevron-left"></span></button>
                                                        <button type="button" class="btn btn-default user_action_next"><span class="fa fa-chevron-right"></span></button>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            $(function() {
                                var actionPage = 0;
                                var userId = <?=$user->id?>;
                                var loggedInId = <?=$loggedInUser->id?>;
                                var userActions = $('#user_actions');
                                var userActionsContainer = $('#user_actions_container');
                                var canRead = <?php
                                    if(Users::userManagesUserAndHasPrivilege($loggedInUser->id,$user->id,"user_actions_read")) echo "true"; else echo "false";
                                    ?>;
                                var loadActions = function() {
                                    if(canRead) {
                                        userActions.extorio_showLoader();
                                        $.extorio_api({
                                            endpoint: "/user-actions/filter",
                                            data: {
                                                userId: userId,
                                                limit: 5,
                                                skip: actionPage * 5
                                            },
                                            oncomplete: function() {
                                                userActions.extorio_hideLoader();
                                            },
                                            onsuccess: function(resp) {
                                                userActionsContainer.html('');
                                                if(resp.data.length > 0) {
                                                    for(var i = 0; i < resp.data.length; i++) {
                                                        var action = resp.data[i];
                                                        var item = $('<div class="list-group-item">' +
                                                            '   <h4 class="list-group-item-heading"><a data-id="'+action.id+'" class="user_action_display" href="javascript:;">'+action.type.description+'</a></h4>' +
                                                            '   <p class="list-group-item-text user_action_timeago"></p>' +
                                                            '</div>');
                                                        item.find('.user_action_timeago').html($.timeago(action.date + " " + action.time));
                                                        item.find('.user_action_display').on("click", function() {
                                                            var id = $(this).attr('data-id');
                                                            $.extorio_showFullPageLoader();
                                                            $.extorio_api({
                                                                endpoint: "/user-actions/" + id,
                                                                oncomplete: function() {
                                                                    $.extorio_hideFullPageLoader();
                                                                },
                                                                onsuccess: function(r) {
                                                                    var a = r.data;
                                                                    $.extorio_dialog({
                                                                        title: a.type.description,
                                                                        content: '<table style="margin-bottom: 0;" class="table table-condensed table-striped">' +
                                                                        '   <tbody>' +
                                                                        '       <tr>' +
                                                                        '           <td style="font-weight: bold; text-align: right; white-space: nowrap;">Date</td>' +
                                                                        '           <td style="width: 100%;">'+a.date+'</td>' +
                                                                        '       </tr>' +
                                                                        '       <tr>' +
                                                                        '           <td style="font-weight: bold; text-align: right; white-space: nowrap;">Time</td>' +
                                                                        '           <td style="width: 100%;">'+a.time+'</td>' +
                                                                        '       </tr>' +
                                                                        '       <tr class="detail1">' +
                                                                        '           <td class="detail1_name" style="font-weight: bold; text-align: right; white-space: nowrap;"></td>' +
                                                                        '           <td class="detail1_value" style="width: 100%;"></td>' +
                                                                        '       </tr>' +
                                                                        '       <tr class="detail2">' +
                                                                        '           <td class="detail2_name" style="font-weight: bold; text-align: right; white-space: nowrap;"></td>' +
                                                                        '           <td class="detail2_value" style="width: 100%;"></td>' +
                                                                        '       </tr>' +
                                                                        '       <tr class="detail3">' +
                                                                        '           <td class="detail3_name" style="font-weight: bold; text-align: right; white-space: nowrap;"></td>' +
                                                                        '           <td class="detail3_value" style="width: 100%;"></td>' +
                                                                        '       </tr>' +
                                                                        '   </tbody>' +
                                                                        '</table>',
                                                                        onopen: function() {
                                                                            var body = this.find('.modal-body');
                                                                            if(a.detail1.length > 0) {
                                                                                body.find('.detail1_name').html(a.type.detail1Description);
                                                                                body.find('.detail1_value').html(a.detail1);
                                                                            } else {
                                                                                body.find('.detail1').hide();
                                                                            }
                                                                            if(a.detail2.length > 0) {
                                                                                body.find('.detail2_name').html(a.type.detail2Description);
                                                                                body.find('.detail2_value').html(a.detail2);
                                                                            } else {
                                                                                body.find('.detail2').hide();
                                                                            }
                                                                            if(a.detail3.length > 0) {
                                                                                body.find('.detail3_name').html(a.type.detail3Description);
                                                                                body.find('.detail3_value').html(a.detail3);
                                                                            } else {
                                                                                body.find('.detail3').hide();
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        });

                                                        userActionsContainer.append(item);
                                                    }

                                                    var totalPages = Math.ceil(resp.countUnlimited / 5);
                                                    if(totalPages <= 0) {
                                                        totalPages = 1;
                                                    }
                                                    if(actionPage > 0) {
                                                        $('.user_action_prev').removeClass("disabled").removeAttr("disabled");
                                                    } else {
                                                        $('.user_action_prev').addClass("disabled").attr("disabled","disabled");
                                                    }
                                                    if((actionPage + 1) < totalPages) {
                                                        $('.user_action_next').removeClass("disabled").removeAttr("disabled");
                                                    } else {
                                                        $('.user_action_next').addClass("disabled").attr("disabled","disabled");
                                                    }

                                                    $('.user_action_cur').html(actionPage+1);
                                                    $('.user_action_tot').html(totalPages);
                                                    $('.user_action_res').html(resp.countUnlimited);

                                                } else {
                                                    userActionsContainer.html('<div class="list-group-item">This user has no actions</div>');
                                                    $('.user_action_prev').addClass("disabled").attr("disabled","disabled");
                                                    $('.user_action_next').addClass("disabled").attr("disabled","disabled");
                                                    $('.user_action_cur').html(0);
                                                    $('.user_action_tot').html(0);
                                                    $('.user_action_res').html(0);
                                                }
                                            }
                                        });
                                    } else {
                                        userActionsContainer.html('<div class="list-group-item">You are not able to read this user\'s actions</div>');
                                    }
                                }

                                $('.user_action_refresh').on("click", function() {
                                    loadActions();
                                });

                                loadActions();
                            });
                        </script>
                        <?php
                        break;
                    case "contacts" :
                        ?>
                        <div id="user_contacts" class="row">
                            <div class="col-sm-4">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Add new contact</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="type">Type</label>
                                            <select class="form-control" id="type">
                                                <?php
                                                foreach(UserContactType::values() as $v) {
                                                    ?><option value="<?=$v?>"><?=$v?></option><?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="topic">Topic</label>
                                            <select class="form-control" id="topic">
                                                <?php
                                                foreach(UserContactTopic::findAll(Query::n()->order("name")) as $topic) {
                                                    ?><option value="<?=$topic->id?>"><?=$topic->name?></option><?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="identifier">Identifier</label>
                                            <input type="text" class="form-control" id="identifier" placeholder="(optional) identifier">
                                        </div>
                                        <button class="btn btn-sm btn-block btn-primary user_contact_add"><span class="fa fa-plus"></span> Add contact</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="list-group" id="user_contacts_container">
                                    
                                </div>
                                <div style="margin-bottom: 0;" class="well well-sm">
                                    <table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align: middle;">
                                                    Page <span class="user_contact_cur">0</span> of <span class="user_contact_tot">0</span> (from <span class="user_contact_res">0</span> total results)
                                                </td>
                                                <td style="vertical-align: middle; text-align: right;">
                                                    <div class="btn-group btn-group-xs" role="group">
                                                        <button type="button" class="btn btn-default user_contact_prev"><span class="fa fa-chevron-left"></span></button>
                                                        <button type="button" class="btn btn-default user_contact_next"><span class="fa fa-chevron-right"></span></button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <script>
                            $(function() {
                                var contactPage = 0;
                                var userId = <?=$user->id?>;
                                var loggedInId = <?=$loggedInUser->id?>;
                                var userContacts = $('#user_contacts');
                                var userContactsContainer = $('#user_contacts_container');
                                var canRead = <?php
                                    if(Users::userManagesUserAndHasPrivilege($loggedInUser->id,$user->id,"user_contacts_read")) echo "true"; else echo "false";
                                    ?>;
                                var loadContacts = function() {
                                    if(canRead) {
                                        userContacts.extorio_showLoader();
                                        $.extorio_api({
                                            endpoint: "/user-contacts/filter",
                                            data: {
                                                userId: userId,
                                                limit: 5,
                                                skip: contactPage * 5
                                            },
                                            oncomplete: function() {
                                                userContacts.extorio_hideLoader();
                                            },
                                            onsuccess: function(resp) {
                                                userContactsContainer.html('');
                                                if(resp.data.length > 0) {
                                                    for(var i = 0; i < resp.data.length; i++) {
                                                        var contact = resp.data[i];
                                                        var identifier = contact.identifier.length > 0 ? contact.identifier : "N/A";
                                                        var item = $('<div class="list-group-item">' +
                                                            '   <button data-id="'+contact.id+'" style="float: right; opacity: 0.5;" class="btn btn-xs btn-danger user_contact_delete"><span class="fa fa-trash"></span></button>' +
                                                            '   <h4 class="list-group-item-heading">Contacted by <span class="user_contact_by">unknown</span> <span class="user_contact_timeago"></span></h4>' +
                                                            '   <p class="list-group-item-text">' +
                                                            '       <strong>Type</strong>: '+contact.type+'<br />' +
                                                            '       <strong>Topic</strong>: '+contact.topic.name+'<br />' +
                                                            '       <strong>Identifier</strong>: '+identifier+'' +
                                                            '   </p>' +
                                                            '</div>');
                                                        if(contact.byUserId > 0) {
                                                            item.find('.user_contact_by').extorio_fetchUserShortName(contact.byUserId);
                                                        } else {
                                                            item.find('.user_contact_by').html("SYSTEM");
                                                        }

                                                        item.find('.user_contact_timeago').html($.timeago(contact.date+" "+contact.time));
                                                        if(contact.byUserId == loggedInId) {
                                                            item.find('.user_contact_delete').on("click", function() {
                                                                var id = $(this).attr("data-id");
                                                                $.extorio_modal({
                                                                    title: "Delete contact",
                                                                    content: "Are you sure that you want to delete this contact?",
                                                                    size: "modal-sm",
                                                                    oncontinuebutton: function() {
                                                                        $.extorio_showFullPageLoader();
                                                                        $.extorio_api({
                                                                            endpoint: "/user-contacts/" + id + "/delete",
                                                                            type: "POST",
                                                                            oncomplete: function() {
                                                                                $.extorio_hideFullPageLoader();
                                                                            },
                                                                            onsuccess: function(r) {
                                                                                $.extorio_messageInfo("User contact deleted");
                                                                                loadContacts();
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            });
                                                        } else {
                                                            item.find('.user_contact_delete').hide();
                                                        }

                                                        userContactsContainer.append(item);
                                                    }

                                                    var totalPages = Math.ceil(resp.countUnlimited / 5);
                                                    if(totalPages <= 0) {
                                                        totalPages = 1;
                                                    }
                                                    if(contactPage > 0) {
                                                        $('.user_contact_prev').removeClass("disabled").removeAttr("disabled");
                                                    } else {
                                                        $('.user_contact_prev').addClass("disabled").attr("disabled","disabled");
                                                    }
                                                    if((contactPage + 1) < totalPages) {
                                                        $('.user_contact_next').removeClass("disabled").removeAttr("disabled");
                                                    } else {
                                                        $('.user_contact_next').addClass("disabled").attr("disabled","disabled");
                                                    }

                                                    $('.user_contact_cur').html(contactPage+1);
                                                    $('.user_contact_tot').html(totalPages);
                                                    $('.user_contact_res').html(resp.countUnlimited);
                                                    
                                                } else {
                                                    userContactsContainer.html('<div class="list-group-item">This user has no contacts</div>');
                                                    $('.user_contact_prev').addClass("disabled").attr("disabled","disabled");
                                                    $('.user_contact_next').addClass("disabled").attr("disabled","disabled");
                                                    $('.user_contact_cur').html(0);
                                                    $('.user_contact_tot').html(0);
                                                    $('.user_contact_res').html(0);
                                                }
                                            }
                                        });
                                    } else {
                                        userContactsContainer.html('<div class="list-group-item">You are not able to view this user\'s contacts</div>');
                                    }
                                }

                                $('.user_contact_add').on("click", function() {
                                    userContacts.extorio_showLoader();
                                    $.extorio_api({
                                        endpoint: "/user-contacts",
                                        type: "POST",
                                        data: {
                                            data: {
                                                userId: userId,
                                                byUserId: loggedInId,
                                                type: $('#type').val(),
                                                topic: parseInt($('#topic').val()),
                                                identifier: $('#identifier').val()
                                            }
                                        },
                                        oncomplete: function() {
                                            userContacts.extorio_hideLoader();
                                        },
                                        onsuccess: function(resp) {
                                            $.extorio_messageSuccess("User contact created");
                                            contactPage = 0;
                                            loadContacts();
                                        }
                                    });
                                });

                                loadContacts();
                            });
                        </script>
                        <?php
                        break;
                }
            });
        }

        //user action detail display
        $this->_Extorio()->bindActionToEvent(InternalEvents::_user_actions_detail_display, function($userActionTypeName,$prop,$value) {
            switch($userActionTypeName) {
                case "user_create" :
                    switch($prop) {
                        case "detail1" :
                            return Users::getLongname($value);
                            break;
                    }
                    break;
                case "user_modify" :
                    switch($prop) {
                        case "detail1" :
                            return Users::getLongname($value);
                            break;
                    }
                    break;
                case "user_delete" :
                    switch($prop) {
                        case "detail1" :
                            return Users::getLongname($value);
                            break;
                    }
                    break;
            }
        });
        //user action detail filter
        $this->_Extorio()->bindActionToEvent(InternalEvents::_user_actions_detail_filter, function($userActionTypeName) {
            switch($userActionTypeName) {
                case "user_create" :
                    return array(
                        true
                    );
                    break;
                case "user_create" :
                    return array(
                        true
                    );
                    break;
                case "user_create" :
                    return array(
                        true
                    );
                    break;
            }
        });

        //if on an admin page
        if($this->_Extorio()->onExtorioAdminPage()) {
            //setup the top menus
            $this->_Extorio()->bindActionToEvent("extorio_admin_menu_top_menus",function() {
                return AdminMenuTop_Menu::n("usermenu","User Menu","","","","fa fa-user","right");
            },100,false);
            //setup the top menu items
            $this->_Extorio()->bindActionToEvent("extorio_admin_menu_top_items",function() {
                return array(
                    AdminMenuTop_Item::n("Account","manage account","/extorio-admin/account","/extorio-admin/account","fa fa-user"),
                    "",
                    AdminMenuTop_Item::n("Logout","logout","/extorio-admin/logout","/extorio-admin/logout","fa fa-sign-out")
                );
            },100,false);

            //setup the left menus
            $loggedInUser = $this->_Extorio()->getLoggedInUser();
            //extorio menu
            if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                "extorio_menus_all",
                "extorio_menus_extorio"
            ),"Core")) {
                $this->_Extorio()->bindActionToEvent("extorio_admin_menu_left_menus",function() {
                    return AdminMenuLeft_Menu::n("extorio","Extorio","",FontAwesomeIcons::_home);
                },100,false);
            }
            //content management
            if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                "extorio_menus_all",
                "extorio_menus_content_management"
            ),"Core")) {
                $this->_Extorio()->bindActionToEvent("extorio_admin_menu_left_menus",function() {
                    return AdminMenuLeft_Menu::n("contentmanagement","Content management","",FontAwesomeIcons::_th);
                },200,false);
            }
            //user management
            if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                "extorio_menus_all",
                "extorio_menus_user_management"
            ),"Core")) {
                $this->_Extorio()->bindActionToEvent("extorio_admin_menu_left_menus",function() {
                    return AdminMenuLeft_Menu::n("usermanagement","User management","",FontAwesomeIcons::_users);
                },300,false);
            }
            //email management
            if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                "extorio_menus_all",
                "extorio_menus_email_management"
            ),"Core")) {
                $this->_Extorio()->bindActionToEvent("extorio_admin_menu_left_menus",function() {
                    return AdminMenuLeft_Menu::n("emailmanagement","Email management","",FontAwesomeIcons::_envelope);
                },400,false);
            }
            //data management
            if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                "extorio_menus_all",
                "extorio_menus_data_management"
            ),"Core")) {
                $this->_Extorio()->bindActionToEvent("extorio_admin_menu_left_menus",function() {
                    return AdminMenuLeft_Menu::n("datamanagement","Data management","",FontAwesomeIcons::_database);
                },500,false);
            }
            //settings
            if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                "extorio_menus_all",
                "extorio_menus_settings"
            ),"Core")) {
                $this->_Extorio()->bindActionToEvent("extorio_admin_menu_left_menus",function() {
                    return AdminMenuLeft_Menu::n("settings","Settings","",FontAwesomeIcons::_cogs);
                },600,false);
            }
            //developers
            if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                "extorio_menus_all",
                "extorio_menus_developers"
            ),"Core")) {
                $this->_Extorio()->bindActionToEvent("extorio_admin_menu_left_menus",function() {
                    return AdminMenuLeft_Menu::n("developers","Developers","",FontAwesomeIcons::_terminal);
                },700,false);
            }
            //utilities
            if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                "extorio_menus_all",
                "extorio_menus_utilities"
            ),"Core")) {
                $this->_Extorio()->bindActionToEvent("extorio_admin_menu_left_menus",function() {
                    return AdminMenuLeft_Menu::n("Utilities","Utilities","",FontAwesomeIcons::_wrench);
                },800,false);
            }

            //setup the left items
            $this->_Extorio()->bindActionToEvent("extorio_admin_menu_left_items",function($menuName) {
                $loggedInUser = Extorio::get()->getLoggedInUser();
                switch ($menuName) {
                    case "extorio" :
                        $items = array();
                        $items[] = AdminMenuLeft_Item::n("Dashboard","dashboard","/extorio-admin/","",FontAwesomeIcons::_dashboard);
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_extensions"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Extensions","manage extensions","/extorio-admin/extensions","/extorio-admin/extensions",FontAwesomeIcons::_cube);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_assets"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Assets","manage assets","/extorio-admin/assets","/extorio-admin/assets",FontAwesomeIcons::_filesO);
                        }
                        return $items;
                        break;
                    case "usermanagement" :
                        $items = array();
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_users"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Users","manage users","/extorio-admin/users","/extorio-admin/users",FontAwesomeIcons::_user);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_user_reports"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Reports","manage user reports","/extorio-admin/user-reports","/extorio-admin/user-reports",FontAwesomeIcons::_table);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_user_groups"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Groups","manage user groups","/extorio-admin/user-groups","/extorio-admin/user-groups",FontAwesomeIcons::_users);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_user_contact_topics"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Contact Topics","manage user contact topics","/extorio-admin/user-contact-topics","/extorio-admin/user-contact-topics",FontAwesomeIcons::_share);
                        }
                        return $items;
                        break;
                    case "contentmanagement" :
                        $items = array();
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_pages"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Pages","manage pages","/extorio-admin/pages","/extorio-admin/pages",FontAwesomeIcons::_fileCodeO);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_templates"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Templates","manage templates","/extorio-admin/templates","/extorio-admin/templates",FontAwesomeIcons::_file);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_blocks"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Blocks","manage blocks","/extorio-admin/blocks","/extorio-admin/blocks",FontAwesomeIcons::_thLarge);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_block_categories"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Block Categories","manage block categories","/extorio-admin/block-categories","/extorio-admin/block-categories",FontAwesomeIcons::_list);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_layouts"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Layouts","manage layouts","/extorio-admin/layouts","/extorio-admin/layouts",FontAwesomeIcons::_square);
                        }
                        return $items;
                        break;
                    case "emailmanagement" :
                        $items = array();
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_email_transports"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Transports","Manage email transports","/extorio-admin/email-transports","/extorio-admin/email-transports",FontAwesomeIcons::_envelopeO);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_email_templates"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Templates","Manage email templates","/extorio-admin/email-templates","/extorio-admin/email-templates",FontAwesomeIcons::_file);
                        }
                        return $items;
                        break;
                    case "datamanagement" :
                        $items = array();
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_models"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Models","manage Models","/extorio-admin/models","/extorio-admin/models",FontAwesomeIcons::_database);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_model_webhooks"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Model webhooks","manage model webhooks","/extorio-admin/model-webhooks","/extorio-admin/model-webhooks",FontAwesomeIcons::_link);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_model_viewer"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Model viewer","view model data","/extorio-admin/model-viewer","/extorio-admin/model-viewer",FontAwesomeIcons::_eye);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_enums"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Enums","manage enums","/extorio-admin/enums","/extorio-admin/enums",FontAwesomeIcons::_thList);
                        }
                        return $items;
                        break;
                    case "settings" :
                        $items = array();
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_application_settings"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Application settings","manage application settings","/extorio-admin/application-settings","/extorio-admin/application-settings",FontAwesomeIcons::_cog);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_extorio_settings"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Extorio settings","manage extorio settings","/extorio-admin/extorio-settings","/extorio-admin/extorio-settings",FontAwesomeIcons::_cog);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_user_settings"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("User settings","manage user settings","/extorio-admin/user-settings","/extorio-admin/user-settings",FontAwesomeIcons::_cog);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_email_settings"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Email settings","Manage email settings","/extorio-admin/email-settings","/extorio-admin/email-settings",FontAwesomeIcons::_cog);
                        }
                        return $items;
                        break;
                    case "developers" :
                        $items = array();
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_user_action_types"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("User action types","manage user action types","/extorio-admin/user-action-types","/extorio-admin/user-action-types",FontAwesomeIcons::_users);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_translations"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Translations","manage translations","/extorio-admin/translations","/extorio-admin/translations",FontAwesomeIcons::_globe);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_user_privileges"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Privileges","manage user privileges","/extorio-admin/user-privileges","/extorio-admin/user-privileges",FontAwesomeIcons::_userSecret);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_user_privilege_categories"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Privilege Categories","manage user privilege categories","/extorio-admin/user-privilege-categories","/extorio-admin/user-privilege-categories",FontAwesomeIcons::_userSecret);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_block_processors"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Block processors","manage block processors","/extorio-admin/block-processors","/extorio-admin/block-processors",FontAwesomeIcons::_circleONotch);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_apis"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Apis","manage dropdowns","/extorio-admin/apis","/extorio-admin/apis",FontAwesomeIcons::_sitemap);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_tasks"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Tasks","manage dropdowns","/extorio-admin/tasks","/extorio-admin/tasks",FontAwesomeIcons::_tasks);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_themes"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Themes","manage Themes","/extorio-admin/themes","/extorio-admin/themes",FontAwesomeIcons::_fileImageO);
                        }
                        return $items;
                        break;
                    case "Utilities" :
                        $items = array();
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_log_manager"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Log manager","manage Models","/extorio-admin/log-manager","/extorio-admin/log-manager",FontAwesomeIcons::_fileTextO);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_task_manager"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Task manager","manage dropdowns","/extorio-admin/task-manager","/extorio-admin/task-manager",FontAwesomeIcons::_tasks);
                        }
                        return $items;
                        break;
                }
            },100,false);
        }
    }
}