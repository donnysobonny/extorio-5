<?php
namespace Core\Components\Views;
/**
 * Manage the application settings
 *
 * Class AdminApplicationSettings
 */
class AdminApplicationSettings extends \Core\Components\Controllers\AdminApplicationSettings {
    public function _onDefault() {
        ?>
        <form method="post" action="">
            <div class="form-group">
                <label for="application_name">Site name</label>
                <input value="<?=$this->config["application"]["name"]?>" type="text" class="form-control" id="application_name" name="application_name" placeholder="Enter a site name">
            </div>
            <div class="form-group">
                <label for="application_description">Site description</label>
                <textarea class="form-control" id="application_description" name="application_description" placeholder="Enter a description"><?=$this->config["application"]["description"]?></textarea>
            </div>
            <div class="form-group">
                <label for="application_company">Company name</label>
                <input value="<?=$this->config["application"]["company"]?>" type="text" class="form-control" id="application_company" name="application_company" placeholder="Enter your company name">
            </div>
            <div class="form-group">
                <label for="application_address">Site address</label>
                <input value="<?=$this->config["application"]["address"]?>" type="text" class="form-control" id="application_address" name="application_address" placeholder="Enter a site address">
            </div>
            <div class="checkbox">
                <label>
                    <input id="application_seo" name="application_seo" <?php
                    if($this->config["application"]["seo_visible"]) echo 'checked="checked"';
                    ?> type="checkbox"> Site visible to SEO crawlers
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input id="application_maintenance" name="application_maintenance" <?php
                    if($this->config["application"]["maintenance_mode"]) echo 'checked="checked"';
                    ?> type="checkbox"> Site in maintenance mode
                </label>
            </div>
            <div class="form-group">
                <label for="application_favicon">Favicon</label>
                <div class="input-group">
                    <input value="<?=$this->config["application"]["favicon"]?>" type="text" class="form-control" id="application_favicon" name="application_favicon">
                    <span class="input-group-btn">
                        <button id="favicon_select" class="btn btn-primary" type="button">Select an image...</button>
                    </span>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Localization</h3>
                </div>
                <div class="panel-body">
                    <div class="checkbox">
                        <label>
                            <input id="auto_translate" name="auto_translate" <?php
                            if($this->config["localization"]["auto_translate"]) echo 'checked="checked"';
                            ?> type="checkbox"> Enable auto translating using the microsoft translator api (<a href="https://www.microsoft.com/en-us/translator/getstarted.aspx">requires credentials</a>)
                        </label>
                    </div>
                    <div id="auto_translate_details" style="<?php
                    if(!$this->config["localization"]["auto_translate"]) echo 'display:none;';
                    ?>" class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Microsoft translator api credentials</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div style="margin-bottom: 0;" class="form-group">
                                        <label for="microsoft_creds_client_id">Client ID</label>
                                        <input value="<?=$this->config["localization"]["microsoft_credentials"]["client_id"]?>" type="text" class="form-control" id="microsoft_creds_client_id" name="microsoft_creds_client_id">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div style="margin-bottom: 0;" class="form-group">
                                        <label for="microsoft_creds_client_secret">Client Secret</label>
                                        <input value="<?=$this->config["localization"]["microsoft_credentials"]["client_secret"]?>" type="text" class="form-control" id="microsoft_creds_client_secret" name="microsoft_creds_client_secret">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="default_language">Default language</label>
                        <select class="form-control" id="default_language" name="default_language">
                            <?php
                            foreach($this->languages as $lang) {
                                ?><option <?php
                                if($this->config["localization"]["default_language"] == $lang->id) echo 'selected="selected"';
                                ?> value="<?=$lang->id?>"><?=$lang->name?></option><?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Enabled languages</label>
                        <div class="row">
                            <?php
                            foreach($this->languages as $lang) {
                                ?>
                                <div class="col-xs-2">
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($lang->enabled) echo 'checked="checked"';
                                            ?> name="enabled_languages[]" value="<?=$lang->id?>" type="checkbox"> <?=$lang->name?>
                                        </label>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" name="settings_updated" class="btn btn-primary">Update</button>
        </form>
        <script>
            $(function() {
                $('#auto_translate').on("change", function() {
                    if($(this).prop("checked")) {
                        $('#auto_translate_details').show();
                    } else {
                        $('#auto_translate_details').hide();
                    }
                });

                $('#favicon_select').on("click", function() {
                    $.extorio_dialog_assetManager({
                        title: "Select an image",
                        allowextensions: false,
                        displaytypes: "images",
                        selectable: true,
                        onfileselected: function(url) {
                            $('#application_favicon').val(url);
                        }
                    });
                });
            });
        </script>
        <?php
    }
}