<?php
namespace Core\Components\Views;
/**
 * Manage email transports
 *
 * Class AdminEmailTransports
 */
class AdminEmailTransports extends \Core\Components\Controllers\AdminEmailTransports {
    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Subject</th>
            <th>To</th>
            <th>From</th>
            <th>Cc</th>
            <th>Bcc</th>
            <th>No. sent</th>
            <th>Date sent</th>
            <th><span class="fa fa-cog"></span></th>
        </tr>
    </thead>
    <tbody>
        <?php
        if(count($this->transports) > 0) {
            foreach($this->transports as $t) {
                ?>
                <tr>
                    <td><?=$t->subject?></td>
                    <td><span class="label label-primary"><?=$t->toName?> &lt;<?=$t->toEmail?>&gt;</span></td>
                    <td><span class="label label-primary"><?=$t->fromName?> &lt;<?=$t->fromEmail?>&gt;</span></td>
                    <td><?php
                        foreach($t->cc as $name => $email) {
                            ?>
                            <span class="label label-primary"><?=$name?> &lt;<?=$email?>&gt;</span>
                            <?php
                        }
                        ?></td>
                    <td><?php
                        foreach($t->bcc as $name => $email) {
                            ?>
                            <span class="label label-primary"><?=$name?> &lt;<?=$email?>&gt;</span>
                            <?php
                        }
                        ?></td>
                    <td><?=$t->numSent?></td>
                    <td><?php
                        if($t->numSent) {
                            echo date("jS F Y",strtotime($t->dateSent));
                        } else {
                            echo "N/A";
                        }
                        ?></td>
                    <td>
                        <a class="btn btn-xs btn-default" href="<?=$this->_getUrlToMethod("view",array($t->id))?>"><span class="fa fa-eye"></span> view</a>
                        <a class="btn btn-xs btn-primary" href="<?=$this->_getUrlToMethod("resend",array($t->id))?>"><span class="fa fa-mail-forward"></span> re-send</a>
                        <a class="btn btn-xs btn-danger" href="<?=$this->_getUrlToMethod("delete",array($t->id))?>"><span class="fa fa-trash"></span> delete</a>
                    </td>
                </tr>
                <?php
            }
        } else {
            ?>
        <tr>
            <td colspan="8">No transports found</td>
        </tr>
            <?php
        }

        ?>
    </tbody>
</table>
        <?php
        $this->f->displayPagination();
    }

    public function view($id = false) {
        if($this->transport->numSent) {
            ?>
            <div class="alert alert-info">
                This email has been sent <?=$this->transport->numSent?> times and was last sent on the <?=date('jS F Y, H:i:s')?>.
            </div>
            <?php
        } else {
            ?>
            <div class="alert alert-warning">
                This email has never been sent. <?php
                if(strlen($this->transport->errorMessage)) {
                    echo "It has the following error message: ".$this->transport->errorMessage;
                } else {
                    echo "It has no error messages.";
                }
                ?>
            </div>
            <?php
        }
        ?>
        <div class="page-header">
            <h4><?=$this->transport->subject?></h4>
        </div>
        <?php
        echo $this->transport->fetchBodyMerged();

    }

    public function resend($id = false) {

    }

    public function delete($id = false) {

    }
}