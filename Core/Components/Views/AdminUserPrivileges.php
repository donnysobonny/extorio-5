<?php
namespace Core\Components\Views;
/**
 * Manage user privileges
 *
 * Class AdminUserPrivileges
 */
class AdminUserPrivileges extends \Core\Components\Controllers\AdminUserPrivileges {



    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Extension</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($this->privileges) > 0) {
                    foreach($this->privileges as $priv) {
                        ?>
                <tr>
                    <td>
                        <span style="cursor: help; border-bottom: 1px dashed;" data-toggle="tooltip" data-placement="right" title="<?=htmlentities($priv->description)?>"><?=$priv->name?></span>
                    </td>
                    <td>
                        <span class="label label-primary"><?=$priv->category->name?></span>
                    </td>
                    <td>
                        <?=$priv->extensionName?>
                    </td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="<?=$this->_getUrlToMethod("edit",array($priv->id))?>"><span class="fa fa-edit"></span> edit</a>
                        <button data-id="<?=$priv->id?>" class="btn btn-xs btn-danger privilege_delete"><span class="fa fa-trash"></span> delete</button>
                    </td>
                </tr>
                        <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="4">No privileges found</td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();

                $('.privilege_delete').on("click", function() {
                    var self = $(this);
                    $.extorio_modal({
                        title: "Delete privilege?",
                        size: "modal-sm",
                        content: "Are you sure you want to delete this user privilege?",
                        closetext: "cancel",
                        continuetext: "delete",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + self.attr("data-id");
                        }
                    });
                });
            })
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($id = false) {
        ?>
        <form method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->privilege->name?>" type="text" class="form-control" name="name" id="name" placeholder="Enter a name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" name="description" id="description" placeholder="(optional) Enter a description"><?=$this->privilege->description?></textarea>
            </div>
            <div class="form-group">
                <label for="category">Category</label>
                <select class="form-control" name="category" id="category">
                    <?php
                    foreach($this->categories as $c) {
                        ?><option <?php
                        if($this->privilege->category == $c->id) echo 'selected="selected"';
                        ?> value="<?=$c->id?>"><?=$c->name?></option><?php
                    }
                    ?>
                </select>
            </div>

            <?php
            if(!$this->privilege) {
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Advanced</div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="extension">Extension</label>
                            <select class="form-control" id="extension" name="extension">
                                <optgroup label="Internal">
                                    <option value="Application">Application</option>
                                    <option value="Core">Core</option>
                                </optgroup>
                                <?php
                                if(count($this->extensions)) {
                                    ?>
                                    <optgroup label="Installed">
                                        <?php
                                        foreach($this->extensions as $e) {
                                            if(!$e->internal) {
                                                ?>
                                                <option value="<?=$e->extensionName?>"><?=$e->extensionName?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
            <button type="submit" name="submit" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
            <button type="submit" name="submit_exit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
        </form>
        <?php
    }
}