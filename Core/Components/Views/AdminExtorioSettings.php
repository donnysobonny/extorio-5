<?php
namespace Core\Components\Views;
/**
 * Manage the settings of extorio
 *
 * Class AdminExtorioSettings
 */
class AdminExtorioSettings extends \Core\Components\Controllers\AdminExtorioSettings {
    public function _onDefault() {
        ?>
        <form method="post" action="">
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Tasks</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="task_max">Maximum number of tasks</label>
                                <input value="<?=$this->config["tasks"]["max_running_tasks"]?>" type="number" class="form-control" id="task_max" name="task_max" placeholder="Max running tasks">
                            </div>
                            <div class="form-group">
                                <label for="task_max">Clean up tasks after how many days?</label>
                                <input value="<?=$this->config["tasks"]["clean_up_tasks_after_days"]?>" type="number" class="form-control" id="task_days" name="task_days" placeholder="Clean up tasks after days">
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Themes</h3>
                        </div>
                        <div class="panel-body">
                            <div class="checkbox">
                                <label>
                                    <input name="themes_smart_compile" <?php
                                    if($this->config["themes"]["smart_compile"]) echo 'checked="checked"';
                                    ?> type="checkbox"> Smartly compile themes when associated less files are modified
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="themes_smart_minify" <?php
                                    if($this->config["themes"]["smart_minify"]) echo 'checked="checked"';
                                    ?> type="checkbox"> Smartly minify themes when associated css/js files are modified
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Models</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="models_cache">Maximum number of entries to store in the modification cache</label>
                                <input value="<?=$this->config["models"]["max_modcache_entries_per_instance"]?>" type="number" class="form-control" id="models_cache" name="models_cache" placeholder="Max modcache entries">
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Webhooks</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="checkbox">
                                        <label>
                                            <input id="models_webhook_autofire" name="models_webhook_autofire" <?php
                                            if($this->config["models"]["webhooks"]["autofire"]) echo 'checked="checked"';
                                            ?> type="checkbox"> Modifications to model instances automatically fire webhooks
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="models_default_verification_key">Default webhook verification key</label>
                                        <input value="<?=$this->config["models"]["webhooks"]["default_verification_key"]?>" type="text" class="form-control" id="models_default_verification_key" name="models_default_verification_key" placeholder="Default verification key">
                                    </div>
                                    <div class="form-group">
                                        <label for="models_webhook_max_retries">Number of times to retry sending a webhook that fails</label>
                                        <input value="<?=$this->config["models"]["webhooks"]["max_retries"]?>" type="number" class="form-control" id="models_webhook_max_retries" name="models_webhook_max_retries" placeholder="Max retries">
                                    </div>
                                    <div class="form-group">
                                        <label for="models_webhook_retry_delay">Delay between retries</label>
                                        <input value="<?=$this->config["models"]["webhooks"]["retry_delay"]?>" type="number" class="form-control" id="models_webhook_retry_delay" name="models_webhook_retry_delay" placeholder="Retry delay">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" name="settings_updated" class="btn btn-primary">Update</button>
        </form>
        <?php
    }
}