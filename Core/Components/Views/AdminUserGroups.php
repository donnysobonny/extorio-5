<?php
namespace Core\Components\Views;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\UserPrivilege;

/**
 * Manage user groups
 *
 * Class AdminUserGroups
 */
class AdminUserGroups extends \Core\Components\Controllers\AdminUserGroups {
    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-striped table-condensed">
            <thead>
                <tr>
                    <th>Name</th>
                    <th style="max-width: 200px;">Privileges</th>
                    <th style="max-width: 150px;">Manages</th>
                    <th>Extension</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($this->groups) > 0) {
                    foreach($this->groups as $group) {
                        ?>
                <tr>
                    <td>
                        <span style="cursor: help; border-bottom: 1px dashed;" data-toggle="tooltip" data-placement="right" title="<?=htmlentities($group->description)?>"><?=$group->name?></span>
                    </td>
                    <td style="max-width: 200px;">
                        <button id="view_privileges_<?=$group->id?>" class="btn btn-xs btn-default"><span class="fa fa-eye"></span> view privileges</button>
                        <script>
                            $(function() {
                                var data = <?=json_encode($group->privileges)?>;
                                $('#view_privileges_<?=$group->id?>').on("click", function() {
                                    $.extorio_dialog({
                                        title: "View privileges",
                                        content:'<div class="list-group priv_cont"></div>',
                                        onopen: function() {
                                            var cont = this.find('.priv_cont');
                                            if(data.length > 0) {
                                                for(var i = 0; i < data.length; i++) {
                                                    var priv = data[i];
                                                    cont.append('<div class="list-group">' +
                                                        '   <h4 class="list-group-item-heading">'+priv.name+'</h4>' +
                                                        '   <p class="list-group-item-text">'+priv.description+'</p>' +
                                                        '</div>');
                                                }
                                            } else {
                                                cont.html('<div class="list-group">This user has no privileges</div>')
                                            }
                                        }
                                    });
                                });
                            });
                        </script>
                    </td>
                    <td style="max-width: 150px;">
                        <?php
                        if($group->manageAll) {
                            echo "ALL GROUPS";
                        } else {
                            if(count($group->manageGroups)) {
                                foreach($group->manageGroups as $mgroup) {
                                    ?><span class="label label-info"><?=$mgroup->name?></span>
                                    <?php
                                }
                            } else {
                                echo "NONE";
                            }
                        }
                        ?>
                    </td>
                    <td><?=$group->extensionName?></td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="<?=$this->_getUrlToMethod("edit",array($group->id))?>"><span class="fa fa-edit"></span> edit</a>
                        <button data-id="<?=$group->id?>" class="btn btn-xs btn-danger delete_group"><span class="fa fa-trash"></span> delete</button>
                    </td>
                </tr>
                        <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="5">No groups found</td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();

                $('.delete_group').on("click", function() {
                    var self = $(this);
                    $.extorio_modal({
                        title: "Delete user group?",
                        size: "modal-sm",
                        content: "Are you sure you want to delete this user group?",
                        closetext: "cancel",
                        continuetext: "delete",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + self.attr("data-id");
                        }
                    });
                });
            })
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($id = false) {
        ?>
        <form method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->group->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter the name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description"><?=$this->group->description?></textarea>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Management</h3>
                </div>
                <div class="panel-body">
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->group->manageAll) echo 'checked="checked"';
                            ?> name="manage_all" id="manage_all" type="checkbox"> This user group manages all other user groups
                        </label>
                    </div>
                    <div style="<?php
                    if($this->group->manageAll) echo 'display: none;';
                    ?>" id="manage_user_group_select" class="form-group">
                        <label>Select the user groups that this group manages</label>
                        <?php
                        foreach($this->groups as $group) {
                            ?>
                            <div class="checkbox">
                                <label>
                                    <input <?php
                                    if(in_array($group->id,$this->group->manageGroups)) echo 'checked="checked"';
                                    ?> name="manage_groups[]" value="<?=$group->id?>" type="checkbox"> <?=$group->name?> <small class="text-muted"><?=$group->description?></small>
                                </label>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Privileges</h3>
                </div>
                <div class="panel-body">
                    <?php
                    $columns = 3;
                    $nextRow = 0;
                    $count = count($this->categories);
                    for($i = 0; $i <= $count; $i++) {
                        if($i < $count) {
                            if($i == $nextRow) {
                                if($i > 0) {
                                    ?></div><?php
                                }
                                ?><div class="row"><?php
                                $nextRow += $columns;
                            }

                            $category = $this->categories[$i];
                            ?>
                            <div class="col-sm-<?=(12/$columns)?>">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><span class="fa fa-<?=$category->icon?>"></span> <?=$category->name?></h3>
                                    </div>
                                    <div id="privilege_select_<?=$category->id?>" class="panel-body">
                                        <a class="privilege_select_all" data-target="privilege_select_<?=$category->id?>" href="javascript:;"><span class="fa fa-check"></span> select all</a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="privilege_unselect_all" data-target="privilege_select_<?=$category->id?>" href="javascript:;"><span class="fa fa-remove"></span> un-select all</a>
                                        <?php
                                        $privs = UserPrivilege::findAll(
                                            Query::n()
                                                ->where(array(
                                                    "category.id" => $category->id
                                                ))
                                                ->order("name"),1
                                        );
                                        foreach($privs as $priv) {
                                            ?>
                                            <div class="checkbox">
                                                <label>
                                                    <input <?php
                                                    if(in_array($priv->id,$this->group->privileges)) echo 'checked="checked"';
                                                    ?> name="privileges[]" value="<?=$priv->id?>" type="checkbox"> <?=$priv->name?>
                                                </label>
                                                <p class="help-block"><?=$priv->description?></p>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php

                        } else {
                            ?></div><?php
                        }
                    }
                    ?>
                </div>
            </div>

            <?php
            if(!$this->group) {
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Advanced</div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="extension">Extension</label>
                            <select class="form-control" id="extension" name="extension">
                                <optgroup label="Internal">
                                    <option value="Application">Application</option>
                                    <option value="Core">Core</option>
                                </optgroup>
                                <?php
                                if(count($this->extensions)) {
                                    ?>
                                    <optgroup label="Installed">
                                        <?php
                                        foreach($this->extensions as $e) {
                                            if(!$e->internal) {
                                                ?>
                                                <option value="<?=$e->extensionName?>"><?=$e->extensionName?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <button type="submit" name="submit" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
            <button type="submit" name="submit_exit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
        </form>
        <script>
            $(function() {
                $('#manage_all').on("change", function() {
                    if($(this).prop("checked")) {
                        $('#manage_user_group_select').hide();
                    } else {
                        $('#manage_user_group_select').show();
                    }
                });

                $('.privilege_select_all').on("click", function() {
                    var target = $(this).attr('data-target');
                    $('#' + target + ' input').each(function() {
                        $(this).prop("checked",true);
                    });
                });

                $('.privilege_unselect_all').on("click", function() {
                    var target = $(this).attr('data-target');
                    $('#' + target + ' input').each(function() {
                        $(this).prop("checked",false);
                    });
                });
            });
        </script>
        <?php
    }
}