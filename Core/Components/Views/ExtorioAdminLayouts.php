<?php
namespace Core\Components\Views;
use Core\Classes\Helpers\LayoutSystem;

class ExtorioAdminLayouts extends \Core\Components\Controllers\ExtorioAdminLayouts {

    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <td>Name</td>
                    <td>Description</td>
                    <td>Extension</td>
                    <td><span class="fa fa-cog"></span></td>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($this->layouts)) {
                    foreach($this->layouts as $layout) {
                        ?>
                        <tr>
                            <td><?=$layout->name?></td>
                            <td><?=$layout->description?></td>
                            <td><?=$layout->extensionName?></td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="/extorio-admin/layouts/edit/<?=$layout->id?>"><span class="fa fa-edit"></span> edit</a>
                                <button data-id="<?=$layout->id?>" class="btn btn-xs btn-danger delete_layout"><span class="fa fa-trash"></span> delete</button>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="4">No layouts found</td>
                </tr>
                    <?php
                }

                ?>
            </tbody>
        </table>
        <script>
            $(function() {
                $('.delete_layout').on("click", function() {
                    var self = $(this);
                    $.extorio_modal({
                        title: "Delete layout?",
                        size: "modal-sm",
                        content: "Are you sure you want to delete this layout?",
                        closetext: "cancel",
                        continuetext: "delete",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + self.attr("data-id");
                        }
                    });
                });
            });
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($layoutId = false) {
        ?>
        <div>
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->layout->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter a name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description"><?=$this->layout->description?></textarea>
            </div>

            <?php
            if(!$this->layout) {
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Advanced</div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="extension">Extension</label>
                            <select class="form-control" id="extension" name="extension">
                                <optgroup label="Internal">
                                    <option value="Application">Application</option>
                                    <option value="Core">Core</option>
                                </optgroup>
                                <?php
                                if(count($this->extensions)) {
                                    ?>
                                    <optgroup label="Installed">
                                        <?php
                                        foreach($this->extensions as $e) {
                                            if(!$e->_internal) {
                                                ?>
                                                <option value="<?=$e->_extensionName?>"><?=$e->_extensionName?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <hr />
            <div class="gutter_padding_left gutter_padding_right">
                <div id="layout_editor_wrapper">
                    <?php
                    if($this->layout) {
                        echo LayoutSystem::display(LayoutSystem::constructFromArray($this->layout->layout),"","",true);
                    } else {
                        echo LayoutSystem::display(LayoutSystem::getDefault(),"","",true);
                    }
                    ?>
                </div>
            </div>
            <br />
            <button type="submit" class="btn btn-primary layout_save"><span class="fa fa-save"></span> Save</button>
            <button type="submit" class="btn btn-info layout_save_exit"><span class="fa fa-sign-out"></span> Save and exit</button>
        </div>
        <script>
            $(function() {
                <?php
                if($this->layout) {
                    ?>
                    $('.layout_save, .layout_save_exit').on("click", function() {
                        var saveAndExit = $(this).hasClass("layout_save_exit");
                        $.extorio_showFullPageLoader();
                        var layout = {};
                        layout.id = <?=$this->layout->id?>;
                        layout.name = $('#name').val();
                        layout.description = $('#description').val();
                        layout.layout = $('#layout_editor_wrapper').layoutsystem__toObject();
                        //save
                        $.ajax({
                            url: "/extorio/apis/data/Layout/" + layout.id,
                            type: "PUT",
                            dataType: "json",
                            data: {
                                data: layout
                            },
                            error: function (a, b, c) {
                                $.extorio_messageError(c);
                                $.extorio_hideFullPageLoader();
                            },
                            success: function (data) {
                                if (data.error) {
                                    $.extorio_messageError(data.error_message);
                                    $.extorio_hideFullPageLoader();
                                } else {
                                    $.extorio_messageSuccess("Layout successfully saved");
                                    if(saveAndExit) {
                                        window.location.href = "/extorio-admin/layouts";
                                    } else {
                                        window.location.href = "/extorio-admin/layouts/edit/" + data.data.id;
                                    }
                                }
                            }
                        });
                    });
                    <?php
                } else {
                    ?>
                    $('.layout_save, .layout_save_exit').on("click", function() {
                        var saveAndExit = $(this).hasClass("layout_save_exit");
                        $.extorio_showFullPageLoader();
                        var layout = {};
                        layout.name = $('#name').val();
                        layout.description = $('#description').val();
                        layout.layout = $('#layout_editor_wrapper').layoutsystem__toObject();
                        layout.extensionName = $('#extension').val();
                        //save
                        $.ajax({
                            url: "/extorio/apis/data/Layout",
                            type: "POST",
                            dataType: "json",
                            data: {
                                data: layout
                            },
                            error: function(a,b,c) {
                                $.extorio_messageError(c);
                                $.extorio_hideFullPageLoader();
                            },
                            success: function(data) {
                                if(data.error) {
                                    $.extorio_messageError(data.error_message);
                                    $.extorio_hideFullPageLoader();
                                } else {
                                    $.extorio_messageSuccess("Layout successfully saved");
                                    if(saveAndExit) {
                                        window.location.href = "/extorio-admin/layouts";
                                    } else {
                                        window.location.href = "/extorio-admin/layouts/edit/" + data.data.id;
                                    }
                                }
                            }
                        });
                    });
                    <?php
                }
                ?>
            });
        </script>
    <?php
    }

    public function delete($layoutId = false) {
        if(!$layoutId) {
            $this->_redirectToDefault();
        }
    }
}