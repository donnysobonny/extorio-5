<?php
namespace Core\Components\Views;

use Core\Classes\Enums\InternalEvents;
use Core\Classes\Helpers\UserTab;

/**
 * 
 *
 * Class MyAccount
 */
class MyAccount extends \Core\Components\Controllers\MyAccount {
    public function _onDefault() {
        $this->_includeIncluder("tabcollapse");
        /** @var UserTab[] $tabs */
        $tabs = array();
        $actions = $this->_Extorio()->getEvent(InternalEvents::_my_account_view_tabs)->run();
        foreach($actions as $action) {
            if(is_array($action)) {
                foreach($action as $a) {
                    $tabs[] = $a;
                }
            } else {
                $tabs[] = $action;
            }
        }
        ?>
        <ul id="my_account_tabs" class="nav nav-tabs">
            <?php
            for($i = 0; $i < count($tabs); $i++) {
                ?><li class="<?php
                if($i == 0) echo 'active';
                ?>"><a href="#<?=$tabs[$i]->name?>" data-toggle="tab"><?php
                    if(strlen($tabs[$i]->icon)) {
                        ?><span class="fa fa-<?=$tabs[$i]->icon?>"></span> <?php
                    }
                    ?><?=$tabs[$i]->label?></a></li><?php
            }
            ?>
        </ul>
        <div id="my_account_content" class="tab-content">
            <?php
            for($i = 0; $i < count($tabs); $i++) {
                ?>
                <div class="tab-pane fade<?php
                if($i == 0) echo ' in active';
                ?>" id="<?=$tabs[$i]->name?>">
                    <?php
                    $actions = $this->_Extorio()->getEvent(InternalEvents::_my_account_view_content)->run(array($tabs[$i]->name));
                    foreach($actions as $action) {
                        echo $action;
                    }
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
        <script>
            $(function() {
                $('#my_account_tabs').tabCollapse();
            });
        </script>
        <?php
    }
}