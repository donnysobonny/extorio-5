<?php
namespace Core\Components\Views;
use Core\Classes\Enums\FontAwesomeIcons;

/**
 * Manage user contact topics
 *
 * Class AdminUserContactTopics
 */
class AdminUserContactTopics extends \Core\Components\Controllers\AdminUserContactTopics {
    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Extension</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($this->topics) > 0) {
                    foreach($this->topics as $topic) {
                        ?>
                <tr>
                    <td><?=$topic->name?></td>
                    <td><?=$topic->description?></td>
                    <td><?=$topic->extensionName?></td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="<?=$this->_getUrlToMethod("edit",array($topic->id))?>"><span class="fa fa-edit"></span> edit</a>
                        <button data-id="<?=$topic->id?>" class="btn btn-xs btn-danger topic_delete"><span class="fa fa-trash"></span> delete</button>
                    </td>
                </tr>
                        <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="4">No topics found</td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $(function() {
                $('.topic_delete').on("click", function() {
                    var id = $(this).attr('data-id');
                    $.extorio_modal({
                        title: "Delete topic",
                        content: "Are you sure that you want to delete this topic?",
                        size: "modal-sm",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + id;
                        }
                    });
                });
            });
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($id = false) {
        ?>
        <form method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->topic->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter a name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description" placeholder="Enter a description"><?=$this->topic->description?></textarea>
            </div>

            <?php
            if(!$this->topic) {
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Advanced</div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="extension">Extension</label>
                            <select class="form-control" id="extension" name="extension">
                                <optgroup label="Internal">
                                    <option value="Application">Application</option>
                                    <option value="Core">Core</option>
                                </optgroup>
                                <?php
                                if(count($this->extensions)) {
                                    ?>
                                    <optgroup label="Installed">
                                        <?php
                                        foreach($this->extensions as $e) {
                                            if(!$e->internal) {
                                                ?>
                                                <option value="<?=$e->extensionName?>"><?=$e->extensionName?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
            <button type="submit" name="submitted" class="btn btn-primary"><span class="fa fa-<?=FontAwesomeIcons::_save?>"></span> Save</button>
            <button type="submit" name="submitted_exit" class="btn btn-info"><span class="fa fa-<?=FontAwesomeIcons::_signOut?>"></span> Save and exit</button>
        </form>
        <?php
    }
}