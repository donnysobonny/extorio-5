<?php
namespace Core\Components\Views;
/**
 * Manage translations and translation bases
 *
 * Class AdminTranslations
 */
class AdminTranslations extends \Core\Components\Controllers\AdminTranslations {
    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        $db = $this->_getDbInstanceDefault();
        ?>
        <table class="table table-striped table-condensed">
            <thead>
                <tr>
                    <th>Msgid</th>
                    <th>Msgid Plural</th>
                    <th>Plural</th>
                    <th>Extension</th>
                    <th>Translations</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($this->translationBases)) {
                    foreach($this->translationBases as $base) {
                        ?>
                <tr>
                    <td><?=$base->msgid?></td>
                    <td><?=$base->msgid_plural?></td>
                    <td><?php
                        if($base->plural) {
                            ?><span class="fa fa-check"></span><?php
                        } else {
                            ?><span class="fa fa-close"></span><?php
                        }
                        ?></td>
                    <td><?=$base->extensionName?></td>
                    <td>
                        <?php
                        $sql = 'SELECT count(id) FROM core_classes_models_translation WHERE "baseId" = $1';
                        $row = $db->query($sql,array($base->id))->fetchRow();
                        $count = 0;
                        if($row) {
                            $count = intval($row[0]);
                        }
                        if($count == 1) {
                            echo '1 translation';
                        } else {
                            echo $count." translations";
                        }
                        ?>
                    </td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="<?=$this->_getUrlToMethod("edit",array($base->id))?>"><span class="fa fa-edit"></span> edit</a>
                        <button data-id="<?=$base->id?>" class="btn btn-xs btn-danger translation_delete"><span class="fa fa-trash"></span> delete</button>
                    </td>
                </tr>
                        <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="6">No translation bases found</td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $(function() {
                $('.translation_delete').on("click",function() {
                    var id = $(this).attr('data-id');
                    $.extorio_modal({
                        title: "Delete translation base",
                        content: "Deleting this translation base will also delete any associated translations. Are you sure you want to delete this?",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + id;
                        }
                    });
                });
            });
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($id = false) {
        ?>
        <form method="post" action="">
            <div class="checkbox">
                <label>
                    <input <?php
                    if($this->translationBase) echo 'disabled="disabled"';
                    ?> id="plural" name="plural" <?php
                    if($this->translationBase->plural) echo 'checked="checked"';
                    ?> type="checkbox"> Used for plural translations
                </label>
            </div>
            <div class="form-group">
                <label for="msgid">Msgid</label>
                <textarea name="msgid" id="msgid" class="form-control" placeholder="The source text (or singular text for plural translations)"><?=$this->translationBase->msgid?></textarea>
            </div>
            <div id="msgid_plural_display" style="<?php
            if(!$this->translationBase->plural) echo 'display: none;';
            ?>" class="form-group">
                <label for="msgid_plural">Msgid (plural)</label>
                <textarea name="msgid_plural" id="msgid_plural" class="form-control" placeholder="The plural source text (for plural translations)"><?=$this->translationBase->msgid_plural?></textarea>
            </div>

            <?php
            if(!$this->translationBase) {
                ?>
                <div class="form-group">
                    <label for="extension">Extension</label>
                    <select class="form-control" id="extension" name="extension">
                        <optgroup label="Internal">
                            <option value="Application">Application</option>
                            <option value="Core">Core</option>
                        </optgroup>
                        <?php
                        if(count($this->extensions)) {
                            ?>
                            <optgroup label="Installed">
                                <?php
                                foreach($this->extensions as $e) {
                                    if(!$e->internal) {
                                        ?>
                                        <option value="<?=$e->extensionName?>"><?=$e->extensionName?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </optgroup>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <?php
            }
            ?>
            <button type="submit" name="submit" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
            <button type="submit" name="submit_exit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
        </form>
        <?php
        if($this->translationBase) {
            ?>
            <br />
            <div style="margin-bottom: 0;" class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Manage translations</h3>
                </div>
                <div style="padding-bottom: 0;" class="panel-body">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="new_language">Language</label>
                            <select class="form-control" id="new_language" name="new_language">
                                <?php
                                foreach($this->languages as $lang) {
                                    ?><option value="<?=$lang->id?>"><?=$lang->name?></option><?php
                                }
                                ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary new_translation"><span class="fa fa-plus"></span> Create new translation</button>
                    </div>
                    <br />
                    <div id="translations_container">

                    </div>
                </div>
            </div>
            <?php
        }
        ?>
        <script>
            $(function() {
                $('#plural').on("change", function() {
                    console.log("test");
                    if($(this).prop("checked")) {
                        $('#msgid_plural_display').show();
                    } else {
                        $('#msgid_plural_display').hide();
                    }
                });

                <?php
                if($this->translationBase) {
                ?>
                $('.new_translation').on("click", function() {
                    var languageId = $('#new_language').val();
                    var baseId = <?=$this->translationBase->id?>;
                    $.extorio_showFullPageLoader();
                    $.extorio_api({
                        endpoint: "/translations/",
                        type: "POST",
                        data: {
                            data: {
                                msgstr: "",
                                msgstr_plural: "",
                                languageId: languageId,
                                baseId: baseId
                            }
                        },
                        oncomplete: function() {
                            $.extorio_hideFullPageLoader();
                        },
                        onsuccess: function(r) {
                            $.extorio_messageSuccess("Translation created");
                            loadTranslations();
                        }
                    });
                });

                var plural = <?php
                    if($this->translationBase->plural) echo 'true'; else echo 'false';
                    ?>;
                var languages = <?=json_encode($this->languages)?>;

                var loadTranslations = function() {
                    var cont = $('#translations_container');
                    cont.extorio_showLoader();
                    var data = {};
                    data.baseId = <?=$this->translationBase->id?>;
                    $.extorio_api({
                        endpoint: "/translations/filter",
                        type: "GET",
                        data: data,
                        oncomplete: function() {
                            cont.extorio_hideLoader();
                        },
                        onsuccess: function(resp) {
                            cont.html('');
                            if(resp.data.length > 0) {
                                for(var i = 0; i < resp.data.length; i++) {
                                    var data = resp.data[i];
                                    if(plural) {
                                        var item = $('<div class="well well-sm">' +
                                            '       <h3 style="margin-top: 0;">'+languages[data.languageId].name+'</h3>' +
                                            '       <div class="row">' +
                                            '           <div class="col-xs-6">' +
                                            '               <div class="form-group">' +
                                            '                   <label>Msgstr</label>' +
                                            '                   <textarea id="translation_msgstr_'+data.id+'" class="form-control input-sm" placeholder="The translated text (or singular text for plural translations)">'+data.msgstr+'</textarea>' +
                                            '               </div>' +
                                            '           </div>' +
                                            '           <div class="col-xs-6">' +
                                            '               <div class="form-group">' +
                                            '                   <label>Msgstr (plural)</label>' +
                                            '                   <textarea id="translation_msgstr_plural_'+data.id+'" class="form-control input-sm" placeholder="The plural source text (for plural translations)">'+data.msgstr_plural+'</textarea>' +
                                            '               </div>' +
                                            '           </div>' +
                                            '       </div>' +
                                            '       <div class="row">' +
                                            '           <div class="col-xs-10">' +
                                            '               <button data-id="'+data.id+'" type="submit" class="btn btn-block btn-primary translation_save"><span class="fa fa-save"></span> save</button>' +
                                            '           </div>' +
                                            '           <div class="col-xs-2">' +
                                            '               <button data-id="'+data.id+'" type="submit" class="btn btn-block btn-danger translation_delete"><span class="fa fa-trash"></span> delete</button>' +
                                            '           </div>' +
                                            '       </div>' +
                                            '       ' +
                                            '</div>');
                                    } else {
                                        var item = $('<div class="well well-sm">' +
                                            '       <h3 style="margin-top: 0;">'+languages[data.languageId].name+'</h3>' +
                                            '       <div class="form-group">' +
                                            '           <label>Msgstr</label>' +
                                            '           <textarea id="translation_msgstr_'+data.id+'" class="form-control input-sm" placeholder="The translated text (or singular text for plural translations)">'+data.msgstr+'</textarea>' +
                                            '       </div>' +
                                            '       <div class="row">' +
                                            '           <div class="col-xs-10">' +
                                            '               <button data-id="'+data.id+'" type="submit" class="btn btn-block btn-primary translation_save"><span class="fa fa-save"></span> save</button>' +
                                            '           </div>' +
                                            '           <div class="col-xs-2">' +
                                            '               <button data-id="'+data.id+'" type="submit" class="btn btn-block btn-danger translation_delete"><span class="fa fa-trash"></span> delete</button>' +
                                            '           </div>' +
                                            '       </div>' +
                                            '       ' +
                                            '</div>');
                                    }

                                    cont.append(item);
                                }
                                cont.find('.translation_save').on("click", function() {
                                    var id = $(this).attr('data-id');
                                    var msgstr = $('#translation_msgstr_'+id).val();
                                    var msgstr_plural = $('#translation_msgstr_plural_'+id).val();
                                    cont.extorio_showLoader();
                                    $.extorio_api({
                                        endpoint: "/translations/" + id,
                                        type: "POST",
                                        data: {
                                            data: {
                                                msgstr: msgstr,
                                                msgstr_plural: msgstr_plural
                                            }
                                        },
                                        onsuccess: function(r) {
                                            $.extorio_messageSuccess("Translation saved");
                                            loadTranslations();
                                        }
                                    });
                                });
                                cont.find('.translation_delete').on("click", function() {
                                    var id = $(this).attr('data-id');
                                    $.extorio_modal({
                                        title: "Delete translation",
                                        content: "Are you sure that you want to delete this translation?",
                                        oncontinuebutton: function() {
                                            cont.extorio_showLoader();
                                            $.extorio_api({
                                                endpoint: "/translations/" + id + "/delete",
                                                type: "POST",
                                                onsuccess: function(r) {
                                                    $.extorio_messageSuccess("Translation deleted");
                                                    loadTranslations();
                                                }
                                            });
                                        }
                                    });

                                });
                            } else {
                                cont.html('<p>No translations found</p>');
                            }
                        }
                    });
                }
                loadTranslations();
                <?php
                }
                ?>
            });
        </script>
        <?php
    }

    public function delete($id = false) {

    }
}