<?php
namespace Core\Components\Views;
class ExtorioAdminLogManager extends \Core\Components\Controllers\ExtorioAdminLogManager {
    public function _onBegin() {
        ?>
        <a class="btn btn-default" href="/extorio-admin/log-manager"><span class="fa fa-refresh"></span> refresh</a>
        <a class="btn btn-danger" href="/extorio-admin/log-manager/clear"><span class="fa fa-trash"></span> clear all logs</a>
        <br /><br />

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>File</th>
                    <th>Path</th>
                    <th>Size</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach($this->files as $file) {
                    if($file->fileName() != "index") {
                        ?>
                <tr>
                    <td><?=$file->baseName()?></td>
                    <td><?=$file->dirName()."/".$file->baseName()?></td>
                    <td><?=$file->size()?></td>
                    <td>
                        <a class="btn btn-primary btn-xs" href="/extorio-admin/log-manager/view/<?=urldecode($file->baseName())?>"><span class="fa fa-eye"></span> view</a>
                        <a class="btn btn-danger btn-xs" href="/extorio-admin/log-manager/delete/<?=urldecode($file->baseName())?>"><span class="fa fa-trash"></span> delete</a>
                    </td>
                </tr>
                    <?php
                    }

                }
                ?>
            </tbody>
        </table>
        <?php
    }

    public function _onDefault() {

    }

    public function _onEnd() {

    }

    protected function onStart() {

    }

    public function view($file = false) {
        if(strlen($this->fileContent)) {
            $this->_Extorio()->appendToBody('
            <div id="file_modal" class="modal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Viewing file: '.$file.'</h4>
                        </div>
                        <div class="modal-body">
                            <pre>'.$this->fileContent.'</pre>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <script>
                $(function() {
                    $("#file_modal").modal();
                })
            </script>
            ');
        }
    }
}