<?php
namespace Core\Components\Views;
use Core\Classes\Models\UserRole;

/**
 * Manage the settings related to users
 *
 * Class AdminUserSettings
 */
class AdminUserSettings extends \Core\Components\Controllers\AdminUserSettings {
    public function _onDefault() {
        $this->_includeIncluder("extorio_editable_advanced");
        ?>
        <form method="post" action="" enctype="multipart/form-data">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">User accounts</h3>
                </div>
                <div class="panel-body">
                    <div class="alert alert-info">
                        This section defines how your user accounts are handled, and how users are able to interact with their user accounts.
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Defaults</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td style="vertical-align: top">
                                                <img style="margin-right: 15px;" class="thumbnail" src="<?=$this->config["users"]["defaults"]["avatar"]?>" width="150" height="150" />
                                            </td>
                                            <td style="vertical-align: top">
                                                <label for="role_select">Default Avatar</label>
                                                <p class="help-block">Accepted file types: jpeg, jpg, png, gif. Recommended dimensions: 150x150 (will be cropped to this size)</p>
                                                <input type="file" name="default_avatar" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Usernames</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["users"]["usernames"]["allow_number"]) echo 'checked="checked"';
                                            ?> id="usernames_allow_number" name="usernames_allow_number" type="checkbox"> Allow numbers
                                            <p class="help-block">Here you can define whether usernames are able to contain numbers. Admins are able to override this.</p>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["users"]["usernames"]["allow_special"]) echo 'checked="checked"';
                                            ?> id="usernames_allow_special" name="usernames_allow_special" type="checkbox"> Allow special characters
                                            <p class="help-block">Here you can define whether usernames are able to contain special characters. Admins are able to override this.</p>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Passwords</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="passwords_min_length">Minimum length</label>
                                        <p class="help-block">Here you can define the minimum length of passwords. Admins are able to override this.</p>
                                        <input value="<?=$this->config["users"]["passwords"]["min_length"]?>" type="number" class="form-control" id="passwords_min_length" name="passwords_min_length" placeholder="Min length">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["users"]["passwords"]["contain_special"]) echo 'checked="checked"';
                                            ?> id="passwords_contain_special" name="passwords_contain_special" type="checkbox"> Must contain special character
                                            <p class="help-block">Here you can define whether passwords must contain at least one special character. Admins are able to override this.</p>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["users"]["passwords"]["contain_number"]) echo 'checked="checked"';
                                            ?> id="passwords_contain_number" name="passwords_contain_number" type="checkbox"> Must contain number
                                            <p class="help-block">Here you can define whether passwords must contain at least one number. Admins are able to override this.</p>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Emails</h3>
                        </div>
                        <div class="panel-body">
                            <div class="checkbox">
                                <label>
                                    <input <?php
                                    if($this->config["users"]["emails"]["validate_emails"]) echo 'checked="checked"';
                                    ?> id="email_validate_email" name="email_validate_email" type="checkbox"> Validate emails
                                    <p class="help-block">Here you can define whether new email addresses are validated. Admins are able to override this.</p>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Account actions</h3>
                        </div>
                        <div class="panel-body">
                            <div class="alert alert-info">
                                This section defines what actions a user can take when navigating the user account page.
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["users"]["can_change_username"]) echo 'checked="checked"';
                                            ?> id="can_change_username" name="can_change_username" type="checkbox"> Users can change their username
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["users"]["can_change_email"]) echo 'checked="checked"';
                                            ?> id="can_change_email" name="can_change_email" type="checkbox"> Users can change their email address
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["users"]["can_change_password"]) echo 'checked="checked"';
                                            ?> id="can_change_password" name="can_change_password" type="checkbox"> Users can change their password
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Login</h3>
                </div>
                <div class="panel-body">
                    <div class="alert alert-info">
                        Modifying the authentication settings will cause all logged in users to have to log in again
                    </div>

                    <div class="form-group">
                        <label for="task_max">Session type</label>
                        <select class="form-control" id="extorio_authentication_session_type" name="extorio_authentication_session_type">
                            <option <?php
                            if($this->config["extorio"]["authentication"]["session_type"] == "basic") echo 'selected="selected"';
                            ?> value="basic">basic (less secure)</option>
                            <option <?php
                            if($this->config["extorio"]["authentication"]["session_type"] == "jwt") echo 'selected="selected"';
                            ?> value="jwt">jwt (more secure)</option>
                        </select>
                    </div>

                    <div id="jwt_settings" style="<?php
                    if($this->config["extorio"]["authentication"]["session_type"] != "jwt") echo 'display:none;';
                    ?> margin-bottom: 0;"  class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">JWT Settings</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="extorio_authentication_jwt_key">Secret key</label>
                                <input value="<?=$this->config["extorio"]["authentication"]["jwt"]["key"]?>" type="text" class="form-control" id="extorio_authentication_jwt_key" name="extorio_authentication_jwt_key" placeholder="JWT Secret Key">
                            </div>
                            <div class="form-group">
                                <label for="task_max">Encryption type</label>
                                <select class="form-control" id="extorio_authentication_jwt_alg" name="extorio_authentication_jwt_alg">
                                    <option <?php
                                    if($this->config["extorio"]["authentication"]["jwt"]["alg"] == "HS256") echo 'selected="selected"';
                                    ?> value="HS256">HS256 (recommended)</option>
                                    <option <?php
                                    if($this->config["extorio"]["authentication"]["jwt"]["alg"] == "HS384") echo 'selected="selected"';
                                    ?> value="HS384">HS384</option>
                                    <option <?php
                                    if($this->config["extorio"]["authentication"]["jwt"]["alg"] == "HS512") echo 'selected="selected"';
                                    ?> value="HS512">HS512</option>
                                    <option <?php
                                    if($this->config["extorio"]["authentication"]["jwt"]["alg"] == "RS256") echo 'selected="selected"';
                                    ?> value="RS256">RS256</option>
                                    <option <?php
                                    if($this->config["extorio"]["authentication"]["jwt"]["alg"] == "RS384") echo 'selected="selected"';
                                    ?> value="RS384">RS384</option>
                                    <option <?php
                                    if($this->config["extorio"]["authentication"]["jwt"]["alg"] == "RS512") echo 'selected="selected"';
                                    ?> value="RS512">RS512</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <script>
                        $(function() {
                            $('#favicon_select').on("click", function() {
                                $.extorio_dialog_assetManager({
                                    title: "Select an image",
                                    allowextensions: false,
                                    displaytypes: "images",
                                    selectable: true,
                                    onfileselected: function(url) {
                                        $('#application_favicon').val(url);
                                    }
                                });
                            });

                            $('#extorio_authentication_session_type').on("change", function() {
                                if($(this).val() == "jwt") {
                                    $('#jwt_settings').fadeIn();
                                } else {
                                    $('#jwt_settings').fadeOut();
                                }
                            });
                        });
                    </script>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Registration</h3>
                </div>
                <div class="panel-body">
                    <div class="alert alert-info">
                        This section defines whether users are able to register to your website, and how registration is handled. Admins are able to manually create users to override this entirely.
                    </div>

                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->config["users"]["registration"]["enabled"]) echo 'checked="checked"';
                            ?> id="registration_enabled" name="registration_enabled" type="checkbox"> Enable registration
                            <p class="help-block">Here you can enable/disable whether users are able to register to your website.</p>
                        </label>
                    </div>

                    <div style="<?php
                    if(!$this->config["users"]["registration"]["enabled"]) echo 'display:none;';
                    ?>" id="user_registration_settings">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">User Group</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="user_group_assign_type">Assign type</label>
                                    <p class="help-block">Here you can define how newly registering users are assigned to a user group.</p>
                                    <select class="form-control" name="user_group_assign_type" id="user_group_assign_type">
                                        <option <?php
                                        if($this->config["users"]["user_groups"]["assign_type"] == "single") echo 'selected="selected"';
                                        ?> value="single">Assign to a single user group</option>
                                        <option <?php
                                        if($this->config["users"]["user_groups"]["assign_type"] == "random") echo 'selected="selected"';
                                        ?> value="random">Assign to a random user group from a selection of user groups</option>
                                        <option <?php
                                        if($this->config["users"]["user_groups"]["assign_type"] == "least") echo 'selected="selected"';
                                        ?> value="least">Assign to the least populated user group from a selection of user groups</option>
                                    </select>
                                </div>

                                <div style="<?php
                                if($this->config["users"]["user_groups"]["assign_type"] != "single") echo 'display:none;';
                                ?>" id="user_group_select_single">
                                    <div class="form-group">
                                        <label>Select a single user group</label>
                                        <div class="row">
                                            <?php
                                            foreach($this->groups as $group) {
                                                ?>
                                                <div class="col-xs-3">
                                                    <div class="radio">
                                                        <label>
                                                            <input <?php
                                                            if($this->config["users"]["user_groups"]["group"] == $group->id) echo 'checked="checked"';
                                                            ?> type="radio" name="user_group" id="user_group" value="<?=$group->id?>">
                                                            <?=$group->name?>
                                                        </label>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div style="<?php
                                if($this->config["users"]["user_groups"]["assign_type"] == "single") echo 'display:none;';
                                ?>" id="user_group_select_many">
                                    <div class="form-group">
                                        <label>Select the user groups below</label>
                                        <div class="row">
                                            <?php
                                            foreach($this->groups as $group) {
                                                ?>
                                                <div class="col-xs-3">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input <?php
                                                            if(in_array($group->id,$this->config["users"]["user_groups"]["groups"])) echo 'checked="checked"';
                                                            ?> type="checkbox" name="user_groups[]" value="<?=$group->id?>">
                                                            <?=$group->name?>
                                                        </label>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <script>
                                    $('#user_group_assign_type').on("change", function() {
                                        switch ($(this).val()) {
                                            case "single" :
                                                $('#user_group_select_single').show();
                                                $('#user_group_select_many').hide();
                                                break;
                                            case "random" :
                                                $('#user_group_select_single').hide();
                                                $('#user_group_select_many').show();
                                                break;
                                            case "least" :
                                                $('#user_group_select_single').hide();
                                                $('#user_group_select_many').show();
                                                break;
                                        }
                                    });
                                </script>
                            </div>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input <?php
                                if($this->config["users"]["registration"]["authenticate_users"]) echo 'checked="checked"';
                                ?> id="registration_authenticate" name="registration_authenticate" type="checkbox"> Authenticate users
                                <p class="help-block">Here you can define whether users must authenticate their account during registration. This sends an email to them where they must click on a link before they are able to log in.</p>
                            </label>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Authentication</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="registration_authentication_key">Authentication key</label>
                                    <p class="help-block">This key is used to encrypt the authentication link that is created in the authentication email. You should keep this safe, and change it regularly.</p>
                                    <input value="<?=$this->config["users"]["registration"]["authentication_key"]?>" type="text" class="form-control" id="registration_authentication_key" name="registration_authentication_key" placeholder="Authentication key">
                                </div>
                                <div class="form-group">
                                    <label for="registration_authentication_email_subject">Email subject</label>
                                    <p class="help-block">This is the subject-line of the authentication email. You should keep this short and precise.</p>
                                    <input value="<?=$this->config["users"]["registration"]["authentication_email"]["subject"]?>" type="text" class="form-control" id="registration_authentication_email_subject" name="registration_authentication_email_subject" placeholder="Email subject">
                                </div>
                                <div class="form-group">
                                    <label for="registration_authentication_email_body">Email content</label>
                                    <p class="help-block">
                                        This is the content of the authentication email. It can contain the following replacers:
                                    <ul>
                                        <li><strong>**AUTHENTICATION_URL**</strong> : (required) displays the authentication url</li>
                                        <li><strong>**USERNAME**</strong> : displays the user's username</li>
                                        <li><strong>**SHORTNAME**</strong> : displays the user's shortname (better than the username for display purposes)</li>
                                        <li><strong>**LONGNAME**</strong> : displays the user's longname (a little more formal than the shortname)</li>
                                        <li><strong>**APPLICATION_ADDRESS**</strong> : displays your website address</li>
                                        <li><strong>**APPLICATION_NAME**</strong> : displays your website name</li>
                                        <li><strong>**APPLICATION_COMPANY**</strong> : displays your company name</li>
                                    </ul>
                                    </p>
                                    <textarea name="registration_authentication_email_body" id="registration_authentication_email_body" class="editable_area"><?=$this->config["users"]["registration"]["authentication_email"]["body"]?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input <?php
                                if($this->config["users"]["registration"]["confirm_users"]) echo 'checked="checked"';
                                ?> id="registration_confirm" name="registration_confirm" type="checkbox"> Confirm users
                                <p class="help-block">Here you can define whether the confirmation email is sent once the user has successfully registered.</p>
                            </label>
                        </div>
                    </div>
                    <script>
                        $('#registration_enabled').on("change", function() {
                            if($(this).prop("checked")) {
                                $('#user_registration_settings').show();
                            } else {
                                $('#user_registration_settings').hide();
                            }
                        });
                    </script>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Password reset</h3>
                </div>
                <div class="panel-body">
                    <div class="alert alert-info">
                        This section defines whether users are able to request a password reset, and how the process is handled.
                    </div>

                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->config["users"]["password_reset"]["enabled"]) echo 'checked="checked"';
                            ?> id="password_reset_enabled" name="password_reset_enabled" type="checkbox"> Enable password reset requests
                            <p class="help-block">Here you can enable/disable whether users are able to request a password reset.</p>
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="password_reset_key">Password reset key</label>
                        <p class="help-block">This key is used to encrypt the password reset link that is created in the password reset email. You should keep this safe, and change it regularly.</p>
                        <input value="<?=$this->config["users"]["password_reset"]["password_reset_key"]?>" type="text" class="form-control" id="password_reset_key" name="password_reset_key" placeholder="Password reset key">
                    </div>

                    <div class="form-group">
                        <label for="password_reset_email_subject">Email subject</label>
                        <p class="help-block">This is the subject-line of the password reset email. You should keep this short and precise.</p>
                        <input value="<?=$this->config["users"]["password_reset"]["password_reset_email"]["subject"]?>" type="text" class="form-control" id="password_reset_email_subject" name="password_reset_email_subject" placeholder="Email subject">
                    </div>

                    <div class="form-group">
                        <label for="password_reset_email_body">Email content</label>
                        <p class="help-block">
                            This is the content of the account email. It can contain the following replacers:
                            <ul>
                                <li><strong>**PASSWORD_RESET_URL**</strong> : (required) displays the password reset url</li>
                                <li><strong>**USERNAME**</strong> : displays the user's password</li>
                                <li><strong>**SHORTNAME**</strong> : displays the user's shortname (better than the username for display purposes)</li>
                                <li><strong>**LONGNAME**</strong> : displays the user's longname (a little more formal than the shortname)</li>
                                <li><strong>**APPLICATION_ADDRESS**</strong> : displays your website address</li>
                                <li><strong>**APPLICATION_NAME**</strong> : displays your website name</li>
                                <li><strong>**APPLICATION_COMPANY**</strong> : displays your company name</li>
                            </ul>
                        </p>
                        <textarea name="password_reset_email_body" id="password_reset_email_body" class="editable_area"><?=$this->config["users"]["password_reset"]["password_reset_email"]["body"]?></textarea>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->config["users"]["password_reset"]["confirm_password_reset"]) echo 'checked="checked"';
                            ?> id="password_reset_confirm" name="password_reset_confirm" type="checkbox"> Confirm password reset
                            <p class="help-block">Here you can define whether the confirmation email is sent once the user has reset their password.</p>
                        </label>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">User account email</h3>
                </div>
                <div class="panel-body">
                    <div class="alert alert-info">
                        This section allows you to configure the user account email that is often sent to the user to notify them of their login details. This can be configured to be sent at the end of the registration process, to confirm a password reset, or manually at any time.
                    </div>

                    <div class="form-group">
                        <label for="account_email_subject">Email subject</label>
                        <p class="help-block">This is the subject-line of the account email. You should keep this short and precise.</p>
                        <input value="<?=$this->config["users"]["account_email"]["subject"]?>" type="text" class="form-control" id="account_email_subject" name="account_email_subject" placeholder="Email subject">
                    </div>

                    <div class="form-group">
                        <label for="account_email_body">Email content</label>
                        <p class="help-block">
                            This is the content of the account email. It can contain the following replacers:
                            <ul>
                                <li><strong>**USERNAME**</strong> : (recommended) displays the user's username</li>
                                <li><strong>**SHORTNAME**</strong> : displays the user's shortname (better than the username for display purposes)</li>
                                <li><strong>**LONGNAME**</strong> : displays the user's longname (a little more formal than the shortname)</li>
                                <li><strong>**PASSWORD**</strong> : (recommended) displays the user's password</li>
                                <li><strong>**APPLICATION_ADDRESS**</strong> : displays your website address</li>
                                <li><strong>**APPLICATION_NAME**</strong> : displays your website name</li>
                                <li><strong>**APPLICATION_COMPANY**</strong> : displays your company name</li>
                            </ul>
                        </p>
                        <textarea name="account_email_body" id="account_email_body" class="editable_area"><?=$this->config["users"]["account_email"]["body"]?></textarea>
                    </div>
                </div>
            </div>
            <button type="submit" name="settings_updated" class="btn btn-primary">Update</button>
        </form>
        <script>
            $(function() {
                $('.editable_area').extorio_editable();
            });
        </script>
        <?php
    }
}