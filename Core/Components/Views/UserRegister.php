<?php
namespace Core\Components\Views;
use Core\Classes\Enums\InternalEvents;

/**
 * User registration page
 *
 * Class UserRegister
 */
class UserRegister extends \Core\Components\Controllers\UserRegister {
    public function _onDefault() {
        $actions = $this->_Extorio()->getEvent(InternalEvents::_user_register_view)->run();
        foreach($actions as $action) {
            echo $action;
        }
    }
}