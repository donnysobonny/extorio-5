<?php
namespace Core\Components\Views;
class ExtorioAdminAssets extends \Core\Components\Controllers\ExtorioAdminAssets {
    public function _onDefault() {
        ?>{b}asset-manager?selectable=false&allowbreadcrumbs=true&allowuploads=true&allowsearching=true&allowfiltering=true&allowextensions=true&displayfolders=true&columns=6{/b}<?php
    }
}