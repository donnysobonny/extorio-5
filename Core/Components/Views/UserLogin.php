<?php
namespace Core\Components\Views;
use Core\Classes\Enums\InternalEvents;

/**
 * A page for users to log in
 *
 * Class UserLogin
 */
class UserLogin extends \Core\Components\Controllers\UserLogin {
    public function _onDefault() {
        $actions = $this->_Extorio()->getEvent(InternalEvents::_user_login_view)->run();
        foreach($actions as $action) {
            echo $action;
        }
    }
}