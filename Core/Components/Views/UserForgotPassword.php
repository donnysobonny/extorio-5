<?php
namespace Core\Components\Views;
/**
 * 
 *
 * Class UserForgotPassword
 */
class UserForgotPassword extends \Core\Components\Controllers\UserForgotPassword {
    public function _onDefault() {
        if($this->passwordResetEnabled) {
            ?>
            <form method="post" action="">
                <div class="form-group">
                    <label for="username">Username or email address</label>
                    <p class="help-block">Please enter either your username or email address below. An email will be sent to you containing a link to reset your password.</p>
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username or email address">
                </div>
                <button type="submit" name="forgot_password_submitted" class="btn btn-primary">Submit</button>
            </form>
            <?php
        } else {
            ?>
            <div class="alert alert-warning">
                Resetting your password is not possible on this website
            </div>
            <?php
        }
    }
}