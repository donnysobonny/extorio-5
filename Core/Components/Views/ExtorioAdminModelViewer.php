<?php
namespace Core\Components\Views;
class ExtorioAdminModelViewer extends \Core\Components\Controllers\ExtorioAdminModelViewer {
    public function _onBegin() {
        $this->_Extorio()->includeIncluder("extorio_model_viewer");
        $this->_Extorio()->includeIncluder("datetimepicker");
        $this->_Extorio()->includeIncluder("datatables");
    }

    public function _onDefault($modelIdentifier=false,$action=false,$id=false) {
        if(!$modelIdentifier) {
            if(!$this->show_hidden_models) {
                ?>
                <a class="btn btn-default" href="/extorio-admin/model-viewer?show_hidden_models=true">Show hidden models</a>
            <?php
            } else {
                ?>
                <a class="btn btn-primary" href="/extorio-admin/model-viewer?show_hidden_models=false">Showing hidden models</a>
            <?php
            }
            ?>
            <br /><br />
            <div class="alert alert-info">Select a model below to view</div>
            <div class="row">
            <?php
            foreach($this->models as $model) {
                    ?>
                <div class="col-xs-2">
                    <a class="btn btn-primary btn-lg btn-block" href="/extorio-admin/model-viewer/<?=$model->class?>"><?=$model->label?></a><br />
                </div>
                <?php
            }
            ?>
            </div>
            <?php
        } else {
            switch($action) {
                case "detail" :
                    ?>
                    <div id="detailview">

                    </div>
                    <script>
                        $(function() {
                            $('#detailview').extorio_modelViewer_detail("<?=$modelIdentifier?>",<?=$id?>);
                        });
                    </script>
                    <?php
                    break;
                case "edit" :
                    ?>
                    <form method="post" action="">
                        <div id="editview">

                        </div>
                        <br />
                        <button type="submit" name="edit_submitted" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
                    </form>
                    <script>
                        $(function() {
                            $('#editview').extorio_modelViewer_edit("<?=$modelIdentifier?>",<?=$id?$id:0?>);
                        });
                    </script>
                    <?php
                    break;
                default :
                    ?>
                    <div id="listview">

                    </div>
                    <div id="tester">

                    </div>
                    <script>
                        $(function() {
                            $('#listview').extorio_modelViewer_list("<?=$modelIdentifier?>");
                        })
                    </script>
                    <?php
                    break;
            }
        }
    }
}