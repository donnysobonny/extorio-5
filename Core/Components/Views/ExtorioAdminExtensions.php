<?php
namespace Core\Components\Views;
class ExtorioAdminExtensions extends \Core\Components\Controllers\ExtorioAdminExtensions {
    public function _onDefault() {
        ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Internal extensions</h3>
            </div>
            <div class="panel-body">
                <table style="margin-bottom: 0;" class="table table-striped table-condensed">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Public version</th>
                        <th>Installed version</th>
                        <th><span class="fa fa-cog"></span></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        $ce = $this->core->getExtensionComponent();
                        $pb = $ce->_getPublicBuild();
                        $update = $this->core->build < $pb;
                        ?>
                        <tr class="<?php
                        if(!$this->core->isInstalled) echo 'danger';
                        elseif($update) echo 'info';
                        ?>">
                            <td>Core</td>
                            <td>The core extension</td>
                            <td><strong><?=$pb?></strong></td>
                            <td><strong><?php
                                    if($this->core->build < $pb) {
                                        ?><span class="text-danger"><?=$this->core->build?></span><?php
                                    } else {
                                        ?><span class="text-success"><?=$this->core->build?></span><?php
                                    }
                                    ?></strong></td>
                            <td>
                                <?php
                                if($update && $this->core->isInstalled) {
                                    ?><button data-id="<?=$this->core->id?>" class="btn btn-xs btn-primary extension_update"><span class="fa fa-wrench"></span> update</button>
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <?php
        $this->f->displaySearching();
        ?>
        <table class="table table-striped table-condensed">
            <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Website</th>
                <th>Author</th>
                <th>Public version</th>
                <th>Installed version</th>
                <th>Installed</th>
                <th><span class="fa fa-cog"></span></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(count($this->extensions) > 0) {
                foreach($this->extensions as $extension) {
                    $c = $extension->getExtensionComponent();
                    $pb = $c->_getPublicBuild();
                    $update = $extension->build < $pb;
                    ?>
                    <tr class="<?php
                    if(!$extension->isInstalled) echo 'danger';
                    elseif($update) echo 'info';
                    ?>">
                        <td><?=$c->_extensionName?></td>
                        <td><?=$c->_getDescription()?></td>
                        <td><?=$c->_getWebsite()?></td>
                        <td><?=$c->_getAuthor()?></td>
                        <td><strong><?=$pb?></strong></td>
                        <td><strong><?php
                                if($extension->build < $pb) {
                                    ?><span class="text-danger"><?=$extension->build?></span><?php
                                } else {
                                    ?><span class="text-success"><?=$extension->build?></span><?php
                                }
                                ?></strong></td>
                        <td><?php
                            if($extension->isInstalled) {
                                ?><span class="fa fa-check"></span><?php
                            } else {
                                ?><span class="fa fa-close"></span><?php
                            }
                            ?></td>
                        <td>
                            <?php
                            if($update && $extension->isInstalled) {
                                ?><button data-id="<?=$extension->id?>" class="btn btn-xs btn-primary extension_update"><span class="fa fa-wrench"></span> update</button>
                                <?php
                            }
                            if($extension->isInstalled) {
                                ?><button data-id="<?=$extension->id?>" class="btn btn-xs btn-danger extension_uninstall"><span class="fa fa-close"></span> uninstall</button>
                                <?php
                            } else {
                                ?><button data-id="<?=$extension->id?>" class="btn btn-xs btn-success extension_install"><span class="fa fa-check"></span> install</button>
                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="8">No extensions found</td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <script>
            $(function() {
                $('.extension_uninstall').on("click", function() {
                    var id = $(this).attr('data-id');
                    $.extorio_modal({
                        title: "Uninstall",
                        content: "Are you sure that you want to uninstall this extension?",
                        oncontinuebutton: function() {
                            $.extorio_showFullPageLoader();
                            window.location.href = "<?=$this->_getUrlToMethod("uninstall")?>" + id;
                        }
                    });
                });
                $('.extension_install').on("click", function() {
                    var id = $(this).attr('data-id');
                    $.extorio_modal({
                        title: "Install",
                        content: "Are you sure that you want to install this extension?",
                        oncontinuebutton: function() {
                            $.extorio_showFullPageLoader();
                            window.location.href = "<?=$this->_getUrlToMethod("install")?>" + id;
                        }
                    });
                });
                $('.extension_update').on("click", function() {
                    var id = $(this).attr('data-id');
                    $.extorio_modal({
                        title: "Update",
                        content: "Are you sure that you want to update this extension?",
                        oncontinuebutton: function() {
                            $.extorio_showFullPageLoader();
                            window.location.href = "<?=$this->_getUrlToMethod("update")?>" + id;
                        }
                    });
                });
            });
        </script>
        <?php
        $this->f->displayPagination();
        ?>
        <?php
    }
}