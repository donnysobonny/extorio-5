<?php
namespace Core\Components\Views;
class ExtorioInstall extends \Core\Components\Controllers\ExtorioInstall {
    public function _onBegin() {
        ?>
<head>
    <title>Extorio Installation</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="/Core/Assets/jquery/js/jquery.js"></script>
    <script type="text/javascript" src="/Core/Assets/jquery/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/Core/Assets/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="/Core/Assets/js/Extorio.js"></script>
    <script type="text/javascript"
            src="/Core/Themes/extorio%20admin/js/script.js"></script>
    <link rel="stylesheet"
          href="/Core/Themes/extorio-admin/css/internal/style.css"/>
    <link rel="stylesheet"
          href="/Core/Themes/extorio-admin/css/style_override.css"/>
</head>
<body>
        <div class="container">
        <?php
    }

    public function _onDefault() {
        if(is_callable(array($this,$this->action))) {
            call_user_func(array($this,$this->action),func_get_args());
        }
    }

    public function _onEnd() {
        ?>
        </div>
</body>
        <?php
    }

    protected function onStart() {

    }

    public function welcome() {
        ?>
        <br />
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Extorio installation - Welcome</h3>
            </div>
            <div class="panel-body">
                <p>
                    <strong>Welcome</strong> to the Extorio installation. The next few pages will take you through some simple
                    steps in order to get you set up and using Extorio.
                </p>
                <p>
                    When you are ready, click the button bellow.
                </p>
                <p>
                    <a style="float: right;" class="btn btn-primary" href="/?action=system">Begin</a>
                </p>
            </div>
        </div>
        <?php
    }

    public function system() {
        ?>
        <br />
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Extorio installation - System check</h3>
            </div>
            <div class="panel-body">
                <p>
                    <strong>First</strong> Extorio must check your server setup, in order to make sure that your server is
                    configured correctly. Below are the results:
                </p>
                <p>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Check</th>
                            <th>Description</th>
                            <th>Result</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Writable</td>
                        <td>Extorio requires the ability to be able to write to it's contained directories. The results will show any directories that it cannot write to.</td>
                        <td>
                            <?php
                            if(count($this->systemUnWritableDirs)) {
                            ?>
                            <p class="text-danger">The following directories are unwritable.</p>
                            <ul>
                                <?php
                                foreach($this->systemUnWritableDirs as $d) {
                                    ?>
                                    <li><?=$d->realPath()?></li>
                                <?php
                                }
                                } else {
                                    ?>
                                    <p class="text-success">Okay</p>
                                <?php
                                }
                                ?>
                            </ul>

                        </td>
                    </tr>
                    <tr>
                        <td>PostgreSQL is installed</td>
                        <td>Extorio requires that your server has PostgreSQL installed, and the PHP extension enabled.</td>
                        <td><?php
                            if(!$this->systemPgInstalled) {
                                ?>
                                <p class="text-danger">PostgreSQL does not appear to be installed/enabled.</p>
                            <?php
                            } else {
                                ?>
                                <p class="text-success">Okay</p>
                            <?php
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <td>PHP from the command line.</td>
                        <td>Extorio's task system requires the ability to run PHP from the command line.</td>
                        <td><?php
                            if(!$this->systemCliAccess) {
                                ?>
                                <p class="text-danger">PHP could not be run from the command line.</p>
                            <?php
                            } else {
                                ?>
                                <p class="text-success">Okay</p>
                            <?php
                            }
                            ?></td>
                    </tr>
                    </tbody>
                </table>
                </p>
                <p>
                    <?php
                    if($this->systemOkay) {
                        ?>
                        <strong>Great!</strong> It looks like you are good to go. Click the button below to continue.
                    <?php
                    } else {
                        ?>
                        <strong>Oops!</strong> It looks like your server is not configured correctly.
                    <?php
                    }
                    ?>
                </p>
                <p>
                    <a class="btn btn-danger" style="float: left;" href="/?action=welcome">Back <span class="fa fa-chevron-left"></span></a>
                    <a class="btn btn-success <?php if(!$this->systemOkay) echo 'disabled'; ?>" style="float: right;" href="/?action=application"><span class="fa fa-chevron-right"></span> Next</a>
                </p>
            </div>
        </div>
        <?php
    }

    public function application() {
        ?>
        <br />
        <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;">
                0%
            </div>
        </div>
        <form method="post" action="">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Extorio installation - Application</h3>
                </div>
                <div class="panel-body">
                    <p>
                        Below, enter some information about your website (these details can be changed later)
                    </p>
                    <p>
                        <div class="form-group">
                            <label for="name">Website name</label>
                            <p class="help-block">The name of your website is used to generate page titles etc</p>
                            <input value="<?=$this->applicationName?>" type="text" class="form-control" id="name" name="name" placeholder="Enter your website name">
                        </div>
                        <div class="form-group">
                            <label for="description">Website description</label>
                            <p class="help-block">A description of your website</p>
                            <textarea id="description" name="description" class="form-control" placeholder="Enter a description"><?=$this->applicationDescription?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="company">Company name</label>
                            <p class="help-block">The name of your company</p>
                            <input value="<?=$this->applicationCompany?>" type="text" class="form-control" id="company" name="company" placeholder="Enter your company name">
                        </div>
                        <div class="form-group">
                            <label for="domain">Address</label>
                            <p class="help-block">This is the address of your website (for example: http://my-website.com)</p>
                            <input value="<?=$this->applicationDomain?>" type="text" class="form-control" id="domain" name="domain" placeholder="Enter your website address">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input <?php
                                if($this->applicationSeo) echo 'checked="checked"';
                                ?> id="seo" name="seo" type="checkbox"> Do you want your website to be visible to search engines?
                                <p class="help-block">Ticking this box will make your website visible to SEO crawlers</p>
                            </label>
                        </div>
                        <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->applicationMaintenance) echo 'checked="checked"';
                            ?> id="maintenance" name="maintenance" type="checkbox"> Do you want to launch your website in maintenance mode?
                            <p class="help-block">Ticking this box will restrict access to your website</p>
                        </label>
                    </div>
                    </p>
                    <p>
                        <a class="btn btn-danger" style="float: left;" href="/?action=system">Back <span class="fa fa-chevron-left"></span></a>
                        <button type="submit" name="application_submitted" class="btn btn-success"  style="float: right;"><span class="fa fa-chevron-right"></span> Next</button>
                    </p>
                </div>
            </div>
        </form>
    <?php
    }

    public function email() {
        ?>
        <br />
        <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                20%
            </div>
        </div>
        <form method="post" action="">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Extorio installation - Email</h3>
                </div>
                <div class="panel-body">
                    <p>
                        Below, configure how Exorio will send your emails.
                    </p>
                    <p>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Defaults</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="default_from_name">Default from name</label>
                                        <p class="help-block">This is the default name that emails will be sent from</p>
                                        <input value="<?=$this->emailFromName?>" type="text" class="form-control" id="default_from_name" name="default_from_name" placeholder="Default from name">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="default_from_email">Default from email</label>
                                        <p class="help-block">This is the default email that emails will be sent from</p>
                                        <input value="<?=$this->emailFromEmail?>" type="text" class="form-control" id="default_from_email" name="default_from_email" placeholder="Default from email">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Transport settings</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="transport">Transport type</label>
                                <p class="help-block">The transport type allows you to choose how the server sends emails. You should select the type best suited to your server configuration. If you are unsure, select "mail".</p>
                                <select class="form-control" name="transport" id="transport">
                                    <option <?php
                                    if($this->emailTransport == "mail") echo 'selected="selected"';
                                    ?> value="mail">mail (recommended)</option>
                                    <option <?php
                                    if($this->emailTransport == "sendmail") echo 'selected="selected"';
                                    ?> value="sendmail">sendmail</option>
                                    <option <?php
                                    if($this->emailTransport == "smtp") echo 'selected="selected"';
                                    ?> value="smtp">smtp</option>
                                </select>
                            </div>

                            <div style="<?php
                            if($this->config["emails"]["transport"] != "smtp") echo 'display: none;';
                            ?>" id="smtp_settings" class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">SMTP Settings</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="smtp_host">Host</label>
                                        <input value="<?=$this->emailSmtpHost?>" type="text" class="form-control" id="smtp_host" name="smtp_host" placeholder="Host">
                                    </div>
                                    <div class="form-group">
                                        <label for="smtp_port">Port</label>
                                        <input value="<?=$this->emailSmtpPort?>" type="text" class="form-control" id="smtp_port" name="smtp_port" placeholder="Port">
                                    </div>
                                    <div class="form-group">
                                        <label for="smtp_username">Username</label>
                                        <input value="<?=$this->emailSmtpUsername?>" type="text" class="form-control" id="smtp_username" name="smtp_username" placeholder="Username">
                                    </div>
                                    <div class="form-group">
                                        <label for="smtp_password">Password</label>
                                        <input value="<?=$this->emailSmtpPassword?>" type="password" class="form-control" id="smtp_password" name="smtp_password" placeholder="Password">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </p>
                    <p>
                        <a class="btn btn-danger" style="float: left;" href="/?action=application">Back <span class="fa fa-chevron-left"></span></a>
                        <button type="submit" name="email_submitted" class="btn btn-success"  style="float: right;"><span class="fa fa-chevron-right"></span> Next</button>
                    </p>
                </div>
            </div>
        </form>
        <script>
            $(function() {
                $('#transport').on("change",function() {
                    if($(this).val() == "smtp") {
                        $('#smtp_settings').fadeIn();
                    } else {
                        $('#smtp_settings').fadeOut();
                    }
                });
            });
        </script>
        <?php
    }

    public function database() {
        ?>
        <br />
        <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                40%
            </div>
        </div>
        <form method="post" action="">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Extorio installation - Database</h3>
                </div>
                <div class="panel-body">
                    <?php
                    if($this->databaseError) {
                        ?>
                    <div class="alert alert-danger">
                        <?=$this->databaseError?>
                    </div>
                        <?php
                    }
                    ?>
                    <p>
                        Next you must configure a database connection to a database
                    </p>
                    <p>
                    <div class="form-group">
                        <label for="name">Database name</label>
                        <p class="help-block">The name of your database</p>
                        <input value="<?=$this->databaseName?>" type="text" class="form-control" id="name" name="name" placeholder="Enter your database name">
                    </div>
                    <div class="form-group">
                        <label for="host">Database host</label>
                        <p class="help-block">The host address of your database (most likely to be "localhost")</p>
                        <input value="<?=$this->databaseHost?>" type="text" class="form-control" id="host" name="host" placeholder="Enter your database host">
                    </div>
                    <div class="form-group">
                        <label for="user">Database username</label>
                        <p class="help-block">The username of a database user that can access the database</p>
                        <input value="<?=$this->databaseUser?>" type="text" class="form-control" id="user" name="user" placeholder="Enter your database user">
                    </div>
                    <div class="form-group">
                        <label for="pass">Database password</label>
                        <p class="help-block">The password of a database user that can access the database</p>
                        <input value="<?=$this->databasePass?>" type="text" class="form-control" id="pass" name="pass" placeholder="Enter your database password">
                    </div>
                    </p>
                    <p>
                        <a class="btn btn-danger" style="float: left;" href="/?action=email">Back <span class="fa fa-chevron-left"></span></a>
                        <button type="submit" name="database_submitted" class="btn btn-success"  style="float: right;"><span class="fa fa-chevron-right"></span> Next</button>
                    </p>
                </div>
            </div>
        </form>
        <?php
    }

    public function import() {
        ?>
        <br />
        <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                60%
            </div>
        </div>
        <form method="post" action="">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Extorio installation - Import</h3>
                </div>
                <div class="panel-body">
                    <p>
                        Extorio comes with a default data-set, containing the admin pages, themes, blocks and other useful components that you will need in order to make your website.
                    </p>
                    <p>
                        To import the data, click the button below.
                    </p>
                    <p>
                        <a class="btn btn-danger" style="float: left;" href="/?action=database">Back <span class="fa fa-chevron-left"></span></a>
                        <button type="submit" name="import_submitted" class="btn btn-success"  style="float: right;"><span class="fa fa-chevron-right"></span> Next</button>
                    </p>
                </div>
            </div>
        </form>
    <?php
    }

    public function admin() {
        ?>
        <br />
        <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                80%
            </div>
        </div>
        <form method="post" action="">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Extorio installation - Admin setup</h3>
                </div>
                <div class="panel-body">
                    <?php
                    if($this->adminError) {
                        ?>
                    <div class="alert alert-danger">
                        <?=$this->adminError?>
                    </div>
                        <?php
                    }
                    ?>
                    <p>
                        <strong>The final step</strong> is to create a super admin account. Use the form below to create it.
                    </p>
                    <p>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Enter your username">
                        </div>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="Enter your email address">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Enter your password">
                        </div>
                        <div class="form-group">
                            <label for="cpassword">Confirm password</label>
                            <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Enter your password">
                        </div>
                    </p>
                    <p>
                        <a class="btn btn-danger" style="float: left;" href="/?action=import">Back <span class="fa fa-chevron-left"></span></a>
                        <button type="submit" name="admin_submitted" class="btn btn-success"  style="float: right;"><span class="fa fa-chevron-right"></span> Next</button>
                    </p>
                </div>
            </div>
        </form>
    <?php
    }

    public function complete() {
        ?>
        <br />
        <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                100%
            </div>
        </div>
        <form method="post" action="">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Extorio installation - Completed</h3>
                </div>
                <div class="panel-body">
                    <p>
                        <strong>Congratulations!</strong> You have completed the installation process and are ready to start using Extorio. To start, click on the button below.
                    </p>
                    <p>
                        <a class="btn btn-danger" style="float: left;" href="/?action=admin">Back <span class="fa fa-chevron-left"></span></a>
                        <button type="submit" name="completed_submitted" class="btn btn-success"  style="float: right;"><span class="fa fa-chevron-right"></span> Finish</button>
                    </p>
                </div>
            </div>
        </form>
    <?php
    }
}