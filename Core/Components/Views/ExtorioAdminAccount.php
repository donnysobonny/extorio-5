<?php
namespace Core\Components\Views;
class ExtorioAdminAccount extends \Core\Components\Controllers\ExtorioAdminAccount {
    public function _onBegin() {

    }

    public function _onDefault() {
        ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Account Information</h3>
            </div>
            <div class="panel-title">
                <table class="table table-striped table-bordered">
                    <tbody>
                        <tr>
                            <td class="account_info_field">Username</td>
                            <td class="account_info_value"><?=$this->loggedInUser->username?></td>
                        </tr>
                        <tr>
                            <td class="account_info_field">Email</td>
                            <td class="account_info_value"><?=$this->loggedInUser->email?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <style>
            .account_info_field {
                font-weight: bold;
                text-align: right;
            }

            .account_info_value {
                width: 90%;
            }
        </style>
        <hr />
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Basic details</h3>
            </div>
            <div class="panel-body">
                <form method="post" action="" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="firstname">First name</label>
                                <input value="<?=$this->loggedInUser->firstname?>" type="text" class="form-control" id="firstname" name="firstname" placeholder="First name">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="lastname">Last name</label>
                                <input value="<?=$this->loggedInUser->lastname?>" type="text" class="form-control" id="lastname" name="lastname" placeholder="Last name">
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="form-group">
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                            <tr>
                                <td style="vertical-align: top">
                                    <img style="margin-right: 15px; margin-bottom: 0;" class="thumbnail" src="<?=$this->loggedInUser->avatar?>" width="150" height="150" />
                                </td>
                                <td style="vertical-align: top">
                                    <label for="role_select">Avatar</label>
                                    <p class="help-block">Accepted file types: jpeg, jpg, png, gif. Recommended dimensions: 150x150 (will be cropped to this size)</p>
                                    <input type="file" name="avatar" id="avatar" />
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <button type="submit" name="user_basic_details_submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
        <?php
        if($this->canChangeUsername) {
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Change username</h3>
                </div>
                <div class="panel-body">
                    <form class="form-inline" method="post" action="">
                        <div class="form-group">
                            <input type="text" class="form-control" id="username" name="username" placeholder="Enter a new username">
                        </div>
                        <button type="submit" name="user_change_username_submit" class="btn btn-primary">Change username</button>
                    </form>
                </div>
            </div>
            <?php
        }
        if($this->canChangeEmail) {
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Change email</h3>
                </div>
                <div class="panel-body">
                    <form class="form-inline" method="post" action="">
                        <div class="form-group">
                            <input type="text" class="form-control" id="email" name="email" placeholder="Enter a new email address">
                        </div>
                        <button type="submit" name="user_change_email_submit" class="btn btn-primary">Change email</button>
                    </form>
                </div>
            </div>
            <?php
        }
        if($this->canChangePassword) {
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Change password</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="">
                        <div class="form-group">
                            <label for="password">Current password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Enter current password">
                        </div>
                        <hr />
                        <div class="form-group">
                            <label for="npassword">New password</label>
                            <input type="password" class="form-control" id="npassword" name="npassword" placeholder="Enter new password">
                        </div>
                        <div class="form-group">
                            <label for="cpassword">Confirm password</label>
                            <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Confirm new password">
                        </div>
                        <button type="submit" name="user_change_password_submit" class="btn btn-primary">Change password</button>
                    </form>
                </div>
            </div>
            <?php
        }
    }

    public function _onEnd() {

    }
}