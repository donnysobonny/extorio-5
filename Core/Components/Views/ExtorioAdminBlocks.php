<?php
namespace Core\Components\Views;
class ExtorioAdminBlocks extends \Core\Components\Controllers\ExtorioAdminBlocks {
    public function _onBegin() {

    }

    public function _onDefault() {
        $urlToDefault = $this->_getUrlToDefault();
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Public</th>
            <th>Category</th>
            <th>Processor</th>
            <th>All read</th>
            <th>Read groups</th>
            <th>Modify groups</th>
            <th>Extension</th>
            <th><span class="fa fa-cog"></span></th>
        </tr>
    </thead>
    <tbody>
        <?php
        if(count($this->blocks) > 0) {
            foreach($this->blocks as $block) {
                ?>
                <tr>
                    <td><span style="border-bottom: 1px dashed; cursor: help;" class="block_name" title="<?=htmlentities($block->description)?>"><?=$block->name?></span></td>
                    <td>
                        <?php
                        if($block->isPublic) {
                            ?>
                            <span class="fa fa-check"></span>
                            <?php
                        } else {
                            ?>
                            <span class="fa fa-remove"></span>
                            <?php
                        }
                        ?>
                    </td>
                    <td><span class="label label-primary"><?=$block->category->name?></span> / <span class="label label-primary"><?=$block->subCategory->name?></span></td>
                    <td><?=$block->processor->name?></td>
                    <td><?php
                        if($block->allRead) {
                            ?><span class="fa fa-check"></span><?php
                        } else {
                            ?><span class="fa fa-remove"></span><?php
                        }
                        ?></td>
                    <td><?php
                        foreach($block->readGroups as $group) {
                            ?><span class="label label-primary"><?=$group->name?></span>
                            <?php
                        }
                        ?></td>
                    <td><?php
                        foreach($block->modifyGroups as $group) {
                            ?><span class="label label-primary"><?=$group->name?></span>
                            <?php
                        }
                        ?></td>
                    <td><?=$block->extensionName?></td>
                    <td>
                        <a class="btn btn-primary btn-xs" href="<?=$urlToDefault."edit/".$block->id?>"><span class="fa fa-edit"></span> edit</a>
                        <button data-id="<?=$block->id?>" class="btn btn-danger btn-xs delete_block"><span class="fa fa-trash"></span> delete</button>
                    </td>
                </tr>
                <?php
            }
        } else {
            ?>
        <tr>
            <td colspan="9">No blocks found</td>
        </tr>
            <?php
        }

        ?>
    </tbody>
</table>
        <script>
            $(function () {
                $('.block_name').tooltip();

                $('.delete_block').on("click", function() {
                    var self = $(this);
                    $.extorio_modal({
                        title: "Delete block?",
                        size: "modal-sm",
                        content: "Are you sure you want to delete this block?",
                        closetext: "cancel",
                        continuetext: "delete",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + self.attr("data-id");
                        }
                    });
                });
            });
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($blockId = false) {
        ?>
        <form method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->block->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter a name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description" placeholder="Enter a description"><?=$this->block->description?></textarea>
            </div>
            <div class="checkbox">
                <label>
                    <input <?php
                    $public = $this->block ? $this->block->isPublic : true;
                    if($public) echo 'checked="checked"';
                    ?> name="public" id="public" type="checkbox"> Block is public
                </label>
            </div>
            <div class="form-group">
                <label for="category">Category</label>
                <select id="category" name="category" class="form-control">
                    <?php
                    $current = $this->block->category.":".$this->block->subCategory;
                    foreach($this->categoryTree as $main) {
                        ?><optgroup label="<?=$main->name?>">
                        <?php
                        foreach($main->subCategories as $sub) {
                            ?><option <?php
                            if($main->id.":".$sub->id == $current) echo 'selected="selected"';
                            ?> value="<?=$main->id?>:<?=$sub->id?>"><?=$sub->name?></option><?php
                        }
                        ?>
                        </optgroup><?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="processor">Processor</label>
                <select class="form-control" id="processor" name="processor">
                    <?php
                    foreach($this->processors as $processor) {
                        ?>
                        <option <?php
                        if($this->block->processor == $processor->id) echo 'selected="selected"';
                        ?> value="<?=$processor->id?>"><?=$processor->label?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Access</div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div style="margin-bottom: 0;" class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">View access</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            $allRead = $this->block ? $this->block->allRead : true;
                                            if($allRead) echo 'checked="checked"';
                                            ?> id="all_read" name="all_read" type="checkbox"> All users can view this block
                                        </label>
                                    </div>
                                    <div style="<?php
                                    if($allRead) echo 'display: none;';
                                    ?>" id="read_roles_select" class="form-group">
                                        <label>Select the roles that can view this block</label>
                                        <?php
                                        foreach($this->groups as $group) {
                                            ?>
                                            <div class="checkbox">
                                                <label>
                                                    <input <?php
                                                    if(in_array($group->id,$this->block->readGroups)) echo 'checked="checked"';
                                                    ?> name="read_groups[]" value="<?=$group->id?>" type="checkbox"> <?=$group->name?> <small><?=$group->description?></small>
                                                </label>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            $(function() {
                                $('#all_read').on("change", function() {
                                    if($(this).prop("checked")) {
                                        $('#read_roles_select').hide();
                                    } else {
                                        $('#read_roles_select').show();
                                    }
                                });
                            });
                        </script>
                        <div class="col-md-6">
                            <div style="margin-bottom: 0;" class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Modify access</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>Select the roles that can modify this block</label>
                                        <?php
                                        $modGroups = $this->block ? $this->block->modifyGroups : array(1,2);
                                        foreach($this->groups as $group) {
                                            ?>
                                            <div class="checkbox">
                                                <label>
                                                    <input <?php
                                                    if(in_array($group->id,$modGroups)) echo 'checked="checked"';
                                                    ?> name="modify_groups[]" value="<?=$group->id?>" type="checkbox"> <?=$group->name?> <small><?=$group->description?></small>
                                                </label>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Advanced</div>
                </div>
                <div class="panel-body">
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->block->isHidden) echo 'checked="checked"';
                            ?> name="hidden" id="hidden" type="checkbox"> Block is hidden from the pallet and block management page
                        </label>
                    </div>
                    <?php
                    if(!$this->block) {
                        ?>
                        <div class="form-group">
                            <label for="extension">Extension</label>
                            <select class="form-control" id="extension" name="extension">
                                <optgroup label="Internal">
                                    <option value="Application">Application</option>
                                    <option value="Core">Core</option>
                                </optgroup>
                                <?php
                                if(count($this->extensions)) {
                                    ?>
                                    <optgroup label="Installed">
                                        <?php
                                        foreach($this->extensions as $e) {
                                            if(!$e->_internal) {
                                                ?>
                                                <option value="<?=$e->_extensionName?>"><?=$e->_extensionName?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>

            <button type="submit" name="block_updated" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
            <button type="submit" name="block_updated_exit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
        </form>
    <?php
    }
}