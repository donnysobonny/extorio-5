<?php
namespace Core\Components\Views;
use Core\Classes\Enums\PropertyType;

class ExtorioAdminModels extends \Core\Components\Controllers\ExtorioAdminModels {

    public function _onDefault() {
        $urlToEditModel = $this->_getUrlToMethod("edit-model");
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
<table class="table table-condensed">
    <thead>
        <tr>
            <th>Name</th>
            <th>Label</th>
            <th>Extension</th>
            <th>Additional</th>
            <th><span class="fa fa-cog"></span></th>
        </tr>
    </thead>
    <tbody>
        <?php
        if(count($this->models) > 0) {
            foreach($this->models as $model) {
                ?>
                <tr>
                    <td>
                        <span style="cursor:help;border-bottom: 1px dashed;" data-toggle="tooltip" data-placement="right" title="<?=htmlentities($model->description,ENT_COMPAT)?>"><?=$model->name?></span>
                    </td>
                    <td><?=$model->label?></td>
                    <td><?=$model->extensionName?></td>
                    <td>
                        <a class="btn btn-xs btn-default" href="/extorio-admin/models/properties/<?=$model->id?>"><span class="fa fa-edit"></span> manage properties</a>
                        <a class="btn btn-xs btn-default" href="/extorio-admin/model-viewer/<?=$model->class?>"><span class="fa fa-database"></span> view data</a>
                    </td>
                    <td>
                        <a href="<?=$urlToEditModel.$model->id?>" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span> edit</a>
                        <button data-id="<?=$model->id?>" class="btn btn-danger btn-xs delete_model"><span class="fa fa-trash"></span> delete</button>
                    </td>
                </tr>
                <?php
            }
        } else {
            ?>
        <tr>
            <td colspan="5">No models found</td>
        </tr>
            <?php
        }

        ?>
    </tbody>
</table>
        <script>
            $(function () {
                $('.delete_model').on("click", function() {
                    var self = $(this);
                    $.extorio_modal({
                        title: "Delete model?",
                        size: "modal-sm",
                        content: "Are you sure you want to delete this model?",
                        closetext: "cancel",
                        continuetext: "delete",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete_model")?>" + self.attr("data-id");
                        }
                    });
                });

                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit_model($modelId = false) {
        ?>
        <form method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->model->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter a name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" name="description" id="description"><?=$this->model->description?></textarea>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Advanced</h3>
                </div>
                <div class="panel-body">
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->model->isHidden) echo 'checked="checked"';
                            ?> name="hidden" id="hidden" type="checkbox"> Model is hidden from model management
                        </label>
                    </div>
                    <?php
                    if(!$this->model) {
                        ?>
                        <div class="form-group">
                            <label for="extension">Extension</label>
                            <select class="form-control" id="extension" name="extension">
                                <optgroup label="Internal">
                                    <option value="Application">Application</option>
                                    <option value="Core">Core</option>
                                </optgroup>
                                <?php
                                if(count($this->extensions)) {
                                    ?>
                                    <optgroup label="Installed">
                                        <?php
                                        foreach($this->extensions as $e) {
                                            if(!$e->_internal) {
                                                ?>
                                                <option value="<?=$e->_extensionName?>"><?=$e->_extensionName?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>

            <button name="model_updated" type="submit" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
            <button name="model_updated_exit" type="submit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
        </form>
    <?php
    }

    public function properties($modelId = false) {
        ?>
        <div style="display: none;" class="property_form">
            <div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter a name">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea id="description" name="description" class="form-control" placeholder="Enter a description"></textarea>
                </div>

                <div class="form-group">
                    <label for="type">Type</label>
                    <select id="type" name="type" class="form-control">
                        <option value="0">--select a type--</option>
                        <option value="basic">Basic</option>
                        <option value="enum">Enum</option>
                        <option value="complex">Complex</option>
                        <option value="meta">Meta</option>
                    </select>
                </div>

                <div style="display: none;" id="basicType_select" class="form-group">
                    <label for="basicType">Basic type</label>
                    <select id="basicType" name="basicType" class="form-control">
                        <option value="checkbox">checkbox</option>
                        <option value="email">email</option>
                        <option value="password">password</option>
                        <option value="textfield">textfield</option>
                        <option value="textarea">textarea</option>
                        <option value="number">number</option>
                        <option value="decimal">decimal</option>
                        <option value="date">date</option>
                        <option value="time">time</option>
                        <option value="datetime">datetime</option>
                    </select>
                </div>

                <div style="display: none;" id="enumNamespace_select" class="form-group">
                    <label for="enumNamespace">Dropdown type</label>
                    <select class="form-control" id="enumNamespace" name="enumNamespace">
                        <?php
                        foreach($this->enums as $enum) {
                            ?>
                        <option value="<?=$enum->namespace?>"><?=$enum->label?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>

                <div style="display: none;" id="complexType_select" class="form-group">
                    <label for="complexType">Complex type</label>
                    <select id="complexType" name="complexType" class="form-control">
                        <option value="array">array</option>
                        <option value="object">object</option>
                    </select>
                </div>

                <div style="display: none;" id="childModelNamespace_select" class="form-group">
                    <label for="childModelNamespace">Child model</label>
                    <select id="childModelNamespace" name="childModelNamespace" class="form-control">
                        <?php
                        foreach($this->models as $model) {
                            ?>
                            <option value="<?=$model->namespace?>"><?=$model->label?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="isIndex" name="isIndex"> Is index
                    </label>
                </div>
            </div>
        </div>

        <button class="btn btn-primary property_add"><span class="fa fa-plus"></span> Add new property</button>
        <br /><br />
        <table id="properties_table" class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Label</th>
                    <th>Description</th>
                    <th>Type</th>
                    <th>Additional</th>
                    <th>Index</th>
                    <th>Move</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($this->model->properties) {
                    foreach($this->model->properties as $property) {
                        ?>
                <tr class="property_row" data-propertyid="<?=$property->id?>">
                    <td style="display: none;" data-field="$json"><?=json_encode($property)?></td>
                    <td data-field="name"><?=$property->name?></td>
                    <td data-field="label"><?=$property->label?></td>
                    <td data-field="description"><?=$property->description?></td>
                    <td data-field="type"><?=$property->propertyType?></td>
                    <td data-field="additional"><?php
                        switch($property->propertyType) {
                            case PropertyType::_basic :
                                echo $property->basicType;
                                break;
                            case PropertyType::_enum :
                                echo $property->enumNamespace;
                                break;
                            case PropertyType::_complex :
                                echo $property->childModelNamespace."(".$property->complexType.")";
                                break;
                        }
                        ?></td>
                    <td data-field="isIndex">
                        <?php
                        if($property->isIndex) {
                            ?>
                            <span class="fa fa-check"></span>
                            <?php
                        } else {
                            ?>
                            <span class="fa fa-remove"></span>
                        <?php
                        }
                        ?>
                    </td>
                    <td>
                        <button class="btn btn-xs btn-primary property_moveup"><span class="fa fa-chevron-up"></span> move up</button>
                        <button class="btn btn-xs btn-primary property_movedown"><span class="fa fa-chevron-down"></span> move down</button>
                    </td>
                    <td>
                        <button class="btn btn-xs btn-primary property_edit"><span class="fa fa-edit"></span> edit</button>
                        <button class="btn btn-xs btn-danger property_delete"><span class="fa fa-trash"></span> delete</button>
                    </td>
                </tr>
                        <?php
                    }
                }
                ?>

            </tbody>
        </table>
        <script>
            $(function() {
                var updateDefault = function() {
                    var n = 0;
                    //set the first row to default
                    $('.property_row').each(function() {
                        if(n == 0) {
                            $(this).addClass('info');
                        } else {
                            $(this).removeClass('info');
                        }
                        n++;
                    });
                };

                var updateMovables = function() {
                    $('.property_row').each(function() {
                        var row = $(this);
                        //if has a prev
                        if(row.prev().length > 0) {
                            row.find('.property_moveup').removeClass("disabled");
                        } else {
                            row.find('.property_moveup').addClass("disabled");
                        }
                        //if has a next
                        if(row.next().length > 0) {
                            row.find('.property_movedown').removeClass("disabled");
                        } else {
                            row.find('.property_movedown').addClass("disabled");
                        }
                    })
                };

                var updateModel = function(displayMessage) {
                    $.extorio_showFullPageLoader();
                    var model = {};
                    model.id = <?=$this->model->id?>;
                    model.properties = [];
                    //fetch the rows in their natural order
                    $('.property_row').each(function() {
                        model.properties.push(JSON.parse($(this).find('[data-field="$json"]').text()));
                    });
                    $.extorio_api({
                        endpoint: "/models/" + model.id,
                        type: "POST",
                        data:  {
                            data: model
                        },
                        oncomplete: function() {
                            $.extorio_hideFullPageLoader();
                        },
                        onsuccess: function() {
                            if(displayMessage) {
                                $.extorio_messageSuccess("Model updated");
                            }
                        }
                    });
                };

                var insertProperty = function(property) {
                    var additional = "";
                    switch (property.propertyType) {
                        case "basic" :
                            additional = property.basicType;
                            break;
                        case "enum" :
                            additional = property.enumNamespace;
                            break;
                        case "complex" :
                            additional = property.childModelNamespace + "(" + property.complexType + ")";
                            break;
                    }
                    var indexed = "";
                    if(property.isIndex) {
                        indexed = '<span class="fa fa-check"></span>';
                    } else {
                        indexed = '<span class="fa fa-remove"></span>';
                    }
                    var row = $('' +
                    '<tr class="property_row" data-propertyid="'+property.id+'">' +
                    '   <td style="display: none;" data-field="$json">'+JSON.stringify(property)+'</td>' +
                    '   <td data-field="name">'+property.name+'</td>' +
                    '   <td data-field="label">'+property.label+'</td>' +
                    '   <td data-field="description">'+property.description+'</td>' +
                    '   <td data-field="type">'+property.propertyType+'</td>' +
                    '   <td data-field="additional">'+additional+'</td>' +
                    '   <td data-field="isIndex">'+indexed+'</td>' +
                    '   <td>' +
                    '       <button class="btn btn-xs btn-primary property_moveup"><span class="fa fa-chevron-up"></span> move up</button>' +
                    '       <button class="btn btn-xs btn-primary property_movedown"><span class="fa fa-chevron-down"></span> move down</button>' +
                    '   </td>' +
                    '   <td>' +
                    '       <button class="btn btn-xs btn-primary property_edit"><span class="fa fa-edit"></span> edit</button>' +
                    '       <button class="btn btn-xs btn-danger property_delete"><span class="fa fa-trash"></span> delete</button>' +
                    '   </td>' +
                    '</tr>');
                    row.find('.property_moveup').on("click", function() {
                        row.insertBefore(row.prev());
                        updateModel();
                        updateMovables();
                        updateDefault();
                    });
                    row.find('.property_movedown').on("click", function() {
                        row.insertAfter(row.next());
                        updateModel();
                        updateMovables();
                        updateDefault();
                    });
                    row.find('.property_delete').on("click", function() {
                        //delete the property
                        $.extorio_showFullPageLoader();
                        //remove from the table
                        row.remove();
                        updateModel(false);
                        updateMovables();
                        updateDefault();
                    });
                    row.find('.property_edit').on("click", function() {
                        $.extorio_showFullPageLoader();
                        //find the property
                        $.extorio_api({
                            endpoint: "/properties/"+parseInt(row.attr('data-propertyid')),
                            type: "GET",
                            oncomplete: function() {
                                $.extorio_hideFullPageLoader();
                            },
                            onsuccess: function(data) {
                                if(data.data) {
                                    var property = data.data;
                                    $.extorio_modal({
                                        title: "Edit property",
                                        closetext: "Cancel",
                                        continuetext: "Update",
                                        content: $('.property_form').html(),
                                        onopen: function() {
                                            var modal = this;
                                            var basicType_select = modal.find('#basicType_select');
                                            var enumNamespace_select = modal.find('#enumNamespace_select');
                                            var complexType_select = modal.find('#complexType_select');
                                            var childModelNamespace_select = modal.find('#childModelNamespace_select');

                                            modal.find('#name').val(property.name);
                                            modal.find('#description').val(property.description);
                                            modal.find('#type').val(property.propertyType);
                                            modal.find('#basicType').val(property.basicType);
                                            modal.find('#complexType').val(property.complexType);
                                            modal.find('#enumNamespace').val(property.enumNamespace);
                                            modal.find('#childModelNamespace').val(property.childModelNamespace);
                                            modal.find('#isIndex').prop("checked",property.isIndex);

                                            switch (modal.find('#type').val()) {
                                                case "0" :
                                                    basicType_select.hide();
                                                    enumNamespace_select.hide();
                                                    complexType_select.hide();
                                                    childModelNamespace_select.hide();
                                                    break;
                                                case "basic" :
                                                    basicType_select.show();
                                                    enumNamespace_select.hide();
                                                    complexType_select.hide();
                                                    childModelNamespace_select.hide();
                                                    break;
                                                case "enum" :
                                                    basicType_select.hide();
                                                    enumNamespace_select.show();
                                                    complexType_select.hide();
                                                    childModelNamespace_select.hide();
                                                    break;
                                                case "complex" :
                                                    basicType_select.hide();
                                                    enumNamespace_select.hide();
                                                    complexType_select.show();
                                                    childModelNamespace_select.show();
                                                    break;
                                                case "meta" :
                                                    basicType_select.hide();
                                                    enumNamespace_select.hide();
                                                    complexType_select.hide();
                                                    childModelNamespace_select.hide();
                                                    break;
                                            }

                                            modal.find('#type').on("change",function() {
                                                switch ($(this).val()) {
                                                    case "0" :
                                                        basicType_select.hide();
                                                        enumNamespace_select.hide();
                                                        complexType_select.hide();
                                                        childModelNamespace_select.hide();
                                                        break;
                                                    case "basic" :
                                                        basicType_select.show();
                                                        enumNamespace_select.hide();
                                                        complexType_select.hide();
                                                        childModelNamespace_select.hide();
                                                        break;
                                                    case "enum" :
                                                        basicType_select.hide();
                                                        enumNamespace_select.show();
                                                        complexType_select.hide();
                                                        childModelNamespace_select.hide();
                                                        break;
                                                    case "complex" :
                                                        basicType_select.hide();
                                                        enumNamespace_select.hide();
                                                        complexType_select.show();
                                                        childModelNamespace_select.show();
                                                        break;
                                                    case "meta" :
                                                        basicType_select.hide();
                                                        enumNamespace_select.hide();
                                                        complexType_select.hide();
                                                        childModelNamespace_select.hide();
                                                        break;
                                                }
                                            });
                                        },
                                        oncontinuebutton: function() {
                                            $.extorio_showFullPageLoader();
                                            var modal = this;

                                            var p = {};
                                            p.id = property.id;
                                            p.name = modal.find('#name').val();
                                            p.description = modal.find('#description').val();
                                            p.propertyType = modal.find('#type').val();
                                            p.basicType = modal.find('#basicType').val();
                                            p.enumNamespace = modal.find('#enumNamespace').val();
                                            p.complexType = modal.find('#complexType').val();
                                            p.childModelNamespace = modal.find('#childModelNamespace').val();
                                            p.isIndex = modal.find('#isIndex').prop("checked");

                                            var r = $('.property_row[data-propertyid="'+ p.id+'"]');
                                            var additional = "";
                                            switch (p.propertyType) {
                                                case "basic" :
                                                    additional = p.basicType;
                                                    break;
                                                case "enum" :
                                                    additional = p.enumNamespace;
                                                    break;
                                                case "complex" :
                                                    additional = p.childModelNamespace + "(" + p.complexType + ")";
                                                    break;
                                            }
                                            var indexed = "";
                                            if(p.isIndex) {
                                                indexed = '<span class="fa fa-check"></span>';
                                            } else {
                                                indexed = '<span class="fa fa-remove"></span>';
                                            }
                                            r.find('td[data-field="$json"]').text(JSON.stringify(p));
                                            r.find('td[data-field="name"]').html(p.name);
                                            r.find('td[data-field="label"]').html(p.label);
                                            r.find('td[data-field="description"]').html(p.description);
                                            r.find('td[data-field="type"]').html(p.propertyType);
                                            r.find('td[data-field="additional"]').html(additional);
                                            r.find('td[data-field="isIndex"]').html(indexed);
                                            $.extorio_hideFullPageLoader();
                                            //update model
                                            updateModel(false);
                                            $.extorio_messageSuccess("Property successfully updated");
                                        }
                                    });
                                }
                            }
                        });
                    });
                    $('#properties_table tbody').append(row);
                    updateModel(false);
                    updateDefault();
                    updateMovables();
                };

                //initiate
                updateMovables();
                updateDefault();

                //bindings
                $('.property_add').on("click", function() {
                    $.extorio_modal({
                        title: "Add new property",
                        closetext: "Cancel",
                        continuetext: "Add",
                        content: $('.property_form').html(),
                        onopen: function() {
                            var modal = this;
                            modal.find('#type').on("change",function() {
                                var basicType_select = modal.find('#basicType_select');
                                var enumNamespace_select = modal.find('#enumNamespace_select');
                                var complexType_select = modal.find('#complexType_select');
                                var childModelNamespace_select = modal.find('#childModelNamespace_select');
                                switch ($(this).val()) {
                                    case "0" :
                                        basicType_select.hide();
                                        enumNamespace_select.hide();
                                        complexType_select.hide();
                                        childModelNamespace_select.hide();
                                        break;
                                    case "basic" :
                                        basicType_select.show();
                                        enumNamespace_select.hide();
                                        complexType_select.hide();
                                        childModelNamespace_select.hide();
                                        break;
                                    case "enum" :
                                        basicType_select.hide();
                                        enumNamespace_select.show();
                                        complexType_select.hide();
                                        childModelNamespace_select.hide();
                                        break;
                                    case "complex" :
                                        basicType_select.hide();
                                        enumNamespace_select.hide();
                                        complexType_select.show();
                                        childModelNamespace_select.show();
                                        break;
                                    case "meta" :
                                        basicType_select.hide();
                                        enumNamespace_select.hide();
                                        complexType_select.hide();
                                        childModelNamespace_select.hide();
                                        break;
                                }
                            });
                        },
                        oncontinuebutton: function() {
                            $.extorio_showFullPageLoader();
                            var modal = this;
                            var property = {};
                            property.name = modal.find('#name').val();
                            property.description = modal.find('#description').val();
                            property.propertyType = modal.find('#type').val();
                            property.basicType = modal.find('#basicType').val();
                            property.enumNamespace = modal.find('#enumNamespace').val();
                            property.complexType = modal.find('#complexType').val();
                            property.enumNamespace = modal.find('#enumNamespace').val();
                            property.childModelNamespace = modal.find('#childModelNamespace').val();
                            property.isIndex = modal.find('#isIndex').prop("checked");
                            $.extorio_api({
                                endpoint: "/properties",
                                type: "POST",
                                data: {
                                    data: property
                                },
                                oncomplete: function() {
                                    $.extorio_hideFullPageLoader();
                                },
                                onsuccess: function(data) {
                                    property = data.data;
                                    insertProperty(property);
                                    $.extorio_messageSuccess("Property successfully created");
                                }
                            });
                        }
                    });
                });

                $('.property_row').each(function() {
                    var row = $(this);
                    row.find('.property_moveup').on("click", function() {
                        row.insertBefore(row.prev());
                        updateModel();
                        updateMovables();
                        updateDefault();
                    });
                    row.find('.property_movedown').on("click", function() {
                        row.insertAfter(row.next());
                        updateModel();
                        updateMovables();
                        updateDefault();
                    });
                    row.find('.property_delete').on("click", function() {
                        //remove from the table
                        row.remove();
                        //update the model
                        updateModel(true);
                        updateMovables();
                        updateDefault();
                    });
                    row.find('.property_edit').on("click", function() {
                        $.extorio_showFullPageLoader();
                        //find the property
                        $.extorio_api({
                            endpoint: "/properties/"+parseInt(row.attr('data-propertyid')),
                            type: "GET",
                            oncomplete: function() {
                                $.extorio_hideFullPageLoader();
                            },
                            onsuccess: function(data) {
                                if(data.data) {
                                    var property = data.data;
                                    $.extorio_modal({
                                        title: "Edit property",
                                        closetext: "Cancel",
                                        continuetext: "Update",
                                        content: $('.property_form').html(),
                                        onopen: function() {
                                            var modal = this;
                                            var basicType_select = modal.find('#basicType_select');
                                            var enumNamespace_select = modal.find('#enumNamespace_select');
                                            var complexType_select = modal.find('#complexType_select');
                                            var childModelNamespace_select = modal.find('#childModelNamespace_select');

                                            modal.find('#name').val(property.name);
                                            modal.find('#label').val(property.label);
                                            modal.find('#description').val(property.description);
                                            modal.find('#type').val(property.propertyType);
                                            modal.find('#basicType').val(property.basicType);
                                            modal.find('#complexType').val(property.complexType);
                                            modal.find('#enumNamespace').val(property.enumNamespace);
                                            modal.find('#childModelNamespace').val(property.childModelNamespace);
                                            modal.find('#isIndex').prop("checked",property.isIndex);

                                            switch (modal.find('#type').val()) {
                                                case "0" :
                                                    basicType_select.hide();
                                                    enumNamespace_select.hide();
                                                    complexType_select.hide();
                                                    childModelNamespace_select.hide();
                                                    break;
                                                case "basic" :
                                                    basicType_select.show();
                                                    enumNamespace_select.hide();
                                                    complexType_select.hide();
                                                    childModelNamespace_select.hide();
                                                    break;
                                                case "enum" :
                                                    basicType_select.hide();
                                                    enumNamespace_select.show();
                                                    complexType_select.hide();
                                                    childModelNamespace_select.hide();
                                                    break;
                                                case "complex" :
                                                    basicType_select.hide();
                                                    enumNamespace_select.hide();
                                                    complexType_select.show();
                                                    childModelNamespace_select.show();
                                                    break;
                                                case "meta" :
                                                    basicType_select.hide();
                                                    enumNamespace_select.hide();
                                                    complexType_select.hide();
                                                    childModelNamespace_select.hide();
                                                    break;
                                            }

                                            modal.find('#type').on("change",function() {
                                                switch ($(this).val()) {
                                                    case "0" :
                                                        basicType_select.hide();
                                                        enumNamespace_select.hide();
                                                        complexType_select.hide();
                                                        childModelNamespace_select.hide();
                                                        break;
                                                    case "basic" :
                                                        basicType_select.show();
                                                        enumNamespace_select.hide();
                                                        complexType_select.hide();
                                                        childModelNamespace_select.hide();
                                                        break;
                                                    case "enum" :
                                                        basicType_select.hide();
                                                        enumNamespace_select.show();
                                                        complexType_select.hide();
                                                        childModelNamespace_select.hide();
                                                        break;
                                                    case "complex" :
                                                        basicType_select.hide();
                                                        enumNamespace_select.hide();
                                                        complexType_select.show();
                                                        childModelNamespace_select.show();
                                                        break;
                                                    case "meta" :
                                                        basicType_select.hide();
                                                        enumNamespace_select.hide();
                                                        complexType_select.hide();
                                                        childModelNamespace_select.hide();
                                                        break;
                                                }
                                            });
                                        },
                                        oncontinuebutton: function() {
                                            $.extorio_showFullPageLoader();
                                            var modal = this;
                                            var p = {};
                                            p.id = property.id;
                                            p.label = modal.find('#label').val();
                                            p.description = modal.find('#description').val();
                                            p.propertyType = modal.find('#type').val();
                                            p.basicType = modal.find('#basicType').val();
                                            p.enumNamespace = modal.find('#enumNamespace').val();
                                            p.complexType = modal.find('#complexType').val();
                                            p.childModelNamespace = modal.find('#childModelNamespace').val();
                                            p.isIndex = modal.find('#isIndex').prop('checked');

                                            var r = $('.property_row[data-propertyid="'+ p.id+'"]');
                                            var additional = "";
                                            switch (p.propertyType) {
                                                case "basic" :
                                                    additional = p.basicType;
                                                    break;
                                                case "enum" :
                                                    additional = p.enumNamespace;
                                                    break;
                                                case "complex" :
                                                    additional = p.childModelNamespace + "(" + p.complexType + ")";
                                                    break;
                                            }
                                            var indexed = "";
                                            if(p.isIndex) {
                                                indexed = '<span class="fa fa-check"></span>';
                                            } else {
                                                indexed = '<span class="fa fa-remove"></span>';
                                            }
                                            r.find('td[data-field="$json"]').text(JSON.stringify(p));
                                            r.find('td[data-field="name"]').html(p.name);
                                            r.find('td[data-field="label"]').html(p.label);
                                            r.find('td[data-field="description"]').html(p.description);
                                            r.find('td[data-field="type"]').html(p.propertyType);
                                            r.find('td[data-field="additional"]').html(additional);
                                            r.find('td[data-field="isIndex"]').html(indexed);
                                            $.extorio_hideFullPageLoader();
                                            //update model
                                            updateModel(false);
                                            $.extorio_messageSuccess("Property successfully updated");
                                        }
                                    });
                                }
                            }
                        });
                    });
                });
            });
        </script>
        <?php
    }
}