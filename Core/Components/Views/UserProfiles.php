<?php
namespace Core\Components\Views;
use Core\Classes\Enums\InternalEvents;

/**
 * 
 *
 * Class UserProfiles
 */
class UserProfiles extends \Core\Components\Controllers\UserProfiles {
    public function _onDefault($id = false) {
        $results = $this->_Extorio()->getEvent(InternalEvents::_user_profiles_view)->run(array($this->user));
        foreach($results as $result) {
            echo $result;
        }
    }
}