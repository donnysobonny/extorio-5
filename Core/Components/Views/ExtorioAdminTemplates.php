<?php
namespace Core\Components\Views;
use Core\Classes\Helpers\LayoutSystem;

class ExtorioAdminTemplates extends \Core\Components\Controllers\ExtorioAdminTemplates {

    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Default</th>
                <th>Modify groups</th>
                <th>Extension</th>
                <th><span class="fa fa-cog"></span></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(count($this->templates) > 0) {
                foreach($this->templates as $template) {
                    ?>
                    <tr>
                        <td><?=$template->name?></td>
                        <td><?=$template->description?></td>
                        <td><?php
                            if($template->default) {
                                ?>
                                <span class="fa fa-check"></span>
                                <?php
                            } else {
                                ?>
                                <span class="fa fa-remove"></span>
                                <?php
                            }
                            ?></td>
                        <td>
                            <?php
                            foreach($template->modifyGroups as $group) {
                                ?><span class="label label-primary"><?=$group->name?></span>
                                <?php
                            }
                            ?>
                        </td>
                        <td><?=$template->extensionName?></td>
                        <td>
                            <a class="btn btn-primary btn-xs" href="<?=$this->_getUrlToMethod("edit",array($template->id))?>"><span class="fa fa-pencil"></span> edit</a>
                            <button data-id="<?=$template->id?>" class="btn btn-danger btn-xs delete_template"><span class="fa fa-trash"></span> delete</button>
                        </td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="6">No templates found</td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <script>
            $(function() {
                $('.delete_template').on("click", function() {
                    var self = $(this);
                    $.extorio_modal({
                        title: "Delete template?",
                        size: "modal-sm",
                        content: "Are you sure you want to delete this template?",
                        closetext: "cancel",
                        continuetext: "delete",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + self.attr("data-id");
                        }
                    });
                });
            });
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($templateId = false) {
        ?>
        <form id="edit_form" method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->template->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea name="description" id="description" class="form-control"><?=$this->template->description?></textarea>
            </div>
            <div class="checkbox">
                <label>
                    <input <?php
                    if($this->template->default) {
                        echo 'checked="checked"';
                    }
                    ?> name="default" id="default" type="checkbox"> Template is default
                </label>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Modify access</div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>Select the user roles that are able to modify this template</label>
                        <?php
                        $mods = $this->template ? $this->template->modifyGroups : array(1,2);
                        foreach($this->groups as $group) {
                            ?>
                            <div class="checkbox">
                                <label>
                                    <input <?php
                                    if(in_array($group->id,$mods)) echo 'checked="checked"';
                                    ?> name="groups[]" value="<?=$group->id?>" type="checkbox"> <?=$group->name?> <small><?=$group->description?></small>
                                </label>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Advanced</div>
                </div>
                <div class="panel-body">
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->template->isHidden) echo 'checked="checked"';
                            ?> name="hidden" id="hidden" type="checkbox"> Template is hidden from template management
                        </label>
                    </div>
                    <?php
                    if(!$this->template) {
                        ?>
                        <div class="form-group">
                            <label for="extension">Extension</label>
                            <select class="form-control" id="extension" name="extension">
                                <optgroup label="Internal">
                                    <option value="Application">Application</option>
                                    <option value="Core">Core</option>
                                </optgroup>
                                <?php
                                if(count($this->extensions)) {
                                    ?>
                                    <optgroup label="Installed">
                                        <?php
                                        foreach($this->extensions as $e) {
                                            if(!$e->internal) {
                                                ?>
                                                <option value="<?=$e->extensionName?>"><?=$e->extensionName?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="prependtohead">Prepend to head</label>
                                <textarea class="form-control" id="prependtohead" name="prependtohead"><?=$this->template->prependToHead?></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="appendtohead">Append to head</label>
                                <textarea class="form-control" id="appendtohead" name="appendtohead"><?=$this->template->appendToHead?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="prependtobody">Prepend to body</label>
                                <textarea class="form-control" id="prependtobody" name="prependtobody"><?=$this->template->prependToBody?></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="appendtobody">Append to body</label>
                                <textarea class="form-control" id="appendtobody" name="appendtobody"><?=$this->template->appendToBody?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button id="template_updated" name="template_updated" type="submit" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
            <button id="template_updated_exit" name="template_updated_exit" type="submit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
        </form>
        <?php
    }
}