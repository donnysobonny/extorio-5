<?php
namespace Core\Components\Views;
use Core\Classes\Helpers\UserReportQuery;

/**
 * Manage user reports
 *
 * Class AdminUserReports
 */
class AdminUserReports extends \Core\Components\Controllers\AdminUserReports {
    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-striped table-condensed">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Extension</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($this->reports) > 0) {
                    foreach($this->reports as $report) {
                        ?>
                <tr>
                    <td><?=$report->name?></td>
                    <td><?=$report->description?></td>
                    <td><?=$report->extensionName?></td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="<?=$this->_getUrlToMethod("edit",array($report->id))?>"><span class="fa fa-edit"></span> edit</a>
                        <button data-id="<?=$report->id?>" class="btn btn-xs btn-danger delete_report"><span class="fa fa-trash"></span> delete</button>
                    </td>
                </tr>
                        <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="4">No reports found</td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $(function() {
                $('.delete_report').on("click", function() {
                    var id = $(this).attr('data-id');
                    $.extorio_modal({
                        title: "Delete user report",
                        content: "Are you sure that you want to delete this user report?",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + id
                        }
                    });
                });
            })
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($id = false) {
        $this->_appendToHead('<script type="text/javascript" src="/Core/Assets/js/Extorio.reports.min.js"></script>');
        ?>
        <form id="user_report_form" method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->report->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter a name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" name="description" id="description" placeholder="Enter a description"><?=$this->report->description?></textarea>
            </div>
            <?php
            if(!$this->report) {
                ?>
                <div class="form-group">
                    <label for="extension">Extension</label>
                    <select class="form-control" id="extension" name="extension">
                        <optgroup label="Internal">
                            <option value="Application">Application</option>
                            <option value="Core">Core</option>
                        </optgroup>
                        <?php
                        if(count($this->extensions)) {
                            ?>
                            <optgroup label="Installed">
                                <?php
                                foreach($this->extensions as $e) {
                                    if(!$e->internal) {
                                        ?>
                                        <option value="<?=$e->extensionName?>"><?=$e->extensionName?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </optgroup>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <?php
            }
            ?>
            <input type="hidden" name="" id="fake_button" value="">
            <input type="hidden" name="query" id="query" value="">
        </form>
        <div class="row">
            <div class="col-xs-3">
                <h3>Query</h3>
                <div id="user_report_query_outer">

                </div>
            </div>
            <div class="col-xs-9">
                <h3>Report</h3>
                <div class="well well-sm">
                    <table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
                        <tbody>
                            <tr>
                                <td style="vertical-align: middle;">
                                    Limit&nbsp;&nbsp;
                                    <select style="width: auto; display: inline;" class="form-control user_report_limit">
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                        <option value="250">250</option>
                                        <option value="500">500</option>
                                        <option value="1000">1000</option>
                                        <option value="2500">2500</option>
                                        <option value="0">NO LIMIT</option>
                                    </select>
                                </td>
                                <td style="vertical-align: middle; text-align: right;">
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-default user_report_download_page"><span class="fa fa-download"></span> Download Page</button>
                                        <button type="button" class="btn btn-default user_report_download_all"><span class="fa fa-download"></span> Download All</button>
                                    </div>
                                    <button type="button" class="btn btn-primary user_report_generate"><span class="fa fa-refresh"></span> Generate Report</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="user_report_table" class="table-responsive">
                    <table class="table table-striped table-condensed table-bordered">
                        <thead>
                            <tr>
                                <?php
                                foreach(UserReportQuery::getExposedProperties() as $p) {
                                    ?><th><?=$p?></th><?php
                                }
                                ?>
                            </tr>
                        </thead>
                        <tbody id="user_report_data">
                            <tr>
                                <td colspan="<?=count(UserReportQuery::getExposedProperties())?>">Generate report by clicking the "Generate Report" button above.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="well well-sm">
                    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                        <tbody>
                            <tr>
                                <td style="vertical-align: middle;">
                                    Page <span class="user_report_page_cur">0</span> of <span class="user_report_page_tot">0</span> (from <span class="user_report_result_tot">0</span> total results)
                                </td>
                                <td style="vertical-align: middle; text-align: right;">
                                    <div class="btn-group" role="group">
                                        <button disabled="disabled" type="button" class="btn btn-default disabled user_report_page_prev"><span class="fa fa-chevron-left"></span> prev</button>
                                        <button disabled="disabled" type="button" class="btn btn-default disabled user_report_page_next">next <span class="fa fa-chevron-right"></span></button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <br />
        <button type="button" class="btn btn-primary submit"><span class="fa fa-save"></span> Save</button>
        <button type="button" class="btn btn-info submit_exit"><span class="fa fa-sign-out"></span> Save and exit</button>
        <style>
            #user_report_table {
                font-size: 9px;
            }

            #user_report_table td {
                white-space: nowrap;
                padding: 5px;
            }

            #user_report_query_outer {
                font-size: 10px;
            }
            #user_report_query_outer .panel {
                margin-bottom: 5px;
            }
            #user_report_query_outer .panel-heading {
                padding: 5px;
            }
            #user_report_query_outer .panel-body {
                padding: 5px;
            }
            #user_report_query_outer button {
                font-size: 10px;
            }
            #user_report_query_outer .form-control {
                font-size: 10px;
                height: auto;
                padding: 2px;
            }
            #user_report_query_outer .btn-group {
                margin-bottom: 5px;
            }
            #user_report_query_outer .checkbox {
                margin: 0 0 5px;
            }
            #user_report_query_outer .checkbox > label {
                height: 0;
                min-height: 0;
            }
            #user_report_query_outer .checkbox > label > input {
                top: -3px;
            }
        </style>
        <script>
            var fieldCount = <?=count(UserReportQuery::getExposedProperties())?>;

            var page = 0;

            $('.user_report_download_page').on("click", function() {
                var limit = parseInt($('.user_report_limit').val());
                $.extorio_showFullPageLoader();
                $.extorio_api({
                    endpoint: "/users/report_download",
                    data: {
                        query: $('#user_report_query_outer').extorio_reports__toObject(),
                        limit: limit,
                        skip: page * limit
                    },
                    oncomplete: function() {
                        $.extorio_hideFullPageLoader();
                    },
                    onsuccess: function(resp) {
                        $.extorio_dialog({
                            title: "Your download is ready",
                            content: '<p>Your download is ready. To download the file, click the button below.</p>' +
                            '<a target="_blank" class="btn btn-default" href="'+resp.data+'"><span class="fa fa-download"></span> Download</a>'
                        });
                    }
                })
            });

            $('.user_report_download_all').on("click", function() {
                var limit = parseInt($('.user_report_limit').val());
                $.extorio_showFullPageLoader();
                $.extorio_api({
                    endpoint: "/users/report_download",
                    data: {
                        query: $('#user_report_query_outer').extorio_reports__toObject(),
                        limit: 0,
                        skip: 0
                    },
                    oncomplete: function() {
                        $.extorio_hideFullPageLoader();
                    },
                    onsuccess: function(resp) {
                        $.extorio_dialog({
                            title: "Your download is ready",
                            content: '<p>Your download is ready. To download the file, click the button below.</p>' +
                            '<a target="_blank" class="btn btn-default" href="'+resp.data+'"><span class="fa fa-download"></span> Download</a>'
                        });
                    }
                })
            });

            var generateReport = function() {
                var limit = parseInt($('.user_report_limit').val());
                $.extorio_showFullPageLoader();
                $.extorio_api({
                    endpoint: "/users/report",
                    data: {
                        query: $('#user_report_query_outer').extorio_reports__toObject(),
                        limit: limit,
                        skip: page * limit
                    },
                    oncomplete: function() {
                        $.extorio_hideFullPageLoader();
                    },
                    onsuccess: function(resp) {
                        var data = $('#user_report_data');
                        if(resp.data.length > 0) {
                            data.html('');
                            for(var i = 0; i < resp.data.length; i++) {
                                var user = resp.data[i];
                                data.append('<tr>');
                                for(var k in user) {
                                    data.append('<td>'+user[k]+'</td>');
                                }
                                data.append('</tr>');
                            }

                            var totalPages = Math.ceil(resp.countUnlimited / limit);
                            if(totalPages <= 0) {
                                totalPages = 1;
                            }

                            $('.user_report_page_cur').html((page + 1));
                            $('.user_report_page_tot').html(totalPages);
                            $('.user_report_result_tot').html(resp.countUnlimited);

                            if(page > 0) {
                                $('.user_report_page_prev').removeClass("disabled").removeAttr("disabled");
                            } else {
                                $('.user_report_page_prev').addClass("disabled").attr("disabled","disabled");
                            }

                            if((page + 1) < totalPages) {
                                $('.user_report_page_next').removeClass("disabled").removeAttr("disabled");
                            } else {
                                $('.user_report_page_next').addClass("disabled").attr("disabled","disabled");
                            }

                        } else {
                            $('.user_report_page_prev, .user_report_page_next').addClass("disabled").attr("disabled","disabled");
                            data.html('<tr><td colspan="'+fieldCount+'">No results found.</td></tr>');
                        }
                    }
                });
            }

            $('.user_report_limit').on("change", function() {
                page = 0;
                generateReport();
            });

            $('.user_report_page_prev').on("click", function() {
                page--;
                generateReport();
            });
            $('.user_report_page_next').on("click", function() {
                page++;
                generateReport();
            });

            $('.user_report_generate').on("click", function() {
                page = 0;
                generateReport();
            });

            $(function() {
                $('.submit, .submit_exit').on("click", function() {
                    $('#query').val(JSON.stringify($('#user_report_query_outer').extorio_reports__toObject()));
                    if($(this).hasClass("submit_exit")) {
                        $('#fake_button').attr("name","submitted_exit");
                    } else {
                        $('#fake_button').attr("name","submitted");
                    }
                    $('#user_report_form').submit();
                });

                <?php
                if($this->report) {
                    ?>$('#user_report_query_outer').extorio_reports_insert(<?=json_encode($this->report->query)?>);<?php
                } else {
                    ?>$('#user_report_query_outer').extorio_reports_insert([{"$and":[]}]);<?php
                }
                ?>
            });
        </script>
        <?php
    }

    public function delete($id = false) {

    }
}