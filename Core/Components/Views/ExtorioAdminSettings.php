<?php
namespace Core\Components\Views;
class ExtorioAdminSettings extends \Core\Components\Controllers\ExtorioAdminSettings {
    public function _onBegin() {

    }

    public function _onDefault() {
        ?>
        <form method="post" action="">

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Application</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Website</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="application_name">Site name</label>
                                        <input value="<?=$this->config["application"]["name"]?>" type="text" class="form-control" id="application_name" name="application_name" placeholder="Enter a site name">
                                    </div>
                                    <div class="form-group">
                                        <label for="application_description">Site description</label>
                                        <textarea class="form-control" id="application_description" name="application_description" placeholder="Enter a description"><?=$this->config["application"]["description"]?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="application_company">Company name</label>
                                        <input value="<?=$this->config["application"]["company"]?>" type="text" class="form-control" id="application_company" name="application_company" placeholder="Enter your company name">
                                    </div>
                                    <div class="form-group">
                                        <label for="application_address">Site address</label>
                                        <input value="<?=$this->config["application"]["address"]?>" type="text" class="form-control" id="application_address" name="application_address" placeholder="Enter a site address">
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="application_seo" name="application_seo" <?php
                                            if($this->config["application"]["seo_visible"]) echo 'checked="checked"';
                                            ?> type="checkbox"> Site visible to SEO crawlers
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="application_maintenance" name="application_maintenance" <?php
                                            if($this->config["application"]["maintenance_mode"]) echo 'checked="checked"';
                                            ?> type="checkbox"> Site in maintenance mode
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="application_favicon">Favicon</label>
                                        <div class="input-group">
                                            <input value="<?=$this->config["application"]["favicon"]?>" type="text" class="form-control" id="application_favicon" name="application_favicon">
                                            <span class="input-group-btn">
                                                <button id="favicon_select" class="btn btn-primary" type="button">Select an image...</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Authentication</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="alert alert-info">
                                        Modifying the authentication settings will cause all logged in users to have to log in again
                                    </div>

                                    <div class="form-group">
                                        <label for="task_max">Session type</label>
                                        <select class="form-control" id="extorio_authentication_session_type" name="extorio_authentication_session_type">
                                            <option <?php
                                            if($this->config["extorio"]["authentication"]["session_type"] == "basic") echo 'selected="selected"';
                                            ?> value="basic">basic (less secure)</option>
                                            <option <?php
                                            if($this->config["extorio"]["authentication"]["session_type"] == "jwt") echo 'selected="selected"';
                                            ?> value="jwt">jwt (more secure)</option>
                                        </select>
                                    </div>

                                    <div id="jwt_settings" style="<?php
                                    if($this->config["extorio"]["authentication"]["session_type"] != "jwt") echo 'display:none;';
                                    ?> margin-bottom: 0;"  class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">JWT Settings</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="extorio_authentication_jwt_key">Secret key</label>
                                                <input value="<?=$this->config["extorio"]["authentication"]["jwt"]["key"]?>" type="text" class="form-control" id="extorio_authentication_jwt_key" name="extorio_authentication_jwt_key" placeholder="JWT Secret Key">
                                            </div>
                                            <div class="form-group">
                                                <label for="task_max">Encryption type</label>
                                                <select class="form-control" id="extorio_authentication_jwt_alg" name="extorio_authentication_jwt_alg">
                                                    <option <?php
                                                    if($this->config["extorio"]["authentication"]["jwt"]["alg"] == "HS256") echo 'selected="selected"';
                                                    ?> value="HS256">HS256 (recommended)</option>
                                                    <option <?php
                                                    if($this->config["extorio"]["authentication"]["jwt"]["alg"] == "HS384") echo 'selected="selected"';
                                                    ?> value="HS384">HS384</option>
                                                    <option <?php
                                                    if($this->config["extorio"]["authentication"]["jwt"]["alg"] == "HS512") echo 'selected="selected"';
                                                    ?> value="HS512">HS512</option>
                                                    <option <?php
                                                    if($this->config["extorio"]["authentication"]["jwt"]["alg"] == "RS256") echo 'selected="selected"';
                                                    ?> value="RS256">RS256</option>
                                                    <option <?php
                                                    if($this->config["extorio"]["authentication"]["jwt"]["alg"] == "RS384") echo 'selected="selected"';
                                                    ?> value="RS384">RS384</option>
                                                    <option <?php
                                                    if($this->config["extorio"]["authentication"]["jwt"]["alg"] == "RS512") echo 'selected="selected"';
                                                    ?> value="RS512">RS512</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        $(function() {
                                            $('#favicon_select').on("click", function() {
                                                $.extorio_dialog_assetManager({
                                                    title: "Select an image",
                                                    allowextensions: false,
                                                    displaytypes: "images",
                                                    selectable: true,
                                                    onfileselected: function(url) {
                                                        $('#application_favicon').val(url);
                                                    }
                                                });
                                            });

                                            $('#extorio_authentication_session_type').on("change", function() {
                                                if($(this).val() == "jwt") {
                                                    $('#jwt_settings').fadeIn();
                                                } else {
                                                    $('#jwt_settings').fadeOut();
                                                }
                                            });
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Extorio</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tasks</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="task_max">Maximum number of tasks</label>
                                        <input value="<?=$this->config["tasks"]["max_running_tasks"]?>" type="number" class="form-control" id="task_max" name="task_max" placeholder="Max running tasks">
                                    </div>
                                    <div class="form-group">
                                        <label for="task_max">Clean up tasks after how many days?</label>
                                        <input value="<?=$this->config["tasks"]["clean_up_tasks_after_days"]?>" type="number" class="form-control" id="task_days" name="task_days" placeholder="Clean up tasks after days">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Models</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="models_cache">Maximum number of entries to store in the modification cache</label>
                                        <input value="<?=$this->config["models"]["max_modcache_entries_per_instance"]?>" type="number" class="form-control" id="models_cache" name="models_cache" placeholder="Max modcache entries">
                                    </div>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Webhooks</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="checkbox">
                                                <label>
                                                    <input id="models_webhook_autofire" name="models_webhook_autofire" <?php
                                                    if($this->config["models"]["webhooks"]["autofire"]) echo 'checked="checked"';
                                                    ?> type="checkbox"> Modifications to model instances automatically fire webhooks
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="models_default_verification_key">Default webhook verification key</label>
                                                <input value="<?=$this->config["models"]["webhooks"]["default_verification_key"]?>" type="text" class="form-control" id="models_default_verification_key" name="models_default_verification_key" placeholder="Default verification key">
                                            </div>
                                            <div class="form-group">
                                                <label for="models_webhook_max_retries">Number of times to retry sending a webhook that fails</label>
                                                <input value="<?=$this->config["models"]["webhooks"]["max_retries"]?>" type="number" class="form-control" id="models_webhook_max_retries" name="models_webhook_max_retries" placeholder="Max retries">
                                            </div>
                                            <div class="form-group">
                                                <label for="models_webhook_retry_delay">Delay between retries</label>
                                                <input value="<?=$this->config["models"]["webhooks"]["retry_delay"]?>" type="number" class="form-control" id="models_webhook_retry_delay" name="models_webhook_retry_delay" placeholder="Retry delay">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" name="settings_updated" class="btn btn-primary">Update</button>
        </form>
        <?php
    }

    public function _onEnd() {

    }
}