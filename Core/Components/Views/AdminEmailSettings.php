<?php
namespace Core\Components\Views;
/**
 * Manage the settings related to emails
 *
 * Class AdminEmailSettings
 */
class AdminEmailSettings extends \Core\Components\Controllers\AdminEmailSettings {
    public function _onDefault() {
        ?>
        <form method="post" action="">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Defaults</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="default_from_name">Default from name</label>
                                <p class="help-block">This is the default name that emails will be sent from</p>
                                <input value="<?=$this->config["emails"]["defaults"]["from_name"]?>" type="text" class="form-control" id="default_from_name" name="default_from_name" placeholder="Default from name">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="default_from_email">Default from email</label>
                                <p class="help-block">This is the default email that emails will be sent from</p>
                                <input value="<?=$this->config["emails"]["defaults"]["from_email"]?>" type="text" class="form-control" id="default_from_email" name="default_from_email" placeholder="Default from email">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Transport settings</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="transport">Transport type</label>
                        <p class="help-block">The transport type allows you to choose how the server sends emails. You should select the type best suited to your server configuration. If you are unsure, select "mail".</p>
                        <select class="form-control" name="transport" id="transport">
                            <option <?php
                            if($this->config["emails"]["transport"] == "mail") echo 'selected="selected"';
                            ?> value="mail">mail (recommended)</option>
                            <option <?php
                            if($this->config["emails"]["transport"] == "sendmail") echo 'selected="selected"';
                            ?> value="sendmail">sendmail</option>
                            <option <?php
                            if($this->config["emails"]["transport"] == "smtp") echo 'selected="selected"';
                            ?> value="smtp">smtp</option>
                        </select>
                    </div>

                    <div style="<?php
                    if($this->config["emails"]["transport"] != "smtp") echo 'display: none;';
                    ?>" id="smtp_settings" class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">SMTP Settings</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="smtp_host">Host</label>
                                <input value="<?=$this->config["emails"]["smtp"]["host"]?>" type="text" class="form-control" id="smtp_host" name="smtp_host" placeholder="Host">
                            </div>
                            <div class="form-group">
                                <label for="smtp_port">Port</label>
                                <input value="<?=$this->config["emails"]["smtp"]["port"]?>" type="text" class="form-control" id="smtp_port" name="smtp_port" placeholder="Port">
                            </div>
                            <div class="form-group">
                                <label for="smtp_username">Username</label>
                                <input value="<?=$this->config["emails"]["smtp"]["username"]?>" type="text" class="form-control" id="smtp_username" name="smtp_username" placeholder="Username">
                            </div>
                            <div class="form-group">
                                <label for="smtp_password">Password</label>
                                <input value="<?=$this->config["emails"]["smtp"]["password"]?>" type="password" class="form-control" id="smtp_password" name="smtp_password" placeholder="Password">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" name="settings_updated" class="btn btn-primary">Update</button>
        </form>
        <script>
            $(function() {
                $('#transport').on("change",function() {
                    if($(this).val() == "smtp") {
                        $('#smtp_settings').fadeIn();
                    } else {
                        $('#smtp_settings').fadeOut();
                    }
                });
            });
        </script>
        <?php
    }
}