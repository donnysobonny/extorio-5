<?php
namespace Core\Components\Views;
use Core\Classes\Enums\InternalEvents;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

class ExtorioAdminUsers extends \Core\Components\Controllers\ExtorioAdminUsers {
    public function _onDefault() {
        ?>
        <div class="alert alert-info">
            <strong>Note</strong> that the list below only contains users that you are able to manage.
        </div>
        <?php
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Can login</th>
                    <th>Num login</th>
                    <th>Last Login</th>
                    <th>Last Active</th>
                    <th>User Group</th>
                    <?php
                    $actions = $this->_Extorio()->getEvent(InternalEvents::_user_list_columns)->run();
                    $customColumns = array();
                    foreach($actions as $action) {
                        if(is_array($action)) {
                            foreach($action as $header) {
                                $customColumns[] = $header;
                                ?><th><?=$header?></th><?php
                            }
                        } else {
                            $customColumns[] = $action;
                            ?><th><?=$action?></th><?php
                        }
                    }
                    ?>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($this->users) > 0) {
                    foreach($this->users as $user) {
                        ?>
                        <tr>
                            <td>
                                <?=$user->username?>
                            </td>
                            <td><?=$user->email?></td>
                            <td>
                                <?php
                                if($user->canLogin) {
                                    ?>
                                    <span class="fa fa-check"></span>
                                    <?php
                                } else {
                                    ?>
                                    <span class="fa fa-remove"></span>
                                    <?php
                                }
                                ?>
                            </td>
                            <td><?=$user->numLogin?></td>
                            <td><?=Strings::dateToTimeAgo($user->dateLogin)?></td>
                            <td><?=Strings::dateToTimeAgo($user->dateActive)?></td>
                            <td>
                                <span class="label label-primary"><?=$user->userGroup->name?></span>
                            </td>
                            <?php
                            $event = $this->_Extorio()->getEvent(InternalEvents::_user_list_cells);
                            foreach($customColumns as $column) {
                                $actions = $event->run(array($column,$user));
                                ?><td><?php
                                foreach($actions as $action) {
                                    echo $action;
                                }
                                ?></td><?php
                            }
                            ?>
                            <td>
                                <div class="btn-group" role="group" aria-label="">
                                    <a href="/extorio-admin/users/edit/<?=$user->id?>" type="button" class="btn btn-xs btn-default"><span class="fa fa-edit"></span> edit</a>

                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="fa fa-cog"></span>
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <?php
                                            if($user->canLogin) {
                                                ?><li><a data-id="<?=$user->id?>" class="send_account" href="javascript:;"><span class="fa fa-envelope"></span> send account email</a></li><?php
                                            } else {
                                                ?><li><a data-id="<?=$user->id?>" class="send_authentication" href="javascript:;"><span class="fa fa-envelope"></span> send authentication email</a></li><?php
                                            }
                                            ?>
                                            <li><a data-id="<?=$user->id?>" class="send_password" href="javascript:;"><span class="fa fa-envelope"></span> send password reset email</a></li>
                                            <li><a data-id="<?=$user->id?>" class="delete_user" href="javascript:;"><span class="fa fa-trash"></span> delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="<?=(7+count($customColumns))?>">No users found</td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $(function() {
                $('.delete_user').on("click", function() {
                    var self = $(this);
                    $.extorio_modal({
                        title: "Delete user?",
                        size: "modal-sm",
                        content: "Are you sure you want to delete this user?",
                        closetext: "cancel",
                        continuetext: "delete",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + self.attr("data-id");
                        }
                    });
                });
                $('.send_account').on("click", function() {
                    var id = $(this).attr('data-id');
                    $.extorio_modal({
                        title: "Send account email",
                        size: "modal-sm",
                        content: "Send this user an email containing their account details?",
                        closetext: "cancel",
                        continuetext: "send",
                        oncontinuebutton: function() {
                            $.extorio_showFullPageLoader();
                            $.extorio_api({
                                endpoint: "/users/" + id + "/send_account",
                                type: "POST",
                                oncomplete: function() {
                                    $.extorio_hideFullPageLoader();
                                },
                                onsuccess: function() {
                                    $.extorio_messageSuccess("Account email sent");
                                }
                            });
                        }
                    });
                });
                $('.send_authentication').on("click", function() {
                    var id = $(this).attr('data-id');
                    $.extorio_modal({
                        title: "Send authentication email",
                        size: "modal-sm",
                        content: "Send this user an email so that they can authenticate their?",
                        closetext: "cancel",
                        continuetext: "send",
                        oncontinuebutton: function() {
                            $.extorio_showFullPageLoader();
                            $.extorio_api({
                                endpoint: "/users/" + id + "/send_authentication",
                                type: "POST",
                                oncomplete: function() {
                                    $.extorio_hideFullPageLoader();
                                },
                                onsuccess: function() {
                                    $.extorio_messageSuccess("Authentication email sent");
                                }
                            });
                        }
                    });
                });
                $('.send_password').on("click", function() {
                    var id = $(this).attr('data-id');
                    $.extorio_modal({
                        title: "Send password reset email",
                        size: "modal-sm",
                        content: "Send this user an email so that they can reset their password?",
                        closetext: "cancel",
                        continuetext: "send",
                        oncontinuebutton: function() {
                            $.extorio_showFullPageLoader();
                            $.extorio_api({
                                endpoint: "/users/" + id + "/send_password_reset",
                                type: "POST",
                                oncomplete: function() {
                                    $.extorio_hideFullPageLoader();
                                },
                                onsuccess: function() {
                                    $.extorio_messageSuccess("Password reset email sent");
                                }
                            });
                        }
                    });
                });
            });
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function create() {
        $actions = $this->_Extorio()->getEvent(InternalEvents::_user_create_view)->run();
        foreach($actions as $action) {
            echo $action;
        }
    }

    public function edit($userId = false) {
        $this->_includeIncluder("tabcollapse");
        /** @var UserTab[] $tabs */
        $tabs = array();
        $actions = $this->_Extorio()->getEvent(InternalEvents::_user_edit_view_tabs)->run();
        foreach($actions as $action) {
            if(is_array($action)) {
                foreach($action as $a) {
                    $tabs[] = $a;
                }
            } else {
                $tabs[] = $action;
            }
        }
        ?>
        <ul id="user_edit_tabs" class="nav nav-tabs">
            <?php
            for($i = 0; $i < count($tabs); $i++) {
                ?><li class="<?php
                if($i == 0) echo 'active';
                ?>"><a href="#<?=$tabs[$i]->name?>" data-toggle="tab"><?php
                    if(strlen($tabs[$i]->icon)) {
                        ?><span class="fa fa-<?=$tabs[$i]->icon?>"></span> <?php
                    }
                    ?><?=$tabs[$i]->label?></a></li><?php
            }
            ?>
        </ul>
        <div id="user_edit_content" class="tab-content">
            <?php
            for($i = 0; $i < count($tabs); $i++) {
                ?>
                <div class="tab-pane fade<?php
                if($i == 0) echo ' in active';
                ?>" id="<?=$tabs[$i]->name?>">
                    <?php
                    $actions = $this->_Extorio()->getEvent(InternalEvents::_user_edit_view_content)->run(array($tabs[$i]->name,$this->user));
                    foreach($actions as $action) {
                        echo $action;
                    }
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
        <script>
            $(function() {
                $('#user_edit_tabs').tabCollapse();
            });
        </script>
        <?php
    }
}