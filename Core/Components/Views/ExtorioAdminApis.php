<?php
namespace Core\Components\Views;
use Core\Classes\Enums\FontAwesomeIcons;

class ExtorioAdminApis extends \Core\Components\Controllers\ExtorioAdminApis {
    public function _onBegin() {

    }

    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Label</th>
                    <th>All read</th>
                    <th>Read groups</th>
                    <th>Extension</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($this->apis) > 0) {
                    foreach($this->apis as $api) {
                        ?>
                        <tr>
                            <td>
                                <span title="<?=htmlentities($api->description)?>" data-toggle="tooltip" data-placement="right" style="cursor: help; border-bottom: 1px dashed;"><?=$api->name?></span>
                            </td>
                            <td><?=$api->label?></td>
                            <td><?php
                                if($api->allRead) {
                                    ?><span class="fa fa-check"></span><?php
                                } else {
                                    ?><span class="fa fa-remove"></span><?php
                                }
                                ?></td>
                            <td><?php
                                foreach($api->readGroups as $group) {
                                    ?><span class="label label-primary"><?=$group->name?></span>
                                    <?php
                                }
                                ?></td>
                            <td><?=$api->extensionName?></td>
                            <td>
                                <a class="btn btn-primary btn-xs" href="/extorio-admin/apis/edit/<?=$api->id?>"><span class="fa fa-edit"></span> edit</a>
                                <button data-id="<?=$api->id?>" class="btn btn-danger btn-xs delete_api"><span class="fa fa-trash"></span> delete</button>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="6">No apis found</td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $(function () {
                $('.delete_api').on("click", function() {
                    var self = $(this);
                    $.extorio_modal({
                        title: "Delete api?",
                        size: "modal-sm",
                        content: "Are you sure you want to delete this api?",
                        closetext: "cancel",
                        continuetext: "delete",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + self.attr("data-id");
                        }
                    });
                });

                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($apiId = false) {
        ?>
        <form method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->api->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter a name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description"><?=$this->api->description?></textarea>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Access</div>
                </div>
                <div class="panel-body">
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->api->allRead) echo 'checked="checked"';
                            ?> name="all_read" id="all_read" type="checkbox"> All users can access this api
                        </label>
                    </div>

                    <div id="user_group_select" style="<?php
                    if($this->api->allRead) echo 'display:none;';
                    ?>" class="form-group">
                        <label for="label">Select the user groups that can access this api</label>
                        <?php
                        foreach($this->groups as $group) {
                            ?>
                            <div class="checkbox">
                                <label>
                                    <input <?php
                                    if(in_array($group->id, $this->api->readGroups)) echo 'checked="checked"';
                                    ?> name="groups[]" value="<?=$group->id?>" type="checkbox"> <?=$group->name?> <small><?=$group->description?></small>
                                </label>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

            <?php
            if(!$this->api) {
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Advanced</div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="extension">Extension</label>
                            <select class="form-control" id="extension" name="extension">
                                <optgroup label="Internal">
                                    <option value="Application">Application</option>
                                    <option value="Core">Core</option>
                                </optgroup>
                                <?php
                                if(count($this->extensions)) {
                                    ?>
                                    <optgroup label="Installed">
                                        <?php
                                        foreach($this->extensions as $e) {
                                            if(!$e->_internal) {
                                                ?>
                                                <option value="<?=$e->_extensionName?>"><?=$e->_extensionName?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <button type="submit" name="api_updated" class="btn btn-primary"><span class="fa fa-<?=FontAwesomeIcons::_save?>"></span> Save</button>
            <button type="submit" name="api_updated_exit" class="btn btn-info"><span class="fa fa-<?=FontAwesomeIcons::_signOut?>"></span> Save and exit</button>
        </form>
        <script>
            $(function() {
                $('#all_read').on("change", function() {
                    if($(this).prop("checked")) {
                        $('#user_group_select').hide();
                    } else {
                        $('#user_group_select').show();
                    }
                });
            });
        </script>
    <?php
    }

    public function delete($apiId = false) {

    }
}