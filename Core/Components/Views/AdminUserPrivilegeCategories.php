<?php
namespace Core\Components\Views;
use Core\Classes\Enums\FontAwesomeIcons;

/**
 * Manage the user privilege categories
 *
 * Class AdminUserPrivilegeCategories
 */
class AdminUserPrivilegeCategories extends \Core\Components\Controllers\AdminUserPrivilegeCategories {
    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Icon</th>
                    <th>Extension</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($this->categories)) {
                    foreach($this->categories as $c) {
                        ?>
                <tr>
                    <td><?=$c->name?></td>
                    <td><?=$c->description?></td>
                    <td><span class="fa fa-<?=$c->icon?>"></span> (<?=$c->icon?>)</td>
                    <td><?=$c->extensionName?></td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="<?=$this->_getUrlToMethod("edit",array($c->id))?>"><span class="fa fa-edit"></span> edit</a>
                        <button data-id="<?=$c->id?>" class="btn btn-xs btn-danger delete_category"><span class="fa fa-trash"></span> delete</button>
                    </td>
                </tr>
                        <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="5">No categories found</td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $('.delete_category').on("click", function() {
                var self = $(this);
                $.extorio_modal({
                    title: "Delete user privilege category?",
                    size: "modal-sm",
                    content: "Are you sure you want to delete this user privilege category?",
                    closetext: "cancel",
                    continuetext: "delete",
                    oncontinuebutton: function() {
                        window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + self.attr("data-id");
                    }
                });
            });
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($id = false) {
        ?>
        <form method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->category->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter a name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description" placeholder="(optional) Enter a description"><?=$this->category->description?></textarea>
            </div>
            <div class="form-group">
                <label for="icon">Icon</label>
                <select class="form-control" name="icon" id="icon">
                    <?php
                    foreach(FontAwesomeIcons::values() as $icon) {
                        ?><option <?php
                        if($this->category->icon == $icon) echo 'selected="selected"';
                        ?> value="<?=$icon?>"><?=$icon?></option><?php
                    }
                    ?>
                </select>
            </div>

            <?php
            if(!$this->category) {
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Advanced</div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="extension">Extension</label>
                            <select class="form-control" id="extension" name="extension">
                                <optgroup label="Internal">
                                    <option value="Application">Application</option>
                                    <option value="Core">Core</option>
                                </optgroup>
                                <?php
                                if(count($this->extensions)) {
                                    ?>
                                    <optgroup label="Installed">
                                        <?php
                                        foreach($this->extensions as $e) {
                                            if(!$e->internal) {
                                                ?>
                                                <option value="<?=$e->extensionName?>"><?=$e->extensionName?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <button type="submit" name="submit" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
            <button type="submit" name="submit_exit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
        </form>
        <?php
    }

    public function delete($id = false) {

    }
}