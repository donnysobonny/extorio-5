<?php
namespace Core\Components\Views;
use Core\Classes\Enums\ModelWebhookActions;
use Core\Classes\Models\User;
use Core\Classes\Utilities\Models;
use Core\Classes\Utilities\Namespaces;

class ExtorioAdminModelWebhooks extends \Core\Components\Controllers\ExtorioAdminModelWebhooks {
    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-striped table-condensed">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Action</th>
                    <th>Model</th>
                    <th>Location</th>
                    <th>Verification Key</th>
                    <th>Extension</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($this->webhooks) > 0) {
                    foreach($this->webhooks as $webhook) {
                        ?>
                        <tr>
                            <td><?=$webhook->name?></td>
                            <td><?=$webhook->action?></td>
                            <td><?=Namespaces::getClassName($webhook->modelNamespace)?></td>
                            <td><?=$webhook->location?></td>
                            <td><?=$webhook->verificationKey?></td>
                            <td><?=$webhook->extensionName?></td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="<?=$this->_getUrlToMethod("edit",array($webhook->id))?>"><span class="fa fa-edit"></span> edit</a>
                                <button data-id="<?=$webhook->id?>" class="btn btn-xs btn-danger delete_webhook"><span class="fa fa-trash"></span> delete</button>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="7">No webhooks found</td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $(function() {
                $('.delete_webhook').on("click", function() {
                    var self = $(this);
                    $.extorio_modal({
                        title: "Delete webhook?",
                        size: "modal-sm",
                        content: "Are you sure you want to delete this webhook?",
                        closetext: "cancel",
                        continuetext: "delete",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + self.attr("data-id");
                        }
                    });
                });
            });
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($id = false) {
        ?>
        <form method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->webhook->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter a name">
            </div>
            <div class="form-group">
                <label for="action">Action</label>
                <select class="form-control" id="action" name="action">
                    <?php
                    foreach(ModelWebhookActions::values() as $value) {
                        ?>
                        <option <?php
                        if($this->webhook->action == $value) echo 'selected="selected"';
                        ?> value="<?=$value?>"><?=$value?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="model">Model</label>
                <select class="form-control" id="model" name="model">
                    <?php
                    foreach($this->models as $model) {
                        ?>
                        <option <?php
                        if($this->webhook->modelNamespace == $model->namespace) echo 'selected="selected"';
                        ?> value="<?=$model->namespace?>"><?=$model->label?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="location">Location</label>
                <input value="<?=$this->webhook->location?>" type="text" class="form-control" id="location" name="location" placeholder="Enter a location">
            </div>
            <div class="form-group">
                <label for="key">Verification key</label>
                <input value="<?=$this->webhook->verificationKey?>" type="text" class="form-control" id="key" name="key" placeholder="Enter a key (leave blank to use default)">
            </div>
            <?php
            if(!$this->webhook) {
                ?>
                <div class="form-group">
                    <label for="extension">Extension</label>
                    <select class="form-control" id="extension" name="extension">
                        <optgroup label="Internal">
                            <option value="Application">Application</option>
                            <option value="Core">Core</option>
                        </optgroup>
                        <?php
                        if(count($this->extensions)) {
                            ?>
                            <optgroup label="Installed">
                                <?php
                                foreach($this->extensions as $e) {
                                    if(!$e->_internal) {
                                        ?>
                                        <option value="<?=$e->_extensionName?>"><?=$e->_extensionName?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </optgroup>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <?php
            }
            ?>
            <button type="submit" name="webhook_edited" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
            <button type="submit" name="webhook_edited_exit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
        </form>
    <?php
    }

    public function delete($id = false) {

    }
}