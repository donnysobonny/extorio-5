<?php
namespace Core\Components\Views;
class ExtorioAdminTasks extends \Core\Components\Controllers\ExtorioAdminTasks {

    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Label</th>
                    <th>All read</th>
                    <th>Read groups</th>
                    <th>Extension</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($this->tasks) > 0) {
                    foreach($this->tasks as $task) {
                        ?>
                        <tr>
                            <td>
                                <span data-toggle="tooltip" data-placement="right" title="<?=htmlentities($task->description)?>" style="cursor: help; border-bottom: 1px dashed;"><?=$task->name?></span>
                            </td>
                            <td><?=$task->label?></td>
                            <td><?php
                                if($task->allRead) {
                                    ?><span class="fa fa-check"></span><?php
                                } else {
                                    ?><span class="fa fa-remove"></span><?php
                                }
                                ?></td>
                            <td><?php
                                foreach($task->readGroups as $group) {
                                    ?><span class="label label-primary"><?=$group->name?></span>
                                    <?php
                                }
                                ?></td>
                            <td><?=$task->extensionName?></td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="/extorio-admin/tasks/edit/<?=$task->id?>"><span class="fa fa-edit"></span> edit</a>
                                <button data-id="<?=$task->id?>" class="btn btn-xs btn-danger delete_task"><span class="fa fa-trash"></span> delete</button>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="6">No tasks found</td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $(function () {
                $('.delete_task').on("click", function() {
                    var self = $(this);
                    $.extorio_modal({
                        title: "Delete task?",
                        size: "modal-sm",
                        content: "Are you sure you want to delete this task?",
                        closetext: "cancel",
                        continuetext: "delete",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + self.attr("data-id");
                        }
                    });
                });

                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($taskId=false) {
        ?>
        <form method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->task->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter a name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description" placeholder="Enter a description"><?=$this->task->description?></textarea>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Access</div>
                </div>
                <div class="panel-body">
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->task->allRead) echo 'checked="checked"';
                            ?> name="all_read" id="all_read" type="checkbox"> All users can access this task
                        </label>
                    </div>

                    <div id="user_group_select" style="<?php
                    if($this->task->allRead) echo 'display:none;';
                    ?>" class="form-group">
                        <label for="label">Select the user groups that can access this task</label>
                        <?php
                        foreach($this->groups as $group) {
                            ?>
                            <div class="checkbox">
                                <label>
                                    <input <?php
                                    if(in_array($group->id, $this->task->readGroups)) echo 'checked="checked"';
                                    ?> name="groups[]" value="<?=$group->id?>" type="checkbox"> <?=$group->name?> <small><?=$group->description?></small>
                                </label>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

            <?php
            if(!$this->task) {
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Advanced</div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="extension">Extension</label>
                            <select class="form-control" id="extension" name="extension">
                                <optgroup label="Internal">
                                    <option value="Application">Application</option>
                                    <option value="Core">Core</option>
                                </optgroup>
                                <?php
                                if(count($this->extensions)) {
                                    ?>
                                    <optgroup label="Installed">
                                        <?php
                                        foreach($this->extensions as $e) {
                                            if(!$e->internal) {
                                                ?>
                                                <option value="<?=$e->extensionName?>"><?=$e->extensionName?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
            
            <button type="submit" name="task_updated" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
            <button type="submit" name="task_updated_exit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
        </form>
        <script>
            $(function() {
                $('#all_read').on("change", function() {
                    if($(this).prop("checked")) {
                        $('#user_group_select').hide();
                    } else {
                        $('#user_group_select').show();
                    }
                });
            });
        </script>
        <?php
    }
}