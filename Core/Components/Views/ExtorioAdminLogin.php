<?php
namespace Core\Components\Views;
class ExtorioAdminLogin extends \Core\Components\Controllers\ExtorioAdminLogin {
    public function _onBegin() {
        ?>
        <div class="page-header">
            <h1>Extorio <small>admin login</small></h1>
        </div>
        <form method="post" action="">
            <div class="form-group">
                <label for="username">Username/Email</label>
                <input type="text" class="form-control" id="username" name="username" placeholder="Username/email">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
            </div>
            <div class="checkbox">
                <label>
                    <input name="keeploggedin" id="keeploggedin" type="checkbox"> Keep me logged in (for 30 days)
                </label>
            </div>
            <button name="login_submit" type="submit" class="btn btn-primary btn-block">Sign in</button>
        </form>
        <hr />
        <?php
    }

    public function _onDefault() {

    }

    public function _onEnd() {

    }

    protected function onStart() {

    }

}