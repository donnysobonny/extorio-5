<?php
namespace Core\Components\Views;
/**
 * 
 *
 * Class UserAuthentication
 */
class UserAuthentication extends \Core\Components\Controllers\UserAuthentication {
    public function _onDefault() {
        if($this->user) {
            ?>
<div class="alert alert-success">
    You have successfully authenticated your account and may now log in.
</div>
            <?php
        } else {
            ?>
<div class="alert alert-warning">
    There was a problem authenticating your account. Please try again.
</div>
            <?php
        }
    }
}