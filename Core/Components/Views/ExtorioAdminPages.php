<?php
namespace Core\Components\Views;
use Core\Classes\Helpers\LayoutSystem;

class ExtorioAdminPages extends \Core\Components\Controllers\ExtorioAdminPages {
    public function _onBegin() {

    }

    public function _onDefault() {
        $this->filtering->displayFiltering();
        $this->filtering->displaySearching();
        ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Address</th>
                <th>Public</th>
                <th>Default</th>
                <th>All read</th>
                <th>Read groups</th>
                <th>Modify groups</th>
                <th>Extension</th>
                <th><span class="fa fa-cog"></span></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(count($this->pages)) {
                foreach($this->pages as $page) {
                    ?>
                    <tr>
                        <td>
                            <span style="cursor: help; border-bottom: 1px dashed;" data-toggle="tooltip" data-placement="right" title="<?=htmlentities($page->description,ENT_COMPAT)?>"><?=$page->name?></span>
                        </td>
                        <td><?=$page->address?></td>
                        <td>
                            <?php
                            if($page->isPublic) {
                                ?>
                                <span class="fa fa-check"></span>
                                <?php
                            } else {
                                ?>
                                <span class="fa fa-remove"></span>
                                <?php
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if($page->default) {
                                ?>
                                <span class="fa fa-check"></span>
                                <?php
                            } else {
                                ?>
                                <span class="fa fa-remove"></span>
                                <?php
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if($page->allRead) {
                                ?>
                                <span class="fa fa-check"></span>
                                <?php
                            } else {
                                ?>
                                <span class="fa fa-remove"></span>
                                <?php
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            foreach($page->readGroups as $group) {
                                ?><span class="label label-primary"><?=$group->name?></span>
                                <?php
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            foreach($page->modifyGroups as $group) {
                                ?><span class="label label-primary"><?=$group->name?></span>
                                <?php
                            }
                            ?>
                        </td>
                        <td>
                            <?=$page->extensionName?>
                        </td>
                        <td>
                            <a class="btn btn-primary btn-xs" href="<?=$this->_getUrlToMethod("edit",array($page->id))?>"><span class="fa fa-pencil"></span> edit</a>
                            <button data-id="<?=$page->id?>" class="btn btn-danger btn-xs delete_page"><span class="fa fa-trash"></span> delete</button>
                        </td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="9">No pages found</td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <?php
        $this->filtering->displayPagination();
        ?>
        <script>
            $(function () {
                $('.delete_page').on("click", function() {
                    var self = $(this);
                    $.extorio_modal({
                        title: "Delete page?",
                        size: "modal-sm",
                        content: "Are you sure you want to delete this page?",
                        closetext: "cancel",
                        continuetext: "delete",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + self.attr("data-id");
                        }
                    });
                });

                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <?php
    }

    public function edit($pageId = false) {
        $this->_Extorio()->appendToBody('
        <link href="/Core/Assets/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
        <script type="text/javascript" src="/Core/Assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        ');
        ?>
        <form id="edit_form" method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->page->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter a name (used in page headers)">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description"><?=$this->page->description?></textarea>
            </div>
            <div class="form-group">
                <label for="address">Address</label>
                <input value="<?=$this->page->address?>" type="text" class="form-control" id="address" name="address" placeholder="Enter an address">
            </div>
            <div class="checkbox">
                <label>
                    <input <?php
                    $isPublic = $this->page ? $this->page->isPublic : true;
                    if($isPublic) {
                        echo 'checked="checked"';
                    }
                    ?> name="public" id="public" type="checkbox"> Page is public
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input <?php
                    if($this->page->default) {
                        echo 'checked="checked"';
                    }
                    ?> name="default" id="default" type="checkbox"> Page is default
                </label>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="theme">Theme</label>
                        <select id="theme" name="theme" class="form-control">
                            <option value="">SYSTEM DEFAULT</option>
                            <?php
                            foreach($this->themes as $theme) {
                                ?>
                                <option <?php
                                if($this->page->theme == $theme->id) {
                                    echo 'selected="selected"';
                                }
                                ?> value="<?=$theme->id?>"><?=$theme->name?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="template">Template</label>
                        <select id="template" name="template" class="form-control">
                            <option value="">SYSTEM DEFAULT</option>
                            <?php
                            foreach($this->templates as $template) {
                                ?>
                                <option <?php
                                if($this->page->template == $template->id) {
                                    echo 'selected="selected"';
                                }
                                ?> value="<?=$template->id?>"><?=$template->name?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>



            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Access</div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">View</h3>
                                </div>
                                <?php
                                $allRead = $this->page ? $this->page->allRead : true;
                                ?>
                                <div class="panel-body">
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($allRead) echo 'checked="checked"';
                                            ?> id="all_view" name="all_view" type="checkbox"> All users can view this page
                                        </label>
                                    </div>
                                    <div style="<?php
                                    if($allRead) echo 'display: none;';
                                    ?>" id="view_user_roles" id="view_user_roles">
                                        <div class="form-group">
                                            <label>Select the user roles that can view this page</label>
                                            <?php
                                            foreach($this->groups as $group) {
                                                ?>
                                                <div class="checkbox">
                                                    <label>
                                                        <input <?php
                                                        if(in_array($group->id,$this->page->readGroups)) echo 'checked="checked"';
                                                        ?> name="read_groups[]" value="<?=$group->id?>" type="checkbox"> <?=$group->name?> <small><?=$group->description?></small>
                                                    </label>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="read_fail_page_id">Select the page to redirect to if viewing access fails</label>
                                            <select class="form-control" name="read_fail_page_id" id="read_fail_page_id">
                                                <?php
                                                $id = $this->page->readFailPageId?:21;
                                                foreach($this->pages as $page) {
                                                    ?><option <?php
                                                    if($id == $page->id) echo 'selected="selected"';
                                                    ?> value="<?=$page->id?>"><?=$page->name?></option><?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Modify</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>Select the user roles that can modify this page</label>
                                        <?php
                                        $modGroups = $this->page ? $this->page->modifyGroups : array(1,2);
                                        foreach($this->groups as $group) {
                                            ?>
                                            <div class="checkbox">
                                                <label>
                                                    <input <?php
                                                    if(in_array($group->id,$modGroups)) echo 'checked="checked"';
                                                    ?> name="modify_groups[]" value="<?=$group->id?>" type="checkbox"> <?=$group->name?> <small><?=$group->description?></small>
                                                </label>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Advanced</div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">Publish date (optional)</label>
                                <input value="<?php
                                if($this->page->publishDate != "1970-01-01") echo $this->page->publishDate;
                                ?>" type="text" class="form-control datepicker" id="publish" name="publish" placeholder="Enter a date (or leave blank)">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">Unpublish date (optional)</label>
                                <input value="<?php
                                if($this->page->unpublishDate != "1970-01-01") echo $this->page->unpublishDate;
                                ?>" type="text" class="form-control datepicker" id="unpublish" name="unpublish" placeholder="Enter a date (or leave blank)">
                            </div>
                        </div>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->page->isHidden) echo 'checked="checked"';
                            ?> name="hidden" id="hidden" type="checkbox"> Page is hidden from page management
                        </label>
                    </div>
                    <?php
                    if(!$this->page) {
                        ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="extension">Extension</label>
                                    <select class="form-control" id="extension" name="extension">
                                        <optgroup label="Internal">
                                            <option value="Application">Application</option>
                                            <option value="Core">Core</option>
                                        </optgroup>
                                        <?php
                                        if(count($this->extensions)) {
                                            ?>
                                            <optgroup label="Installed">
                                                <?php
                                                foreach($this->extensions as $e) {
                                                    if(!$e->_internal) {
                                                        ?>
                                                        <option value="<?=$e->_extensionName?>"><?=$e->_extensionName?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </optgroup>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="prependtohead">Prepend to head</label>
                                <textarea class="form-control" id="prependtohead" name="prependtohead"><?=$this->page->prependToHead?></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="appendtohead">Append to head</label>
                                <textarea class="form-control" id="appendtohead" name="appendtohead"><?=$this->page->appendToHead?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="prependtobody">Prepend to body</label>
                                <textarea class="form-control" id="prependtobody" name="prependtobody"><?=$this->page->prependToBody?></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="appendtobody">Append to body</label>
                                <textarea class="form-control" id="appendtobody" name="appendtobody"><?=$this->page->appendToBody?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input id="page_updated_hidden" type="hidden" name="page_updated" value="submitted">

            <button id="page_updated" name="page_updated" type="submit" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
            <button id="page_updated_exit" name="page_updated_exit" type="submit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
            <?php
            if($this->page) {
                ?>
                <a class="btn btn-default" target="_blank" href="<?=$this->page->address?>"><span class="fa fa-eye"></span> Preview</a>
                <?php
            }
            ?>
        </form>
        <script>
            $(function() {
                $('#all_view').on("change", function() {
                    if($(this).prop("checked")) {
                        $('#view_user_roles').hide();
                    } else {
                        $('#view_user_roles').show();
                    }
                });

                $('.datepicker').datepicker({format: 'yyyy-mm-dd'});
            });
        </script>
    <?php
    }
}