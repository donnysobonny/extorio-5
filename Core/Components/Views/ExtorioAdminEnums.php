<?php
namespace Core\Components\Views;
class ExtorioAdminEnums extends \Core\Components\Controllers\ExtorioAdminEnums {
    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-striped table-condensed">
            <thead>
            <tr>
                <th>Name</th>
                <th>Label</th>
                <th>Extension</th>
                <th>Additional</th>
                <th><span class="fa fa-cog"></span></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(count($this->enums) > 0) {
                foreach($this->enums as $enum) {
                    ?>
                    <tr>
                        <td>
                            <span style="cursor: help; border-bottom: 1px dashed;" data-toggle="tooltip" data-placement="right" title="<?=htmlentities($enum->description,ENT_COMPAT)?>"><?=$enum->name?></span>
                        </td>
                        <td><?=$enum->label?></td>
                        <td><?=$enum->extensionName?></td>
                        <td>
                            <a class="btn btn-xs btn-default" href="<?=$this->_getUrlToMethod("values",array($enum->id))?>"><span class="fa fa-edit"></span> manage values</a>
                        </td>
                        <td>
                            <a class="btn btn-xs btn-primary" href="<?=$this->_getUrlToMethod("edit",array($enum->id))?>"><span class="fa fa-edit"></span> edit</a>
                            <button data-id="<?=$enum->id?>" class="btn btn-xs btn-danger delete_enum"><span class="fa fa-trash"></span> delete</button>
                        </td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="5">No enums found</td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <script>
            $(function () {
                $('.delete_enum').on("click", function() {
                    var self = $(this);
                    $.extorio_modal({
                        title: "Delete enum?",
                        size: "modal-sm",
                        content: "Are you sure you want to delete this enum?",
                        closetext: "cancel",
                        continuetext: "delete",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + self.attr("data-id");
                        }
                    });
                });

                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($enumId = false) {
        ?>
        <form method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->enum->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter a name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description" placeholder="Enter a description"><?=$this->enum->description?></textarea>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Advanced</div>
                </div>
                <div class="panel-body">
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->enum->isHidden) echo 'checked="checked"';
                            ?> name="hidden" id="hidden" type="checkbox"> Enum is hidden from enum management
                        </label>
                    </div>
                    <?php
                    if(!$this->enum) {
                        ?>
                        <div class="form-group">
                            <label for="extension">Extension</label>
                            <select class="form-control" id="extension" name="extension">
                                <optgroup label="Internal">
                                    <option value="Application">Application</option>
                                    <option value="Core">Core</option>
                                </optgroup>
                                <?php
                                if(count($this->extensions)) {
                                    ?>
                                    <optgroup label="Installed">
                                        <?php
                                        foreach($this->extensions as $e) {
                                            if(!$e->_internal) {
                                                ?>
                                                <option value="<?=$e->_extensionName?>"><?=$e->_extensionName?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>

            <button type="submit" name="enum_edited" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
            <button type="submit" name="enum_edited_exit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
        </form>
        <?php
    }

    public function values($enumId = false) {
        ?>
        <div class="form-inline">
            <div class="form-group">
                <label for="value">Value</label>
                <input type="text" class="form-control" id="value" name="value" placeholder="Value">
            </div>
            <button role="button" class="btn btn-primary element_move_add"><span class="fa fa-plus"></span> Add new value</button>
        </div>
        <br />
        <table id="element_table" class="table table-striped">
            <thead>
            <tr>
                <th>
                    Value
                </th>
                <th>
                    Move
                </th>
                <th><span class="fa fa-cog"></span></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if($this->enum->values) {
                foreach($this->enum->values as $value) {
                    ?>
                    <tr>
                        <td class="element_value"><?=$value?></td>
                        <td>
                            <button class="btn btn-xs btn-primary element_move_up" ><span class="fa fa-chevron-up"></span> move up</button>
                            <button class="btn btn-xs btn-primary element_move_down" ><span class="fa fa-chevron-down"></span> move down</button>
                        </td>
                        <td>
                            <button class="btn btn-xs btn-danger element_move_delete" ><span class="fa fa-trash"></span> delete</button>
                        </td>
                    </tr>
                <?PHP
                }
            }
            ?>
            </tbody>
        </table>
        <script>
            $(function() {
                var enumId = <?=$this->enum->id?>;

                var updateEnum = function(displayMessage) {
                    $.extorio_showFullPageLoader();
                    var values = [];
                    $('.element_value').each(function() {
                        values.push($(this).text());
                    });
                    $.extorio_api({
                        endpoint: "/enums/" + enumId + "/values",
                        type: "POST",
                        data: {
                            values: values
                        },
                        oncomplete: function() {
                            $.extorio_hideFullPageLoader();
                        },
                        onsuccess: function(r) {
                            if(displayMessage) {
                                $.extorio_messageSuccess("Enum updated");
                            }
                        }
                    });
                };

                var updateTable = function() {
                    var prev,next;
                    $('#element_table tbody tr').each(function() {
                        //if has prev
                        prev = $(this).prev();
                        if(prev.length > 0) {
                            $(this).find('.element_move_up').removeClass('disabled');
                        } else {
                            $(this).find('.element_move_up').addClass('disabled');
                        }
                        //if has next
                        next = $(this).next();
                        if(next.length > 0) {
                            $(this).find('.element_move_down').removeClass('disabled');
                        } else {
                            $(this).find('.element_move_down').addClass('disabled');
                        }
                    });
                };
                updateTable();

                $('#element_table tbody tr').each(function() {
                    var self = $(this);
                    $(this).find('.element_move_up').on("click", function() {
                        self.insertBefore(self.prev());
                        updateTable();
                        updateEnum(false);
                    });
                    $(this).find('.element_move_down').on("click", function() {
                        self.insertAfter(self.next());
                        updateTable();
                        updateEnum(false);
                    });
                    $(this).find('.element_move_delete').on("click", function() {
                        self.remove();
                        updateTable();
                        updateEnum(true);
                    });
                });

                $('.element_move_add').on("click", function() {
                    var value = $('#value').val();
                    if(value.length > 0) {
                        var row = $('' +
                        '<tr>' +
                        '   <td class="element_value">'+$('#value').val()+'</td>' +
                        '   <td>' +
                        '       <button class="btn btn-xs btn-primary element_move_up" ><span class="fa fa-chevron-up"></span> move up</button>' +
                        '       <button class="btn btn-xs btn-primary element_move_down" ><span class="fa fa-chevron-down"></span> move down</button>' +
                        '   </td>' +
                        '   <td>' +
                        '       <button class="btn btn-xs btn-danger element_move_delete" ><span class="fa fa-trash"></span> delete</button>' +
                        '   </td>' +
                        '</tr>')
                        row.find('.element_move_up').on("click", function() {
                            row.insertBefore(row.prev());
                            updateTable();
                            updateEnum(false);
                        });
                        row.find('.element_move_down').on("click", function() {
                            row.insertAfter(row.next());
                            updateTable();
                            updateEnum(false);
                        });
                        row.find('.element_move_delete').on("click", function() {
                            row.remove();
                            updateTable();
                            updateEnum(true);
                        });
                        $('#element_table tbody').append(row);
                        updateTable();
                        updateEnum(true);
                        $('#value').val('');
                        $('#value').focus();
                    }
                });
            });
        </script>
        <?php
    }


}