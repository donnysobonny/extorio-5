<?php
namespace Core\Components\Views;
/**
 * Manage the email templates
 *
 * Class AdminEmailTemplates
 */
class AdminEmailTemplates extends \Core\Components\Controllers\AdminEmailTemplates {
    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Subject</th>
            <th>Description</th>
            <th>Extension</th>
            <th><span class="fa fa-cog"></span></th>
        </tr>
    </thead>
    <tbody>
        <?php
        if(count($this->templates) > 0) {
            foreach($this->templates as $t) {
                ?>
                <tr>
                    <td><?=$t->name?></td>
                    <td><?=$t->subject?></td>
                    <td><?=$t->description?></td>
                    <td><?=$t->extensionName?></td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="<?=$this->_getUrlToMethod("edit",array($t->id))?>"><span class="fa fa-edit"></span> edit</a>
                        <button data-id="<?=$t->id?>" class="btn btn-xs btn-danger delete_template"><span class="fa fa-trash"></span> delete</button>
                    </td>
                </tr>
                <?php
            }
        } else {
            ?>
        <tr>
            <td colspan="5">No templates found</td>
        </tr>
            <?php
        }
        ?>
    </tbody>
</table>
        <script>
            $(function() {
                $('.delete_template').on("click", function() {
                    var self = $(this);
                    $.extorio_modal({
                        title: "Delete email template?",
                        size: "modal-sm",
                        content: "Are you sure you want to delete this email template?",
                        closetext: "cancel",
                        continuetext: "delete",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + self.attr("data-id");
                        }
                    });
                });
            });
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($id = false) {
        $this->_includeIncluder("extorio_editable");
        ?>
        <form method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->template->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter a name">
            </div>
            <div class="form-group">
                <label for="subject">Subject</label>
                <input value="<?=$this->template->subject?>" type="text" class="form-control" id="subject" name="subject" placeholder="Enter a subject">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description" placeholder="Enter a description"><?=$this->template->description?></textarea>
            </div>
            <hr />
            <div class="form-group">
                <label for="body_content">Body</label>
                <textarea id="body_content" name="body_content"><?=$this->template->body?></textarea>
            </div>

            <?php
            if(!$this->template) {
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Advanced</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="extension">Extension</label>
                            <select class="form-control" id="extension" name="extension">
                                <optgroup label="Internal">
                                    <option value="Application">Application</option>
                                    <option value="Core">Core</option>
                                </optgroup>
                                <?php
                                if(count($this->extensions)) {
                                    ?>
                                    <optgroup label="Installed">
                                        <?php
                                        foreach($this->extensions as $e) {
                                            if(!$e->_internal) {
                                                ?>
                                                <option value="<?=$e->_extensionName?>"><?=$e->_extensionName?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <button type="submit" name="template_updated" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
            <button type="submit" name="template_updated_exit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
        </form>
        <script>
            $(function() {
                $('#body_content').extorio_editable();
            });
        </script>
    <?php
    }

    public function delete($id = false) {

    }
}