<?php
namespace Core\Components\Views;
class ExtorioAdminThemes extends \Core\Components\Controllers\ExtorioAdminThemes {

    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Default</th>
                    <th>Extension</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($this->themes) > 0) {
                    foreach($this->themes as $theme) {
                        ?>
                        <tr>
                            <td><?=$theme->name?></td>
                            <td><?=$theme->description?></td>
                            <td><?php
                                if($theme->default) {
                                    ?>
                                    <span class="fa fa-check"></span>
                                    <?php
                                } else {
                                    ?>
                                    <span class="fa fa-remove"></span>
                                    <?php
                                }
                                ?></td>
                            <td><?=$theme->extensionName?></td>
                            <td>
                                <a class="btn btn-primary btn-xs" href="<?=$this->_getUrlToMethod("edit",array($theme->id))?>"><span class="fa fa-pencil"></span> edit</a>
                                <a class="btn btn-primary btn-xs" href="<?=$this->_getUrlToMethod("compile",array($theme->id))?>"><span class="fa fa-save"></span> re-compile</a>
                                <button data-id="<?=$theme->id?>" class="btn btn-danger btn-xs delete_theme"><span class="fa fa-trash"></span> delete</button>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="5">No themes found</td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $(function() {
                $('.delete_theme').on("click", function() {
                    var self = $(this);
                    $.extorio_modal({
                        title: "Delete theme?",
                        size: "modal-sm",
                        content: "Are you sure you want to delete this theme?",
                        closetext: "cancel",
                        continuetext: "delete",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + self.attr("data-id");
                        }
                    });
                });
            });
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($themeId = false) {
        ?>
        <form method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->theme->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea name="description" id="description" class="form-control"><?=$this->theme->description?></textarea>
            </div>
            <div class="checkbox">
                <label>
                    <input <?php
                    if($this->theme->default) echo 'checked="checked"';
                    ?> name="default" id="default" type="checkbox"> Theme is default
                </label>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Advanced</h3>
                </div>
                <div class="panel-body">
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->theme->isHidden) echo 'checked="checked"';
                            ?> name="hidden" id="hidden" type="checkbox"> Theme is hidden from theme management
                        </label>
                    </div>
                    <?php
                    if(!$this->theme) {
                        ?>
                        <div class="form-group">
                            <label for="extension">Extension</label>
                            <select class="form-control" id="extension" name="extension">
                                <optgroup label="Internal">
                                    <option value="Application">Application</option>
                                    <option value="Core">Core</option>
                                </optgroup>
                                <?php
                                if(count($this->extensions)) {
                                    ?>
                                    <optgroup label="Installed">
                                        <?php
                                        foreach($this->extensions as $e) {
                                            if(!$e->_internal) {
                                                ?>
                                                <option value="<?=$e->_extensionName?>"><?=$e->_extensionName?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>

            <button name="theme_updated" type="submit" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
            <button name="theme_updated_exit" type="submit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
        </form>
        <?php
    }
}