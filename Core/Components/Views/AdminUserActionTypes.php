<?php
namespace Core\Components\Views;
use Core\Classes\Enums\FontAwesomeIcons;

/**
 * Manage the user action types
 *
 * Class AdminUserActionTypes
 */
class AdminUserActionTypes extends \Core\Components\Controllers\AdminUserActionTypes {
    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Detail 1</th>
                    <th>Detail 2</th>
                    <th>Detail 3</th>
                    <th>Extension</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($this->actionTypes) > 0) {
                    foreach($this->actionTypes as $type) {
                        ?>
                <tr>
                    <td><?=$type->name?></td>
                    <td><?=$type->description?></td>
                    <td><?=$type->detail1Description?></td>
                    <td><?=$type->detail2Description?></td>
                    <td><?=$type->detail3Description?></td>
                    <td><?=$type->extensionName?></td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="<?=$this->_getUrlToMethod("edit",array($type->id))?>"><span class="fa fa-edit"></span> edit</a>
                        <button data-id="<?=$type->id?>" class="btn btn-xs btn-danger action_delete"><span class="fa fa-trash"></span> delete</button>
                    </td>
                </tr>
                        <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="7">No user action types found</td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $(function() {
                $('.action_delete').on("click", function() {
                    var id = $(this).attr("data-id");
                    $.extorio_modal({
                        title: "Delete user action type",
                        content: "Are you sure that you want to delete this user action type?",
                        size: "modal-sm",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + id;
                        }
                    });
                });
            });
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($id = false) {
        ?>
        <form method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->actionType->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter a name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" name="description" id="description" placeholder="Enter a description"><?=$this->actionType->description?></textarea>
            </div>
            <div class="form-group">
                <label for="detail1">Detail 1 description</label>
                <textarea class="form-control" name="detail1" id="detail1" placeholder="Enter a description for detail 1"><?=$this->actionType->detail1Description?></textarea>
            </div>
            <div class="form-group">
                <label for="detail2">Detail 2 description</label>
                <textarea class="form-control" name="detail2" id="detail2" placeholder="Enter a description for detail 1"><?=$this->actionType->detail2Description?></textarea>
            </div>
            <div class="form-group">
                <label for="detail3">Detail 3 description</label>
                <textarea class="form-control" name="detail3" id="detail3" placeholder="Enter a description for detail 1"><?=$this->actionType->detail3Description?></textarea>
            </div>

            <?php
            if(!$this->actionType) {
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Advanced</div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="extension">Extension</label>
                            <select class="form-control" id="extension" name="extension">
                                <optgroup label="Internal">
                                    <option value="Application">Application</option>
                                    <option value="Core">Core</option>
                                </optgroup>
                                <?php
                                if(count($this->extensions)) {
                                    ?>
                                    <optgroup label="Installed">
                                        <?php
                                        foreach($this->extensions as $e) {
                                            if(!$e->internal) {
                                                ?>
                                                <option value="<?=$e->extensionName?>"><?=$e->extensionName?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
            <button type="submit" name="submitted" class="btn btn-primary"><span class="fa fa-<?=FontAwesomeIcons::_save?>"></span> Save</button>
            <button type="submit" name="submitted_exit" class="btn btn-info"><span class="fa fa-<?=FontAwesomeIcons::_signOut?>"></span> Save and exit</button>
        </form>
        <?php
    }
}