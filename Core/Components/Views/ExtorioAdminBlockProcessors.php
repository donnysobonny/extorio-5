<?php
namespace Core\Components\Views;
class ExtorioAdminBlockProcessors extends \Core\Components\Controllers\ExtorioAdminBlockProcessors {
    public function _onBegin() {

    }

    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Label</th>
                    <th>Category</th>
                    <th>Inline Enabled</th>
                    <th>All inline</th>
                    <th>Inline groups</th>
                    <th>Extension</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($this->processors) > 0) {
                    foreach($this->processors as $processor) {
                        ?>
                        <tr>
                            <td>
                                <span data-toggle="tooltip" data-placement="right" title="<?=htmlentities($processor->description,ENT_COMPAT)?>" style="cursor: help; border-bottom: 1px dashed;"><?=$processor->name?></span>
                            </td>
                            <td><?=$processor->label?></td>
                            <td><span class="label label-primary"><?=$processor->category->name?></span></td>
                            <td>
                                <?php
                                if($processor->inlineEnabled) {
                                    ?>
                                    <span class="fa fa-check"></span>
                                    <?php
                                } else {
                                    ?>
                                    <span class="fa fa-remove"></span>
                                    <?php
                                }
                                ?>
                            </td>
                            <td><?php
                                if($processor->allInline) {
                                    ?><span class="fa fa-check"></span><?php
                                } else {
                                    ?><span class="fa fa-remove"></span><?php
                                }
                                ?></td>
                            <td><?php
                                foreach($processor->inlineGroups as $group) {
                                    ?><span class="label label-primary"><?=$group->name?></span>
                                    <?php
                                }
                                ?></td>
                            <td><?=$processor->extensionName?></td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="/extorio-admin/block-processors/edit/<?=$processor->id?>"><span class="fa fa-edit"></span> edit</a>
                                <button data-id="<?=$processor->id?>" class="btn btn-xs btn-danger delete_block_processor"><span class="fa fa-trash"></span> delete</button>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="8">No block processors found</td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $(function () {
                $('.delete_block_processor').on("click", function() {
                    var self = $(this);
                    $.extorio_modal({
                        title: "Delete block processor?",
                        size: "modal-sm",
                        content: "Are you sure you want to delete this block processor?",
                        closetext: "cancel",
                        continuetext: "delete",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("delete")?>" + self.attr("data-id");
                        }
                    });
                });

                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <?php
        $this->f->displayPagination();
    }

    public function edit($processorId = false) {
        ?>
        <form method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?=$this->processor->name?>" type="text" class="form-control" id="name" name="name" placeholder="Enter a name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description" placeholder="Enter a description"><?=$this->processor->description?></textarea>
            </div>
            <div class="form-group">
                <label for="category">Category</label>
                <select class="form-control" id="category" name="category">
                    <?php
                    foreach($this->categories as $category) {
                        ?><option <?php
                        if($this->processor->category == $category->id) echo 'selected="selected"';
                        ?> value="<?=$category->id?>"><?=$category->name?></option><?php
                    }
                    ?>
                </select>
            </div>
            <div class="checkbox">
                <label>
                    <input <?php
                    if($this->processor->inlineEnabled) echo 'checked="checked"';
                    ?> name="inline" id="inline" type="checkbox"> Can be used inline
                </label>
            </div>

            <div <?php
            if(!$this->processor->inlineEnabled) echo 'style="display: none;"';
            ?> id="inline_access_conf" class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Inline access</div>
                </div>
                <div class="panel-body">
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->processor->allInline) echo 'checked="checked"';
                            ?> name="all_inline" id="all_inline" type="checkbox"> All users can access this block processor inline
                        </label>
                    </div>

                    <div id="user_group_select" style="<?php
                    if($this->processor->allInline) echo 'display:none;';
                    ?>" class="form-group">
                        <label for="label">Select the user groups that can access this block processor inline</label>
                        <?php
                        foreach($this->groups as $group) {
                            ?>
                            <div class="checkbox">
                                <label>
                                    <input <?php
                                    if(in_array($group->id, $this->processor->inlineGroups)) echo 'checked="checked"';
                                    ?> name="groups[]" value="<?=$group->id?>" type="checkbox"> <?=$group->name?> <small><?=$group->description?></small>
                                </label>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

            <?php
            if(!$this->processor) {
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Advanced</div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="extension">Extension</label>
                            <select class="form-control" id="extension" name="extension">
                                <optgroup label="Internal">
                                    <option value="Application">Application</option>
                                    <option value="Core">Core</option>
                                </optgroup>
                                <?php
                                if(count($this->extensions)) {
                                    ?>
                                    <optgroup label="Installed">
                                        <?php
                                        foreach($this->extensions as $e) {
                                            if(!$e->_internal) {
                                                ?>
                                                <option value="<?=$e->_extensionName?>"><?=$e->_extensionName?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <button type="submit" name="processor_updated" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
            <button type="submit" name="processor_updated_exit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
        </form>
        <script>
            $(function() {
                $('#inline').on("change", function() {
                    if ($(this).prop("checked")) {
                        $('#inline_access_conf').show();
                    } else {
                        $('#inline_access_conf').hide();
                    }
                });

                $('#all_inline').on("change", function() {
                    if($(this).prop("checked")) {
                        $('#user_group_select').hide();
                    } else {
                        $('#user_group_select').show();
                    }
                });
            });
        </script>
    <?php
    }

    public function delete($processorId = false) {

    }

}