<?php
namespace Core\Components\Views;
use Core\Classes\Enums\TaskStatus;

class ExtorioAdminTaskManager extends \Core\Components\Controllers\ExtorioAdminTaskManager {
    public function _onBegin() {

    }

    public function _onDefault() {
        ?>
        <a class="btn btn-default" href="/extorio-admin/task-manager"><span class="fa fa-refresh"></span> refresh</a>
        <a class="btn btn-danger" href="/extorio-admin/task-manager/clear"><span class="fa fa-trash"></span> clear all non-running tasks</a>
        <br /><br />
        <?php
        if(!$this->showHiddenTasks) {
            ?>
        <a class="btn btn-default" href="/extorio-admin/task-manager?show_hidden_tasks=true">Show hidden tasks</a>
            <?php
        } else {
            ?>
        <a class="btn btn-primary" href="/extorio-admin/task-manager?show_hidden_tasks=false">Showing hidden tasks</a>
            <?php
        }
        ?>
        <br /><br />
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Task</th>
                    <th>Action</th>
                    <th>Params</th>
                    <th>Date started</th>
                    <th>Date updated</th>
                    <th>Status</th>
                    <th>Error message</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($this->liveTasks as $liveTask) {
                    ?>
                <tr>
                    <td>
                        <?=$liveTask["task"]?>
                    </td>
                    <td><?=$liveTask["action"]?></td>
                    <td><?=$liveTask["params"]?></td>
                    <td><?=$liveTask["dateCreated"]?></td>
                    <td><?=$liveTask["dateUpdated"]?></td>
                    <td><?=$liveTask["status"]?></td>
                    <td><?=$liveTask["errorMessage"]?></td>
                    <td>
                        <div style="display: none" class="task_log" data-livetaskpid="<?=$liveTask["pid"]?>">
                            <pre><?=$liveTask["log"]?></pre>
                        </div>
                        <button class="btn btn-xs btn-primary task_log_view" data-target=".task_log[data-livetaskpid='<?=$liveTask["pid"]?>']"><span class="fa fa-eye"></span> view log</button>
                        <?php
                        if($liveTask["status"] == TaskStatus::_running || $liveTask["status"] == TaskStatus::_queued) {
                            ?>
                            <a class="btn btn-xs btn-danger" href="/extorio-admin/task-manager/kill/<?=$liveTask["pid"]?>"><span class="fa fa-remove"></span> kill task</a>
                        <?php
                        }
                        ?>

                    </td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <h3>Run a task</h3>
        <form method="post" action="">
            <div class="form-group">
                <label for="task">Task</label>
                <select class="form-control" id="task" name="task">
                    <option value="">--select a task--</option>
                    <?php
                    foreach($this->callableActions as $task => $actions) {
                        ?>
                    <option value="<?=$task?>"><?=$task?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div id="action_select" style="display: none;" class="form-group">
                <label for="action">Action</label>
                <select class="form-control" id="action" name="action">

                </select>
            </div>
            <div class="form-group">
                <label for="task">Parameters</label>
                <input class="form-control" type="text" name="params" id="params" placeholder="Comma seperated parameters"/>
            </div>
            <button type="submit" name="run_task" class="btn btn-primary">Run</button>
        </form>
        <script>
            $(function() {
                var callableActions = <?=json_encode($this->callableActions)?>;

                $('#task').on("change", function() {
                    var task = $(this).val();
                    var action = $('#action');
                    if(task.length > 0) {
                        action.html('');
                        for(var i = 0; i < callableActions[task].length; i++) {
                            action.append('<option value="'+callableActions[task][i]+'">'+callableActions[task][i]+'</option>');
                        }
                        $('#action_select').show();
                    } else {
                        action.html('');
                        $('#action_select').hide();
                    }

                });

                $('.task_log_view').on("click", function() {
                    $.extorio_dialog({
                        title: "Log viewer",
                        content: $($(this).attr('data-target')).html()
                    });
                })
            });
        </script>
        <?php
    }

    public function _onEnd() {

    }

    protected function onStart() {

    }
}