<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\FileSystem\Directory;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\Extension;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

class ExtorioAdminExtensions extends \Core\Classes\Commons\Controller {

    public $canInstall = false;
    public $canUninstall = false;
    public $canUpdate = false;

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_extensions"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canInstall = Users::userHasPrivilege($this->_getLoggedInUserId(),"extensions_install","Core");
        $this->canUninstall = Users::userHasPrivilege($this->_getLoggedInUserId(),"extensions_uninstall","Core");
        $this->canUpdate = Users::userHasPrivilege($this->_getLoggedInUserId(),"extensions_update","Core");
    }

    /**
     * @var SimpleFiltering
     */
    public $f;

    /**
     * @var Extension
     */
    public $core;
    /**
     * @var Extension
     */
    public $application;

    /**
     * @var Extension[]
     */
    public $extensions;

    public function _onDefault() {
        $dirs = Directory::getDirectoriesWithinDirectory("Extensions");
        foreach($dirs as $dir) {
            if(!Extension::existsOne(Query::n()->where(array("name" => $dir->baseName())))) {
                if($dir->baseName() == Strings::classNameSafe($dir->baseName())) {
                    $e = Extension::n();
                    $e->name = $dir->baseName();
                    $e->namespace = '\\'.$e->name.'\Components\Extension\Extension';
                    $e->isInstalled = $e->isInternal = false;
                    $c = $this->_Extorio()->constructExtension($e->namespace);
                    if($c) {
                        $e->build = $c->_getCurrentBuild();
                        $e->pushThis();
                    }
                }
            }
        }

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));

        $this->f->extractFiltering();
        $this->f->setLimit(10);

        $query = Query::n();
        $where = array();
        $where[] = array("isInternal"=>false);
        if(strlen($this->f->getSearchQuery())) {
            $where[] = array("name" => array(
                Query::_lk => "%".$this->f->getSearchQuery()."%"
            ));
        }

        $query->where($where);
        $clone = clone($query);

        $query->order("name")->limit($this->f->getLimit())->skip($this->f->getOffset());

        $this->extensions = Extension::findAll($query);
        $this->f->setCount(Extension::findCount($clone));

        $this->core = Extension::findByName("Core");
    }

    public function update($id = false) {
        if(!$this->canUpdate) {
            $this->_messageWarning("You are not able to update extensions");
            $this->_redirectToDefault();
        }
        $e = Extension::findById($id);
        if($e) {
            $c = $e->getExtensionComponent();
            if($c) {
                $error = false;
                try {
                    $c->_update();
                } catch(\Exception $ex) {
                    $error = $ex->getMessage();
                }

                if($error) {
                    $this->_messageError("ERROR: ".$error);
                } else {
                    $this->_redirectToMethod("pre_patch",array($id));
                }
            }
        }
        $this->_redirectToDefault();
    }

    public function pre_patch($id = false,$num=0) {
        if(intval($num) > 15) {
            $this->_messageError("There was a problem trying to patch the update. Please try again.");
            $this->_redirectToDefault();
        } else {
            if(!$this->canUpdate) {
                $this->_messageWarning("You are not able to update extensions");
                $this->_redirectToDefault();
            }
            $e = Extension::findById($id);
            if($e) {
                $c = $e->getExtensionComponent();
                if($c) {
                    if($c->_getCurrentBuild() < $c->_getPublicBuild()) {
                        sleep(0.5);
                        $this->_redirectToMethod("pre_patch",array($id,intval($num)+1));
                    } else {
                        $this->_redirectToMethod("patch",array($id));
                    }
                }
            }
        }
        $this->_redirectToDefault();
    }

    public function patch($id = false) {
        if(!$this->canUpdate) {
            $this->_messageWarning("You are not able to patch extensions");
            $this->_redirectToDefault();
        }
        $e = Extension::findById($id);
        if($e) {
            $c = $e->getExtensionComponent();
            if($c) {
                $error = false;
                try {
                    $c->_patch();
                } catch(\Exception $ex) {
                    $error = $ex->getMessage();
                }

                if($error) {
                    echo $error;
                    $this->_messageError("ERROR: ".$error);
                } else {
                    $this->_messageSuccess("Extension updated");
                }
            }
        }
        $this->_redirectToDefault();
    }

    public function install($id = false) {
        if(!$this->canInstall) {
            $this->_messageWarning("You are not able to install extensions");
            $this->_redirectToDefault();
        }
        $e = Extension::findById($id);
        if($e) {
            $c = $e->getExtensionComponent();
            if($c) {
                $error = false;
                try {
                    $c->_install();
                } catch(\Exception $ex) {
                    $error = $ex->getMessage();
                }

                if($error) {
                    $this->_messageError("ERROR: ".$error);
                } else {
                    $this->_messageSuccess("Extension installed");
                }
            }
        }
        $this->_redirectToDefault();
    }

    public function uninstall($id = false) {
        if(!$this->canUninstall) {
            $this->_messageWarning("You are not able to uninstall extensions");
            $this->_redirectToDefault();
        }
        $e = Extension::findById($id);
        if($e) {
            $c = $e->getExtensionComponent();
            if($c) {
                $error = false;
                try {
                    $c->_uninstall();
                } catch(\Exception $ex) {
                    $error = $ex->getMessage();
                }

                if($error) {
                    $this->_messageError("ERROR: ".$error);
                } else {
                    $this->_messageSuccess("Extension uninstalled");
                }
            }
        }
        $this->_redirectToDefault();
    }
}