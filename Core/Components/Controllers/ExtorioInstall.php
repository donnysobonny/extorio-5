<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\FileSystem\Directory;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Enums\UserAccessLevels;
use Core\Classes\Helpers\Options;
use Core\Classes\Models\User;
use Core\Classes\Utilities\Extorio;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

class ExtorioInstall extends \Core\Classes\Commons\Controller {

    public $action;

    /**
     * @var Directory[]
     */
    public $systemUnWritableDirs = array();
    public $systemWritable = true;
    public $systemPgInstalled = false;
    public $systemCliAccess = false;
    public $systemOkay = true;

    public $applicationName = "";
    public $applicationDescription = "";
    public $applicationCompany = "";
    public $applicationDomain = "";
    public $applicationSeo = true;
    public $applicationMaintenance = false;

    public $databaseName = "";
    public $databaseHost = "localhost";
    public $databaseUser = "";
    public $databasePass = "";
    public $databaseError = false;

    public $adminError = false;
    public $emailFromName;
    public $emailFromEmail;
    public $emailTransport;
    public $emailSmtpHost;
    public $emailSmtpPort;
    public $emailSmtpUsername;
    public $emailSmtpPassword;

    public function _onBegin() {
        //if extorio is already installed, we shouldn't be here
        if($this->_Extorio()->installationComplete()) {
            header("Location: /extorio-admin");
            exit;
        }
        //the install mvc should always be run independently of the normal dispatch process
        $this->_Extorio()->setDispatchSetting("dispatch_auto_render",false);
    }

    public function _onDefault() {
        //if no action, redirect with action "welcome"
        if(!isset($_GET["action"])) {
            header("Location: /?action=welcome");
            exit;
        }
        $this->action = $_GET["action"];
        if(is_callable(array($this,$this->action))) {
            call_user_func(array($this,$this->action),func_get_args());
        }
    }

    public function welcome() {

    }

    public function system() {
        //find all directories, and make sure we can write to them
        $ds = Directory::getDirectoriesWithinDirectory("./",true);
        foreach($ds as $d) {
            if(!$d->isWritable()) {
                $this->systemUnWritableDirs[] = $d;
                $this->systemWritable = false;
            }
        }

        //check pg installed
        $this->systemPgInstalled = Extorio::pgInstalled();

        //check cli access
        $this->systemCliAccess = Extorio::canCliAccess();

        //system okay?
        $this->systemOkay = $this->systemWritable && $this->systemPgInstalled && $this->systemCliAccess;
    }

    public function application() {
        $config = $this->_Extorio()->getConfig();

        if(isset($_POST["application_submitted"])) {
            $this->applicationName = $_POST["name"];
            $this->applicationDescription = $_POST["description"];
            $this->applicationCompany = $_POST["company"];
            $this->applicationDomain = Strings::notEndsWith($_POST["domain"],"/");
            $this->applicationSeo = isset($_POST["seo"]);
            $this->applicationMaintenance = isset($_POST["maintenance"]);

            //write to Config
            $core = $this->_Extorio()->getExtension("Core");
            $core->_mergeConfigOverride(array(
                "application" => array(
                    "name" => $this->applicationName,
                    "description" => $this->applicationDescription,
                    "company" => $this->applicationCompany,
                    "address" => $this->applicationDomain,
                    "seo_visible" => $this->applicationSeo,
                    "maintenance_mode" => $this->applicationMaintenance
                )
            ));

            //move on to email
            header("Location: /?action=email");
            exit;
        }

        $this->applicationName = $config["application"]["name"];
        $this->applicationDescription = $config["application"]["description"];
        $this->applicationCompany = $config["application"]["company"];
        $this->applicationDomain = strlen($config["application"]["address"]) ? $config["application"]["address"] : Server::getDomain();
        $this->applicationSeo = $config["application"]["seo_visible"];
        $this->applicationMaintenance = $config["application"]["maintenance_mode"];
    }

    public function email() {
        $config = $this->_Extorio()->getConfig();

        if(isset($_POST["email_submitted"])) {
            $core = $this->_Extorio()->getExtension("Core");
            $core->_mergeConfigOverride(array(
                "emails" => array(
                    "transport" => $_POST["transport"],
                    "smtp" => array(
                        "host" => $_POST["smtp_host"],
                        "port" => intval($_POST["smtp_port"]),
                        "username" => $_POST["smtp_username"],
                        "password" => $_POST["smtp_password"]
                    ),
                    "defaults" => array(
                        "from_name" => $_POST["default_from_name"],
                        "from_email" => $_POST["default_from_email"]
                    )
                )
            ));

            //move to database
            header("Location: /?action=database");
            exit;
        }

        $this->emailFromEmail = $config["emails"]["defaults"]["from_email"];
        $this->emailFromName = $config["emails"]["defaults"]["from_name"];
        $this->emailTransport = $config["emails"]["transport"];
        $this->emailSmtpHost = $config["emails"]["smtp"]["host"];
        $this->emailSmtpPort = $config["emails"]["smtp"]["port"];
        $this->emailSmtpUsername = $config["emails"]["smtp"]["username"];
        $this->emailSmtpPassword = $config["emails"]["smtp"]["password"];
    }

    public function database() {
        if(isset($_POST["database_submitted"])) {

            $this->databaseName = $_POST["name"];
            $this->databaseUser = $_POST["user"];
            $this->databasePass = $_POST["pass"];
            $this->databaseHost = $_POST["host"];

            //write to Config
            $core = $this->_Extorio()->getExtension("Core");
            $core->_mergeConfigOverride(array(
                "database" => array(
                    "name" => $this->databaseName,
                    "username" => $this->databaseUser,
                    "password" => $this->databasePass,
                    "host" => $this->databaseHost
                )
            ));

            $db = $this->_Extorio()->getDbInstanceDefault();
            if(!$db->connected()) {
                $this->databaseError = "Unable to establish a connection with the database. Please check your details and try again";
            } else {
                //write to Config
                $core->_mergeConfigOverride(array(
                    "extorio" => array(
                        "database_connected" => true
                    )
                ));

                //move on to import
                header("Location: /?action=import");
                exit;
            }
        }

        $config = $this->_Extorio()->getConfig();
        $this->databaseName = $config["database"]["name"];
        $this->databaseUser = $config["database"]["username"];
        $this->databasePass = $config["database"]["password"];
        $this->databaseHost = strlen($config["database"]["host"]) ? $config["database"]["host"] : "localhost";
    }

    public function import() {
        if(isset($_POST["import_submitted"])) {
            $core = $this->_Extorio()->getExtension("Core");
            //get the db_dump
            $asset = File::constructFromPath("Core/Assets/db_dump.sql");
            if($asset) {
                $db = $this->_Extorio()->getDbInstanceDefault();
                $db->query($asset->read());
            }

            //write to Config
            $core->_mergeConfigOverride(array(
                "extorio" => array(
                    "database_imported" => true
                )
            ));

            //move on to import
            header("Location: /?action=admin");
            exit;
        }
    }

    public function admin() {
        $config = $this->_Extorio()->getConfig();
        if(isset($_POST["admin_submitted"])) {
            if(!strlen($_POST["username"])) {
                $this->adminError = "You must enter a username";
            }
            if(strlen($_POST["password"]) < 6) {
                $this->adminError = "Password must be atleast 6 characters in length";
            }
            if($_POST["password"] != $_POST["cpassword"]) {
                $this->adminError = "Passwords do not match";
            }
            if(!$this->adminError) {
                $u = User::n();
                $u = User::q(
                    $_POST["username"],
                    $_POST["password"],
                    $_POST["email"],
                    1,
                    "",
                    "",
                    true
                );
                $u->avatar = $config["users"]["defaults"]["avatar"];
                try {
                    $u->pushThis();
                } catch(\Exception $ex) {
                    $this->adminError = $ex->getMessage();
                }
                if(!$this->adminError) {
                    //log in as the new admin
                    try {
                        Users::login($u->username,$u->password,7,false,-7);
                    } catch(\Exception $ex) {
                        //do nothing
                    }

                    //write to Config
                    $core = $this->_Extorio()->getExtension("Core");
                    $core->_mergeConfigOverride(array(
                        "extorio" => array(
                            "admin_created" => true
                        )
                    ));

                    //move to completed
                    header("Location: /?action=complete");
                    exit;
                }
            }
        }
    }

    public function complete() {
        if(isset($_POST["completed_submitted"])) {
            //write to Config
            $core = $this->_Extorio()->getExtension("Core");
            $core->_mergeConfigOverride(array(
                "extorio" => array(
                    "installation_complete" => true
                )
            ));

            //create the robots file
            Extorio::createRobotsTxt();

            //go to admin
            header("Location: /extorio-admin");
            exit;
        }
    }
}