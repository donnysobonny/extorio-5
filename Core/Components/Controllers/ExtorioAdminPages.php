<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\Extension;
use Core\Classes\Enums\FontAwesomeIcons;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\LayoutSystem;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\Category;
use Core\Classes\Models\Enum;
use Core\Classes\Models\GeneralAccessPoint;
use Core\Classes\Models\Page;
use Core\Classes\Models\PageAccessPoint;
use Core\Classes\Models\Template;
use Core\Classes\Models\Theme;
use Core\Classes\Models\UserGroup;
use Core\Classes\Models\UserRole;
use Core\Classes\Utilities\Arrays;
use Core\Classes\Utilities\Pages;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

class ExtorioAdminPages extends \Core\Classes\Commons\Controller {

    public $canCreate = false;
    public $canDeleteAll = false;
    /**
     *
     * @var Page[]
     */
    public $pages;
    /**
     * @var SimpleFiltering
     */
    public $filtering;
    /**
     *
     * @var Page
     */
    public $page;
    /**
     *
     * @var Template[]
     */
    public $templates;
    /**
     *
     * @var Theme[]
     */
    public $themes;
    /**
     * @var UserGroup[]
     */
    public $groups = array();
    /**
     * @var Extension[]
     */
    public $extensions = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_pages"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUser()->id,"pages_create","Core");
        $this->canDeleteAll = Users::userHasPrivilege($this->_getLoggedInUser()->id,"pages_delete_all","Core");
    }

    public function _onDefault() {
        $this->filtering = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));

        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Pages","/extorio-admin/pages"),
                BreadCrumb::n(false,"Create new page","/extorio-admin/pages/edit",FontAwesomeIcons::_plus,!$this->canCreate)
            )
        );
        $this->filtering->addFilter("limit",array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $this->filtering->addFilter("state", array(
            "any" => "Any State",
            "public" => "Public pages",
            "unpublic" => "Unpublic pages",
            "default" => "Default page",
            "hidden" => "Hidden pages"
        ));
        $this->filtering->addFilter("1",null);
        $this->filtering->addFilter("order",array(
            "address" => "Address",
            "name" => "Name"
        ));

        $this->filtering->setSearchTypes(array(
            "name" => "Name",
            "address" => "Address",
            "extension" => "Extension"
        ));

        $this->filtering->extractFiltering();
        $this->filtering->setLimit($this->filtering->getFilter("limit"));

        $query = Query::n();
        $where = array();
        switch($this->filtering->getFilter("state")) {
            case "public" :
                $where[] = array("isHidden" => false);
                $where[] = array("isPublic" => true);
                break;
            case "unpublic" :
                $where[] = array("isHidden" => false);
                $where[] = array("isPublic" => false);
                break;
            case "default" :
                $where[] = array("isHidden" => false);
                $where[] = array("default" => true);
                break;
            case "hidden" :
                $where[] = array("isHidden" => true);
                break;
            default :
                $where[] = array("isHidden" => false);
                break;
        }
        if(strlen($this->filtering->getSearchQuery())) {
            switch($this->filtering->getSearchType()) {
                case "name" :
                    $where[] = array("name" => array(
                        Query::_lk => "%".$this->filtering->getSearchQuery()."%"
                    ));
                    break;
                case "address" :
                    $where[] = array("address" => array(
                        Query::_lk => "%".$this->filtering->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName" => array(
                        Query::_lk => "%".$this->filtering->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $queryCount = clone($query);

        switch($this->filtering->getFilter("order")) {
            case "address" :
                $query->order(array("address" => "asc"));
                break;
            case "name" :
                $query->order(array("name" => "asc"));
                break;
        }

        $query->limit($this->filtering->getLimit());
        $query->skip(($this->filtering->getPage() * $this->filtering->getLimit()));

        $this->pages = Page::findAll($query,2);
        $this->filtering->setCount(Page::findCount($queryCount));
    }

    public function edit($pageId=false) {
        if($pageId) {
            if(!Pages::canUserModifyPage($this->_getLoggedInUser()->id,$pageId)) {
                $this->_messageWarning("You are not able to modify this page");
                $this->_redirectToDefault();
            }
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create pages");
                $this->_redirectToDefault();
            }
        }

        if(isset($_POST["page_updated"]) || isset($_POST["page_updated_exit"])) {
            $p = Page::findById($pageId,1);
            if(!$p) {
                $p = Page::n();
            }
            $p->name = $_POST["name"];
            $p->description = $_POST["description"];
            $p->default = isset($_POST["default"]);
            $p->isPublic = isset($_POST["public"]);
            $p->isHidden = isset($_POST["hidden"]);
            $p->publishDate = $_POST["publish"];
            $p->unpublishDate = $_POST["unpublish"];
            $p->appendToHead = $_POST["appendtohead"];
            $p->prependToHead = $_POST["prependtohead"];
            $p->appendToBody = $_POST["appendtobody"];
            $p->prependToBody = $_POST["prependtobody"];
            $p->address = Strings::startsAndEndsWith($_POST["address"],"/");
            $p->extensionName = $_POST["extension"]?:$p->extensionName;

            $p->allRead = isset($_POST["all_view"]);
            if(isset($_POST["read_groups"])) {
                $p->readGroups = Arrays::smartCast($_POST["read_groups"]);
            } else {
                $p->readGroups = array();
            }
            if(isset($_POST["modify_groups"])) {
                $p->modifyGroups = Arrays::smartCast($_POST["modify_groups"]);
            } else {
                $p->modifyGroups = array();
            }
            $p->readFailPageId = intval($_POST["read_fail_page_id"]);

            if(strlen($_POST["theme"])) {
                $theme = Theme::findById(intval($_POST["theme"]));
                if($theme) {
                    $p->theme = $theme;
                }
            } else {
                $p->theme = new \stdClass();
            }
            if(strlen($_POST["template"])) {
                $template = Template::findById(intval($_POST["template"]));
                if($template) {
                    $p->template = $template;
                }
            } else {
                $p->template = new \stdClass();
            }

            $error = false;
            try{
                $p->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_Extorio()->messageError($error);
            } else {
                $this->_Extorio()->messageSuccess("Page successfully saved");
                if(isset($_POST["page_updated_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit",array($p->id));
                }
            }
        }
        $this->page = Page::findById($pageId,1);
        $this->pages = Page::findAll(Query::n()->order("name"),1);
        $this->themes = Theme::findAll(
            Query::n()->order(array("name"))
        );
        $this->templates = Template::findAll(
            Query::n()->order(array("name"))
        );
        $this->groups = UserGroup::findAll(Query::n()->order(array("name"=>"asc")));

        if($this->page) {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Pages","/extorio-admin/pages"),
                    BreadCrumb::n(true,$this->page->name)
                )
            );
        } else {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Pages","/extorio-admin/pages"),
                    BreadCrumb::n(true,"Create new page","",FontAwesomeIcons::_plus)
                )
            );
        }
        $this->extensions = $this->_Extorio()->getExtensions();
    }

    public function delete($pageId=false) {
        if(!$pageId) {
            $this->_redirectToDefault();
        }
        if(!$this->canDeleteAll) {
            $this->_messageWarning("You are not able to delete this page");
            $this->_redirectToDefault();
        }

        $page = Page::findById($pageId);
        if($page) {
            $page->deleteThis();
        }
        $this->_Extorio()->messageInfo("Page successfully deleted");
        $this->_redirectToDefault();
    }
}