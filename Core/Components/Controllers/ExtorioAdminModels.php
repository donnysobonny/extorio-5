<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\Extension;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\Enum;
use Core\Classes\Models\GeneralAccessPoint;
use Core\Classes\Models\Model;
use Core\Classes\Models\Property;
use Core\Classes\Models\UserRole;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

class ExtorioAdminModels extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModify;
    public $canDelete;
    /**
     * @var Model[]
     */
    public $models;
    /**
     *
     * @var Model
     */
    public $model;
    /**
     *
     * @var Enum[]
     */
    public $enums = array();
    /**
     *
     * @var Property
     */
    public $property;
    /**
     *
     * @var UserRole[]
     */
    public $roles = array();
    /**
     * @var Extension[]
     */
    public $extensions = array();
    /**
     * @var SimpleFiltering
     */
    public $f;

    public function _onBegin() {
        if (!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id, array("extorio_pages_all", "extorio_pages_models"))) {
            $this->_redirectTo401AccessDeniedPage(array("r" => Server::getRequestURI(), array(), 401));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(), "models_create", "Core");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(), "models_modify", "Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(), "models_delete", "Core");
    }

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Models","/extorio-admin/models"),
                BreadCrumb::n(false,"Create new model","/extorio-admin/models/edit_model","plus",!$this->canCreate)
            )
        );

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit",array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $this->f->addFilter("state", array(
            "any" => "Any state",
            "hidden" => "Hidden models"
        ));
        $this->f->addFilter("1",null);
        $this->f->addFilter("2",null);
        $this->f->setSearchTypes(array(
            "name" => "Name",
            "extension" => "Extension"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getFilter("state"))) {
            if($this->f->getFilter("state") != "any") {
                switch($this->f->getFilter("state")) {
                    case "hidden" :
                        $where[] = array("isHidden" => true);
                        break;
                    default:
                        $where[] = array("isHidden" => false);
                        break;
                }
            } else {
                $where[] = array("isHidden" => false);
            }
        } else {
            $where[] = array("isHidden" => false);
        }
        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[] = array("name" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $queryCount = clone($query);

        $query->order("name");
        $query->limit($this->f->getLimit());
        $query->skip($this->f->getLimit() * $this->f->getPage());

        $this->models = Model::findAll($query);
        $this->f->setCount(Model::findCount($queryCount));
    }

    public function edit_model($modelId=false) {
        if($modelId) {
            if(!$this->canModify) {
                $this->_messageWarning("You are not able to modify models");
                $this->_redirectToDefault();
            }
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create models");
                $this->_redirectToDefault();
            }
        }

        if(isset($_POST["model_updated"]) || isset($_POST["model_updated_exit"])) {
            $m = Model::findById($modelId,2);
            if(!$m) {
                $m = Model::n();
            }
            $m->name = $_POST["name"];
            $m->description = $_POST["description"];
            $m->isDependant = isset($_POST["isDependant"]);
            $m->isHidden = isset($_POST["hidden"]);
            $m->extensionName = $_POST["extension"]?:$m->extensionName;

            $error = false;
            try{
                $m->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_Extorio()->messageError($error);
            } else {
                $this->_Extorio()->messageSuccess("Model saved successfully");
                if(isset($_POST["model_updated_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit_model",array($m->id));
                }
            }
        }
        $this->model = Model::findById($modelId,2);

        if($this->model) {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Models","/extorio-admin/models"),
                    BreadCrumb::n(true,$this->model->label),
                    BreadCrumb::n(false,"Properties","/extorio-admin/models/properties/".$this->model->id),
                )
            );
        } else {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Models","/extorio-admin/models"),
                    BreadCrumb::n(true,"Create new model","","plus"),
                )
            );
        }

        $this->extensions = $this->_Extorio()->getExtensions();
    }

    public function delete_model($modelId=false) {
        if(!$modelId) {
            $this->_redirectToDefault();
        }
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete models");
            $this->_redirectToDefault();
        }
        $model = Model::findById($modelId);
        if($model) {
            $model->deleteThis();
        }
        $this->_Extorio()->messageSuccess("Model deleted successfully");
        $this->_redirectToDefault();
    }

    public function properties($modelId=false) {
        if(!$modelId) {
            $this->_redirectToDefault();
        }
        if(!$this->canModify) {
            $this->_messageWarning("You are not able to modify models");
            $this->_redirectToDefault();
        }

        $this->models = Model::findAll(
            Query::n()->order(array("class"))
        );
        $this->enums = Enum::findAll(
            Query::n()->order(array("label"))
        );
        $this->model = Model::findById($modelId,2);

        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"Models","/extorio-admin/models"),
                BreadCrumb::n(false,$this->model->label,"/extorio-admin/models/edit_model/".$this->model->id),
                BreadCrumb::n(true,"Properties"),
            )
        );
    }
}