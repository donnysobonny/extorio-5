<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\FileSystem\Asset;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Users;

class ExtorioAdminAssets extends \Core\Classes\Commons\Controller {
    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_assets"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
    }

    public function _onDefault() {

    }
}