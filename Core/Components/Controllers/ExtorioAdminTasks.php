<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\GeneralAccessPoint;
use Core\Classes\Models\Task;
use Core\Classes\Models\UserGroup;
use Core\Classes\Models\UserRole;
use Core\Classes\Utilities\Arrays;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

class ExtorioAdminTasks extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModify;
    public $canDelete;
    /**
     *
     * @var Task[]
     */
    public $tasks = array();
    /**
     * @var SimpleFiltering
     */
    public $f;
    /**
     *
     * @var Task
     */
    public $task = false;
    /**
     *
     * @var UserGroup[]
     */
    public $groups = array();
    /**
     * @var Extension[]
     */
    public $extensions = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_tasks"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(),"tasks_create","Core");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(),"tasks_modify","Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(),"takss_delete","Core");
    }

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Tasks","/extorio-admin/tasks"),
                BreadCrumb::n(false,"Create new task","/extorio-admin/tasks/edit","plus",!$this->canCreate)
            )
        );

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit", array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $this->f->addEmpty();
        $this->f->addEmpty();
        $this->f->addEmpty();
        $this->f->setSearchTypes(array(
            "name" => "Name",
            "extension" => "Extension"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[] = array("name" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $this->f->setCount(Task::findCount($query));

        $query->order("name");
        $query->limit($this->f->getLimit());
        $query->skip($this->f->getLimit() * $this->f->getPage());

        $this->tasks = Task::findAll($query,2);
    }

    public function edit($taskId=false) {
        if($taskId) {
            if(!$this->canModify) {
                $this->_messageWarning("You are not able to modify tasks");
                $this->_redirectToDefault();
            }
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create tasks");
                $this->_redirectToDefault();
            }
        }

        if(isset($_POST["task_updated"]) || isset($_POST["task_updated_exit"])) {
            $t = Task::findById($taskId,2);
            if(!$t) {
                $t = Task::n();
            }
            $t->name = $_POST["name"];
            $t->description = $_POST["description"];
            $t->allRead = isset($_POST["all_read"]);
            if(isset($_POST["groups"])) {
                $t->readGroups = Arrays::smartCast($_POST["groups"]);
            } else {
                $t->readGroups = array();
            }
            $t->extensionName = $_POST["extension"]?:$t->extensionName;

            $error = false;
            try {
                $t->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_Extorio()->messageError($error);
            } else {
                $this->_Extorio()->messageSuccess("Task successfully saved");
                if(isset($_POST["task_updated_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit",array($t->id));
                }
            }
        }
        $this->roles = UserRole::findAll(
            Query::n()->order(array("name"))
        );
        $this->task = Task::findById($taskId,1);
        $this->groups = UserGroup::findAll(Query::n()->order(array("name"=>"asc")));

        if($this->task) {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Tasks","/extorio-admin/tasks"),
                    BreadCrumb::n(true,$this->task->label)
                )
            );
        } else {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Tasks","/extorio-admin/tasks"),
                    BreadCrumb::n(true,"Create new task","","plus")
                )
            );
        }
        $this->extensions = $this->_Extorio()->getExtensions();
    }

    public function delete($taskId=false) {
        if(!$taskId) {
            $this->_redirectToDefault();
        }
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete tasks");
            $this->_redirectToDefault();
        }

        $t = Task::findById($taskId);
        if($t) {
            $t->deleteThis();
        }

        $this->_Extorio()->messageSuccess("Task successfully deleted");
        $this->_redirectToDefault();
    }
}