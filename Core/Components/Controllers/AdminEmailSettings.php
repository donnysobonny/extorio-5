<?php
namespace Core\Components\Controllers;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Users;

/**
 * Manage the settings related to emails
 *
 * Class AdminEmailSettings
 */
class AdminEmailSettings extends \Core\Classes\Commons\Controller {
    public $config = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_email_settings"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
    }

    public function _onDefault() {
        if(isset($_POST["settings_updated"])) {
            $core = $this->_Extorio()->getExtension("Core");
            if($core) {
                $core->_mergeConfigOverride(array(
                    "emails" => array(
                        "transport" => $_POST["transport"],
                        "smtp" => array(
                            "host" => $_POST["smtp_host"],
                            "port" => intval($_POST["smtp_port"]),
                            "username" => $_POST["smtp_username"],
                            "password" => $_POST["smtp_password"]
                        ),
                        "defaults" => array(
                            "from_name" => $_POST["default_from_name"],
                            "from_email" => $_POST["default_from_email"]
                        )
                    )
                ));
            }

            $this->_Extorio()->messageSuccess("Settings updated");
        }

        $this->config = $this->_Extorio()->getConfig();
    }
}