<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Model;
use Core\Classes\Utilities\Arrays;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Users;

class ExtorioAdminModelViewer extends \Core\Classes\Commons\Controller {

    public $canList;
    public $canEdit;
    public $canDetail;
    public $canDelete;

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_model_viewer"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canList = Users::userHasPrivilege($this->_getLoggedInUserId(),"model_viewer_list","Core");
        $this->canEdit = Users::userHasPrivilege($this->_getLoggedInUserId(),"model_viewer_edit","Core");
        $this->canDetail = Users::userHasPrivilege($this->_getLoggedInUserId(),"model_viewer_detail","Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(),"model_viewer_delete","Core");
    }

    /**
     *
     * @var Model[]
     */
    public $models = array();

    public $show_hidden_models = false;

    public function _onDefault($modelIdentifier=false,$action=false,$id=false) {
        if(!$modelIdentifier) {

            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(true,"Model Viewer","/extorio-admin/model-viewer"),
                )
            );

            if(isset($_GET["show_hidden_models"])) {
                if($_GET["show_hidden_models"] == "true") {
                    $_SESSION["show_hidden_models"] = true;
                } elseif($_GET["show_hidden_models"] == "false") {
                    $_SESSION["show_hidden_models"] = false;
                }
            }

            if(isset($_SESSION["show_hidden_models"])) {
                $this->show_hidden_models = $_SESSION["show_hidden_models"];
            } else {
                $this->show_hidden_models = false;
            }

            if($this->show_hidden_models) {
                $this->models = Model::findAll(Query::n()->order(array("class"=>'$asc')));
            } else {
                $this->models = Model::findAll(
                    Query::n()
                        ->where(array("isHidden"=>false))
                        ->order(array("class"=>'$asc')));
            }
        } else {
            $model = Model::findOne(Query::n()->where(array('$or' => array("class" => $modelIdentifier, "label" => $modelIdentifier))), 1);
            /** @var Model $type */
            $type = $model->namespace;
            switch($action) {
                case "edit" :

                    if(!$this->canEdit) {
                        $this->_messageWarning("You are not able to use the edit view of the model viewer");
                        $this->_redirectToDefault();
                    }

                    if($id) {
                        $this->_Extorio()->setTargetBreadCrumbs(
                            array(
                                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                                BreadCrumb::n(false,"Model Viewer","/extorio-admin/model-viewer"),
                                BreadCrumb::n(false,$model->label,"/extorio-admin/model-viewer/".$model->label),
                                BreadCrumb::n(true,$id),
                            )
                        );
                    } else {
                        $this->_Extorio()->setTargetBreadCrumbs(
                            array(
                                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                                BreadCrumb::n(false,"Model Viewer","/extorio-admin/model-viewer"),
                                BreadCrumb::n(false,$model->label,"/extorio-admin/model-viewer/".$model->label),
                                BreadCrumb::n(true,"Create new","","plus"),
                            )
                        );
                    }

                    if(isset($_POST["edit_submitted"])) {
                        $error = false;
                        try{
                            $mi = $type::constructFromArray(Arrays::smartCast($_POST["data"]));
                            $mi->pushThis();
                        } catch(\Exception $ex) {
                            $error = $ex->getMessage();
                        }
                        if($error) {
                            $this->_Extorio()->messageError($error);
                        } else {
                            $this->_Extorio()->messageSuccess("Object saved");
                            $this->_redirectToMethod($model->class);
                        }
                    }
                    break;
                default :

                    if(!$this->canList) {
                        $this->_messageWarning("You are not able to enter the list view of the model viewer");
                        $this->_redirectToDefault();
                    }

                    $this->_Extorio()->setTargetBreadCrumbs(
                        array(
                            BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                            BreadCrumb::n(false,"Model Viewer","/extorio-admin/model-viewer"),
                            BreadCrumb::n(true,$model->label,"/extorio-admin/model-viewer/".$model->label),
                            BreadCrumb::n(false,"Create new","/extorio-admin/model-viewer/".$model->label."/edit","plus",!$this->canEdit),
                        )
                    );
                    break;
            }
        }
    }
}