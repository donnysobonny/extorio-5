<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\UserPrivilegeCategory;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

/**
 * Manage the user privilege categories
 *
 * Class AdminUserPrivilegeCategories
 */
class AdminUserPrivilegeCategories extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModify;
    public $canDelete;
    /**
     * @var UserPrivilegeCategory[]
     */
    public $categories = array();
    /**
     * @var SimpleFiltering
     */
    public $f;
    /**
     * @var UserPrivilegeCategory
     */
    public $category;
    /**
     * @var Extension[]
     */
    public $extensions = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_user_privilege_categories"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_privilege_categories_create","Core");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_privilege_categories_modify","Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_privilege_categories_delete","Core");
    }

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(array(
            BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
            BreadCrumb::n(true,"User Privilege Categories","/extorio-admin/user-privilege-categories/"),
            BreadCrumb::n(false,"Create new category","/extorio-admin/user-privilege-categories/edit/","plus",!$this->canCreate)
        ));

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit", array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $this->f->addEmpty();
        $this->f->addEmpty();
        $this->f->addEmpty();
        $this->f->setSearchTypes(array(
            "name" => "Name",
            "extension" => "Extension"
        ));

        $this->f->extractFiltering();

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[] = array("name" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $this->f->setCount(UserPrivilegeCategory::findCount($query));

        $query->order("name");
        $query->limit($this->f->getLimit());
        $query->skip($this->f->getLimit() * $this->f->getPage());

        $this->categories = UserPrivilegeCategory::findAll($query);
    }

    public function edit($id = false) {
        if($id) {
            if(!$this->canModify) {
                $this->_messageWarning("You are not able to modify privilege categories");
                $this->_redirectToDefault();
            }
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create privilege categories");
                $this->_redirectToDefault();
            }
        }

        $this->category = UserPrivilegeCategory::findById($id,1);

        if(isset($_POST["submit"]) || isset($_POST["submit_exit"])) {
            $c = UserPrivilegeCategory::findById($id,1);
            if(!$c) {
                $c = UserPrivilegeCategory::n();
            }
            $c->name = $_POST["name"];
            $c->description = $_POST["description"];
            $c->icon = $_POST["icon"];
            $c->extensionName = $_POST["extension"]?:$c->extensionName;

            $error = false;
            try {
                $c->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                $this->_messageError($error);
            } else {
                $this->_messageSuccess("User Privilege Category Saved");
                if(isset($_POST["submit_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit",array($c->id));
                }
            }
        }

        if($this->category) {
            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"User Privilege Categories","/extorio-admin/user-privilege-categories/"),
                BreadCrumb::n(true,$this->category->name)
            ));
        } else {
            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"User Privilege Categories","/extorio-admin/user-privilege-categories/"),
                BreadCrumb::n(true,"Create new category","/extorio-admin/user-privilege-categories/edit/","plus")
            ));
        }

        $this->extensions = $this->_Extorio()->getExtensions();
    }

    public function delete($id = false) {
        if(!$id) {
            $this->_redirectToMethod();
        }
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete privilege categories");
            $this->_redirectToDefault();
        }

        $error = false;
        try{
            $c = UserPrivilegeCategory::findById($id,1);
            if($c) {
                $c->deleteThis();
            }
        } catch(\Exception $ex) {
            $error = $ex->getMessage();
        }

        if($error) {
            $this->_messageError($error);
        } else {
            $this->_messageSuccess("User Privilege Category Deleted");
        }

        $this->_redirectToDefault();
    }
}