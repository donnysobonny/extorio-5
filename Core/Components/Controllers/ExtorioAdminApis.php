<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\Extension;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\Api;
use Core\Classes\Models\GeneralAccessPoint;
use Core\Classes\Models\UserGroup;
use Core\Classes\Models\UserRole;
use Core\Classes\Utilities\Arrays;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

class ExtorioAdminApis extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModify;
    public $canDelete;
    /**
     * @var Api[]
     */
    public $apis;
    /**
     * @var SimpleFiltering
     */
    public $f;
    /**
     *
     * @var Api
     */
    public $api;
    /**
     * @var UserGroup[]
     */
    public $groups = array();
    /**
     * @var Extension[]
     */
    public $extensions = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_apis"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(),"apis_create","Core");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(),"apis_modify","Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(),"apis_delete","Core");
    }

    public function _onDefault() {
        //add the create crumb
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Apis","/extorio-admin/apis"),
                BreadCrumb::n(false,"Create new api","/extorio-admin/apis/edit","plus",!$this->canCreate)
            )
        );

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit", array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $this->f->addEmpty();
        $this->f->addEmpty();
        $this->f->addEmpty();
        $this->f->setSearchTypes(array(
            "name" => "Name",
            "extension" => "Extension"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[] = array("name" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $this->f->setCount(Api::findCount($query));

        $query->order("name");
        $query->limit($this->f->getLimit());
        $query->skip($this->f->getLimit() * $this->f->getPage());

        $this->apis = Api::findAll($query,2);
    }

    public function edit($apiId=false) {
        if($apiId) {
            if(!$this->canModify) {
                $this->_messageWarning("You are not able to modify apis");
                $this->_redirectToDefault();
            }
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create apis");
                $this->_redirectToDefault();
            }
        }

        $this->api = Api::findById($apiId,1);
        $this->groups = UserGroup::findAll(Query::n()->order(array("name"=>"asc")));

        if(isset($_POST["api_updated"]) || isset($_POST["api_updated_exit"])) {
            if(!$this->api) {
                $this->api = Api::n();
            }
            $this->api->name = $_POST["name"];
            $this->api->description = $_POST["description"];
            $this->api->allRead = isset($_POST["all_read"]);
            if(isset($_POST["groups"])) {
                $this->api->readGroups = Arrays::smartCast($_POST["groups"]);
            } else {
                $this->api->readGroups = array();
            }
            $this->api->extensionName = $_POST["extension"]?:$this->api->extensionName;

            $error = false;
            try {
                $this->api->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_Extorio()->messageError($error);
            } else {
                $this->_Extorio()->messageSuccess("Api successfully saved");
                if(isset($_POST["api_updated_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit",array($this->api->id));
                }
            }
        }
        if($this->api) {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Apis","/extorio-admin/apis"),
                    BreadCrumb::n(true,$this->api->label,"")
                )
            );
        } else {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Apis","/extorio-admin/apis"),
                    BreadCrumb::n(true,"Create new api","/extorio-admin/apis/edit","plus")
                )
            );
        }
        $this->extensions = $this->_Extorio()->getExtensions();
    }

    public function delete($apiId=false) {
        if(!$apiId) {
            $this->_redirectToDefault();
        }
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete apis");
            $this->_redirectToDefault();
        }

        $a = Api::findById($apiId);
        if($a) {
            $a->deleteThis();
        }

        $this->_Extorio()->messageSuccess("Api successfully deleted");
        $this->_redirectToDefault();
    }
}