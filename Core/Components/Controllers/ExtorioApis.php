<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\Api;
use Core\Classes\Commons\Extorio;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\User;
use Core\Classes\Utilities\Arrays;
use Core\Classes\Utilities\Server;

class ExtorioApis extends \Core\Classes\Commons\Controller {
    /**
     *
     * @var Api
     */
    private $api;

    public function _onBegin() {
        //disable auto render
        $this->_setDispatchSetting_AutoRender(false);
        $this->_Extorio()->bindActionToEvent("on_exception",array($this,"on_exception"),1,true);
    }

    /**
     * @param \Exception $ex
     */
    final public function on_exception($ex) {
        $this->_Extorio()->logCustom("api_error.log",$ex->__toString());
        if($this->api) {
            $this->api->_failApi($ex->getMessage());
        } else {
            ?>
<h1>Internal error</h1>
<p>
    An internal error was encountered before the api could be run. If this message persists, please contact the website administrator with the following information:
</p>
<p>
    <pre><?=$ex->__toString()?></pre>
</p>
            <?php
        }
    }

    public function _onDefault() {
        $args = Extorio::get()->getTargetPageParams();
        //the minimum args is 2 (identifier and action)
        if(count($args) < 1) {
            $this->unableToStartApi();
        }
        $apiIdentifier = $args[0];
        //try and find the api
        $a = \Core\Classes\Models\Api::findOne(
            Query::n()
                ->where(array(
                    "label" => $apiIdentifier
                )),2
        );
        if(!$a) {
            $this->unableToStartApi();
        }
        $extension = $this->_Extorio()->getExtension($a->extensionName);
        if(!$extension) {
            $this->unableToStartApi();
        }
        /** @var Api $api */
        $api = $extension->_constructComponent($a->namespace);
        if(!$api) {
            $this->unableToStartApi();
        }
        //set up the output object
        $api->_output = new \stdClass();
        $api->_output->error = false;
        $api->_output->error_message = "";
        $api->_output->warning_message = "";
        $api->_output->info_message = "";
        $api->_output->success_message = "";

        $this->api = $api;

        //setting up CORS
        //allow any origin.
        header("Access-Control-Allow-Origin: *");
        //allow the basic http methods, plus any requested methods
        $addMethods = "";
        if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"])) {
            $addMethods = ", ".$_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"];
        }
        header("Access-Control-Allow-Methods: PUT, POST, GET, DELETE, OPTIONS".$addMethods);
        //allow any custom headers, otherwise no need (most likely an ajax request)
        if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"])) {
            header("Access-Control-Allow-Headers: ".$_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]);
        }

        //setup the api
        $this->api->_httpMethod = Server::getMethod();
        if(!$this->api->_httpMethod) {
            $this->api->_failApi("Invalid http request method");
        }

        //if the http method is OPTIONS, we just need to respond with the http header
        if($this->api->_httpMethod == "OPTIONS") {
            $this->api->_display();
            exit;
        }
        //work out the decode type (how we will decode the payload)
        $decodeMap = array(
            "multipart/form-data" => "query",
            "application/x-www-form-urlencoded" => "query",
            "application/json" => "json",
            "text/json" => "json",
            "application/xhtml+xml" => "xml",
            "text/xml" => "xml",
            "application/xml" => "xml"
        );
        $decodeType = false;
        $contentType = Server::getContentType();
        foreach($decodeMap as $type => $decode) {
            if(strpos($contentType,$type) !== false) {
                $decodeType = $decode;
                break;
            }
        }
        //setting up the params
        $this->api->_fileData = $_FILES;
        parse_str(Server::getQueryString(),$this->api->_getParams);
        //if the $json param exists
        if(isset($this->api->_getParams['$json'])) {
            $array = json_decode($this->api->_getParams['$json'],true);
            if(is_array($array)) {
                foreach($array as $key => $value) {
                    $this->api->_getParams[$key] = $value;
                }
            }
        }
        $this->api->_getParams = Arrays::smartCast($this->api->_getParams);
        $this->api->_postParams = $_POST;
        //if the $json param exists
        if(isset($this->api->_postParams['$json'])) {
            $array = json_decode($this->api->_postParams['$json'],true);
            if(is_array($array)) {
                foreach($array as $key => $value) {
                    $this->api->_postParams[$key] = $value;
                }
            }
        }
        $this->api->_postParams = Arrays::smartCast($this->api->_postParams);
        //decode the payload based on the decode type
        $inputParams = array();
        if($this->api->_httpMethod == "POST" || $this->api->_httpMethod != "GET") {
            //params may be in the payload
            switch($decodeType) {
                case "query" :
                    parse_str(Extorio::get()->getInputStream(),$inputParams);
                    break;
                case "json" :
                    $array = json_decode(Extorio::get()->getInputStream(),true);
                    if($array) {
                        $inputParams = $array;
                    }
                    break;
                case "xml" :
                    throw new \Exception("XML currently not supported");
                    break;
            }
        }
        //if the $json param exists
        if(isset($inputParams['$json'])) {
            $array = json_decode($inputParams['$json'],true);
            if(is_array($array)) {
                foreach($array as $key => $value) {
                    $inputParams[$key] = $value;
                }
            }
        }
        //fix the input params
        $inputParams = Arrays::smartCast($inputParams);
        //merge get, post and input params into allParams
        foreach($this->api->_getParams as $key => $value) {
            $this->api->_allParams[$key] = $value;
        }
        foreach($this->api->_postParams as $key => $value) {
            $this->api->_allParams[$key] = $value;
        }
        foreach($inputParams as $key => $value) {
            $this->api->_allParams[$key] = $value;
        }
        //if the api has public properties that match the keys of the params, assign them to these public properties
        //reflect the api element so that we can check whether the property is accessible
        $apiR = new \ReflectionClass($this->api);
        foreach($this->api->_allParams as $key => $value) {
            if($key != "output") {
                try{
                    $elementProperty = $apiR->getProperty($key);
                } catch(\ReflectionException $ex) {
                    $elementProperty = false;
                }

                if($elementProperty) {
                    if($elementProperty->isPublic()) {
                        //overwrite
                        $this->api->$key = $value;
                    }
                } else {
                    //not defined, magically assign
                    $this->api->$key = $value;
                }
            }
        }

        //make sure we can access the api
        if(!\Core\Classes\Utilities\Apis::canUserReadApi($this->_getLoggedInUser()->id,$a->id)) {
            $this->api->_failApi("Access denied");
        }

        //work out the method and method params
        $this->api->_method = false;
        $this->api->_methodParams = array();
        if(count($args) > 1) {
            if(is_callable(array($this->api,$args[1]))) {
                $this->api->_method = $args[1];
                if(count($args) > 2) {
                    for($i = 2; $i < count($args); $i++) {
                        $this->api->_methodParams[] = $args[$i];
                    }
                }
            } else {
                for($i = 1; $i < count($args); $i++) {
                    $this->api->_methodParams[] = $args[$i];
                }
            }
        }

        //fix the method params
        $this->api->_methodParams = Arrays::smartCast($this->api->_methodParams);

        $this->api->_onBegin();

        //call the target action, or onDefault
        if($this->api->_method) {
            call_user_func_array(array($this->api,$this->api->_method),$this->api->_methodParams);
        } else {

            call_user_func_array(array($this->api, "_onDefault"),$this->api->_methodParams);
        }

        $this->api->_onEnd();

        //if we are still running by this point, the api has not displayed the output
        $this->api->_display();
    }

    private function unableToStartApi() {
        header("HTTP/1.1 404 Not Found");
        exit;
    }
}