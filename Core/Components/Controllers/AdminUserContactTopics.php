<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\UserContactTopic;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

/**
 * Manage user contact topics
 *
 * Class AdminUserContactTopics
 */
class AdminUserContactTopics extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModify;
    public $canDelete;
    /**
     * @var SimpleFiltering
     */
    public $f;
    /**
     * @var UserContactTopic[]
     */
    public $topics;
    /**
     * @var UserContactTopic
     */
    public $topic;
    /**
     * @var Extension[]
     */
    public $extensions = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_user_contact_topics"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_contact_topics_create","Core");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_contact_topics_modify","Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_contact_topics_delete","Core");
    }

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(array(
            BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
            BreadCrumb::n(true,"Contact Topics","/extorio-admin/user-contact-topics/"),
            BreadCrumb::n(false,"Create new topic","/extorio-admin/user-contact-topics/edit","plus",!$this->canCreate)
        ));

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit", array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $this->f->addEmpty();
        $this->f->addEmpty();
        $this->f->addEmpty();
        $this->f->setSearchTypes(array(
            "name" => "Name",
            "extension" => "Extension"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[] = array("name" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $clone = clone($query);

        $query->limit($this->f->getLimit())->skip($this->f->getOffset())->order("name");

        $this->topics = UserContactTopic::findAll($query);
        $this->f->setCount(UserContactTopic::findCount($clone));
    }

    public function edit($id = false) {
        $this->topic = UserContactTopic::findById($id);
        if($this->topic) {
            if(!$this->canModify) {
                $this->_messageWarning("You are not able to modify contact topics");
                $this->_redirectToDefault();
            }

            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"Contact Topics","/extorio-admin/user-contact-topics/"),
                BreadCrumb::n(true,$this->topic->name)
            ));
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create contact topics");
                $this->_redirectToDefault();
            }

            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"Contact Topics","/extorio-admin/user-contact-topics/"),
                BreadCrumb::n(true,"Create new topic","/extorio-admin/user-contact-topics/edit","plus",!$this->canCreate)
            ));
        }

        if(isset($_POST["submitted"]) || isset($_POST["submitted_exit"])) {
            $t = UserContactTopic::findById($id);
            if(!$t) {
                $t = UserContactTopic::n();
            }
            $t->name = $_POST["name"];
            $t->description = $_POST["description"];
            $t->extensionName = $_POST["extension"]?:$t->extensionName;

            $error = false;

            try {
                $t->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                $this->_messageError($error);
            } else {
                $this->_messageSuccess("Contact topic created");
                if(isset($_POST["submitted_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit", array($t->id));
                }
            }
        }

        $this->extensions = $this->_Extorio()->getExtensions();
    }

    public function delete($id = false) {
        if(!$id) {
            $this->_redirectToDefault();
        }
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete contact topics");
            $this->_redirectToDefault();
        }
        $t = UserContactTopic::findById($id);
        if($t) {
            $t->deleteThis();
        }
        $this->_messageInfo("Contact topic deleted");
        $this->_redirectToDefault();
    }
}