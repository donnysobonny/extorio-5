<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\User;
use Core\Classes\Utilities\Users;

/**
 * 
 *
 * Class UserForgotPassword
 */
class UserForgotPassword extends \Core\Classes\Commons\Controller {

    public $passwordResetEnabled = false;

    public function _onDefault() {
        $config = $this->_Extorio()->getConfig();
        $this->passwordResetEnabled = $config["users"]["password_reset"]["enabled"];
        if(isset($_POST["forgot_password_submitted"]) && $this->passwordResetEnabled) {
            $username = $_POST["username"];
            //try and find the user
            $user = User::findOne(
                Query::n()->where(array(
                    '$or' => array(
                        "username" => $username,
                        "email" => $username
                    )
                )),1
            );
            if($user) {
                //send the email
                Users::sendPasswordResetEmail($user);
            }

            $this->_Extorio()->messageInfo("Your request has been submitted. If a user account can be located by the information provided, an email will be sent to you shortly containing a link to reset your password");
        }
    }
}