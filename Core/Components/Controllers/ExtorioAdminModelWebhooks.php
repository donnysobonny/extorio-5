<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\Extension;
use Core\Classes\Enums\ModelWebhookActions;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\Model;
use Core\Classes\Models\ModelWebhook;
use Core\Classes\Models\User;
use Core\Classes\Utilities\Models;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

class ExtorioAdminModelWebhooks extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModify;
    public $canDelete;
    /**
     * @var ModelWebhook[]
     */
    public $webhooks = array();
    /**
     * @var ModelWebhook
     */
    public $webhook;
    /**
     *
     * @var Model[]
     */
    public $models = array();
    public $userList = array();
    /**
     * @var Extension[]
     */
    public $extensions = array();
    /**
     * @var SimpleFiltering
     */
    public $f;

    public function _onBegin() {
        if (!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id, array("extorio_pages_all", "extorio_pages_model_webhooks"))) {
            $this->_redirectTo401AccessDeniedPage(array("r" => Server::getRequestURI(), array(), 401));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(), "model_webhooks_create", "Core");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(), "model_webhooks_modify", "Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(), "model_webhooks_delete", "Core");
    }

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Model Webhooks","/extorio-admin/model-webhooks"),
                BreadCrumb::n(false,"Create new webhook","/extorio-admin/model-webhooks/edit","plus",!$this->canCreate)
            )
        );

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit", array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $actions = ModelWebhookActions::values();
        $filters = array(
            "any" => "Any action"
        );
        $this->f->addFilter("action",$filters);
        $this->f->addFilter("1",null);
        $this->f->addFilter("2",null);
        foreach($actions as $action) {
            $filters[$action] = Strings::titleSafe($action);
        }
        $this->f->setSearchTypes(array(
            "name" => "Name",
            "model" => "Model",
            "extension" => "Extension"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getFilter("action"))) {
            if($this->f->getFilter("action") != "any") {
                $where[] = array("action" => $this->f->getFilterLeft());
            }
        }
        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[] = array("name" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "model" :
                    $where[] = array("modelNamespace" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $queryCount = clone($query);

        $query->order("name");
        $query->limit($this->f->getLimit());
        $query->skip($this->f->getLimit() * $this->f->getPage());

        $this->webhooks = ModelWebhook::findAll($query,1);
        $this->f->setCount(ModelWebhook::findCount($queryCount));
    }

    public function edit($id=false) {
        if($id) {
            if(!$this->canModify) {
                $this->_messageWarning("You are not able to modify webhooks");
                $this->_redirectToDefault();
            }
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create webhooks");
                $this->_redirectToDefault();
            }
        }

        $this->models = Model::findAll(Query::n()->order(array("label")));
        $sql = "SELECT id, username FROM ".Models::getTableName(User::getClass())." ORDER BY username ASC";
        $this->userList = $this->_Extorio()->getDbInstanceDefault()->query($sql);
        $this->extensions = $this->_Extorio()->getExtensions();

        if(isset($_POST["webhook_edited"]) || isset($_POST["webhook_edited_exit"])) {
            $wh = ModelWebhook::findById($id);
            if(!$wh) {
                $wh = ModelWebhook::n();
            }
            $wh->name = $_POST["name"];
            $wh->action = $_POST["action"];
            $wh->modelNamespace = $_POST["model"];
            $wh->location = $_POST["location"];
            $wh->verificationKey = $_POST["key"];
            $wh->extensionName = $_POST["extension"]?:$wh->extensionName;

            $error = false;
            try {
                $wh->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                $this->_Extorio()->messageError($error);
            } else {
                $this->_Extorio()->messageSuccess("Webhook saved");
                if(isset($_POST["webhook_edited_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit",array($wh->id));
                }
            }
        }

        $this->webhook = ModelWebhook::findById($id);

        if($this->webhook) {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Model Webhooks","/extorio-admin/model-webhooks"),
                    BreadCrumb::n(true,$this->webhook->name)
                )
            );
        } else {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Model Webhooks","/extorio-admin/model-webhooks"),
                    BreadCrumb::n(true,"Create new webhook","","plus")
                )
            );
        }
    }

    public function delete($id=false) {

        if(!$id) {
            $this->_redirectToDefault();
        }
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete webhooks");
            $this->_redirectToDefault();
        }

        $wh = ModelWebhook::findById($id);
        if($wh) {
            $wh->deleteThis();
        }

        $this->_Extorio()->messageSuccess("Webhook deleted");
        $this->_redirectToDefault();
    }
}