<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\UserGroup;
use Core\Classes\Models\UserPrivilege;
use Core\Classes\Models\UserPrivilegeCategory;
use Core\Classes\Utilities\Arrays;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

/**
 * Manage user groups
 *
 * Class AdminUserGroups
 */
class AdminUserGroups extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModify;
    public $canDelete;
    /**
     * @var UserGroup[]
     */
    public $groups = array();
    /**
     * @var SimpleFiltering
     */
    public $f;
    /**
     * @var UserGroup
     */
    public $group;
    /**
     * @var UserPrivilegeCategory[]
     */
    public $categories = array();
    /**
     * @var Extension[]
     */
    public $extensions = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_user_groups"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_groups_create","Core");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_groups_modify","Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_groups_delete","Core");
    }

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"User Groups","/extorio-admin/user-groups/"),
                BreadCrumb::n(false,"Create new user group","/extorio-admin/user-groups/edit","plus",!$this->canCreate)
            )
        );

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit", array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $privs = UserPrivilege::findAll(Query::n()->order(array(
            "category.name" => "asc",
            "name" => "asc"
        )),1);
        $filterls = array(
            "any" => "Any privileges"
        );
        foreach($privs as $priv) {
            $filterls[$priv->id] = $priv->name;
        }
        $this->f->addFilter("with-privilege",$filterls);
        $groups = UserGroup::findAll(Query::n()->order("name"),1);
        $filterrs = array(
            "any" => "Any groups"
        );
        foreach($groups as $group) {
            $filterrs[$group->id] = $group->name;
        }
        $this->f->addFilter("manages",$filterrs);
        $this->f->setSearchTypes(array(
            "name" => "Name",
            "extension" => "Extension"
        ));
        $this->f->addFilter("1",null);

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getFilter("with-privilege"))) {
            if($this->f->getFilter("with-privilege") != "any") {
                $where[Query::_and][] = array("privileges.id" => intval($this->f->getFilter("with-privilege")));
            }
        }
        if(strlen($this->f->getFilter("manages"))) {
            if($this->f->getFilter("manages") != "any") {
                $where[Query::_or][] = array("manageAll" => true);
                $where[Query::_or][] = array("manageGroups.id" => intval($this->f->getFilter("manages")));
            }
        }
        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[Query::_and][] = array("name" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[Query::_and][] = array("extensionsName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $queryCount = clone($query);

        $query->order("name");
        $query->limit($this->f->getLimit());
        $query->skip($this->f->getLimit() * $this->f->getPage());

        $this->groups = UserGroup::findAll($query,2);
        $this->f->setCount(UserGroup::findCount($queryCount));
    }

    public function edit($id = false) {
        if($id) {
            if(!$this->canModify) {
                $this->_messageWarning("You are not able to modify user groups");
                $this->_redirectToDefault();
            }
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create user groups");
                $this->_redirectToDefault();
            }
        }

        $this->group = UserGroup::findById($id,1);
        $this->groups = UserGroup::findAll(
            Query::n()
                ->where(array(
                    "id" => array(
                        Query::_ne => $id
                    )
                ))
                ->order("name")
            ,1
        );
        $this->categories = UserPrivilegeCategory::findAll(Query::n()->order("name"));

        if(isset($_POST["submit"]) || isset($_POST["submit_exit"])) {
            $g = UserGroup::findById($id,1);
            if(!$g) {
                $g = UserGroup::n();
            }
            $g->name = $_POST["name"];
            $g->description = $_POST["description"];
            $g->manageAll = isset($_POST["manage_all"]);
            $g->manageGroups = array();
            if(isset($_POST["manage_groups"])) {
                $g->manageGroups = Arrays::smartCast($_POST["manage_groups"]);
            }
            $g->privileges = array();
            if(isset($_POST["privileges"])) {
                $g->privileges = Arrays::smartCast($_POST["privileges"]);
            }
            $g->extensionName = $_POST["extension"]?:$g->extensionName;

            $error = false;
            try {
                $g->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                $this->_messageError($error);
            } else {
                $this->_messageSuccess("User Group Saved");
                if(isset($_POST["submit_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit",array($g->id));
                }
            }
        }

        if($this->group) {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"User Groups","/extorio-admin/user-groups/"),
                    BreadCrumb::n(true,$this->group->name)
                )
            );
        } else {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"User Groups","/extorio-admin/user-groups/"),
                    BreadCrumb::n(true,"Create new user group","/extorio-admin/user-groups/edit","plus")
                )
            );
        }
        $this->extensions = $this->_Extorio()->getExtensions();
    }

    public function delete($id = false) {
        if(!$id) {
            $this->_redirectToDefault();
        }
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete user groups");
            $this->_redirectToDefault();
        }
        $error = false;
        try{
            $g = UserGroup::findById($id);
            if($g) {
                $g->deleteThis();
            }
        } catch(\Exception $ex) {
            $error = $ex->getMessage();
        }

        if($error) {
            $this->_messageError($error);
        } else {
            $this->_messageSuccess("User Group Deleted");
        }

        $this->_redirectToDefault();
    }
}