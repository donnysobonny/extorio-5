<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Models\BlockCategory;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Users;

/**
 * Manage block categories
 *
 * Class AdminBlockCategories
 */
class AdminBlockCategories extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModify;
    public $canDelete;

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_block_categories"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(),"block_categories_create","Core");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(),"block_categories_modify","Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(),"block_categories_delete","Core");
    }

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(array(
            BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
            BreadCrumb::n(true,"Block Categories","/extorio-admin/block-categories/"),
            BreadCrumb::n(false,"Create new block category","/extorio-admin/block-categories/edit/","plus",!$this->canCreate)
        ));
    }

    /**
     * @var BlockCategory
     */
    public $category;

    public function edit($id = false) {

    }

    public function delete($id = false) {

    }

    public function sub_edit($parentId = false, $subId = false) {

    }

    public function sub_delete($subId = false) {

    }
}