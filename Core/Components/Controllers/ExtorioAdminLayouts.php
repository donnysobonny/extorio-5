<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\Extension;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\Layout;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

class ExtorioAdminLayouts extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModify;
    public $canDelete;
    /**
     * @var Layout[]
     */
    public $layouts = array();
    /**
     * @var Layout
     */
    public $layout;
    /**
     * @var Extension[]
     */
    public $extensions = array();
    /**
     * @var SimpleFiltering
     */
    public $f;

    public function _onBegin() {
        if (!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id, array("extorio_pages_all", "extorio_pages_layouts"))) {
            $this->_redirectTo401AccessDeniedPage(array("r" => Server::getRequestURI(), array(), 401));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(), "layouts_create", "Core");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(), "layouts_modify", "Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(), "layouts_delete", "Core");
    }

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Layouts","/extorio-admin/layouts"),
                BreadCrumb::n(false,"Create new layout","/extorio-admin/layouts/edit","plus",!$this->canCreate)
            )
        );

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit", array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $this->f->addFilter("1",null);
        $this->f->addFilter("2",null);
        $this->f->addFilter("3",null);
        $this->f->setSearchTypes(array(
            "name" => "Name",
            "extension" => "Extension"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[] = array("name" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $queryCount = clone($query);

        $query->order("name");
        $query->limit($this->f->getLimit());
        $query->skip($this->f->getLimit() * $this->f->getPage());

        $this->layouts = Layout::findAll($query);
        $this->f->setCount(Layout::findCount($queryCount));
    }

    public function edit($layoutId=false) {
        if($layoutId) {
            if(!$this->canModify) {
                $this->_messageWarning("You are not able to modify layouts");
                $this->_redirectToDefault();
            }
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create layouts");
                $this->_redirectToDefault();
            }
        }

        //turn on edit mode
        $this->_Extorio()->setDispatchSetting("dispatch_layout_edit_mode",true);

        $this->layout = Layout::findById($layoutId);

        if($this->layout) {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Layouts","/extorio-admin/layouts"),
                    BreadCrumb::n(true,$this->layout->name)
                )
            );
        } else {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Layouts","/extorio-admin/layouts"),
                    BreadCrumb::n(true,"Create new layout","","plus")
                )
            );
        }
        $this->extensions = $this->_Extorio()->getExtensions();
    }

    public function delete($layoutId=false) {
        if(!$layoutId) {
            $this->_redirectToDefault();
        }
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete this layout");
            $this->_redirectToDefault();
        }

        $l = Layout::findById($layoutId);
        if($l) {
            $l->deleteThis();
        }

        $this->_Extorio()->messageSuccess("Layout successfully deleted");
        $this->_redirectToDefault();
    }
}