<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\UserActionType;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

/**
 * Manage the user action types
 *
 * Class AdminUserActionTypes
 */
class AdminUserActionTypes extends \Core\Classes\Commons\Controller {
    public $canCreate;
    public $canModify;
    public $canDelete;
    /**
     * @var SimpleFiltering
     */
    public $f;
    /**
     * @var UserActionType[]
     */
    public $actionTypes;
    /**
     * @var UserActionType
     */
    public $actionType;
    /**
     * @var Extension[]
     */
    public $extensions = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_apis"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_action_types_create","Core");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_action_types_modify","Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_action_types_delete","Core");
    }

    public function _onDefault() {
        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit", array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $this->f->addEmpty();
        $this->f->addEmpty();
        $this->f->addEmpty();
        $this->f->setSearchTypes(array(
            "name" => "Name",
            "extension" => "Extension"
        ));
        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[] = array("name" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $clone = clone($query);

        $query->order("name")->limit($this->f->getLimit())->skip($this->f->getOffset());

        $this->actionTypes = UserActionType::findAll($query);
        $this->f->setCount(UserActionType::findCount($clone));

        $this->_Extorio()->setTargetBreadCrumbs(array(
            BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
            BreadCrumb::n(true,"User Action Types","/extorio-admin/user-action-types/"),
            BreadCrumb::n(false,"Create new action type","/extorio-admin/user-action-types/edit/","plus",!$this->canCreate),
        ));
    }

    public function edit($id = false) {
        $this->actionType = UserActionType::findById($id);
        if($this->actionType) {
            if(!$this->canModify) {
                $this->_messageWarning("You are not able to modify user action types");
                $this->_redirectToDefault();
            }
            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"User Action Types","/extorio-admin/user-action-types/"),
                BreadCrumb::n(true,$this->actionType->name),
            ));
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create user action types");
                $this->_redirectToDefault();
            }
            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"User Action Types","/extorio-admin/user-action-types/"),
                BreadCrumb::n(true,"Create new action type","/extorio-admin/user-action-types/edit/","plus",!$this->canCreate),
            ));
        }

        if(isset($_POST["submitted"]) || isset($_POST["submitted_exit"])) {
            $t = UserActionType::findById($id);
            if(!$t) {
                $t = UserActionType::n();
            }
            $t->name = $_POST["name"];
            $t->description = $_POST["description"];
            $t->detail1Description = $_POST["detail1"];
            $t->detail2Description = $_POST["detail2"];
            $t->detail3Description = $_POST["detail3"];
            $t->extensionName = $_POST["extension"]?:$t->extensionName;

            $error = false;

            try {
                $t->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                $this->_messageError($error);
            } else {
                $this->_messageSuccess("User action saved");
                if(isset($_POST["submitted_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit",array($t->id));
                }
            }
        }

        $this->extensions = $this->_Extorio()->getExtensions();
    }

    public function delete($id = false) {
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete user action types");
            $this->_redirectToDefault();
        }
        $t = UserActionType::findById($id);
        if(!$t) {
            $this->_redirectToDefault();
        }
        $t->deleteThis();
        $this->_messageInfo("User action deleted");
        $this->_redirectToDefault();
    }
}