<?php
namespace Core\Components\Controllers;
use Core\Classes\Enums\FontAwesomeIcons;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\GeneralAccessPoint;
use Core\Classes\Models\Template;
use Core\Classes\Models\UserGroup;
use Core\Classes\Models\UserRole;
use Core\Classes\Utilities\Arrays;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Templates;
use Core\Classes\Utilities\Users;

class ExtorioAdminTemplates extends \Core\Classes\Commons\Controller {

    public $canCreate = false;
    public $canDeleteAll = false;
    /**
     *
     * @var Template[]
     */
    public $templates;
    /**
     * @var SimpleFiltering
     */
    public $f;
    /**
     *
     * @var Template
     */
    public $template;
    /**
     * @var UserGroup[]
     */
    public $groups;
    /**
     * @var Extension[]
     */
    public $extensions = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_templates"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUser()->id,"templates_create","Core");
        $this->canDeleteAll = Users::userHasPrivilege($this->_getLoggedInUser()->id,"templates_delete_all","Core");
    }

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Templates","/extorio-admin/templates"),
                BreadCrumb::n(false,"Create new template","/extorio-admin/templates/edit",FontAwesomeIcons::_plus,!$this->canCreate)
            )
        );

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit", array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $this->f->addFilter("state", array(
            "any" => "Any state",
            "default" => "Default template",
            "hidden" => "Hidden templates"
        ));
        $this->f->addFilter("1",null);
        $this->f->addFilter("2",null);
        $this->f->setSearchTypes(array(
            "name" => "Name",
            "extension" => "Extension"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getFilter("state"))) {
            switch($this->f->getFilter("state")) {
                case "default" :
                    $where[] = array("isHidden" => false);
                    $where[] = array("default" => true);
                    break;
                case "hidden" :
                    $where[] = array("isHidden" => true);
                    break;
                default :
                    $where[] = array("isHidden" => false);
                    break;
            }
        } else {
            $where[] = array("isHidden" => false);
        }
        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[] = array("name" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }
        $query->where($where);
        $queryCount = clone($query);

        $query->order("name");
        $query->limit($this->f->getLimit());
        $query->skip($this->f->getLimit() * $this->f->getPage());

        $this->templates = Template::findAll($query,1);
        $this->f->setCount(Template::findCount($queryCount));
    }

    public function delete($templateId=false) {
        if(!$templateId) {
            $this->_redirectToDefault();
        }
        if(!$this->canDeleteAll) {
            $this->_messageWarning("You are not able to delete this template");
            $this->_redirectToDefault();
        }
        $t = Template::findById($templateId);
        if($t) {
            $t->deleteThis();
        }
        $this->_Extorio()->messageInfo("Template successfully deleted");
        $this->_redirectToDefault();
    }

    public function edit($templateId=false) {
        if($templateId) {
            if(!Templates::canUserModifyTemplate($this->_getLoggedInUser()->id,$templateId)) {
                $this->_messageWarning("You are not able to modify this template");
                $this->_redirectToDefault();
            }
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create templates");
                $this->_redirectToDefault();
            }
        }

        if(isset($_POST["template_updated"]) || isset($_POST["template_updated_exit"])) {
            $t = Template::findById($templateId,2);
            if(!$t) {
                $t = Template::n();
            }
            $t->name = $_POST["name"];
            $t->description = $_POST["description"];
            $t->default = isset($_POST["default"]);
            $t->isHidden = isset($_POST["hidden"]);

            $t->prependToHead = $_POST["prependtohead"];
            $t->appendToHead = $_POST["appendtohead"];
            $t->prependToBody = $_POST["prependtobody"];
            $t->appendToBody = $_POST["appendtobody"];
            $t->extensionName = $_POST["extension"]?:$t->extensionName;
            if(isset($_POST["groups"])) {
                $t->modifyGroups = Arrays::smartCast($_POST["groups"]);
            } else {
                $t->modifyGroups = array();
            }

            $error = false;
            try{
                $t->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_Extorio()->messageError($error);
            } else {
                $this->_Extorio()->messageSuccess("Template successfully saved");
                if(isset($_POST["template_updated_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit",array($t->id));
                }
            }
        }

        $this->groups = UserGroup::findAll(Query::n()->order("name"));
        $this->template = Template::findById($templateId,1);

        if($this->template) {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Templates","/extorio-admin/templates"),
                    BreadCrumb::n(true,$this->template->name)
                )
            );
        } else {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Templates","/extorio-admin/templates"),
                    BreadCrumb::n(true,"+ Create new template...")
                )
            );
        }
        $this->extensions = $this->_Extorio()->getExtensions();
    }
}