<?php
namespace Core\Components\Controllers;
use Core\Classes\Enums\InternalEvents;
use Core\Classes\Models\Page;
use Core\Classes\Utilities\Server;

/**
 * 
 *
 * Class MyAccount
 */
class MyAccount extends \Core\Classes\Commons\Controller {
    public function _onBegin() {
        if(!$this->_Extorio()->getLoggedInUser()) {
            Page::findUserLogin()->redirect(array(
                "r" => Server::getRequestURI()
            ));
        }
    }

    public function _onDefault() {
        $this->_Extorio()->getEvent(InternalEvents::_my_account_controller)->run();
    }
}