<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\BlockProcessor;
use Core\Classes\Utilities\BlockProcessors;
use Core\Classes\Utilities\Components;

/**
 * Entry point for processing block processors
 *
 * Class ExtorioBlockProcessors
 */
class ExtorioBlockProcessors extends \Core\Classes\Commons\Controller {
    public $inline;

    public function _onBegin() {
        //disable auto render
        $this->_setDispatchSetting_AutoRender(false);
    }

    public function _onDefault($bpi = false) {
        if($bpi) {
            //processing the block processor based on the GET params
            $processor = BlockProcessor::findOne(
                Query::n()
                    ->where(array(
                        "inlineEnabled" => true,
                        Query::_or => array(
                            "label" => $bpi,
                            "class" => $bpi,
                            "namespace" => $bpi
                        )
                    )),2
            );
            if($processor) {
                $loggedInUser = $this->_getLoggedInUser();
                if(BlockProcessors::canUserInlineBlockProcessor($loggedInUser ? $loggedInUser->id : 0,$processor->id)) {
                    $extension = $this->_Extorio()->getExtension($processor->extensionName);
                    if($extension) {
                        $bp = $extension->_constructComponent($processor->namespace);
                        if($bp) {
                            echo $this->_Extorio()->fetchBlockProcessorViewContent($bp, $_GET, null, true);
                        }
                    }
                }
            }
        }
    }
}