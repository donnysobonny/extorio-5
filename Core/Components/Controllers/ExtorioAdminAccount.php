<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Commons\FileSystem\Image;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Models\User;
use Core\Classes\Utilities\Users;

class ExtorioAdminAccount extends \Core\Classes\Commons\Controller {
    public function _onBegin() {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Account","/extorio-admin/account"),
            )
        );
    }

    /**
     *
     * @var User
     */
    public $loggedInUser;
    public $canChangeUsername = false;
    public $canChangePassword = false;
    public $canChangeEmail = false;

    public function _onDefault() {
        $config = $this->_Extorio()->getConfig();
        $this->loggedInUser = $this->_Extorio()->getLoggedInUser();
        $this->canChangeUsername = $config["users"]["can_change_username"];
        $this->canChangePassword = $config["users"]["can_change_password"];
        $this->canChangeEmail = $config["users"]["can_change_email"];

        if(isset($_POST["user_basic_details_submit"])) {
            if(strlen($_FILES["avatar"]["name"])) {
                $file = $_FILES["avatar"];
                //check the image type
                $check = File::constructFromPath($file["name"]);
                if(!in_array($check->extension(), array(
                    "jpg",
                    "jpeg",
                    "png",
                    "gif"
                ))) {
                    $this->_Extorio()->messageError("The avatar image is not a valid file type");
                    $this->_redirectToDefault();
                }
                //try to upload
                if(move_uploaded_file($file["tmp_name"],"Application/Assets/images/avatars/".$this->loggedInUser->id."_avatar.".$check->extension())) {
                    $image = Image::constructFromPath("Application/Assets/images/avatars/".$this->loggedInUser->id."_avatar.".$check->extension());
                    //set the smallest
                    $image->resizeSmallestSide(150);
                    //crop
                    $image->cropAtCenter(150,150);
                    $image->save();

                    $this->loggedInUser->avatar = "/Application/Assets/images/avatars/".$this->loggedInUser->id."_avatar.".$check->extension();
                }
            }

            $this->loggedInUser->firstname = $_POST["firstname"];
            $this->loggedInUser->lastname = $_POST["lastname"];

            $error = false;
            try {
                $this->loggedInUser->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                $this->_Extorio()->messageError($error);
            } else {
                $this->_Extorio()->messageSuccess("Details successfully updated");
            }
        }

        if(isset($_POST["user_change_username_submit"]) && $this->canChangeUsername) {
            $error = false;
            try{
                Users::changeUsername($this->_Extorio()->getLoggedInUser(),$_POST["username"]);
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_Extorio()->messageError($error);
            } else {
                $this->_Extorio()->messageSuccess("Username successfully updated");
            }
        }

        if(isset($_POST["user_change_email_submit"]) && $this->canChangeEmail) {
            $error = false;
            try{
                Users::changeEmail($this->_Extorio()->getLoggedInUser(),$_POST["email"]);
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_Extorio()->messageError($error);
            } else {
                $this->_Extorio()->messageSuccess("Email successfully updated");
            }
        }

        if(isset($_POST["user_change_password_submit"]) && $this->canChangePassword) {

            $password = $_POST["password"];
            $npassword = $_POST["npassword"];
            $cpassword = $_POST["cpassword"];

            $error = false;
            try{
                $user = $this->_Extorio()->getLoggedInUser();

                if($user->password != $password) {
                    throw new \Exception("There was a problem when trying to update your password. Please try again.");
                }

                if($npassword != $cpassword) {
                    throw new \Exception("There was a problem when trying to update your password. Please try again.");
                }

                Users::changePassword($user,$npassword);
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_Extorio()->messageError($error);
            } else {
                $this->_Extorio()->messageSuccess("Password successfully updated");
            }
        }
    }

    public function _onEnd() {

    }
}