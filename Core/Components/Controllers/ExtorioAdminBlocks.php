<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\Extension;
use Core\Classes\Enums\FontAwesomeIcons;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\Block;
use Core\Classes\Models\BlockCategory;
use Core\Classes\Models\BlockProcessor;
use Core\Classes\Models\Category;
use Core\Classes\Models\GeneralAccessPoint;
use Core\Classes\Models\UserGroup;
use Core\Classes\Models\UserRole;
use Core\Classes\Utilities\Arrays;
use Core\Classes\Utilities\Blocks;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

class ExtorioAdminBlocks extends \Core\Classes\Commons\Controller {

    public $canCreate = false;
    public $canDeleteAll = false;
    /**
     *
     * @var Block[]
     */
    public $blocks = array();
    /**
     * @var SimpleFiltering
     */
    public $f;
    /**
     *
     * @var Block
     */
    public $block = false;
    /**
     *
     * @var UserGroup[]
     */
    public $groups = array();
    /**
     * @var BlockProcessor[]
     */
    public $processors = array();
    /**
     * @var Extension[]
     */
    public $extensions = array();
    /**
     *
     * @var BlockCategory[]
     */
    public $categoryTree = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_blocks"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUser()->id,"blocks_create","Core");
        $this->canDeleteAll = Users::userHasPrivilege($this->_getLoggedInUser()->id,"blocks_delete_all","Core");
    }

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Blocks","/extorio-admin/blocks"),
                BreadCrumb::n(false,"Create new block","/extorio-admin/blocks/edit",FontAwesomeIcons::_plus,!$this->canCreate)
            )
        );

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit", array(
            10 => 10,
            25 => 25,
            100 => 100,
            250 => 250
        ));
        $this->f->addFilter("state",array(
            "any" => "Any state",
            "public" => "Public blocks",
            "unpublic" => "Unpublic blocks",
            "hidden" => "Hidden blocks"
        ));
        $tree = Blocks::getBlockCategoryTree();
        $filters = array();
        $filters["any"] = "Any category";
        foreach($tree as $main) {
            foreach($main->subCategories as $sub) {
                $filters[$main->name][$sub->id] = $main->name." / ".$sub->name;
            }
        }
        $this->f->addFilter("category",$filters);
        $this->f->addFilter("1",null);

        $this->f->setSearchTypes(array(
            "name" => "Name",
            "extension" => "Extension"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getFilter("state"))) {
            switch($this->f->getFilter("state")) {
                case "public" :
                    $where[] = array("isPublic"=>true);
                    $where[] = array("isHidden"=>false);
                    break;
                case "unpublic" :
                    $where[] = array("isPublic"=>false);
                    $where[] = array("isHidden"=>false);
                    break;
                case "hidden" :
                    $where[] = array("isHidden"=>true);
                    break;
                default :
                    $where[] = array("isHidden"=>false);
                    break;
            }
        } else {
            $where[] = array("isHidden"=>false);
        }
        if(strlen($this->f->getFilter("category"))) {
            if($this->f->getFilter("category") != "any") {
                $where[] = array("subCategory.id"=>intval($this->f->getFilter("category")));
            }
        }
        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[] = array("name"=>array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName"=>array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $queryCount = clone($query);

        $query->order("name");
        $query->limit($this->f->getLimit());
        $query->skip($this->f->getLimit() * $this->f->getPage());

        $this->blocks = Block::findAll($query,2);
        $this->f->setCount(Block::findCount($queryCount));
    }

    public function delete($blockId=false) {
        if(!$blockId) {
            $this->_redirectToDefault();
        }
        if(!$this->canDeleteAll) {
            $this->_messageWarning("You are not able to delete this block");
            $this->_redirectToDefault();
        }

        $b = Block::findById($blockId);
        if($b) {
            $b->deleteThis();
        }

        $this->_Extorio()->messageSuccess("Block deleted successfully");
        $this->_redirectToDefault();
    }

    public function edit($blockId=false) {
        if($blockId) {
            if(!Blocks::canUserModifyBlock($this->_getLoggedInUserId(),$blockId)) {
                $this->_messageWarning("You are not able to modify this block");
                $this->_redirectToDefault();
            }
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create blocks");
                $this->_redirectToDefault();
            }
        }

        if(isset($_POST["block_updated"]) || isset($_POST["block_updated_exit"])) {
            $b = Block::findById($blockId,2);
            if(!$b) {
                $b = Block::n();
            }
            $b->name = $_POST["name"];
            $b->description = $_POST["description"];
            $b->isHidden = isset($_POST["hidden"]);
            $b->isPublic = isset($_POST["public"]);
            $cparts = explode(":",$_POST["category"]);
            $b->category = intval($cparts[0]);
            $b->subCategory = intval($cparts[1]);
            $b->allRead = isset($_POST["all_read"]);
            if(isset($_POST["read_groups"])) {
                $b->readGroups = Arrays::smartCast($_POST["read_groups"]);
            } else {
                $b->readGroups = array();
            }
            if(isset($_POST["modify_groups"])) {
                $b->modifyGroups = Arrays::smartCast($_POST["modify_groups"]);
            } else {
                $b->modifyGroups = array();
            }
            $b->processor = BlockProcessor::findById($_POST["processor"]);
            $b->extensionName = $_POST["extension"]?:$b->extensionName;

            $error = false;
            try {
                $b->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_Extorio()->messageError($error);
            } else {
                $this->_Extorio()->messageSuccess("Block saved successfully");
                if(isset($_POST["block_updated_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit",array($b->id));
                }
            }
        }

        $this->block = Block::findById($blockId,1);
        $this->groups = UserGroup::findAll(Query::n()->order(array("name"=>"asc")));
        $this->processors = BlockProcessor::findAll(
            Query::n()->order(array("label")),1
        );
        $this->categoryTree = Blocks::getBlockCategoryTree();

        if($this->block) {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Blocks","/extorio-admin/blocks"),
                    BreadCrumb::n(true,$this->block->name)
                )
            );
        } else {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Blocks","/extorio-admin/blocks"),
                    BreadCrumb::n(true,"Create new block","","plus")
                )
            );
        }
        $this->extensions = $this->_Extorio()->getExtensions();
    }
}