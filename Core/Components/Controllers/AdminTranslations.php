<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\Language;
use Core\Classes\Models\TranslationBase;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;
use Core\Components\Extension\Extension;

/**
 * Manage translations and translation bases
 *
 * Class AdminTranslations
 */
class AdminTranslations extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModify;
    /**
     * @var SimpleFiltering
     */
    public $f;
    /**
     * @var TranslationBase[]
     */
    public $translationBases;
    /**
     * @var TranslationBase
     */
    public $translationBase;
    /**
     * @var Extension
     */
    public $extensions;
    /**
     * @var Language[]
     */
    public $languages = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_translations"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(),"translation_base_create");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(),"translation_base_modify");
    }

    public function _onDefault() {
        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit",array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $this->f->addEmpty();
        $this->f->addEmpty();
        $this->f->addEmpty();
        $this->f->setSearchTypes(array(
            "msgid" => "Msgid",
            "extension" => "Extension"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "msgid" :
                    $where[] = array("msgid" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $clone = clone($query);

        $query->order("msgid")->limit($this->f->getLimit())->skip($this->f->getOffset());

        $this->translationBases = TranslationBase::findAll($query);
        $this->f->setCount(TranslationBase::findCount($clone));

        $this->_Extorio()->setTargetBreadCrumbs(array(
            BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
            BreadCrumb::n(true,"Translations","/extorio-admin/translations/"),
            BreadCrumb::n(false,"Create new translation base","/extorio-admin/translations/edit","plus",!$this->canCreate)
        ));
    }

    public function edit($id = false) {
        $this->translationBase = TranslationBase::findById($id);
        if(!$this->translationBase) {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create translation bases");
                $this->_redirectToDefault();
            }
            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"Translations","/extorio-admin/translations/"),
                BreadCrumb::n(true,"Create new translation base","/extorio-admin/translations/edit","plus",!$this->canCreate)
            ));
        } else {
            if(!$this->canModify) {
                $this->_messageWarning("You are not able to modify translation bases");
                $this->_redirectToDefault();
            }
            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"Translations","/extorio-admin/translations/"),
                BreadCrumb::n(true,$this->translationBase->msgid)
            ));
        }

        $languages = Language::findAll(Query::n()->order("name"));
        foreach($languages as $language) {
            $this->languages[$language->id] = $language;
        }

        if(isset($_POST["submit"]) || isset($_POST["submit_exit"])) {
            $tb = TranslationBase::findById($id);
            if(!$tb) {
                $tb = TranslationBase::n();
            }
            $tb->plural = isset($_POST["plural"]);
            $tb->msgid = $_POST["msgid"];
            $tb->msgid_plural = $_POST["msgid_plural"];
            $tb->extensionName = $_POST["extension"]?:$tb->extensionName;

            $error = false;
            try {
                $tb->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                $this->_messageError($error);
            } else {
                $this->_messageSuccess("Translation base saved");
                if(isset($_POST["submit_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit",array($tb->id));
                }
            }
        }

        $this->extensions = $this->_Extorio()->getExtensions();
    }

    public function delete($id = false) {
        $tb = TranslationBase::findById($id);
        if(!$tb) {
            $this->_redirectToDefault();
        }
        if(!Users::userHasPrivilege($this->_getLoggedInUserId(),"translation_base_delete")) {
            $this->_messageWarning("You are not able to delete this translation base");
            $this->_redirectToDefault();
        }
        $tb->deleteThis();
        $this->_messageSuccess("Translation base deleted");
        $this->_redirectToDefault();
    }
}