<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Commons\FileSystem\Image;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\UserGroup;
use Core\Classes\Models\UserRole;
use Core\Classes\Utilities\Arrays;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Users;

/**
 * Manage the settings related to users
 *
 * Class AdminUserSettings
 */
class AdminUserSettings extends \Core\Classes\Commons\Controller {
    public $config = array();
    /**
     * @var UserGroup[]
     */
    public $groups = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_user_settings"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
    }

    public function _onDefault() {
        $this->groups = UserGroup::findAll(Query::n()->order("name"));

        if(isset($_POST["settings_updated"])) {
            $core = $this->_Extorio()->getExtension("Core");

            $this->config = $this->_Extorio()->getConfig();

            $avatar = $this->config["users"]["defaults"]["avatar"];
            if(strlen($_FILES["default_avatar"]["name"])) {
                $file = $_FILES["default_avatar"];
                $check = File::constructFromPath($file["name"]);
                if(!in_array($check->extension(), array(
                    "jpg",
                    "jpeg",
                    "png",
                    "gif"
                ))) {
                    $this->_Extorio()->messageError("The default avatar is not a valid file type");
                    $this->_redirectToDefault();
                }
                if(move_uploaded_file($file["tmp_name"],"Application/Assets/images/".$file["name"])) {
                    $image = Image::constructFromPath("Application/Assets/images/".$file["name"]);
                    $image->resizeSmallestSide(150);
                    $image->cropAtCenter(150,150);
                    $image->save();

                    $avatar = "/Application/Assets/images/".$file["name"];
                }
            }

            if($core) {
                $core->_mergeConfigOverride(array(
                    "extorio" => array(
                        "authentication" => array(
                            "session_type" => $_POST["extorio_authentication_session_type"],
                            "jwt" => array(
                                "key" => $_POST["extorio_authentication_jwt_key"],
                                "alg" => $_POST["extorio_authentication_jwt_alg"]
                            )
                        )
                    ),
                    "users" => array(
                        "user_groups" => array(
                            "assign_type" => $_POST["user_group_assign_type"],
                            "group" => intval($_POST["user_group"]),
                            "groups" => Arrays::smartCast($_POST["user_groups"])
                        ),
                        "passwords" => array(
                            "min_length" => intval($_POST["passwords_min_length"]),
                            "contain_special" => isset($_POST["passwords_contain_special"]),
                            "contain_number" => isset($_POST["passwords_contain_number"])
                        ),
                        "usernames" => array(
                            "allow_number" => isset($_POST["usernames_allow_number"]),
                            "allow_special" => isset($_POST["usernames_allow_special"])
                        ),
                        "emails" => array(
                            "validate_emails" => isset($_POST["usernames_allow_special"])
                        ),
                        "defaults" => array(
                            "avatar" => $avatar
                        ),
                        "registration" => array(
                            "enabled" => isset($_POST["registration_enabled"]),
                            "authenticate_users" => isset($_POST["registration_authenticate"]),
                            "authentication_key" => $_POST["registration_authentication_key"],
                            "authentication_email" => array(
                                "subject" => $_POST["registration_authentication_email_subject"],
                                "body" => $_POST["registration_authentication_email_body"]
                            ),
                            "confirm_users" => isset($_POST["registration_confirm"])
                        ),
                        "account_email" => array(
                            "subject" => $_POST["account_email_subject"],
                            "body" => $_POST["account_email_body"]
                        ),
                        "password_reset" => array(
                            "enabled" => isset($_POST["password_reset_enabled"]),
                            "password_reset_key" => $_POST["password_reset_key"],
                            "password_reset_email" => array(
                                "subject" => $_POST["password_reset_email_subject"],
                                "body" => $_POST["password_reset_email_body"]
                            ),
                            "confirm_password_reset" => isset($_POST["password_reset_confirm"])
                        ),
                        "can_change_username" => isset($_POST["can_change_username"]),
                        "can_change_email" => isset($_POST["can_change_email"]),
                        "can_change_password" => isset($_POST["can_change_password"])
                    )
                ));
            }

            $this->_Extorio()->messageSuccess("Settings updated");
        }

        $this->config = $this->_Extorio()->getConfig();
    }
}