<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Page;
use Core\Classes\Utilities\Users;

/**
 * User logout page
 *
 * Class UserLogout
 */
class UserLogout extends \Core\Classes\Commons\Controller {
    public function _onDefault() {
        Users::logout();
        //redirect to the default page
        $p = Page::findOne(Query::n()->where(array("default"=>true)),1);
        $r = "";
        if($p) {
            $r = $p->address;
        } else {
            $r = "/";
        }
        $this->_Extorio()->messageSuccess("Logout successful");
        header("Location: ".$r);
        exit;
    }
}