<?php
namespace Core\Components\Controllers;
use Core\Classes\Models\Page;
use Core\Classes\Utilities\Users;

class ExtorioAdminLogout extends \Core\Classes\Commons\Controller {
    public function _onBegin() {
        Users::logout();
        $this->_Extorio()->messageInfo("Logout successful");
        $p = Page::findByAddress("/extorio-admin/login/");
        if($p) {
            $p->redirect();
        }
    }
}