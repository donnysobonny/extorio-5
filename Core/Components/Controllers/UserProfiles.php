<?php
namespace Core\Components\Controllers;
use Core\Classes\Enums\InternalEvents;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Models\User;

/**
 * 
 *
 * Class UserProfiles
 */
class UserProfiles extends \Core\Classes\Commons\Controller {

    public $user;

    public function _onDefault($id = false) {
        if(!$id) {
            $this->_redirectToDefaultPage();
        }
        $this->user = User::findById($id,INF);
        if(!$this->user) {
            $this->_redirectTo404PageNotFoundPage();
        }
        $this->_Extorio()->setTargetBreadCrumbs(array(
            BreadCrumb::n(true,"Users", "/users/"),
            BreadCrumb::n(true,$this->user->shortname)
        ));
        $results = $this->_Extorio()->getEvent(InternalEvents::_user_profiles_controller)->run(array($this->user));
        foreach($results as $result) {
            echo $result;
        }
    }
}