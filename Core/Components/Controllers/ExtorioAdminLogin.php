<?php
namespace Core\Components\Controllers;
use Core\Classes\Models\Page;
use Core\Classes\Utilities\Users;

class ExtorioAdminLogin extends \Core\Classes\Commons\Controller {
    public function _onBegin() {
        if(isset($_POST["login_submit"])) {
            $error = false;
            try{
                $keeploggedin = isset($_POST["keeploggedin"]);
                if($keeploggedin) {
                    Users::login($_POST["username"],$_POST["password"],7,true,30);
                } else {
                    Users::login($_POST["username"],$_POST["password"],7,false);
                }
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_Extorio()->messageError($error);
            } else {
                $this->_Extorio()->messageSuccess("Login successful");
                if(isset($_GET["r"])) {
                    header("Location: ".urldecode($_GET["r"]));
                    exit;
                } else {
                    $page = Page::findByAddress("/extorio-admin/");
                    if($page) {
                        $page->redirect();
                    }
                }
            }
        }
    }

    public function _onDefault() {

    }

    public function _onEnd() {

    }

    protected function onStart() {

    }
}