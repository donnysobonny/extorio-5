<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\Extorio;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Language;
use Core\Classes\Utilities\Arrays;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Users;

/**
 * Manage the application settings
 *
 * Class AdminApplicationSettings
 */
class AdminApplicationSettings extends \Core\Classes\Commons\Controller {
    public $config = array();
    /**
     * @var Language[]
     */
    public $languages = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_application_settings"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
    }

    public function _onDefault() {
        $this->languages = Language::findAll(Query::n()->order("name"));

        if(isset($_POST["settings_updated"])) {

            $enabled_languages = Arrays::smartCast($_POST["enabled_languages"]);
            foreach($this->languages as $lang) {
                if($lang->enabled && !in_array($lang->id,$enabled_languages)) {
                    $lang->enabled = false;
                    $lang->pushThis();
                } elseif(!$lang->enabled && in_array($lang->id,$enabled_languages)) {
                    $lang->enabled = true;
                    $lang->pushThis();
                }
            }
            $this->languages = Language::findAll(Query::n()->order("name"));

            $core = Extorio::get()->getExtension("Core");
            //merge settings
            $core->_mergeConfigOverride(array(
                "application" => array(
                    "name" => $_POST["application_name"],
                    "description" => $_POST["application_description"],
                    "company" => $_POST["application_company"],
                    "address" => $_POST["application_address"],
                    "seo_visible" => isset($_POST["application_seo"]),
                    "maintenance_mode" => isset($_POST["application_maintenance"]),
                    "favicon" => $_POST["application_favicon"],
                ),
                "localization" => array(
                    "default_language" => intval($_POST["default_language"]),
                    "auto_translate" => isset($_POST["auto_translate"]),
                    "microsoft_credentials" => array(
                        "client_id" => $_POST["microsoft_creds_client_id"],
                        "client_secret" => $_POST["microsoft_creds_client_secret"]
                    )
                )
            ));

            //create the robots
            \Core\Classes\Utilities\Extorio::createRobotsTxt();

            Extorio::get()->messageSuccess("Settings updated successfully");
        }

        $this->config = Extorio::get()->getConfig();
    }
}