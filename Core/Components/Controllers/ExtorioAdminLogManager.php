<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Users;

class ExtorioAdminLogManager extends \Core\Classes\Commons\Controller {
    /**
     * @var File[]
     */
    public $files = array();

    public $fileContent = "";

    public $canView;
    public $canDelete;

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_log_manager"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }

        $this->canView = Users::userHasPrivilege($this->_getLoggedInUserId(),"logs_view","Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(),"logs_delete","Core");

        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Log manager","/extorio-admin/log-manager")
            )
        );

        $this->files = File::getFilesWithinDirectory("Logs",true);
    }

    public function _onDefault() {

    }

    public function _onEnd() {

    }

    protected function onStart() {

    }

    public function view($file=false) {
        if(!$this->canView) {
            $this->_messageWarning("You are not able to view log files");
            $this->_redirectToDefault();
        }

        if($file) {
            $file = urldecode($file);
            foreach($this->files as $poss) {
                if($file == $poss->baseName()) {
                    if($poss->size() > 0) {
                        $this->fileContent = $poss->read();
                    }
                }
            }
        }
    }

    public function clear() {
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete log files");
            $this->_redirectToDefault();
        }

        foreach($this->files as $file) {
            $file->delete();
        }

        $this->_Extorio()->messageInfo("All log files successfully cleared");
        $this->_redirectToDefault();
    }

    public function delete($file=false) {
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete log files");
            $this->_redirectToDefault();
        }

        if($file) {
            $file = urldecode($file);
            foreach($this->files as $poss) {
                if($file == $poss->baseName()) {
                    $poss->delete();

                    $this->_Extorio()->messageInfo($file." successfully deleted");
                    $this->_redirectToDefault();
                }
            }
        }
    }
}