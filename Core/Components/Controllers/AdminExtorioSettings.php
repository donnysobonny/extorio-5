<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\Extorio;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Users;

/**
 * Manage the settings of extorio
 *
 * Class AdminExtorioSettings
 */
class AdminExtorioSettings extends \Core\Classes\Commons\Controller {
    public $config = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_extorio_settings"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
    }

    public function _onDefault() {
        if(isset($_POST["settings_updated"])) {
            $core = Extorio::get()->getExtension("Core");
            //merge settings
            $core->_mergeConfigOverride(array(
                "tasks" => array(
                    "max_running_tasks" => intval($_POST["task_max"]),
                    "clean_up_tasks_after_days" => intval($_POST["task_days"])
                ),
                "models" => array(
                    "max_modcache_entries_per_instance" => intval($_POST["models_cache"]),
                    "webhooks" => array(
                        "autofire" => isset($_POST["models_webhook_autofire"]),
                        "default_verification_key" => $_POST["models_default_verification_key"],
                        "retry_delay" => intval($_POST["models_webhook_retry_delay"]),
                        "max_retries" => intval($_POST["models_webhook_max_retries"])
                    )
                ),
                "themes" => array(
                    "smart_compile" => isset($_POST["themes_smart_compile"]),
                    "smart_minify" => isset($_POST["themes_smart_minify"])
                )
            ));

            //create the robots
            \Core\Classes\Utilities\Extorio::createRobotsTxt();

            Extorio::get()->messageSuccess("Settings updated successfully");
        }

        $this->config = Extorio::get()->getConfig();
    }
}