<?php
namespace Core\Components\Controllers;
use Core\Classes\Enums\InternalEvents;

/**
 * A page for users to log in
 *
 * Class UserLogin
 */
class UserLogin extends \Core\Classes\Commons\Controller {

    public function _onDefault() {
        $this->_Extorio()->getEvent(InternalEvents::_user_login_controller)->run();
    }
}