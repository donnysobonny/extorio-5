<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\LayoutSystem;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Layout;

class ExtorioLayouts extends \Core\Classes\Commons\Controller {
    public function _onBegin() {
        //disable auto render
        $this->_setDispatchSetting_AutoRender(false);
    }

    public function _onDefault($li = false,$action="view") {
        if ($li) {
            //try and find the layout
            $l = Layout::findOne(
                Query::n()
                    ->where(array(
                        '$or' => array(
                            "name" => $li,
                            "id" => intval($li)
                        )
                    ))
            );
            if($l) {
                $editMode = false;
                if($action == "edit") {
                    $editMode = true;
                }
                echo LayoutSystem::display(LayoutSystem::constructFromArray($l->layout),"","",$editMode);
            }
        }
    }
}