<?php
namespace Core\Components\Controllers;
use Core\Classes\Enums\InternalEvents;

/**
 * User registration page
 *
 * Class UserRegister
 */
class UserRegister extends \Core\Classes\Commons\Controller {
    public function _onDefault() {
        $this->_Extorio()->getEvent(InternalEvents::_user_register_controller)->run();
    }
}