<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\Page;
use Core\Classes\Models\UserPrivilege;
use Core\Classes\Models\UserPrivilegeCategory;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

/**
 * Manage user privileges
 *
 * Class AdminUserPrivileges
 */
class AdminUserPrivileges extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModify;
    public $canDelete;
    /**
     * @var UserPrivilege[]
     */
    public $privileges = array();
    /**
     * @var SimpleFiltering
     */
    public $f;
    /**
     * @var UserPrivilege
     */
    public $privilege;
    /**
     * @var UserPrivilegeCategory[]
     */
    public $categories = array();
    /**
     * @var Extension[]
     */
    public $extensions = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_user_privileges"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_privileges_create","Core");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_privileges_modify","Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_privileges_delete","Core");
    }

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(array(
            BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
            BreadCrumb::n(true,"User Privileges","/extorio-admin/user-privileges/"),
            BreadCrumb::n(false,"Create new privilege","/extorio-admin/user-privileges/edit","plus",!$this->canCreate)
        ));

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit", array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $cats = UserPrivilegeCategory::findAll(Query::n()->order("name"));
        $filters = array(
            "any" => "Any category"
        );
        foreach($cats as $cat) {
            $filters[$cat->id] = $cat->name;
        }
        $this->f->addFilter("category", $filters);
        $this->f->addFilter("1",null);
        $this->f->addFilter("2",null);
        $this->f->setSearchTypes(array(
            "name" => "Name",
            "extension" => "Extension"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getFilter("category"))) {
            if($this->f->getFilter("category") != "any") {
                $where[] = array("category.id" => intval($this->f->getFilter("category")));
            }
        }
        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[] = array("name" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $queryCount = clone($query);

        $query->order(array(
            "category.name" => "asc",
            "name" => "asc"
        ));
        $query->limit($this->f->getLimit());
        $query->skip($this->f->getLimit() * $this->f->getPage());

        $this->privileges = UserPrivilege::findAll($query,2);
        $this->f->setCount(UserPrivilege::findCount($queryCount));
    }

    public function edit($id = false) {
        if($id) {
            if(!$this->canModify) {
                $this->_messageWarning("You are not able to modify privileges");
                $this->_redirectToDefault();
            }
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create privileges");
                $this->_redirectToDefault();
            }
        }

        $this->privilege = UserPrivilege::findById($id,1);
        $this->categories = UserPrivilegeCategory::findAll(Query::n()->order("name"));
        $this->extensions = $this->_Extorio()->getExtensions();

        if(isset($_POST["submit"]) || isset($_POST["submit_exit"])) {
            $p = UserPrivilege::findById($id,1);
            if(!$p) {
                $p = UserPrivilege::n();
            }
            $p->name = $_POST["name"];
            $p->description = $_POST["description"];
            $p->category = intval($_POST["category"]);
            $p->extensionName = $_POST["extension"]?:$p->extensionName;

            $error = false;
            try{
                $p->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                $this->_messageError($error);
            } else {
                $this->_messageSuccess("User Privilege Saved");
                if(isset($_POST["submit_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit",array($p->id));
                }
            }
        }

        if($this->privilege) {
            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"User Privileges","/extorio-admin/user-privileges/"),
                BreadCrumb::n(true,$this->privilege->name)
            ));
        } else {
            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"User Privileges","/extorio-admin/user-privileges/"),
                BreadCrumb::n(true,"Create new privilege","/extorio-admin/user-privileges/edit","plus")
            ));
        }
    }

    public function delete($id = false) {
        if(!$id) {
            $this->_redirectToDefault();
        }
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete privileges");
            $this->_redirectToDefault();
        }

        $error = false;
        try {
            $p = UserPrivilege::findById($id);
            if($p) {
                $p->deleteThis();
            }
        } catch(\Exception $ex) {
            $error = $ex->getMessage();
        }

        if($error) {
            $this->_messageError($error);
        } else {
            $this->_messageSuccess("User Privilege Deleted");
        }

        $this->_redirectToDefault();
    }
}