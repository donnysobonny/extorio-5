<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\Extorio;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Users;

class ExtorioAdminSettings extends \Core\Classes\Commons\Controller {

    public $config = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_settings"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
    }

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Settings","/extorio-admin/settings"),
            )
        );

        if(isset($_POST["settings_updated"])) {
            $core = Extorio::get()->getExtension("Core");
            //merge settings
            $core->_mergeConfigOverride(array(
                "extorio" => array(
                    "core_data" => array(
                        "modify_apis" => isset($_POST["extorio_core_data_apis"]),
                        "modify_blocks" => isset($_POST["extorio_core_data_blocks"]),
                        "modify_block_processors" => isset($_POST["extorio_core_data_block_processors"]),
                        "modify_enums" => isset($_POST["extorio_core_data_enums"]),
                        "modify_layouts" => isset($_POST["extorio_core_data_layouts"]),
                        "modify_models" => isset($_POST["extorio_core_data_models"]),
                        "modify_webhooks" => isset($_POST["extorio_core_data_webhooks"]),
                        "modify_pages" => isset($_POST["extorio_core_data_pages"]),
                        "modify_tasks" => isset($_POST["extorio_core_data_tasks"]),
                        "modify_templates" => isset($_POST["extorio_core_data_templates"]),
                        "modify_themes" => isset($_POST["extorio_core_data_themes"]),
                        "modify_email_templates" => isset($_POST["extorio_core_data_email_templates"])
                    ),
                    "menu_access" => array(
                        "extorio_menu" => intval($_POST["extorio_menu_access_extorio"]),
                        "user_menu" => intval($_POST["extorio_menu_access_users"]),
                        "content_menu" => intval($_POST["extorio_menu_access_content"]),
                        "data_menu" => intval($_POST["extorio_menu_access_data"]),
                        "developer_menu" => intval($_POST["extorio_menu_access_developer"]),
                        "utilities_menu" => intval($_POST["extorio_menu_access_utilities"])
                    ),
                    "authentication" => array(
                        "session_type" => $_POST["extorio_authentication_session_type"],
                        "jwt" => array(
                            "key" => $_POST["extorio_authentication_jwt_key"],
                            "alg" => $_POST["extorio_authentication_jwt_alg"]
                        )
                    )
                ),
                "application" => array(
                    "name" => $_POST["application_name"],
                    "description" => $_POST["application_description"],
                    "company" => $_POST["application_company"],
                    "address" => $_POST["application_address"],
                    "seo_visible" => isset($_POST["application_seo"]),
                    "maintenance_mode" => isset($_POST["application_maintenance"]),
                    "favicon" => $_POST["application_favicon"],
                ),
                "tasks" => array(
                    "max_running_tasks" => intval($_POST["task_max"]),
                    "clean_up_tasks_after_days" => intval($_POST["task_days"])
                ),
                "models" => array(
                    "max_modcache_entries_per_instance" => intval($_POST["models_cache"]),
                    "webhooks" => array(
                        "autofire" => isset($_POST["models_webhook_autofire"]),
                        "default_verification_key" => $_POST["models_default_verification_key"],
                        "retry_delay" => intval($_POST["models_webhook_retry_delay"]),
                        "max_retries" => intval($_POST["models_webhook_max_retries"])
                    )
                )
            ));

            //create the robots
            \Core\Classes\Utilities\Extorio::createRobotsTxt();

            Extorio::get()->messageSuccess("Settings updated successfully");
        }

        $this->config = Extorio::get()->getConfig();
    }

    public function _onEnd() {

    }
}