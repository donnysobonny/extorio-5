<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\Extension;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\BlockProcessor;
use Core\Classes\Models\BlockProcessorCategory;
use Core\Classes\Models\UserGroup;
use Core\Classes\Models\UserRole;
use Core\Classes\Utilities\Arrays;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

class ExtorioAdminBlockProcessors extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModify;
    public $canDelete;
    /**
     * @var BlockProcessor[]
     */
    public $processors = array();
    /**
     * @var SimpleFiltering
     */
    public $f;
    /**
     * @var BlockProcessor
     */
    public $processor = false;
    /**
     * @var UserGroup[]
     */
    public $groups = array();
    /**
     * @var Extension[]
     */
    public $extensions = array();
    /**
     * @var BlockProcessorCategory[]
     */
    public $categories = array();

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_block_processors"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(),"block_processors_create","Core");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(),"block_processors_modify","Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(),"block_processors_delete","Core");
    }

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Block Processors","/extorio-admin/block-processors"),
                BreadCrumb::n(false,"Create new block processor","/extorio-admin/block-processors/edit","plus",!$this->canCreate)
            )
        );

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit",array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $cats = BlockProcessorCategory::findAll(Query::n()->order("name"));
        $filters = array(
            "" => "Any category"
        );
        foreach($cats as $cat) {
            $filters[$cat->id] = $cat->name;
        }
        $this->f->addFilter("category",$filters);
        $this->f->addEmpty();
        $this->f->addEmpty();
        $this->f->setSearchTypes(array(
            "name" => "Name",
            "extension" => "Extension"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getFilter("category"))) {
            $where[] = array("category.id" => intval($this->f->getFilter("category")));
        }
        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[] = array("name" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $this->f->setCount(BlockProcessor::findCount($query));

        $query->order("name");
        $query->limit($this->f->getLimit());
        $query->skip($this->f->getLimit() * $this->f->getPage());

        $this->processors = BlockProcessor::findAll($query,2);
    }

    public function edit($processorId=false) {
        if($processorId) {
            if(!$this->canModify) {
                $this->_messageWarning("You are not able to modify block processors");
                $this->_redirectToDefault();
            }
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create block processors");
                $this->_redirectToDefault();
            }
        }

        if(isset($_POST["processor_updated"]) || isset($_POST["processor_updated_exit"])) {
            $p = BlockProcessor::findById($processorId);
            if(!$p) {
                $p = BlockProcessor::n();
            }
            $p->name = $_POST["name"];
            $p->description = $_POST["description"];
            $p->inlineEnabled = isset($_POST["inline"]);
            $p->allInline = isset($_POST["all_inline"]);
            $p->category = intval($_POST["category"]);
            if(isset($_POST["groups"])) {
                $p->inlineGroups = Arrays::smartCast($_POST["groups"]);
            } else {
                $p->inlineGroups = array();
            }
            $p->extensionName = $_POST["extension"]?:$p->extensionName;

            $error = false;
            try {
                $p->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_Extorio()->messageError($error);
            } else {
                $this->_Extorio()->messageSuccess("Block processor successfully saved");
                if(isset($_POST["processor_updated_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit",array($p->id));
                }

            }
        }

        $this->processor = BlockProcessor::findById($processorId,1);
        $this->groups = UserGroup::findAll(Query::n()->order(array("name"=>"asc")));
        $this->categories = BlockProcessorCategory::findAll(Query::n()->order(array("name"=>"asc")));

        if($this->processor) {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Block Processors","/extorio-admin/block-processors"),
                    BreadCrumb::n(true,$this->processor->label)
                )
            );
        } else {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Block Processors","/extorio-admin/block-processors"),
                    BreadCrumb::n(true,"Create new block processor","","plus")
                )
            );
        }
        $this->extensions = $this->_Extorio()->getExtensions();
    }

    public function delete($processorId=false) {
        if(!$processorId) {
            $this->_redirectToDefault();
        }
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete block processors");
            $this->_redirectToDefault();
        }

        $bp = BlockProcessor::findById($processorId);
        if($bp) {
            $bp->deleteThis();
        }

        $this->_Extorio()->messageSuccess("Block processor successfully deleted");
        $this->_redirectToDefault();
    }
}