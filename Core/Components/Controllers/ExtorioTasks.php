<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\Task;
use Core\Classes\Exceptions\General_Exception;
use Core\Classes\Helpers\Query;use Core\Classes\Utilities\Components;use Core\Classes\Utilities\Tasks;

class ExtorioTasks extends \Core\Classes\Commons\Controller {
    /**
     * @var Task
     */
    private $taskComponent;
    
    public function _onBegin() {
        ignore_user_abort(true);
        set_time_limit(0);
        //disable auto render
        $this->_setDispatchSetting_AutoRender(false);
        $this->_Extorio()->bindActionToEvent("on_exception",array($this,"on_exception"),1,true);
    }

    /**
     * @param \Exception $ex
     */
    final public function on_exception($ex) {
        $this->_Extorio()->logCustom("task_error.log",$ex->__toString());
        if($this->taskComponent) {
            $this->taskComponent->_failTask($ex->getMessage());
        } else {
            ?>
<h1>Internal error</h1>
<p>
    An internal error was encountered before the task could be run. If this message persists, please contact the website administrator with the following information:
</p>
<p>
    <pre><?=$ex->__toString()?></pre>
</p>
            <?php
        }
    }
    
    public function _onDefault($tid=false) {
        $this->taskComponent = false;
        //from this point on, catch any Exceptions
        if(!$tid) {
            throw new General_Exception("Trying to start a task without any way to identify the task you are trying to access");
        }

        //try and find the task
        $task = \Core\Classes\Models\Task::findOne(
            Query::n()
                ->where(array(
                    "label" => $tid
                )),2
        );
        if(!$task) {
            throw new General_Exception($tid." does not lead to an identifiable task");
        }

        //make sure we can read the task
        $loggedInUser = $this->_getLoggedInUser();
        if(!Tasks::canUserReadTask($loggedInUser ? $loggedInUser->id : 0,$task->id)) {
            throw new General_Exception("Access denied");
        }

        $extension = $this->_Extorio()->getExtension($task->extensionName);
        if(!$extension) {
            throw new General_Exception("The task you are trying to access requires an Extension named ".$task->extensionName." which is either disabled or not installed");
        }

        //find the task module
        $this->taskComponent = $extension->_constructComponent($task->namespace);
        if(!$this->taskComponent) {
            throw new General_Exception("Unable to locate the task module!");
        }

        //we have the task module, run it in extorio
        $action = "onDefault";
        $params = array();
        $args = func_get_args();
        if(count($args) > 1) {
            $action = $args[1];
        }
        for($i = 2; $i < count($args); $i++) {
            $params[] = $args[$i];
        }

        $isHidden = false;
        if(isset($_GET["task_is_hidden"])) {
            if($_GET["task_is_hidden"] == "true") {
                $isHidden = true;
            }
        }

        $this->_Extorio()->runTask_Now($this->taskComponent,$isHidden,$action,$params);
    }
}