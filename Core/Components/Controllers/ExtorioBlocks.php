<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Block;
use Core\Classes\Utilities\Blocks;

class ExtorioBlocks extends \Core\Classes\Commons\Controller {
    public function _onBegin() {
        //disable auto render
        $this->_setDispatchSetting_AutoRender(false);
    }

    public function _onDefault($bi=false,$action="view") {
        if($bi) {
            //try and find the instance
            $block = Block::findOne(
                Query::n()
                    ->where(array(
                        '$or' => array(
                            "name" => $bi,
                            "id" => intval($bi)
                        )
                    )),2
            );
            if($block) {
                //we are viewing the view/edit action
                switch($action) {
                    case "view" :
                        //make sure we can read
                        if(Blocks::canUserReadBlock($this->_getLoggedInUser()->id,$block->id)) {
                            //find the block processor module
                            $extension = $this->_Extorio()->getExtension($block->processor->extensionName);
                            if($extension) {
                                $bp = $extension->_constructComponent($block->processor->namespace);
                                if($bp) {
                                    $content = $this->_Extorio()->fetchBlockProcessorViewContent($bp,$block->config,$block->id,false);
                                    $content = $this->_Extorio()->parseInlineBlocks($content);
                                    $content = $this->_Extorio()->parseInlineTranslations($content);
                                    echo $content;
                                }
                            }
                        }
                        break;
                    case "edit" :
                        //make sure we can modify
                        if(Blocks::canUserModifyBlock($this->_getLoggedInUser()->id,$block->id)) {
                            //find the block processor module
                            $extension = $this->_Extorio()->getExtension($block->processor->extensionName);
                            if($extension) {
                                $bp = $extension->_constructComponent($block->processor->namespace);
                                if($bp) {
                                    echo $this->_Extorio()->fetchBlockProcessorEditContent($bp,$block->config,$block->id,false);
                                }
                            }
                        }
                        break;
                    case "save" :
                        if(Blocks::canUserModifyBlock($this->_getLoggedInUser()->id,$block->id)) {
                            $config = $_POST["config"];
                            $currentConfig = array();
                            parse_str($block->config,$currentConfig);
                            foreach($config as $k => $v) {
                                $currentConfig[$k] = $config[$k];
                            }
                            $block->config = http_build_query($currentConfig);
                            $block->pushThis();
                        }
                        break;
                    case "label" :
                        //make sure we can read
                        if(Blocks::canUserReadBlock($this->_getLoggedInUser()->id,$block->id)) {
                            echo $block->name." (".$block->processor->label.")";
                        }
                        break;
                }
            }
        }
    }
}