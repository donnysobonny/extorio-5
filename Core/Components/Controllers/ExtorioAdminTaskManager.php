<?php
namespace Core\Components\Controllers;
use Core\Classes\Enums\TaskStatus;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Task;
use Core\Classes\Utilities\Components;
use Core\Classes\Utilities\Namespaces;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Tasks;
use Core\Classes\Utilities\Users;

class ExtorioAdminTaskManager extends \Core\Classes\Commons\Controller {

    public $liveTasks = array();

    public $callableActions = array();

    public $showHiddenTasks = false;

    public $canRun;
    public $canDelete;
    public $canKill;

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_task_manager"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }

        $this->canRun = Users::userHasPrivilege($this->_getLoggedInUserId(),"tasks_run","Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(),"tasks_delete","Core");
        $this->canKill = Users::userHasPrivilege($this->_getLoggedInUserId(),"tasks_kill","Core");

        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Task manager","/extorio-admin/task-manager"),
            )
        );
    }

    public function _onDefault() {
        if(isset($_GET["show_hidden_tasks"])) {
            if($_GET["show_hidden_tasks"] == "true") {
                $_SESSION["show_hidden_tasks"] = true;
            } elseif($_GET["show_hidden_tasks"] == "false") {
                $_SESSION["show_hidden_tasks"] = false;
            }
        }

        if(isset($_SESSION["show_hidden_tasks"])) {
            $this->showHiddenTasks = $_SESSION["show_hidden_tasks"];
        } else {
            $this->showHiddenTasks = false;
        }

        //get all installed Tasks
        $tasks = Task::findAll(
            Query::n()->order(array("label"))
        );

        foreach($tasks as $task) {
            //try and have the Extension find and load the class file for this task
            $extension = $this->_Extorio()->getExtension($task->extensionName);
            if($extension) {
                $reflector = new \ReflectionClass($task->namespace);
                //get the public methods
                foreach($reflector->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
                    if(Namespaces::getNamespace($method->class) == $task->namespace) {
                        $this->callableActions[$task->label][] = $method->name;
                    }
                }
            }
        }

        if(isset($_POST["run_task"])) {
            if(!$this->canRun) {
                $this->_messageWarning("You are not able to run tasks");
                $this->_redirectToDefault();
            }

            $label = $_POST["task"];
            $action = $_POST["action"];
            if(!strlen($action)) $action = "onDefault";
            $params = $_POST["params"];
            $loggedInUser = $this->_getLoggedInUser();
            foreach($tasks as $task) {
                if($task->label == $label) {
                    if(Tasks::canUserReadTask($loggedInUser ? $loggedInUser->id : 0,$task->id)) {
                        //fix the params
                        $params = str_replace(", ",",",$params);
                        $params = explode(",",$params);
                        //find the task module
                        $extension = $this->_Extorio()->getExtension($task->extensionName);
                        if($extension) {
                            $this->_Extorio()->runTask_Background($extension->_constructComponent($task->namespace),false,$action,$params);
                        }

                        $this->_Extorio()->messageInfo("Task started successfully. You may need to refresh the task manager to see your Tasks status");
                        $this->_redirectToDefault();
                    }
                    break;
                }
            }
        }

        $this->liveTasks = Tasks::getLiveTasks($this->showHiddenTasks);
    }

    public function clear() {
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete tasks");
            $this->_redirectToDefault();
        }

        $db = $this->_Extorio()->getDbInstanceDefault();
        $db->query('DELETE FROM tasks_livetask WHERE status != $1 OR status != $2',array(TaskStatus::_running,TaskStatus::_queued));

        $this->_Extorio()->messageInfo("Non-running Tasks cleared");
        $this->_redirectToDefault();
    }

    public function kill($pid=false) {
        if(!$this->canKill) {
            $this->_messageWarning("You are not able to kill tasks");
            $this->_redirectToDefault();
        }

        Tasks::killLiveTask(intval($pid));

        $this->_Extorio()->messageInfo("Task killed successfully");
        $this->_redirectToDefault();
    }
}