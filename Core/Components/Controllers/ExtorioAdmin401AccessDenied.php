<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;

class ExtorioAdmin401AccessDenied extends \Core\Classes\Commons\Controller {
    public function _onBegin() {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Access Denied","/extorio-admin/401+access+denied"),
            )
        );
    }
}