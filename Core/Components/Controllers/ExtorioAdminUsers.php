<?php
namespace Core\Components\Controllers;
use Core\Classes\Enums\InternalEvents;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\User;
use Core\Classes\Models\UserAction;
use Core\Classes\Models\UserActionType;
use Core\Classes\Models\UserGroup;
use Core\Classes\Models\UserRole;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

class ExtorioAdminUsers extends \Core\Classes\Commons\Controller {

    /**
     * @var User[]
     */
    public $users = array();
    /**
     * @var User
     */
    public $loggedInUser;

    public $canCreate = false;

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_users"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->loggedInUser = $this->_Extorio()->getLoggedInUser();
        $this->canCreate = Users::userHasPrivilege($this->loggedInUser->id,"users_create","Core");
    }

    /**
     * @var SimpleFiltering
     */
    public $f;

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Users","/extorio-admin/users"),
                BreadCrumb::n(false,"Create new user","/extorio-admin/users/create","plus",!$this->canCreate)
            )
        );

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit",array(
            10 => 10,
            25 => 25,
            100 => 100,
            250 => 250
        ));
        $this->f->addFilter("state",array(
            "any" => "Any state",
            "login" => "Can login",
            "nologin" => "Cannot login"
        ));
        $filters = array();
        $loggedInUser = $this->_getLoggedInUser();
        if($loggedInUser->userGroup->manageAll) {
            $filters[""] = "Any user group";
        }
        $ugs = $this->_getLoggedInUserManageGroups();
        foreach($ugs as $ug) {
            $filters[$ug->id] = $ug->name;
        }
        $this->f->addFilter("user-group",$filters);
        $this->f->addFilter("order", array(
            "username" => "Username",
            "email" => "Email",
            "firstname" => "First name"
        ));
        $this->f->setSearchTypes(array(
            "username" => "Username",
            "email" => "Email",
            "firstname" => "First name",
            "lastname" => "Last name"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();
        $where[] = array(
            "id" => array(
                Query::_ne => $this->loggedInUser->id
            )
        );
        if(!$this->loggedInUser->userGroup->manageAll) {
            $where[] = array(
                "userGroup.id" => array(
                    Query::_in => $this->loggedInUser->userGroup->manageGroups
                )
            );
        }

        if(strlen($this->f->getFilter("state"))) {
            switch($this->f->getFilter("state")) {
                case "login" :
                    $where[]  = array("canLogin" => true);
                    break;
                case "nologin" :
                    $where[]  = array("canLogin" => false);
                    break;
            }
        }
        if(strlen($this->f->getFilter("user-group"))) {
            if($this->f->getFilter("user-group") != "any") {
                $where[] = array("userGroup.id" => intval($this->f->getFilter("user-group")));
            }
        }
        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "username" :
                    $where[] = array("username" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "email" :
                    $where[] = array("email" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "firstname" :
                    $where[] = array("firstname" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "lastname" :
                    $where[] = array("lastname" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $queryCount = clone($query);

        switch($this->f->getFilter("order")) {
            case "username" :
                $query->order("username");
                break;
            case "email" :
                $query->order("email");
                break;
            case "firstname" :
                $query->order("firstname");
                break;
        }

        $query->limit($this->f->getLimit());
        $query->skip($this->f->getLimit() * $this->f->getPage());

        $this->users = User::findAll($query,2);
        $this->f->setCount(User::findCount($queryCount));
    }

    /**
     * @var UserGroup[]
     */
    public $allGroups = array();
    /**
     * @var UserGroup[]
     */
    public $manageGroups = array();
    public $manageGroupIds = array();

    public function create() {
        if(!$this->canCreate) {
            $this->_messageWarning("You are not able to create users");
            $this->_redirectToDefault();
        }

        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"Users","/extorio-admin/users"),
                BreadCrumb::n(true,"Create new user","/extorio-admin/users/create","plus")
            )
        );

        $this->_Extorio()->getEvent(InternalEvents::_user_create_controller)->run();
    }

    /**
     * @var User
     */
    public $user;
    public $canChangeUsername = false;
    public $canChangeEmail = false;
    public $canChangePassword = false;

    public function edit($userId=false) {
        if(!$userId) {
            $this->_redirectToDefault();
        }
        if(!Users::userManagesUser($this->loggedInUser->id,$userId) || !Users::userHasPrivilege($this->loggedInUser->id,"users_modify","Core")) {
            $this->_messageWarning("You are not able to modify this user");
            $this->_redirectToDefault();
        }
        $this->user = User::findById($userId,3);
        if(!$this->user) {
            $this->_redirectToDefault();
        }

        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"Users","/extorio-admin/users"),
                BreadCrumb::n(true,$this->user->longname)
            )
        );

        $this->_Extorio()->getEvent(InternalEvents::_user_edit_controller)->run(array($this->user));
    }

    public function delete($userId=false) {
        if(!$userId) {
            $this->_redirectToDefault();
        }
        if(!Users::userManagesUser($this->loggedInUser->id,$userId) || !Users::userHasPrivilege($this->loggedInUser->id,"users_delete","Core")) {
            $this->_messageWarning("You are not able to delete this user");
            $this->_redirectToDefault();
        }

        $u = User::findById($userId);
        if($u) {
            $u->deleteThis();

            //user_delete action
            UserAction::qc(
                $this->_getLoggedInUserId(),
                UserActionType::findByName("user_delete"),
                null,
                null,
                $u->id
            );
        }

        $this->_Extorio()->messageSuccess("User successfully deleted");
        $this->_redirectToDefault();
    }
}