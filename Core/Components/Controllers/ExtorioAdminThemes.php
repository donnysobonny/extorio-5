<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\Extension;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\Theme;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

class ExtorioAdminThemes extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModify;
    public $canDelete;
    public $canCompile;
    /**
     *
     * @var Theme[]
     */
    public $themes;
    /**
     * @var Theme
     */
    public $theme;
    /**
     * @var Extension[]
     */
    public $extensions = array();
    /**
     * @var SimpleFiltering
     */
    public $f;

    public function _onBegin() {
        if (!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id, array("extorio_pages_all", "extorio_pages_themes"))) {
            $this->_redirectTo401AccessDeniedPage(array("r" => Server::getRequestURI(), array(), 401));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(), "themes_create", "Core");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(), "themes_modify", "Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(), "themes_delete", "Core");
        $this->canCompile = Users::userHasPrivilege($this->_getLoggedInUserId(), "themes_compile", "Core");
    }

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Themes","/extorio-admin/themes"),
                BreadCrumb::n(false,"Create new theme","/extorio-admin/themes/edit","plus",!$this->canCreate)
            )
        );

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit",array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $this->f->addFilter("state", array(
            "" => "Any state",
            "default" => "Default theme",
            "hidden" => "Hidden themes"
        ));
        $this->f->addEmpty();
        $this->f->addEmpty();
        $this->f->setSearchTypes(array(
            "name" => "Name",
            "extension" => "Extension"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getFilter("state"))) {
            switch($this->f->getFilter("state")) {
                case "default" :
                    $where[] = array("default" => true);
                    $where[] = array("isHidden" => false);
                    break;
                case "hidden" :
                    $where[] = array("isHidden" => true);
                    break;
                default :
                    $where[] = array("isHidden" => false);
                    break;
            }
        } else {
            $where[] = array("isHidden" => false);
        }
        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[] = array("name" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $this->f->setCount(Theme::findCount($query));

        $query->order("name");
        $query->limit($this->f->getLimit());
        $query->skip($this->f->getOffset());

        $this->themes = Theme::findAll($query);
    }

    public function compile($themeId = false) {
        ignore_user_abort(true);
        set_time_limit(0);
        if(!$themeId) {
            $this->_redirectToDefault();
        }
        if(!$this->canCompile) {
            $this->_messageWarning("You are not able to compile themes");
            $this->_redirectToDefault();
        }
        $t = Theme::findById($themeId);
        if($t) {
            //compile the less to css
            $t->compileLessToCss();
            //minify the main css and js file
            $t->minify();
            $this->_Extorio()->messageSuccess("Theme successfully re-compiled");
        }
        $this->_redirectToDefault();
    }

    public function edit($themeId = false) {
        if($themeId) {
            if(!$this->canModify) {
                $this->_messageWarning("You are not able to modify themes");
                $this->_redirectToDefault();
            }
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create themes");
                $this->_redirectToDefault();
            }
        }

        if(isset($_POST["theme_updated"]) || isset($_POST["theme_updated_exit"])) {
            $t = Theme::findById($themeId);
            if(!$t) {
                $t = Theme::n();
            }
            $t->name = $_POST["name"];
            $t->description = $_POST["description"];
            $t->default = isset($_POST["default"]);
            $t->isHidden = isset($_POST["hidden"]);
            $t->prependToHead = $_POST["prependToHead"];
            $t->prependToBody = $_POST["prependToBody"];
            $t->appendToHead = $_POST["appendToHead"];
            $t->appendToBody = $_POST["appendToBody"];
            $t->extensionName = $_POST["extension"]?:$t->extensionName;

            $error = false;
            try{
                $t->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_Extorio()->messageError($error);
            } else {
                $this->_Extorio()->messageSuccess("Theme successfully saved");
                if(isset($_POST["theme_updated_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit",array($t->id));
                }

            }
        }

        $this->theme = Theme::findById($themeId);
        if($this->theme) {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Themes","/extorio-admin/themes"),
                    BreadCrumb::n(true,$this->theme->name)
                )
            );
        } else {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Themes","/extorio-admin/themes"),
                    BreadCrumb::n(true,"Create new theme","","plus")
                )
            );
        }

        $this->extensions = $this->_Extorio()->getExtensions();
    }

    public function delete($themeId = false) {
        if(!$themeId) {
            $this->_redirectToDefault();
        }
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete themes");
            $this->_redirectToDefault();
        }
        $t = Theme::findById($themeId);
        if($t) {
            $t->deleteThis();
        }
        $this->_Extorio()->messageInfo("Theme successfully deleted");
        $this->_redirectToDefault();
    }
}