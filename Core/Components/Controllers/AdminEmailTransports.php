<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\EmailTemplate;
use Core\Classes\Models\EmailTransport;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

/**
 * Manage email transports
 *
 * Class AdminEmailTransports
 */
class AdminEmailTransports extends \Core\Classes\Commons\Controller {

    public $canView;
    public $canResend;
    public $canDelete;

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_email_transports"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canView = Users::userHasPrivilege($this->_getLoggedInUserId(),"email_transports_view","Core");
        $this->canResend = Users::userHasPrivilege($this->_getLoggedInUserId(),"email_transports_resent","Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(),"email_transports_delete","Core");
    }

    /**
     * @var EmailTransport[]
     */
    public $transports = array();
    /**
     * @var EmailTransport
     */
    public $transport;
    /**
     * @var SimpleFiltering
     */
    public $f;

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Email Transports","/extorio-admin/email-transports")
            )
        );

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit", array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $this->f->addFilter("1",null);
        $this->f->addFilter("2",null);
        $this->f->addFilter("order",array(
            "sent" => "Send date",
            "subject" => "Subject"
        ));
        $this->f->setSearchTypes(array(
            "subject" => "Subject",
            "name" => "Recipient name",
            "email" => "Recipient email"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "subject" :
                    $where[] = array("subject" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "name" :
                    $where[] = array("toName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "email" :
                    $where[] = array("toEmail" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $queryCount = clone($query);

        switch($this->f->getFilter("order")) {
            case "sent" :
                $query->order(array("dateSent" => "desc"));
                break;
            case "subject" :
                $query->order("subject");
                break;
        }

        $query->limit($this->f->getLimit());
        $query->skip($this->f->getLimit() * $this->f->getPage());

        $this->transports = EmailTransport::findAll($query,2);
        $this->f->setCount(EmailTransport::findCount($queryCount));
    }

    public function view($id = false) {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"Email Transports","/extorio-admin/email-transports"),
                BreadCrumb::n(true,"View","/extorio-admin/email-transports/view/".$id)
            )
        );

        if(!$id) {
            $this->_redirectToDefault();
        }
        if(!$this->canView) {
            $this->_messageWarning("You are not able to view the contents of email transports");
            $this->_redirectToDefault();
        }

        $this->transport = EmailTransport::findById($id,2);
    }

    public function resend($id = false) {
        if(!$id) {
            $this->_redirectToDefault();
        }
        if(!$this->canResend) {
            $this->_messageWarning("You are not able to resend email transports");
            $this->_redirectToDefault();
        }

        $t = EmailTransport::findById($id,2);
        if($t) {
            $t->send();
        }

        $this->_Extorio()->messageSuccess("Email re-sent");
        $this->_redirectToDefault();
    }

    public function delete($id = false) {
        if(!$id) {
            $this->_redirectToDefault();
        }
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete email transports");
            $this->_redirectToDefault();
        }

        $t = EmailTransport::findById($id,1);
        if($t) {
            $t->deleteThis();
        }

        $this->_Extorio()->messageSuccess("Email deleted");
        $this->_redirectToDefault();
    }
}