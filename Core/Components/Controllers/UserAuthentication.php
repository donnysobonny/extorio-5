<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\User;
use Core\Classes\Utilities\Users;

/**
 * 
 *
 * Class UserAuthentication
 */
class UserAuthentication extends \Core\Classes\Commons\Controller {

    /**
     *
     * @var User
     */
    public $user = false;

    public function _onDefault() {
        $config = $this->_Extorio()->getConfig();

        $t = urldecode($_GET["t"]);
        $e = urldecode($_GET["e"]);
        if(is_null($e) || is_null($t)) {
            return;
        }

        //validate the token
        if($t != md5($e.$config["users"]["registration"]["authentication_key"])) {
            return;
        }

        //find the user
        $this->user = User::findOne(Query::n()->where(array("email"=>$e)),1);

        if($this->user) {
            //update the user, allowing them to log in
            $this->user->canLogin = true;
            $this->user->pushThis();

            //do we confirm the user?
            if($config["users"]["registration"]["confirm_users"]) {
                Users::sendAccountEmail($this->user);
            }
        }
    }
}