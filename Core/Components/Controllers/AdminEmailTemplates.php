<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\Extension;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\EmailTemplate;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

/**
 * Manage the email templates
 *
 * Class AdminEmailTemplates
 */
class AdminEmailTemplates extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModify;
    public $canDelete;
    /**
     *
     * @var EmailTemplate
     */
    public $template;
    /**
     *
     * @var EmailTemplate[]
     */
    public $templates = array();
    /**
     * @var Extension[]
     */
    public $extensions = array();
    /**
     * @var SimpleFiltering
     */
    public $f;

    public function _onBegin() {
        if (!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id, array("extorio_pages_all", "extorio_pages_email_templates"))) {
            $this->_redirectTo401AccessDeniedPage(array("r" => Server::getRequestURI(), array(), 401));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(), "email_templates_create", "Core");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(), "email_templates_modify", "Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(), "email_templates_delete", "Core");
    }

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Email Templates","/extorio-admin/email-templates"),
                BreadCrumb::n(false,"Create new email template","/extorio-admin/email-templates/edit","plus",!$this->canCreate)
            )
        );

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit", array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $this->f->addFilter("1",null);
        $this->f->addFilter("2",null);
        $this->f->addFilter("order",array(
            "name" => "Name",
            "subject" => "Subject"
        ));
        $this->f->setSearchTypes(array(
            "name" => "Name",
            "subject" => "Subject",
            "extension" => "Extension"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[] = array("name" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "subject" :
                    $where[] = array("subject" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extension" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $queryCount = clone($query);

        switch($this->f->getFilter("order")) {
            case "name" :
                $query->order("name");
                break;
            case "subject":
                $query->order("subject");
                break;
        }

        $query->limit($this->f->getLimit());
        $query->skip($this->f->getLimit() * $this->f->getPage());

        $this->templates = EmailTemplate::findAll($query,1);
        $this->f->setCount(EmailTemplate::findCount($queryCount));
    }

    public function edit($id = false) {
        if($id) {
            if(!$this->canModify) {
                $this->_messageWarning("You are not able to modify this template");
                $this->_redirectToDefault();
            }
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create templates");
                $this->_redirectToDefault();
            }
        }

        if(isset($_POST["template_updated"]) || isset($_POST["template_updated_exit"])) {
            $t = EmailTemplate::findById($id);
            if(!$t) {
                $t = EmailTemplate::n();
            }
            $t->name = $_POST["name"];
            $t->subject = $_POST["subject"];
            $t->description = $_POST["description"];
            $t->body = $_POST["body_content"];
            $t->extensionName = $_POST["extension"]?:$t->extensionName;

            $error = false;
            try {
                $t->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                $this->_Extorio()->messageError($error);
            } else {
                $this->_Extorio()->messageSuccess("Email template successfully saved");
                if(isset($_POST["template_updated_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit",array($t->id));
                }
            }
        }

        $this->template = EmailTemplate::findById($id);

        if($this->template) {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Email Templates","/extorio-admin/email-templates"),
                    BreadCrumb::n(true,$this->template->name)
                )
            );
        } else {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Email Templates","/extorio-admin/email-templates"),
                    BreadCrumb::n(true,"Create new email template","","plus")
                )
            );
        }

        $this->extensions = $this->_Extorio()->getExtensions();
    }

    public function delete($id = false) {
        if(!$id) {
            $this->_redirectToDefault();
        }
        if(!$this->canDelete) {
            $this->_messageWarning("You are notable to delete templates");
            $this->_redirectToDefault();
        }

        $t = EmailTemplate::findById($id);
        if($t) {
            $t->deleteThis();
        }

        $this->_Extorio()->messageSuccess("Email template successfully deleted");
        $this->_redirectToDefault();
    }
}