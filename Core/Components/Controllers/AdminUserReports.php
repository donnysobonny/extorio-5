<?php
namespace Core\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\UserActionType;
use Core\Classes\Models\UserReport;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;
use Core\Components\Extension\Extension;

/**
 * Manage user reports
 *
 * Class AdminUserReports
 */
class AdminUserReports extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModify;
    public $canDelete;
    /**
     * @var UserReport[]
     */
    public $reports;
    /**
     * @var SimpleFiltering
     */
    public $f;
    /**
     * @var UserReport
     */
    public $report;
    /**
     * @var Extension
     */
    public $extensions;

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_user_reports"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_reports_create");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_reports_modify");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_reports_delete");
    }

    public function _onDefault() {
        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit",array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $this->f->addEmpty();
        $this->f->addEmpty();
        $this->f->addEmpty();
        $this->f->setSearchTypes(array(
            "name" => "Name",
            "extension" => "Extension"
        ));
        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();
        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[] = array("name" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }
        $query->where($where);
        $clone = clone($query);

        $query->limit($this->f->getLimit())->skip($this->f->getOffset());

        $this->reports = UserReport::findAll($query);
        $this->f->setCount(UserReport::findCount($clone));

        $this->_Extorio()->setTargetBreadCrumbs(array(
            BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
            BreadCrumb::n(true,"Reports","/extorio-admin/user-reports/"),
            BreadCrumb::n(false,"Create new report","/extorio-admin/user-reports/edit/","plus",!$this->canCreate)
        ));
    }

    public function edit($id = false) {
        $this->report = UserReport::findById($id);
        if($this->report) {
            if(!$this->canModify) {
                $this->_messageWarning("You are not able to modify user reports");
                $this->_redirectToDefault();
            }
            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"Reports","/extorio-admin/user-reports/"),
                BreadCrumb::n(true,$this->report->name)
            ));
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create user reports");
                $this->_redirectToDefault();
            }
            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"Reports","/extorio-admin/user-reports/"),
                BreadCrumb::n(true,"Create new report","/extorio-admin/user-reports/edit/","plus",!$this->canCreate)
            ));
        }

        if(isset($_POST["submitted"]) || isset($_POST["submitted_exit"])) {
            $ur = UserReport::findById($id);
            if(!$ur) {
                $ur = UserReport::n();
            }
            $ur->name = $_POST["name"];
            $ur->description = $_POST["description"];
            $ur->query = json_decode($_POST["query"],true);
            $ur->extensionName = $_POST["extension"]?:$ur->extensionName;

            $error = false;
            try {
                $ur->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                $this->_messageError($error);
            } else {
                $this->_messageSuccess("User report saved");
                if(isset($_POST["submitted_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit",array($ur->id));
                }
            }
        }

        $this->extensions = $this->_Extorio()->getExtensions();
    }

    public function delete($id = false) {
        if(!$id) {
            $this->_redirectToDefault();
        }
        $ur = UserReport::findById($id);
        if(!$ur) {
            $this->_redirectToDefault();
        }
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete user reports");
            $this->_redirectToDefault();
        }
        $ur->deleteThis();
        $this->_messageInfo("User report deleted");
        $this->_redirectToDefault();
    }
}