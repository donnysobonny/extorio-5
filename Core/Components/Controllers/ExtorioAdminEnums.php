<?php
namespace Core\Components\Controllers;
use Core\Classes\Commons\Extension;
use Core\Classes\Enums\InternalEvents;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\Enum;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

class ExtorioAdminEnums extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModify;
    public $canDelete;
    /**
     * @var Enum[]
     */
    public $enums = array();
    /**
     * @var Enum
     */
    public $enum;
    /**
     * @var Extension[]
     */
    public $extensions = array();
    /**
     * @var SimpleFiltering
     */
    public $f;

    public function _onBegin() {
        if (!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id, array("extorio_pages_all", "extorio_pages_enums"))) {
            $this->_redirectTo401AccessDeniedPage(array("r" => Server::getRequestURI(), array(), 401));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(), "enums_create", "Core");
        $this->canModify = Users::userHasPrivilege($this->_getLoggedInUserId(), "enums_modify", "Core");
        $this->canDelete = Users::userHasPrivilege($this->_getLoggedInUserId(), "enums_delete", "Core");
    }

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Enums","/extorio-admin/enums"),
                BreadCrumb::n(false,"Create new enum","/extorio-admin/enums/edit","plus",!$this->canCreate)
            )
        );

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit", array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $this->f->addFilter("state",array(
            "any" => "Any state",
            "hidden" => "Hidden enums"
        ));
        $this->f->setSearchTypes(array(
            "name" => "Name",
            "extension" => "Extension"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $query = Query::n();
        $where = array();

        if(strlen($this->f->getFilter("state"))) {
            if($this->f->getFilter("state") != "any") {
                switch($this->f->getFilter("state")) {
                    case "hidden" :
                        $where[] = array("isHidden" => true);
                        break;
                    default :
                        $where[] = array("isHidden" => false);
                        break;
                }
            } else {
                $where[] = array("isHidden" => false);
            }
        } else {
            $where[] = array("isHidden" => false);
        }
        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "name" :
                    $where[] = array("name" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
                case "extension" :
                    $where[] = array("extensionName" => array(
                        Query::_lk => "%".$this->f->getSearchQuery()."%"
                    ));
                    break;
            }
        }

        $query->where($where);
        $queryCount = clone($query);

        $query->order("name");
        $query->limit($this->f->getLimit());
        $query->skip($this->f->getLimit() * $this->f->getPage());

        $this->enums = Enum::findAll($query);
        $this->f->setCount(Enum::findCount($queryCount));
    }
    
    public function edit($enumId = false) {
        if($enumId) {
            if(!$this->canModify) {
                $this->_messageWarning("You are not able to modify enums");
                $this->_redirectToDefault();
            }
        } else {
            if(!$this->canCreate) {
                $this->_messageWarning("You are not able to create enums");
                $this->_redirectToDefault();
            }
        }

        if(isset($_POST["enum_edited"]) || isset($_POST["enum_edited_exit"])) {
            $e = Enum::findById($enumId);
            if(!$e) {
                $e = Enum::n();
            }
            $e->name = $_POST["name"];
            $e->description = $_POST["description"];
            $e->isHidden = isset($_POST["hidden"]);
            $e->extensionName = $_POST["extension"]?:$e->extensionName;

            $error = false;
            try {
                $e->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                $this->_Extorio()->messageError($error);
            } else {
                $this->_Extorio()->messageSuccess("Enum successfully saved");
                if(isset($_POST["enum_edited_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToMethod("edit",array($e->id));
                }
            }
        }

        $this->enum = Enum::findById($enumId);

        if($this->enum) {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Enums","/extorio-admin/enums"),
                    BreadCrumb::n(true,$this->enum->label),
                    BreadCrumb::n(false,"Values","/extorio-admin/enums/values/".$this->enum->id),
                )
            );
        } else {
            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Enums","/extorio-admin/enums"),
                    BreadCrumb::n(true,"Create new enum","","plus"),
                )
            );
        }
        $this->extensions = $this->_Extorio()->getExtensions();
    }
    
    public function delete($enumId = false) {
        if(!$enumId) {
            $this->_redirectToDefault();
        }
        if(!$this->canDelete) {
            $this->_messageWarning("You are not able to delete enums");
            $this->_redirectToDefault();
        }

        $e = Enum::findById($enumId);
        if($e) {
            $e->deleteThis();
        }

        $this->_Extorio()->messageSuccess("Enum successfully deleted");
        $this->_redirectToDefault();
    }
    
    public function values($enumId = false) {
        if(!$enumId) {
            $this->_redirectToDefault();
        }
        if(!$this->canModify) {
            $this->_messageWarning("You are not able to modify enums");
            $this->_redirectToDefault();
        }

        $this->enum = Enum::findById($enumId);

        $this->_Extorio()->setTargetBreadCrumbs(
            array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"Enums","/extorio-admin/enums"),
                BreadCrumb::n(false,$this->enum->label,"/extorio-admin/enums/edit/".$this->enum->id),
                BreadCrumb::n(true,"Values","/extorio-admin/enums/values/".$this->enum->id),
            )
        );
    }
}