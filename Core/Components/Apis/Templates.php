<?php
namespace Core\Components\Apis;
use Core\Classes\Models\Template;

/**
 * This api is used to modify a template
 *
 * Class Templates
 */
class Templates extends \Core\Classes\Commons\Api {
    public $data;

    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "POST" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        if(!is_array($this->data)) {
                            $this->_failApi("The data property is not set or could not be read");
                        }
                        if(!Template::existsById($id)) {
                            $this->_notFound();
                        }
                        if(!\Core\Classes\Utilities\Templates::canUserModifyTemplate($this->_getLoggedInUserId(),$id)) {
                            $this->_accessDenied("You are not able to modify this template");
                        }
                        $dt = Template::constructFromArray($this->data);
                        $dt->id = intval($id);
                        $dt->pushThis();
                        $this->_output->data = $dt;
                    } else {
                        $this->_badRequest();
                    }
                }
                break;
            default :
                $this->_badRequest();
                break;
        }
    }
}