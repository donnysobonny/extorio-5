<?php
namespace Core\Components\Apis;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\BlockProcessorCategory;
use Core\Classes\Utilities\Users;

/**
 * This api is used to get, create and modify block processor categories
 *
 * Class BlockProcessorCategories
 */
class BlockProcessorCategories extends \Core\Classes\Commons\Api {

    public $name;
    public $orderBy = "name";
    public $orderDirection = "asc";
    public $limit = 0;
    public $skip = 0;

    public function filter() {
        if($this->_httpMethod != "GET") {
            $this->_badRequest();
        }

        $query = Query::n();
        $where = array();
        if(strlen($this->name)) {
            $where[] = array("name" => array(Query::_lk => "%".$this->name."%"));
        }
        $query->where($where);
        $queryCount = clone($query);

        $query->order(array($this->orderBy => $this->orderDirection))->limit($this->limit)->skip($this->skip);

        $this->_output->data = BlockProcessorCategory::findAll($query,1);

        $this->_output->countLimited = BlockProcessorCategory::findCount($query);
        $this->_output->countUnlimieted = BlockProcessorCategory::findCount($queryCount);
    }

    public $data;

    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "GET" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $bpc = BlockProcessorCategory::findById($id,1);
                        if(!$bpc) {
                            $this->_notFound();
                        }
                        $this->_output->data = $bpc;
                    } else {
                        $this->_badRequest();
                    }
                }
                break;
            case "POST" :
                if(!$id) {
                    if(!is_array($this->data)) {
                        $this->_failApi("The data property is not set or could not be read");
                    }
                    $loggedInUser = $this->_getLoggedInUser();
                    if(!Users::userHasPrivilege($loggedInUser->id,"block_processor_categories_create","Core")) {
                        $this->_accessDenied("You are not able to create block processor categories");
                    }
                    $dbpc = BlockProcessorCategory::constructFromArray($this->data);
                    $dbpc->id = null;
                    $dbpc->pushThis();
                    $this->_output->data = $dbpc;
                } else {
                    if(!$action) {
                        if(!is_array($this->data)) {
                            $this->_failApi("The data property is not set or could not be read");
                        }
                        $loggedInUser = $this->_getLoggedInUser();
                        if(!Users::userHasPrivilege($loggedInUser->id,"block_processor_categories_modify","Core")) {
                            $this->_accessDenied("You are not able to modify block processor categories");
                        }
                        if(!BlockProcessorCategory::existsById($id)) {
                            $this->_notFound();
                        }
                        $dbpc = BlockProcessorCategory::constructFromArray($this->data);
                        $dbpc->id = $id;
                        $dbpc->pushThis();
                        $this->_output->data = $dbpc;
                    } else {
                        switch($action) {
                            case "delete" :
                                $loggedInUser = $this->_getLoggedInUser();
                                if(!Users::userHasPrivilege($loggedInUser->id,"block_processor_categories_delete","Core")) {
                                    $this->_accessDenied("You are not able to delete block processor categories");
                                }
                                $bpc = BlockProcessorCategory::findById($id,1);
                                if(!$bpc) {
                                    $this->_notFound();
                                }
                                $bpc->deleteThis();
                                break;
                            default :
                                $this->_badRequest();
                                break;
                        }
                    }
                }
                break;
        }
    }
}