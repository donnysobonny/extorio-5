<?php
namespace Core\Components\Apis;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\UserContact;
use Core\Classes\Utilities\Users;

/**
 * This api is used to manage user contacts
 *
 * Class UserContacts
 */
class UserContacts extends \Core\Classes\Commons\Api {

    public $userId;
    public $byUserId;
    public $type;
    public $topicId;
    public $identifier;
    public $dateFrom;
    public $dateTo;
    public $timeFrom;
    public $timeTo;
    public $limit = 0;
    public $skip = 0;

    public function filter() {
        if(!strlen($this->userId) || $this->userId <= 0) {
            $this->_failApi("You must supply a user id");
        }
        if(!Users::userManagesUserAndHasPrivilege($this->_getLoggedInUserId(),$this->userId,"user_contacts_read")) {
            $this->_accessDenied("You are not able to read the notes of this user");
        }
        $query = Query::n();
        $where = array();
        $where[] = array("userId" => $this->userId);
        if(strlen($this->byUserId)) {
            $where[] = array("byUserId" => $this->byUserId);
        }
        if(strlen($this->type)) {
            $where[] = array("type" => $this->type);
        }
        if(strlen($this->topicId)) {
            $where[] = array("topic.id" => $this->topicId);
        }
        if(strlen($this->identifier)) {
            $where[] = array("identifier" => array(
                Query::_lk => "%".$this->identifier."%"
            ));
        }
        if(strlen($this->dateFrom)) {
            $where[] = array("date" => array(
                Query::_gte => date("Y-m-d",strtotime($this->dateFrom))
            ));
        }
        if(strlen($this->dateTo)) {
            $where[] = array("date" => array(
                Query::_lte => date("Y-m-d",strtotime($this->dateTo))
            ));
        }
        if(strlen($this->timeFrom)) {
            $where[] = array("time" => array(
                Query::_gte => $this->timeFrom
            ));
        }
        if(strlen($this->timeTo)) {
            $where[] = array("time" => array(
                Query::_lte => $this->timeTo
            ));
        }

        $query->where($where);
        $clone = clone($query);

        $query->limit($this->limit)->skip($this->skip)->order(array(
            "date" => "desc",
            "time" => "desc"
        ));

        $this->_output->data = UserContact::findAll($query,2);

        $this->_output->countLimited = UserContact::findCount($query);
        $this->_output->countUnlimited = UserContact::findCount($clone);
    }

    public $data;

    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "GET" :
                $this->_badRequest();
                break;
            case "POST" :
                if(!$this->_getLoggedInUser()) {
                    $this->_accessDenied("You must be logged in to do that");
                }

                if(!$id) {
                    if(!is_array($this->data)) {
                        $this->_failApi("The data property is not set or could not be read");
                    }
                    $dc = UserContact::constructFromArray($this->data);
                    $dc->id = null;
                    if(!strlen($dc->userId) || $dc->userId <= 0) {
                        $this->_failApi("A user contact must have a user id");
                    }
                    $dc->byUserId = $this->_getLoggedInUserId();
                    if(!Users::userManagesUserAndHasPrivilege($dc->byUserId,$dc->userId,"user_contacts_create")) {
                        $this->_accessDenied("You are not able to create user contacts for this user");
                    }

                    $dc->pushThis();
                    $this->_output->data = $dc;

                } else {
                    if(!$action) {
                        if(!is_array($this->data)) {
                            $this->_failApi("The data property is not set or could not be read");
                        }
                        $c = UserContact::findById($id,2);
                        if(!$c) {
                            $this->_notFound();
                        }
                        $c->byUserId = $this->_getLoggedInUserId();
                        if(!Users::userManagesUserAndHasPrivilege($c->byUserId,$c->userId,"user_contacts_modify")) {
                            $this->_accessDenied("You are not able to modify this users's contacts");
                        }
                        $dc = UserContact::constructFromArray($this->data);
                        $c->type = $dc->type;
                        $c->topic = $dc->topic;
                        $c->identifier = $dc->identifier;

                        $c->pushThis();
                        $this->_output->data = $c;

                    } else {
                        switch($action) {
                            case "delete" :
                                $c = UserContact::findById($id,2);
                                if(!$c) {
                                    $this->_notFound();
                                }
                                $c->byUserId = $this->_getLoggedInUserId();
                                if(!Users::userManagesUserAndHasPrivilege($c->byUserId,$c->userId,"user_contacts_delete")) {
                                    $this->_accessDenied("You are not able to delete this users's contacts");
                                }

                                $c->deleteThis();

                                break;
                            default :
                                $this->_badRequest();
                                break;
                        }
                    }
                }
                break;
        }
    }
}