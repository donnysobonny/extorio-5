<?php
namespace Core\Components\Apis;

use Core\Classes\Commons\Enum;
use Core\Classes\Enums\PHPTypes;
use Core\Classes\Enums\PropertyBasicType;
use Core\Classes\Enums\PropertyType;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Model;
use Core\Classes\Utilities\Namespaces;
use Core\Classes\Utilities\Users;

class ModelViewer extends \Core\Classes\Commons\Api {

    public $draw;
    public $start;
    public $length;
    public $search = array();
    public $order = array();
    public $columns = array();

    public $selectable = false;
    public $selectableTarget = "";
    public $selectableNamePath = array();
    public $selectableRemovable = false;

    public $namePath = array();
    public $removable = false;

    public function _onDefault() {
        parent::_onDefault();
    }

    public function _onBegin() {
        parent::_onBegin();
    }

    public function _onEnd() {
        parent::_onEnd();
    }

    public function listview($modelIdentifier = false) {
        if(!Users::userHasPrivilege($this->_getLoggedInUserId(),"model_viewer_list","Core")) {
            $this->_accessDenied("You are not able to use list mode in the model viewer");
        }
        if (!$modelIdentifier) {
            $this->_failApi("No model specified");
        }
        $model = $this->getModel($modelIdentifier);
        if (!$model) {
            $this->_failApi("Cannot find a model using the identifier: " . $modelIdentifier);
        }

        //build the column data
        $columns = array();
        //for the id field
        $columns[] = array("name" => "id", "searchable" => true, "orderable" => true);
        foreach ($model->properties as $property) {
            $columns[] = array("name" => $property->name, "searchable" => $property->isIndex, "orderable" => $property->isIndex);
        }
        //add the action field
        $columns[] = array("name" => "action", "searchable" => false, "orderable" => false);

        $uid = uniqid();
        ob_start();
        $colid = 0;
        ?>
        <div class="table-responsive">
            <table style="width: 100%;" id="<?= $uid ?>"
                   class="table table-striped table-condensed table-bordered listview_table">
                <thead>
                <tr>
                    <td><input type="text" class="form-control input-sm input-sm" placeholder="Search..."
                               data-colid="<?= $colid ?>"/></td>
                    <?php
                    $colid++;
                    foreach ($model->properties as $property) {
                        if ($property->isIndex) {
                            switch ($property->propertyType) {
                                case PropertyType::_basic :
                                    switch ($property->basicType) {
                                        case PropertyBasicType::_checkbox :
                                            ?>
                                            <td>
                                                <select class="form-control input-sm input-sm"
                                                        data-colid="<?= $colid ?>">
                                                    <option value=""></option>
                                                    <option value="t">TRUE</option>
                                                    <option value="f">FALSE</option>
                                                </select>
                                            </td>
                                            <?php
                                            break;
                                        default :
                                            ?>
                                            <td><input type="text" class="form-control input-sm input-sm"
                                                       placeholder="Search..." data-colid="<?= $colid ?>"/></td>
                                            <?php
                                            break;
                                    }
                                    break;
                                case PropertyType::_enum :
                                    /** @var Enum $type */
                                    $type = $property->enumNamespace;
                                    ?>
                                    <td>
                                        <select class="form-control input-sm input-sm" data-colid="<?= $colid ?>">
                                            <option value=""></option>
                                            <?php
                                            foreach ($type::values() as $value) {
                                                ?>
                                                <option value="<?= $value ?>"><?= $value ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <?php
                                    break;
                            }
                        } else {
                            ?>
                            <td></td>
                        <?php
                        }
                        $colid++;
                    }
                    ?>
                    <td></td>
                </tr>
                <tr>
                    <th>ID</th>
                    <?php
                    foreach ($model->properties as $property) {
                        ?>
                        <th>
                            <?= $property->label ?>
                        </th>
                    <?php
                    }
                    ?>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
                </thead>
            </table>
        </div>
        <style>
            .table-responsive .row {
                margin: 0;
            }

            .listview_table td {
                font-size: 8px;
            }

            .listview_table th {
                font-size: 10px;
            }

            .listview_table input, .listview_table select {
                font-size: 9px;
                padding: 3px;
                width: 100% !important;
                height: inherit;
            }

            .dataTables_filter {
                display: none;
            }
        </style>
        <script>
            $(function () {
                var table = $('#<?=$uid?>').DataTable({
                    serverSide: true,
                    ajax: {
                        url: '/extorio/apis/model-viewer/listview_data/<?=$modelIdentifier?>',
                        data: {
                            selectable: <?=$this->selectable ? "true":"false"?>,
                            selectableTarget: "<?=$this->selectableTarget?>",
                            selectableNamePath: <?=json_encode($this->selectableNamePath)?>,
                            selectableRemovable: <?=$this->selectableRemovable ? "true":"false"?>
                        }
                    },
                    columns: <?=json_encode($columns)?>
                });

                // Apply the search
                table.columns().eq(0).each(function (colIdx) {
                    $('input[data-colid="' + colIdx + '"], select[data-colid="' + colIdx + '"]').on('keyup change', function () {
                        console.log(colIdx);
                        table
                            .column(colIdx)
                            .search(this.value)
                            .draw();
                    }).on('click', function (e) {
                        e.stopPropagation();
                    });
                });
            });
        </script>
        <?php
        $this->_output->html = ob_get_contents();
        ob_end_clean();
    }

    /**
     *
     * @param bool $modelIdentifier
     */
    public function listview_data($modelIdentifier = false) {
        if(!Users::userHasPrivilege($this->_getLoggedInUserId(),"model_viewer_list","Core")) {
            $this->_accessDenied("You are not able to use list mode in the model viewer");
        }
        unset($this->_output->error);
        if (!$modelIdentifier) {
            $this->_failApi("No model specified");
        }
        $model = $this->getModel($modelIdentifier);
        if (!$model) {
            $this->_failApi("Cannot find a model using the identifier: " . $modelIdentifier);
        }
        $this->_output->draw = intval($this->draw);

        /** @var Model $type */
        $type = $model->namespace;
        $q = Query::n();
        $where = array();

        //build the where, based on the fields that have a search value
        foreach ($this->columns as $column) {
            if (strlen($column["search"]["value"])) {
                $where[] = array($column["name"] => array('$lk' => "%" . $column["search"]["value"] . "%"));
            }
        }

        $q->where($where);
        $q->limit($this->length);
        $q->skip($this->start);
        $q->order(array($this->columns[$this->order[0]["column"]]["name"] => '$' . $this->order[0]["dir"]));
        $objects = $type::findAll($q);
        $dataFieldsSorted = array();

        $n = 0;
        foreach ($objects as $object) {
            $dataFieldsSorted[$n][] = $object->id;
            foreach ($model->properties as $property) {
                $name = $property->label;
                switch ($property->propertyType) {
                    case PropertyType::_basic :
                        switch ($property->basicType) {
                            case PropertyBasicType::_checkbox :
                                $dataFieldsSorted[$n][] = $object->$name ? "TRUE" : "FALSE";
                                break;
                            default :
                                $dataFieldsSorted[$n][] = $object->$name;
                                break;
                        }
                        break;
                    case PropertyType::_enum :
                        $dataFieldsSorted[$n][] = $object->$name;
                        break;
                    case PropertyType::_meta :
                        $dataFieldsSorted[$n][] = json_encode($object->$name);
                        break;
                    case PropertyType::_complex :
                        $childModel = Namespaces::getClassName($property->childModelNamespace);
                        switch ($property->complexType) {
                            case PHPTypes::_object :
                                $child = $object->$name;
                                if (gettype($child) == PHPTypes::_integer) {
                                    $dataFieldsSorted[$n][] = '<button onclick="$.extorio_modelViewer_detail_dialog(\'' . $childModel . '\',' . $child . ');" class="btn btn-primary btn-xs"><span class="fa fa-download"></span> ' . $child . '</button>';
                                } else {
                                    $dataFieldsSorted[$n][] = "";
                                }
                                break;
                            case PHPTypes::_array :
                                $children = $object->$name;
                                $buttons = "";
                                if (gettype($children) == PHPTypes::_array) {
                                    for ($i = 0; $i < count($children); $i++) {
                                        if (gettype($children[$i]) == PHPTypes::_integer) {
                                            $buttons .= '<button style="margin: 0px 2px 2px 0px;" onclick="$.extorio_modelViewer_detail_dialog(\'' . $childModel . '\',' . $children[$i] . ');" class="btn btn-primary btn-xs"><span class="fa fa-download"></span> ' . $children[$i] . '</button>';
                                        }
                                    }
                                }
                                $dataFieldsSorted[$n][] = $buttons;
                                break;
                        }
                        break;
                }

            }
            if ($this->selectable) {
                $dataFieldsSorted[$n][] = '
                <button onclick="$(\'' . $this->selectableTarget . '\').extorio_modelViewer_edit(\'' . $modelIdentifier . '\',' . $object->id . ',{namePath:' . str_replace('"', "'", json_encode($this->selectableNamePath)) . ',removable:' . var_export($this->selectableRemovable, true) . '});$.extorio_dialog_close();" class="btn btn-xs btn-primary"><span class="fa fa-download"></span> select</button>
                ';
            } else {
                $dataFieldsSorted[$n][] = '
                <button onclick="$.extorio_modelViewer_detail_dialog(\'' . $modelIdentifier . '\',' . $object->id . ');" style="margin: 0px 2px 2px 0px" class="btn btn-xs btn-primary"><span class="fa fa-download"></span> detail</button>
                <a style="margin: 0px 2px 2px 0px" class="btn btn-xs btn-primary" href="/extorio-admin/model-viewer/' . $modelIdentifier . '/edit/' . $object->id . '"><span class="fa fa-edit"></span> edit</a>
                <button onclick="$.extorio_modelViewer_delete_dialog(\'' . $modelIdentifier . '\',' . $object->id . ',function(){window.location.href=\'/extorio-admin/model-viewer/'.$modelIdentifier.'\'});" style="margin: 0px 2px 2px 0px" class="btn btn-xs btn-danger"><span class="fa fa-trash"></span> delete</button>
                ';
            }

            $n++;
        }

        $this->_output->recordsTotal = $type::findCount();
        $this->_output->recordsFiltered = $type::findCount(Query::n()->where($where));

        $this->_output->data = $dataFieldsSorted;
    }

    public function editview($modelIdentifier = false, $id = false) {
        if(!Users::userHasPrivilege($this->_getLoggedInUserId(),"model_viewer_edit","Core")) {
            $this->_accessDenied("You are not able to use edit mode in the model viewer");
        }
        if (!$modelIdentifier) {
            $this->_failApi("No model specified");
        }
        $model = $this->getModel($modelIdentifier);
        if (!$model) {
            $this->_failApi("Cannot find a model using the identifier: " . $modelIdentifier);
        }
        /** @var Model $type */
        $type = $model->namespace;
        if ($id) {
            $object = $type::findById($id);
        } else {
            $object = $type::n();
        }
        ob_start();
        if (!$object) {
            echo "Object not found";
        } else {
            $tableid = uniqid();
            ?>
            <table id="<?= $tableid ?>" class="model_view_table">
                <tbody>
                <tr>
                    <td class="model_view_table_left">ID</td>
                    <td class="model_view_table_right">
                        <?php
                        if ($id) {
                            ?>
                            <input class="form-control input-sm" type="number" disabled="disabled"
                                    value="<?= $object->id ?>"/>
                            <input class="form-control input-sm" type="hidden"
                                   name="data<?= $this->namePathToAttr() ?>[id]" value="<?= $object->id ?>"/>
                        <?php
                        } else {
                            ?>
                            <input class="form-control input-sm" type="number"
                                   name="data<?= $this->namePathToAttr() ?>[id]" value=""/>
                        <?php
                        }
                        ?>

                    </td>
                </tr>
                <?php
                foreach ($model->properties as $property) {
                    $name = $property->label;
                    ?>
                    <tr>
                        <td class="model_view_table_left"><?= $property->label ?></td>
                        <td class="model_view_table_right">
                            <?php
                            switch ($property->propertyType) {
                                case PropertyType::_basic :
                                    switch ($property->basicType) {
                                        case PropertyBasicType::_checkbox :
                                            ?>
                                            <input data-target="model_view_table_checkbox_value_<?= $name ?>"
                                                   class="form-control input-sm model_view_table_checkbox_display"
                                                   type="checkbox"
                                                   name="data<?= $this->namePathToAttr() ?>[<?= $name ?>]" <?php
                                            if ($object->$name)
                                                echo 'checked="checked"';
                                            ?> />
                                            <input type="hidden" id="model_view_table_checkbox_value_<?= $name ?>"
                                                   name="data<?= $this->namePathToAttr() ?>[<?= $name ?>]" value="<?php
                                            if ($object->$name)
                                                echo 'true'; else echo 'false';
                                            ?>"/>
                                            <?php
                                            break;
                                        case PropertyBasicType::_email :
                                            ?>
                                            <input class="form-control input-sm" type="text"
                                                   name="data<?= $this->namePathToAttr() ?>[<?= $name ?>]"
                                                   value="<?= $object->$name ?>"/>
                                            <?php
                                            break;
                                        case PropertyBasicType::_password :
                                            ?>
                                            <input class="form-control input-sm" type="text"
                                                   name="data<?= $this->namePathToAttr() ?>[<?= $name ?>]"
                                                   value="<?= $object->$name ?>"/>
                                            <?php
                                            break;
                                        case PropertyBasicType::_textfield :
                                            ?>
                                            <input class="form-control input-sm" type="text"
                                                   name="data<?= $this->namePathToAttr() ?>[<?= $name ?>]"
                                                   value="<?= $object->$name ?>"/>
                                            <?php
                                            break;
                                        case PropertyBasicType::_textarea :
                                            ?>
                                            <textarea class="form-control input-sm"
                                                      name="data<?= $this->namePathToAttr() ?>[<?= $name ?>]"><?= $object->$name ?></textarea>
                                            <?php
                                            break;
                                        case PropertyBasicType::_number :
                                            ?>
                                            <input class="form-control input-sm model_view_table_number" type="text"
                                                   name="data<?= $this->namePathToAttr() ?>[<?= $name ?>]"
                                                   value="<?= $object->$name ?>"/>
                                            <?php
                                            break;
                                        case PropertyBasicType::_decimal :
                                            ?>
                                            <input class="form-control input-sm model_view_table_decimal" type="text"
                                                   name="data<?= $this->namePathToAttr() ?>[<?= $name ?>]"
                                                   value="<?= $object->$name ?>"/>
                                            <?php
                                            break;
                                        case PropertyBasicType::_date :
                                            ?>
                                            <input class="form-control input-sm model_view_table_date" type="date"
                                                   name="data<?= $this->namePathToAttr() ?>[<?= $name ?>]"
                                                   value="<?= $object->$name ?>"/>
                                            <?php
                                            break;
                                        case PropertyBasicType::_time :
                                            ?>
                                            <input class="form-control input-sm model_view_table_time" type="time"
                                                   name="data<?= $this->namePathToAttr() ?>[<?= $name ?>]"
                                                   value="<?= $object->$name ?>"/>
                                            <?php
                                            break;
                                        case PropertyBasicType::_datetime :
                                            ?>
                                            <input class="form-control input-sm model_view_table_datetime"
                                                   type="datetime"
                                                   name="data<?= $this->namePathToAttr() ?>[<?= $name ?>]"
                                                   value="<?= $object->$name ?>"/>
                                            <?php
                                            break;
                                    }
                                    break;
                                case PropertyType::_enum :
                                    /** @var Enum $enum */
                                    $enum = $property->enumNamespace;
                                    $values = $enum::values();
                                    ?>
                                    <select class="form-control input-sm"
                                            name="data<?= $this->namePathToAttr() ?>[<?= $name ?>]">
                                        <?php
                                        foreach ($values as $value) {
                                            ?>
                                            <option <?php
                                            if ($object->$name == $value)
                                                echo 'selected="selected"';
                                            ?> value="<?= $value ?>"><?= $value ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    <?php
                                    break;
                                case PropertyType::_meta :
                                    ?>
                                    <textarea class="form-control input-sm"
                                              name="data<?= $this->namePathToAttr() ?>[<?= $name ?>]"><?= json_encode($object->$name) ?></textarea>
                                    <?php
                                    break;
                                case PropertyType::_complex :
                                    $childModel = Namespaces::getClassName($property->childModelNamespace);
                                    switch ($property->complexType) {
                                        case PHPTypes::_object :
                                            $insertid = uniqid();
                                            $childNamePath = $this->namePath;
                                            $childNamePath[] = $name;
                                            ?>
                                            <div>
                                                <table class="model_view_table">
                                                    <tr>
                                                        <td class="bg-info">
                                                            <button type="button"
                                                                onclick="$.extorio_modelViewer_list_dialog('<?=$childModel?>',{
                                                                        selectable: true,
                                                                        selectableTarget: '#<?=$insertid?>',
                                                                        selectableNamePath: <?=str_replace('"',"'",json_encode($childNamePath))?>,
                                                                        selectableRemovable: true
                                                                    })"
                                                                class="btn btn-xs btn-primary"><span
                                                                    class="fa fa-search"></span> select
                                                            </button>
                                                            <button type="button"
                                                                onclick="$('#<?= $insertid ?>').extorio_modelViewer_edit('<?= $childModel ?>',false,{
                                                                    removable: true,
                                                                    namePath: <?= str_replace('"', "'", json_encode($childNamePath)) ?>
                                                                    })" class="btn btn-xs btn-primary"><span
                                                                    class="fa fa-plus"></span> create
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="<?= $insertid ?>">
                                                            <?php
                                                            $child = $object->$name;
                                                            if (gettype($child) == PHPTypes::_integer) {
                                                                ?>
                                                                <input type="hidden"
                                                                       name="data<?= $this->namePathToAttr() ?>[<?= $name ?>]"
                                                                       value="<?= $child ?>"/>
                                                                <button type="button"
                                                                    onclick="$('#<?= $insertid ?>').extorio_modelViewer_edit('<?= $childModel ?>',<?= $child ?>,{
                                                                        removable: true,
                                                                        namePath: <?= str_replace('"', "'", json_encode($childNamePath)) ?>
                                                                        })" class="btn btn-xs btn-primary"><span
                                                                        class="fa fa-download"></span> <?= $child ?>
                                                                </button>
                                                                <button type="button" onclick="
                                                                    var input = $('<input />');
                                                                    input.attr('type','hidden');
                                                                    input.attr('name','data<?=$this->namePathToAttr()?>[<?= $name ?>]');
                                                                    input.attr('value','empty');
                                                                    $('#<?= $insertid ?>').html(input);"
                                                                        class="btn btn-xs btn-warning"><span
                                                                        class="fa fa-remove"></span></button>
                                                            <?php
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <?php
                                            break;
                                        case PHPTypes::_array :
                                            $insertid = uniqid();
                                            $childNamePath = $this->namePath;
                                            $childNamePath[] = $name;
                                            ?>
                                            <div>
                                                <table class="model_view_table">
                                                    <tr>
                                                        <td class="bg-info">
                                                            <button type="button"
                                                                onclick="
                                                                    var guid = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
                                                                    var childNamePath = <?=str_replace('"',"'",json_encode($childNamePath))?>;
                                                                    childNamePath.push(guid);
                                                                    var div = $('<div></div>');
                                                                    div.attr('id',guid);
                                                                    $('#<?=$insertid?>').append(div);
                                                                    $.extorio_modelViewer_list_dialog('<?=$childModel?>',{
                                                                        selectable: true,
                                                                        selectableTarget: '#'+guid,
                                                                        selectableNamePath: childNamePath,
                                                                        selectableRemovable: true
                                                                    })"
                                                                class="btn btn-xs btn-primary"><span
                                                                    class="fa fa-search"></span> select
                                                            </button>
                                                            <button type="button"
                                                                onclick="
                                                                    var guid = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
                                                                    var childNamePath = <?=str_replace('"',"'",json_encode($childNamePath))?>;
                                                                    childNamePath.push(guid);
                                                                    var div = $('<div></div>');
                                                                    div.attr('id',guid);
                                                                    div.extorio_modelViewer_edit('<?= $childModel ?>',false,{
                                                                        removable: true,
                                                                        namePath: childNamePath
                                                                    });
                                                                    $('#<?=$insertid?>').append(div);
                                                                    " class="btn btn-xs btn-primary"><span
                                                                    class="fa fa-plus"></span> create
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="<?= $insertid ?>">
                                                            <?php
                                                            $children = $object->$name;
                                                            if(gettype($children) == PHPTypes::_array) {
                                                                for($i = 0; $i < count($children); $i++) {
                                                                    $uid = uniqid();
                                                                    $childNamePath_element = $childNamePath;
                                                                    $childNamePath_element[] = $uid;
                                                                    ?>
                                                                    <div id="<?=$uid?>">
                                                                        <input type="hidden"
                                                                               name="data<?= $this->namePathToAttr() ?>[<?= $name ?>][<?=$uid?>]"
                                                                               value="<?= $children[$i] ?>"/>
                                                                        <button type="button" style="margin: 0px 2px 2px 0px"
                                                                            onclick="$('#<?= $uid ?>').extorio_modelViewer_edit('<?= $childModel ?>',<?= $children[$i] ?>,{
                                                                                removable: true,
                                                                                namePath: <?= str_replace('"', "'", json_encode($childNamePath_element)) ?>
                                                                                })" class="btn btn-xs btn-primary"><span
                                                                                class="fa fa-download"></span> <?= $children[$i] ?>
                                                                        </button>
                                                                        <button type="button" style="margin: 0px 2px 2px 0px" onclick="$('#<?= $uid ?>').remove();"
                                                                                class="btn btn-xs btn-warning"><span
                                                                                class="fa fa-remove"></span></button>
                                                                    </div>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <?php
                                            break;
                                    }
                                    break;
                            }
                            ?>
                        </td>
                    </tr>
                <?php
                }
                if ($this->removable) {
                    ?>
                    <tr>
                        <td colspan="2">
                            <button type="button" onclick="
                                var input = $('<input />');
                                input.attr('type','hidden');
                                input.attr('name','data<?=$this->namePathToAttr()?>');
                                input.attr('value','empty');
                                $('#<?= $tableid ?>').parent().append(input);
                                $('#<?= $tableid ?>').remove();"
                                    class="btn btn-xs btn-warning btn-block"><span class="fa fa-remove"></span>
                                remove
                            </button>
                        </td>
                    </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
        <?php
        }
        $this->_output->html = ob_get_contents();
        ob_end_clean();
    }

    private function namePathToAttr() {
        $attr = "";
        foreach ($this->namePath as $part) {
            $attr .= "[" . $part . "]";
        }
        return $attr;
    }

    public function delete($modelIdentifier = false, $id = false) {
        if(!Users::userHasPrivilege($this->_getLoggedInUserId(),"model_viewer_delete","Core")) {
            $this->_accessDenied("You are not able to delete entries in the model viewer");
        }
        if (!$modelIdentifier) {
            $this->_failApi("No model specified");
        }
        $model = $this->getModel($modelIdentifier);
        if (!$model) {
            $this->_failApi("Cannot find a model using the identifier: " . $modelIdentifier);
        }
        if (!$id) {
            $this->_failApi("You must specify the target id");
        }
        /** @var Model $type */
        $type = $model->namespace;
        $obj = $type::findById($id,1);
        if(!$obj) {
            $this->_notFound();
        }
        $obj->deleteThis();
    }

    public function detailview($modelIdentifier = false, $id = false) {
        if(!Users::userHasPrivilege($this->_getLoggedInUserId(),"model_viewer_detail","Core")) {
            $this->_accessDenied("You are not able to use detail mode in the model viewer");
        }
        if (!$modelIdentifier) {
            $this->_failApi("No model specified");
        }
        $model = $this->getModel($modelIdentifier);
        if (!$model) {
            $this->_failApi("Cannot find a model using the identifier: " . $modelIdentifier);
        }
        if (!$id) {
            $this->_failApi("You must specify the target id");
        }
        /** @var Model $type */
        $type = $model->namespace;
        $object = $type::findById($id);
        ob_start();
        if (!$object) {
            echo "Object not found";
        } else {
            ?>
            <table class="model_view_table">
                <tbody>
                <tr>
                    <td class="model_view_table_left">ID</td>
                    <td class="model_view_table_right"><strong><?= $object->id ?></strong></td>
                </tr>
                <?php
                foreach ($model->properties as $property) {
                    $name = $property->label;
                    ?>
                    <tr>
                        <td class="model_view_table_left"><?= $property->label ?></td>
                        <td class="model_view_table_right">
                            <?php
                            switch ($property->propertyType) {
                                case PropertyType::_basic :
                                    switch ($property->basicType) {
                                        case PropertyBasicType::_checkbox :
                                            echo $object->$name ? "TRUE" : "FALSE";
                                            break;
                                        default :
                                            echo $object->$name;
                                            break;
                                    }
                                    break;
                                case PropertyType::_enum :
                                    echo $object->$name;
                                    break;
                                case PropertyType::_meta :
                                    echo json_encode($object->$name);
                                    break;
                                case PropertyType::_complex :
                                    $childModel = Namespaces::getClassName($property->childModelNamespace);
                                    switch ($property->complexType) {
                                        case PHPTypes::_object :
                                            $uid = uniqid();
                                            $child = $object->$name;
                                            if (gettype($child) == PHPTypes::_integer) {
                                                ?>
                                                <div id="<?= $uid ?>">
                                                    <button type="button"
                                                        onclick="$('#<?= $uid ?>').extorio_modelViewer_detail('<?= $childModel ?>',<?= $child ?>)"
                                                        class="btn btn-xs btn-primary"><span
                                                            class="fa fa-download"></span> <?= $child ?></button>
                                                </div>
                                            <?php
                                            }
                                            break;
                                        case PHPTypes::_array :
                                            $children = $object->$name;
                                            if (gettype($children) == PHPTypes::_array) {
                                                for ($i = 0; $i < count($children); $i++) {
                                                    if (gettype($children[$i]) == PHPTypes::_integer) {
                                                        $uid = uniqid();
                                                        ?>
                                                        <div id="<?= $uid ?>">
                                                            <button type="button" style="margin: 0px 1px 1px 0px;"
                                                                    onclick="$('#<?= $uid ?>').extorio_modelViewer_detail('<?= $childModel ?>',<?= $children[$i] ?>)"
                                                                    class="btn btn-xs btn-primary"><span
                                                                    class="fa fa-download"></span> <?= $children[$i] ?>
                                                            </button>
                                                        </div>
                                                    <?php
                                                    }
                                                }
                                            }
                                            break;
                                    }
                                    break;
                            }
                            ?>
                        </td>
                    </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
        <?php
        }
        $this->_output->html = ob_get_contents();
        ob_end_clean();
    }

    private function getModel($modelIdentifier) {
        return Model::findOne(Query::n()->where(array('$or' => array("class" => $modelIdentifier, "label" => $modelIdentifier))), 2);
    }
}