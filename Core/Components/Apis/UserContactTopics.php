<?php
namespace Core\Components\Apis;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\UserContactTopic;

/**
 * 
 *
 * Class UserContactTopics
 */
class UserContactTopics extends \Core\Classes\Commons\Api {
    public function _onDefault() {
        $this->_output->data = UserContactTopic::findAll(Query::n()->order("name"));
    }
}