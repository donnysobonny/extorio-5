<?php
namespace Core\Components\Apis;
use Core\Classes\Enums\InternalEvents;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\UserAction;
use Core\Classes\Utilities\Users;

/**
 * This api is used to get user actions
 *
 * Class UserActions
 */
class UserActions extends \Core\Classes\Commons\Api {

    public $userId;
    public $typeId;
    public $dateFrom;
    public $dateTo;
    public $timeFrom;
    public $timeTo;
    public $limit = 0;
    public $skip = 0;

    public function filter() {
        if(!strlen($this->userId) || $this->userId <= 0) {
            $this->_badRequest("You must specify the user id");
        }
        if(!Users::userManagesUserAndHasPrivilege($this->_getLoggedInUserId(),$this->userId,"user_actions_read")) {
            $this->_accessDenied("You are not able to view the actions of this user");
        }
        $query = Query::n();
        $where = array();
        $where[] = array("userId" => $this->userId);
        if(strlen($this->typeId)) {
            $where[] = array("type.id" => $this->typeId);
        }
        if(strlen($this->dateFrom)) {
            $where[] = array("date" => array(
                Query::_gte => date("Y-m-d", strtotime($this->dateFrom))
            ));
        }
        if(strlen($this->dateTo)) {
            $where[] = array("date" => array(
                Query::_lte => date("Y-m-d", strtotime($this->dateTo))
            ));
        }
        if(strlen($this->timeFrom)) {
            $where[] = array("time" => array(
                Query::_gte => $this->timeFrom
            ));
        }
        if(strlen($this->timeTo)) {
            $where[] = array("time" => array(
                Query::_lte => $this->timeTo
            ));
        }

        $query->where($where);
        $clone = clone($query);

        $query->order(array(
            "date" => "desc",
            "time" => "desc"
        ))->limit($this->limit)->skip($this->skip);

        $this->_output->data = UserAction::findAll($query,2);
        $this->_output->countLimited = UserAction::findCount($query);
        $this->_output->countUnlimited = UserAction::findCount($clone);
    }

    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "GET" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $ua = UserAction::findById($id,2);
                        if(!$ua) {
                            $this->_notFound();
                        }
                        if(!Users::userManagesUserAndHasPrivilege($this->_getLoggedInUserId(),$ua->userId,"user_actions_read")) {
                            $this->_accessDenied("You are not able to view the actions of this user");
                        }

                        $results = $this->_Extorio()->getEvent(InternalEvents::_user_actions_detail_display)->run(array($ua->type->name,"detail1",$ua->detail1),1);
                        foreach($results as $result) {
                            $ua->detail1 = $result;
                        }
                        $results = $this->_Extorio()->getEvent(InternalEvents::_user_actions_detail_display)->run(array($ua->type->name,"detail2",$ua->detail2),1);
                        foreach($results as $result) {
                            $ua->detail2 = $result;
                        }
                        $results = $this->_Extorio()->getEvent(InternalEvents::_user_actions_detail_display)->run(array($ua->type->name,"detail3",$ua->detail3),1);
                        foreach($results as $result) {
                            $ua->detail3 = $result;
                        }

                        $this->_output->data = $ua;

                    } else {
                        $this->_badRequest();
                    }
                }
                break;
            case "POST" :
                $this->_badRequest();
                break;
        }
    }

}