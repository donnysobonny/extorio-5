<?php
namespace Core\Components\Apis;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\UserReport;
use Core\Classes\Utilities\Users;

/**
 * This api is used to manage user reports
 *
 * Class UserReports
 */
class UserReports extends \Core\Classes\Commons\Api {

    public $name;
    public $limit = 0;
    public $skip = 0;

    public function filter() {
        $query = Query::n();
        $where = array();
        if(strlen($this->name)) {
            $where[] = array("name" => array(
                Query::_lk => "%".$this->name."%"
            ));
        }

        $query->where($where);
        $clone = clone($query);

        $query->order("name")->limit($this->limit)->skip($this->skip);

        $this->_output->data = UserReport::findAll($query);
        $this->_output->countLimited = UserReport::findCount($query);
        $this->_output->countUnlimited = UserReport::findCount($clone);
    }

    public $data;

    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "GET" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $ur = UserReport::findById($id);
                        if(!$ur) {
                            $this->_notFound();
                        }
                        $this->_output->data = $ur;
                    } else {
                        $this->_badRequest();
                    }
                }
                break;
            case "POST" :
                if(!$id) {
                    if(!is_array($this->data)) {
                        $this->_failApi("The data property is not set or could not be read");
                    }
                    if(!Users::userHasPrivilege($this->_getLoggedInUserId(),"user_reports_crete")) {
                        $this->_accessDenied("You are not able to create user reports");
                    }

                    $dur = UserReport::constructFromArray($this->data);
                    $dur->id = null;
                    $dur->pushThis();

                    $this->_output->data = $dur;

                } else {
                    if(!$action) {

                    } else {
                        switch($action) {

                            default:
                                $this->_badRequest();
                                break;
                        }
                    }
                }
                break;
        }
    }
}