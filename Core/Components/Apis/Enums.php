<?php
namespace Core\Components\Apis;
use Core\Classes\Models\Enum;
use Core\Classes\Utilities\Users;

/**
 * This api is used to get and update enum values
 *
 * Class Enums
 */
class Enums extends \Core\Classes\Commons\Api {

    public $values;
    public $value;

    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "GET" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $this->_badRequest();
                    } else {
                        switch($action) {
                            case "values" :
                                $e = Enum::findById($id,1);
                                if(!$e) {
                                    $this->_notFound();
                                }
                                $this->_output->values = $e->values;
                                break;
                            default :
                                $this->_badRequest();
                                break;
                        }
                    }
                }
                break;
            case "POST" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $this->_badRequest();
                    } else {
                        switch($action) {
                            case "values" :
                                if(!is_array($this->values)) {
                                    $this->_failApi("The values array is not set or could not be read");
                                }
                                if(!Users::userHasPrivilege($this->_getLoggedInUser()->id,"enums_modify","Core")) {
                                    $this->_accessDenied("You are not able to modify enums");
                                }
                                $e = Enum::findById($id,1);
                                if(!$e) {
                                    $this->_notFound();
                                }
                                $e->values = $this->values;
                                $e->pushThis();

                                $this->_output->values = $e->values;
                                break;
                            case "add_value" :
                                if(!strlen($this->values)) {
                                    $this->_failApi("You must specify the value that you want to add");
                                }
                                if(!Users::userHasPrivilege($this->_getLoggedInUser()->id,"enums_modify","Core")) {
                                    $this->_accessDenied("You are not able to modify enums");
                                }
                                $e = Enum::findById($id,1);
                                if(!$e) {
                                    $this->_notFound();
                                }
                                if(!in_array($this->values,$e->values)) {
                                    $e->values[] = $this->value;
                                }
                                $e->pushThis();

                                $this->_output->values = $e->values;
                                break;
                            case "remove_value" :
                                if(!strlen($this->values)) {
                                    $this->_failApi("You must specify the value that you want to add");
                                }
                                if(!Users::userHasPrivilege($this->_getLoggedInUser()->id,"enums_modify","Core")) {
                                    $this->_accessDenied("You are not able to modify enums");
                                }
                                $e = Enum::findById($id,1);
                                if(!$e) {
                                    $this->_notFound();
                                }
                                $values = $e->values;
                                $newValues = array();
                                foreach($values as $value) {
                                    if($values != $this->value) {
                                        $newValues[] = $values;
                                    }
                                }
                                $e->values = $newValues;
                                $e->pushThis();

                                $this->_output->values = $e->values;
                                break;
                        }
                    }
                }
                break;
        }
    }
}