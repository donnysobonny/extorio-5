<?php
namespace Core\Components\Apis;
use Core\Classes\Models\Property;
use Core\Classes\Utilities\Users;

/**
 * This api is currently just used to get a property
 *
 * Class Properties
 */
class Properties extends \Core\Classes\Commons\Api {

    public $data;

    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "GET" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $p = Property::findById($id,1);
                        if(!$p) {
                            $this->_notFound();
                        }
                        $this->_output->data = $p;
                    } else {
                        $this->_badRequest();
                    }
                }
                break;
            case "POST" :
                if(!$id) {
                    if(!is_array($this->data)) {
                        $this->_failApi("The data property is not set or could not be read");
                    }
                    if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
                        "models_create",
                        "models_modify"
                    ),"Core")) {
                        $this->_failApi("You are not able to create properties");
                    }

                    $dp = Property::constructFromArray($this->data);
                    $dp->id = null;
                    $dp->pushThis();
                    $this->_output->data = $dp;
                } else {
                    if(!$action) {
                        if(!is_array($this->data)) {
                            $this->_failApi("The data property is not set or could not be read");
                        }
                        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
                            "models_create",
                            "models_modify"
                        ),"Core")) {
                            $this->_failApi("You are not able to modify properties");
                        }
                        if(!Property::existsById($id)) {
                            $this->_notFound();
                        }

                        $dp = Property::constructFromArray($this->data);
                        $dp->id = $id;
                        $dp->pushThis();
                        $this->_output->data = $dp;
                    } else {
                        $this->_badRequest();
                    }
                }
                break;
        }
    }
}