<?php
namespace Core\Components\Apis;
/**
 * This api is used to get the values of the font awesome icons enum
 *
 * Class FontAwesomeIcons
 */
class FontAwesomeIcons extends \Core\Classes\Commons\Api {
    public function _onDefault() {
        $this->_output->data = \Core\Classes\Enums\FontAwesomeIcons::values();
    }
}