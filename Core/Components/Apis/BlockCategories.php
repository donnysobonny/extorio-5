<?php
namespace Core\Components\Apis;
use Core\Classes\Utilities\Blocks;
use Core\Classes\Utilities\Users;

/**
 * This api is used to get, create and modify block categories
 *
 * Class BlockCategories
 */
class BlockCategories extends \Core\Classes\Commons\Api {
    public function tree() {
        if($this->_httpMethod != "GET") $this->_badRequest();
        $this->_output->data = Blocks::getBlockCategoryTree();
    }

    public $data;

    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "GET" :
                $this->_badRequest();
                break;
            case "POST" :

                $loggedInUser = $this->_getLoggedInUser();
                if(!$loggedInUser) {
                    $this->_accessDenied("You must be logged in to do that");
                }

                if(!$id) {
                    if(!is_array($this->data)) {
                        $this->_failApi("The data property is not set or could not be read");
                    }

                    if(!Users::userHasPrivilege($loggedInUser->id, "block_categories_create","Core")) {
                        $this->_accessDenied("You are not able to create block categories");
                    }

                    $dcat = \Core\Classes\Models\BlockCategory::constructFromArray($this->data);
                    $dcat->id = null;
                    $dcat->pushThis();

                    $this->_output->data = $dcat;
                } else {
                    if(!$action) {
                        if(!is_array($this->data)) {
                            $this->_failApi("The data property is not set or could not be read");
                        }

                        if(!Users::userHasPrivilege($loggedInUser->id,"block_categories_modify","Core")) {
                            $this->_accessDenied("You are not able to modify block categories");
                        }

                        if(!\Core\Classes\Models\BlockCategory::existsById($id)) {
                            $this->_notFound();
                        }

                        $dcat = \Core\Classes\Models\BlockCategory::constructFromArray($this->data);
                        $dcat->id = $id;
                        $dcat->pushThis();

                        $this->_output->data = $dcat;
                    } else {
                        switch($action) {
                            case "delete" :
                                if(!Users::userHasPrivilege($loggedInUser->id,"block_categories_delete","Core")) {
                                    $this->_accessDenied("You are not able to delete block categories");
                                }

                                if(!\Core\Classes\Models\BlockCategory::existsById($id)) {
                                    $this->_notFound();
                                }

                                $cat = \Core\Classes\Models\BlockCategory::findById($id);
                                if($cat) {
                                    $cat->deleteThis();
                                }
                                break;
                            default:
                                $this->_badRequest();
                                break;
                        }
                    }
                }

                break;
        }
    }
}