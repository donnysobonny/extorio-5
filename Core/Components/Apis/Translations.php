<?php
namespace Core\Components\Apis;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Translation;
use Core\Classes\Utilities\Users;

/**
 * This api is used to get, create, modify and delete translations
 *
 * Class Translations
 */
class Translations extends \Core\Classes\Commons\Api {

    public $baseId;
    public $languageId;
    public $msgstr;
    public $extension;

    public function filter() {
        if(!strlen($this->baseId) || $this->baseId <= 0) {
            $this->_failApi("You must supply a baseId");
        }

        $query = Query::n();
        $where = array();
        $where[] = array("baseId" => $this->baseId);
        if(strlen($this->languageId)) {
            $where[] = array("languageId" => $this->languageId);
        }
        if(strlen($this->msgstr)) {
            $where[] = array("msgstr" => array(
                Query::_lk => "%".$this->msgstr."%"
            ));
        }
        if(strlen($this->extension)) {
            $where[] = array("extensionName" => $this->_extensionName);
        }

        $query->where($where);
        $query->order(array("id" => "asc"))->limit(0);

        $this->_output->data = Translation::findAll($query);
    }

    public $data;

    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "GET" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $t = Translation::findById($id);
                        if(!$t) {
                            $this->_notFound();
                        }
                        $this->_output->data = $t;
                    } else {
                        $this->_badRequest();
                    }
                }
                break;
            case "POST" :
                if(!$id) {
                    if(!is_array($this->data)) {
                        $this->_badRequest("The data property is not set or could not be read");
                    }
                    if(!Users::userHasPrivilege($this->_getLoggedInUserId(),"translation_create")) {
                        $this->_accessDenied("You are not able to create translations");
                    }

                    $dt = Translation::constructFromArray($this->data);
                    $dt->id = null;
                    $dt->pushThis();

                    $this->_output->data = $dt;
                } else {
                    if(!$action) {
                        if(!is_array($this->data)) {
                            $this->_badRequest("The data property is not set or could not be read");
                        }
                        if(!Users::userHasPrivilege($this->_getLoggedInUserId(),"translation_modify")) {
                            $this->_accessDenied("You are not able to modify translations");
                        }
                        $t = Translation::findById($id);
                        if(!$t) {
                            $this->_notFound();
                        }
                        $dt = Translation::constructFromArray($this->data);
                        $t->msgstr = $dt->msgstr;
                        $t->msgstr_plural = $dt->msgstr_plural;
                        $t->pushThis();

                        $this->_output->data = $t;
                    } else {
                        switch($action) {
                            case "delete" :
                                if(!Users::userHasPrivilege($this->_getLoggedInUserId(),"translation_delete")) {
                                    $this->_accessDenied("You are not able to delete translations");
                                }
                                $t = Translation::findById($id);
                                if(!$t) {
                                    $this->_notFound();
                                }
                                $t->deleteThis();
                                break;
                            default:
                                $this->_badRequest();
                                break;
                        }
                    }
                }
                break;
        }
    }
}