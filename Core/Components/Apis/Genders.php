<?php
namespace Core\Components\Apis;
/**
 * This api is used to get the values from the genders enum
 *
 * Class Genders
 */
class Genders extends \Core\Classes\Commons\Api {
    public function _onDefault() {
        $this->_output->data = \Core\Classes\Enums\Genders::values();
    }
}