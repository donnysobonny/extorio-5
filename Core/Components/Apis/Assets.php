<?php
namespace Core\Components\Apis;
use Core\Classes\Commons\FileSystem\Directory;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Enums\UserAccessLevels;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Asset;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

/**
 * Api used to create and manage assets
 *
 * Class Assets
 */
class Assets extends \Core\Classes\Commons\Api {

    public $fileName;
    public $dirPath;
    public $contentTypeMain;
    public $contentTypeSub;
    public $extension;

    public function filter() {
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        if(!$loggedInUser) {
            $this->_failApi("You must be logged in to do that");
        }

        $fileWhere = array();
        $dirWhere = array();

        $fileWhere[] = array("isFile" => true);
        $dirWhere[] = array("isFile" => false);

        if(strlen($this->fileName)) {
            $fileWhere[] = array("fileName" => array(
                Query::_lk => "%".$this->fileName."%"
            ));
            $dirWhere[] = array("fileName" => array(
                Query::_lk => "%".$this->fileName."%"
            ));
        }
        $this->dirPath = Strings::startsAndEndsWith($this->dirPath,"/");
        if(strlen($this->dirPath)) {
            $fileWhere[] = array("dirPath" => $this->dirPath);
            $dirWhere[] = array("dirPath" => $this->dirPath);
        }
        if(strlen($this->contentTypeMain)) {
            $fileWhere[] = array("contentTypeMain" => $this->contentTypeMain);
        }
        if(strlen($this->contentTypeSub)) {
            $fileWhere[] = array("contentTypeSub" => $this->contentTypeSub);
        }
        if(strlen($this->extension)) {
            $fileWhere[] = array("extensionName" => $this->extension);
            $dirWhere[] = array("extensionName" => $this->extension);
        }
        $fileQuery = Query::n();
        $fileQuery->where($fileWhere);
        $fileQuery->order(array("fileName" => "asc"));
        $fileQuery->limit(0);

        $dirQuery = Query::n();
        $dirQuery->where($dirWhere);
        $dirQuery->order(array("fileName" => "asc"));
        $dirQuery->limit(0);

        $this->_output->files = Asset::findAll($fileQuery,1);
        $this->_output->folders = Asset::findAll($dirQuery,1);
    }

    public $isFile;

    public function _onDefault($assetId = false, $action = false) {
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        if(!$loggedInUser) {
            $this->_failApi("You must be logged in to do that");
        }

        switch($this->_httpMethod) {
            case "GET" :
                if(!$assetId) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $a = Asset::findById($assetId,1);
                        if(!$a) {
                            $this->_notFound();
                        }
                        $this->_output->data = $a;
                    } else {
                        $this->_badRequest();
                    }
                }
                break;
            case "POST" :
                if(!$assetId) {
                    //creating new asset
                    if(!strlen($this->dirPath)) {
                        $this->_failApi("You must specify a dirPath");
                    }
                    if(!strlen($this->extension)) {
                        $this->_failApi("You must specify the extension");
                    }
                    if(is_null($this->isFile)) {
                        $this->isFile = false;
                    }

                    if(!Users::userHasPrivilege($this->_getLoggedInUser()->id,"assets_create","Core")) {
                        $this->_accessDenied("You are not able to create asset files or folders");
                    }

                    //if creating folder
                    if(!$this->isFile) {
                        if(!strlen($this->fileName)) {
                            $this->_failApi("When creating a folder, you must specify the file name");
                        }

                        //try and create the asset
                        $a = Asset::n();
                        $a->isFile = false;
                        $a->fileName = $this->fileName;
                        $a->dirPath = $this->dirPath;
                        $a->extensionName = $this->extension;
                        $a->pushThis();

                        //if the dir doesn't exist, create it
                        if(!Directory::exsistsFromPath($a->localUrl)) {
                            if(!Directory::createFromPath($a->localUrl)) {
                                $a->deleteThis();
                                $this->_failApi("Could not create directory");
                            }
                        }

                        //create the dirs if need be
                        $dir = Strings::notStartsAndNotEndsWith($a->dirPath,"/");
                        if(strlen($dir)) {
                            $parts = explode("/",$dir);
                            if(count($parts) > 0) {
                                $path = "/";
                                foreach($parts as $part) {
                                    if(!Asset::findOne(
                                        Query::n()
                                            ->where(array(
                                                "isFile" => false,
                                                "extensionName" => $a->extensionName,
                                                "fileName" => $part,
                                                "dirPath" => $path
                                            ))
                                    )) {
                                        $a = Asset::n();
                                        $a->isFile = false;
                                        $a->fileName = $part;
                                        $a->dirPath = $path;
                                        $a->extensionName = $this->extension;
                                        $a->pushThis();
                                    }
                                    $path .= $part."/";
                                }
                            }
                        }

                        $this->_output->data = $a;
                    } else {
                        if(count($this->_fileData) <= 0) {
                            $this->_failApi("No file data found");
                        }
                        if(count($this->_fileData) > 1) {
                            $this->_failApi("You can only upload one file at a time");
                        }

                        $fileData = $this->_fileData[0];

                        if(!strlen($this->fileName)) {
                            $this->fileName = $fileData["name"];
                        }

                        //check for general errors
                        switch ($fileData['error']) {
                            case UPLOAD_ERR_OK:
                                break;
                            case UPLOAD_ERR_NO_FILE:
                                $this->_failApi("No file sent");
                                break;
                            case UPLOAD_ERR_INI_SIZE:
                            case UPLOAD_ERR_FORM_SIZE:
                                $this->_failApi("File size too big");
                                break;
                            default:
                                $this->_failApi("Unknown error");
                        }

                        //config checks
                        $config = $this->_Extorio()->getConfig();
                        if($fileData["size"] > $config["assets"]["max_size"]) {
                            $this->_failApi("File size too big");
                        }
                        if(!in_array($fileData["type"],$config["assets"]["allowed_types"])) {
                            $this->_failApi("File type is not allowed");
                        }

                        //create the asset
                        $parts = explode("/", $fileData["type"]);

                        $a = Asset::n();
                        $a->isFile = true;
                        $a->fileName = $this->fileName;
                        $a->dirPath = $this->dirPath;
                        $a->contentTypeMain = $parts[0];
                        $a->contentTypeSub = $parts[1];
                        $a->extensionName = $this->extension;
                        $a->pushThis();

                        $f = File::constructFromPath($a->localUrl);
                        //create dir if need be
                        if(!Directory::exsistsFromPath($f->dirName())) {
                            if(!Directory::createFromPath($f->dirName())) {
                                $a->deleteThis();
                                $this->_failApi("Could not create directory");
                            }
                        }

                        //try and move the uploaded file
                        if(!move_uploaded_file($fileData["tmp_name"],$a->localUrl)) {
                            $a->deleteThis();
                            $this->_failApi("Could not move uploaded file");
                        }

                        //create the dirs if need be
                        $dir = Strings::notStartsAndNotEndsWith($a->dirPath,"/");
                        if(strlen($dir)) {
                            $parts = explode("/",$dir);
                            if(count($parts) > 0) {
                                $path = "/";
                                foreach($parts as $part) {
                                    if(!Asset::findOne(
                                        Query::n()
                                            ->where(array(
                                                "isFile" => false,
                                                "extensionName" => $a->extensionName,
                                                "fileName" => $part,
                                                "dirPath" => $path
                                            ))
                                    )) {
                                        $a = Asset::n();
                                        $a->isFile = false;
                                        $a->fileName = $part;
                                        $a->dirPath = $path;
                                        $a->extensionName = $this->extension;
                                        $a->pushThis();
                                    }
                                    $path .= $part."/";
                                }
                            }
                        }

                        $this->_output->data = $a;
                    }
                } else {
                    if(!$action) {
                        $this->_failApi("Invalid api request");
                    } else {
                        switch($action) {
                            case "delete" :
                                if(!Users::userHasPrivilege($this->_getLoggedInUser()->id,"assets_delete","Core")) {
                                    $this->_accessDenied("You are not able to delete assets");
                                }

                                $a = Asset::findById($assetId,1);
                                if(!$a) {
                                    $this->_notFound();
                                }

                                if($a->isFile) {
                                    //try to delete the file
                                    if(!unlink($a->localUrl) && Directory::exsistsFromPath($a->localUrl)) {
                                        $this->_failApi("Failed to delete file");
                                    }
                                } else {
                                    //try to delete the file
                                    if(!rmdir($a->localUrl) && File::exsistsFromPath($a->localUrl)) {
                                        $this->_failApi("Failed to delete folder. Make sure it is empty first.");
                                    }
                                }

                                //delete
                                $a->deleteThis();

                                break;
                        }
                    }
                }
                break;
        }
    }
}