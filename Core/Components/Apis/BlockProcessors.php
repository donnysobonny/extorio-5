<?php
namespace Core\Components\Apis;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Block;
use Core\Classes\Models\BlockProcessor;
use Core\Classes\Utilities\Users;

/**
 * This api is used to get, create and modify block processors
 *
 * Class BlockProcessors
 */
class BlockProcessors extends \Core\Classes\Commons\Api {

    public $name;
    public $label;
    public $extension;
    public $categoryId;
    public $orderBy = "name";
    public $orderDirection = "asc";
    public $limit = 0;
    public $skip = 0;

    public function filter() {
        if($this->_httpMethod != "GET") {
            $this->_badRequest();
        }

        $query = Query::n();
        $where = array();

        if(strlen($this->name)) {
            $where[] = array("name" => array(Query::_lk => "%".$this->name."%"));
        }
        if(strlen($this->label)) {
            $where[] = array("label" => $this->label);
        }
        if(strlen($this->extension)) {
            $where[] = array("extensionName" => $this->extension);
        }
        if(strlen($this->categoryId)) {
            $where[] = array("category.id" => intval($this->categoryId));
        }

        $query->where($where);
        $queryCount = clone($query);

        $query->order(array($this->orderBy => $this->orderDirection))->limit($this->limit)->skip($this->skip);

        $this->_output->data = BlockProcessor::findAll($query,2);
        $this->_output->countLimited = BlockProcessor::findCount($query);
        $this->_output->countUnlimited = BlockProcessor::findCount($queryCount);
    }

    public $data;

    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "GET" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $bp = BlockProcessor::findById($id,2);
                        if(!$bp) {
                            $this->_notFound();
                        }
                        $this->_output->data = $bp;
                    } else {
                        $this->_badRequest();
                    }
                }
                break;
            case "POST" :
                if(!$id) {
                    if(!is_array($this->data)) {
                        $this->_failApi("The data property is not set or could not be read");
                    }
                    $loggedInUser = $this->_getLoggedInUser();
                    if(!Users::userHasPrivilege($loggedInUser->id,"block_processors_create","Core")) {
                        $this->_accessDenied("You are not able to create block processors");
                    }

                    $dbp = BlockProcessor::constructFromArray($this->data);
                    $dbp->id = null;
                    $dbp->pushThis();
                    $this->_output->data = $dbp;
                } else {
                    if(!$action) {
                        if(!is_array($this->data)) {
                            $this->_failApi("The data property is not set or could not be read");
                        }
                        $loggedInUser = $this->_getLoggedInUser();
                        if(!Users::userHasPrivilege($loggedInUser->id,"block_processors_modify","Core")) {
                            $this->_accessDenied("You are not able to modify block processors");
                        }
                        if(!BlockProcessor::existsById($id)) {
                            $this->_notFound();
                        }
                        $dbp = BlockProcessor::constructFromArray($this->data);
                        $dbp->id = $id;
                        $dbp->pushThis();
                        $this->_output->data = $dbp;
                    } else {
                        switch($action) {
                            case "delete" :
                                $loggedInUser = $this->_getLoggedInUser();
                                if(!Users::userHasPrivilege($loggedInUser->id,"block_processors_delete","Core")) {
                                    $this->_accessDenied("You are not able to delete block processors");
                                }
                                $bp = BlockProcessor::findById($id,1);
                                if(!$bp) {
                                    $this->_notFound();
                                }
                                $bp->deleteThis();
                                break;
                            default :
                                $this->_badRequest();
                                break;
                        }
                    }
                }
                break;
        }
    }
}