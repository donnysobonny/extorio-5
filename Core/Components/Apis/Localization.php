<?php
namespace Core\Components\Apis;
use Core\Classes\Enums\PHPTypes;

/**
 * Functionality related to localization
 *
 * Class Localization
 */
class Localization extends \Core\Classes\Commons\Api {
    public function auto_translate_auth_token() {
        $config = $this->_getConfig();
        //Initialize the Curl Session.
        $ch = curl_init();
        //Create the request Array.
        //Create an Http Query.//
        $paramArr = http_build_query(array(
            "client_id" => $config["localization"]["microsoft_credentials"]["client_id"],
            "client_secret" => $config["localization"]["microsoft_credentials"]["client_secret"],
            "scope" => "http://api.microsofttranslator.com",
            "grant_type" => "client_credentials"
        ));
        //Set the Curl URL.
        curl_setopt($ch, CURLOPT_URL, "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13");
        //Set HTTP POST Request.
        curl_setopt($ch, CURLOPT_POST, TRUE);
        //Set data to POST in HTTP "POST" Operation.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $paramArr);
        //CURLOPT_RETURNTRANSFER- TRUE to return the transfer as a string of the return value of curl_exec().
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //CURLOPT_SSL_VERIFYPEER- Set FALSE to stop cURL from verifying the peer's certificate.
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //Execute the  cURL session.
        $strResponse = curl_exec($ch);
        //Get the Error Code returned by Curl.
        $curlErrno = curl_errno($ch);
        if($curlErrno){
            $curlError = curl_error($ch);
            $this->_failApi($curlError);
        }
        //Close the Curl Session.
        curl_close($ch);
        //Decode the returned JSON string.
        $objResponse = json_decode($strResponse);
        if ($objResponse->error){
            $this->_failApi($objResponse->error_description);
        }
        $this->_output->data = $objResponse;
    }

    public $text;
    public $texts;

    public function translate() {
        if(gettype($this->text) == PHPTypes::_array) {
            $this->_output->text = tn($this->text[0],$this->text[1],intval($this->text[2]));
        } elseif(gettype($this->text) == PHPTypes::_string) {
            $this->_output->text = t($this->text);
        }

        if(is_array($this->texts)) {
            $this->_output->texts = array();
            for($i = 0; $i < count($this->texts); $i++) {
                $text = $this->texts[$i];
                if(gettype($text) == PHPTypes::_array) {
                    $this->_output->texts[$i] = tn($text[0],$text[1],intval($text[2]));
                } elseif(gettype($text) == PHPTypes::_string) {
                    $this->_output->texts[$i] = t($text);
                }
            }
        }
    }
}