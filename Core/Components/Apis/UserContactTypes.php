<?php
namespace Core\Components\Apis;
use Core\Classes\Enums\UserContactType;

/**
 * 
 *
 * Class UserContactTypes
 */
class UserContactTypes extends \Core\Classes\Commons\Api {
    public function _onDefault() {
        $this->_output->data = UserContactType::values();
    }
}