<?php
namespace Core\Components\Apis;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\UserGroup;

/**
 * This api is used to get, create and modify user groups
 *
 * Class UserGroups
 */
class UserGroups extends \Core\Classes\Commons\Api {

    public $name;
    public $extension;
    public $orderBy = "name";
    public $orderDirection = "asc";
    public $limit = 0;
    public $skip = 0;

    public function filter() {
        if($this->_httpMethod != "GET") {
            $this->_badRequest();
        }

        $query = Query::n();
        $where = array();

        if(strlen($this->name)) {
            $where[] = array("name" => array(
                Query::_lk => "%".$this->name."%"
            ));
        }
        if(strlen($this->extension)) {
            $where[] = array("extensionName" => $this->extension);
        }

        $query->where($where);
        $queryCount = clone($query);

        $query->order(array($this->orderBy => $this->orderDirection))
            ->limit($this->limit)
            ->skip($this->skip);

        $this->_output->data = UserGroup::findAll($query,1);
        $this->_output->countLimited = UserGroup::findCount($query);
        $this->_output->countUnlimited = UserGroup::findCount($queryCount);
    }

    public function _onDefault() {
        $this->_badRequest();
    }
}