<?php
namespace Core\Components\Apis;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Model;
use Core\Classes\Utilities\Users;

/**
 * This api is used to get and modify models
 *
 * Class Models
 */
class Models extends \Core\Classes\Commons\Api {

    public $name;
    public $label;
    public $extension;
    public $isHidden;
    public $orderBy = "name";
    public $orderDirection = "asc";
    public $limit = 0;
    public $skip = 0;

    public function filter() {
        if($this->_httpMethod != "GET") {
            $this->_badRequest();
        }

        $query = Query::n();
        $where = array();

        if(strlen($this->name)) {
            $where[] = array("name" => array(
                Query::_lk => "%".$this->name."%"
            ));
        }
        if(strlen($this->label)) {
            $where[] = array("label" => $this->label);
        }
        if(strlen($this->extension)) {
            $where[] = array("extensionName" => $this->extension);
        }
        if(!is_null($this->isHidden)) {
            $where[] = array("isHidden" => boolval($this->isHidden));
        }

        $query->where($where);
        $queryCount = clone($query);

        $query->order(array($this->orderBy => $this->orderDirection))->limit($this->limit)->skip($this->skip);

        $this->_output->data = Model::findAll($query,2);
        $this->_output->countLimited = Model::findCount($query);
        $this->_output->countUnlimited = Model::findCount($queryCount);
    }

    public $data;

    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "GET" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $m = Model::findById($id,2);
                        if(!$m) {
                            $this->_notFound();
                        }
                        $this->_output->data = $m;
                    } else {
                        $this->_badRequest();
                    }
                }
                break;
            case "POST" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        if(!is_array($this->data)) {
                            $this->_failApi("The data property is not set or could not be read");
                        }
                        if(!Users::userHasPrivilege($this->_getLoggedInUser()->id,"models_modify","Core")) {
                            $this->_accessDenied("You are not able to modify models");
                        }
                        if(!Model::existsById($id)) {
                            $this->_notFound();
                        }

                        $dModel = Model::constructFromArray($this->data);
                        $dModel->id = $id;
                        $dModel->pushThis();
                        $dModel->pullThis(2);

                        $this->_output->data = $dModel;

                    } else {
                        $this->_badRequest();
                    }
                }
                break;
        }
    }
}