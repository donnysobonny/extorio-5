<?php
namespace Core\Components\Apis;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Model;

class Data extends \Core\Classes\Commons\Api {

    public function _onBegin() {
        $this->_failApi("deprecated");
    }

    public $where = array();
    public $order = array();
    public $limit = false;
    public $skip = false;
    public $data = false;

    public $depth = 1;

    public function _onDefault($model = false, $id = false, $action = false) {
        if (!$model) {
            $this->_failApi("No model selected");
        }
        //make sure the model exists
        $m = Model::findOne(
            Query::n()
                ->where(array(
                    '$or' => array(
                        "class" => $model,
                        "label" => $model,
                        "id" => intval($model)
                    )
                ))
        );
        if(!$m) {
            $this->_failApi($model." could not be used to identify a model");
        }
        /** @var Model $modelType */
        $modelType = $m->namespace;

        //valid methods: GET, POST, PUT and DELETE
        switch ($this->_httpMethod) {
            case "GET" :
                //allow the $ params
                if (isset($this->_getParams['$depth'])) {
                    $this->depth = $this->_getParams['$depth'];
                }

                //if id is numeric, we are finding by id
                if (is_numeric($id)) {
                    $id = intval($id);
                    $this->_output->data = $modelType::findById($id,$this->depth);
                } else {

                    //allow the $ params
                    if (isset($this->_getParams['$where'])) {
                        $this->where = $this->_getParams['$where'];
                    }
                    if (isset($this->_getParams['$order'])) {
                        $this->order = $this->_getParams['$order'];
                    }
                    if (isset($this->_getParams['$limit'])) {
                        $this->limit = $this->_getParams['$limit'];
                    }
                    if (isset($this->_getParams['$skip'])) {
                        $this->skip = $this->_getParams['$skip'];
                    }

                    $query = Query::n();
                    $query->where($this->where);
                    $query->order($this->order);
                    $query->limit($this->limit);
                    $query->skip($this->skip);

                    $this->_output->data = $modelType::findAll($query,$this->depth);
                }
                break;
            case "POST" :

                //allow the $ param
                if (isset($this->_allParams['$data'])) {
                    $this->data = $this->_allParams['$data'];
                }

                //allow the _id to be the action
                if (is_numeric($id)) {
                    $this->data["id"] = intval($id);
                }

                $m = $modelType::constructFromArray($this->data);

                //it is possible to use a POST request to delete, by specifying "delete" as the action
                if($action == "delete") {
                    $m->deleteThis();
                } else {
                    if (!$this->data) {
                        $this->_failApi("The data property is not set");
                    } elseif (!is_array($this->data)) {
                        $this->_failApi("Unable to read the data property");
                    }

                    $m = $modelType::constructFromArray($this->data);
                    $m->pushThis();
                    $this->_output->data = $m;
                }

                break;
            case "PUT" :

                //allow the $ param
                if (isset($this->_allParams['$data'])) {
                    $this->data = $this->_allParams['$data'];
                }

                if (!$this->data) {
                    $this->_failApi("The data property is not set");
                } elseif (!is_array($this->data)) {
                    $this->_failApi("Unable to read the data property");
                }

                //allow the _id to be the action
                if (is_numeric($id)) {
                    $this->data["id"] = intval($id);
                }

                $m = $modelType::constructFromArray($this->data);
                $m->pushThis();
                $this->_output->data = $m;

                break;
            case "DELETE" :

                //allow the $ param
                if (isset($this->_allParams['$data'])) {
                    $this->data = $this->_allParams['$data'];
                }

                //allow the _id to be the action
                if (is_numeric($id)) {
                    $this->data["id"] = intval($id);
                }

                //if there is no id
                if(!isset($this->data["id"])) {
                    $this->_failApi("The id of the object must exist either in the url, or as part of the data object");
                }

                if (isset($this->data["id"])) {
                    $m = $modelType::findById(intval($this->data["id"]));
                    if ($m) {
                        $m->deleteThis();
                    }
                }

                break;
        }
    }
}
