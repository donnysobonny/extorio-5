<?php
namespace Core\Components\Apis;
use Core\Classes\Enums\InternalEvents;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\UserActionType;

/**
 * This api is used to get user action types
 *
 * Class UserActionTypes
 */
class UserActionTypes extends \Core\Classes\Commons\Api {
    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "GET" :
                if(!$id) {
                    $this->_output->data = UserActionType::findAll(Query::n()->order("name"));
                } else {
                    if(!$action) {
                        $at = UserActionType::findById($id);
                        if(!$at) {
                            $this->_notFound();
                        }

                        $actions = $this->_Extorio()->getEvent(InternalEvents::_user_actions_detail_filter)->run(array($at->name),1);
                        foreach($actions as $a) {
                            $at->filters = $a;
                        }

                        $this->_output->data = $at;

                    } else {
                        $this->_badRequest();
                    }
                }
                break;
            case "POST" :
                $this->_badRequest();
                break;
        }
    }
}