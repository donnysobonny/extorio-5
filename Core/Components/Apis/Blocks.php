<?php
namespace Core\Components\Apis;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Block;
use Core\Classes\Utilities\Users;

/**
 * This api is used to get, create and modify blocks
 *
 * Class Blocks
 */
class Blocks extends \Core\Classes\Commons\Api {

    public $name;
    public $isHidden;
    public $isPublic;
    public $extension;
    public $categoryId;
    public $subCategoryId;
    public $orderBy = "name";
    public $orderDirection = "asc";
    public $limit = 0;
    public $skip = 0;

    public function filter() {
        if($this->_httpMethod != "GET") $this->_badRequest();

        $loggedInUser = $this->_getLoggedInUser();
        $blocksReadAll = Users::userHasPrivilege($loggedInUser->id,"blocks_read_all","Core");
        $userGroup = Users::getUserGroupIdByUser($loggedInUser->id);

        $query = Query::n();
        $where = array();

        if(strlen($this->name)) {
            $where[Query::_and][] = array("name" => array(
                Query::_lk => "%".$this->name."%"
            ));
        }
        if(!is_null($this->isHidden)) {
            $where[Query::_and][] = array("isHidden" => boolval($this->isHidden));
        }
        if(!is_null($this->isPublic)) {
            $where[Query::_and][] = array("isPublic" => boolval($this->isPublic));
        }
        if(strlen($this->extension)) {
            $where[Query::_and][] = array("extensionName" => $this->extension);
        }
        if(strlen($this->categoryId)) {
            $where[Query::_and][] = array("category.id" => $this->categoryId);
        }
        if(strlen($this->subCategoryId)) {
            $where[Query::_and][] = array("subCategory.id" => $this->subCategoryId);
        }

        if(!$blocksReadAll) {
            $where[Query::_or][] = array("allRead" => true);
            $where[Query::_or][] = array("readGroups.id" => $userGroup);
        }

        $query->where($where);
        $queryCount = clone($query);

        $query->order(array($this->orderBy => $this->orderDirection));
        $query->limit($this->limit);
        $query->skip($this->skip);

        $this->_output->data = Block::findAll($query,2);
        $this->_output->countLimited = Block::findCount($query);
        $this->_output->countUnlimited = Block::findCount($queryCount);
    }

    public $data;

    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "GET" :

                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $loggedInUser = $this->_getLoggedInUser();
                        if(!\Core\Classes\Utilities\Blocks::canUserReadBlock($loggedInUser->id,$id)) {
                            $this->_accessDenied();
                        }

                        $block = Block::findById($id,2);
                        if(!$block) {
                            $this->_notFound();
                        }

                        $this->_output->data = $block;

                    } else {
                        switch($action) {
                            case "can_modify" :
                                $loggedInUser = $this->_getLoggedInUser();
                                $this->_output->data = \Core\Classes\Utilities\Blocks::canUserModifyBlock($loggedInUser->id,$id);
                                break;
                            default:
                                $this->_badRequest();
                                break;
                        }
                    }
                }

                break;
            case "POST" :

                $loggedInUser = $this->_getLoggedInUser();

                if(!$id) {
                    if(!is_array($this->data)) {
                        $this->_failApi("The data property is not set or could not be read");
                    }

                    if(!Users::userHasPrivilege($loggedInUser->id,"blocks_create","Core")) {
                        $this->_accessDenied("You are not able to create blocks");
                    }

                    $dBlock = Block::constructFromArray($this->data);
                    $dBlock->id = null;

                    $dBlock->pushThis();
                    $dBlock->pullThis(2);

                    $this->_output->data = $dBlock;
                } else {
                    if(!$action) {
                        if(!is_array($this->data)) {
                            $this->_failApi("The data property is not set or could not be read");
                        }

                        if(!\Core\Classes\Utilities\Blocks::canUserModifyBlock($loggedInUser->id,$id)) {
                            $this->_accessDenied("You are not able to modify this block");
                        }

                        $dBlock = Block::constructFromArray($this->data);
                        $dBlock->id = intval($id);
                        $dBlock->pushThis();
                        $dBlock->pushThis(2);

                        $this->_output->data = $dBlock;
                    } else {
                        switch($action) {
                            case "delete" :

                                if(!Users::userHasPrivilege($loggedInUser->id,"blocks_delete_all","Core")) {
                                    $this->_accessDenied("You are not able to delete blocks");
                                }

                                $b = Block::findById($id,1);
                                if(!$b) {
                                    $this->_notFound();
                                }

                                $b->deleteThis();

                                break;
                            default:
                                $this->_badRequest();
                                break;
                        }
                    }
                }

                break;
        }
    }
}