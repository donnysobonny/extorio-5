<?php
namespace Core\Components\Apis;
/**
 * This api is used to get the user titles enum values
 *
 * Class Titles
 */
class Titles extends \Core\Classes\Commons\Api {
    public function _onDefault() {
        $this->_output->data = \Core\Classes\Enums\Titles::values();
    }
}