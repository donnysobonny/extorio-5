<?php
namespace Core\Components\Apis;

use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\UserActionFilter;
use Core\Classes\Helpers\UserReportQuery;
use Core\Classes\Models\User;
use Core\Classes\Models\UserReport;
use Core\Classes\Utilities\Localization;

class Users extends \Core\Classes\Commons\Api {

    public $username;
    public $password;

    public function login() {
        $this->_output->session = \Core\Classes\Utilities\Users::login($this->username,$this->password);
    }

    public function logout() {
        \Core\Classes\Utilities\Users::logout();
    }

    public $languageId;

    public function language() {
        if(!strlen($this->languageId) || $this->languageId <= 0) {
            $this->_failApi("You must specify the language id");
        }
        $loggedInUser = $this->_getLoggedInUser();
        if($loggedInUser) {
            $loggedInUser->language = $this->languageId;
            $loggedInUser->pushThis();
        }
        Localization::setCurrentLanguage($this->languageId,true,7);
    }

    public $phrase;
    public $canLogin;
    public $acceptsMarketing;
    public $userGroup;
    public $userGroups;
    public $managedUserGroups;

    public function selectsearch() {
        if(!strlen($this->phrase)) {
            $this->_output->data = array();
        } else {
            $query = Query::n();
            $where = array();
            $where['$or'][] = array("username" => array(
                Query::_lk => "%".$this->phrase."%"
            ));
            $where['$or'][] = array("email" => array(
                Query::_lk => "%".$this->phrase."%"
            ));
            $where['$or'][] = array("firstname" => array(
                Query::_lk => "%".$this->phrase."%"
            ));
            $where['$or'][] = array("lastname" => array(
                Query::_lk => "%".$this->phrase."%"
            ));

            if(!is_null($this->canLogin)) {
                $where['$and'][] = array("canLogin" => boolval($this->canLogin));
            }
            if(!is_null($this->acceptsMarketing)) {
                $where['$and'][] = array("acceptsMarketing" => boolval($this->acceptsMarketing));
            }
            if(strlen($this->userGroup)) {
                $where['$and'][] = array("userGroup.id" => intval($this->userGroup));
            }
            if(is_array($this->userGroups) && !empty($this->userGroups)) {
                $where['$and'][] = array("userGroup.id" => array(
                    Query::_in => $this->userGroups
                ));
            }
            if($this->managedUserGroups) {
                $userGroups = $this->_getLoggedInUserManageGroups();
                if(empty($userGroups)) {
                    $this->_failApi("You must be logged in to do that.");
                } else {
                    $ids = array();
                    foreach ($userGroups as $ug) {
                        $ids[] = $ug->id;
                    }
                    $where['$and'][] = array("userGroup.id" => array(
                        Query::_in => $ids
                    ));
                }
            }

            $query->where($where)->limit($this->limit);

            $this->_output->data = array();
            foreach (User::findAll($query) as $user) {
                $this->_output->data[$user->id] = $user->longname;
            }
        }
    }

    public $query;
    public $limit = 0;
    public $skip = 0;

    public function report($id = false) {
        $this->_cacheOutput(5);

        if($this->_httpMethod != "GET") {
            $this->_badRequest();
        }
        if(!$id) {
            if(!is_array($this->query)) {
                $this->_failApi("The query property is not set or could not be read");
            }
            $this->getReport($this->query);
        } else {
            $ur = UserReport::findById($id);
            if(!$ur) {
                $this->_notFound();
            }
            $this->getReport($ur->query);
        }
    }

    private function getReport($query) {
        $urq = UserReportQuery::n();
        $urq->where($query);
        $clone = clone($urq);

        $urq->limit($this->limit)->skip($this->skip);

        $this->_output->data = array();
        foreach(User::findByUserReportQuery($urq,1) as $user) {
            $clean = array();
            foreach(UserReportQuery::getExposedProperties() as $p) {
                $clean[$p] = $user->$p;
            }
            $this->_output->data[] = $clean;
        }

        $this->_output->countLimited = User::findCountByUserReportQuery($urq);
        $this->_output->countUnlimited = User::findCountByUserReportQuery($clone);
    }

    public function report_download($id = false) {
        $this->_cacheOutput(5);

        if($this->_httpMethod != "GET") {
            $this->_badRequest();
        }
        if(!$id) {
            if(!is_array($this->query)) {
                $this->_failApi("The query property is not set or could not be read");
            }
            $this->getReportDownload($this->query);
        } else {
            $ur = UserReport::findById($id);
            if(!$ur) {
                $this->_notFound();
            }
            $this->getReportDownload($ur->query);
        }
    }

    private function getReportDownload($query) {
        $this->_output->test = "test";
        $urq = UserReportQuery::n();
        $urq->where($query)->limit($this->limit)->skip($this->skip);
        $this->_output->data = "/".\Core\Classes\Utilities\Users::generateCsvFromUserReportQuery($urq);
    }

    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "GET" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $this->_badRequest();
                    } else {
                        $user = User::findById($id,1);
                        if(!$user) {
                            $this->_notFound();
                        }
                        switch($action) {
                            case "public" :
                                $this->_cacheOutput();

                                $this->_output->data = array(
                                    "shortname" => $user->shortname,
                                    "longname" => $user->longname,
                                    "avatar" => $user->avatar,
                                    "bio" => $user->bio
                                );
                                break;
                            case "longname" :
                                $this->_cacheOutput();

                                $this->_output->data = $user->longname;
                                break;
                            case "shortname" :
                                $this->_cacheOutput();

                                $this->_output->data = $user->shortname;
                                break;
                            case "avatar" :
                                $this->_cacheOutput();

                                $this->_output->data = $user->avatar;
                                break;
                            case "bio" :
                                $this->_cacheOutput();

                                $this->_output->data = $user->bio;
                                break;
                        }
                    }
                }
                break;
            case "POST" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $this->_badRequest();
                    } else {
                        $u = User::findById($id,3);
                        if(!$u) {
                            $this->_notFound();
                        }
                        if(!\Core\Classes\Utilities\Users::userManagesUser($this->_getLoggedInUser()->id,$u->id)) {
                            $this->_accessDenied("You do not manage this user");
                        }

                        switch($action) {
                            case "send_account" :
                                \Core\Classes\Utilities\Users::sendAccountEmail($u);
                                break;
                            case "send_authentication":
                                \Core\Classes\Utilities\Users::sendAuthenticationEmail($u);
                                break;
                            case "send_password_reset":
                                \Core\Classes\Utilities\Users::sendPasswordResetEmail($u);
                                break;
                            default:
                                $this->_badRequest();
                                break;
                        }
                    }
                }
                break;
        }
    }
}