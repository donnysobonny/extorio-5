<?php
namespace Core\Components\Apis;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\UserNote;
use Core\Classes\Utilities\Users;

/**
 * This api is used to get, create and modify user notes
 *
 * Class UserNotes
 */
class UserNotes extends \Core\Classes\Commons\Api {

    public $userId;
    public $updatedById;
    public $dateUpdatedFrom;
    public $dateUpdatedTo;
    public $orderBy = "dateUpdated";
    public $orderDirection = "desc";
    public $limit = 0;
    public $skip = 0;

    public function filter() {
        if($this->_httpMethod != "GET") {
            $this->_badRequest();
        }

        if(!strlen($this->userId) || $this->userId <= 0) {
            $this->_failApi("userId is required");
        }

        if(!Users::userManagesUser($this->_getLoggedInUser()->id,$this->userId)) {
            $this->_accessDenied("You are not able to manage this user");
        }
        if(!Users::userHasPrivilege($this->_getLoggedInUser()->id,"user_notes_read","Core")) {
            $this->_accessDenied("You are not able to read notes");
        }

        $query = Query::n();
        $where = array();

        $where[] = array("userId" => intval($this->userId));
        if(strlen($this->updatedById)) {
            $where[] = array("updatedById" => intval($this->userId));
        }
        if(strlen($this->dateUpdatedFrom)) {
            $where[] = array("dateUpdated" => array(
                Query::_gte => date('Y-m-d H:i:s', strtotime($this->dateUpdatedFrom))
            ));
        }
        if(strlen($this->dateUpdatedTo)) {
            $where[] = array("dateUpdated" => array(
                Query::_lte => date('Y-m-d H:i:s', strtotime($this->dateUpdatedTo))
            ));
        }

        $query->where($where);
        $queryCount = clone($query);

        $query->order(array($this->orderBy => $this->orderDirection))->limit($this->limit)->skip($this->skip);

        $this->_output->data = UserNote::findAll($query);
        $this->_output->countLimited = UserNote::findCount($query);
        $this->_output->countUnlimited = UserNote::findCount($queryCount);
    }

    public $data;

    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "GET" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        if(!Users::userHasPrivilege($this->_getLoggedInUser()->id,"user_notes_read","Core")) {
                            $this->_accessDenied("You are not able to read user notes");
                        }

                        $note = UserNote::findById($id);
                        if(!$note) {
                            $this->_notFound();
                        }

                        if(!Users::userManagesUser($this->_getLoggedInUser()->id,$note->userId)) {
                            $this->_accessDenied("You do not manage the user that this note belongs to, and therefore you cannot read this note");
                        }

                        $this->_output->data = $note;
                    } else {
                        $this->_badRequest();
                    }
                }
                break;
            case "POST" :
                if(!$id) {
                    if(!is_array($this->data)) {
                        $this->_failApi("The data api could not be read or is not set");
                    }
                    if(!Users::userHasPrivilege($this->_getLoggedInUser()->id,"user_notes_create","Core")) {
                        $this->_accessDenied("You are not able to create user notes");
                    }

                    $dNote = UserNote::constructFromArray($this->data);
                    if(!strlen($dNote->userId) || $dNote->userId <= 0) {
                        $this->_failApi("You must specify the userId of the note");
                    }
                    if(!Users::userManagesUser($this->_getLoggedInUser()->id,$dNote->userId)) {
                        $this->_accessDenied("You do not manage this user, and therefore cannot create notes for this user");
                    }

                    $dNote->id = null;
                    $dNote->pushThis();

                    $this->_output->data = $dNote;
                } else {
                    if(!$action) {
                        if(!is_array($this->data)) {
                            $this->_failApi("The data api could not be read or is not set");
                        }
                        if(!Users::userHasPrivilege($this->_getLoggedInUser()->id,"user_notes_modify","Core")) {
                            $this->_accessDenied("You are not able to modify user notes");
                        }

                        $note = UserNote::findById($id);
                        if(!$note) {
                            $this->_notFound();
                        }

                        if(!Users::userManagesUser($this->_getLoggedInUser()->id,$note->userId)) {
                            $this->_accessDenied("You do not manage the user that this note belongs to, and therefore you cannot modify this note");
                        }

                        $dNote = UserNote::constructFromArray($this->data);
                        $note->body = $dNote->body;

                        $note->pushThis();

                        $this->_output->data = $note;
                    } else {
                        switch($action) {
                            case "delete":
                                if(!Users::userHasPrivilege($this->_getLoggedInUser()->id,"user_notes_delete","Core")) {
                                    $this->_accessDenied("You are not able to delete user notes");
                                }

                                $note = UserNote::findById($id);
                                if(!$note) {
                                    $this->_notFound();
                                }

                                if(!Users::userManagesUser($this->_getLoggedInUser()->id,$note->userId)) {
                                    $this->_accessDenied("You do not manage the user that this note belongs to, and therefore you cannot delete this note");
                                }

                                $note->deleteThis();

                                break;
                            default:
                                $this->_badRequest();
                                break;
                        }
                    }
                }
                break;
        }
    }
}