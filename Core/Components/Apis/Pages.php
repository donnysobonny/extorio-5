<?php
namespace Core\Components\Apis;
use Core\Classes\Models\Page;

/**
 * This api is used to modify a page
 *
 * Class Pages
 */
class Pages extends \Core\Classes\Commons\Api {

    public $data;

    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "POST" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        if(!is_array($this->data)) {
                            $this->_failApi("The data property is not set or could not be read");
                        }
                        if(!Page::existsById($id)) {
                            $this->_notFound();
                        }
                        if(!\Core\Classes\Utilities\Pages::canUserModifyPage($this->_getLoggedInUserId(),$id)) {
                            $this->_accessDenied("You are not able to modify this page");
                        }
                        $dp = Page::constructFromArray($this->data);
                        $dp->id = $id;
                        $dp->pushThis();
                        $this->_output->data = $dp;
                    } else {
                        $this->_badRequest();
                    }
                }
                break;
            default :
                $this->_badRequest();
                break;
        }
    }
}