<?php
namespace Core\Components\Apis;
/**
 * This api is used to get the values of the Countries enum
 *
 * Class Countries
 */
class Countries extends \Core\Classes\Commons\Api {
    public function _onDefault() {
        $this->_output->data = \Core\Classes\Enums\Countries::values();
    }
}