<?php
namespace Core\Components\BlockProcessors;
/**
 * Displays a basic footer, based on the application's company name
 *
 * Class BasicFooter
 */
class BasicFooter extends \Core\Classes\Commons\BlockProcessor {

    public $align = "left";

    protected function _onEdit() {
        ?>
        <div class="form-inline">
            <div class="form-group">
                <label for="align">Align</label>
                <select class="form-control" id="align" name="align">
                    <option <?php if($this->align == "left") echo 'selected="selected"'; ?> value="left">left</option>
                    <option <?php if($this->align == "center") echo 'selected="selected"'; ?> value="center">center</option>
                    <option <?php if($this->align == "right") echo 'selected="selected"'; ?> value="right">right</option>
                </select>
            </div>
        </div>
        <?php
    }

    protected function _onView() {
        $config = $this->_Extorio()->getConfig();
        ?>
        <hr />
        <div style="text-align: <?=$this->align?>; width: 100%;">
            <strong>&copy; <?=$config["application"]["company"]?> <?=date("Y")?></strong><br />
            <span class="help-block">All rights reserved</span>
        </div>
        <?php
    }
}