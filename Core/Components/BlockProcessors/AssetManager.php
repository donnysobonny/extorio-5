<?php
namespace Core\Components\BlockProcessors;
use Core\Classes\Enums\FontAwesomeIcons;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

/**
 * Manage assets
 *
 * Class AssetManager
 */
class AssetManager extends \Core\Classes\Commons\BlockProcessor {

    public $extension = "Application";
    public $dirpath = "/";

    public $allowfiltering = true;
    public $allowextensions = true;

    public $displayfolders = true;
    public $displaytypes = "null";

    public $selectable = true;

    public $columns = 4;

    protected function _onView() {
        $this->dirpath = Strings::startsAndEndsWith($this->dirpath,"/");
        $extensions = $this->_Extorio()->getExtensions();
        $names = array();
        foreach($extensions as $extension) {
            $names[] = $extension->_extensionName;
        }
        $outer = uniqid()."_outer";
        $fileOuter = uniqid()."_fileouter";
        $breadCrumbs = uniqid()."_breadcrumbs";
        ?>

        <div id="<?=$outer?>" class="row asset_manager">
            <div class="col-xs-12">
                <table class="gutter_margin_bottom_half" cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
                    <tbody>
                        <tr>
                            <td style="width: 100%; vertical-align: top;">
                                <ol style="margin-bottom: 0;" id="<?=$breadCrumbs?>" class="breadcrumb">

                                </ol>
                            </td>
                            <td style="<?php
                            if(!Users::userHasPrivilege($this->_getLoggedInUser()->id,"assets_create","Core")) echo 'display: none; ';
                            ?>vertical-align: top; padding-left: 30px;">
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <span class="fa fa-bars"></span> Options
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="javascript:;" class="asset_manager_new_folder"><span class="fa fa-plus"></span> New Folder</a></li>
                                        <li><a href="javascript:;" class="asset_manager_new_file"><span class="fa fa-upload"></span> Upload</a></li>
                                    </ul>
                                </div>
                                <input style="display: none;" type="file" class="asset_manager_new_file_input">
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div id="<?=$fileOuter?>">

                </div>
                <br />
                <div class="row">
                    <div class="col-xs-12">
                        <?php
                        $tabid = uniqid();
                        ?>
                        <div class="panel-group" id="<?=$tabid?>_accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="<?=$tabid?>_heading">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#<?=$tabid?>_accordion" href="#<?=$tabid?>_collapse" aria-expanded="true" aria-controls="<?=$tabid?>_collapse">
                                            Filters
                                        </a>
                                    </h4>
                                </div>
                                <div id="<?=$tabid?>_collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="<?=$tabid?>_heading">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="input-group">
                                                    <input type="text" class="form-control input-sm asset_manager_search" placeholder="Search..." aria-describedby="basic-addon2">
                                                    <span class="input-group-addon" id="basic-addon2"><span class="fa fa-search"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div style="<?php
                                            if(!$this->allowfiltering) echo 'display: none;';
                                            ?>" class="col-xs-6">
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="asset_manager_display_folders" <?php
                                                        if($this->displayfolders) echo 'checked="checked"';
                                                        ?> type="checkbox"> Display folders
                                                    </label>
                                                </div>
                                                <div class="form-group">
                                                    <label>Display type</label>
                                                    <select class="form-control asset_manager_display_types">
                                                        <option <?php
                                                        if($this->displaytypes == "null") echo 'selected="selected"';
                                                        ?> value="null">display all types</option>
                                                        <option <?php
                                                        if($this->displaytypes == "image") echo 'selected="selected"';
                                                        ?> value="image">display images</option>
                                                        <option <?php
                                                        if($this->displaytypes == "video") echo 'selected="selected"';
                                                        ?> value="video">display videos</option>
                                                        <option <?php
                                                        if($this->displaytypes == "audio") echo 'selected="selected"';
                                                        ?> value="audio">display audio files</option>
                                                        <option <?php
                                                        if($this->displaytypes == "text") echo 'selected="selected"';
                                                        ?> value="text">display text files</option>
                                                        <option <?php
                                                        if($this->displaytypes == "application") echo 'selected="selected"';
                                                        ?> value="application">display other files</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div style="<?php
                                            if(!$this->allowextensions) echo 'display: none;';
                                            ?>" class="col-xs-6">
                                                <div class="form-group">
                                                    <label>Select extension</label>
                                                    <select class="form-control asset_manager_extension">
                                                        <?php
                                                        foreach($extensions as $ext) {
                                                            ?><option <?php
                                                            if($ext->_extensionName == $this->extension) {
                                                                echo 'selected="selected"';
                                                            }
                                                            ?> value="<?=$ext->_extensionName?>"><?=$ext->_extensionName?></option><?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function() {
                var assetsDelete = <?php
                    if(Users::userHasPrivilege($this->_getLoggedInUser()->id,"assets_delete","Core")) echo "true"; else echo "false";
                    ?>;

                var domain = "<?=Server::getDomain()?>";
                var outer = "#<?=$outer?>";
                var breadcrumbs = "#<?=$breadCrumbs?>";
                var fileOuter = "#<?=$fileOuter?>";
                var extension = "<?=$this->extension?>";
                var dirPath = "<?=$this->dirpath?>";
                var columns = <?=intval($this->columns)?>;

                var displayFolders = $(outer+" .asset_manager_display_folders").prop("checked");
                var displayType = $(outer+" .asset_manager_display_types").val();

                var selectable = <?php if($this->selectable) echo "true"; else echo "false"; ?>;

                var files = [];

                var updateBreadCrumbs = function() {
                    var partsRaw = dirPath.split("/");
                    var partsClean = [];
                    for(var i = 0; i < partsRaw.length; i++) {
                        if(partsRaw[i].length > 0) {
                            partsClean.push(partsRaw[i]);
                        }
                    }
                    var cont = $(breadcrumbs);
                    var path = "/";
                    cont.find('li').each(function() {
                        $(this).remove();
                    });
                    //add the home crumb
                    cont.append('<li><a data-path="'+path+'" href="javascript:;">Root</a></li>');
                    for(var j = 0; j < partsClean.length; j++) {
                        path += partsClean[j] + "/";
                        cont.append('<li><a data-path="'+path+'" href="javascript:;">'+partsClean[j]+'</a></li>');
                    }
                    cont.find('a').each(function() {
                        var pathi = $(this).attr('data-path');
                        $(this).on("click", function() {
                            dirPath = pathi;
                            loadFiles();
                            updateBreadCrumbs();
                        });
                    });
                };

                var loadFiles = function() {
                    $(outer).extorio_showLoader();
                    $.extorio_api({
                        endpoint: "/assets/filter",
                        type: "GET",
                        data: {
                            dirPath: dirPath,
                            extension: extension,
                            contentTypeMain: displayType,
                            fileName: $(outer+" .asset_manager_search").val()
                        },
                        oncomplete: function() {
                            $(outer).extorio_hideLoader();
                        },
                        onsuccess: function(response) {
                            var nextRow = 0;
                            var k = 0;
                            var cont = $(fileOuter);
                            var row = false;
                            var item;
                            cont.html('');

                            if(displayFolders && (response.folders.length + response.files.length) == 0) {
                                cont.html('No files or folders to display...');
                            } else if(!displayFolders && response.files.length == 0) {
                                cont.html('No files to display...');
                            } else {
                                if(displayFolders) {
                                    while(k <= response.folders.length) {
                                        if(k == response.folders.length) {
                                            if(row) {
                                                cont.append(row);
                                            }
                                            break;
                                        }

                                        var folder = response.folders[k];
                                        if(k == nextRow) {
                                            if(row) {
                                                cont.append(row);
                                            }
                                            row = $('<div class="row"></div>');
                                            nextRow += columns;
                                        }

                                        item = $('<div class="col-xs-'+Math.round(12/columns)+'">' +
                                            '   <button data-path="'+folder.dirPath+folder.fileName+'" style="padding: 10px;" class="btn btn-block asset_manager_selector">' +
                                            '       <div class="asset_manager_selector_image" style="background: url(\'/Core/Assets/images/foldericon.png\') repeat scroll center center / cover  transparent; height: 100%; width: 100%;"></div>' +
                                            '   </button>' +
                                            '   <div style="font-size: 10px; padding: 5px; text-align: center; margin-bottom: 5px;">' +
                                            '       <strong>' + folder.fileName + '</strong>&nbsp&nbsp' +
                                            '       <button title="delete folder" data-id="'+folder.id+'" class="btn btn-xs btn-danger asset_manager_folder_delete"><span class="fa fa-trash"></span></button>' +
                                            '   </div>' +
                                            '</div>');
                                        item.find('.asset_manager_selector').on("click", function() {
                                            dirPath = $(this).attr('data-path');
                                            loadFiles();
                                            updateBreadCrumbs();
                                        });
                                        if(assetsDelete) {
                                            item.find('.asset_manager_folder_delete').on("click", function() {
                                                var id = $(this).attr('data-id');
                                                $.extorio_modal({
                                                    title: "Delete folder...",
                                                    content: "Are you sure you want to delete this folder?",
                                                    size: "modal-sm",
                                                    closetext: "cancel",
                                                    continuetext: "delete",
                                                    oncontinuebutton: function() {
                                                        $(outer).extorio_showLoader();
                                                        $.extorio_api({
                                                            endpoint: "/assets/" + id + "/delete",
                                                            type: "POST",
                                                            oncomplete: function() {
                                                                $(outer).extorio_hideLoader();
                                                            },
                                                            onsuccess: function(response) {
                                                                $.extorio_messageSuccess("Folder deleted");
                                                                loadFiles();
                                                            }
                                                        });
                                                    }
                                                });
                                            });
                                        } else {
                                            item.find('.asset_manager_folder_delete').addClass("disabled").attr("disabled","disabled");
                                        }

                                        row.append(item);

                                        k++;
                                    }
                                }

                                var l = 0;
                                while(l <= response.files.length) {
                                    if(l == response.files.length) {
                                        if(row) {
                                            cont.append(row);
                                        }
                                        break;
                                    }

                                    var file = response.files[l];
                                    if(k == nextRow) {
                                        if(row) {
                                            cont.append(row);
                                        }
                                        row = $('<div class="row"></div>');
                                        nextRow += columns;
                                    }

                                    var image = "/Core/Assets/images/fileicon.png";
                                    if(file.contentTypeMain == "image") {
                                        image = file.webUrl;
                                    }

                                    item = $('<div class="col-xs-'+Math.round(12/columns)+'">' +
                                        '   <button data-url="'+file.webUrl+'" style="padding: 5px;" class="btn btn-block asset_manager_selector">' +
                                        '       <div class="asset_manager_selector_image" style="background: url(\''+image+'\') repeat scroll center center / cover  transparent; height: 100%; width: 100%;"></div>' +
                                        '   </button>' +
                                        '   <div style="font-size: 10px; padding: 5px; text-align: center; margin-bottom: 5px;">' +
                                        '       <strong>' + file.fileName + '</strong>&nbsp;&nbsp;' +
                                        '       <button title="file info" data-id="'+file.id+'" class="btn btn-xs btn-primary asset_manager_folder_info"><span class="fa fa-<?=FontAwesomeIcons::_infoCircle?>"></span></button>' +
                                        '       <button title="delete file" data-id="'+file.id+'" class="btn btn-xs btn-danger asset_manager_file_delete"><span class="fa fa-trash"></span></button>' +
                                        '   </div>' +
                                        '</div>');
                                    item.find('.asset_manager_selector').each(function() {
                                        if(!selectable) {
                                            $(this).addClass("disabled");
                                            $(this).attr("disabled","disabled");
                                        } else {
                                            var url = $(this).attr('data-url');
                                            $(this).on("click", function() {
                                                $(outer).trigger("on_file_selected",url);
                                            });
                                        }
                                    });
                                    item.find('.asset_manager_folder_info').each(function() {
                                        var id = $(this).attr('data-id');
                                        $(this).on("click", function() {
                                            $.extorio_dialog({
                                                title: "File info",
                                                content: "<br /><br /><br /><br /><br /><br />",
                                                onopen: function() {
                                                    var body = this.find('.modal-body');
                                                    body.extorio_showLoader();
                                                    $.extorio_api({
                                                        endpoint: "/assets/" +id,
                                                        type: "GET",
                                                        oncomplete: function() {
                                                            body.extorio_hideLoader();
                                                        },
                                                        onsuccess: function(response) {
                                                            body.html('' +
                                                                '<div class="row">' +
                                                                '   <div class="col-xs-6">' +
                                                                '       <div class="form-group">' +
                                                                '           <label>Name</label><br />' +
                                                                '           <code>'+response.data.fileName+'</code>' +
                                                                '       </div>' +
                                                                '   </div>' +
                                                                '   <div class="col-xs-6">' +
                                                                '       <div class="form-group">' +
                                                                '           <label>Type</label><br />' +
                                                                '           <code>'+response.data.contentTypeMain+'/'+response.data.contentTypeSub+'</code>' +
                                                                '       </div>' +
                                                                '   </div>' +
                                                                '</div>' +
                                                                '<div class="form-group">' +
                                                                '   <label>Local url</label><br />' +
                                                                '   <input type="text" class="form-control" value="'+response.data.webUrl+'" />' +
                                                                '</div>' +
                                                                '<div class="form-group">' +
                                                                '   <label>External url</label><br />' +
                                                                '   <input type="text" class="form-control" value="'+domain+response.data.webUrl+'" />' +
                                                                '</div>');
                                                        }
                                                    });
                                                }
                                            });
                                        });
                                    });
                                    if(assetsDelete) {
                                        item.find('.asset_manager_file_delete').on("click", function() {
                                            var id = $(this).attr('data-id');
                                            $.extorio_modal({
                                                title: "Delete file...",
                                                content: "Are you sure you want to delete this file?",
                                                size: "modal-sm",
                                                closetext: "cancel",
                                                continuetext: "delete",
                                                oncontinuebutton: function() {
                                                    $(outer).extorio_showLoader();
                                                    $.extorio_api({
                                                        endpoint: "/assets/" + id + "/delete",
                                                        type: "POST",
                                                        oncomplete: function() {
                                                            $(outer).extorio_hideLoader();
                                                        },
                                                        onsuccess: function(response) {
                                                            $.extorio_messageSuccess("File deleted");
                                                            loadFiles();
                                                        }
                                                    });
                                                }
                                            });
                                        });
                                    } else {
                                        item.find('.asset_manager_file_delete').addClass("disabled").attr("disabled","disabled");
                                    }

                                    row.append(item);
                                    k++;
                                    l++;
                                }
                                cont.find('.asset_manager_selector, .asset_manager_selector_image').each(function() {
                                    $(this).height($(this).width());
                                });
                            }

                            $(outer).trigger("on_files_loaded");
                        }
                    });
                };

                $(outer+" .asset_manager_search").extorio_searchinput(function () {
                    loadFiles();
                })

                $(outer+" .asset_manager_display_types").on("change", function() {
                    displayType = $(this).val();
                    loadFiles();
                });

                $(outer+" .asset_manager_display_folders").on("change", function() {
                    displayFolders = $(this).prop("checked");
                    loadFiles();
                });

                $(outer+" .asset_manager_extension").on("change", function() {
                    extension = $(this).val();
                    dirPath = "/";
                    loadFiles();
                    updateBreadCrumbs();
                });

                $(outer+" .asset_manager_new_file").on("click", function() {
                    if($(outer+" .asset_manager_new_file").attr("disabled") == undefined) {
                        $(outer+" .asset_manager_new_file_input").trigger("click");
                    }
                });

                $(outer+" .asset_manager_new_file_input").on("change",function(e) {
                    files = e.target.files;
                    if(files.length > 0) {
                        var button = $(outer+" .asset_manager_new_file");
                        button.attr("disabled", "disabled");
                        button.html("Uploading: 0%");

                        $.extorio_modal({
                            title: "Set file name...",
                            content: '<input value="'+files[0].name+'" class="form-control asset_manager_file_name" placeholder="Enter the file name" />',
                            closetext: "cancel",
                            continuetext: "upload",
                            size: "modal-sm",
                            onclosebutton: function() {
                                button.removeAttr("disabled");
                                button.html('<span class="fa fa-upload"></span> Upload');
                            },
                            oncontinuebutton: function() {
                                var name = this.find('.asset_manager_file_name').val();
                                var data = new FormData();
                                $.each(files, function(k,v) {
                                    data.append(k,v);
                                });
                                $.extorio_api({
                                    endpoint: "/assets/?extension="+extension+"&dirPath="+dirPath+"&isFile=true&fileName="+name,
                                    type: "POST",
                                    data: data,
                                    cache: false,
                                    dataType: 'json',
                                    processData: false,
                                    contentType: false,
                                    oncomplete: function() {
                                        button.removeAttr("disabled");
                                        button.html('<span class="fa fa-upload"></span> Upload');
                                    },
                                    onsuccess: function(response) {
                                        $.extorio_messageSuccess("File uploaded");
                                        loadFiles();
                                    }
                                });
                            }
                        });
                    }
                });

                $(outer+" .asset_manager_new_folder").on("click", function() {
                    $.extorio_modal({
                        title: "New folder...",
                        content: '<input type="text" class="form-control asset_manager_new_folder_name" placeholder="Enter the folder name..." />',
                        size: "modal-sm",
                        closetext: "cancel",
                        continuetext: "create",
                        oncontinuebutton: function() {
                            var name = this.find('.asset_manager_new_folder_name').val();
                            $(outer).extorio_showLoader();
                            $.extorio_api({
                                endpoint: "/assets/",
                                type: "POST",
                                data: {
                                    isFile: false,
                                    dirPath: dirPath,
                                    fileName: name,
                                    extension: extension
                                },
                                oncomplete: function() {
                                    $(outer).extorio_hideLoader();
                                },
                                onsuccess: function(response) {
                                    $.extorio_messageSuccess("Folder created");
                                    loadFiles();
                                }
                            });
                        }
                    });
                });

                loadFiles();
                updateBreadCrumbs();
            });
        </script>
        <?php
    }

    protected function _onEdit() {
        ?>
        <div class="form-group">
            <label for="extension">Extension</label>
            <?php
            $extensions = $this->_Extorio()->getExtensions();
            ?>
            <select class="form-control" id="extension" name="extension">
                <?php
                foreach($extensions as $extension) {
                    ?><option <?php
                    if($this->extension == $extension->_extensionName) echo 'selected="selected"';
                    ?> value="<?=$extension->_extensionName?>"><?=$extension->_extensionName?></option><?php
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="extension">Directory</label>
            <input type="text" class="form-control" id="dirpath" name="dirpath" value="<?=$this->dirpath?>" />
        </div>
        <hr />
        <div class="checkbox">
            <label>
                <input <?php
                if($this->allowbreadcrumbs) echo 'checked="checked"';
                ?> id="allowbreadcrumbs" name="allowbreadcrumbs" type="checkbox"> Display bread crumbs
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->allowuploads) echo 'checked="checked"';
                ?> id="allowuploads" name="allowuploads" type="checkbox"> Allow uploads
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->allowsearching) echo 'checked="checked"';
                ?> id="allowsearching" name="allowsearching" type="checkbox"> Allow searching
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->allowfiltering) echo 'checked="checked"';
                ?> id="allowfiltering" name="allowfiltering" type="checkbox"> Allow filtering
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->allowextensions) echo 'checked="checked"';
                ?> id="allowextensions" name="allowextensions" type="checkbox"> Allow extensions
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->displayfolders) echo 'checked="checked"';
                ?> id="displayfolders" name="displayfolders" type="checkbox"> Display folders
            </label>
        </div>
        <div class="form-group">
            <label for="extension">Display type</label>
            <select class="form-control" id="displaytypes" name="displaytypes">
                <option <?php
                if($this->displaytypes == "null") echo 'selected="selected"';
                ?> value="null">display all types</option>
                <option <?php
                if($this->displaytypes == "image") echo 'selected="selected"';
                ?> value="image">display images</option>
                <option <?php
                if($this->displaytypes == "video") echo 'selected="selected"';
                ?> value="video">display videos</option>
                <option <?php
                if($this->displaytypes == "audio") echo 'selected="selected"';
                ?> value="audio">display audio files</option>
                <option <?php
                if($this->displaytypes == "text") echo 'selected="selected"';
                ?> value="text">display text files</option>
                <option <?php
                if($this->displaytypes == "application") echo 'selected="selected"';
                ?> value="application">display other files</option>
            </select>
        </div>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->selectable) echo 'checked="checked"';
                ?> id="selectable" name="selectable" type="checkbox"> Files are selectable
            </label>
        </div>
        <?php
    }
}