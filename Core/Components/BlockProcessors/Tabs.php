<?php
namespace Core\Components\BlockProcessors;
class Tabs extends \Core\Classes\Commons\BlockProcessor {
    protected function _onView() {
        if(isset($this->config["title"])) {
            $uid = uniqid();
            ?>
            <div role="tabpanel">
                <ul class="nav nav-tabs" role="tablist">
                    <?php
                    $n = 0;
                    foreach($this->config["title"] as $i => $title) {
                        ?>
                    <li role="presentation" class="<?php if($n == 0) echo "active"; ?>"><a href="#<?=$uid?>_<?=$i?>" aria-controls="<?=$uid?>_<?=$i?>" role="tab" data-toggle="tab"><?=$title?></a></li>
                        <?php
                        $n++;
                    }
                    ?>
                </ul>

                <div class="tab-content">
                    <?php
                    $n = 0;
                    foreach($this->config["content"] as $i => $content) {
                        ?>
                    <div role="tabpanel" class="tab-pane fade <?php if($n == 0) echo "in active"; ?>" id="<?=$uid?>_<?=$i?>"><?=$content?></div>
                        <?php
                        $n++;
                    }
                    ?>
                </div>
            </div>
            <?php
        }
    }

    protected function _onEdit() {
        ?>
        <div id="tab_forms">
            <?php
            //if no tabs configured
            if(!isset($this->config["title"])) {
                ?>
            <div class="tab_form well well-sm">
                <div class="form-group">
                    <input type="text" class="form-control" id="title" name="title[]" placeholder="Enter a title">
                </div>
                <div class="form-group">
                    <textarea class="froala_editor" name="content[]" id="content" placeholder="Enter some content"></textarea>
                </div>
            </div>
            <?php
            } else {
                foreach($this->config["title"] as $i => $title) {
                    ?>
            <div class="tab_form well well-sm">
                <button class="btn btn-xs btn-danger button_clear_tab" style="float: right; clear: both;" type="button"><span class="fa fa-remove"></span></button>
                <p>&nbsp;</p>
                <div class="form-group">
                    <input value="<?=$title?>" type="text" class="form-control" id="title" name="title[]" placeholder="Enter a title">
                </div>
                <div class="form-group">
                    <textarea class="froala_editor" name="content[]" id="content" placeholder="Enter some content"><?=$this->config["content"][$i]?></textarea>
                </div>
            </div>
                    <?php
                }
            }
            ?>
        </div>
        <button type="button" class="btn btn-sm btn-block btn-primary button_add_tab"><span class="fa fa-plus"></span> add tab</button>
        <style>
            .tab_form {
                cursor: move;
            }

            .tab_form .form-group:last-child {
                margin-bottom: 0;
            }
        </style>
        <script>
            $(function() {
                var initiate = function() {
                    $( "#tab_forms" ).sortable({
                        cancel: ".form-group"
                    });

                    $('.tab_form').each(function() {
                        var tab = $(this);
                        tab.find('.button_clear_tab').on("click", function() {
                            tab.remove();
                        });
                    });

                    $('#tab_forms .froala_editor').extorio_editable();
                }

                $('.button_add_tab').on("click", function() {
                    $('#tab_forms').append('' +
                    '<div class="tab_form well well-sm">' +
                    '   <div class="form-group">' +
                    '       <input type="text" class="form-control" id="title" name="title[]" placeholder="Enter a title">' +
                    '   </div>' +
                    '   <div class="form-group">' +
                    '       <textarea class="froala_editor" name="content[]" id="content" placeholder="Enter some content"></textarea>' +
                    '   </div>' +
                    '</div>');

                    initiate();
                });

                initiate();
            });
        </script>
        <?php
    }
}