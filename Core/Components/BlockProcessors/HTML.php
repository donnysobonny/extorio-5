<?php
namespace Core\Components\BlockProcessors;
/**
 * Create html content using the html editor
 *
 * Class HTML
 */
class HTML extends \Core\Classes\Commons\BlockProcessor {
    public $content = "";

    protected function _onView() {
        echo $this->content;
    }

    protected function _onEdit() {
        ?>
        <textarea id="content" name="content"><?=$this->content?></textarea>
        <script>
            $('#content').extorio_editable({
                <?php
                if($this->blockId > 0 && !$this->inline) {
                ?>
                autosave: true,
                autosaveInverval: 100000,
                onAutosave: function(val) {
                    //put the config
                    $.ajax({
                        url: "/extorio/blocks/<?=$this->blockId?>/save",
                        type: "POST",
                        data: {
                            config: {
                                content: val
                            }
                        },
                        error: function(a,b,c) {
                            console.log(c);
                        },
                        success: function(data) {
                            $.extorio_messageInfo("Content auto saved");
                        }
                    });
                }
                <?php
                }
                ?>
            });
        </script>
    <?php
    }
}