<?php
namespace Core\Components\BlockProcessors;
/**
 * This displays a basic header, including the site name and description
 *
 * Class BasicHeader
 */
class BasicHeader extends \Core\Classes\Commons\BlockProcessor {
    protected function _onView() {
        $config = $this->_Extorio()->getConfig();
        ?>
        <div class="page-header">
            <h1><?=$config["application"]["name"]?> <small><?=$config["application"]["description"]?></small></h1>
        </div>
        <?php
    }
}