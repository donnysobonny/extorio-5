<?php
namespace Core\Components\BlockProcessors;

/**
 * A basic processor for displaying an image
 *
 * Class Image
 */
class Image extends \Core\Classes\Commons\BlockProcessor {

    public $image = "";
    public $alt = "loading...";
    public $style = "none";
    public $align = "left";
    public $fullwidth = true;
    public $maxwidth = 0;
    public $linkable = false;
    public $link = "";

    protected function _onView() {
        if (strlen($this->image)) {
            ?>
            <div style="<?php
            switch($this->align) {
                case "left" :
                    echo "text-align:left;";
                    break;
                case "middle" :
                    echo "text-align:center;";
                    break;
                case "right" :
                    echo "text-align:right;";
                    break;
            }
            ?>">
                <?php
                if($this->linkable) {
                    ?><a href="<?=$this->link?>"><?php
                }
                ?>
                <img style="<?php
                if ($this->fullwidth) {
                    echo "width:100%;";
                }
                $this->maxwidth = intval($this->maxwidth);
                if ($this->maxwidth > 0) {
                    echo "max-width:" . $this->maxwidth . "px;";
                } else {
                    echo "max-width: 100%;";
                }
                ?>" class="<?php
                switch ($this->style) {
                    case "thumbnail" :
                        echo "img-thumbnail";
                        break;
                    case "rounded" :
                        echo "img-rounded";
                        break;
                    case "circle" :
                        echo "img-circle";
                        break;
                }
                ?>" src="<?= $this->image ?>" alt="<?= $this->alt ?>"/>
                <?php
                if($this->linkable) {
                    ?></a><?php
                }
                ?>
            </div>
            <?php
        }
    }

    protected function _onEdit() {
        ?>
        <div class="form-group">
            <label for="exampleInputEmail1">Image</label>
            <div class="input-group">
                <input id="image" name="image" type="text" class="form-control" placeholder="" value="<?=$this->image?>">
                <span class="input-group-btn">
                    <button class="btn btn-primary image_select_button" type="button">Select an image...</button>
                </span>
            </div>
            <!-- /input-group -->
        </div>
        <div class="form-group">
            <label for="alt">Alt text</label>
            <input type="text" class="form-control" id="alt" name="alt" placeholder="Alt text" value="<?=$this->alt?>">
        </div>
        <div class="form-group">
            <label for="style">Style</label>
            <select id="style" name="style" class="form-control">
                <option <?php
                if($this->style == "none") echo 'selected="selected"';
                ?> value="none">none</option>
                <option <?php
                if($this->style == "thumbnail") echo 'selected="selected"';
                ?> value="thumbnail">thumbnail</option>
                <option <?php
                if($this->style == "rounded") echo 'selected="selected"';
                ?> value="rounded">rounded</option>
                <option <?php
                if($this->style == "circle") echo 'selected="selected"';
                ?> value="circle">circle</option>
            </select>
        </div>
        <div class="form-group">
            <label for="align">Align</label>
            <select id="align" name="align" class="form-control">
                <option <?php
                if($this->align == "left") echo 'selected="selected"';
                ?> value="left">left</option>
                <option <?php
                if($this->align == "middle") echo 'selected="selected"';
                ?> value="middle">middle</option>
                <option <?php
                if($this->align == "right") echo 'selected="selected"';
                ?> value="right">right</option>
            </select>
        </div>
        <div class="checkbox">
            <label>
                <input id="fullwidth" name="fullwidth" <?php
                if($this->fullwidth) echo 'checked="checked"';
                ?> type="checkbox"> Full width
            </label>
        </div>
        <div class="form-group">
            <label for="maxwidth">Max width</label>
            <input type="number" class="form-control" id="maxwidth" name="maxwidth" placeholder="Maximum width" value="<?=$this->maxwidth?>">
        </div>
        <div class="checkbox">
            <label>
                <input id="linkable" name="linkable" <?php
                if($this->linkable) echo 'checked="checked"';
                ?> type="checkbox"> Image is linkable
            </label>
        </div>
        <div style="<?php
        if(!$this->linkable) echo 'display: none;';
        ?>" id="linkable_input" class="form-group">
            <label for="maxwidth">Image link</label>
            <input type="text" class="form-control" id="link" name="link" placeholder="Enter the image link" value="<?=$this->link?>">
        </div>
        <script>
            $(function() {
                $('#linkable').on("change", function() {
                    if($(this).prop("checked")) {
                        $('#linkable_input').show();
                    } else {
                        $('#linkable_input').hide();
                    }
                });

                $('.image_select_button').on("click", function() {
                    $.extorio_dialog_assetManager({
                        title: "Select an image...",
                        displaytypes: "image",
                        allowfiltering: false,
                        selectable: true,
                        size: "",
                        onfileselected: function(url) {
                            $('#image').val(url);
                        }
                    });
                });
            });
        </script>
        <?php
    }
}