<?php
namespace Core\Components\BlockProcessors;
use Core\Classes\Enums\FontAwesomeIcons;
use Core\Classes\Enums\InternalEvents;
use Core\Classes\Helpers\NavMenuItem;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;

/**
 * Displays nav menu items based on installed exensions
 *
 * Class DefaultNavMenu
 */
class DefaultNavMenu extends \Core\Classes\Commons\BlockProcessor {
    public $displayhome = false;
    public $type = "tabs";
    public $style = "";

    protected function _onView() {
        $url = Server::getRequestURL();
        $url = Strings::startsAndEndsWith($url,"/");
        $actions = $this->_Extorio()->getEvent(InternalEvents::_default_nav_menu_items)->run();
        /** @var NavMenuItem[] $items */
        $items = array();
        if($this->displayhome) {
            $db = $this->_Extorio()->getDbInstanceDefault();
            $sql = 'SELECT name, address FROM core_classes_models_page WHERE "default" = true LIMIT 1';
            $row = $db->query($sql)->fetchRow();
            if($row) {
                $items[] = NavMenuItem::item($row[0],$row[1],"",FontAwesomeIcons::_home,false);
            }
        }
        foreach($actions as $action) {
            if(is_array($action)) {
                foreach($action as $i) {
                    if(get_class($i) == 'Core\Classes\Helpers\NavMenuItem') {
                        $items[] = $i;
                    }
                }
            } elseif(is_object($action)) {
                if(get_class($action) == 'Core\Classes\Helpers\NavMenuItem') {
                    $items[] = $action;
                }
            }
        }
        ?>
        <ul class="nav nav-<?=$this->type?><?php
        if($this->style == "justified") echo ' nav-justified';
        if($this->style == "stacked") echo ' nav-stacked';
        ?>">
            <?php
            foreach($items as $item) {
                echo $item->__toHTML($url);
            }
            ?>
        </ul><br />
        <?php
    }

    protected function _onEdit() {
        ?>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->displayhome) echo 'checked="checked"';
                ?> id="displayhome" name="displayhome" type="checkbox"> Display an item for the home page
            </label>
        </div>
        <div class="form-group">
            <label for="type">Menu Type</label>
            <select class="form-control" id="type" name="type">
                <option <?php
                if($this->type == "tabs") echo 'selected="selected"';
                ?> value="tabs">tabs</option>
                <option <?php
                if($this->type == "pills") echo 'selected="selected"';
                ?> value="pills">pills</option>
            </select>
        </div>
        <div class="form-group">
            <label for="style">Menu Style</label>
            <select class="form-control" id="style" name="style">
                <option <?php
                if($this->style == "") echo 'selected="selected"';
                ?> value="">none</option>
                <option <?php
                if($this->style == "justified") echo 'selected="selected"';
                ?> value="justified">justified</option>
                <option <?php
                if($this->style == "stacked") echo 'selected="selected"';
                ?> value="stacked">stacked</option>
            </select>
        </div>
        <?php
    }
}