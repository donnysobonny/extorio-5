<?php
namespace Core\Components\BlockProcessors;
/**
 * This creates a horizontal space using an empty paragraph. This is useful for separating rows.
 *
 * Class HorizontalSpace
 */
class HorizontalSpace extends \Core\Classes\Commons\BlockProcessor {
    protected function _onView() {
        ?><p>&nbsp;</p><?php
    }
}