<?php
namespace Core\Components\BlockProcessors;
use Core\Classes\Enums\PHPTypes;
use Core\Classes\Helpers\AdminMenuLeft_Item;
use Core\Classes\Helpers\AdminMenuLeft_Menu;
use Core\Classes\Utilities\Server;

class ExtorioAdminMenuLeft extends \Core\Classes\Commons\BlockProcessor {
    protected function _onView() {
        $this->_Extorio()->includeIncluder("js_cookie");
        $url = Server::getRequestURL();
        $uid = uniqid();
        //run the event to fetch the menus
        $me = $this->_Extorio()->getEvent("extorio_admin_menu_left_menus");
        $menus = $me->run();
        //fetch the items for each menu
        $ie = $this->_Extorio()->getEvent("extorio_admin_menu_left_items");
        ?><div id="<?=$uid?>"><?php
        foreach($menus as $menu) {
            if(gettype($menu) == PHPTypes::_array) {
                $menu = AdminMenuLeft_Menu::constructFromArray($menu);
            }
            if(gettype($menu) == PHPTypes::_object) {
                $itemsHtml = "";
                $foundActive = false;
                ob_start();
                //fetch the items
                $results = $ie->run(array($menu->name));
                foreach($results as $items) {
                    foreach($items as $item) {
                        //convert arrays to the helper object
                        if(gettype($item) == PHPTypes::_array) {
                            $item = AdminMenuLeft_Item::constructFromArray($item);
                        }
                        if(gettype($item) == PHPTypes::_object) {
                            ?>
                            <a <?php if($item->new_window) echo 'target="_blank"'; ?> href="<?=$item->url?>" style="font-size: 12px; padding: 3px 7px;" class="list-group-item <?php
                            if(strlen($item->active_url)) {
                                if(strpos($url,$item->active_url) !== false) {
                                    $foundActive = true;
                                    echo "active";
                                }
                            }
                            ?>"><span class="fa fa-<?=$item->icon?>"></span>&nbsp;&nbsp;<?=$item->label?></a>
                            <?php
                        }
                    }
                }
                $itemsHtml = ob_get_contents();
                ob_end_clean();
                ?>
                <div style="margin-bottom: 0px;" class="panel panel-<?php
                if($foundActive) echo "primary"; else echo "info";
                ?>">
                    <div
                        style="cursor: pointer; padding: 7px 10px;"
                        role="button" data-toggle="collapse"
                        href="#<?=$uid?>_collapse_<?=$menu->name?>"
                        aria-expanded="false"
                        aria-controls="<?=$uid?>_collapse_<?=$menu->name?>"
                        class="panel-heading"
                    >
                        <h3 style="font-size: 14px;" class="panel-title">
                            <span class="fa fa-<?=$menu->icon?>"></span>
                            <?=$menu->label?>
                            <span id="<?=$uid?>_icon_<?=$menu->name?>" style="float: right;" class="fa fa-chevron-<?php
                            if($foundActive) echo "up"; else echo "down";
                            ?>"></span>
                        </h3>
                    </div>
                    <div data-menuname="<?=$menu->name?>" id="<?=$uid?>_collapse_<?=$menu->name?>" class="panel-collapse collapse<?php
                    if($foundActive) echo ' in';
                    ?>">
                        <div style="padding: 1px;" class="panel-body">
                            <div style="margin-bottom: 0;" class="list-group">
                                <?=$itemsHtml?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>
        </div>
        <br />
        <script>
            var uid = '<?=$uid?>';
            var items = Cookies.get('extorio_admin_menu_left_active');
            if(items == undefined) {
                items = [];
            } else {
                items = JSON.parse(items);
            }
            for(var i = 0; i < items.length; i++) {
                $('#' + uid + "_icon_" + items[i]).removeClass('fa-chevron-down').addClass('fa-chevron-up');
                $('#' + uid + "_collapse_" + items[i]).addClass("in");
            }

            $(function() {
                $('#' + uid + ' .panel-collapse').on('hide.bs.collapse', function () {
                    items = Cookies.get('extorio_admin_menu_left_active');
                    if(items == undefined) {
                        items = [];
                    } else {
                        items = JSON.parse(items);
                    }
                    var menuname = $(this).attr('data-menuname');
                    $('#' + uid + "_icon_" + menuname).removeClass('fa-chevron-up').addClass('fa-chevron-down');
                    var index = items.indexOf(menuname);
                    if(index > -1) {
                        items.splice(index,1);
                        Cookies.set('extorio_admin_menu_left_active', JSON.stringify(items));
                    }
                }).on('show.bs.collapse', function () {
                    items = Cookies.get('extorio_admin_menu_left_active');
                    if(items == undefined) {
                        items = [];
                    } else {
                        items = JSON.parse(items);
                    }
                    var menuname = $(this).attr('data-menuname');
                    $('#' + uid + "_icon_" + menuname).removeClass('fa-chevron-down').addClass('fa-chevron-up');
                    var index = items.indexOf(menuname);
                    if(index <= -1) {
                        items.push(menuname);
                        Cookies.set('extorio_admin_menu_left_active', JSON.stringify(items));
                    }
                });
            })
        </script>
        <?php
    }
}