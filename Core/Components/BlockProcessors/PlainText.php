<?php
namespace Core\Components\BlockProcessors;
/**
 * Create content using a basic text-box. You can use html and markdown syntax with this processor.
 *
 * Class PlainText
 */
class PlainText extends \Core\Classes\Commons\BlockProcessor {
    public $content = "";

    protected function _onView() {
        echo nl2br($this->content);
    }

    protected function _onEdit() {
        ?>
        <textarea rows="10" class="form-control" id="content" name="content"><?=$this->content?></textarea>
    <?php
    }
}