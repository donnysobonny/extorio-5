<?php
namespace Core\Components\BlockProcessors;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Page;

/**
 * Adds breadcrumbs to the page/template
 *
 * Class Breadcrumbs
 */
class Breadcrumbs extends \Core\Classes\Commons\BlockProcessor {

    public $display_default;
    public $trail_to_default;

    protected function _onView() {
        $breadCrumbs = $this->_Extorio()->getTargetBreadCrumbs();

        $defaultPage = Page::findOne(
            Query::n()
                ->where(array("default" => true)),1
        );
        $onDefault = false;
        if($defaultPage) {
            if($this->_Extorio()->getTargetPage()) {
                $onDefault = $defaultPage->id == $this->_Extorio()->getTargetPage()->id;
            }
        }

        $display = true;
        if(($onDefault && !$this->display_default) || (empty($breadCrumbs) && !$this->trail_to_default)) {
            $display = false;
        }

        if($display) {
            ?>
            <ol class="breadcrumb">
                <?php
                if($this->trail_to_default && $defaultPage) {
                    ?>
                    <li class="<?php if(empty($breadCrumbs)) echo "active"; ?>"><a href="<?=$defaultPage->address?>"><span class="fa fa-home"></span> <?=$defaultPage->name?></a></li>
                    <?php
                }
                foreach($breadCrumbs as $breadCrumb) {
                    ?>
                    <li <?php if($breadCrumb->active) echo 'class="active"'; ?>>
                        <?php
                        if(!$breadCrumb->active && !$breadCrumb->disabled) {
                            ?>
                            <a style="" title="<?=$breadCrumb->title?>" href="<?=$breadCrumb->url?>"><?php
                                if(strlen($breadCrumb->icon)) {
                                    echo '<span class="fa fa-'.$breadCrumb->icon.'"></span> ';
                                }
                                ?><?=$breadCrumb->label?></a>
                        <?php
                        } else {
                            ?>
                            <span <?php
                            if($breadCrumb->disabled) echo 'style="cursor: not-allowed;"';
                            ?> title="<?=$breadCrumb->title?>">
                                <?php
                                if(strlen($breadCrumb->icon)) {
                                    echo '<span class="fa fa-'.$breadCrumb->icon.'"></span> ';
                                }
                                echo $breadCrumb->label;
                                ?>
                            </span>
                            <?php
                        }
                        ?>

                    </li>
                <?php
                }
                ?>
            </ol>
            <?php
        }
    }

    protected function _onEdit() {
        ?>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->trail_to_default) echo 'checked="checked"';
                ?> id="trail_to_default" name="trail_to_default" type="checkbox"> Trail back to default page
                <p class="help-block">Enabling this will mean that all breadcrumbs trail back to your currently set default page</p>
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->display_default) echo 'checked="checked"';
                ?> id="display_default" name="display_default" type="checkbox"> Display on default page
                <p class="help-block">Disabling this will mean that breadcrumbs wont appear on the default page</p>
            </label>
        </div>
        <?php
    }
}