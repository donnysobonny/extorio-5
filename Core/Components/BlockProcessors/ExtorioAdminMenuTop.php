<?php
namespace Core\Components\BlockProcessors;
use Core\Classes\Enums\PHPTypes;
use Core\Classes\Helpers\AdminMenuTop_Item;
use Core\Classes\Helpers\AdminMenuTop_Menu;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;

/**
 * Displays the top menu
 *
 * Class ExtorioAdminMenuTop
 */
class ExtorioAdminMenuTop extends \Core\Classes\Commons\BlockProcessor {
    protected function _onView() {
        $templateEdit = $this->_Extorio()->getDispatchSetting("dispatch_template_edit_mode");
        $pageEdit = $this->_Extorio()->getDispatchSetting("dispatch_page_edit_mode");
        $url = Strings::startsAndEndsWith(Server::getRequestURL(), "/");
        ?>
        <nav style="margin-bottom: 0px;" class="navbar navbar-inverse navbar-static-top">
            <div class="container-fluid">
                <ul class="nav navbar-nav">
                    <?php
                    $adminPage = $this->_Extorio()->onExtorioAdminPage();
                    ?>
                    <li class="<?php if ($adminPage)
                        echo "active"; ?>"><a class="" href="/extorio-admin"><span class="fa fa-home"></span>
                            Extorio</a></li>
                    <li class="<?php if (!$adminPage)
                        echo "active"; ?>"><a class="" href="/"><span class="fa fa-home"></span> Website</a>
                    </li>
                </ul>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="navbar-form navbar-right">
                        <a href="<?= Server::getRequestURL() ?><?php
                        if (!$templateEdit)
                            echo "?dispatch_template_edit_mode=true";
                        ?>" class="btn btn-<?php
                        if ($templateEdit)
                            echo "danger"; else echo "warning";
                        ?>">Edit template</a>
                        <a href="<?= Server::getRequestURL() ?><?php
                        if (!$pageEdit)
                            echo "?dispatch_page_edit_mode=true";
                        ?>" class="btn btn-<?php
                        if ($pageEdit)
                            echo "danger"; else echo "warning";
                        ?>">Edit page</a>
                        <?php
                        if ($templateEdit || $pageEdit) {
                            ?>
                            <button class="btn btn-success extorio_admin_menu_top_save"><span
                                    class="fa fa-save"></span> Save changes
                            </button>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    $me = $this->_Extorio()->getEvent("extorio_admin_menu_top_menus");
                    $ie = $this->_Extorio()->getEvent("extorio_admin_menu_top_items");
                    if ($me && $ie) {
                        //fetch the menus
                        $menus = $me->run();
                        foreach ($menus as $menu) {
                            //convert arrays to the helper object
                            if (gettype($menu) == PHPTypes::_array) {
                                $menu = AdminMenuTop_Menu::constructFromArray($menu);
                            }
                            if (gettype($menu) == PHPTypes::_object) {
                                //if the menu has items, it is a dropdown
                                $results = $ie->run(array($menu->name));
                                if (!empty($results)) {
                                    ?>
                                    <ul class="nav navbar-nav navbar-<?= $menu->align ?>">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                               role="button" aria-expanded="false"><span
                                                    class="<?= $menu->icon_class ?>"></span> <?= $menu->label ?>
                                                <span class="caret"></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <?php
                                                foreach ($results as $items) {
                                                    foreach ($items as $item) {
                                                        //convert arrays to the helper class
                                                        if (gettype($item) == PHPTypes::_array) {
                                                            if (!empty($item)) {
                                                                $item = AdminMenuTop_Item::constructFromArray($item);
                                                            }
                                                        }
                                                        if (gettype($item) == PHPTypes::_object) {
                                                            ?>
                                                            <li class="<?php if (strpos($url, $item->active_url) !== false)
                                                                echo "active"; ?>">
                                                                <a <?php if ($item->new_window)
                                                                    echo 'target="_blank"'; ?>
                                                                    href="<?= $item->url ?>"><span
                                                                        class="<?= $item->icon_class ?>"></span> <?= $item->label ?>
                                                                </a></li>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <li class="divider"></li>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        </li>
                                    </ul>
                                    <?php
                                }
                            }
                        }
                    }
                    ?>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <?php
        if ($templateEdit) {
            ?>
            <script>
                $(function () {
                    $('.extorio_admin_menu_top_save').on("click", function () {
                        $.extorio_showFullPageLoader();
                        $.extorio_api({
                            endpoint: "/templates/<?=$this->_Extorio()->getTargetTemplate()->id?>",
                            type: "POST",
                            data: {
                                data: {
                                    layout: $('#layout_wrapper').layoutsystem__toObject()
                                }
                            },
                            oncomplete: function() {
                                $.extorio_hideFullPageLoader();
                            },
                            onsuccess: function(d) {
                                $.extorio_messageSuccess("Template layout saved");
                            }
                        });
                    });
                });
            </script>
            <?php
        } elseif ($pageEdit) {
            ?>
            <script>
                $(function () {
                    $('.extorio_admin_menu_top_save').on("click", function () {
                        $.extorio_showFullPageLoader();
                        $.extorio_api({
                            endpoint: "/pages/<?=$this->_Extorio()->getTargetPage()->id?>",
                            type: "POST",
                            data: {
                                data: {
                                    layout: $('#layout_page_content > .layout_cell_content_outer > .layout_cell_content_inner').layoutsystem__toObject()
                                }
                            },
                            oncomplete: function() {
                                $.extorio_hideFullPageLoader();
                            },
                            onsuccess: function(d) {
                                $.extorio_messageSuccess("Page layout saved");
                            }
                        });
                    });
                });
            </script>
            <?php
        }
    }
}