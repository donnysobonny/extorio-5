<?php
namespace Core\Components\BlockProcessors;
class Collapse extends \Core\Classes\Commons\BlockProcessor {

    public $title = array();
    public $content = array();

    protected function _onView() {
        if(!empty($this->title)) {
            $uid = uniqid();
            ?>
            <div class="panel-group" id="<?=$uid?>" role="tablist" aria-multiselectable="true">
                <?php
                $n = 0;
                foreach($this->title as $i => $title) {
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="<?=$uid?>_head_<?=$n?>">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#<?=$uid?>" href="#<?=$uid?>_collapse_<?=$n?>" aria-expanded="true" aria-controls="<?=$uid?>_collapse_<?=$n?>">
                                    <?=$title?>
                                </a>
                            </h4>
                        </div>
                        <div id="<?=$uid?>_collapse_<?=$n?>" class="panel-collapse collapse fade <?php if($n == 0) echo "in"; ?>" role="tabpanel" aria-labelledby="<?=$uid?>_head_<?=$n?>">
                            <div class="panel-body">
                                <?=$this->content[$i]?>
                            </div>
                        </div>
                    </div>
                    <?php
                    $n++;
                }
                ?>
            </div>
        <?php
        }
    }

    protected function _onEdit() {
        ?>
        <div id="collapse_forms">
            <?php
            //if no tabs configured
            if(empty($this->title)) {
                ?>
                <div class="collapse_form well well-sm">
                    <button class="btn btn-xs btn-danger button_clear_collapse" style="float: right; clear: both;" type="button"><span class="fa fa-remove"></span></button>
                    <p>&nbsp;</p>
                    <div class="form-group">
                        <input type="text" class="form-control" id="title" name="title[]" placeholder="Enter a title">
                    </div>
                    <div class="form-group">
                        <textarea class="froala_editor" name="content[]" id="content" placeholder="Enter some content"></textarea>
                    </div>
                </div>
            <?php
            } else {
                foreach($this->title as $i => $title) {
                    ?>
                    <div class="collapse_form well well-sm">
                        <button class="btn btn-xs btn-danger button_clear_collapse" style="float: right; clear: both;" type="button"><span class="fa fa-remove"></span></button>
                        <p>&nbsp;</p>
                        <div class="form-group">
                            <input value="<?=$title?>" type="text" class="form-control" id="title" name="title[]" placeholder="Enter a title">
                        </div>
                        <div class="form-group">
                            <textarea class="froala_editor" name="content[]" id="content" placeholder="Enter some content"><?=$this->content[$i]?></textarea>
                        </div>
                    </div>
                <?php
                }
            }
            ?>
        </div>
        <button type="button" class="btn btn-sm btn-block btn-primary button_add_collapse"><span class="fa fa-plus"></span> add collapse</button>
        <style>
            .collapse_form {
                cursor: move;
            }

            .collapse_form .form-group:last-child {
                margin-bottom: 0;
            }
        </style>
        <script>
            $(function() {
                $( "#collapse_forms" ).sortable({
                    cancel: ".form-group"
                });

                $('.collapse_form').each(function() {
                    var collapse = $(this);
                    collapse.find('.button_clear_collapse').on("click", function() {
                        collapse.remove();
                    });
                    collapse.find('.froala_editor').extorio_editable();
                });

                $('.button_add_collapse').on("click", function() {
                    var collapse = $('' +
                        '<div class="collapse_form well well-sm">' +
                        '   <button class="btn btn-xs btn-danger button_clear_collapse" style="float: right; clear: both;" type="button"><span class="fa fa-remove"></span></button>' +
                        '   <p>&nbsp;</p>' +
                        '   <div class="form-group">' +
                        '       <input type="text" class="form-control" id="title" name="title[]" placeholder="Enter a title">' +
                        '   </div>' +
                        '   <div class="form-group">' +
                        '       <textarea class="froala_editor" name="content[]" id="content" placeholder="Enter some content"></textarea>' +
                        '   </div>' +
                        '</div>');
                    collapse.find('.button_clear_collapse').on("click", function() {
                        collapse.remove();
                    });
                    collapse.find('.froala_editor').extorio_editable();
                    $('#collapse_forms').append(collapse);
                });
            });
        </script>
    <?php
    }
}