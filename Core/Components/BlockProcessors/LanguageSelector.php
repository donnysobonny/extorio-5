<?php
namespace Core\Components\BlockProcessors;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Language;
use Core\Classes\Utilities\Localization;

/**
 * Displays a list of enabled languages to change how the website is translated
 *
 * Class LanguageSelector
 */
class LanguageSelector extends \Core\Classes\Commons\BlockProcessor {

    public $size = "";
    public $context = "btn-info";
    public $block = false;
    public $menualign = "left";
    public $textalign = "left";
    public $navmode = false;

    protected function _onView() {
        $uid = uniqid();
        $langs = Language::findAll(Query::n()->where(array("enabled" => true))->order("name"));
        $lang = Localization::getCurrentLanguage();
        if($this->navmode) {
            ?>
            <ul class="language_selector nav navbar-nav navbar-<?=$this->menualign?>">
                <li style="text-align: <?=$this->menualign?>;" class="dropdown notranslate">
                    <a
                        style="text-align: <?=$this->textalign?>; overflow: hidden;"
                        href="javascript:;"
                        class="dropdown-toggle"
                        data-toggle="dropdown"
                        role="button"
                        aria-haspopup="true"
                        aria-expanded="false">
                        <span style='background: rgba(0, 0, 0, 0) url("<?=$lang->image?>") no-repeat scroll center center / 100% auto'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;<?=$lang->name?>
                        <span class="caret"></span>
                    </a>
                    <ul style="overflow: auto; max-height: 300px; text-align: <?=$this->textalign?>;" class="dropdown-menu <?php
                    if($this->menualign == "right") echo 'dropdown-menu-right';
                    ?>">
                        <?php
                        if(count($langs) > 5) {
                            ?>
                            <li style="text-align: <?=$this->textalign?>;"><a data-id="1" class="language_selector_select" href="javascript:;"><span style='background: rgba(0, 0, 0, 0) url("/Core/Assets/images/flags/United%20Kingdom(Great%20Britain).png") no-repeat scroll center center / 100% auto'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;English</a></li>
                            <li role="separator" class="divider"></li>
                            <?php
                        }
                        foreach($langs as $l) {
                            ?><li style="text-align: <?=$this->textalign?>;"><a data-id="<?=$l->id?>" class="language_selector_select" href="javascript:;"><span style='background: rgba(0, 0, 0, 0) url("<?=$l->image?>") no-repeat scroll center center / 100% auto'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;<?=$l->name?></a></li><?php
                        }
                        ?>
                    </ul>
                </li>
            </ul>
            <?php
        } else {
            ?>
            <div style="text-align: <?=$this->menualign?>;" class="language_selector">
                <div style="text-align: <?=$this->menualign?>;" class="dropdown notranslate">
                    <button
                        style="text-align: <?=$this->textalign?>; overflow: hidden;"
                        class="btn <?=$this->context?> <?=$this->size?> <?php
                        if($this->block) echo 'btn-block';
                        ?> dropdown-toggle"
                        type="button"
                        id="<?=$uid?>"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="true">
                        <span style='background: rgba(0, 0, 0, 0) url("<?=$lang->image?>") no-repeat scroll center center / 100% auto'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;<?=$lang->name?>
                        <span class="caret"></span>
                    </button>
                    <ul style="overflow: auto; max-height: 300px; text-align: <?=$this->textalign?>;" class="dropdown-menu <?php
                    if($this->menualign == "right") echo 'dropdown-menu-right';
                    ?>" aria-labelledby="<?=$uid?>">
                        <?php
                        if(count($langs) > 5) {
                            ?>
                            <li style="text-align: <?=$this->textalign?>;"><a data-id="1" class="language_selector_select" href="javascript:;"><span style='background: rgba(0, 0, 0, 0) url("/Core/Assets/images/flags/United%20Kingdom(Great%20Britain).png") no-repeat scroll center center / 100% auto'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;English</a></li>
                            <li role="separator" class="divider"></li>
                            <?php
                        }
                        foreach($langs as $l) {
                            ?><li style="text-align: <?=$this->textalign?>;"><a data-id="<?=$l->id?>" class="language_selector_select" href="javascript:;"><span style='background: rgba(0, 0, 0, 0) url("<?=$l->image?>") no-repeat scroll center center / 100% auto'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;<?=$l->name?></a></li><?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <?php
        }
        ?>
        <script>
            $(function() {
                $('.language_selector_select').on("click", function() {
                    var id = $(this).attr('data-id');
                    $.extorio_showFullPageLoader();
                    $.extorio_api({
                        endpoint: "/users/language",
                        type: "POST",
                        data:  {
                            languageId: id
                        },
                        onsuccess: function(resp) {
                            window.location.reload();
                        }
                    });
                });
            });
        </script>
        <?php
    }

    protected function _onEdit() {
        ?>
        <div class="form-group">
            <label for="size">Size</label>
            <select id="size" name="size" class="form-control">
                <option value="">normal</option>
                <option <?php
                if($this->size == "btn-xs") echo 'selected="selected"';
                ?> value="btn-xs">extra small</option>
                <option <?php
                if($this->size == "btn-sm") echo 'selected="selected"';
                ?> value="btn-sm">small</option>
                <option <?php
                if($this->size == "btn-lg") echo 'selected="selected"';
                ?> value="btn-lg">large</option>
            </select>
        </div>
        <div class="form-group">
            <label for="context">Context</label>
            <select id="context" name="context" class="form-control">
                <option <?php
                if($this->context == "btn-default") echo 'selected="selected"';
                ?> value="btn-default">default</option>
                <option <?php
                if($this->context == "btn-primary") echo 'selected="selected"';
                ?> value="btn-primary">primary</option>
                <option <?php
                if($this->context == "btn-info") echo 'selected="selected"';
                ?> value="btn-info">info</option>
                <option <?php
                if($this->context == "btn-success") echo 'selected="selected"';
                ?> value="btn-success">success</option>
                <option <?php
                if($this->context == "btn-warning") echo 'selected="selected"';
                ?> value="btn-warning">warning</option>
                <option <?php
                if($this->context == "btn-danger") echo 'selected="selected"';
                ?> value="btn-danger">danger</option>
            </select>
        </div>
        <div class="form-group">
            <label for="menualign">Menu align</label>
            <select id="menualign" name="menualign" class="form-control">
                <option <?php
                if($this->menualign == "left") echo 'selected="selected"';
                ?> value="left">left</option>
                <option <?php
                if($this->menualign == "center") echo 'selected="selected"';
                ?> value="center">middle</option>
                <option <?php
                if($this->menualign == "right") echo 'selected="selected"';
                ?> value="right">right</option>
            </select>
        </div>
        <div class="form-group">
            <label for="textalign">Text align</label>
            <select id="textalign" name="textalign" class="form-control">
                <option <?php
                if($this->textalign == "left") echo 'selected="selected"';
                ?> value="left">left</option>
                <option <?php
                if($this->textalign == "center") echo 'selected="selected"';
                ?> value="center">middle</option>
                <option <?php
                if($this->textalign == "right") echo 'selected="selected"';
                ?> value="right">right</option>
            </select>
        </div>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->block) echo 'checked="checked"';
                ?> name="block" id="block" type="checkbox"> Display as block
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->navmode) echo 'checked="checked"';
                ?> name="navmode" id="navmode" type="checkbox"> Navbar mode (when being displayed within a navbar
            </label>
        </div>
        <?php
    }
}