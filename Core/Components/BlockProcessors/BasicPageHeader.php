<?php
namespace Core\Components\BlockProcessors;
/**
 * Displays a basic page header, including the page's name and description
 *
 * Class BasicPageHeader
 */
class BasicPageHeader extends \Core\Classes\Commons\BlockProcessor {
    protected function _onView() {
        $page = $this->_Extorio()->getTargetPage();
        if($page) {
            ?>
            <div class="page-header">
                <h1><?=$page->name?> <small><?=$page->description?></small></h1>
            </div>
            <?php
        }
    }
}