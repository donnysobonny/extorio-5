<?php
namespace Core\Components\BlockProcessors;
use Core\Classes\Enums\InternalPages;
/**
 * Displays a user menu as a login form if no user is logged in, or a link to their account and to log out.
 *
 * Class UserMenu
 */
class UserMenu extends \Core\Classes\Commons\BlockProcessor {

    public $align = "right";
    public $container_type = "none";

    protected function _onView() {
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        $config = $this->_Extorio()->getConfig();
        ?>
        <div style="text-align: <?=$this->align?>;">
            <?php
            if($loggedInUser) {
                ?>
                <strong>Welcome <?=ucwords($loggedInUser->username)?>!</strong>&nbsp;
                <a href="<?=InternalPages::_myAccount?>"><span class="fa fa-user"></span> Account</a>&nbsp;
                <a href="<?=InternalPages::_userLogout?>"><span class="fa fa-power-off"></span> Log out</a>
            <?php
            } else {
                ?>
                <strong>You are not logged in!</strong>&nbsp;
                <a href="<?=InternalPages::_userLogin?>"><span class="fa fa-arrow-right"></span> Log in</a><?php
                if($config["users"]["registration"]["enabled"]) {
                    ?>
                    &nbsp;<a href="<?=InternalPages::_userRegister?>"><span class="fa fa-user"></span> Register</a>
                <?php
                }
            }
            ?>
        </div>
        <?php
    }

    protected function _onEdit() {
        ?>
        <div class="form-group">
            <label for="align">Align</label>
            <select id="align" name="align" class="form-control">
                <option <?php
                if($this->align == "left") echo 'selected="selected"';
                ?> value="left">left</option>
                <option <?php
                if($this->align == "center") echo 'selected="selected"';
                ?> value="center">center</option>
                <option <?php
                if($this->align == "right") echo 'selected="selected"';
                ?> value="right">right</option>
            </select>
        </div>
        <?php
    }
}