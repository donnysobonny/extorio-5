<?php
namespace Core\Components\BlockProcessors;
use Core\Classes\Utilities\Strings;

/**
 * 
 *
 * Class Includer
 */
class Includer extends \Core\Classes\Commons\BlockProcessor {

    public $includers = array();
    public $prependtohead = "";
    public $appendtohead = "";
    public $prependtobody = "";
    public $appendtobody = "";

    protected function _onView() {
        foreach($this->includers as $includer) {
            $this->_includeIncluder($includer);
        }
        $this->_prependToHead($this->prependtohead);
        $this->_prependToBody($this->prependtobody);
        $this->_appendToHead($this->appendtohead);
        $this->_appendToBody($this->appendtobody);
    }

    protected function _onEdit() {
        $includers = $this->_Extorio()->getIncluders();
        foreach($includers as $name => $data) {
            ?>
            <div class="checkbox">
                <label>
                    <input <?php
                    if(in_array($name,$this->includers)) echo 'checked="checked"';
                    ?> name="includers[]" value="<?=$name?>" type="checkbox"> <?=Strings::titleSafe($name)?>
                </label>
                <?php
                if(strlen($data["description"])) {
                    ?><p class="help-block"><?=$data["description"]?></p><?php
                } else {
                    ?><p class="help-block">No description available.</p><?php
                }
                ?>
            </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="prependtohead">Prepend to head</label>
                    <textarea class="form-control" name="prependtohead" id="prependtohead"><?=$this->prependtohead?></textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="appendtohead">Append to head</label>
                    <textarea class="form-control" name="appendtohead" id="appendtohead"><?=$this->appendtohead?></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="prependtobody">Prepend to body</label>
                    <textarea class="form-control" name="prependtobody" id="prependtobody"><?=$this->prependtobody?></textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="appendtobody">Append to body</label>
                    <textarea class="form-control" name="appendtobody" id="appendtobody"><?=$this->appendtobody?></textarea>
                </div>
            </div>
        </div>
        <?php
    }
}