<?php
namespace Core\Components\BlockProcessors;
/**
 * Using markdown syntax, create content using a basic text box
 *
 * Class Markdown
 */
class Markdown extends \Core\Classes\Commons\BlockProcessor {
    public $content = "";

    protected function _onView() {
        $p = new \Parsedown();
        echo $p->text($this->content);
    }

    protected function _onEdit() {
        ?>
        <textarea rows="10" class="form-control" id="content" name="content"><?=$this->content?></textarea>
    <?php
    }
}