<?php
namespace Core\Components\BlockProcessors;
/**
 * Creates a horizontal line. Useful for creating defined spaces between rows.
 *
 * Class HorizontalRule
 */
class HorizontalRule extends \Core\Classes\Commons\BlockProcessor {
    protected function _onView() {
        ?><hr /><?php
    }
}