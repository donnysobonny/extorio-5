<?php
namespace Core\Components\BlockProcessors;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;

/**
 * Displays custom nav-menu items, either as links to a page, or to custom links
 *
 * Class CustomNavMenu
 */
class CustomNavMenu extends \Core\Classes\Commons\BlockProcessor {
    public $type = "tabs";
    public $style = "";

    public $itemTypes = array();
    public $itemPageIds = array();
    public $itemCustomNames = array();
    public $itemCustomLinks = array();
    public $itemCustomActives = array();

    protected function _onView() {
        $url = Server::getRequestURL();
        $url = Strings::startsAndEndsWith($url,"/");
        if(count($this->itemTypes)) {
            $db = $this->_Extorio()->getDbInstanceDefault();
            ?>
            <ul class="nav nav-<?=$this->type?><?php
            if($this->style == "justified") echo ' nav-justified';
            if($this->style == "stacked") echo ' nav-stacked';
            ?>">
                <?php
                $i = 0;
                foreach($this->itemTypes as $type) {
                    switch($type) {
                        case "page" :
                            $sql = 'SELECT name, address FROM core_classes_models_page WHERE id = $1 LIMIT 1';
                            $row = $db->query($sql, array($this->itemPageIds[$i]))->fetchRow();
                            if($row) {
                                ?><li class="<?php
                                if($url == $row[1]) echo 'active';
                                ?>"><a href="<?=$row[1]?>"><?=$row[0]?></a></li><?php
                            }
                            break;
                        case "custom" :
                            ?><li class="<?php
                        if($url == $this->itemCustomActives[$i]) echo 'active';
                        ?>"><a href="<?=$this->itemCustomLinks[$i]?>"><?=$this->itemCustomNames[$i]?></a></li><?php
                            break;
                    }
                    $i++;
                }
                ?>
            </ul><br />
            <?php
        }
    }

    protected function _onEdit() {
        ?>
        <div class="form-group">
            <label for="type">Menu type</label>
            <select class="form-control" id="type" name="type">
                <option <?php if($this->config["type"] == "pills") echo 'selected="selected"'; ?> value="pills">pills</option>
                <option <?php if($this->config["type"] == "tabs") echo 'selected="selected"'; ?> value="tabs">tabs</option>
            </select>
        </div>
        <div class="form-group">
            <label for="style">Menu style</label>
            <select class="form-control" id="style" name="style">
                <option value="">default</option>
                <option <?php if($this->config["style"] == "justified") echo 'selected="selected"'; ?> value="justified">justified</option>
                <option <?php if($this->config["style"] == "stacked") echo 'selected="selected"'; ?> value="stacked">stacked</option>
            </select>
        </div>

        <hr />
        <?php
        $db = $this->_Extorio()->getDbInstanceDefault();
        $sql = 'SELECT id, name FROM core_classes_models_page ORDER BY name ASC';
        $rows = $db->query($sql)->fetchAll();
        ?>
        <div id="items_container">

        </div>
        <button type="button" class="btn btn-block btn-primary new_menu_item">add new menu item</button>
        <script>
            $(function() {
                var pageData = <?=json_encode($rows)?>;
                var itemTypes = <?=json_encode($this->itemTypes)?>;
                var itemPageIds = <?=json_encode($this->itemPageIds)?>;
                var itemCustomNames = <?=json_encode($this->itemCustomNames)?>;
                var itemCustomLinks = <?=json_encode($this->itemCustomLinks)?>;
                var itemCustomActives = <?=json_encode($this->itemCustomActives)?>;

                var addItem = function(type,pageId,customName,customLink,customActive) {
                    var item = $('<div style="cursor: move;" class="well well-sm">' +
                        '   <div style="text-align: right;">' +
                        '       <button type="button" class="btn btn-xs btn-danger item_remove"><span class="fa fa-close"></span></button>' +
                        '   </div>' +
                        '   <div class="form-group">' +
                        '       <label>Item Type</label>' +
                        '       <select class="form-control itemTypes" name="itemTypes[]">' +
                        '           <option value="">--select a type--</option>' +
                        '           <option value="page">page</option>' +
                        '           <option value="custom">custom</option>' +
                        '       </select>' +
                        '   </div>' +
                        '   <div class="pageSection">' +
                        '       <div class="form-group">' +
                        '           <label>Page</label>' +
                        '           <select class="form-control itemPageIds" name="itemPageIds[]">' +
                        '           </select>' +
                        '       </div>' +
                        '   </div>' +
                        '   <div class="customSection">' +
                        '       <div class="form-group">' +
                        '           <label>Name</label>' +
                        '           <input type="text" class="form-control itemCustomNames" name="itemCustomNames[]" placeholder="Enter the name">' +
                        '       </div>' +
                        '       <div class="form-group">' +
                        '           <label>Link</label>' +
                        '           <input type="text" class="form-control itemCustomLinks" name="itemCustomLinks[]" placeholder="Enter the link">' +
                        '       </div>' +
                        '       <div class="form-group">' +
                        '           <label>Active link</label>' +
                        '           <input type="text" class="form-control itemCustomActives" name="itemCustomActives[]" placeholder="(optional) Enter the active link">' +
                        '       </div>' +
                        '   </div>' +
                        '</div>');

                    if(customName !== undefined) {
                        item.find('.itemCustomNames').val(customName);
                    }
                    if(customLink !== undefined) {
                        item.find('.itemCustomLinks').val(customLink);
                    }
                    if(customActive !== undefined) {
                        item.find('.itemCustomActives').val(customActive);
                    }

                    item.find('.item_remove').on("click", function() {
                        item.remove();
                    });

                    if(type !== undefined) {
                        item.find('.itemTypes').val(type);
                        switch (type) {
                            case "page" :
                                item.find('.pageSection').show();
                                item.find('.customSection').hide();
                                break;
                            case "custom" :
                                item.find('.pageSection').hide();
                                item.find('.customSection').show();
                                break;
                        }
                    } else {
                        item.find('.pageSection').hide();
                        item.find('.customSection').hide();
                    }
                    item.find('.itemTypes').on("change", function() {
                        var value = $(this).val();
                        if(value.length > 0) {
                            switch (value) {
                                case "page" :
                                    item.find('.pageSection').show();
                                    item.find('.customSection').hide();
                                    break;
                                case "custom" :
                                    item.find('.pageSection').hide();
                                    item.find('.customSection').show();
                                    break;
                            }
                        } else {
                            item.find('.pageSection').hide();
                            item.find('.customSection').hide();
                        }
                    });

                    var itemPageIdsSelect = item.find('.itemPageIds');
                    for(var i = 0; i < pageData.length; i++) {
                        itemPageIdsSelect.append('<option value="'+pageData[i].id+'">'+pageData[i].name+'</option>');
                    }
                    if(pageId !== undefined) {
                        itemPageIdsSelect.val(pageId);
                    }

                    $('#items_container').append(item);
                }

                for(var i = 0; i < itemTypes.length; i++) {
                    addItem(itemTypes[i],itemPageIds[i],itemCustomNames[i],itemCustomLinks[i],itemCustomActives[i]);
                }

                $('.new_menu_item').on("click", function() {
                    addItem();
                });

                $( "#items_container" ).sortable({
                    cancel: ".form-group"
                });
            });
        </script>
        <?php
    }
}