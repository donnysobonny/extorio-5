<?php
namespace Core\Components\BlockProcessors;
/**
 * Display a carousel with basic or custom content
 *
 * Class Carousel
 */
class Carousel extends \Core\Classes\Commons\BlockProcessor {
    
    public $types = array();
    public $images = array();
    public $titles = array();
    public $captions = array();
    public $customs = array();

    public $fadeedges = true;
    public $interval = 2000;
    public $showindicators = true;

    protected function _onView() {
        $uid = uniqid();
        ?>
        <div id="<?=$uid?>" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators<?php
            if(!$this->showindicators) echo ' carousel-hide-indicators';
            ?>">
                <?php
                for($i = 0; $i < count($this->types); $i++) {
                    ?>
                    <li data-target="#<?=$uid?>" data-slide-to="<?=$i?>" class="<?php
                    if($i == 0) echo 'active';
                    ?>"></li>
                    <?php
                }
                ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php
                for($i = 0; $i < count($this->types); $i++) {
                    ?>
                    <div class="item<?php
                    if($i == 0) echo ' active';
                    ?>">
                        <img style="width: 100%;" src="<?=$this->images[$i]?>" alt="Loading...">
                        <?php
                        switch($this->types[$i]) {
                            case "basic" :
                                ?>
                                <div class="carousel-caption">
                                    <?php
                                    if(strlen($this->titles[$i])) {
                                        ?><h3><?=$this->titles[$i]?></h3><?php
                                    }
                                    if(strlen($this->captions[$i])) {
                                        ?><p><?=$this->captions[$i]?></p><?php
                                    }
                                    ?>
                                </div>
                                <?php
                                break;
                            case "custom" :
                                ?>
                                <div class="carousel-caption carousel-caption-custom">
                                    <?=$this->customs[$i]?>
                                </div>
                                <?php
                                break;
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>

            <!-- Controls -->
            <a <?php
            if(!$this->fadeedges) {
                echo 'style="background: none !important;"';
            }
            ?> class="left carousel-control" href="#<?=$uid?>" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a <?php
            if(!$this->fadeedges) {
                echo 'style="background: none !important;"';
            }
            ?> class="right carousel-control" href="#<?=$uid?>" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <?php
    }

    protected function _onEdit() {
        $outer = uniqid();
        ?>
        <div class="row">
            <div class="col-xs-4">
                <div class="form-group">
                    <label for="interval">Rotation interval (milliseconds)</label>
                    <input value="<?=$this->interval?>" type="number" class="form-control" id="interval" name="interval" placeholder="Rotation interval (milliseconds)">
                </div>
            </div>
            <div class="col-xs-4">
                <div class="checkbox">
                    <label>
                        <input id="fadeedges" name="fadeedges" <?php
                        if($this->fadeedges) echo 'checked="checked"';
                        ?> type="checkbox"> Fade edges
                    </label>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="checkbox">
                    <label>
                        <input id="showindicators" name="showindicators" <?php
                        if($this->showindicators) echo 'checked="checked"';
                        ?> type="checkbox"> Display indicators
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="<?=$outer?>">

                </div>
            </div>
        </div>
        <button type="button" class="btn btn-block btn-primary carousel_new"><span class="fa fa-plus"></span> Add item</button>
        <script>
            $(function() {
                var outer = "#<?=$outer?>";
                var types = <?=json_encode($this->types)?>;
                var images = <?=json_encode($this->images)?>;
                var titles = <?=json_encode($this->titles)?>;
                var captions = <?=json_encode($this->captions)?>;
                var customs = <?=json_encode($this->customs)?>;

                $(outer).sortable({
                    cancel: ".form-group"
                });

                var addItem = function(type, image, title, caption, custom) {
                    var item = $('<div style="cursor: move;" class="well well-sm">' +
                        '   <div style="text-align: right;">' +
                        '       <button type="button" class="btn btn-xs btn-danger carousel_remove"><span class="fa fa-close"></span></button>' +
                        '   </div>' +
                        '   <div class="form-group">' +
                        '       <label>Image</label>' +
                        '       <div class="input-group">' +
                        '           <input value="'+image+'" type="text" class="form-control carousel_image" name="images[]">' +
                        '           <span class="input-group-btn">' +
                        '               <button class="btn btn-primary carousel_select" type="button">Select an image...</button>' +
                        '           </span>' +
                        '       </div>' +
                        '   </div>' +
                        '   <div class="form-group">' +
                        '       <label>Title</label>' +
                        '       <select class="form-control carousel_type_select" name="types[]">' +
                        '           <option value="basic">basic</option>' +
                        '           <option value="custom">custom</option>' +
                        '       </select>' +
                        '   </div>' +
                        '   <div class="carousel_basic_section">' +
                        '       <div class="form-group">' +
                        '           <label>Title</label>' +
                        '           <input value="'+title+'" type="text" class="form-control" name="titles[]" placeholder="(optional) Enter a title">' +
                        '       </div>' +
                        '       <div class="form-group">' +
                        '           <label>Caption</label>' +
                        '           <input value="'+caption+'" type="text" class="form-control" name="captions[]" placeholder="(optional) Enter a caption">' +
                        '       </div>' +
                        '   </div>' +
                        '   <div class="carousel_custom_section">' +
                        '       <div class="form-group">' +
                        '           <label>Custom content</label>' +
                        '           <textarea class="carousel_cusoms" name="customs[]">'+custom+'</textarea>' +
                        '       </div>' +
                        '   </div>' +
                        '</div>');
                    item.find('.carousel_remove').on("click", function() {
                        item.remove();
                    });
                    item.find('.carousel_type_select').on("change", function() {
                        switch ($(this).val()) {
                            case "basic" :
                                item.find('.carousel_basic_section').show();
                                item.find('.carousel_custom_section').hide();
                                break;
                            case "custom" :
                                item.find('.carousel_basic_section').hide();
                                item.find('.carousel_custom_section').show();
                                break;
                            default :
                                item.find('.carousel_basic_section').show();
                                item.find('.carousel_custom_section').hide();
                                break;
                        }
                    }).val(type);
                    switch (type) {
                        case "basic" :
                            item.find('.carousel_basic_section').show();
                            item.find('.carousel_custom_section').hide();
                            break;
                        case "custom" :
                            item.find('.carousel_basic_section').hide();
                            item.find('.carousel_custom_section').show();
                            break;
                        default :
                            item.find('.carousel_basic_section').show();
                            item.find('.carousel_custom_section').hide();
                            break;
                    }
                    item.find('.carousel_select').on("click", function() {
                        $.extorio_dialog_assetManager({
                            title: "Select an image...",
                            displaytypes: "image",
                            allowfiltering: false,
                            selectable: true,
                            size: "",
                            onfileselected: function(url) {
                                item.find('.carousel_image').val(url);
                            }
                        });
                    });
                    $(outer).append(item).find('.carousel_cusoms').extorio_editable();
                };

                $('.carousel_new').on("click", function() {
                    addItem("basic","","","","");
                });

                for(var i = 0; i < images.length; i++) {
                    addItem(types[i],images[i],titles[i],captions[i],customs[i]);
                }
            })
        </script>
        <?php
    }
}