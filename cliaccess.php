<?php
//second param send to this script should be the reqeust uri
$request = $argv[1];

//split out the url and the query from the uri
$url = "";
$query = "";
if(strpos($request,"?") !== false) {
    $url = substr($request,0,strpos($request,"?"));
    $query = substr($request,strpos($request,"?")+1,strlen($request) - strpos($request,"?"));
} else {
    $url = $request;
}

//force the server properties
$_SERVER["REQUEST_URI"] = $request;
$_SERVER["REDIRECT_URL"] = $url;
$_SERVER["QUERY_STRING"] = $query;

//assign the query $_GET
parse_str($query,$_GET);
//and append to $_REQUEST
foreach($_GET as $key => $value) {
    $_REQUEST[$key] = $value;
}

//and run as normal
require_once("index.php");